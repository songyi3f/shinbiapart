﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GameVanilla.Game.Popups.RegenLevelPopup
struct RegenLevelPopup_t1421666675;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1
struct U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978;
// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2
struct U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951;
// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3
struct U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673;
// GameVanilla.Game.Common.TileEntity
struct TileEntity_t3486638455;
// GameVanilla.Core.BaseScene
struct BaseScene_t2918414559;
// GameVanilla.Game.UI.BuyBoosterButton
struct BuyBoosterButton_t1892892637;
// GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey0
struct U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172;
// GameVanilla.Game.Scenes.GameScene
struct GameScene_t3472316885;
// GameVanilla.Game.Popups.LevelGoalsPopup
struct LevelGoalsPopup_t649240731;
// GameVanilla.Game.Popups.BoosterAwardPopup
struct BoosterAwardPopup_t3255387682;
// GameVanilla.Game.Scenes.LevelScene
struct LevelScene_t3942611905;
// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BlockType,System.Int32>
struct Dictionary_2_t2420221179;
// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BlockerType,System.Int32>
struct Dictionary_2_t866411006;
// System.Collections.Generic.List`1<GameVanilla.Game.Common.TileScoreOverride>
struct List_1_t1104741157;
// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BoosterType,System.Int32>
struct Dictionary_2_t265350959;
// System.Collections.Generic.List`1<GameVanilla.Game.Common.IapItem>
struct List_1_t625377672;
// System.Predicate`1<GameVanilla.Game.Common.TileScoreOverride>
struct Predicate_1_t457960539;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.String
struct String_t;
// GameVanilla.Core.Transition
struct Transition_t3232023259;
// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey13
struct U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330;
// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey15
struct U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258;
// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey10
struct U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928;
// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey12
struct U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<GameVanilla.Game.Common.LevelTile>
struct List_1_t1617233054;
// System.Collections.Generic.List`1<GameVanilla.Game.Common.Goal>
struct List_1_t2971766377;
// System.Collections.Generic.List`1<GameVanilla.Game.Common.ColorBlockType>
struct List_1_t1083490016;
// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BoosterType,System.Boolean>
struct Dictionary_2_t1706660467;
// System.Collections.Generic.List`1<GameVanilla.Game.Common.BoosterType>
struct List_1_t1057125992;
// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyE
struct U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t4243939292;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t2586782146;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.Collections.Generic.Stack`1<UnityEngine.GameObject>
struct Stack_1_t1957026074;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.Animator
struct Animator_t434523843;
// GameVanilla.Game.UI.ProgressBar
struct ProgressBar_t959013686;
// GameVanilla.Game.UI.GoalUi
struct GoalUi_t912033724;
// System.Predicate`1<GameVanilla.Game.Common.Goal>
struct Predicate_1_t2324985759;
// System.Action`2<System.TimeSpan,System.Int32>
struct Action_2_t3788266618;
// GameVanilla.Game.Common.PuzzleMatchManager
struct PuzzleMatchManager_t2899242229;
// GameVanilla.Game.Common.GameConfiguration
struct GameConfiguration_t2964563698;
// GameVanilla.Game.Common.LivesSystem
struct LivesSystem_t2420331318;
// GameVanilla.Game.Common.CoinsSystem
struct CoinsSystem_t740186038;
// GameVanilla.Core.ObjectPool
struct ObjectPool_t858432606;
// System.Collections.Generic.List`1<GameVanilla.Core.ObjectPool>
struct List_1_t2330507348;
// GameVanilla.Core.AnimatedButton
struct AnimatedButton_t997585140;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// GameVanilla.Core.Popup
struct Popup_t1063720813;
// GameVanilla.Game.Common.GamePools
struct GamePools_t2964324386;
// GameVanilla.Game.UI.GameUi
struct GameUi_t3170959104;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t857997111;
// GameVanilla.Game.Common.Level
struct Level_t3869101417;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// GameVanilla.Game.Common.GameState
struct GameState_t1141404161;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// GhostSkillInfo
struct GhostSkillInfo_t3192096645;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<GameVanilla.Game.Common.BoosterType,System.Int32>,System.Int32>
struct Func_2_t570526977;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t123837990;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// System.Action`1<GameVanilla.Game.Popups.AlertPopup>
struct Action_1_t327652521;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CAUTOKILLU3EC__ITERATOR1_T1621944280_H
#define U3CAUTOKILLU3EC__ITERATOR1_T1621944280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.RegenLevelPopup/<AutoKill>c__Iterator1
struct  U3CAutoKillU3Ec__Iterator1_t1621944280  : public RuntimeObject
{
public:
	// GameVanilla.Game.Popups.RegenLevelPopup GameVanilla.Game.Popups.RegenLevelPopup/<AutoKill>c__Iterator1::$this
	RegenLevelPopup_t1421666675 * ___U24this_0;
	// System.Object GameVanilla.Game.Popups.RegenLevelPopup/<AutoKill>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Popups.RegenLevelPopup/<AutoKill>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Popups.RegenLevelPopup/<AutoKill>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator1_t1621944280, ___U24this_0)); }
	inline RegenLevelPopup_t1421666675 * get_U24this_0() const { return ___U24this_0; }
	inline RegenLevelPopup_t1421666675 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RegenLevelPopup_t1421666675 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator1_t1621944280, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator1_t1621944280, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator1_t1621944280, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOKILLU3EC__ITERATOR1_T1621944280_H
#ifndef U3CDESTROYBOOSTERCORUTINEU3EC__ANONSTOREYF_T3071285680_H
#define U3CDESTROYBOOSTERCORUTINEU3EC__ANONSTOREYF_T3071285680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyF
struct  U3CDestroyBoosterCorutineU3Ec__AnonStoreyF_t3071285680  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyF::block
	GameObject_t1113636619 * ___block_0;
	// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1 GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyF::<>f__ref$1
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__AnonStoreyF_t3071285680, ___block_0)); }
	inline GameObject_t1113636619 * get_block_0() const { return ___block_0; }
	inline GameObject_t1113636619 ** get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(GameObject_t1113636619 * value)
	{
		___block_0 = value;
		Il2CppCodeGenWriteBarrier((&___block_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__AnonStoreyF_t3071285680, ___U3CU3Ef__refU241_1)); }
	inline U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBOOSTERCORUTINEU3EC__ANONSTOREYF_T3071285680_H
#ifndef U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY10_T2426847928_H
#define U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY10_T2426847928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey10
struct  U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey10::block
	GameObject_t1113636619 * ___block_0;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey10::<>f__ref$2
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928, ___block_0)); }
	inline GameObject_t1113636619 * get_block_0() const { return ___block_0; }
	inline GameObject_t1113636619 ** get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(GameObject_t1113636619 * value)
	{
		___block_0 = value;
		Il2CppCodeGenWriteBarrier((&___block_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928, ___U3CU3Ef__refU242_1)); }
	inline U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY10_T2426847928_H
#ifndef U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY11_T88195768_H
#define U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY11_T88195768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey11
struct  U3CColorBomb_B_MatchU3Ec__AnonStorey11_t88195768  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey11::obj
	GameObject_t1113636619 * ___obj_0;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey11::<>f__ref$2
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__AnonStorey11_t88195768, ___obj_0)); }
	inline GameObject_t1113636619 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t1113636619 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t1113636619 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__AnonStorey11_t88195768, ___U3CU3Ef__refU242_1)); }
	inline U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY11_T88195768_H
#ifndef U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY12_T2809184952_H
#define U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY12_T2809184952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey12
struct  U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey12::block
	GameObject_t1113636619 * ___block_0;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey12::<>f__ref$2
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952, ___block_0)); }
	inline GameObject_t1113636619 * get_block_0() const { return ___block_0; }
	inline GameObject_t1113636619 ** get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(GameObject_t1113636619 * value)
	{
		___block_0 = value;
		Il2CppCodeGenWriteBarrier((&___block_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952, ___U3CU3Ef__refU242_1)); }
	inline U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_B_MATCHU3EC__ANONSTOREY12_T2809184952_H
#ifndef U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY13_T1455563330_H
#define U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY13_T1455563330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey13
struct  U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey13::block
	GameObject_t1113636619 * ___block_0;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey13::<>f__ref$3
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330, ___block_0)); }
	inline GameObject_t1113636619 * get_block_0() const { return ___block_0; }
	inline GameObject_t1113636619 ** get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(GameObject_t1113636619 * value)
	{
		___block_0 = value;
		Il2CppCodeGenWriteBarrier((&___block_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330, ___U3CU3Ef__refU243_1)); }
	inline U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY13_T1455563330_H
#ifndef U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY14_T2647204418_H
#define U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY14_T2647204418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey14
struct  U3CColorBomb_A_MatchU3Ec__AnonStorey14_t2647204418  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey14::obj
	GameObject_t1113636619 * ___obj_0;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey14::<>f__ref$3
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__AnonStorey14_t2647204418, ___obj_0)); }
	inline GameObject_t1113636619 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t1113636619 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t1113636619 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__AnonStorey14_t2647204418, ___U3CU3Ef__refU243_1)); }
	inline U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY14_T2647204418_H
#ifndef U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY15_T308552258_H
#define U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY15_T308552258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey15
struct  U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey15::block
	GameObject_t1113636619 * ___block_0;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey15::<>f__ref$3
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258, ___block_0)); }
	inline GameObject_t1113636619 * get_block_0() const { return ___block_0; }
	inline GameObject_t1113636619 ** get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(GameObject_t1113636619 * value)
	{
		___block_0 = value;
		Il2CppCodeGenWriteBarrier((&___block_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258, ___U3CU3Ef__refU243_1)); }
	inline U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_A_MATCHU3EC__ANONSTOREY15_T308552258_H
#ifndef U3CDESTROYBOOSTERRECURSIVEU3EC__ANONSTOREY16_T3723667014_H
#define U3CDESTROYBOOSTERRECURSIVEU3EC__ANONSTOREY16_T3723667014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterRecursive>c__AnonStorey16
struct  U3CDestroyBoosterRecursiveU3Ec__AnonStorey16_t3723667014  : public RuntimeObject
{
public:
	// GameVanilla.Game.Common.TileEntity GameVanilla.Game.Scenes.GameScene/<DestroyBoosterRecursive>c__AnonStorey16::blockToDestroy
	TileEntity_t3486638455 * ___blockToDestroy_0;

public:
	inline static int32_t get_offset_of_blockToDestroy_0() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterRecursiveU3Ec__AnonStorey16_t3723667014, ___blockToDestroy_0)); }
	inline TileEntity_t3486638455 * get_blockToDestroy_0() const { return ___blockToDestroy_0; }
	inline TileEntity_t3486638455 ** get_address_of_blockToDestroy_0() { return &___blockToDestroy_0; }
	inline void set_blockToDestroy_0(TileEntity_t3486638455 * value)
	{
		___blockToDestroy_0 = value;
		Il2CppCodeGenWriteBarrier((&___blockToDestroy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBOOSTERRECURSIVEU3EC__ANONSTOREY16_T3723667014_H
#ifndef U3CHIGHLIGHTRANDOMMATCHU3EC__ANONSTOREY17_T3640827137_H
#define U3CHIGHLIGHTRANDOMMATCHU3EC__ANONSTOREY17_T3640827137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<HighlightRandomMatch>c__AnonStorey17
struct  U3CHighlightRandomMatchU3Ec__AnonStorey17_t3640827137  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<HighlightRandomMatch>c__AnonStorey17::tile
	GameObject_t1113636619 * ___tile_0;

public:
	inline static int32_t get_offset_of_tile_0() { return static_cast<int32_t>(offsetof(U3CHighlightRandomMatchU3Ec__AnonStorey17_t3640827137, ___tile_0)); }
	inline GameObject_t1113636619 * get_tile_0() const { return ___tile_0; }
	inline GameObject_t1113636619 ** get_address_of_tile_0() { return &___tile_0; }
	inline void set_tile_0(GameObject_t1113636619 * value)
	{
		___tile_0 = value;
		Il2CppCodeGenWriteBarrier((&___tile_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIGHLIGHTRANDOMMATCHU3EC__ANONSTOREY17_T3640827137_H
#ifndef U3CDESTROYBOOSTERCORUTINEU3EC__ANONSTOREYE_T3071285677_H
#define U3CDESTROYBOOSTERCORUTINEU3EC__ANONSTOREYE_T3071285677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyE
struct  U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677  : public RuntimeObject
{
public:
	// GameVanilla.Game.Common.TileEntity GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyE::blockToDestroy
	TileEntity_t3486638455 * ___blockToDestroy_0;
	// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1 GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyE::<>f__ref$1
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_blockToDestroy_0() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677, ___blockToDestroy_0)); }
	inline TileEntity_t3486638455 * get_blockToDestroy_0() const { return ___blockToDestroy_0; }
	inline TileEntity_t3486638455 ** get_address_of_blockToDestroy_0() { return &___blockToDestroy_0; }
	inline void set_blockToDestroy_0(TileEntity_t3486638455 * value)
	{
		___blockToDestroy_0 = value;
		Il2CppCodeGenWriteBarrier((&___blockToDestroy_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677, ___U3CU3Ef__refU241_1)); }
	inline U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBOOSTERCORUTINEU3EC__ANONSTOREYE_T3071285677_H
#ifndef U3CONREFILLBUTTONPRESSEDU3EC__ANONSTOREY0_T3808345845_H
#define U3CONREFILLBUTTONPRESSEDU3EC__ANONSTOREY0_T3808345845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BuyLivesPopup/<OnRefillButtonPressed>c__AnonStorey0
struct  U3COnRefillButtonPressedU3Ec__AnonStorey0_t3808345845  : public RuntimeObject
{
public:
	// GameVanilla.Core.BaseScene GameVanilla.Game.Popups.BuyLivesPopup/<OnRefillButtonPressed>c__AnonStorey0::scene
	BaseScene_t2918414559 * ___scene_0;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(U3COnRefillButtonPressedU3Ec__AnonStorey0_t3808345845, ___scene_0)); }
	inline BaseScene_t2918414559 * get_scene_0() const { return ___scene_0; }
	inline BaseScene_t2918414559 ** get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(BaseScene_t2918414559 * value)
	{
		___scene_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONREFILLBUTTONPRESSEDU3EC__ANONSTOREY0_T3808345845_H
#ifndef U3CONBUYBUTTONPRESSEDU3EC__ANONSTOREY1_T3327493113_H
#define U3CONBUYBUTTONPRESSEDU3EC__ANONSTOREY1_T3327493113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey1
struct  U3COnBuyButtonPressedU3Ec__AnonStorey1_t3327493113  : public RuntimeObject
{
public:
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey1::button
	BuyBoosterButton_t1892892637 * ___button_0;
	// GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey0 GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey1::<>f__ref$0
	U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3COnBuyButtonPressedU3Ec__AnonStorey1_t3327493113, ___button_0)); }
	inline BuyBoosterButton_t1892892637 * get_button_0() const { return ___button_0; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(BuyBoosterButton_t1892892637 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier((&___button_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3COnBuyButtonPressedU3Ec__AnonStorey1_t3327493113, ___U3CU3Ef__refU240_1)); }
	inline U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBUYBUTTONPRESSEDU3EC__ANONSTOREY1_T3327493113_H
#ifndef U3CONBUYBUTTONPRESSEDU3EC__ANONSTOREY0_T1761409172_H
#define U3CONBUYBUTTONPRESSEDU3EC__ANONSTOREY0_T1761409172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey0
struct  U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172  : public RuntimeObject
{
public:
	// GameVanilla.Core.BaseScene GameVanilla.Game.Popups.BuyBoostersPopup/<OnBuyButtonPressed>c__AnonStorey0::scene
	BaseScene_t2918414559 * ___scene_0;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172, ___scene_0)); }
	inline BaseScene_t2918414559 * get_scene_0() const { return ___scene_0; }
	inline BaseScene_t2918414559 ** get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(BaseScene_t2918414559 * value)
	{
		___scene_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBUYBUTTONPRESSEDU3EC__ANONSTOREY0_T1761409172_H
#ifndef U3CAUTOKILLU3EC__ITERATOR0_T994986258_H
#define U3CAUTOKILLU3EC__ITERATOR0_T994986258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.LevelGoalsPopup/<AutoKill>c__Iterator0
struct  U3CAutoKillU3Ec__Iterator0_t994986258  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Popups.LevelGoalsPopup/<AutoKill>c__Iterator0::<gameScene>__0
	GameScene_t3472316885 * ___U3CgameSceneU3E__0_0;
	// GameVanilla.Game.Popups.LevelGoalsPopup GameVanilla.Game.Popups.LevelGoalsPopup/<AutoKill>c__Iterator0::$this
	LevelGoalsPopup_t649240731 * ___U24this_1;
	// System.Object GameVanilla.Game.Popups.LevelGoalsPopup/<AutoKill>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameVanilla.Game.Popups.LevelGoalsPopup/<AutoKill>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameVanilla.Game.Popups.LevelGoalsPopup/<AutoKill>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CgameSceneU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t994986258, ___U3CgameSceneU3E__0_0)); }
	inline GameScene_t3472316885 * get_U3CgameSceneU3E__0_0() const { return ___U3CgameSceneU3E__0_0; }
	inline GameScene_t3472316885 ** get_address_of_U3CgameSceneU3E__0_0() { return &___U3CgameSceneU3E__0_0; }
	inline void set_U3CgameSceneU3E__0_0(GameScene_t3472316885 * value)
	{
		___U3CgameSceneU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameSceneU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t994986258, ___U24this_1)); }
	inline LevelGoalsPopup_t649240731 * get_U24this_1() const { return ___U24this_1; }
	inline LevelGoalsPopup_t649240731 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LevelGoalsPopup_t649240731 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t994986258, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t994986258, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t994986258, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOKILLU3EC__ITERATOR0_T994986258_H
#ifndef U3CAUTOCLOSEU3EC__ITERATOR0_T1565191538_H
#define U3CAUTOCLOSEU3EC__ITERATOR0_T1565191538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BoosterAwardPopup/<AutoClose>c__Iterator0
struct  U3CAutoCloseU3Ec__Iterator0_t1565191538  : public RuntimeObject
{
public:
	// GameVanilla.Game.Popups.BoosterAwardPopup GameVanilla.Game.Popups.BoosterAwardPopup/<AutoClose>c__Iterator0::$this
	BoosterAwardPopup_t3255387682 * ___U24this_0;
	// System.Object GameVanilla.Game.Popups.BoosterAwardPopup/<AutoClose>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Popups.BoosterAwardPopup/<AutoClose>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Popups.BoosterAwardPopup/<AutoClose>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoCloseU3Ec__Iterator0_t1565191538, ___U24this_0)); }
	inline BoosterAwardPopup_t3255387682 * get_U24this_0() const { return ___U24this_0; }
	inline BoosterAwardPopup_t3255387682 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(BoosterAwardPopup_t3255387682 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoCloseU3Ec__Iterator0_t1565191538, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoCloseU3Ec__Iterator0_t1565191538, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoCloseU3Ec__Iterator0_t1565191538, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOCLOSEU3EC__ITERATOR0_T1565191538_H
#ifndef U3COPENCOINSPOPUPU3EC__ANONSTOREY0_T1530218158_H
#define U3COPENCOINSPOPUPU3EC__ANONSTOREY0_T1530218158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.NoMovesOrTimePopup/<OpenCoinsPopup>c__AnonStorey0
struct  U3COpenCoinsPopupU3Ec__AnonStorey0_t1530218158  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Popups.NoMovesOrTimePopup/<OpenCoinsPopup>c__AnonStorey0::scene
	GameScene_t3472316885 * ___scene_0;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(U3COpenCoinsPopupU3Ec__AnonStorey0_t1530218158, ___scene_0)); }
	inline GameScene_t3472316885 * get_scene_0() const { return ___scene_0; }
	inline GameScene_t3472316885 ** get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(GameScene_t3472316885 * value)
	{
		___scene_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENCOINSPOPUPU3EC__ANONSTOREY0_T1530218158_H
#ifndef U3CANIMATETEXTU3EC__ITERATOR0_T3670638568_H
#define U3CANIMATETEXTU3EC__ITERATOR0_T3670638568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.RegenLevelPopup/<AnimateText>c__Iterator0
struct  U3CAnimateTextU3Ec__Iterator0_t3670638568  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Popups.RegenLevelPopup/<AnimateText>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// GameVanilla.Game.Popups.RegenLevelPopup GameVanilla.Game.Popups.RegenLevelPopup/<AnimateText>c__Iterator0::$this
	RegenLevelPopup_t1421666675 * ___U24this_1;
	// System.Object GameVanilla.Game.Popups.RegenLevelPopup/<AnimateText>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameVanilla.Game.Popups.RegenLevelPopup/<AnimateText>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameVanilla.Game.Popups.RegenLevelPopup/<AnimateText>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t3670638568, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t3670638568, ___U24this_1)); }
	inline RegenLevelPopup_t1421666675 * get_U24this_1() const { return ___U24this_1; }
	inline RegenLevelPopup_t1421666675 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RegenLevelPopup_t1421666675 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t3670638568, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t3670638568, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimateTextU3Ec__Iterator0_t3670638568, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATETEXTU3EC__ITERATOR0_T3670638568_H
#ifndef U3CDESTROYBLOCKU3EC__ANONSTOREYC_T3224521225_H
#define U3CDESTROYBLOCKU3EC__ANONSTOREYC_T3224521225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<DestroyBlock>c__AnonStoreyC
struct  U3CDestroyBlockU3Ec__AnonStoreyC_t3224521225  : public RuntimeObject
{
public:
	// GameVanilla.Game.Common.TileEntity GameVanilla.Game.Scenes.GameScene/<DestroyBlock>c__AnonStoreyC::blockToDestroy
	TileEntity_t3486638455 * ___blockToDestroy_0;

public:
	inline static int32_t get_offset_of_blockToDestroy_0() { return static_cast<int32_t>(offsetof(U3CDestroyBlockU3Ec__AnonStoreyC_t3224521225, ___blockToDestroy_0)); }
	inline TileEntity_t3486638455 * get_blockToDestroy_0() const { return ___blockToDestroy_0; }
	inline TileEntity_t3486638455 ** get_address_of_blockToDestroy_0() { return &___blockToDestroy_0; }
	inline void set_blockToDestroy_0(TileEntity_t3486638455 * value)
	{
		___blockToDestroy_0 = value;
		Il2CppCodeGenWriteBarrier((&___blockToDestroy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBLOCKU3EC__ANONSTOREYC_T3224521225_H
#ifndef U3CDESTROYBLOCKU3EC__ANONSTOREYD_T3224586761_H
#define U3CDESTROYBLOCKU3EC__ANONSTOREYD_T3224586761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<DestroyBlock>c__AnonStoreyD
struct  U3CDestroyBlockU3Ec__AnonStoreyD_t3224586761  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<DestroyBlock>c__AnonStoreyD::block
	GameObject_t1113636619 * ___block_0;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CDestroyBlockU3Ec__AnonStoreyD_t3224586761, ___block_0)); }
	inline GameObject_t1113636619 * get_block_0() const { return ___block_0; }
	inline GameObject_t1113636619 ** get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(GameObject_t1113636619 * value)
	{
		___block_0 = value;
		Il2CppCodeGenWriteBarrier((&___block_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBLOCKU3EC__ANONSTOREYD_T3224586761_H
#ifndef U3CANIMATELIMITDOWNU3EC__ITERATOR0_T2094673283_H
#define U3CANIMATELIMITDOWNU3EC__ITERATOR0_T2094673283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0
struct  U3CAnimateLimitDownU3Ec__Iterator0_t2094673283  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0::penalty
	int32_t ___penalty_0;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0::<endValue>__0
	int32_t ___U3CendValueU3E__0_1;
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0::$this
	GameScene_t3472316885 * ___U24this_2;
	// System.Object GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<AnimateLimitDown>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_penalty_0() { return static_cast<int32_t>(offsetof(U3CAnimateLimitDownU3Ec__Iterator0_t2094673283, ___penalty_0)); }
	inline int32_t get_penalty_0() const { return ___penalty_0; }
	inline int32_t* get_address_of_penalty_0() { return &___penalty_0; }
	inline void set_penalty_0(int32_t value)
	{
		___penalty_0 = value;
	}

	inline static int32_t get_offset_of_U3CendValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateLimitDownU3Ec__Iterator0_t2094673283, ___U3CendValueU3E__0_1)); }
	inline int32_t get_U3CendValueU3E__0_1() const { return ___U3CendValueU3E__0_1; }
	inline int32_t* get_address_of_U3CendValueU3E__0_1() { return &___U3CendValueU3E__0_1; }
	inline void set_U3CendValueU3E__0_1(int32_t value)
	{
		___U3CendValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAnimateLimitDownU3Ec__Iterator0_t2094673283, ___U24this_2)); }
	inline GameScene_t3472316885 * get_U24this_2() const { return ___U24this_2; }
	inline GameScene_t3472316885 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GameScene_t3472316885 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CAnimateLimitDownU3Ec__Iterator0_t2094673283, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CAnimateLimitDownU3Ec__Iterator0_t2094673283, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CAnimateLimitDownU3Ec__Iterator0_t2094673283, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATELIMITDOWNU3EC__ITERATOR0_T2094673283_H
#ifndef U3CREGENERATELEVELU3EC__ITERATOR4_T3676849939_H
#define U3CREGENERATELEVELU3EC__ITERATOR4_T3676849939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<RegenerateLevel>c__Iterator4
struct  U3CRegenerateLevelU3Ec__Iterator4_t3676849939  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<RegenerateLevel>c__Iterator4::$this
	GameScene_t3472316885 * ___U24this_0;
	// System.Object GameVanilla.Game.Scenes.GameScene/<RegenerateLevel>c__Iterator4::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<RegenerateLevel>c__Iterator4::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<RegenerateLevel>c__Iterator4::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRegenerateLevelU3Ec__Iterator4_t3676849939, ___U24this_0)); }
	inline GameScene_t3472316885 * get_U24this_0() const { return ___U24this_0; }
	inline GameScene_t3472316885 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameScene_t3472316885 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRegenerateLevelU3Ec__Iterator4_t3676849939, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRegenerateLevelU3Ec__Iterator4_t3676849939, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRegenerateLevelU3Ec__Iterator4_t3676849939, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREGENERATELEVELU3EC__ITERATOR4_T3676849939_H
#ifndef U3CAPPLYGRAVITYU3EC__ANONSTOREY1B_T1444154256_H
#define U3CAPPLYGRAVITYU3EC__ANONSTOREY1B_T1444154256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ApplyGravity>c__AnonStorey1B
struct  U3CApplyGravityU3Ec__AnonStorey1B_t1444154256  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ApplyGravity>c__AnonStorey1B::tile
	GameObject_t1113636619 * ___tile_0;

public:
	inline static int32_t get_offset_of_tile_0() { return static_cast<int32_t>(offsetof(U3CApplyGravityU3Ec__AnonStorey1B_t1444154256, ___tile_0)); }
	inline GameObject_t1113636619 * get_tile_0() const { return ___tile_0; }
	inline GameObject_t1113636619 ** get_address_of_tile_0() { return &___tile_0; }
	inline void set_tile_0(GameObject_t1113636619 * value)
	{
		___tile_0 = value;
		Il2CppCodeGenWriteBarrier((&___tile_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPLYGRAVITYU3EC__ANONSTOREY1B_T1444154256_H
#ifndef U3CSTARTCOUNTDOWNU3EC__ITERATOR6_T3776475245_H
#define U3CSTARTCOUNTDOWNU3EC__ITERATOR6_T3776475245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<StartCountdown>c__Iterator6
struct  U3CStartCountdownU3Ec__Iterator6_t3776475245  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<StartCountdown>c__Iterator6::$this
	GameScene_t3472316885 * ___U24this_0;
	// System.Object GameVanilla.Game.Scenes.GameScene/<StartCountdown>c__Iterator6::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<StartCountdown>c__Iterator6::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<StartCountdown>c__Iterator6::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartCountdownU3Ec__Iterator6_t3776475245, ___U24this_0)); }
	inline GameScene_t3472316885 * get_U24this_0() const { return ___U24this_0; }
	inline GameScene_t3472316885 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameScene_t3472316885 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartCountdownU3Ec__Iterator6_t3776475245, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartCountdownU3Ec__Iterator6_t3776475245, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartCountdownU3Ec__Iterator6_t3776475245, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTCOUNTDOWNU3EC__ITERATOR6_T3776475245_H
#ifndef U3COPENWINPOPUPASYNCU3EC__ITERATOR8_T2899540316_H
#define U3COPENWINPOPUPASYNCU3EC__ITERATOR8_T2899540316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<OpenWinPopupAsync>c__Iterator8
struct  U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<OpenWinPopupAsync>c__Iterator8::$this
	GameScene_t3472316885 * ___U24this_0;
	// System.Object GameVanilla.Game.Scenes.GameScene/<OpenWinPopupAsync>c__Iterator8::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<OpenWinPopupAsync>c__Iterator8::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<OpenWinPopupAsync>c__Iterator8::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316, ___U24this_0)); }
	inline GameScene_t3472316885 * get_U24this_0() const { return ___U24this_0; }
	inline GameScene_t3472316885 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameScene_t3472316885 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENWINPOPUPASYNCU3EC__ITERATOR8_T2899540316_H
#ifndef U3COPENNOMOVESORTIMEPOPUPASYNCU3EC__ITERATOR9_T1912861982_H
#define U3COPENNOMOVESORTIMEPOPUPASYNCU3EC__ITERATOR9_T1912861982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<OpenNoMovesOrTimePopupAsync>c__Iterator9
struct  U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<OpenNoMovesOrTimePopupAsync>c__Iterator9::$this
	GameScene_t3472316885 * ___U24this_0;
	// System.Object GameVanilla.Game.Scenes.GameScene/<OpenNoMovesOrTimePopupAsync>c__Iterator9::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<OpenNoMovesOrTimePopupAsync>c__Iterator9::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<OpenNoMovesOrTimePopupAsync>c__Iterator9::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982, ___U24this_0)); }
	inline GameScene_t3472316885 * get_U24this_0() const { return ___U24this_0; }
	inline GameScene_t3472316885 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameScene_t3472316885 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COPENNOMOVESORTIMEPOPUPASYNCU3EC__ITERATOR9_T1912861982_H
#ifndef U3CONBOOSTERBUTTONPRESSEDU3EC__ANONSTOREY1C_T306576499_H
#define U3CONBOOSTERBUTTONPRESSEDU3EC__ANONSTOREY1C_T306576499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<OnBoosterButtonPressed>c__AnonStorey1C
struct  U3COnBoosterButtonPressedU3Ec__AnonStorey1C_t306576499  : public RuntimeObject
{
public:
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Scenes.GameScene/<OnBoosterButtonPressed>c__AnonStorey1C::button
	BuyBoosterButton_t1892892637 * ___button_0;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3COnBoosterButtonPressedU3Ec__AnonStorey1C_t306576499, ___button_0)); }
	inline BuyBoosterButton_t1892892637 * get_button_0() const { return ___button_0; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(BuyBoosterButton_t1892892637 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier((&___button_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONBOOSTERBUTTONPRESSEDU3EC__ANONSTOREY1C_T306576499_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T2227182473_H
#define U3CSTARTU3EC__ANONSTOREY0_T2227182473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.LevelScene/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t2227182473  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.LevelScene/<Start>c__AnonStorey0::avatar
	GameObject_t1113636619 * ___avatar_0;
	// GameVanilla.Game.Scenes.LevelScene GameVanilla.Game.Scenes.LevelScene/<Start>c__AnonStorey0::$this
	LevelScene_t3942611905 * ___U24this_1;

public:
	inline static int32_t get_offset_of_avatar_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t2227182473, ___avatar_0)); }
	inline GameObject_t1113636619 * get_avatar_0() const { return ___avatar_0; }
	inline GameObject_t1113636619 ** get_address_of_avatar_0() { return &___avatar_0; }
	inline void set_avatar_0(GameObject_t1113636619 * value)
	{
		___avatar_0 = value;
		Il2CppCodeGenWriteBarrier((&___avatar_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t2227182473, ___U24this_1)); }
	inline LevelScene_t3942611905 * get_U24this_1() const { return ___U24this_1; }
	inline LevelScene_t3942611905 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LevelScene_t3942611905 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T2227182473_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TILESCOREOVERRIDE_T3927633711_H
#define TILESCOREOVERRIDE_T3927633711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.TileScoreOverride
struct  TileScoreOverride_t3927633711  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Common.TileScoreOverride::score
	int32_t ___score_0;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(TileScoreOverride_t3927633711, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILESCOREOVERRIDE_T3927633711_H
#ifndef GOAL_T1499691635_H
#define GOAL_T1499691635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Goal
struct  Goal_t1499691635  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOAL_T1499691635_H
#ifndef GAMESTATE_T1141404161_H
#define GAMESTATE_T1141404161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.GameState
struct  GameState_t1141404161  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Common.GameState::score
	int32_t ___score_0;
	// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BlockType,System.Int32> GameVanilla.Game.Common.GameState::collectedBlocks
	Dictionary_2_t2420221179 * ___collectedBlocks_1;
	// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BlockerType,System.Int32> GameVanilla.Game.Common.GameState::collectedBlockers
	Dictionary_2_t866411006 * ___collectedBlockers_2;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(GameState_t1141404161, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}

	inline static int32_t get_offset_of_collectedBlocks_1() { return static_cast<int32_t>(offsetof(GameState_t1141404161, ___collectedBlocks_1)); }
	inline Dictionary_2_t2420221179 * get_collectedBlocks_1() const { return ___collectedBlocks_1; }
	inline Dictionary_2_t2420221179 ** get_address_of_collectedBlocks_1() { return &___collectedBlocks_1; }
	inline void set_collectedBlocks_1(Dictionary_2_t2420221179 * value)
	{
		___collectedBlocks_1 = value;
		Il2CppCodeGenWriteBarrier((&___collectedBlocks_1), value);
	}

	inline static int32_t get_offset_of_collectedBlockers_2() { return static_cast<int32_t>(offsetof(GameState_t1141404161, ___collectedBlockers_2)); }
	inline Dictionary_2_t866411006 * get_collectedBlockers_2() const { return ___collectedBlockers_2; }
	inline Dictionary_2_t866411006 ** get_address_of_collectedBlockers_2() { return &___collectedBlockers_2; }
	inline void set_collectedBlockers_2(Dictionary_2_t866411006 * value)
	{
		___collectedBlockers_2 = value;
		Il2CppCodeGenWriteBarrier((&___collectedBlockers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTATE_T1141404161_H
#ifndef U3CGETMATCHESU3EC__ANONSTOREY18_T620215367_H
#define U3CGETMATCHESU3EC__ANONSTOREY18_T620215367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<GetMatches>c__AnonStorey18
struct  U3CGetMatchesU3Ec__AnonStorey18_t620215367  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<GetMatches>c__AnonStorey18::go
	GameObject_t1113636619 * ___go_0;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(U3CGetMatchesU3Ec__AnonStorey18_t620215367, ___go_0)); }
	inline GameObject_t1113636619 * get_go_0() const { return ___go_0; }
	inline GameObject_t1113636619 ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_t1113636619 * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETMATCHESU3EC__ANONSTOREY18_T620215367_H
#ifndef GAMECONFIGURATION_T2964563698_H
#define GAMECONFIGURATION_T2964563698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.GameConfiguration
struct  GameConfiguration_t2964563698  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::maxLives
	int32_t ___maxLives_0;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::timeToNextLife
	int32_t ___timeToNextLife_1;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::livesRefillCost
	int32_t ___livesRefillCost_2;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::initialCoins
	int32_t ___initialCoins_3;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::numExtraMoves
	int32_t ___numExtraMoves_4;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::extraMovesCost
	int32_t ___extraMovesCost_5;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::numExtraTime
	int32_t ___numExtraTime_6;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::extraTimeCost
	int32_t ___extraTimeCost_7;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::defaultTileScore
	int32_t ___defaultTileScore_8;
	// System.Collections.Generic.List`1<GameVanilla.Game.Common.TileScoreOverride> GameVanilla.Game.Common.GameConfiguration::tileScoreOverrides
	List_1_t1104741157 * ___tileScoreOverrides_9;
	// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BoosterType,System.Int32> GameVanilla.Game.Common.GameConfiguration::boosterNeededMatches
	Dictionary_2_t265350959 * ___boosterNeededMatches_10;
	// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BoosterType,System.Int32> GameVanilla.Game.Common.GameConfiguration::ingameBoosterAmount
	Dictionary_2_t265350959 * ___ingameBoosterAmount_11;
	// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BoosterType,System.Int32> GameVanilla.Game.Common.GameConfiguration::ingameBoosterCost
	Dictionary_2_t265350959 * ___ingameBoosterCost_12;
	// System.Int32 GameVanilla.Game.Common.GameConfiguration::rewardedAdCoins
	int32_t ___rewardedAdCoins_13;
	// System.Collections.Generic.List`1<GameVanilla.Game.Common.IapItem> GameVanilla.Game.Common.GameConfiguration::iapItems
	List_1_t625377672 * ___iapItems_14;

public:
	inline static int32_t get_offset_of_maxLives_0() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___maxLives_0)); }
	inline int32_t get_maxLives_0() const { return ___maxLives_0; }
	inline int32_t* get_address_of_maxLives_0() { return &___maxLives_0; }
	inline void set_maxLives_0(int32_t value)
	{
		___maxLives_0 = value;
	}

	inline static int32_t get_offset_of_timeToNextLife_1() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___timeToNextLife_1)); }
	inline int32_t get_timeToNextLife_1() const { return ___timeToNextLife_1; }
	inline int32_t* get_address_of_timeToNextLife_1() { return &___timeToNextLife_1; }
	inline void set_timeToNextLife_1(int32_t value)
	{
		___timeToNextLife_1 = value;
	}

	inline static int32_t get_offset_of_livesRefillCost_2() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___livesRefillCost_2)); }
	inline int32_t get_livesRefillCost_2() const { return ___livesRefillCost_2; }
	inline int32_t* get_address_of_livesRefillCost_2() { return &___livesRefillCost_2; }
	inline void set_livesRefillCost_2(int32_t value)
	{
		___livesRefillCost_2 = value;
	}

	inline static int32_t get_offset_of_initialCoins_3() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___initialCoins_3)); }
	inline int32_t get_initialCoins_3() const { return ___initialCoins_3; }
	inline int32_t* get_address_of_initialCoins_3() { return &___initialCoins_3; }
	inline void set_initialCoins_3(int32_t value)
	{
		___initialCoins_3 = value;
	}

	inline static int32_t get_offset_of_numExtraMoves_4() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___numExtraMoves_4)); }
	inline int32_t get_numExtraMoves_4() const { return ___numExtraMoves_4; }
	inline int32_t* get_address_of_numExtraMoves_4() { return &___numExtraMoves_4; }
	inline void set_numExtraMoves_4(int32_t value)
	{
		___numExtraMoves_4 = value;
	}

	inline static int32_t get_offset_of_extraMovesCost_5() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___extraMovesCost_5)); }
	inline int32_t get_extraMovesCost_5() const { return ___extraMovesCost_5; }
	inline int32_t* get_address_of_extraMovesCost_5() { return &___extraMovesCost_5; }
	inline void set_extraMovesCost_5(int32_t value)
	{
		___extraMovesCost_5 = value;
	}

	inline static int32_t get_offset_of_numExtraTime_6() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___numExtraTime_6)); }
	inline int32_t get_numExtraTime_6() const { return ___numExtraTime_6; }
	inline int32_t* get_address_of_numExtraTime_6() { return &___numExtraTime_6; }
	inline void set_numExtraTime_6(int32_t value)
	{
		___numExtraTime_6 = value;
	}

	inline static int32_t get_offset_of_extraTimeCost_7() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___extraTimeCost_7)); }
	inline int32_t get_extraTimeCost_7() const { return ___extraTimeCost_7; }
	inline int32_t* get_address_of_extraTimeCost_7() { return &___extraTimeCost_7; }
	inline void set_extraTimeCost_7(int32_t value)
	{
		___extraTimeCost_7 = value;
	}

	inline static int32_t get_offset_of_defaultTileScore_8() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___defaultTileScore_8)); }
	inline int32_t get_defaultTileScore_8() const { return ___defaultTileScore_8; }
	inline int32_t* get_address_of_defaultTileScore_8() { return &___defaultTileScore_8; }
	inline void set_defaultTileScore_8(int32_t value)
	{
		___defaultTileScore_8 = value;
	}

	inline static int32_t get_offset_of_tileScoreOverrides_9() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___tileScoreOverrides_9)); }
	inline List_1_t1104741157 * get_tileScoreOverrides_9() const { return ___tileScoreOverrides_9; }
	inline List_1_t1104741157 ** get_address_of_tileScoreOverrides_9() { return &___tileScoreOverrides_9; }
	inline void set_tileScoreOverrides_9(List_1_t1104741157 * value)
	{
		___tileScoreOverrides_9 = value;
		Il2CppCodeGenWriteBarrier((&___tileScoreOverrides_9), value);
	}

	inline static int32_t get_offset_of_boosterNeededMatches_10() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___boosterNeededMatches_10)); }
	inline Dictionary_2_t265350959 * get_boosterNeededMatches_10() const { return ___boosterNeededMatches_10; }
	inline Dictionary_2_t265350959 ** get_address_of_boosterNeededMatches_10() { return &___boosterNeededMatches_10; }
	inline void set_boosterNeededMatches_10(Dictionary_2_t265350959 * value)
	{
		___boosterNeededMatches_10 = value;
		Il2CppCodeGenWriteBarrier((&___boosterNeededMatches_10), value);
	}

	inline static int32_t get_offset_of_ingameBoosterAmount_11() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___ingameBoosterAmount_11)); }
	inline Dictionary_2_t265350959 * get_ingameBoosterAmount_11() const { return ___ingameBoosterAmount_11; }
	inline Dictionary_2_t265350959 ** get_address_of_ingameBoosterAmount_11() { return &___ingameBoosterAmount_11; }
	inline void set_ingameBoosterAmount_11(Dictionary_2_t265350959 * value)
	{
		___ingameBoosterAmount_11 = value;
		Il2CppCodeGenWriteBarrier((&___ingameBoosterAmount_11), value);
	}

	inline static int32_t get_offset_of_ingameBoosterCost_12() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___ingameBoosterCost_12)); }
	inline Dictionary_2_t265350959 * get_ingameBoosterCost_12() const { return ___ingameBoosterCost_12; }
	inline Dictionary_2_t265350959 ** get_address_of_ingameBoosterCost_12() { return &___ingameBoosterCost_12; }
	inline void set_ingameBoosterCost_12(Dictionary_2_t265350959 * value)
	{
		___ingameBoosterCost_12 = value;
		Il2CppCodeGenWriteBarrier((&___ingameBoosterCost_12), value);
	}

	inline static int32_t get_offset_of_rewardedAdCoins_13() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___rewardedAdCoins_13)); }
	inline int32_t get_rewardedAdCoins_13() const { return ___rewardedAdCoins_13; }
	inline int32_t* get_address_of_rewardedAdCoins_13() { return &___rewardedAdCoins_13; }
	inline void set_rewardedAdCoins_13(int32_t value)
	{
		___rewardedAdCoins_13 = value;
	}

	inline static int32_t get_offset_of_iapItems_14() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698, ___iapItems_14)); }
	inline List_1_t625377672 * get_iapItems_14() const { return ___iapItems_14; }
	inline List_1_t625377672 ** get_address_of_iapItems_14() { return &___iapItems_14; }
	inline void set_iapItems_14(List_1_t625377672 * value)
	{
		___iapItems_14 = value;
		Il2CppCodeGenWriteBarrier((&___iapItems_14), value);
	}
};

struct GameConfiguration_t2964563698_StaticFields
{
public:
	// System.Predicate`1<GameVanilla.Game.Common.TileScoreOverride> GameVanilla.Game.Common.GameConfiguration::<>f__am$cache0
	Predicate_1_t457960539 * ___U3CU3Ef__amU24cache0_15;
	// System.Predicate`1<GameVanilla.Game.Common.TileScoreOverride> GameVanilla.Game.Common.GameConfiguration::<>f__am$cache1
	Predicate_1_t457960539 * ___U3CU3Ef__amU24cache1_16;
	// System.Predicate`1<GameVanilla.Game.Common.TileScoreOverride> GameVanilla.Game.Common.GameConfiguration::<>f__am$cache2
	Predicate_1_t457960539 * ___U3CU3Ef__amU24cache2_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Predicate_1_t457960539 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Predicate_1_t457960539 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Predicate_1_t457960539 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_16() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698_StaticFields, ___U3CU3Ef__amU24cache1_16)); }
	inline Predicate_1_t457960539 * get_U3CU3Ef__amU24cache1_16() const { return ___U3CU3Ef__amU24cache1_16; }
	inline Predicate_1_t457960539 ** get_address_of_U3CU3Ef__amU24cache1_16() { return &___U3CU3Ef__amU24cache1_16; }
	inline void set_U3CU3Ef__amU24cache1_16(Predicate_1_t457960539 * value)
	{
		___U3CU3Ef__amU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_17() { return static_cast<int32_t>(offsetof(GameConfiguration_t2964563698_StaticFields, ___U3CU3Ef__amU24cache2_17)); }
	inline Predicate_1_t457960539 * get_U3CU3Ef__amU24cache2_17() const { return ___U3CU3Ef__amU24cache2_17; }
	inline Predicate_1_t457960539 ** get_address_of_U3CU3Ef__amU24cache2_17() { return &___U3CU3Ef__amU24cache2_17; }
	inline void set_U3CU3Ef__amU24cache2_17(Predicate_1_t457960539 * value)
	{
		___U3CU3Ef__amU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONFIGURATION_T2964563698_H
#ifndef U3CCREATEBOOSTERU3EC__ANONSTOREY19_T596934901_H
#define U3CCREATEBOOSTERU3EC__ANONSTOREY19_T596934901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<CreateBooster>c__AnonStorey19
struct  U3CCreateBoosterU3Ec__AnonStorey19_t596934901  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<CreateBooster>c__AnonStorey19::max
	int32_t ___max_0;

public:
	inline static int32_t get_offset_of_max_0() { return static_cast<int32_t>(offsetof(U3CCreateBoosterU3Ec__AnonStorey19_t596934901, ___max_0)); }
	inline int32_t get_max_0() const { return ___max_0; }
	inline int32_t* get_address_of_max_0() { return &___max_0; }
	inline void set_max_0(int32_t value)
	{
		___max_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEBOOSTERU3EC__ANONSTOREY19_T596934901_H
#ifndef U3CAPPLYGRAVITYASYNCU3EC__ITERATOR5_T2481046416_H
#define U3CAPPLYGRAVITYASYNCU3EC__ITERATOR5_T2481046416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ApplyGravityAsync>c__Iterator5
struct  U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416  : public RuntimeObject
{
public:
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<ApplyGravityAsync>c__Iterator5::$this
	GameScene_t3472316885 * ___U24this_0;
	// System.Object GameVanilla.Game.Scenes.GameScene/<ApplyGravityAsync>c__Iterator5::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<ApplyGravityAsync>c__Iterator5::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ApplyGravityAsync>c__Iterator5::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416, ___U24this_0)); }
	inline GameScene_t3472316885 * get_U24this_0() const { return ___U24this_0; }
	inline GameScene_t3472316885 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameScene_t3472316885 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPLYGRAVITYASYNCU3EC__ITERATOR5_T2481046416_H
#ifndef U3CAPPLYGRAVITYU3EC__ANONSTOREY1A_T1847438783_H
#define U3CAPPLYGRAVITYU3EC__ANONSTOREY1A_T1847438783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ApplyGravity>c__AnonStorey1A
struct  U3CApplyGravityU3Ec__AnonStorey1A_t1847438783  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ApplyGravity>c__AnonStorey1A::tile
	GameObject_t1113636619 * ___tile_0;

public:
	inline static int32_t get_offset_of_tile_0() { return static_cast<int32_t>(offsetof(U3CApplyGravityU3Ec__AnonStorey1A_t1847438783, ___tile_0)); }
	inline GameObject_t1113636619 * get_tile_0() const { return ___tile_0; }
	inline GameObject_t1113636619 ** get_address_of_tile_0() { return &___tile_0; }
	inline void set_tile_0(GameObject_t1113636619 * value)
	{
		___tile_0 = value;
		Il2CppCodeGenWriteBarrier((&___tile_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPLYGRAVITYU3EC__ANONSTOREY1A_T1847438783_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUMERATOR_T179987942_H
#define ENUMERATOR_T179987942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct  Enumerator_t179987942 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2585711361 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_t1113636619 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___l_0)); }
	inline List_1_t2585711361 * get_l_0() const { return ___l_0; }
	inline List_1_t2585711361 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2585711361 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___current_3)); }
	inline GameObject_t1113636619 * get_current_3() const { return ___current_3; }
	inline GameObject_t1113636619 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_t1113636619 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T179987942_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef REACHSCOREGOAL_T1810434159_H
#define REACHSCOREGOAL_T1810434159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.ReachScoreGoal
struct  ReachScoreGoal_t1810434159  : public Goal_t1499691635
{
public:
	// System.Int32 GameVanilla.Game.Common.ReachScoreGoal::score
	int32_t ___score_0;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(ReachScoreGoal_t1810434159, ___score_0)); }
	inline int32_t get_score_0() const { return ___score_0; }
	inline int32_t* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(int32_t value)
	{
		___score_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACHSCOREGOAL_T1810434159_H
#ifndef TILEDEF_T1926324708_H
#define TILEDEF_T1926324708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.TileDef
struct  TileDef_t1926324708 
{
public:
	// System.Int32 GameVanilla.Game.Common.TileDef::x
	int32_t ___x_0;
	// System.Int32 GameVanilla.Game.Common.TileDef::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(TileDef_t1926324708, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(TileDef_t1926324708, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEDEF_T1926324708_H
#ifndef COLORBLOCKTYPE_T3906382570_H
#define COLORBLOCKTYPE_T3906382570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.ColorBlockType
struct  ColorBlockType_t3906382570 
{
public:
	// System.Int32 GameVanilla.Game.Common.ColorBlockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorBlockType_t3906382570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCKTYPE_T3906382570_H
#ifndef U3CAWARDBOOSTERSU3EC__ITERATOR7_T3395044180_H
#define U3CAWARDBOOSTERSU3EC__ITERATOR7_T3395044180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7
struct  U3CAwardBoostersU3Ec__Iterator7_t3395044180  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::<randomIdx>__1
	int32_t ___U3CrandomIdxU3E__1_0;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::<tile>__2
	GameObject_t1113636619 * ___U3CtileU3E__2_1;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::$locvar0
	Enumerator_t179987942  ___U24locvar0_2;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::<block>__3
	GameObject_t1113636619 * ___U3CblockU3E__3_3;
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::$this
	GameScene_t3472316885 * ___U24this_4;
	// System.Object GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::$disposing
	bool ___U24disposing_6;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<AwardBoosters>c__Iterator7::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CrandomIdxU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U3CrandomIdxU3E__1_0)); }
	inline int32_t get_U3CrandomIdxU3E__1_0() const { return ___U3CrandomIdxU3E__1_0; }
	inline int32_t* get_address_of_U3CrandomIdxU3E__1_0() { return &___U3CrandomIdxU3E__1_0; }
	inline void set_U3CrandomIdxU3E__1_0(int32_t value)
	{
		___U3CrandomIdxU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CtileU3E__2_1() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U3CtileU3E__2_1)); }
	inline GameObject_t1113636619 * get_U3CtileU3E__2_1() const { return ___U3CtileU3E__2_1; }
	inline GameObject_t1113636619 ** get_address_of_U3CtileU3E__2_1() { return &___U3CtileU3E__2_1; }
	inline void set_U3CtileU3E__2_1(GameObject_t1113636619 * value)
	{
		___U3CtileU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtileU3E__2_1), value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U24locvar0_2)); }
	inline Enumerator_t179987942  get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline Enumerator_t179987942 * get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(Enumerator_t179987942  value)
	{
		___U24locvar0_2 = value;
	}

	inline static int32_t get_offset_of_U3CblockU3E__3_3() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U3CblockU3E__3_3)); }
	inline GameObject_t1113636619 * get_U3CblockU3E__3_3() const { return ___U3CblockU3E__3_3; }
	inline GameObject_t1113636619 ** get_address_of_U3CblockU3E__3_3() { return &___U3CblockU3E__3_3; }
	inline void set_U3CblockU3E__3_3(GameObject_t1113636619 * value)
	{
		___U3CblockU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CblockU3E__3_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U24this_4)); }
	inline GameScene_t3472316885 * get_U24this_4() const { return ___U24this_4; }
	inline GameScene_t3472316885 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GameScene_t3472316885 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CAwardBoostersU3Ec__Iterator7_t3395044180, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAWARDBOOSTERSU3EC__ITERATOR7_T3395044180_H
#ifndef U3CRUNFADEU3EC__ITERATOR0_T3294870132_H
#define U3CRUNFADEU3EC__ITERATOR0_T3294870132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.Transition/<RunFade>c__Iterator0
struct  U3CRunFadeU3Ec__Iterator0_t3294870132  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D GameVanilla.Core.Transition/<RunFade>c__Iterator0::<bgTex>__0
	Texture2D_t3840446185 * ___U3CbgTexU3E__0_0;
	// UnityEngine.Color GameVanilla.Core.Transition/<RunFade>c__Iterator0::fadeColor
	Color_t2555686324  ___fadeColor_1;
	// UnityEngine.UI.Image GameVanilla.Core.Transition/<RunFade>c__Iterator0::<image>__0
	Image_t2670269651 * ___U3CimageU3E__0_2;
	// UnityEngine.Rect GameVanilla.Core.Transition/<RunFade>c__Iterator0::<rect>__0
	Rect_t2360479859  ___U3CrectU3E__0_3;
	// UnityEngine.Sprite GameVanilla.Core.Transition/<RunFade>c__Iterator0::<sprite>__0
	Sprite_t280657092 * ___U3CspriteU3E__0_4;
	// UnityEngine.Color GameVanilla.Core.Transition/<RunFade>c__Iterator0::<newColor>__0
	Color_t2555686324  ___U3CnewColorU3E__0_5;
	// System.Single GameVanilla.Core.Transition/<RunFade>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_6;
	// System.Single GameVanilla.Core.Transition/<RunFade>c__Iterator0::duration
	float ___duration_7;
	// System.Single GameVanilla.Core.Transition/<RunFade>c__Iterator0::<halfDuration>__0
	float ___U3ChalfDurationU3E__0_8;
	// System.String GameVanilla.Core.Transition/<RunFade>c__Iterator0::level
	String_t* ___level_9;
	// GameVanilla.Core.Transition GameVanilla.Core.Transition/<RunFade>c__Iterator0::$this
	Transition_t3232023259 * ___U24this_10;
	// System.Object GameVanilla.Core.Transition/<RunFade>c__Iterator0::$current
	RuntimeObject * ___U24current_11;
	// System.Boolean GameVanilla.Core.Transition/<RunFade>c__Iterator0::$disposing
	bool ___U24disposing_12;
	// System.Int32 GameVanilla.Core.Transition/<RunFade>c__Iterator0::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3CbgTexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3CbgTexU3E__0_0)); }
	inline Texture2D_t3840446185 * get_U3CbgTexU3E__0_0() const { return ___U3CbgTexU3E__0_0; }
	inline Texture2D_t3840446185 ** get_address_of_U3CbgTexU3E__0_0() { return &___U3CbgTexU3E__0_0; }
	inline void set_U3CbgTexU3E__0_0(Texture2D_t3840446185 * value)
	{
		___U3CbgTexU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbgTexU3E__0_0), value);
	}

	inline static int32_t get_offset_of_fadeColor_1() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___fadeColor_1)); }
	inline Color_t2555686324  get_fadeColor_1() const { return ___fadeColor_1; }
	inline Color_t2555686324 * get_address_of_fadeColor_1() { return &___fadeColor_1; }
	inline void set_fadeColor_1(Color_t2555686324  value)
	{
		___fadeColor_1 = value;
	}

	inline static int32_t get_offset_of_U3CimageU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3CimageU3E__0_2)); }
	inline Image_t2670269651 * get_U3CimageU3E__0_2() const { return ___U3CimageU3E__0_2; }
	inline Image_t2670269651 ** get_address_of_U3CimageU3E__0_2() { return &___U3CimageU3E__0_2; }
	inline void set_U3CimageU3E__0_2(Image_t2670269651 * value)
	{
		___U3CimageU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrectU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3CrectU3E__0_3)); }
	inline Rect_t2360479859  get_U3CrectU3E__0_3() const { return ___U3CrectU3E__0_3; }
	inline Rect_t2360479859 * get_address_of_U3CrectU3E__0_3() { return &___U3CrectU3E__0_3; }
	inline void set_U3CrectU3E__0_3(Rect_t2360479859  value)
	{
		___U3CrectU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CspriteU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3CspriteU3E__0_4)); }
	inline Sprite_t280657092 * get_U3CspriteU3E__0_4() const { return ___U3CspriteU3E__0_4; }
	inline Sprite_t280657092 ** get_address_of_U3CspriteU3E__0_4() { return &___U3CspriteU3E__0_4; }
	inline void set_U3CspriteU3E__0_4(Sprite_t280657092 * value)
	{
		___U3CspriteU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspriteU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3CnewColorU3E__0_5)); }
	inline Color_t2555686324  get_U3CnewColorU3E__0_5() const { return ___U3CnewColorU3E__0_5; }
	inline Color_t2555686324 * get_address_of_U3CnewColorU3E__0_5() { return &___U3CnewColorU3E__0_5; }
	inline void set_U3CnewColorU3E__0_5(Color_t2555686324  value)
	{
		___U3CnewColorU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CtimeU3E__0_6() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3CtimeU3E__0_6)); }
	inline float get_U3CtimeU3E__0_6() const { return ___U3CtimeU3E__0_6; }
	inline float* get_address_of_U3CtimeU3E__0_6() { return &___U3CtimeU3E__0_6; }
	inline void set_U3CtimeU3E__0_6(float value)
	{
		___U3CtimeU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___duration_7)); }
	inline float get_duration_7() const { return ___duration_7; }
	inline float* get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(float value)
	{
		___duration_7 = value;
	}

	inline static int32_t get_offset_of_U3ChalfDurationU3E__0_8() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U3ChalfDurationU3E__0_8)); }
	inline float get_U3ChalfDurationU3E__0_8() const { return ___U3ChalfDurationU3E__0_8; }
	inline float* get_address_of_U3ChalfDurationU3E__0_8() { return &___U3ChalfDurationU3E__0_8; }
	inline void set_U3ChalfDurationU3E__0_8(float value)
	{
		___U3ChalfDurationU3E__0_8 = value;
	}

	inline static int32_t get_offset_of_level_9() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___level_9)); }
	inline String_t* get_level_9() const { return ___level_9; }
	inline String_t** get_address_of_level_9() { return &___level_9; }
	inline void set_level_9(String_t* value)
	{
		___level_9 = value;
		Il2CppCodeGenWriteBarrier((&___level_9), value);
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U24this_10)); }
	inline Transition_t3232023259 * get_U24this_10() const { return ___U24this_10; }
	inline Transition_t3232023259 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(Transition_t3232023259 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_10), value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U24current_11)); }
	inline RuntimeObject * get_U24current_11() const { return ___U24current_11; }
	inline RuntimeObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(RuntimeObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_11), value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CRunFadeU3Ec__Iterator0_t3294870132, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNFADEU3EC__ITERATOR0_T3294870132_H
#ifndef COINICON_T1300102460_H
#define COINICON_T1300102460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.CoinIcon
struct  CoinIcon_t1300102460 
{
public:
	// System.Int32 GameVanilla.Game.Common.CoinIcon::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CoinIcon_t1300102460, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINICON_T1300102460_H
#ifndef LIMITTYPE_T3278153677_H
#define LIMITTYPE_T3278153677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.LimitType
struct  LimitType_t3278153677 
{
public:
	// System.Int32 GameVanilla.Game.Common.LimitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LimitType_t3278153677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIMITTYPE_T3278153677_H
#ifndef U3CCOLORBOMB_A_MATCHU3EC__ITERATOR3_T3856318673_H
#define U3CCOLORBOMB_A_MATCHU3EC__ITERATOR3_T3856318673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3
struct  U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::blocksToDestroy
	List_1_t2585711361 * ___blocksToDestroy_0;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$locvar0
	Enumerator_t179987942  ___U24locvar0_1;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::<idx>__2
	int32_t ___U3CidxU3E__2_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::aroundObject
	List_1_t2585711361 * ___aroundObject_3;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::<tile>__2
	GameObject_t1113636619 * ___U3CtileU3E__2_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::<x_List>__0
	List_1_t2585711361 * ___U3Cx_ListU3E__0_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::<y_List>__0
	List_1_t2585711361 * ___U3Cy_ListU3E__0_6;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$locvar2
	Enumerator_t179987942  ___U24locvar2_7;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::<index>__4
	int32_t ___U3CindexU3E__4_8;
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$this
	GameScene_t3472316885 * ___U24this_9;
	// System.Object GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$disposing
	bool ___U24disposing_11;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$PC
	int32_t ___U24PC_12;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey13 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$locvar3
	U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330 * ___U24locvar3_13;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3/<ColorBomb_A_Match>c__AnonStorey15 GameVanilla.Game.Scenes.GameScene/<ColorBomb_A_Match>c__Iterator3::$locvar5
	U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258 * ___U24locvar5_14;

public:
	inline static int32_t get_offset_of_blocksToDestroy_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___blocksToDestroy_0)); }
	inline List_1_t2585711361 * get_blocksToDestroy_0() const { return ___blocksToDestroy_0; }
	inline List_1_t2585711361 ** get_address_of_blocksToDestroy_0() { return &___blocksToDestroy_0; }
	inline void set_blocksToDestroy_0(List_1_t2585711361 * value)
	{
		___blocksToDestroy_0 = value;
		Il2CppCodeGenWriteBarrier((&___blocksToDestroy_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24locvar0_1)); }
	inline Enumerator_t179987942  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t179987942 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t179987942  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CidxU3E__2_2() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U3CidxU3E__2_2)); }
	inline int32_t get_U3CidxU3E__2_2() const { return ___U3CidxU3E__2_2; }
	inline int32_t* get_address_of_U3CidxU3E__2_2() { return &___U3CidxU3E__2_2; }
	inline void set_U3CidxU3E__2_2(int32_t value)
	{
		___U3CidxU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_aroundObject_3() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___aroundObject_3)); }
	inline List_1_t2585711361 * get_aroundObject_3() const { return ___aroundObject_3; }
	inline List_1_t2585711361 ** get_address_of_aroundObject_3() { return &___aroundObject_3; }
	inline void set_aroundObject_3(List_1_t2585711361 * value)
	{
		___aroundObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___aroundObject_3), value);
	}

	inline static int32_t get_offset_of_U3CtileU3E__2_4() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U3CtileU3E__2_4)); }
	inline GameObject_t1113636619 * get_U3CtileU3E__2_4() const { return ___U3CtileU3E__2_4; }
	inline GameObject_t1113636619 ** get_address_of_U3CtileU3E__2_4() { return &___U3CtileU3E__2_4; }
	inline void set_U3CtileU3E__2_4(GameObject_t1113636619 * value)
	{
		___U3CtileU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtileU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3Cx_ListU3E__0_5() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U3Cx_ListU3E__0_5)); }
	inline List_1_t2585711361 * get_U3Cx_ListU3E__0_5() const { return ___U3Cx_ListU3E__0_5; }
	inline List_1_t2585711361 ** get_address_of_U3Cx_ListU3E__0_5() { return &___U3Cx_ListU3E__0_5; }
	inline void set_U3Cx_ListU3E__0_5(List_1_t2585711361 * value)
	{
		___U3Cx_ListU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cx_ListU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3Cy_ListU3E__0_6() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U3Cy_ListU3E__0_6)); }
	inline List_1_t2585711361 * get_U3Cy_ListU3E__0_6() const { return ___U3Cy_ListU3E__0_6; }
	inline List_1_t2585711361 ** get_address_of_U3Cy_ListU3E__0_6() { return &___U3Cy_ListU3E__0_6; }
	inline void set_U3Cy_ListU3E__0_6(List_1_t2585711361 * value)
	{
		___U3Cy_ListU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cy_ListU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U24locvar2_7() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24locvar2_7)); }
	inline Enumerator_t179987942  get_U24locvar2_7() const { return ___U24locvar2_7; }
	inline Enumerator_t179987942 * get_address_of_U24locvar2_7() { return &___U24locvar2_7; }
	inline void set_U24locvar2_7(Enumerator_t179987942  value)
	{
		___U24locvar2_7 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__4_8() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U3CindexU3E__4_8)); }
	inline int32_t get_U3CindexU3E__4_8() const { return ___U3CindexU3E__4_8; }
	inline int32_t* get_address_of_U3CindexU3E__4_8() { return &___U3CindexU3E__4_8; }
	inline void set_U3CindexU3E__4_8(int32_t value)
	{
		___U3CindexU3E__4_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24this_9)); }
	inline GameScene_t3472316885 * get_U24this_9() const { return ___U24this_9; }
	inline GameScene_t3472316885 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(GameScene_t3472316885 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24locvar3_13() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24locvar3_13)); }
	inline U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330 * get_U24locvar3_13() const { return ___U24locvar3_13; }
	inline U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330 ** get_address_of_U24locvar3_13() { return &___U24locvar3_13; }
	inline void set_U24locvar3_13(U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330 * value)
	{
		___U24locvar3_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar3_13), value);
	}

	inline static int32_t get_offset_of_U24locvar5_14() { return static_cast<int32_t>(offsetof(U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673, ___U24locvar5_14)); }
	inline U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258 * get_U24locvar5_14() const { return ___U24locvar5_14; }
	inline U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258 ** get_address_of_U24locvar5_14() { return &___U24locvar5_14; }
	inline void set_U24locvar5_14(U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258 * value)
	{
		___U24locvar5_14 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar5_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_A_MATCHU3EC__ITERATOR3_T3856318673_H
#ifndef U3CCOLORBOMB_B_MATCHU3EC__ITERATOR2_T3457397951_H
#define U3CCOLORBOMB_B_MATCHU3EC__ITERATOR2_T3457397951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2
struct  U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::blocksToDestroy
	List_1_t2585711361 * ___blocksToDestroy_0;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$locvar0
	Enumerator_t179987942  ___U24locvar0_1;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::<idx>__2
	int32_t ___U3CidxU3E__2_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::aroundObject
	List_1_t2585711361 * ___aroundObject_3;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::<tile>__2
	GameObject_t1113636619 * ___U3CtileU3E__2_4;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$locvar2
	Enumerator_t179987942  ___U24locvar2_5;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::<index>__4
	int32_t ___U3CindexU3E__4_6;
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$this
	GameScene_t3472316885 * ___U24this_7;
	// System.Object GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$disposing
	bool ___U24disposing_9;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$PC
	int32_t ___U24PC_10;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey10 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$locvar3
	U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928 * ___U24locvar3_11;
	// GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2/<ColorBomb_B_Match>c__AnonStorey12 GameVanilla.Game.Scenes.GameScene/<ColorBomb_B_Match>c__Iterator2::$locvar5
	U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952 * ___U24locvar5_12;

public:
	inline static int32_t get_offset_of_blocksToDestroy_0() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___blocksToDestroy_0)); }
	inline List_1_t2585711361 * get_blocksToDestroy_0() const { return ___blocksToDestroy_0; }
	inline List_1_t2585711361 ** get_address_of_blocksToDestroy_0() { return &___blocksToDestroy_0; }
	inline void set_blocksToDestroy_0(List_1_t2585711361 * value)
	{
		___blocksToDestroy_0 = value;
		Il2CppCodeGenWriteBarrier((&___blocksToDestroy_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24locvar0_1)); }
	inline Enumerator_t179987942  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t179987942 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t179987942  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CidxU3E__2_2() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U3CidxU3E__2_2)); }
	inline int32_t get_U3CidxU3E__2_2() const { return ___U3CidxU3E__2_2; }
	inline int32_t* get_address_of_U3CidxU3E__2_2() { return &___U3CidxU3E__2_2; }
	inline void set_U3CidxU3E__2_2(int32_t value)
	{
		___U3CidxU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_aroundObject_3() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___aroundObject_3)); }
	inline List_1_t2585711361 * get_aroundObject_3() const { return ___aroundObject_3; }
	inline List_1_t2585711361 ** get_address_of_aroundObject_3() { return &___aroundObject_3; }
	inline void set_aroundObject_3(List_1_t2585711361 * value)
	{
		___aroundObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___aroundObject_3), value);
	}

	inline static int32_t get_offset_of_U3CtileU3E__2_4() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U3CtileU3E__2_4)); }
	inline GameObject_t1113636619 * get_U3CtileU3E__2_4() const { return ___U3CtileU3E__2_4; }
	inline GameObject_t1113636619 ** get_address_of_U3CtileU3E__2_4() { return &___U3CtileU3E__2_4; }
	inline void set_U3CtileU3E__2_4(GameObject_t1113636619 * value)
	{
		___U3CtileU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtileU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U24locvar2_5() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24locvar2_5)); }
	inline Enumerator_t179987942  get_U24locvar2_5() const { return ___U24locvar2_5; }
	inline Enumerator_t179987942 * get_address_of_U24locvar2_5() { return &___U24locvar2_5; }
	inline void set_U24locvar2_5(Enumerator_t179987942  value)
	{
		___U24locvar2_5 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__4_6() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U3CindexU3E__4_6)); }
	inline int32_t get_U3CindexU3E__4_6() const { return ___U3CindexU3E__4_6; }
	inline int32_t* get_address_of_U3CindexU3E__4_6() { return &___U3CindexU3E__4_6; }
	inline void set_U3CindexU3E__4_6(int32_t value)
	{
		___U3CindexU3E__4_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24this_7)); }
	inline GameScene_t3472316885 * get_U24this_7() const { return ___U24this_7; }
	inline GameScene_t3472316885 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(GameScene_t3472316885 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24locvar3_11() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24locvar3_11)); }
	inline U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928 * get_U24locvar3_11() const { return ___U24locvar3_11; }
	inline U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928 ** get_address_of_U24locvar3_11() { return &___U24locvar3_11; }
	inline void set_U24locvar3_11(U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928 * value)
	{
		___U24locvar3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar3_11), value);
	}

	inline static int32_t get_offset_of_U24locvar5_12() { return static_cast<int32_t>(offsetof(U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951, ___U24locvar5_12)); }
	inline U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952 * get_U24locvar5_12() const { return ___U24locvar5_12; }
	inline U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952 ** get_address_of_U24locvar5_12() { return &___U24locvar5_12; }
	inline void set_U24locvar5_12(U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952 * value)
	{
		___U24locvar5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar5_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLORBOMB_B_MATCHU3EC__ITERATOR2_T3457397951_H
#ifndef MATCHTYPE_T2040227041_H
#define MATCHTYPE_T2040227041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.MatchType
struct  MatchType_t2040227041 
{
public:
	// System.Int32 GameVanilla.Game.Common.MatchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MatchType_t2040227041, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTYPE_T2040227041_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef BLOCKERTYPE_T1933664911_H
#define BLOCKERTYPE_T1933664911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BlockerType
struct  BlockerType_t1933664911 
{
public:
	// System.Int32 GameVanilla.Game.Common.BlockerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockerType_t1933664911, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKERTYPE_T1933664911_H
#ifndef BLOCKTYPE_T4096884630_H
#define BLOCKTYPE_T4096884630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BlockType
struct  BlockType_t4096884630 
{
public:
	// System.Int32 GameVanilla.Game.Common.BlockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockType_t4096884630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKTYPE_T4096884630_H
#ifndef DIRECTION_T250950753_H
#define DIRECTION_T250950753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Bomb/Direction
struct  Direction_t250950753 
{
public:
	// System.Int32 GameVanilla.Game.Common.Bomb/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t250950753, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T250950753_H
#ifndef COMBOTYPE_T4102589120_H
#define COMBOTYPE_T4102589120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Bomb/ComboType
struct  ComboType_t4102589120 
{
public:
	// System.Int32 GameVanilla.Game.Common.Bomb/ComboType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ComboType_t4102589120, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBOTYPE_T4102589120_H
#ifndef BOOSTERTYPE_T3880018546_H
#define BOOSTERTYPE_T3880018546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BoosterType
struct  BoosterType_t3880018546 
{
public:
	// System.Int32 GameVanilla.Game.Common.BoosterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BoosterType_t3880018546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOSTERTYPE_T3880018546_H
#ifndef U3CUPDATEU3EC__ANONSTOREYA_T3216144777_H
#define U3CUPDATEU3EC__ANONSTOREYA_T3216144777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<Update>c__AnonStoreyA
struct  U3CUpdateU3Ec__AnonStoreyA_t3216144777  : public RuntimeObject
{
public:
	// UnityEngine.RaycastHit2D GameVanilla.Game.Scenes.GameScene/<Update>c__AnonStoreyA::hit
	RaycastHit2D_t2279581989  ___hit_0;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStoreyA_t3216144777, ___hit_0)); }
	inline RaycastHit2D_t2279581989  get_hit_0() const { return ___hit_0; }
	inline RaycastHit2D_t2279581989 * get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(RaycastHit2D_t2279581989  value)
	{
		___hit_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEU3EC__ANONSTOREYA_T3216144777_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef LEVELTILE_T145158312_H
#define LEVELTILE_T145158312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.LevelTile
struct  LevelTile_t145158312  : public RuntimeObject
{
public:
	// GameVanilla.Game.Common.BlockerType GameVanilla.Game.Common.LevelTile::blockerType
	int32_t ___blockerType_0;

public:
	inline static int32_t get_offset_of_blockerType_0() { return static_cast<int32_t>(offsetof(LevelTile_t145158312, ___blockerType_0)); }
	inline int32_t get_blockerType_0() const { return ___blockerType_0; }
	inline int32_t* get_address_of_blockerType_0() { return &___blockerType_0; }
	inline void set_blockerType_0(int32_t value)
	{
		___blockerType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELTILE_T145158312_H
#ifndef LEVEL_T3869101417_H
#define LEVEL_T3869101417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Level
struct  Level_t3869101417  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.Common.Level::id
	int32_t ___id_0;
	// System.Int32 GameVanilla.Game.Common.Level::width
	int32_t ___width_1;
	// System.Int32 GameVanilla.Game.Common.Level::height
	int32_t ___height_2;
	// System.Collections.Generic.List`1<GameVanilla.Game.Common.LevelTile> GameVanilla.Game.Common.Level::tiles
	List_1_t1617233054 * ___tiles_3;
	// GameVanilla.Game.Common.LimitType GameVanilla.Game.Common.Level::limitType
	int32_t ___limitType_4;
	// System.Int32 GameVanilla.Game.Common.Level::limit
	int32_t ___limit_5;
	// System.Int32 GameVanilla.Game.Common.Level::penalty
	int32_t ___penalty_6;
	// System.Collections.Generic.List`1<GameVanilla.Game.Common.Goal> GameVanilla.Game.Common.Level::goals
	List_1_t2971766377 * ___goals_7;
	// System.Collections.Generic.List`1<GameVanilla.Game.Common.ColorBlockType> GameVanilla.Game.Common.Level::availableColors
	List_1_t1083490016 * ___availableColors_8;
	// System.Int32 GameVanilla.Game.Common.Level::score1
	int32_t ___score1_9;
	// System.Int32 GameVanilla.Game.Common.Level::score2
	int32_t ___score2_10;
	// System.Int32 GameVanilla.Game.Common.Level::score3
	int32_t ___score3_11;
	// System.Boolean GameVanilla.Game.Common.Level::awardBoostersWithRemainingMoves
	bool ___awardBoostersWithRemainingMoves_12;
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.Common.Level::awardedBoosterType
	int32_t ___awardedBoosterType_13;
	// System.Int32 GameVanilla.Game.Common.Level::collectableChance
	int32_t ___collectableChance_14;
	// System.Collections.Generic.Dictionary`2<GameVanilla.Game.Common.BoosterType,System.Boolean> GameVanilla.Game.Common.Level::availableBoosters
	Dictionary_2_t1706660467 * ___availableBoosters_15;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_tiles_3() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___tiles_3)); }
	inline List_1_t1617233054 * get_tiles_3() const { return ___tiles_3; }
	inline List_1_t1617233054 ** get_address_of_tiles_3() { return &___tiles_3; }
	inline void set_tiles_3(List_1_t1617233054 * value)
	{
		___tiles_3 = value;
		Il2CppCodeGenWriteBarrier((&___tiles_3), value);
	}

	inline static int32_t get_offset_of_limitType_4() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___limitType_4)); }
	inline int32_t get_limitType_4() const { return ___limitType_4; }
	inline int32_t* get_address_of_limitType_4() { return &___limitType_4; }
	inline void set_limitType_4(int32_t value)
	{
		___limitType_4 = value;
	}

	inline static int32_t get_offset_of_limit_5() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___limit_5)); }
	inline int32_t get_limit_5() const { return ___limit_5; }
	inline int32_t* get_address_of_limit_5() { return &___limit_5; }
	inline void set_limit_5(int32_t value)
	{
		___limit_5 = value;
	}

	inline static int32_t get_offset_of_penalty_6() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___penalty_6)); }
	inline int32_t get_penalty_6() const { return ___penalty_6; }
	inline int32_t* get_address_of_penalty_6() { return &___penalty_6; }
	inline void set_penalty_6(int32_t value)
	{
		___penalty_6 = value;
	}

	inline static int32_t get_offset_of_goals_7() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___goals_7)); }
	inline List_1_t2971766377 * get_goals_7() const { return ___goals_7; }
	inline List_1_t2971766377 ** get_address_of_goals_7() { return &___goals_7; }
	inline void set_goals_7(List_1_t2971766377 * value)
	{
		___goals_7 = value;
		Il2CppCodeGenWriteBarrier((&___goals_7), value);
	}

	inline static int32_t get_offset_of_availableColors_8() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___availableColors_8)); }
	inline List_1_t1083490016 * get_availableColors_8() const { return ___availableColors_8; }
	inline List_1_t1083490016 ** get_address_of_availableColors_8() { return &___availableColors_8; }
	inline void set_availableColors_8(List_1_t1083490016 * value)
	{
		___availableColors_8 = value;
		Il2CppCodeGenWriteBarrier((&___availableColors_8), value);
	}

	inline static int32_t get_offset_of_score1_9() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___score1_9)); }
	inline int32_t get_score1_9() const { return ___score1_9; }
	inline int32_t* get_address_of_score1_9() { return &___score1_9; }
	inline void set_score1_9(int32_t value)
	{
		___score1_9 = value;
	}

	inline static int32_t get_offset_of_score2_10() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___score2_10)); }
	inline int32_t get_score2_10() const { return ___score2_10; }
	inline int32_t* get_address_of_score2_10() { return &___score2_10; }
	inline void set_score2_10(int32_t value)
	{
		___score2_10 = value;
	}

	inline static int32_t get_offset_of_score3_11() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___score3_11)); }
	inline int32_t get_score3_11() const { return ___score3_11; }
	inline int32_t* get_address_of_score3_11() { return &___score3_11; }
	inline void set_score3_11(int32_t value)
	{
		___score3_11 = value;
	}

	inline static int32_t get_offset_of_awardBoostersWithRemainingMoves_12() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___awardBoostersWithRemainingMoves_12)); }
	inline bool get_awardBoostersWithRemainingMoves_12() const { return ___awardBoostersWithRemainingMoves_12; }
	inline bool* get_address_of_awardBoostersWithRemainingMoves_12() { return &___awardBoostersWithRemainingMoves_12; }
	inline void set_awardBoostersWithRemainingMoves_12(bool value)
	{
		___awardBoostersWithRemainingMoves_12 = value;
	}

	inline static int32_t get_offset_of_awardedBoosterType_13() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___awardedBoosterType_13)); }
	inline int32_t get_awardedBoosterType_13() const { return ___awardedBoosterType_13; }
	inline int32_t* get_address_of_awardedBoosterType_13() { return &___awardedBoosterType_13; }
	inline void set_awardedBoosterType_13(int32_t value)
	{
		___awardedBoosterType_13 = value;
	}

	inline static int32_t get_offset_of_collectableChance_14() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___collectableChance_14)); }
	inline int32_t get_collectableChance_14() const { return ___collectableChance_14; }
	inline int32_t* get_address_of_collectableChance_14() { return &___collectableChance_14; }
	inline void set_collectableChance_14(int32_t value)
	{
		___collectableChance_14 = value;
	}

	inline static int32_t get_offset_of_availableBoosters_15() { return static_cast<int32_t>(offsetof(Level_t3869101417, ___availableBoosters_15)); }
	inline Dictionary_2_t1706660467 * get_availableBoosters_15() const { return ___availableBoosters_15; }
	inline Dictionary_2_t1706660467 ** get_address_of_availableBoosters_15() { return &___availableBoosters_15; }
	inline void set_availableBoosters_15(Dictionary_2_t1706660467 * value)
	{
		___availableBoosters_15 = value;
		Il2CppCodeGenWriteBarrier((&___availableBoosters_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T3869101417_H
#ifndef BOOSTERSCORE_T1409186799_H
#define BOOSTERSCORE_T1409186799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BoosterScore
struct  BoosterScore_t1409186799  : public TileScoreOverride_t3927633711
{
public:
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.Common.BoosterScore::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(BoosterScore_t1409186799, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOSTERSCORE_T1409186799_H
#ifndef COLLECTBLOCKGOAL_T1015685037_H
#define COLLECTBLOCKGOAL_T1015685037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.CollectBlockGoal
struct  CollectBlockGoal_t1015685037  : public Goal_t1499691635
{
public:
	// GameVanilla.Game.Common.BlockType GameVanilla.Game.Common.CollectBlockGoal::blockType
	int32_t ___blockType_0;
	// System.Int32 GameVanilla.Game.Common.CollectBlockGoal::amount
	int32_t ___amount_1;

public:
	inline static int32_t get_offset_of_blockType_0() { return static_cast<int32_t>(offsetof(CollectBlockGoal_t1015685037, ___blockType_0)); }
	inline int32_t get_blockType_0() const { return ___blockType_0; }
	inline int32_t* get_address_of_blockType_0() { return &___blockType_0; }
	inline void set_blockType_0(int32_t value)
	{
		___blockType_0 = value;
	}

	inline static int32_t get_offset_of_amount_1() { return static_cast<int32_t>(offsetof(CollectBlockGoal_t1015685037, ___amount_1)); }
	inline int32_t get_amount_1() const { return ___amount_1; }
	inline int32_t* get_address_of_amount_1() { return &___amount_1; }
	inline void set_amount_1(int32_t value)
	{
		___amount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTBLOCKGOAL_T1015685037_H
#ifndef IAPITEM_T3448270226_H
#define IAPITEM_T3448270226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.IapItem
struct  IapItem_t3448270226  : public RuntimeObject
{
public:
	// System.String GameVanilla.Game.Common.IapItem::storeId
	String_t* ___storeId_0;
	// System.Int32 GameVanilla.Game.Common.IapItem::numCoins
	int32_t ___numCoins_1;
	// System.Int32 GameVanilla.Game.Common.IapItem::discount
	int32_t ___discount_2;
	// System.Boolean GameVanilla.Game.Common.IapItem::mostPopular
	bool ___mostPopular_3;
	// System.Boolean GameVanilla.Game.Common.IapItem::bestValue
	bool ___bestValue_4;
	// GameVanilla.Game.Common.CoinIcon GameVanilla.Game.Common.IapItem::coinIcon
	int32_t ___coinIcon_5;

public:
	inline static int32_t get_offset_of_storeId_0() { return static_cast<int32_t>(offsetof(IapItem_t3448270226, ___storeId_0)); }
	inline String_t* get_storeId_0() const { return ___storeId_0; }
	inline String_t** get_address_of_storeId_0() { return &___storeId_0; }
	inline void set_storeId_0(String_t* value)
	{
		___storeId_0 = value;
		Il2CppCodeGenWriteBarrier((&___storeId_0), value);
	}

	inline static int32_t get_offset_of_numCoins_1() { return static_cast<int32_t>(offsetof(IapItem_t3448270226, ___numCoins_1)); }
	inline int32_t get_numCoins_1() const { return ___numCoins_1; }
	inline int32_t* get_address_of_numCoins_1() { return &___numCoins_1; }
	inline void set_numCoins_1(int32_t value)
	{
		___numCoins_1 = value;
	}

	inline static int32_t get_offset_of_discount_2() { return static_cast<int32_t>(offsetof(IapItem_t3448270226, ___discount_2)); }
	inline int32_t get_discount_2() const { return ___discount_2; }
	inline int32_t* get_address_of_discount_2() { return &___discount_2; }
	inline void set_discount_2(int32_t value)
	{
		___discount_2 = value;
	}

	inline static int32_t get_offset_of_mostPopular_3() { return static_cast<int32_t>(offsetof(IapItem_t3448270226, ___mostPopular_3)); }
	inline bool get_mostPopular_3() const { return ___mostPopular_3; }
	inline bool* get_address_of_mostPopular_3() { return &___mostPopular_3; }
	inline void set_mostPopular_3(bool value)
	{
		___mostPopular_3 = value;
	}

	inline static int32_t get_offset_of_bestValue_4() { return static_cast<int32_t>(offsetof(IapItem_t3448270226, ___bestValue_4)); }
	inline bool get_bestValue_4() const { return ___bestValue_4; }
	inline bool* get_address_of_bestValue_4() { return &___bestValue_4; }
	inline void set_bestValue_4(bool value)
	{
		___bestValue_4 = value;
	}

	inline static int32_t get_offset_of_coinIcon_5() { return static_cast<int32_t>(offsetof(IapItem_t3448270226, ___coinIcon_5)); }
	inline int32_t get_coinIcon_5() const { return ___coinIcon_5; }
	inline int32_t* get_address_of_coinIcon_5() { return &___coinIcon_5; }
	inline void set_coinIcon_5(int32_t value)
	{
		___coinIcon_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPITEM_T3448270226_H
#ifndef COLLECTBLOCKERGOAL_T1534435222_H
#define COLLECTBLOCKERGOAL_T1534435222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.CollectBlockerGoal
struct  CollectBlockerGoal_t1534435222  : public Goal_t1499691635
{
public:
	// GameVanilla.Game.Common.BlockerType GameVanilla.Game.Common.CollectBlockerGoal::blockerType
	int32_t ___blockerType_0;
	// System.Int32 GameVanilla.Game.Common.CollectBlockerGoal::amount
	int32_t ___amount_1;

public:
	inline static int32_t get_offset_of_blockerType_0() { return static_cast<int32_t>(offsetof(CollectBlockerGoal_t1534435222, ___blockerType_0)); }
	inline int32_t get_blockerType_0() const { return ___blockerType_0; }
	inline int32_t* get_address_of_blockerType_0() { return &___blockerType_0; }
	inline void set_blockerType_0(int32_t value)
	{
		___blockerType_0 = value;
	}

	inline static int32_t get_offset_of_amount_1() { return static_cast<int32_t>(offsetof(CollectBlockerGoal_t1534435222, ___amount_1)); }
	inline int32_t get_amount_1() const { return ___amount_1; }
	inline int32_t* get_address_of_amount_1() { return &___amount_1; }
	inline void set_amount_1(int32_t value)
	{
		___amount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTBLOCKERGOAL_T1534435222_H
#ifndef U3CUPDATEU3EC__ANONSTOREYB_T1642166665_H
#define U3CUPDATEU3EC__ANONSTOREYB_T1642166665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<Update>c__AnonStoreyB
struct  U3CUpdateU3Ec__AnonStoreyB_t1642166665  : public RuntimeObject
{
public:
	// UnityEngine.RaycastHit2D GameVanilla.Game.Scenes.GameScene/<Update>c__AnonStoreyB::hit
	RaycastHit2D_t2279581989  ___hit_0;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStoreyB_t1642166665, ___hit_0)); }
	inline RaycastHit2D_t2279581989  get_hit_0() const { return ___hit_0; }
	inline RaycastHit2D_t2279581989 * get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(RaycastHit2D_t2279581989  value)
	{
		___hit_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEU3EC__ANONSTOREYB_T1642166665_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BLOCKSCORE_T3740208527_H
#define BLOCKSCORE_T3740208527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BlockScore
struct  BlockScore_t3740208527  : public TileScoreOverride_t3927633711
{
public:
	// GameVanilla.Game.Common.BlockType GameVanilla.Game.Common.BlockScore::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(BlockScore_t3740208527, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKSCORE_T3740208527_H
#ifndef U3CDESTROYBOOSTERCORUTINEU3EC__ITERATOR1_T1074004978_H
#define U3CDESTROYBOOSTERCORUTINEU3EC__ITERATOR1_T1074004978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1
struct  U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978  : public RuntimeObject
{
public:
	// GameVanilla.Game.Common.MatchType GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<matchType>__0
	int32_t ___U3CmatchTypeU3E__0_0;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<score>__0
	int32_t ___U3CscoreU3E__0_1;
	// GameVanilla.Game.Common.TileEntity GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::blockToDestroy
	TileEntity_t3486638455 * ___blockToDestroy_2;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<blockIdx>__0
	int32_t ___U3CblockIdxU3E__0_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<blocksToDestroy>__0
	List_1_t2585711361 * ___U3CblocksToDestroyU3E__0_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<usedBoosters>__0
	List_1_t2585711361 * ___U3CusedBoostersU3E__0_5;
	// System.Collections.Generic.List`1<GameVanilla.Game.Common.BoosterType> GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<boosterTypeList>__0
	List_1_t1057125992 * ___U3CboosterTypeListU3E__0_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<aroundObject>__0
	List_1_t2585711361 * ___U3CaroundObjectU3E__0_7;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::isColorBombCombo
	bool ___isColorBombCombo_8;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::$locvar0
	Enumerator_t179987942  ___U24locvar0_9;
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<maxType>__1
	int32_t ___U3CmaxTypeU3E__1_10;
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<minType>__1
	int32_t ___U3CminTypeU3E__1_11;
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::$this
	GameScene_t3472316885 * ___U24this_12;
	// System.Object GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::$current
	RuntimeObject * ___U24current_13;
	// System.Boolean GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::$disposing
	bool ___U24disposing_14;
	// System.Int32 GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::$PC
	int32_t ___U24PC_15;
	// GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1/<DestroyBoosterCorutine>c__AnonStoreyE GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::$locvar2
	U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677 * ___U24locvar2_16;

public:
	inline static int32_t get_offset_of_U3CmatchTypeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CmatchTypeU3E__0_0)); }
	inline int32_t get_U3CmatchTypeU3E__0_0() const { return ___U3CmatchTypeU3E__0_0; }
	inline int32_t* get_address_of_U3CmatchTypeU3E__0_0() { return &___U3CmatchTypeU3E__0_0; }
	inline void set_U3CmatchTypeU3E__0_0(int32_t value)
	{
		___U3CmatchTypeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CscoreU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CscoreU3E__0_1)); }
	inline int32_t get_U3CscoreU3E__0_1() const { return ___U3CscoreU3E__0_1; }
	inline int32_t* get_address_of_U3CscoreU3E__0_1() { return &___U3CscoreU3E__0_1; }
	inline void set_U3CscoreU3E__0_1(int32_t value)
	{
		___U3CscoreU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_blockToDestroy_2() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___blockToDestroy_2)); }
	inline TileEntity_t3486638455 * get_blockToDestroy_2() const { return ___blockToDestroy_2; }
	inline TileEntity_t3486638455 ** get_address_of_blockToDestroy_2() { return &___blockToDestroy_2; }
	inline void set_blockToDestroy_2(TileEntity_t3486638455 * value)
	{
		___blockToDestroy_2 = value;
		Il2CppCodeGenWriteBarrier((&___blockToDestroy_2), value);
	}

	inline static int32_t get_offset_of_U3CblockIdxU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CblockIdxU3E__0_3)); }
	inline int32_t get_U3CblockIdxU3E__0_3() const { return ___U3CblockIdxU3E__0_3; }
	inline int32_t* get_address_of_U3CblockIdxU3E__0_3() { return &___U3CblockIdxU3E__0_3; }
	inline void set_U3CblockIdxU3E__0_3(int32_t value)
	{
		___U3CblockIdxU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CblocksToDestroyU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CblocksToDestroyU3E__0_4)); }
	inline List_1_t2585711361 * get_U3CblocksToDestroyU3E__0_4() const { return ___U3CblocksToDestroyU3E__0_4; }
	inline List_1_t2585711361 ** get_address_of_U3CblocksToDestroyU3E__0_4() { return &___U3CblocksToDestroyU3E__0_4; }
	inline void set_U3CblocksToDestroyU3E__0_4(List_1_t2585711361 * value)
	{
		___U3CblocksToDestroyU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CblocksToDestroyU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CusedBoostersU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CusedBoostersU3E__0_5)); }
	inline List_1_t2585711361 * get_U3CusedBoostersU3E__0_5() const { return ___U3CusedBoostersU3E__0_5; }
	inline List_1_t2585711361 ** get_address_of_U3CusedBoostersU3E__0_5() { return &___U3CusedBoostersU3E__0_5; }
	inline void set_U3CusedBoostersU3E__0_5(List_1_t2585711361 * value)
	{
		___U3CusedBoostersU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CusedBoostersU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CboosterTypeListU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CboosterTypeListU3E__0_6)); }
	inline List_1_t1057125992 * get_U3CboosterTypeListU3E__0_6() const { return ___U3CboosterTypeListU3E__0_6; }
	inline List_1_t1057125992 ** get_address_of_U3CboosterTypeListU3E__0_6() { return &___U3CboosterTypeListU3E__0_6; }
	inline void set_U3CboosterTypeListU3E__0_6(List_1_t1057125992 * value)
	{
		___U3CboosterTypeListU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CboosterTypeListU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CaroundObjectU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CaroundObjectU3E__0_7)); }
	inline List_1_t2585711361 * get_U3CaroundObjectU3E__0_7() const { return ___U3CaroundObjectU3E__0_7; }
	inline List_1_t2585711361 ** get_address_of_U3CaroundObjectU3E__0_7() { return &___U3CaroundObjectU3E__0_7; }
	inline void set_U3CaroundObjectU3E__0_7(List_1_t2585711361 * value)
	{
		___U3CaroundObjectU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaroundObjectU3E__0_7), value);
	}

	inline static int32_t get_offset_of_isColorBombCombo_8() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___isColorBombCombo_8)); }
	inline bool get_isColorBombCombo_8() const { return ___isColorBombCombo_8; }
	inline bool* get_address_of_isColorBombCombo_8() { return &___isColorBombCombo_8; }
	inline void set_isColorBombCombo_8(bool value)
	{
		___isColorBombCombo_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U24locvar0_9)); }
	inline Enumerator_t179987942  get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline Enumerator_t179987942 * get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(Enumerator_t179987942  value)
	{
		___U24locvar0_9 = value;
	}

	inline static int32_t get_offset_of_U3CmaxTypeU3E__1_10() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CmaxTypeU3E__1_10)); }
	inline int32_t get_U3CmaxTypeU3E__1_10() const { return ___U3CmaxTypeU3E__1_10; }
	inline int32_t* get_address_of_U3CmaxTypeU3E__1_10() { return &___U3CmaxTypeU3E__1_10; }
	inline void set_U3CmaxTypeU3E__1_10(int32_t value)
	{
		___U3CmaxTypeU3E__1_10 = value;
	}

	inline static int32_t get_offset_of_U3CminTypeU3E__1_11() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U3CminTypeU3E__1_11)); }
	inline int32_t get_U3CminTypeU3E__1_11() const { return ___U3CminTypeU3E__1_11; }
	inline int32_t* get_address_of_U3CminTypeU3E__1_11() { return &___U3CminTypeU3E__1_11; }
	inline void set_U3CminTypeU3E__1_11(int32_t value)
	{
		___U3CminTypeU3E__1_11 = value;
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U24this_12)); }
	inline GameScene_t3472316885 * get_U24this_12() const { return ___U24this_12; }
	inline GameScene_t3472316885 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(GameScene_t3472316885 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_12), value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U24current_13)); }
	inline RuntimeObject * get_U24current_13() const { return ___U24current_13; }
	inline RuntimeObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(RuntimeObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_13), value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_16() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978, ___U24locvar2_16)); }
	inline U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677 * get_U24locvar2_16() const { return ___U24locvar2_16; }
	inline U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677 ** get_address_of_U24locvar2_16() { return &___U24locvar2_16; }
	inline void set_U24locvar2_16(U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677 * value)
	{
		___U24locvar2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_16), value);
	}
};

struct U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978_StaticFields
{
public:
	// System.Func`2<UnityEngine.GameObject,System.Boolean> GameVanilla.Game.Scenes.GameScene/<DestroyBoosterCorutine>c__Iterator1::<>f__am$cache0
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYBOOSTERCORUTINEU3EC__ITERATOR1_T1074004978_H
#ifndef BLOCKERSCORE_T111246344_H
#define BLOCKERSCORE_T111246344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BlockerScore
struct  BlockerScore_t111246344  : public TileScoreOverride_t3927633711
{
public:
	// GameVanilla.Game.Common.BlockerType GameVanilla.Game.Common.BlockerScore::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(BlockerScore_t111246344, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKERSCORE_T111246344_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BLOCKTILE_T985975110_H
#define BLOCKTILE_T985975110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BlockTile
struct  BlockTile_t985975110  : public LevelTile_t145158312
{
public:
	// GameVanilla.Game.Common.BlockType GameVanilla.Game.Common.BlockTile::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(BlockTile_t985975110, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKTILE_T985975110_H
#ifndef BOOSTERTILE_T3880804994_H
#define BOOSTERTILE_T3880804994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BoosterTile
struct  BoosterTile_t3880804994  : public LevelTile_t145158312
{
public:
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.Common.BoosterTile::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(BoosterTile_t3880804994, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOSTERTILE_T3880804994_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GOALUI_T912033724_H
#define GOALUI_T912033724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.GoalUi
struct  GoalUi_t912033724  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.HorizontalLayoutGroup GameVanilla.Game.UI.GoalUi::group
	HorizontalLayoutGroup_t2586782146 * ___group_2;

public:
	inline static int32_t get_offset_of_group_2() { return static_cast<int32_t>(offsetof(GoalUi_t912033724, ___group_2)); }
	inline HorizontalLayoutGroup_t2586782146 * get_group_2() const { return ___group_2; }
	inline HorizontalLayoutGroup_t2586782146 ** get_address_of_group_2() { return &___group_2; }
	inline void set_group_2(HorizontalLayoutGroup_t2586782146 * value)
	{
		___group_2 = value;
		Il2CppCodeGenWriteBarrier((&___group_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOALUI_T912033724_H
#ifndef COINSSYSTEM_T740186038_H
#define COINSSYSTEM_T740186038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.CoinsSystem
struct  CoinsSystem_t740186038  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<System.Int32> GameVanilla.Game.Common.CoinsSystem::onCoinsUpdated
	Action_1_t3123413348 * ___onCoinsUpdated_2;

public:
	inline static int32_t get_offset_of_onCoinsUpdated_2() { return static_cast<int32_t>(offsetof(CoinsSystem_t740186038, ___onCoinsUpdated_2)); }
	inline Action_1_t3123413348 * get_onCoinsUpdated_2() const { return ___onCoinsUpdated_2; }
	inline Action_1_t3123413348 ** get_address_of_onCoinsUpdated_2() { return &___onCoinsUpdated_2; }
	inline void set_onCoinsUpdated_2(Action_1_t3123413348 * value)
	{
		___onCoinsUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCoinsUpdated_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINSSYSTEM_T740186038_H
#ifndef BUYBOOSTERBUTTON_T1892892637_H
#define BUYBOOSTERBUTTON_T1892892637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.BuyBoosterButton
struct  BuyBoosterButton_t1892892637  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.UI.BuyBoosterButton::boosterType
	int32_t ___boosterType_2;
	// UnityEngine.GameObject GameVanilla.Game.UI.BuyBoosterButton::amountGroup
	GameObject_t1113636619 * ___amountGroup_3;
	// UnityEngine.GameObject GameVanilla.Game.UI.BuyBoosterButton::moreGroup
	GameObject_t1113636619 * ___moreGroup_4;
	// UnityEngine.UI.Text GameVanilla.Game.UI.BuyBoosterButton::amountText
	Text_t1901882714 * ___amountText_5;

public:
	inline static int32_t get_offset_of_boosterType_2() { return static_cast<int32_t>(offsetof(BuyBoosterButton_t1892892637, ___boosterType_2)); }
	inline int32_t get_boosterType_2() const { return ___boosterType_2; }
	inline int32_t* get_address_of_boosterType_2() { return &___boosterType_2; }
	inline void set_boosterType_2(int32_t value)
	{
		___boosterType_2 = value;
	}

	inline static int32_t get_offset_of_amountGroup_3() { return static_cast<int32_t>(offsetof(BuyBoosterButton_t1892892637, ___amountGroup_3)); }
	inline GameObject_t1113636619 * get_amountGroup_3() const { return ___amountGroup_3; }
	inline GameObject_t1113636619 ** get_address_of_amountGroup_3() { return &___amountGroup_3; }
	inline void set_amountGroup_3(GameObject_t1113636619 * value)
	{
		___amountGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___amountGroup_3), value);
	}

	inline static int32_t get_offset_of_moreGroup_4() { return static_cast<int32_t>(offsetof(BuyBoosterButton_t1892892637, ___moreGroup_4)); }
	inline GameObject_t1113636619 * get_moreGroup_4() const { return ___moreGroup_4; }
	inline GameObject_t1113636619 ** get_address_of_moreGroup_4() { return &___moreGroup_4; }
	inline void set_moreGroup_4(GameObject_t1113636619 * value)
	{
		___moreGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___moreGroup_4), value);
	}

	inline static int32_t get_offset_of_amountText_5() { return static_cast<int32_t>(offsetof(BuyBoosterButton_t1892892637, ___amountText_5)); }
	inline Text_t1901882714 * get_amountText_5() const { return ___amountText_5; }
	inline Text_t1901882714 ** get_address_of_amountText_5() { return &___amountText_5; }
	inline void set_amountText_5(Text_t1901882714 * value)
	{
		___amountText_5 = value;
		Il2CppCodeGenWriteBarrier((&___amountText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYBOOSTERBUTTON_T1892892637_H
#ifndef BASESCENE_T2918414559_H
#define BASESCENE_T2918414559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene
struct  BaseScene_t2918414559  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas GameVanilla.Core.BaseScene::canvas
	Canvas_t3310196443 * ___canvas_2;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPopups
	Stack_1_t1957026074 * ___currentPopups_3;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPanels
	Stack_1_t1957026074 * ___currentPanels_4;

public:
	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___canvas_2)); }
	inline Canvas_t3310196443 * get_canvas_2() const { return ___canvas_2; }
	inline Canvas_t3310196443 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(Canvas_t3310196443 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}

	inline static int32_t get_offset_of_currentPopups_3() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPopups_3)); }
	inline Stack_1_t1957026074 * get_currentPopups_3() const { return ___currentPopups_3; }
	inline Stack_1_t1957026074 ** get_address_of_currentPopups_3() { return &___currentPopups_3; }
	inline void set_currentPopups_3(Stack_1_t1957026074 * value)
	{
		___currentPopups_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentPopups_3), value);
	}

	inline static int32_t get_offset_of_currentPanels_4() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPanels_4)); }
	inline Stack_1_t1957026074 * get_currentPanels_4() const { return ___currentPanels_4; }
	inline Stack_1_t1957026074 ** get_address_of_currentPanels_4() { return &___currentPanels_4; }
	inline void set_currentPanels_4(Stack_1_t1957026074 * value)
	{
		___currentPanels_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanels_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCENE_T2918414559_H
#ifndef POPUP_T1063720813_H
#define POPUP_T1063720813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.Popup
struct  Popup_t1063720813  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Core.BaseScene GameVanilla.Core.Popup::parentScene
	BaseScene_t2918414559 * ___parentScene_2;
	// UnityEngine.Events.UnityEvent GameVanilla.Core.Popup::onOpen
	UnityEvent_t2581268647 * ___onOpen_3;
	// UnityEngine.Events.UnityEvent GameVanilla.Core.Popup::onClose
	UnityEvent_t2581268647 * ___onClose_4;
	// UnityEngine.Animator GameVanilla.Core.Popup::animator
	Animator_t434523843 * ___animator_5;

public:
	inline static int32_t get_offset_of_parentScene_2() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___parentScene_2)); }
	inline BaseScene_t2918414559 * get_parentScene_2() const { return ___parentScene_2; }
	inline BaseScene_t2918414559 ** get_address_of_parentScene_2() { return &___parentScene_2; }
	inline void set_parentScene_2(BaseScene_t2918414559 * value)
	{
		___parentScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentScene_2), value);
	}

	inline static int32_t get_offset_of_onOpen_3() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___onOpen_3)); }
	inline UnityEvent_t2581268647 * get_onOpen_3() const { return ___onOpen_3; }
	inline UnityEvent_t2581268647 ** get_address_of_onOpen_3() { return &___onOpen_3; }
	inline void set_onOpen_3(UnityEvent_t2581268647 * value)
	{
		___onOpen_3 = value;
		Il2CppCodeGenWriteBarrier((&___onOpen_3), value);
	}

	inline static int32_t get_offset_of_onClose_4() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___onClose_4)); }
	inline UnityEvent_t2581268647 * get_onClose_4() const { return ___onClose_4; }
	inline UnityEvent_t2581268647 ** get_address_of_onClose_4() { return &___onClose_4; }
	inline void set_onClose_4(UnityEvent_t2581268647 * value)
	{
		___onClose_4 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_4), value);
	}

	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUP_T1063720813_H
#ifndef GAMEUI_T3170959104_H
#define GAMEUI_T3170959104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.GameUi
struct  GameUi_t3170959104  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text GameVanilla.Game.UI.GameUi::limitTitleText
	Text_t1901882714 * ___limitTitleText_2;
	// UnityEngine.UI.Text GameVanilla.Game.UI.GameUi::limitText
	Text_t1901882714 * ___limitText_3;
	// UnityEngine.UI.Text GameVanilla.Game.UI.GameUi::scoreText
	Text_t1901882714 * ___scoreText_4;
	// GameVanilla.Game.UI.ProgressBar GameVanilla.Game.UI.GameUi::progressBar
	ProgressBar_t959013686 * ___progressBar_5;
	// GameVanilla.Game.UI.GoalUi GameVanilla.Game.UI.GameUi::goalUi
	GoalUi_t912033724 * ___goalUi_6;
	// UnityEngine.GameObject GameVanilla.Game.UI.GameUi::goalHeadline
	GameObject_t1113636619 * ___goalHeadline_7;
	// UnityEngine.GameObject GameVanilla.Game.UI.GameUi::scoreGoalHeadline
	GameObject_t1113636619 * ___scoreGoalHeadline_8;
	// UnityEngine.UI.Text GameVanilla.Game.UI.GameUi::scoreGoalAmountText
	Text_t1901882714 * ___scoreGoalAmountText_9;

public:
	inline static int32_t get_offset_of_limitTitleText_2() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___limitTitleText_2)); }
	inline Text_t1901882714 * get_limitTitleText_2() const { return ___limitTitleText_2; }
	inline Text_t1901882714 ** get_address_of_limitTitleText_2() { return &___limitTitleText_2; }
	inline void set_limitTitleText_2(Text_t1901882714 * value)
	{
		___limitTitleText_2 = value;
		Il2CppCodeGenWriteBarrier((&___limitTitleText_2), value);
	}

	inline static int32_t get_offset_of_limitText_3() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___limitText_3)); }
	inline Text_t1901882714 * get_limitText_3() const { return ___limitText_3; }
	inline Text_t1901882714 ** get_address_of_limitText_3() { return &___limitText_3; }
	inline void set_limitText_3(Text_t1901882714 * value)
	{
		___limitText_3 = value;
		Il2CppCodeGenWriteBarrier((&___limitText_3), value);
	}

	inline static int32_t get_offset_of_scoreText_4() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___scoreText_4)); }
	inline Text_t1901882714 * get_scoreText_4() const { return ___scoreText_4; }
	inline Text_t1901882714 ** get_address_of_scoreText_4() { return &___scoreText_4; }
	inline void set_scoreText_4(Text_t1901882714 * value)
	{
		___scoreText_4 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_4), value);
	}

	inline static int32_t get_offset_of_progressBar_5() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___progressBar_5)); }
	inline ProgressBar_t959013686 * get_progressBar_5() const { return ___progressBar_5; }
	inline ProgressBar_t959013686 ** get_address_of_progressBar_5() { return &___progressBar_5; }
	inline void set_progressBar_5(ProgressBar_t959013686 * value)
	{
		___progressBar_5 = value;
		Il2CppCodeGenWriteBarrier((&___progressBar_5), value);
	}

	inline static int32_t get_offset_of_goalUi_6() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___goalUi_6)); }
	inline GoalUi_t912033724 * get_goalUi_6() const { return ___goalUi_6; }
	inline GoalUi_t912033724 ** get_address_of_goalUi_6() { return &___goalUi_6; }
	inline void set_goalUi_6(GoalUi_t912033724 * value)
	{
		___goalUi_6 = value;
		Il2CppCodeGenWriteBarrier((&___goalUi_6), value);
	}

	inline static int32_t get_offset_of_goalHeadline_7() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___goalHeadline_7)); }
	inline GameObject_t1113636619 * get_goalHeadline_7() const { return ___goalHeadline_7; }
	inline GameObject_t1113636619 ** get_address_of_goalHeadline_7() { return &___goalHeadline_7; }
	inline void set_goalHeadline_7(GameObject_t1113636619 * value)
	{
		___goalHeadline_7 = value;
		Il2CppCodeGenWriteBarrier((&___goalHeadline_7), value);
	}

	inline static int32_t get_offset_of_scoreGoalHeadline_8() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___scoreGoalHeadline_8)); }
	inline GameObject_t1113636619 * get_scoreGoalHeadline_8() const { return ___scoreGoalHeadline_8; }
	inline GameObject_t1113636619 ** get_address_of_scoreGoalHeadline_8() { return &___scoreGoalHeadline_8; }
	inline void set_scoreGoalHeadline_8(GameObject_t1113636619 * value)
	{
		___scoreGoalHeadline_8 = value;
		Il2CppCodeGenWriteBarrier((&___scoreGoalHeadline_8), value);
	}

	inline static int32_t get_offset_of_scoreGoalAmountText_9() { return static_cast<int32_t>(offsetof(GameUi_t3170959104, ___scoreGoalAmountText_9)); }
	inline Text_t1901882714 * get_scoreGoalAmountText_9() const { return ___scoreGoalAmountText_9; }
	inline Text_t1901882714 ** get_address_of_scoreGoalAmountText_9() { return &___scoreGoalAmountText_9; }
	inline void set_scoreGoalAmountText_9(Text_t1901882714 * value)
	{
		___scoreGoalAmountText_9 = value;
		Il2CppCodeGenWriteBarrier((&___scoreGoalAmountText_9), value);
	}
};

struct GameUi_t3170959104_StaticFields
{
public:
	// System.Predicate`1<GameVanilla.Game.Common.Goal> GameVanilla.Game.UI.GameUi::<>f__am$cache0
	Predicate_1_t2324985759 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(GameUi_t3170959104_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Predicate_1_t2324985759 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Predicate_1_t2324985759 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Predicate_1_t2324985759 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEUI_T3170959104_H
#ifndef BUYCOINSBAR_T669101563_H
#define BUYCOINSBAR_T669101563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.BuyCoinsBar
struct  BuyCoinsBar_t669101563  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Scenes.LevelScene GameVanilla.Game.UI.BuyCoinsBar::levelScene
	LevelScene_t3942611905 * ___levelScene_2;
	// UnityEngine.UI.Text GameVanilla.Game.UI.BuyCoinsBar::numCoinsText
	Text_t1901882714 * ___numCoinsText_3;

public:
	inline static int32_t get_offset_of_levelScene_2() { return static_cast<int32_t>(offsetof(BuyCoinsBar_t669101563, ___levelScene_2)); }
	inline LevelScene_t3942611905 * get_levelScene_2() const { return ___levelScene_2; }
	inline LevelScene_t3942611905 ** get_address_of_levelScene_2() { return &___levelScene_2; }
	inline void set_levelScene_2(LevelScene_t3942611905 * value)
	{
		___levelScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelScene_2), value);
	}

	inline static int32_t get_offset_of_numCoinsText_3() { return static_cast<int32_t>(offsetof(BuyCoinsBar_t669101563, ___numCoinsText_3)); }
	inline Text_t1901882714 * get_numCoinsText_3() const { return ___numCoinsText_3; }
	inline Text_t1901882714 ** get_address_of_numCoinsText_3() { return &___numCoinsText_3; }
	inline void set_numCoinsText_3(Text_t1901882714 * value)
	{
		___numCoinsText_3 = value;
		Il2CppCodeGenWriteBarrier((&___numCoinsText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYCOINSBAR_T669101563_H
#ifndef BUYLIVESBAR_T2747199656_H
#define BUYLIVESBAR_T2747199656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.BuyLivesBar
struct  BuyLivesBar_t2747199656  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Scenes.LevelScene GameVanilla.Game.UI.BuyLivesBar::levelScene
	LevelScene_t3942611905 * ___levelScene_2;
	// UnityEngine.Sprite GameVanilla.Game.UI.BuyLivesBar::enabledLifeSprite
	Sprite_t280657092 * ___enabledLifeSprite_3;
	// UnityEngine.Sprite GameVanilla.Game.UI.BuyLivesBar::disabledLifeSprite
	Sprite_t280657092 * ___disabledLifeSprite_4;
	// UnityEngine.UI.Image GameVanilla.Game.UI.BuyLivesBar::lifeImage
	Image_t2670269651 * ___lifeImage_5;
	// UnityEngine.UI.Text GameVanilla.Game.UI.BuyLivesBar::numLivesText
	Text_t1901882714 * ___numLivesText_6;
	// UnityEngine.UI.Text GameVanilla.Game.UI.BuyLivesBar::timeToNextLifeText
	Text_t1901882714 * ___timeToNextLifeText_7;
	// UnityEngine.UI.Image GameVanilla.Game.UI.BuyLivesBar::buttonImage
	Image_t2670269651 * ___buttonImage_8;
	// UnityEngine.Sprite GameVanilla.Game.UI.BuyLivesBar::enabledButtonSprite
	Sprite_t280657092 * ___enabledButtonSprite_9;
	// UnityEngine.Sprite GameVanilla.Game.UI.BuyLivesBar::disabledButtonSprite
	Sprite_t280657092 * ___disabledButtonSprite_10;

public:
	inline static int32_t get_offset_of_levelScene_2() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___levelScene_2)); }
	inline LevelScene_t3942611905 * get_levelScene_2() const { return ___levelScene_2; }
	inline LevelScene_t3942611905 ** get_address_of_levelScene_2() { return &___levelScene_2; }
	inline void set_levelScene_2(LevelScene_t3942611905 * value)
	{
		___levelScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelScene_2), value);
	}

	inline static int32_t get_offset_of_enabledLifeSprite_3() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___enabledLifeSprite_3)); }
	inline Sprite_t280657092 * get_enabledLifeSprite_3() const { return ___enabledLifeSprite_3; }
	inline Sprite_t280657092 ** get_address_of_enabledLifeSprite_3() { return &___enabledLifeSprite_3; }
	inline void set_enabledLifeSprite_3(Sprite_t280657092 * value)
	{
		___enabledLifeSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___enabledLifeSprite_3), value);
	}

	inline static int32_t get_offset_of_disabledLifeSprite_4() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___disabledLifeSprite_4)); }
	inline Sprite_t280657092 * get_disabledLifeSprite_4() const { return ___disabledLifeSprite_4; }
	inline Sprite_t280657092 ** get_address_of_disabledLifeSprite_4() { return &___disabledLifeSprite_4; }
	inline void set_disabledLifeSprite_4(Sprite_t280657092 * value)
	{
		___disabledLifeSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___disabledLifeSprite_4), value);
	}

	inline static int32_t get_offset_of_lifeImage_5() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___lifeImage_5)); }
	inline Image_t2670269651 * get_lifeImage_5() const { return ___lifeImage_5; }
	inline Image_t2670269651 ** get_address_of_lifeImage_5() { return &___lifeImage_5; }
	inline void set_lifeImage_5(Image_t2670269651 * value)
	{
		___lifeImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___lifeImage_5), value);
	}

	inline static int32_t get_offset_of_numLivesText_6() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___numLivesText_6)); }
	inline Text_t1901882714 * get_numLivesText_6() const { return ___numLivesText_6; }
	inline Text_t1901882714 ** get_address_of_numLivesText_6() { return &___numLivesText_6; }
	inline void set_numLivesText_6(Text_t1901882714 * value)
	{
		___numLivesText_6 = value;
		Il2CppCodeGenWriteBarrier((&___numLivesText_6), value);
	}

	inline static int32_t get_offset_of_timeToNextLifeText_7() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___timeToNextLifeText_7)); }
	inline Text_t1901882714 * get_timeToNextLifeText_7() const { return ___timeToNextLifeText_7; }
	inline Text_t1901882714 ** get_address_of_timeToNextLifeText_7() { return &___timeToNextLifeText_7; }
	inline void set_timeToNextLifeText_7(Text_t1901882714 * value)
	{
		___timeToNextLifeText_7 = value;
		Il2CppCodeGenWriteBarrier((&___timeToNextLifeText_7), value);
	}

	inline static int32_t get_offset_of_buttonImage_8() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___buttonImage_8)); }
	inline Image_t2670269651 * get_buttonImage_8() const { return ___buttonImage_8; }
	inline Image_t2670269651 ** get_address_of_buttonImage_8() { return &___buttonImage_8; }
	inline void set_buttonImage_8(Image_t2670269651 * value)
	{
		___buttonImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___buttonImage_8), value);
	}

	inline static int32_t get_offset_of_enabledButtonSprite_9() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___enabledButtonSprite_9)); }
	inline Sprite_t280657092 * get_enabledButtonSprite_9() const { return ___enabledButtonSprite_9; }
	inline Sprite_t280657092 ** get_address_of_enabledButtonSprite_9() { return &___enabledButtonSprite_9; }
	inline void set_enabledButtonSprite_9(Sprite_t280657092 * value)
	{
		___enabledButtonSprite_9 = value;
		Il2CppCodeGenWriteBarrier((&___enabledButtonSprite_9), value);
	}

	inline static int32_t get_offset_of_disabledButtonSprite_10() { return static_cast<int32_t>(offsetof(BuyLivesBar_t2747199656, ___disabledButtonSprite_10)); }
	inline Sprite_t280657092 * get_disabledButtonSprite_10() const { return ___disabledButtonSprite_10; }
	inline Sprite_t280657092 ** get_address_of_disabledButtonSprite_10() { return &___disabledButtonSprite_10; }
	inline void set_disabledButtonSprite_10(Sprite_t280657092 * value)
	{
		___disabledButtonSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___disabledButtonSprite_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYLIVESBAR_T2747199656_H
#ifndef LIVESSYSTEM_T2420331318_H
#define LIVESSYSTEM_T2420331318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.LivesSystem
struct  LivesSystem_t2420331318  : public MonoBehaviour_t3962482529
{
public:
	// System.DateTime GameVanilla.Game.Common.LivesSystem::oldDate
	DateTime_t3738529785  ___oldDate_2;
	// System.TimeSpan GameVanilla.Game.Common.LivesSystem::timeSpan
	TimeSpan_t881159249  ___timeSpan_3;
	// System.Boolean GameVanilla.Game.Common.LivesSystem::runningCountdown
	bool ___runningCountdown_4;
	// System.Single GameVanilla.Game.Common.LivesSystem::accTime
	float ___accTime_5;
	// System.Action`2<System.TimeSpan,System.Int32> GameVanilla.Game.Common.LivesSystem::onCountdownUpdated
	Action_2_t3788266618 * ___onCountdownUpdated_6;
	// System.Action`1<System.Int32> GameVanilla.Game.Common.LivesSystem::onCountdownFinished
	Action_1_t3123413348 * ___onCountdownFinished_7;

public:
	inline static int32_t get_offset_of_oldDate_2() { return static_cast<int32_t>(offsetof(LivesSystem_t2420331318, ___oldDate_2)); }
	inline DateTime_t3738529785  get_oldDate_2() const { return ___oldDate_2; }
	inline DateTime_t3738529785 * get_address_of_oldDate_2() { return &___oldDate_2; }
	inline void set_oldDate_2(DateTime_t3738529785  value)
	{
		___oldDate_2 = value;
	}

	inline static int32_t get_offset_of_timeSpan_3() { return static_cast<int32_t>(offsetof(LivesSystem_t2420331318, ___timeSpan_3)); }
	inline TimeSpan_t881159249  get_timeSpan_3() const { return ___timeSpan_3; }
	inline TimeSpan_t881159249 * get_address_of_timeSpan_3() { return &___timeSpan_3; }
	inline void set_timeSpan_3(TimeSpan_t881159249  value)
	{
		___timeSpan_3 = value;
	}

	inline static int32_t get_offset_of_runningCountdown_4() { return static_cast<int32_t>(offsetof(LivesSystem_t2420331318, ___runningCountdown_4)); }
	inline bool get_runningCountdown_4() const { return ___runningCountdown_4; }
	inline bool* get_address_of_runningCountdown_4() { return &___runningCountdown_4; }
	inline void set_runningCountdown_4(bool value)
	{
		___runningCountdown_4 = value;
	}

	inline static int32_t get_offset_of_accTime_5() { return static_cast<int32_t>(offsetof(LivesSystem_t2420331318, ___accTime_5)); }
	inline float get_accTime_5() const { return ___accTime_5; }
	inline float* get_address_of_accTime_5() { return &___accTime_5; }
	inline void set_accTime_5(float value)
	{
		___accTime_5 = value;
	}

	inline static int32_t get_offset_of_onCountdownUpdated_6() { return static_cast<int32_t>(offsetof(LivesSystem_t2420331318, ___onCountdownUpdated_6)); }
	inline Action_2_t3788266618 * get_onCountdownUpdated_6() const { return ___onCountdownUpdated_6; }
	inline Action_2_t3788266618 ** get_address_of_onCountdownUpdated_6() { return &___onCountdownUpdated_6; }
	inline void set_onCountdownUpdated_6(Action_2_t3788266618 * value)
	{
		___onCountdownUpdated_6 = value;
		Il2CppCodeGenWriteBarrier((&___onCountdownUpdated_6), value);
	}

	inline static int32_t get_offset_of_onCountdownFinished_7() { return static_cast<int32_t>(offsetof(LivesSystem_t2420331318, ___onCountdownFinished_7)); }
	inline Action_1_t3123413348 * get_onCountdownFinished_7() const { return ___onCountdownFinished_7; }
	inline Action_1_t3123413348 ** get_address_of_onCountdownFinished_7() { return &___onCountdownFinished_7; }
	inline void set_onCountdownFinished_7(Action_1_t3123413348 * value)
	{
		___onCountdownFinished_7 = value;
		Il2CppCodeGenWriteBarrier((&___onCountdownFinished_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIVESSYSTEM_T2420331318_H
#ifndef LOADER_T1548667593_H
#define LOADER_T1548667593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Loader
struct  Loader_t1548667593  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Common.PuzzleMatchManager GameVanilla.Game.Common.Loader::gameManager
	PuzzleMatchManager_t2899242229 * ___gameManager_2;

public:
	inline static int32_t get_offset_of_gameManager_2() { return static_cast<int32_t>(offsetof(Loader_t1548667593, ___gameManager_2)); }
	inline PuzzleMatchManager_t2899242229 * get_gameManager_2() const { return ___gameManager_2; }
	inline PuzzleMatchManager_t2899242229 ** get_address_of_gameManager_2() { return &___gameManager_2; }
	inline void set_gameManager_2(PuzzleMatchManager_t2899242229 * value)
	{
		___gameManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADER_T1548667593_H
#ifndef TILEENTITY_T3486638455_H
#define TILEENTITY_T3486638455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.TileEntity
struct  TileEntity_t3486638455  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Events.UnityEvent GameVanilla.Game.Common.TileEntity::onSpawn
	UnityEvent_t2581268647 * ___onSpawn_2;
	// UnityEngine.Events.UnityEvent GameVanilla.Game.Common.TileEntity::onExplode
	UnityEvent_t2581268647 * ___onExplode_3;

public:
	inline static int32_t get_offset_of_onSpawn_2() { return static_cast<int32_t>(offsetof(TileEntity_t3486638455, ___onSpawn_2)); }
	inline UnityEvent_t2581268647 * get_onSpawn_2() const { return ___onSpawn_2; }
	inline UnityEvent_t2581268647 ** get_address_of_onSpawn_2() { return &___onSpawn_2; }
	inline void set_onSpawn_2(UnityEvent_t2581268647 * value)
	{
		___onSpawn_2 = value;
		Il2CppCodeGenWriteBarrier((&___onSpawn_2), value);
	}

	inline static int32_t get_offset_of_onExplode_3() { return static_cast<int32_t>(offsetof(TileEntity_t3486638455, ___onExplode_3)); }
	inline UnityEvent_t2581268647 * get_onExplode_3() const { return ___onExplode_3; }
	inline UnityEvent_t2581268647 ** get_address_of_onExplode_3() { return &___onExplode_3; }
	inline void set_onExplode_3(UnityEvent_t2581268647 * value)
	{
		___onExplode_3 = value;
		Il2CppCodeGenWriteBarrier((&___onExplode_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEENTITY_T3486638455_H
#ifndef PUZZLEMATCHMANAGER_T2899242229_H
#define PUZZLEMATCHMANAGER_T2899242229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.PuzzleMatchManager
struct  PuzzleMatchManager_t2899242229  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Common.GameConfiguration GameVanilla.Game.Common.PuzzleMatchManager::gameConfig
	GameConfiguration_t2964563698 * ___gameConfig_3;
	// GameVanilla.Game.Common.LivesSystem GameVanilla.Game.Common.PuzzleMatchManager::livesSystem
	LivesSystem_t2420331318 * ___livesSystem_4;
	// GameVanilla.Game.Common.CoinsSystem GameVanilla.Game.Common.PuzzleMatchManager::coinsSystem
	CoinsSystem_t740186038 * ___coinsSystem_5;
	// System.Int32 GameVanilla.Game.Common.PuzzleMatchManager::lastSelectedLevel
	int32_t ___lastSelectedLevel_6;
	// System.Boolean GameVanilla.Game.Common.PuzzleMatchManager::unlockedNextLevel
	bool ___unlockedNextLevel_7;

public:
	inline static int32_t get_offset_of_gameConfig_3() { return static_cast<int32_t>(offsetof(PuzzleMatchManager_t2899242229, ___gameConfig_3)); }
	inline GameConfiguration_t2964563698 * get_gameConfig_3() const { return ___gameConfig_3; }
	inline GameConfiguration_t2964563698 ** get_address_of_gameConfig_3() { return &___gameConfig_3; }
	inline void set_gameConfig_3(GameConfiguration_t2964563698 * value)
	{
		___gameConfig_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameConfig_3), value);
	}

	inline static int32_t get_offset_of_livesSystem_4() { return static_cast<int32_t>(offsetof(PuzzleMatchManager_t2899242229, ___livesSystem_4)); }
	inline LivesSystem_t2420331318 * get_livesSystem_4() const { return ___livesSystem_4; }
	inline LivesSystem_t2420331318 ** get_address_of_livesSystem_4() { return &___livesSystem_4; }
	inline void set_livesSystem_4(LivesSystem_t2420331318 * value)
	{
		___livesSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___livesSystem_4), value);
	}

	inline static int32_t get_offset_of_coinsSystem_5() { return static_cast<int32_t>(offsetof(PuzzleMatchManager_t2899242229, ___coinsSystem_5)); }
	inline CoinsSystem_t740186038 * get_coinsSystem_5() const { return ___coinsSystem_5; }
	inline CoinsSystem_t740186038 ** get_address_of_coinsSystem_5() { return &___coinsSystem_5; }
	inline void set_coinsSystem_5(CoinsSystem_t740186038 * value)
	{
		___coinsSystem_5 = value;
		Il2CppCodeGenWriteBarrier((&___coinsSystem_5), value);
	}

	inline static int32_t get_offset_of_lastSelectedLevel_6() { return static_cast<int32_t>(offsetof(PuzzleMatchManager_t2899242229, ___lastSelectedLevel_6)); }
	inline int32_t get_lastSelectedLevel_6() const { return ___lastSelectedLevel_6; }
	inline int32_t* get_address_of_lastSelectedLevel_6() { return &___lastSelectedLevel_6; }
	inline void set_lastSelectedLevel_6(int32_t value)
	{
		___lastSelectedLevel_6 = value;
	}

	inline static int32_t get_offset_of_unlockedNextLevel_7() { return static_cast<int32_t>(offsetof(PuzzleMatchManager_t2899242229, ___unlockedNextLevel_7)); }
	inline bool get_unlockedNextLevel_7() const { return ___unlockedNextLevel_7; }
	inline bool* get_address_of_unlockedNextLevel_7() { return &___unlockedNextLevel_7; }
	inline void set_unlockedNextLevel_7(bool value)
	{
		___unlockedNextLevel_7 = value;
	}
};

struct PuzzleMatchManager_t2899242229_StaticFields
{
public:
	// GameVanilla.Game.Common.PuzzleMatchManager GameVanilla.Game.Common.PuzzleMatchManager::instance
	PuzzleMatchManager_t2899242229 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PuzzleMatchManager_t2899242229_StaticFields, ___instance_2)); }
	inline PuzzleMatchManager_t2899242229 * get_instance_2() const { return ___instance_2; }
	inline PuzzleMatchManager_t2899242229 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PuzzleMatchManager_t2899242229 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEMATCHMANAGER_T2899242229_H
#ifndef GAMEPOOLS_T2964324386_H
#define GAMEPOOLS_T2964324386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.GamePools
struct  GamePools_t2964324386  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block1Pool
	ObjectPool_t858432606 * ___block1Pool_2;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block2Pool
	ObjectPool_t858432606 * ___block2Pool_3;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block3Pool
	ObjectPool_t858432606 * ___block3Pool_4;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block4Pool
	ObjectPool_t858432606 * ___block4Pool_5;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block5Pool
	ObjectPool_t858432606 * ___block5Pool_6;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block6Pool
	ObjectPool_t858432606 * ___block6Pool_7;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::emptyTilePool
	ObjectPool_t858432606 * ___emptyTilePool_8;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::ballPool
	ObjectPool_t858432606 * ___ballPool_9;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::stonePool
	ObjectPool_t858432606 * ___stonePool_10;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::collectablePool
	ObjectPool_t858432606 * ___collectablePool_11;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::horizontalBombPool
	ObjectPool_t858432606 * ___horizontalBombPool_12;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::verticalBombPool
	ObjectPool_t858432606 * ___verticalBombPool_13;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::dynamitePool
	ObjectPool_t858432606 * ___dynamitePool_14;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::colorBombPool
	ObjectPool_t858432606 * ___colorBombPool_15;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::icePool
	ObjectPool_t858432606 * ___icePool_16;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block1ParticlesPool
	ObjectPool_t858432606 * ___block1ParticlesPool_17;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block2ParticlesPool
	ObjectPool_t858432606 * ___block2ParticlesPool_18;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block3ParticlesPool
	ObjectPool_t858432606 * ___block3ParticlesPool_19;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block4ParticlesPool
	ObjectPool_t858432606 * ___block4ParticlesPool_20;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block5ParticlesPool
	ObjectPool_t858432606 * ___block5ParticlesPool_21;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::block6ParticlesPool
	ObjectPool_t858432606 * ___block6ParticlesPool_22;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::ballParticlesPool
	ObjectPool_t858432606 * ___ballParticlesPool_23;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::stoneParticlesPool
	ObjectPool_t858432606 * ___stoneParticlesPool_24;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::collectableParticlesPool
	ObjectPool_t858432606 * ___collectableParticlesPool_25;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::boosterSpawnParticlesPool
	ObjectPool_t858432606 * ___boosterSpawnParticlesPool_26;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::horizontalBombParticlesPool
	ObjectPool_t858432606 * ___horizontalBombParticlesPool_27;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::verticalBombParticlesPool
	ObjectPool_t858432606 * ___verticalBombParticlesPool_28;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::dynamiteParticlesPool
	ObjectPool_t858432606 * ___dynamiteParticlesPool_29;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::colorBombParticlesPool
	ObjectPool_t858432606 * ___colorBombParticlesPool_30;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Common.GamePools::iceParticlesPool
	ObjectPool_t858432606 * ___iceParticlesPool_31;
	// System.Collections.Generic.List`1<GameVanilla.Core.ObjectPool> GameVanilla.Game.Common.GamePools::blockPools
	List_1_t2330507348 * ___blockPools_32;

public:
	inline static int32_t get_offset_of_block1Pool_2() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block1Pool_2)); }
	inline ObjectPool_t858432606 * get_block1Pool_2() const { return ___block1Pool_2; }
	inline ObjectPool_t858432606 ** get_address_of_block1Pool_2() { return &___block1Pool_2; }
	inline void set_block1Pool_2(ObjectPool_t858432606 * value)
	{
		___block1Pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___block1Pool_2), value);
	}

	inline static int32_t get_offset_of_block2Pool_3() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block2Pool_3)); }
	inline ObjectPool_t858432606 * get_block2Pool_3() const { return ___block2Pool_3; }
	inline ObjectPool_t858432606 ** get_address_of_block2Pool_3() { return &___block2Pool_3; }
	inline void set_block2Pool_3(ObjectPool_t858432606 * value)
	{
		___block2Pool_3 = value;
		Il2CppCodeGenWriteBarrier((&___block2Pool_3), value);
	}

	inline static int32_t get_offset_of_block3Pool_4() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block3Pool_4)); }
	inline ObjectPool_t858432606 * get_block3Pool_4() const { return ___block3Pool_4; }
	inline ObjectPool_t858432606 ** get_address_of_block3Pool_4() { return &___block3Pool_4; }
	inline void set_block3Pool_4(ObjectPool_t858432606 * value)
	{
		___block3Pool_4 = value;
		Il2CppCodeGenWriteBarrier((&___block3Pool_4), value);
	}

	inline static int32_t get_offset_of_block4Pool_5() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block4Pool_5)); }
	inline ObjectPool_t858432606 * get_block4Pool_5() const { return ___block4Pool_5; }
	inline ObjectPool_t858432606 ** get_address_of_block4Pool_5() { return &___block4Pool_5; }
	inline void set_block4Pool_5(ObjectPool_t858432606 * value)
	{
		___block4Pool_5 = value;
		Il2CppCodeGenWriteBarrier((&___block4Pool_5), value);
	}

	inline static int32_t get_offset_of_block5Pool_6() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block5Pool_6)); }
	inline ObjectPool_t858432606 * get_block5Pool_6() const { return ___block5Pool_6; }
	inline ObjectPool_t858432606 ** get_address_of_block5Pool_6() { return &___block5Pool_6; }
	inline void set_block5Pool_6(ObjectPool_t858432606 * value)
	{
		___block5Pool_6 = value;
		Il2CppCodeGenWriteBarrier((&___block5Pool_6), value);
	}

	inline static int32_t get_offset_of_block6Pool_7() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block6Pool_7)); }
	inline ObjectPool_t858432606 * get_block6Pool_7() const { return ___block6Pool_7; }
	inline ObjectPool_t858432606 ** get_address_of_block6Pool_7() { return &___block6Pool_7; }
	inline void set_block6Pool_7(ObjectPool_t858432606 * value)
	{
		___block6Pool_7 = value;
		Il2CppCodeGenWriteBarrier((&___block6Pool_7), value);
	}

	inline static int32_t get_offset_of_emptyTilePool_8() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___emptyTilePool_8)); }
	inline ObjectPool_t858432606 * get_emptyTilePool_8() const { return ___emptyTilePool_8; }
	inline ObjectPool_t858432606 ** get_address_of_emptyTilePool_8() { return &___emptyTilePool_8; }
	inline void set_emptyTilePool_8(ObjectPool_t858432606 * value)
	{
		___emptyTilePool_8 = value;
		Il2CppCodeGenWriteBarrier((&___emptyTilePool_8), value);
	}

	inline static int32_t get_offset_of_ballPool_9() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___ballPool_9)); }
	inline ObjectPool_t858432606 * get_ballPool_9() const { return ___ballPool_9; }
	inline ObjectPool_t858432606 ** get_address_of_ballPool_9() { return &___ballPool_9; }
	inline void set_ballPool_9(ObjectPool_t858432606 * value)
	{
		___ballPool_9 = value;
		Il2CppCodeGenWriteBarrier((&___ballPool_9), value);
	}

	inline static int32_t get_offset_of_stonePool_10() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___stonePool_10)); }
	inline ObjectPool_t858432606 * get_stonePool_10() const { return ___stonePool_10; }
	inline ObjectPool_t858432606 ** get_address_of_stonePool_10() { return &___stonePool_10; }
	inline void set_stonePool_10(ObjectPool_t858432606 * value)
	{
		___stonePool_10 = value;
		Il2CppCodeGenWriteBarrier((&___stonePool_10), value);
	}

	inline static int32_t get_offset_of_collectablePool_11() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___collectablePool_11)); }
	inline ObjectPool_t858432606 * get_collectablePool_11() const { return ___collectablePool_11; }
	inline ObjectPool_t858432606 ** get_address_of_collectablePool_11() { return &___collectablePool_11; }
	inline void set_collectablePool_11(ObjectPool_t858432606 * value)
	{
		___collectablePool_11 = value;
		Il2CppCodeGenWriteBarrier((&___collectablePool_11), value);
	}

	inline static int32_t get_offset_of_horizontalBombPool_12() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___horizontalBombPool_12)); }
	inline ObjectPool_t858432606 * get_horizontalBombPool_12() const { return ___horizontalBombPool_12; }
	inline ObjectPool_t858432606 ** get_address_of_horizontalBombPool_12() { return &___horizontalBombPool_12; }
	inline void set_horizontalBombPool_12(ObjectPool_t858432606 * value)
	{
		___horizontalBombPool_12 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalBombPool_12), value);
	}

	inline static int32_t get_offset_of_verticalBombPool_13() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___verticalBombPool_13)); }
	inline ObjectPool_t858432606 * get_verticalBombPool_13() const { return ___verticalBombPool_13; }
	inline ObjectPool_t858432606 ** get_address_of_verticalBombPool_13() { return &___verticalBombPool_13; }
	inline void set_verticalBombPool_13(ObjectPool_t858432606 * value)
	{
		___verticalBombPool_13 = value;
		Il2CppCodeGenWriteBarrier((&___verticalBombPool_13), value);
	}

	inline static int32_t get_offset_of_dynamitePool_14() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___dynamitePool_14)); }
	inline ObjectPool_t858432606 * get_dynamitePool_14() const { return ___dynamitePool_14; }
	inline ObjectPool_t858432606 ** get_address_of_dynamitePool_14() { return &___dynamitePool_14; }
	inline void set_dynamitePool_14(ObjectPool_t858432606 * value)
	{
		___dynamitePool_14 = value;
		Il2CppCodeGenWriteBarrier((&___dynamitePool_14), value);
	}

	inline static int32_t get_offset_of_colorBombPool_15() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___colorBombPool_15)); }
	inline ObjectPool_t858432606 * get_colorBombPool_15() const { return ___colorBombPool_15; }
	inline ObjectPool_t858432606 ** get_address_of_colorBombPool_15() { return &___colorBombPool_15; }
	inline void set_colorBombPool_15(ObjectPool_t858432606 * value)
	{
		___colorBombPool_15 = value;
		Il2CppCodeGenWriteBarrier((&___colorBombPool_15), value);
	}

	inline static int32_t get_offset_of_icePool_16() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___icePool_16)); }
	inline ObjectPool_t858432606 * get_icePool_16() const { return ___icePool_16; }
	inline ObjectPool_t858432606 ** get_address_of_icePool_16() { return &___icePool_16; }
	inline void set_icePool_16(ObjectPool_t858432606 * value)
	{
		___icePool_16 = value;
		Il2CppCodeGenWriteBarrier((&___icePool_16), value);
	}

	inline static int32_t get_offset_of_block1ParticlesPool_17() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block1ParticlesPool_17)); }
	inline ObjectPool_t858432606 * get_block1ParticlesPool_17() const { return ___block1ParticlesPool_17; }
	inline ObjectPool_t858432606 ** get_address_of_block1ParticlesPool_17() { return &___block1ParticlesPool_17; }
	inline void set_block1ParticlesPool_17(ObjectPool_t858432606 * value)
	{
		___block1ParticlesPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___block1ParticlesPool_17), value);
	}

	inline static int32_t get_offset_of_block2ParticlesPool_18() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block2ParticlesPool_18)); }
	inline ObjectPool_t858432606 * get_block2ParticlesPool_18() const { return ___block2ParticlesPool_18; }
	inline ObjectPool_t858432606 ** get_address_of_block2ParticlesPool_18() { return &___block2ParticlesPool_18; }
	inline void set_block2ParticlesPool_18(ObjectPool_t858432606 * value)
	{
		___block2ParticlesPool_18 = value;
		Il2CppCodeGenWriteBarrier((&___block2ParticlesPool_18), value);
	}

	inline static int32_t get_offset_of_block3ParticlesPool_19() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block3ParticlesPool_19)); }
	inline ObjectPool_t858432606 * get_block3ParticlesPool_19() const { return ___block3ParticlesPool_19; }
	inline ObjectPool_t858432606 ** get_address_of_block3ParticlesPool_19() { return &___block3ParticlesPool_19; }
	inline void set_block3ParticlesPool_19(ObjectPool_t858432606 * value)
	{
		___block3ParticlesPool_19 = value;
		Il2CppCodeGenWriteBarrier((&___block3ParticlesPool_19), value);
	}

	inline static int32_t get_offset_of_block4ParticlesPool_20() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block4ParticlesPool_20)); }
	inline ObjectPool_t858432606 * get_block4ParticlesPool_20() const { return ___block4ParticlesPool_20; }
	inline ObjectPool_t858432606 ** get_address_of_block4ParticlesPool_20() { return &___block4ParticlesPool_20; }
	inline void set_block4ParticlesPool_20(ObjectPool_t858432606 * value)
	{
		___block4ParticlesPool_20 = value;
		Il2CppCodeGenWriteBarrier((&___block4ParticlesPool_20), value);
	}

	inline static int32_t get_offset_of_block5ParticlesPool_21() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block5ParticlesPool_21)); }
	inline ObjectPool_t858432606 * get_block5ParticlesPool_21() const { return ___block5ParticlesPool_21; }
	inline ObjectPool_t858432606 ** get_address_of_block5ParticlesPool_21() { return &___block5ParticlesPool_21; }
	inline void set_block5ParticlesPool_21(ObjectPool_t858432606 * value)
	{
		___block5ParticlesPool_21 = value;
		Il2CppCodeGenWriteBarrier((&___block5ParticlesPool_21), value);
	}

	inline static int32_t get_offset_of_block6ParticlesPool_22() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___block6ParticlesPool_22)); }
	inline ObjectPool_t858432606 * get_block6ParticlesPool_22() const { return ___block6ParticlesPool_22; }
	inline ObjectPool_t858432606 ** get_address_of_block6ParticlesPool_22() { return &___block6ParticlesPool_22; }
	inline void set_block6ParticlesPool_22(ObjectPool_t858432606 * value)
	{
		___block6ParticlesPool_22 = value;
		Il2CppCodeGenWriteBarrier((&___block6ParticlesPool_22), value);
	}

	inline static int32_t get_offset_of_ballParticlesPool_23() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___ballParticlesPool_23)); }
	inline ObjectPool_t858432606 * get_ballParticlesPool_23() const { return ___ballParticlesPool_23; }
	inline ObjectPool_t858432606 ** get_address_of_ballParticlesPool_23() { return &___ballParticlesPool_23; }
	inline void set_ballParticlesPool_23(ObjectPool_t858432606 * value)
	{
		___ballParticlesPool_23 = value;
		Il2CppCodeGenWriteBarrier((&___ballParticlesPool_23), value);
	}

	inline static int32_t get_offset_of_stoneParticlesPool_24() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___stoneParticlesPool_24)); }
	inline ObjectPool_t858432606 * get_stoneParticlesPool_24() const { return ___stoneParticlesPool_24; }
	inline ObjectPool_t858432606 ** get_address_of_stoneParticlesPool_24() { return &___stoneParticlesPool_24; }
	inline void set_stoneParticlesPool_24(ObjectPool_t858432606 * value)
	{
		___stoneParticlesPool_24 = value;
		Il2CppCodeGenWriteBarrier((&___stoneParticlesPool_24), value);
	}

	inline static int32_t get_offset_of_collectableParticlesPool_25() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___collectableParticlesPool_25)); }
	inline ObjectPool_t858432606 * get_collectableParticlesPool_25() const { return ___collectableParticlesPool_25; }
	inline ObjectPool_t858432606 ** get_address_of_collectableParticlesPool_25() { return &___collectableParticlesPool_25; }
	inline void set_collectableParticlesPool_25(ObjectPool_t858432606 * value)
	{
		___collectableParticlesPool_25 = value;
		Il2CppCodeGenWriteBarrier((&___collectableParticlesPool_25), value);
	}

	inline static int32_t get_offset_of_boosterSpawnParticlesPool_26() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___boosterSpawnParticlesPool_26)); }
	inline ObjectPool_t858432606 * get_boosterSpawnParticlesPool_26() const { return ___boosterSpawnParticlesPool_26; }
	inline ObjectPool_t858432606 ** get_address_of_boosterSpawnParticlesPool_26() { return &___boosterSpawnParticlesPool_26; }
	inline void set_boosterSpawnParticlesPool_26(ObjectPool_t858432606 * value)
	{
		___boosterSpawnParticlesPool_26 = value;
		Il2CppCodeGenWriteBarrier((&___boosterSpawnParticlesPool_26), value);
	}

	inline static int32_t get_offset_of_horizontalBombParticlesPool_27() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___horizontalBombParticlesPool_27)); }
	inline ObjectPool_t858432606 * get_horizontalBombParticlesPool_27() const { return ___horizontalBombParticlesPool_27; }
	inline ObjectPool_t858432606 ** get_address_of_horizontalBombParticlesPool_27() { return &___horizontalBombParticlesPool_27; }
	inline void set_horizontalBombParticlesPool_27(ObjectPool_t858432606 * value)
	{
		___horizontalBombParticlesPool_27 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalBombParticlesPool_27), value);
	}

	inline static int32_t get_offset_of_verticalBombParticlesPool_28() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___verticalBombParticlesPool_28)); }
	inline ObjectPool_t858432606 * get_verticalBombParticlesPool_28() const { return ___verticalBombParticlesPool_28; }
	inline ObjectPool_t858432606 ** get_address_of_verticalBombParticlesPool_28() { return &___verticalBombParticlesPool_28; }
	inline void set_verticalBombParticlesPool_28(ObjectPool_t858432606 * value)
	{
		___verticalBombParticlesPool_28 = value;
		Il2CppCodeGenWriteBarrier((&___verticalBombParticlesPool_28), value);
	}

	inline static int32_t get_offset_of_dynamiteParticlesPool_29() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___dynamiteParticlesPool_29)); }
	inline ObjectPool_t858432606 * get_dynamiteParticlesPool_29() const { return ___dynamiteParticlesPool_29; }
	inline ObjectPool_t858432606 ** get_address_of_dynamiteParticlesPool_29() { return &___dynamiteParticlesPool_29; }
	inline void set_dynamiteParticlesPool_29(ObjectPool_t858432606 * value)
	{
		___dynamiteParticlesPool_29 = value;
		Il2CppCodeGenWriteBarrier((&___dynamiteParticlesPool_29), value);
	}

	inline static int32_t get_offset_of_colorBombParticlesPool_30() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___colorBombParticlesPool_30)); }
	inline ObjectPool_t858432606 * get_colorBombParticlesPool_30() const { return ___colorBombParticlesPool_30; }
	inline ObjectPool_t858432606 ** get_address_of_colorBombParticlesPool_30() { return &___colorBombParticlesPool_30; }
	inline void set_colorBombParticlesPool_30(ObjectPool_t858432606 * value)
	{
		___colorBombParticlesPool_30 = value;
		Il2CppCodeGenWriteBarrier((&___colorBombParticlesPool_30), value);
	}

	inline static int32_t get_offset_of_iceParticlesPool_31() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___iceParticlesPool_31)); }
	inline ObjectPool_t858432606 * get_iceParticlesPool_31() const { return ___iceParticlesPool_31; }
	inline ObjectPool_t858432606 ** get_address_of_iceParticlesPool_31() { return &___iceParticlesPool_31; }
	inline void set_iceParticlesPool_31(ObjectPool_t858432606 * value)
	{
		___iceParticlesPool_31 = value;
		Il2CppCodeGenWriteBarrier((&___iceParticlesPool_31), value);
	}

	inline static int32_t get_offset_of_blockPools_32() { return static_cast<int32_t>(offsetof(GamePools_t2964324386, ___blockPools_32)); }
	inline List_1_t2330507348 * get_blockPools_32() const { return ___blockPools_32; }
	inline List_1_t2330507348 ** get_address_of_blockPools_32() { return &___blockPools_32; }
	inline void set_blockPools_32(List_1_t2330507348 * value)
	{
		___blockPools_32 = value;
		Il2CppCodeGenWriteBarrier((&___blockPools_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPOOLS_T2964324386_H
#ifndef INGAMESETTINGSPOPUP_T2985363043_H
#define INGAMESETTINGSPOPUP_T2985363043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.InGameSettingsPopup
struct  InGameSettingsPopup_t2985363043  : public Popup_t1063720813
{
public:
	// GameVanilla.Core.AnimatedButton GameVanilla.Game.Popups.InGameSettingsPopup::soundButton
	AnimatedButton_t997585140 * ___soundButton_6;
	// GameVanilla.Core.AnimatedButton GameVanilla.Game.Popups.InGameSettingsPopup::musicButton
	AnimatedButton_t997585140 * ___musicButton_7;

public:
	inline static int32_t get_offset_of_soundButton_6() { return static_cast<int32_t>(offsetof(InGameSettingsPopup_t2985363043, ___soundButton_6)); }
	inline AnimatedButton_t997585140 * get_soundButton_6() const { return ___soundButton_6; }
	inline AnimatedButton_t997585140 ** get_address_of_soundButton_6() { return &___soundButton_6; }
	inline void set_soundButton_6(AnimatedButton_t997585140 * value)
	{
		___soundButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___soundButton_6), value);
	}

	inline static int32_t get_offset_of_musicButton_7() { return static_cast<int32_t>(offsetof(InGameSettingsPopup_t2985363043, ___musicButton_7)); }
	inline AnimatedButton_t997585140 * get_musicButton_7() const { return ___musicButton_7; }
	inline AnimatedButton_t997585140 ** get_address_of_musicButton_7() { return &___musicButton_7; }
	inline void set_musicButton_7(AnimatedButton_t997585140 * value)
	{
		___musicButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___musicButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMESETTINGSPOPUP_T2985363043_H
#ifndef LEVELGOALSPOPUP_T649240731_H
#define LEVELGOALSPOPUP_T649240731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.LevelGoalsPopup
struct  LevelGoalsPopup_t649240731  : public Popup_t1063720813
{
public:
	// UnityEngine.Sprite GameVanilla.Game.Popups.LevelGoalsPopup::girlAvatarSprite
	Sprite_t280657092 * ___girlAvatarSprite_6;
	// UnityEngine.Sprite GameVanilla.Game.Popups.LevelGoalsPopup::boyAvatarSprite
	Sprite_t280657092 * ___boyAvatarSprite_7;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.LevelGoalsPopup::avatarImage
	Image_t2670269651 * ___avatarImage_8;
	// UnityEngine.GameObject GameVanilla.Game.Popups.LevelGoalsPopup::goalGroup
	GameObject_t1113636619 * ___goalGroup_9;
	// UnityEngine.GameObject GameVanilla.Game.Popups.LevelGoalsPopup::goalPrefab
	GameObject_t1113636619 * ___goalPrefab_10;
	// UnityEngine.GameObject GameVanilla.Game.Popups.LevelGoalsPopup::goalHeadline
	GameObject_t1113636619 * ___goalHeadline_11;
	// UnityEngine.GameObject GameVanilla.Game.Popups.LevelGoalsPopup::scoreGoalHeadline
	GameObject_t1113636619 * ___scoreGoalHeadline_12;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.LevelGoalsPopup::scoreGoalAmountText
	Text_t1901882714 * ___scoreGoalAmountText_13;

public:
	inline static int32_t get_offset_of_girlAvatarSprite_6() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___girlAvatarSprite_6)); }
	inline Sprite_t280657092 * get_girlAvatarSprite_6() const { return ___girlAvatarSprite_6; }
	inline Sprite_t280657092 ** get_address_of_girlAvatarSprite_6() { return &___girlAvatarSprite_6; }
	inline void set_girlAvatarSprite_6(Sprite_t280657092 * value)
	{
		___girlAvatarSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___girlAvatarSprite_6), value);
	}

	inline static int32_t get_offset_of_boyAvatarSprite_7() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___boyAvatarSprite_7)); }
	inline Sprite_t280657092 * get_boyAvatarSprite_7() const { return ___boyAvatarSprite_7; }
	inline Sprite_t280657092 ** get_address_of_boyAvatarSprite_7() { return &___boyAvatarSprite_7; }
	inline void set_boyAvatarSprite_7(Sprite_t280657092 * value)
	{
		___boyAvatarSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___boyAvatarSprite_7), value);
	}

	inline static int32_t get_offset_of_avatarImage_8() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___avatarImage_8)); }
	inline Image_t2670269651 * get_avatarImage_8() const { return ___avatarImage_8; }
	inline Image_t2670269651 ** get_address_of_avatarImage_8() { return &___avatarImage_8; }
	inline void set_avatarImage_8(Image_t2670269651 * value)
	{
		___avatarImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___avatarImage_8), value);
	}

	inline static int32_t get_offset_of_goalGroup_9() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___goalGroup_9)); }
	inline GameObject_t1113636619 * get_goalGroup_9() const { return ___goalGroup_9; }
	inline GameObject_t1113636619 ** get_address_of_goalGroup_9() { return &___goalGroup_9; }
	inline void set_goalGroup_9(GameObject_t1113636619 * value)
	{
		___goalGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&___goalGroup_9), value);
	}

	inline static int32_t get_offset_of_goalPrefab_10() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___goalPrefab_10)); }
	inline GameObject_t1113636619 * get_goalPrefab_10() const { return ___goalPrefab_10; }
	inline GameObject_t1113636619 ** get_address_of_goalPrefab_10() { return &___goalPrefab_10; }
	inline void set_goalPrefab_10(GameObject_t1113636619 * value)
	{
		___goalPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___goalPrefab_10), value);
	}

	inline static int32_t get_offset_of_goalHeadline_11() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___goalHeadline_11)); }
	inline GameObject_t1113636619 * get_goalHeadline_11() const { return ___goalHeadline_11; }
	inline GameObject_t1113636619 ** get_address_of_goalHeadline_11() { return &___goalHeadline_11; }
	inline void set_goalHeadline_11(GameObject_t1113636619 * value)
	{
		___goalHeadline_11 = value;
		Il2CppCodeGenWriteBarrier((&___goalHeadline_11), value);
	}

	inline static int32_t get_offset_of_scoreGoalHeadline_12() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___scoreGoalHeadline_12)); }
	inline GameObject_t1113636619 * get_scoreGoalHeadline_12() const { return ___scoreGoalHeadline_12; }
	inline GameObject_t1113636619 ** get_address_of_scoreGoalHeadline_12() { return &___scoreGoalHeadline_12; }
	inline void set_scoreGoalHeadline_12(GameObject_t1113636619 * value)
	{
		___scoreGoalHeadline_12 = value;
		Il2CppCodeGenWriteBarrier((&___scoreGoalHeadline_12), value);
	}

	inline static int32_t get_offset_of_scoreGoalAmountText_13() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731, ___scoreGoalAmountText_13)); }
	inline Text_t1901882714 * get_scoreGoalAmountText_13() const { return ___scoreGoalAmountText_13; }
	inline Text_t1901882714 ** get_address_of_scoreGoalAmountText_13() { return &___scoreGoalAmountText_13; }
	inline void set_scoreGoalAmountText_13(Text_t1901882714 * value)
	{
		___scoreGoalAmountText_13 = value;
		Il2CppCodeGenWriteBarrier((&___scoreGoalAmountText_13), value);
	}
};

struct LevelGoalsPopup_t649240731_StaticFields
{
public:
	// System.Predicate`1<GameVanilla.Game.Common.Goal> GameVanilla.Game.Popups.LevelGoalsPopup::<>f__am$cache0
	Predicate_1_t2324985759 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(LevelGoalsPopup_t649240731_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Predicate_1_t2324985759 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Predicate_1_t2324985759 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Predicate_1_t2324985759 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELGOALSPOPUP_T649240731_H
#ifndef STARTGAMEPOPUP_T4281035199_H
#define STARTGAMEPOPUP_T4281035199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.StartGamePopup
struct  StartGamePopup_t4281035199  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GameVanilla.Game.Popups.StartGamePopup::levelText
	Text_t1901882714 * ___levelText_6;
	// UnityEngine.Sprite GameVanilla.Game.Popups.StartGamePopup::enabledStarSprite
	Sprite_t280657092 * ___enabledStarSprite_7;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.StartGamePopup::star1Image
	Image_t2670269651 * ___star1Image_8;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.StartGamePopup::star2Image
	Image_t2670269651 * ___star2Image_9;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.StartGamePopup::star3Image
	Image_t2670269651 * ___star3Image_10;
	// UnityEngine.GameObject GameVanilla.Game.Popups.StartGamePopup::goalPrefab
	GameObject_t1113636619 * ___goalPrefab_11;
	// UnityEngine.GameObject GameVanilla.Game.Popups.StartGamePopup::goalGroup
	GameObject_t1113636619 * ___goalGroup_12;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.StartGamePopup::goalText
	Text_t1901882714 * ___goalText_13;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.StartGamePopup::scoreGoalTitleText
	Text_t1901882714 * ___scoreGoalTitleText_14;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.StartGamePopup::scoreGoalAmountText
	Text_t1901882714 * ___scoreGoalAmountText_15;
	// System.Int32 GameVanilla.Game.Popups.StartGamePopup::numLevel
	int32_t ___numLevel_16;

public:
	inline static int32_t get_offset_of_levelText_6() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___levelText_6)); }
	inline Text_t1901882714 * get_levelText_6() const { return ___levelText_6; }
	inline Text_t1901882714 ** get_address_of_levelText_6() { return &___levelText_6; }
	inline void set_levelText_6(Text_t1901882714 * value)
	{
		___levelText_6 = value;
		Il2CppCodeGenWriteBarrier((&___levelText_6), value);
	}

	inline static int32_t get_offset_of_enabledStarSprite_7() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___enabledStarSprite_7)); }
	inline Sprite_t280657092 * get_enabledStarSprite_7() const { return ___enabledStarSprite_7; }
	inline Sprite_t280657092 ** get_address_of_enabledStarSprite_7() { return &___enabledStarSprite_7; }
	inline void set_enabledStarSprite_7(Sprite_t280657092 * value)
	{
		___enabledStarSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___enabledStarSprite_7), value);
	}

	inline static int32_t get_offset_of_star1Image_8() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___star1Image_8)); }
	inline Image_t2670269651 * get_star1Image_8() const { return ___star1Image_8; }
	inline Image_t2670269651 ** get_address_of_star1Image_8() { return &___star1Image_8; }
	inline void set_star1Image_8(Image_t2670269651 * value)
	{
		___star1Image_8 = value;
		Il2CppCodeGenWriteBarrier((&___star1Image_8), value);
	}

	inline static int32_t get_offset_of_star2Image_9() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___star2Image_9)); }
	inline Image_t2670269651 * get_star2Image_9() const { return ___star2Image_9; }
	inline Image_t2670269651 ** get_address_of_star2Image_9() { return &___star2Image_9; }
	inline void set_star2Image_9(Image_t2670269651 * value)
	{
		___star2Image_9 = value;
		Il2CppCodeGenWriteBarrier((&___star2Image_9), value);
	}

	inline static int32_t get_offset_of_star3Image_10() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___star3Image_10)); }
	inline Image_t2670269651 * get_star3Image_10() const { return ___star3Image_10; }
	inline Image_t2670269651 ** get_address_of_star3Image_10() { return &___star3Image_10; }
	inline void set_star3Image_10(Image_t2670269651 * value)
	{
		___star3Image_10 = value;
		Il2CppCodeGenWriteBarrier((&___star3Image_10), value);
	}

	inline static int32_t get_offset_of_goalPrefab_11() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___goalPrefab_11)); }
	inline GameObject_t1113636619 * get_goalPrefab_11() const { return ___goalPrefab_11; }
	inline GameObject_t1113636619 ** get_address_of_goalPrefab_11() { return &___goalPrefab_11; }
	inline void set_goalPrefab_11(GameObject_t1113636619 * value)
	{
		___goalPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___goalPrefab_11), value);
	}

	inline static int32_t get_offset_of_goalGroup_12() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___goalGroup_12)); }
	inline GameObject_t1113636619 * get_goalGroup_12() const { return ___goalGroup_12; }
	inline GameObject_t1113636619 ** get_address_of_goalGroup_12() { return &___goalGroup_12; }
	inline void set_goalGroup_12(GameObject_t1113636619 * value)
	{
		___goalGroup_12 = value;
		Il2CppCodeGenWriteBarrier((&___goalGroup_12), value);
	}

	inline static int32_t get_offset_of_goalText_13() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___goalText_13)); }
	inline Text_t1901882714 * get_goalText_13() const { return ___goalText_13; }
	inline Text_t1901882714 ** get_address_of_goalText_13() { return &___goalText_13; }
	inline void set_goalText_13(Text_t1901882714 * value)
	{
		___goalText_13 = value;
		Il2CppCodeGenWriteBarrier((&___goalText_13), value);
	}

	inline static int32_t get_offset_of_scoreGoalTitleText_14() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___scoreGoalTitleText_14)); }
	inline Text_t1901882714 * get_scoreGoalTitleText_14() const { return ___scoreGoalTitleText_14; }
	inline Text_t1901882714 ** get_address_of_scoreGoalTitleText_14() { return &___scoreGoalTitleText_14; }
	inline void set_scoreGoalTitleText_14(Text_t1901882714 * value)
	{
		___scoreGoalTitleText_14 = value;
		Il2CppCodeGenWriteBarrier((&___scoreGoalTitleText_14), value);
	}

	inline static int32_t get_offset_of_scoreGoalAmountText_15() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___scoreGoalAmountText_15)); }
	inline Text_t1901882714 * get_scoreGoalAmountText_15() const { return ___scoreGoalAmountText_15; }
	inline Text_t1901882714 ** get_address_of_scoreGoalAmountText_15() { return &___scoreGoalAmountText_15; }
	inline void set_scoreGoalAmountText_15(Text_t1901882714 * value)
	{
		___scoreGoalAmountText_15 = value;
		Il2CppCodeGenWriteBarrier((&___scoreGoalAmountText_15), value);
	}

	inline static int32_t get_offset_of_numLevel_16() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199, ___numLevel_16)); }
	inline int32_t get_numLevel_16() const { return ___numLevel_16; }
	inline int32_t* get_address_of_numLevel_16() { return &___numLevel_16; }
	inline void set_numLevel_16(int32_t value)
	{
		___numLevel_16 = value;
	}
};

struct StartGamePopup_t4281035199_StaticFields
{
public:
	// System.Predicate`1<GameVanilla.Game.Common.Goal> GameVanilla.Game.Popups.StartGamePopup::<>f__am$cache0
	Predicate_1_t2324985759 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(StartGamePopup_t4281035199_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Predicate_1_t2324985759 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Predicate_1_t2324985759 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Predicate_1_t2324985759 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTGAMEPOPUP_T4281035199_H
#ifndef REGENLEVELPOPUP_T1421666675_H
#define REGENLEVELPOPUP_T1421666675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.RegenLevelPopup
struct  RegenLevelPopup_t1421666675  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GameVanilla.Game.Popups.RegenLevelPopup::text
	Text_t1901882714 * ___text_6;

public:
	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(RegenLevelPopup_t1421666675, ___text_6)); }
	inline Text_t1901882714 * get_text_6() const { return ___text_6; }
	inline Text_t1901882714 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_t1901882714 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((&___text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGENLEVELPOPUP_T1421666675_H
#ifndef BUYCOINSPOPUP_T414112668_H
#define BUYCOINSPOPUP_T414112668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BuyCoinsPopup
struct  BuyCoinsPopup_t414112668  : public Popup_t1063720813
{
public:
	// UnityEngine.GameObject GameVanilla.Game.Popups.BuyCoinsPopup::iapItemsParent
	GameObject_t1113636619 * ___iapItemsParent_6;
	// UnityEngine.GameObject GameVanilla.Game.Popups.BuyCoinsPopup::iapRowPrefab
	GameObject_t1113636619 * ___iapRowPrefab_7;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyCoinsPopup::numCoinsText
	Text_t1901882714 * ___numCoinsText_8;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.BuyCoinsPopup::coinsParticles
	ParticleSystem_t1800779281 * ___coinsParticles_9;
	// GameVanilla.Core.Popup GameVanilla.Game.Popups.BuyCoinsPopup::loadingPopup
	Popup_t1063720813 * ___loadingPopup_10;

public:
	inline static int32_t get_offset_of_iapItemsParent_6() { return static_cast<int32_t>(offsetof(BuyCoinsPopup_t414112668, ___iapItemsParent_6)); }
	inline GameObject_t1113636619 * get_iapItemsParent_6() const { return ___iapItemsParent_6; }
	inline GameObject_t1113636619 ** get_address_of_iapItemsParent_6() { return &___iapItemsParent_6; }
	inline void set_iapItemsParent_6(GameObject_t1113636619 * value)
	{
		___iapItemsParent_6 = value;
		Il2CppCodeGenWriteBarrier((&___iapItemsParent_6), value);
	}

	inline static int32_t get_offset_of_iapRowPrefab_7() { return static_cast<int32_t>(offsetof(BuyCoinsPopup_t414112668, ___iapRowPrefab_7)); }
	inline GameObject_t1113636619 * get_iapRowPrefab_7() const { return ___iapRowPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_iapRowPrefab_7() { return &___iapRowPrefab_7; }
	inline void set_iapRowPrefab_7(GameObject_t1113636619 * value)
	{
		___iapRowPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___iapRowPrefab_7), value);
	}

	inline static int32_t get_offset_of_numCoinsText_8() { return static_cast<int32_t>(offsetof(BuyCoinsPopup_t414112668, ___numCoinsText_8)); }
	inline Text_t1901882714 * get_numCoinsText_8() const { return ___numCoinsText_8; }
	inline Text_t1901882714 ** get_address_of_numCoinsText_8() { return &___numCoinsText_8; }
	inline void set_numCoinsText_8(Text_t1901882714 * value)
	{
		___numCoinsText_8 = value;
		Il2CppCodeGenWriteBarrier((&___numCoinsText_8), value);
	}

	inline static int32_t get_offset_of_coinsParticles_9() { return static_cast<int32_t>(offsetof(BuyCoinsPopup_t414112668, ___coinsParticles_9)); }
	inline ParticleSystem_t1800779281 * get_coinsParticles_9() const { return ___coinsParticles_9; }
	inline ParticleSystem_t1800779281 ** get_address_of_coinsParticles_9() { return &___coinsParticles_9; }
	inline void set_coinsParticles_9(ParticleSystem_t1800779281 * value)
	{
		___coinsParticles_9 = value;
		Il2CppCodeGenWriteBarrier((&___coinsParticles_9), value);
	}

	inline static int32_t get_offset_of_loadingPopup_10() { return static_cast<int32_t>(offsetof(BuyCoinsPopup_t414112668, ___loadingPopup_10)); }
	inline Popup_t1063720813 * get_loadingPopup_10() const { return ___loadingPopup_10; }
	inline Popup_t1063720813 ** get_address_of_loadingPopup_10() { return &___loadingPopup_10; }
	inline void set_loadingPopup_10(Popup_t1063720813 * value)
	{
		___loadingPopup_10 = value;
		Il2CppCodeGenWriteBarrier((&___loadingPopup_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYCOINSPOPUP_T414112668_H
#ifndef GAMESCENE_T3472316885_H
#define GAMESCENE_T3472316885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.GameScene
struct  GameScene_t3472316885  : public BaseScene_t2918414559
{
public:
	// GameVanilla.Game.Common.GamePools GameVanilla.Game.Scenes.GameScene::gamePools
	GamePools_t2964324386 * ___gamePools_5;
	// GameVanilla.Game.UI.GameUi GameVanilla.Game.Scenes.GameScene::gameUi
	GameUi_t3170959104 * ___gameUi_6;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.GameScene::goalPrefab
	GameObject_t1113636619 * ___goalPrefab_7;
	// GameVanilla.Core.ObjectPool GameVanilla.Game.Scenes.GameScene::scoreTextPool
	ObjectPool_t858432606 * ___scoreTextPool_8;
	// System.Single GameVanilla.Game.Scenes.GameScene::blockFallSpeed
	float ___blockFallSpeed_9;
	// System.Single GameVanilla.Game.Scenes.GameScene::horizontalSpacing
	float ___horizontalSpacing_10;
	// System.Single GameVanilla.Game.Scenes.GameScene::verticalSpacing
	float ___verticalSpacing_11;
	// UnityEngine.Transform GameVanilla.Game.Scenes.GameScene::levelLocation
	Transform_t3600365921 * ___levelLocation_12;
	// UnityEngine.Sprite GameVanilla.Game.Scenes.GameScene::backgroundSprite
	Sprite_t280657092 * ___backgroundSprite_13;
	// UnityEngine.Color GameVanilla.Game.Scenes.GameScene::backgroundColor
	Color_t2555686324  ___backgroundColor_14;
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> GameVanilla.Game.Scenes.GameScene::gameSounds
	List_1_t857997111 * ___gameSounds_15;
	// GameVanilla.Game.Common.Level GameVanilla.Game.Scenes.GameScene::level
	Level_t3869101417 * ___level_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene::tileEntities
	List_1_t2585711361 * ___tileEntities_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene::blockers
	List_1_t2585711361 * ___blockers_18;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> GameVanilla.Game.Scenes.GameScene::tilePositions
	List_1_t3628304265 * ___tilePositions_19;
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Scenes.GameScene::buyBooster1Button
	BuyBoosterButton_t1892892637 * ___buyBooster1Button_20;
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Scenes.GameScene::buyBooster2Button
	BuyBoosterButton_t1892892637 * ___buyBooster2Button_21;
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Scenes.GameScene::buyBooster3Button
	BuyBoosterButton_t1892892637 * ___buyBooster3Button_22;
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Scenes.GameScene::buyBooster4Button
	BuyBoosterButton_t1892892637 * ___buyBooster4Button_23;
	// UnityEngine.UI.Image GameVanilla.Game.Scenes.GameScene::ingameBoosterPanel
	Image_t2670269651 * ___ingameBoosterPanel_24;
	// UnityEngine.UI.Text GameVanilla.Game.Scenes.GameScene::ingameBoosterText
	Text_t1901882714 * ___ingameBoosterText_25;
	// GameVanilla.Game.Common.GameState GameVanilla.Game.Scenes.GameScene::gameState
	GameState_t1141404161 * ___gameState_26;
	// GameVanilla.Game.Common.GameConfiguration GameVanilla.Game.Scenes.GameScene::gameConfig
	GameConfiguration_t2964563698 * ___gameConfig_27;
	// System.Boolean GameVanilla.Game.Scenes.GameScene::gameStarted
	bool ___gameStarted_28;
	// System.Boolean GameVanilla.Game.Scenes.GameScene::gameFinished
	bool ___gameFinished_29;
	// System.Single GameVanilla.Game.Scenes.GameScene::accTime
	float ___accTime_30;
	// System.Single GameVanilla.Game.Scenes.GameScene::blockWidth
	float ___blockWidth_31;
	// System.Single GameVanilla.Game.Scenes.GameScene::blockHeight
	float ___blockHeight_32;
	// System.Boolean GameVanilla.Game.Scenes.GameScene::suggestedMatch
	bool ___suggestedMatch_33;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameVanilla.Game.Scenes.GameScene::suggestedMatchBlocks
	List_1_t2585711361 * ___suggestedMatchBlocks_34;
	// System.Int32 GameVanilla.Game.Scenes.GameScene::currentLimit
	int32_t ___currentLimit_35;
	// System.Boolean GameVanilla.Game.Scenes.GameScene::currentlyAwardingBoosters
	bool ___currentlyAwardingBoosters_36;
	// UnityEngine.Coroutine GameVanilla.Game.Scenes.GameScene::countdownCoroutine
	Coroutine_t3829159415 * ___countdownCoroutine_37;
	// System.Boolean GameVanilla.Game.Scenes.GameScene::boosterMode
	bool ___boosterMode_38;
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Scenes.GameScene::currentBoosterButton
	BuyBoosterButton_t1892892637 * ___currentBoosterButton_39;
	// System.Boolean GameVanilla.Game.Scenes.GameScene::applyingPenalty
	bool ___applyingPenalty_42;
	// System.Int32 GameVanilla.Game.Scenes.GameScene::ingameBoosterBgTweenId
	int32_t ___ingameBoosterBgTweenId_43;
	// System.Int32 GameVanilla.Game.Scenes.GameScene::generatedCollectables
	int32_t ___generatedCollectables_44;
	// System.Int32 GameVanilla.Game.Scenes.GameScene::neededCollectables
	int32_t ___neededCollectables_45;
	// GhostSkillInfo GameVanilla.Game.Scenes.GameScene::ghostSkillInfo
	GhostSkillInfo_t3192096645 * ___ghostSkillInfo_46;
	// UnityEngine.UI.Text GameVanilla.Game.Scenes.GameScene::scoreText
	Text_t1901882714 * ___scoreText_47;
	// UnityEngine.UI.Text GameVanilla.Game.Scenes.GameScene::goldText
	Text_t1901882714 * ___goldText_48;
	// UnityEngine.LineRenderer GameVanilla.Game.Scenes.GameScene::lineRenderer
	LineRenderer_t3154350270 * ___lineRenderer_49;

public:
	inline static int32_t get_offset_of_gamePools_5() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gamePools_5)); }
	inline GamePools_t2964324386 * get_gamePools_5() const { return ___gamePools_5; }
	inline GamePools_t2964324386 ** get_address_of_gamePools_5() { return &___gamePools_5; }
	inline void set_gamePools_5(GamePools_t2964324386 * value)
	{
		___gamePools_5 = value;
		Il2CppCodeGenWriteBarrier((&___gamePools_5), value);
	}

	inline static int32_t get_offset_of_gameUi_6() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gameUi_6)); }
	inline GameUi_t3170959104 * get_gameUi_6() const { return ___gameUi_6; }
	inline GameUi_t3170959104 ** get_address_of_gameUi_6() { return &___gameUi_6; }
	inline void set_gameUi_6(GameUi_t3170959104 * value)
	{
		___gameUi_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameUi_6), value);
	}

	inline static int32_t get_offset_of_goalPrefab_7() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___goalPrefab_7)); }
	inline GameObject_t1113636619 * get_goalPrefab_7() const { return ___goalPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_goalPrefab_7() { return &___goalPrefab_7; }
	inline void set_goalPrefab_7(GameObject_t1113636619 * value)
	{
		___goalPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___goalPrefab_7), value);
	}

	inline static int32_t get_offset_of_scoreTextPool_8() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___scoreTextPool_8)); }
	inline ObjectPool_t858432606 * get_scoreTextPool_8() const { return ___scoreTextPool_8; }
	inline ObjectPool_t858432606 ** get_address_of_scoreTextPool_8() { return &___scoreTextPool_8; }
	inline void set_scoreTextPool_8(ObjectPool_t858432606 * value)
	{
		___scoreTextPool_8 = value;
		Il2CppCodeGenWriteBarrier((&___scoreTextPool_8), value);
	}

	inline static int32_t get_offset_of_blockFallSpeed_9() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___blockFallSpeed_9)); }
	inline float get_blockFallSpeed_9() const { return ___blockFallSpeed_9; }
	inline float* get_address_of_blockFallSpeed_9() { return &___blockFallSpeed_9; }
	inline void set_blockFallSpeed_9(float value)
	{
		___blockFallSpeed_9 = value;
	}

	inline static int32_t get_offset_of_horizontalSpacing_10() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___horizontalSpacing_10)); }
	inline float get_horizontalSpacing_10() const { return ___horizontalSpacing_10; }
	inline float* get_address_of_horizontalSpacing_10() { return &___horizontalSpacing_10; }
	inline void set_horizontalSpacing_10(float value)
	{
		___horizontalSpacing_10 = value;
	}

	inline static int32_t get_offset_of_verticalSpacing_11() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___verticalSpacing_11)); }
	inline float get_verticalSpacing_11() const { return ___verticalSpacing_11; }
	inline float* get_address_of_verticalSpacing_11() { return &___verticalSpacing_11; }
	inline void set_verticalSpacing_11(float value)
	{
		___verticalSpacing_11 = value;
	}

	inline static int32_t get_offset_of_levelLocation_12() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___levelLocation_12)); }
	inline Transform_t3600365921 * get_levelLocation_12() const { return ___levelLocation_12; }
	inline Transform_t3600365921 ** get_address_of_levelLocation_12() { return &___levelLocation_12; }
	inline void set_levelLocation_12(Transform_t3600365921 * value)
	{
		___levelLocation_12 = value;
		Il2CppCodeGenWriteBarrier((&___levelLocation_12), value);
	}

	inline static int32_t get_offset_of_backgroundSprite_13() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___backgroundSprite_13)); }
	inline Sprite_t280657092 * get_backgroundSprite_13() const { return ___backgroundSprite_13; }
	inline Sprite_t280657092 ** get_address_of_backgroundSprite_13() { return &___backgroundSprite_13; }
	inline void set_backgroundSprite_13(Sprite_t280657092 * value)
	{
		___backgroundSprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSprite_13), value);
	}

	inline static int32_t get_offset_of_backgroundColor_14() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___backgroundColor_14)); }
	inline Color_t2555686324  get_backgroundColor_14() const { return ___backgroundColor_14; }
	inline Color_t2555686324 * get_address_of_backgroundColor_14() { return &___backgroundColor_14; }
	inline void set_backgroundColor_14(Color_t2555686324  value)
	{
		___backgroundColor_14 = value;
	}

	inline static int32_t get_offset_of_gameSounds_15() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gameSounds_15)); }
	inline List_1_t857997111 * get_gameSounds_15() const { return ___gameSounds_15; }
	inline List_1_t857997111 ** get_address_of_gameSounds_15() { return &___gameSounds_15; }
	inline void set_gameSounds_15(List_1_t857997111 * value)
	{
		___gameSounds_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameSounds_15), value);
	}

	inline static int32_t get_offset_of_level_16() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___level_16)); }
	inline Level_t3869101417 * get_level_16() const { return ___level_16; }
	inline Level_t3869101417 ** get_address_of_level_16() { return &___level_16; }
	inline void set_level_16(Level_t3869101417 * value)
	{
		___level_16 = value;
		Il2CppCodeGenWriteBarrier((&___level_16), value);
	}

	inline static int32_t get_offset_of_tileEntities_17() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___tileEntities_17)); }
	inline List_1_t2585711361 * get_tileEntities_17() const { return ___tileEntities_17; }
	inline List_1_t2585711361 ** get_address_of_tileEntities_17() { return &___tileEntities_17; }
	inline void set_tileEntities_17(List_1_t2585711361 * value)
	{
		___tileEntities_17 = value;
		Il2CppCodeGenWriteBarrier((&___tileEntities_17), value);
	}

	inline static int32_t get_offset_of_blockers_18() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___blockers_18)); }
	inline List_1_t2585711361 * get_blockers_18() const { return ___blockers_18; }
	inline List_1_t2585711361 ** get_address_of_blockers_18() { return &___blockers_18; }
	inline void set_blockers_18(List_1_t2585711361 * value)
	{
		___blockers_18 = value;
		Il2CppCodeGenWriteBarrier((&___blockers_18), value);
	}

	inline static int32_t get_offset_of_tilePositions_19() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___tilePositions_19)); }
	inline List_1_t3628304265 * get_tilePositions_19() const { return ___tilePositions_19; }
	inline List_1_t3628304265 ** get_address_of_tilePositions_19() { return &___tilePositions_19; }
	inline void set_tilePositions_19(List_1_t3628304265 * value)
	{
		___tilePositions_19 = value;
		Il2CppCodeGenWriteBarrier((&___tilePositions_19), value);
	}

	inline static int32_t get_offset_of_buyBooster1Button_20() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___buyBooster1Button_20)); }
	inline BuyBoosterButton_t1892892637 * get_buyBooster1Button_20() const { return ___buyBooster1Button_20; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_buyBooster1Button_20() { return &___buyBooster1Button_20; }
	inline void set_buyBooster1Button_20(BuyBoosterButton_t1892892637 * value)
	{
		___buyBooster1Button_20 = value;
		Il2CppCodeGenWriteBarrier((&___buyBooster1Button_20), value);
	}

	inline static int32_t get_offset_of_buyBooster2Button_21() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___buyBooster2Button_21)); }
	inline BuyBoosterButton_t1892892637 * get_buyBooster2Button_21() const { return ___buyBooster2Button_21; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_buyBooster2Button_21() { return &___buyBooster2Button_21; }
	inline void set_buyBooster2Button_21(BuyBoosterButton_t1892892637 * value)
	{
		___buyBooster2Button_21 = value;
		Il2CppCodeGenWriteBarrier((&___buyBooster2Button_21), value);
	}

	inline static int32_t get_offset_of_buyBooster3Button_22() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___buyBooster3Button_22)); }
	inline BuyBoosterButton_t1892892637 * get_buyBooster3Button_22() const { return ___buyBooster3Button_22; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_buyBooster3Button_22() { return &___buyBooster3Button_22; }
	inline void set_buyBooster3Button_22(BuyBoosterButton_t1892892637 * value)
	{
		___buyBooster3Button_22 = value;
		Il2CppCodeGenWriteBarrier((&___buyBooster3Button_22), value);
	}

	inline static int32_t get_offset_of_buyBooster4Button_23() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___buyBooster4Button_23)); }
	inline BuyBoosterButton_t1892892637 * get_buyBooster4Button_23() const { return ___buyBooster4Button_23; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_buyBooster4Button_23() { return &___buyBooster4Button_23; }
	inline void set_buyBooster4Button_23(BuyBoosterButton_t1892892637 * value)
	{
		___buyBooster4Button_23 = value;
		Il2CppCodeGenWriteBarrier((&___buyBooster4Button_23), value);
	}

	inline static int32_t get_offset_of_ingameBoosterPanel_24() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___ingameBoosterPanel_24)); }
	inline Image_t2670269651 * get_ingameBoosterPanel_24() const { return ___ingameBoosterPanel_24; }
	inline Image_t2670269651 ** get_address_of_ingameBoosterPanel_24() { return &___ingameBoosterPanel_24; }
	inline void set_ingameBoosterPanel_24(Image_t2670269651 * value)
	{
		___ingameBoosterPanel_24 = value;
		Il2CppCodeGenWriteBarrier((&___ingameBoosterPanel_24), value);
	}

	inline static int32_t get_offset_of_ingameBoosterText_25() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___ingameBoosterText_25)); }
	inline Text_t1901882714 * get_ingameBoosterText_25() const { return ___ingameBoosterText_25; }
	inline Text_t1901882714 ** get_address_of_ingameBoosterText_25() { return &___ingameBoosterText_25; }
	inline void set_ingameBoosterText_25(Text_t1901882714 * value)
	{
		___ingameBoosterText_25 = value;
		Il2CppCodeGenWriteBarrier((&___ingameBoosterText_25), value);
	}

	inline static int32_t get_offset_of_gameState_26() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gameState_26)); }
	inline GameState_t1141404161 * get_gameState_26() const { return ___gameState_26; }
	inline GameState_t1141404161 ** get_address_of_gameState_26() { return &___gameState_26; }
	inline void set_gameState_26(GameState_t1141404161 * value)
	{
		___gameState_26 = value;
		Il2CppCodeGenWriteBarrier((&___gameState_26), value);
	}

	inline static int32_t get_offset_of_gameConfig_27() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gameConfig_27)); }
	inline GameConfiguration_t2964563698 * get_gameConfig_27() const { return ___gameConfig_27; }
	inline GameConfiguration_t2964563698 ** get_address_of_gameConfig_27() { return &___gameConfig_27; }
	inline void set_gameConfig_27(GameConfiguration_t2964563698 * value)
	{
		___gameConfig_27 = value;
		Il2CppCodeGenWriteBarrier((&___gameConfig_27), value);
	}

	inline static int32_t get_offset_of_gameStarted_28() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gameStarted_28)); }
	inline bool get_gameStarted_28() const { return ___gameStarted_28; }
	inline bool* get_address_of_gameStarted_28() { return &___gameStarted_28; }
	inline void set_gameStarted_28(bool value)
	{
		___gameStarted_28 = value;
	}

	inline static int32_t get_offset_of_gameFinished_29() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___gameFinished_29)); }
	inline bool get_gameFinished_29() const { return ___gameFinished_29; }
	inline bool* get_address_of_gameFinished_29() { return &___gameFinished_29; }
	inline void set_gameFinished_29(bool value)
	{
		___gameFinished_29 = value;
	}

	inline static int32_t get_offset_of_accTime_30() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___accTime_30)); }
	inline float get_accTime_30() const { return ___accTime_30; }
	inline float* get_address_of_accTime_30() { return &___accTime_30; }
	inline void set_accTime_30(float value)
	{
		___accTime_30 = value;
	}

	inline static int32_t get_offset_of_blockWidth_31() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___blockWidth_31)); }
	inline float get_blockWidth_31() const { return ___blockWidth_31; }
	inline float* get_address_of_blockWidth_31() { return &___blockWidth_31; }
	inline void set_blockWidth_31(float value)
	{
		___blockWidth_31 = value;
	}

	inline static int32_t get_offset_of_blockHeight_32() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___blockHeight_32)); }
	inline float get_blockHeight_32() const { return ___blockHeight_32; }
	inline float* get_address_of_blockHeight_32() { return &___blockHeight_32; }
	inline void set_blockHeight_32(float value)
	{
		___blockHeight_32 = value;
	}

	inline static int32_t get_offset_of_suggestedMatch_33() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___suggestedMatch_33)); }
	inline bool get_suggestedMatch_33() const { return ___suggestedMatch_33; }
	inline bool* get_address_of_suggestedMatch_33() { return &___suggestedMatch_33; }
	inline void set_suggestedMatch_33(bool value)
	{
		___suggestedMatch_33 = value;
	}

	inline static int32_t get_offset_of_suggestedMatchBlocks_34() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___suggestedMatchBlocks_34)); }
	inline List_1_t2585711361 * get_suggestedMatchBlocks_34() const { return ___suggestedMatchBlocks_34; }
	inline List_1_t2585711361 ** get_address_of_suggestedMatchBlocks_34() { return &___suggestedMatchBlocks_34; }
	inline void set_suggestedMatchBlocks_34(List_1_t2585711361 * value)
	{
		___suggestedMatchBlocks_34 = value;
		Il2CppCodeGenWriteBarrier((&___suggestedMatchBlocks_34), value);
	}

	inline static int32_t get_offset_of_currentLimit_35() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___currentLimit_35)); }
	inline int32_t get_currentLimit_35() const { return ___currentLimit_35; }
	inline int32_t* get_address_of_currentLimit_35() { return &___currentLimit_35; }
	inline void set_currentLimit_35(int32_t value)
	{
		___currentLimit_35 = value;
	}

	inline static int32_t get_offset_of_currentlyAwardingBoosters_36() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___currentlyAwardingBoosters_36)); }
	inline bool get_currentlyAwardingBoosters_36() const { return ___currentlyAwardingBoosters_36; }
	inline bool* get_address_of_currentlyAwardingBoosters_36() { return &___currentlyAwardingBoosters_36; }
	inline void set_currentlyAwardingBoosters_36(bool value)
	{
		___currentlyAwardingBoosters_36 = value;
	}

	inline static int32_t get_offset_of_countdownCoroutine_37() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___countdownCoroutine_37)); }
	inline Coroutine_t3829159415 * get_countdownCoroutine_37() const { return ___countdownCoroutine_37; }
	inline Coroutine_t3829159415 ** get_address_of_countdownCoroutine_37() { return &___countdownCoroutine_37; }
	inline void set_countdownCoroutine_37(Coroutine_t3829159415 * value)
	{
		___countdownCoroutine_37 = value;
		Il2CppCodeGenWriteBarrier((&___countdownCoroutine_37), value);
	}

	inline static int32_t get_offset_of_boosterMode_38() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___boosterMode_38)); }
	inline bool get_boosterMode_38() const { return ___boosterMode_38; }
	inline bool* get_address_of_boosterMode_38() { return &___boosterMode_38; }
	inline void set_boosterMode_38(bool value)
	{
		___boosterMode_38 = value;
	}

	inline static int32_t get_offset_of_currentBoosterButton_39() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___currentBoosterButton_39)); }
	inline BuyBoosterButton_t1892892637 * get_currentBoosterButton_39() const { return ___currentBoosterButton_39; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_currentBoosterButton_39() { return &___currentBoosterButton_39; }
	inline void set_currentBoosterButton_39(BuyBoosterButton_t1892892637 * value)
	{
		___currentBoosterButton_39 = value;
		Il2CppCodeGenWriteBarrier((&___currentBoosterButton_39), value);
	}

	inline static int32_t get_offset_of_applyingPenalty_42() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___applyingPenalty_42)); }
	inline bool get_applyingPenalty_42() const { return ___applyingPenalty_42; }
	inline bool* get_address_of_applyingPenalty_42() { return &___applyingPenalty_42; }
	inline void set_applyingPenalty_42(bool value)
	{
		___applyingPenalty_42 = value;
	}

	inline static int32_t get_offset_of_ingameBoosterBgTweenId_43() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___ingameBoosterBgTweenId_43)); }
	inline int32_t get_ingameBoosterBgTweenId_43() const { return ___ingameBoosterBgTweenId_43; }
	inline int32_t* get_address_of_ingameBoosterBgTweenId_43() { return &___ingameBoosterBgTweenId_43; }
	inline void set_ingameBoosterBgTweenId_43(int32_t value)
	{
		___ingameBoosterBgTweenId_43 = value;
	}

	inline static int32_t get_offset_of_generatedCollectables_44() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___generatedCollectables_44)); }
	inline int32_t get_generatedCollectables_44() const { return ___generatedCollectables_44; }
	inline int32_t* get_address_of_generatedCollectables_44() { return &___generatedCollectables_44; }
	inline void set_generatedCollectables_44(int32_t value)
	{
		___generatedCollectables_44 = value;
	}

	inline static int32_t get_offset_of_neededCollectables_45() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___neededCollectables_45)); }
	inline int32_t get_neededCollectables_45() const { return ___neededCollectables_45; }
	inline int32_t* get_address_of_neededCollectables_45() { return &___neededCollectables_45; }
	inline void set_neededCollectables_45(int32_t value)
	{
		___neededCollectables_45 = value;
	}

	inline static int32_t get_offset_of_ghostSkillInfo_46() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___ghostSkillInfo_46)); }
	inline GhostSkillInfo_t3192096645 * get_ghostSkillInfo_46() const { return ___ghostSkillInfo_46; }
	inline GhostSkillInfo_t3192096645 ** get_address_of_ghostSkillInfo_46() { return &___ghostSkillInfo_46; }
	inline void set_ghostSkillInfo_46(GhostSkillInfo_t3192096645 * value)
	{
		___ghostSkillInfo_46 = value;
		Il2CppCodeGenWriteBarrier((&___ghostSkillInfo_46), value);
	}

	inline static int32_t get_offset_of_scoreText_47() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___scoreText_47)); }
	inline Text_t1901882714 * get_scoreText_47() const { return ___scoreText_47; }
	inline Text_t1901882714 ** get_address_of_scoreText_47() { return &___scoreText_47; }
	inline void set_scoreText_47(Text_t1901882714 * value)
	{
		___scoreText_47 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_47), value);
	}

	inline static int32_t get_offset_of_goldText_48() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___goldText_48)); }
	inline Text_t1901882714 * get_goldText_48() const { return ___goldText_48; }
	inline Text_t1901882714 ** get_address_of_goldText_48() { return &___goldText_48; }
	inline void set_goldText_48(Text_t1901882714 * value)
	{
		___goldText_48 = value;
		Il2CppCodeGenWriteBarrier((&___goldText_48), value);
	}

	inline static int32_t get_offset_of_lineRenderer_49() { return static_cast<int32_t>(offsetof(GameScene_t3472316885, ___lineRenderer_49)); }
	inline LineRenderer_t3154350270 * get_lineRenderer_49() const { return ___lineRenderer_49; }
	inline LineRenderer_t3154350270 ** get_address_of_lineRenderer_49() { return &___lineRenderer_49; }
	inline void set_lineRenderer_49(LineRenderer_t3154350270 * value)
	{
		___lineRenderer_49 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_49), value);
	}
};

struct GameScene_t3472316885_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<GameVanilla.Game.Common.BoosterType,System.Int32>,System.Int32> GameVanilla.Game.Scenes.GameScene::<>f__am$cache0
	Func_2_t570526977 * ___U3CU3Ef__amU24cache0_50;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_50() { return static_cast<int32_t>(offsetof(GameScene_t3472316885_StaticFields, ___U3CU3Ef__amU24cache0_50)); }
	inline Func_2_t570526977 * get_U3CU3Ef__amU24cache0_50() const { return ___U3CU3Ef__amU24cache0_50; }
	inline Func_2_t570526977 ** get_address_of_U3CU3Ef__amU24cache0_50() { return &___U3CU3Ef__amU24cache0_50; }
	inline void set_U3CU3Ef__amU24cache0_50(Func_2_t570526977 * value)
	{
		___U3CU3Ef__amU24cache0_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESCENE_T3472316885_H
#ifndef BLOCK_T3916805519_H
#define BLOCK_T3916805519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Block
struct  Block_t3916805519  : public TileEntity_t3486638455
{
public:
	// GameVanilla.Game.Common.BlockType GameVanilla.Game.Common.Block::type
	int32_t ___type_4;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(Block_t3916805519, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK_T3916805519_H
#ifndef ENDGAMEPOPUP_T660757690_H
#define ENDGAMEPOPUP_T660757690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.EndGamePopup
struct  EndGamePopup_t660757690  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GameVanilla.Game.Popups.EndGamePopup::levelText
	Text_t1901882714 * ___levelText_6;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.EndGamePopup::scoreText
	Text_t1901882714 * ___scoreText_7;
	// UnityEngine.GameObject GameVanilla.Game.Popups.EndGamePopup::goalGroup
	GameObject_t1113636619 * ___goalGroup_8;

public:
	inline static int32_t get_offset_of_levelText_6() { return static_cast<int32_t>(offsetof(EndGamePopup_t660757690, ___levelText_6)); }
	inline Text_t1901882714 * get_levelText_6() const { return ___levelText_6; }
	inline Text_t1901882714 ** get_address_of_levelText_6() { return &___levelText_6; }
	inline void set_levelText_6(Text_t1901882714 * value)
	{
		___levelText_6 = value;
		Il2CppCodeGenWriteBarrier((&___levelText_6), value);
	}

	inline static int32_t get_offset_of_scoreText_7() { return static_cast<int32_t>(offsetof(EndGamePopup_t660757690, ___scoreText_7)); }
	inline Text_t1901882714 * get_scoreText_7() const { return ___scoreText_7; }
	inline Text_t1901882714 ** get_address_of_scoreText_7() { return &___scoreText_7; }
	inline void set_scoreText_7(Text_t1901882714 * value)
	{
		___scoreText_7 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_7), value);
	}

	inline static int32_t get_offset_of_goalGroup_8() { return static_cast<int32_t>(offsetof(EndGamePopup_t660757690, ___goalGroup_8)); }
	inline GameObject_t1113636619 * get_goalGroup_8() const { return ___goalGroup_8; }
	inline GameObject_t1113636619 ** get_address_of_goalGroup_8() { return &___goalGroup_8; }
	inline void set_goalGroup_8(GameObject_t1113636619 * value)
	{
		___goalGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&___goalGroup_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDGAMEPOPUP_T660757690_H
#ifndef BLOCKER_T1923197640_H
#define BLOCKER_T1923197640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Blocker
struct  Blocker_t1923197640  : public TileEntity_t3486638455
{
public:
	// GameVanilla.Game.Common.BlockerType GameVanilla.Game.Common.Blocker::type
	int32_t ___type_4;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(Blocker_t1923197640, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKER_T1923197640_H
#ifndef EXITGAMEPOPUP_T3936844710_H
#define EXITGAMEPOPUP_T3936844710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.ExitGamePopup
struct  ExitGamePopup_t3936844710  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXITGAMEPOPUP_T3936844710_H
#ifndef BUYLIVESPOPUP_T1157651407_H
#define BUYLIVESPOPUP_T1157651407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BuyLivesPopup
struct  BuyLivesPopup_t1157651407  : public Popup_t1063720813
{
public:
	// UnityEngine.Sprite GameVanilla.Game.Popups.BuyLivesPopup::lifeSprite
	Sprite_t280657092 * ___lifeSprite_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> GameVanilla.Game.Popups.BuyLivesPopup::lifeImages
	List_1_t4142344393 * ___lifeImages_7;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyLivesPopup::refillCostText
	Text_t1901882714 * ___refillCostText_8;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyLivesPopup::timeToNextLifeText
	Text_t1901882714 * ___timeToNextLifeText_9;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.BuyLivesPopup::lifeParticles
	ParticleSystem_t1800779281 * ___lifeParticles_10;
	// GameVanilla.Core.AnimatedButton GameVanilla.Game.Popups.BuyLivesPopup::refillButton
	AnimatedButton_t997585140 * ___refillButton_11;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.BuyLivesPopup::refillButtonImage
	Image_t2670269651 * ___refillButtonImage_12;
	// UnityEngine.Sprite GameVanilla.Game.Popups.BuyLivesPopup::refillButtonDisabledSprite
	Sprite_t280657092 * ___refillButtonDisabledSprite_13;

public:
	inline static int32_t get_offset_of_lifeSprite_6() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___lifeSprite_6)); }
	inline Sprite_t280657092 * get_lifeSprite_6() const { return ___lifeSprite_6; }
	inline Sprite_t280657092 ** get_address_of_lifeSprite_6() { return &___lifeSprite_6; }
	inline void set_lifeSprite_6(Sprite_t280657092 * value)
	{
		___lifeSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___lifeSprite_6), value);
	}

	inline static int32_t get_offset_of_lifeImages_7() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___lifeImages_7)); }
	inline List_1_t4142344393 * get_lifeImages_7() const { return ___lifeImages_7; }
	inline List_1_t4142344393 ** get_address_of_lifeImages_7() { return &___lifeImages_7; }
	inline void set_lifeImages_7(List_1_t4142344393 * value)
	{
		___lifeImages_7 = value;
		Il2CppCodeGenWriteBarrier((&___lifeImages_7), value);
	}

	inline static int32_t get_offset_of_refillCostText_8() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___refillCostText_8)); }
	inline Text_t1901882714 * get_refillCostText_8() const { return ___refillCostText_8; }
	inline Text_t1901882714 ** get_address_of_refillCostText_8() { return &___refillCostText_8; }
	inline void set_refillCostText_8(Text_t1901882714 * value)
	{
		___refillCostText_8 = value;
		Il2CppCodeGenWriteBarrier((&___refillCostText_8), value);
	}

	inline static int32_t get_offset_of_timeToNextLifeText_9() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___timeToNextLifeText_9)); }
	inline Text_t1901882714 * get_timeToNextLifeText_9() const { return ___timeToNextLifeText_9; }
	inline Text_t1901882714 ** get_address_of_timeToNextLifeText_9() { return &___timeToNextLifeText_9; }
	inline void set_timeToNextLifeText_9(Text_t1901882714 * value)
	{
		___timeToNextLifeText_9 = value;
		Il2CppCodeGenWriteBarrier((&___timeToNextLifeText_9), value);
	}

	inline static int32_t get_offset_of_lifeParticles_10() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___lifeParticles_10)); }
	inline ParticleSystem_t1800779281 * get_lifeParticles_10() const { return ___lifeParticles_10; }
	inline ParticleSystem_t1800779281 ** get_address_of_lifeParticles_10() { return &___lifeParticles_10; }
	inline void set_lifeParticles_10(ParticleSystem_t1800779281 * value)
	{
		___lifeParticles_10 = value;
		Il2CppCodeGenWriteBarrier((&___lifeParticles_10), value);
	}

	inline static int32_t get_offset_of_refillButton_11() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___refillButton_11)); }
	inline AnimatedButton_t997585140 * get_refillButton_11() const { return ___refillButton_11; }
	inline AnimatedButton_t997585140 ** get_address_of_refillButton_11() { return &___refillButton_11; }
	inline void set_refillButton_11(AnimatedButton_t997585140 * value)
	{
		___refillButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___refillButton_11), value);
	}

	inline static int32_t get_offset_of_refillButtonImage_12() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___refillButtonImage_12)); }
	inline Image_t2670269651 * get_refillButtonImage_12() const { return ___refillButtonImage_12; }
	inline Image_t2670269651 ** get_address_of_refillButtonImage_12() { return &___refillButtonImage_12; }
	inline void set_refillButtonImage_12(Image_t2670269651 * value)
	{
		___refillButtonImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___refillButtonImage_12), value);
	}

	inline static int32_t get_offset_of_refillButtonDisabledSprite_13() { return static_cast<int32_t>(offsetof(BuyLivesPopup_t1157651407, ___refillButtonDisabledSprite_13)); }
	inline Sprite_t280657092 * get_refillButtonDisabledSprite_13() const { return ___refillButtonDisabledSprite_13; }
	inline Sprite_t280657092 ** get_address_of_refillButtonDisabledSprite_13() { return &___refillButtonDisabledSprite_13; }
	inline void set_refillButtonDisabledSprite_13(Sprite_t280657092 * value)
	{
		___refillButtonDisabledSprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___refillButtonDisabledSprite_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYLIVESPOPUP_T1157651407_H
#ifndef LEVELSCENE_T3942611905_H
#define LEVELSCENE_T3942611905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.LevelScene
struct  LevelScene_t3942611905  : public BaseScene_t2918414559
{
public:
	// UnityEngine.UI.ScrollRect GameVanilla.Game.Scenes.LevelScene::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_5;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.LevelScene::scrollView
	GameObject_t1113636619 * ___scrollView_6;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.LevelScene::avatarPrefab
	GameObject_t1113636619 * ___avatarPrefab_7;
	// UnityEngine.GameObject GameVanilla.Game.Scenes.LevelScene::rewardedAdButton
	GameObject_t1113636619 * ___rewardedAdButton_8;

public:
	inline static int32_t get_offset_of_scrollRect_5() { return static_cast<int32_t>(offsetof(LevelScene_t3942611905, ___scrollRect_5)); }
	inline ScrollRect_t4137855814 * get_scrollRect_5() const { return ___scrollRect_5; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_5() { return &___scrollRect_5; }
	inline void set_scrollRect_5(ScrollRect_t4137855814 * value)
	{
		___scrollRect_5 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_5), value);
	}

	inline static int32_t get_offset_of_scrollView_6() { return static_cast<int32_t>(offsetof(LevelScene_t3942611905, ___scrollView_6)); }
	inline GameObject_t1113636619 * get_scrollView_6() const { return ___scrollView_6; }
	inline GameObject_t1113636619 ** get_address_of_scrollView_6() { return &___scrollView_6; }
	inline void set_scrollView_6(GameObject_t1113636619 * value)
	{
		___scrollView_6 = value;
		Il2CppCodeGenWriteBarrier((&___scrollView_6), value);
	}

	inline static int32_t get_offset_of_avatarPrefab_7() { return static_cast<int32_t>(offsetof(LevelScene_t3942611905, ___avatarPrefab_7)); }
	inline GameObject_t1113636619 * get_avatarPrefab_7() const { return ___avatarPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_avatarPrefab_7() { return &___avatarPrefab_7; }
	inline void set_avatarPrefab_7(GameObject_t1113636619 * value)
	{
		___avatarPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___avatarPrefab_7), value);
	}

	inline static int32_t get_offset_of_rewardedAdButton_8() { return static_cast<int32_t>(offsetof(LevelScene_t3942611905, ___rewardedAdButton_8)); }
	inline GameObject_t1113636619 * get_rewardedAdButton_8() const { return ___rewardedAdButton_8; }
	inline GameObject_t1113636619 ** get_address_of_rewardedAdButton_8() { return &___rewardedAdButton_8; }
	inline void set_rewardedAdButton_8(GameObject_t1113636619 * value)
	{
		___rewardedAdButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___rewardedAdButton_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSCENE_T3942611905_H
#ifndef NOMOVESORTIMEPOPUP_T3423578163_H
#define NOMOVESORTIMEPOPUP_T3423578163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.NoMovesOrTimePopup
struct  NoMovesOrTimePopup_t3423578163  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GameVanilla.Game.Popups.NoMovesOrTimePopup::numCoinsText
	Text_t1901882714 * ___numCoinsText_6;
	// UnityEngine.GameObject GameVanilla.Game.Popups.NoMovesOrTimePopup::movesGroup
	GameObject_t1113636619 * ___movesGroup_7;
	// UnityEngine.GameObject GameVanilla.Game.Popups.NoMovesOrTimePopup::timeGroup
	GameObject_t1113636619 * ___timeGroup_8;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.NoMovesOrTimePopup::title1Text
	Text_t1901882714 * ___title1Text_9;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.NoMovesOrTimePopup::title2Text
	Text_t1901882714 * ___title2Text_10;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.NoMovesOrTimePopup::numExtraMovesText
	Text_t1901882714 * ___numExtraMovesText_11;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.NoMovesOrTimePopup::costText
	Text_t1901882714 * ___costText_12;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.NoMovesOrTimePopup::coinParticles
	ParticleSystem_t1800779281 * ___coinParticles_13;
	// GameVanilla.Game.Scenes.GameScene GameVanilla.Game.Popups.NoMovesOrTimePopup::gameScene
	GameScene_t3472316885 * ___gameScene_14;

public:
	inline static int32_t get_offset_of_numCoinsText_6() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___numCoinsText_6)); }
	inline Text_t1901882714 * get_numCoinsText_6() const { return ___numCoinsText_6; }
	inline Text_t1901882714 ** get_address_of_numCoinsText_6() { return &___numCoinsText_6; }
	inline void set_numCoinsText_6(Text_t1901882714 * value)
	{
		___numCoinsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___numCoinsText_6), value);
	}

	inline static int32_t get_offset_of_movesGroup_7() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___movesGroup_7)); }
	inline GameObject_t1113636619 * get_movesGroup_7() const { return ___movesGroup_7; }
	inline GameObject_t1113636619 ** get_address_of_movesGroup_7() { return &___movesGroup_7; }
	inline void set_movesGroup_7(GameObject_t1113636619 * value)
	{
		___movesGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___movesGroup_7), value);
	}

	inline static int32_t get_offset_of_timeGroup_8() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___timeGroup_8)); }
	inline GameObject_t1113636619 * get_timeGroup_8() const { return ___timeGroup_8; }
	inline GameObject_t1113636619 ** get_address_of_timeGroup_8() { return &___timeGroup_8; }
	inline void set_timeGroup_8(GameObject_t1113636619 * value)
	{
		___timeGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&___timeGroup_8), value);
	}

	inline static int32_t get_offset_of_title1Text_9() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___title1Text_9)); }
	inline Text_t1901882714 * get_title1Text_9() const { return ___title1Text_9; }
	inline Text_t1901882714 ** get_address_of_title1Text_9() { return &___title1Text_9; }
	inline void set_title1Text_9(Text_t1901882714 * value)
	{
		___title1Text_9 = value;
		Il2CppCodeGenWriteBarrier((&___title1Text_9), value);
	}

	inline static int32_t get_offset_of_title2Text_10() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___title2Text_10)); }
	inline Text_t1901882714 * get_title2Text_10() const { return ___title2Text_10; }
	inline Text_t1901882714 ** get_address_of_title2Text_10() { return &___title2Text_10; }
	inline void set_title2Text_10(Text_t1901882714 * value)
	{
		___title2Text_10 = value;
		Il2CppCodeGenWriteBarrier((&___title2Text_10), value);
	}

	inline static int32_t get_offset_of_numExtraMovesText_11() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___numExtraMovesText_11)); }
	inline Text_t1901882714 * get_numExtraMovesText_11() const { return ___numExtraMovesText_11; }
	inline Text_t1901882714 ** get_address_of_numExtraMovesText_11() { return &___numExtraMovesText_11; }
	inline void set_numExtraMovesText_11(Text_t1901882714 * value)
	{
		___numExtraMovesText_11 = value;
		Il2CppCodeGenWriteBarrier((&___numExtraMovesText_11), value);
	}

	inline static int32_t get_offset_of_costText_12() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___costText_12)); }
	inline Text_t1901882714 * get_costText_12() const { return ___costText_12; }
	inline Text_t1901882714 ** get_address_of_costText_12() { return &___costText_12; }
	inline void set_costText_12(Text_t1901882714 * value)
	{
		___costText_12 = value;
		Il2CppCodeGenWriteBarrier((&___costText_12), value);
	}

	inline static int32_t get_offset_of_coinParticles_13() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___coinParticles_13)); }
	inline ParticleSystem_t1800779281 * get_coinParticles_13() const { return ___coinParticles_13; }
	inline ParticleSystem_t1800779281 ** get_address_of_coinParticles_13() { return &___coinParticles_13; }
	inline void set_coinParticles_13(ParticleSystem_t1800779281 * value)
	{
		___coinParticles_13 = value;
		Il2CppCodeGenWriteBarrier((&___coinParticles_13), value);
	}

	inline static int32_t get_offset_of_gameScene_14() { return static_cast<int32_t>(offsetof(NoMovesOrTimePopup_t3423578163, ___gameScene_14)); }
	inline GameScene_t3472316885 * get_gameScene_14() const { return ___gameScene_14; }
	inline GameScene_t3472316885 ** get_address_of_gameScene_14() { return &___gameScene_14; }
	inline void set_gameScene_14(GameScene_t3472316885 * value)
	{
		___gameScene_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameScene_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOMOVESORTIMEPOPUP_T3423578163_H
#ifndef ALERTPOPUP_T155184926_H
#define ALERTPOPUP_T155184926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.AlertPopup
struct  AlertPopup_t155184926  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GameVanilla.Game.Popups.AlertPopup::titleText
	Text_t1901882714 * ___titleText_6;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.AlertPopup::bodyText
	Text_t1901882714 * ___bodyText_7;

public:
	inline static int32_t get_offset_of_titleText_6() { return static_cast<int32_t>(offsetof(AlertPopup_t155184926, ___titleText_6)); }
	inline Text_t1901882714 * get_titleText_6() const { return ___titleText_6; }
	inline Text_t1901882714 ** get_address_of_titleText_6() { return &___titleText_6; }
	inline void set_titleText_6(Text_t1901882714 * value)
	{
		___titleText_6 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_6), value);
	}

	inline static int32_t get_offset_of_bodyText_7() { return static_cast<int32_t>(offsetof(AlertPopup_t155184926, ___bodyText_7)); }
	inline Text_t1901882714 * get_bodyText_7() const { return ___bodyText_7; }
	inline Text_t1901882714 ** get_address_of_bodyText_7() { return &___bodyText_7; }
	inline void set_bodyText_7(Text_t1901882714 * value)
	{
		___bodyText_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTPOPUP_T155184926_H
#ifndef SETTINGSPOPUP_T536495157_H
#define SETTINGSPOPUP_T536495157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.SettingsPopup
struct  SettingsPopup_t536495157  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.ToggleGroup GameVanilla.Game.Popups.SettingsPopup::avatarToggleGroup
	ToggleGroup_t123837990 * ___avatarToggleGroup_6;
	// UnityEngine.UI.Slider GameVanilla.Game.Popups.SettingsPopup::soundSlider
	Slider_t3903728902 * ___soundSlider_7;
	// UnityEngine.UI.Slider GameVanilla.Game.Popups.SettingsPopup::musicSlider
	Slider_t3903728902 * ___musicSlider_8;
	// UnityEngine.UI.Toggle GameVanilla.Game.Popups.SettingsPopup::notificationsToggle
	Toggle_t2735377061 * ___notificationsToggle_9;
	// GameVanilla.Core.AnimatedButton GameVanilla.Game.Popups.SettingsPopup::resetProgressButton
	AnimatedButton_t997585140 * ___resetProgressButton_10;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.SettingsPopup::resetProgressImage
	Image_t2670269651 * ___resetProgressImage_11;
	// UnityEngine.Sprite GameVanilla.Game.Popups.SettingsPopup::resetProgressDisabledSprite
	Sprite_t280657092 * ___resetProgressDisabledSprite_12;
	// System.Int32 GameVanilla.Game.Popups.SettingsPopup::currentAvatar
	int32_t ___currentAvatar_13;
	// System.Int32 GameVanilla.Game.Popups.SettingsPopup::currentSound
	int32_t ___currentSound_14;
	// System.Int32 GameVanilla.Game.Popups.SettingsPopup::currentMusic
	int32_t ___currentMusic_15;
	// System.Int32 GameVanilla.Game.Popups.SettingsPopup::currentNotifications
	int32_t ___currentNotifications_16;

public:
	inline static int32_t get_offset_of_avatarToggleGroup_6() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___avatarToggleGroup_6)); }
	inline ToggleGroup_t123837990 * get_avatarToggleGroup_6() const { return ___avatarToggleGroup_6; }
	inline ToggleGroup_t123837990 ** get_address_of_avatarToggleGroup_6() { return &___avatarToggleGroup_6; }
	inline void set_avatarToggleGroup_6(ToggleGroup_t123837990 * value)
	{
		___avatarToggleGroup_6 = value;
		Il2CppCodeGenWriteBarrier((&___avatarToggleGroup_6), value);
	}

	inline static int32_t get_offset_of_soundSlider_7() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___soundSlider_7)); }
	inline Slider_t3903728902 * get_soundSlider_7() const { return ___soundSlider_7; }
	inline Slider_t3903728902 ** get_address_of_soundSlider_7() { return &___soundSlider_7; }
	inline void set_soundSlider_7(Slider_t3903728902 * value)
	{
		___soundSlider_7 = value;
		Il2CppCodeGenWriteBarrier((&___soundSlider_7), value);
	}

	inline static int32_t get_offset_of_musicSlider_8() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___musicSlider_8)); }
	inline Slider_t3903728902 * get_musicSlider_8() const { return ___musicSlider_8; }
	inline Slider_t3903728902 ** get_address_of_musicSlider_8() { return &___musicSlider_8; }
	inline void set_musicSlider_8(Slider_t3903728902 * value)
	{
		___musicSlider_8 = value;
		Il2CppCodeGenWriteBarrier((&___musicSlider_8), value);
	}

	inline static int32_t get_offset_of_notificationsToggle_9() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___notificationsToggle_9)); }
	inline Toggle_t2735377061 * get_notificationsToggle_9() const { return ___notificationsToggle_9; }
	inline Toggle_t2735377061 ** get_address_of_notificationsToggle_9() { return &___notificationsToggle_9; }
	inline void set_notificationsToggle_9(Toggle_t2735377061 * value)
	{
		___notificationsToggle_9 = value;
		Il2CppCodeGenWriteBarrier((&___notificationsToggle_9), value);
	}

	inline static int32_t get_offset_of_resetProgressButton_10() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___resetProgressButton_10)); }
	inline AnimatedButton_t997585140 * get_resetProgressButton_10() const { return ___resetProgressButton_10; }
	inline AnimatedButton_t997585140 ** get_address_of_resetProgressButton_10() { return &___resetProgressButton_10; }
	inline void set_resetProgressButton_10(AnimatedButton_t997585140 * value)
	{
		___resetProgressButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___resetProgressButton_10), value);
	}

	inline static int32_t get_offset_of_resetProgressImage_11() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___resetProgressImage_11)); }
	inline Image_t2670269651 * get_resetProgressImage_11() const { return ___resetProgressImage_11; }
	inline Image_t2670269651 ** get_address_of_resetProgressImage_11() { return &___resetProgressImage_11; }
	inline void set_resetProgressImage_11(Image_t2670269651 * value)
	{
		___resetProgressImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___resetProgressImage_11), value);
	}

	inline static int32_t get_offset_of_resetProgressDisabledSprite_12() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___resetProgressDisabledSprite_12)); }
	inline Sprite_t280657092 * get_resetProgressDisabledSprite_12() const { return ___resetProgressDisabledSprite_12; }
	inline Sprite_t280657092 ** get_address_of_resetProgressDisabledSprite_12() { return &___resetProgressDisabledSprite_12; }
	inline void set_resetProgressDisabledSprite_12(Sprite_t280657092 * value)
	{
		___resetProgressDisabledSprite_12 = value;
		Il2CppCodeGenWriteBarrier((&___resetProgressDisabledSprite_12), value);
	}

	inline static int32_t get_offset_of_currentAvatar_13() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___currentAvatar_13)); }
	inline int32_t get_currentAvatar_13() const { return ___currentAvatar_13; }
	inline int32_t* get_address_of_currentAvatar_13() { return &___currentAvatar_13; }
	inline void set_currentAvatar_13(int32_t value)
	{
		___currentAvatar_13 = value;
	}

	inline static int32_t get_offset_of_currentSound_14() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___currentSound_14)); }
	inline int32_t get_currentSound_14() const { return ___currentSound_14; }
	inline int32_t* get_address_of_currentSound_14() { return &___currentSound_14; }
	inline void set_currentSound_14(int32_t value)
	{
		___currentSound_14 = value;
	}

	inline static int32_t get_offset_of_currentMusic_15() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___currentMusic_15)); }
	inline int32_t get_currentMusic_15() const { return ___currentMusic_15; }
	inline int32_t* get_address_of_currentMusic_15() { return &___currentMusic_15; }
	inline void set_currentMusic_15(int32_t value)
	{
		___currentMusic_15 = value;
	}

	inline static int32_t get_offset_of_currentNotifications_16() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157, ___currentNotifications_16)); }
	inline int32_t get_currentNotifications_16() const { return ___currentNotifications_16; }
	inline int32_t* get_address_of_currentNotifications_16() { return &___currentNotifications_16; }
	inline void set_currentNotifications_16(int32_t value)
	{
		___currentNotifications_16 = value;
	}
};

struct SettingsPopup_t536495157_StaticFields
{
public:
	// System.Action`1<GameVanilla.Game.Popups.AlertPopup> GameVanilla.Game.Popups.SettingsPopup::<>f__am$cache0
	Action_1_t327652521 * ___U3CU3Ef__amU24cache0_17;
	// System.Action`1<GameVanilla.Game.Popups.AlertPopup> GameVanilla.Game.Popups.SettingsPopup::<>f__am$cache1
	Action_1_t327652521 * ___U3CU3Ef__amU24cache1_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Action_1_t327652521 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Action_1_t327652521 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Action_1_t327652521 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_18() { return static_cast<int32_t>(offsetof(SettingsPopup_t536495157_StaticFields, ___U3CU3Ef__amU24cache1_18)); }
	inline Action_1_t327652521 * get_U3CU3Ef__amU24cache1_18() const { return ___U3CU3Ef__amU24cache1_18; }
	inline Action_1_t327652521 ** get_address_of_U3CU3Ef__amU24cache1_18() { return &___U3CU3Ef__amU24cache1_18; }
	inline void set_U3CU3Ef__amU24cache1_18(Action_1_t327652521 * value)
	{
		___U3CU3Ef__amU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSPOPUP_T536495157_H
#ifndef BOOSTER_T4064586441_H
#define BOOSTER_T4064586441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Booster
struct  Booster_t4064586441  : public TileEntity_t3486638455
{
public:
	// GameVanilla.Game.Common.BoosterType GameVanilla.Game.Common.Booster::type
	int32_t ___type_4;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(Booster_t4064586441, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOSTER_T4064586441_H
#ifndef LOADINGPOPUP_T2279989808_H
#define LOADINGPOPUP_T2279989808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.LoadingPopup
struct  LoadingPopup_t2279989808  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGPOPUP_T2279989808_H
#ifndef BOOSTERAWARDPOPUP_T3255387682_H
#define BOOSTERAWARDPOPUP_T3255387682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BoosterAwardPopup
struct  BoosterAwardPopup_t3255387682  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOSTERAWARDPOPUP_T3255387682_H
#ifndef BUYBOOSTERSPOPUP_T373990552_H
#define BUYBOOSTERSPOPUP_T373990552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.BuyBoostersPopup
struct  BuyBoostersPopup_t373990552  : public Popup_t1063720813
{
public:
	// UnityEngine.Sprite GameVanilla.Game.Popups.BuyBoostersPopup::horizontalBombSprite
	Sprite_t280657092 * ___horizontalBombSprite_6;
	// UnityEngine.Sprite GameVanilla.Game.Popups.BuyBoostersPopup::verticalBombSprite
	Sprite_t280657092 * ___verticalBombSprite_7;
	// UnityEngine.Sprite GameVanilla.Game.Popups.BuyBoostersPopup::dynamiteSprite
	Sprite_t280657092 * ___dynamiteSprite_8;
	// UnityEngine.Sprite GameVanilla.Game.Popups.BuyBoostersPopup::colorBombSprite
	Sprite_t280657092 * ___colorBombSprite_9;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyBoostersPopup::boosterNameText
	Text_t1901882714 * ___boosterNameText_10;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyBoostersPopup::boosterDescriptionText
	Text_t1901882714 * ___boosterDescriptionText_11;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.BuyBoostersPopup::boosterImage
	Image_t2670269651 * ___boosterImage_12;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyBoostersPopup::boosterAmountText
	Text_t1901882714 * ___boosterAmountText_13;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyBoostersPopup::boosterCostText
	Text_t1901882714 * ___boosterCostText_14;
	// UnityEngine.UI.Text GameVanilla.Game.Popups.BuyBoostersPopup::numCoinsText
	Text_t1901882714 * ___numCoinsText_15;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.BuyBoostersPopup::coinParticles
	ParticleSystem_t1800779281 * ___coinParticles_16;
	// GameVanilla.Game.UI.BuyBoosterButton GameVanilla.Game.Popups.BuyBoostersPopup::buyButton
	BuyBoosterButton_t1892892637 * ___buyButton_17;

public:
	inline static int32_t get_offset_of_horizontalBombSprite_6() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___horizontalBombSprite_6)); }
	inline Sprite_t280657092 * get_horizontalBombSprite_6() const { return ___horizontalBombSprite_6; }
	inline Sprite_t280657092 ** get_address_of_horizontalBombSprite_6() { return &___horizontalBombSprite_6; }
	inline void set_horizontalBombSprite_6(Sprite_t280657092 * value)
	{
		___horizontalBombSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalBombSprite_6), value);
	}

	inline static int32_t get_offset_of_verticalBombSprite_7() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___verticalBombSprite_7)); }
	inline Sprite_t280657092 * get_verticalBombSprite_7() const { return ___verticalBombSprite_7; }
	inline Sprite_t280657092 ** get_address_of_verticalBombSprite_7() { return &___verticalBombSprite_7; }
	inline void set_verticalBombSprite_7(Sprite_t280657092 * value)
	{
		___verticalBombSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalBombSprite_7), value);
	}

	inline static int32_t get_offset_of_dynamiteSprite_8() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___dynamiteSprite_8)); }
	inline Sprite_t280657092 * get_dynamiteSprite_8() const { return ___dynamiteSprite_8; }
	inline Sprite_t280657092 ** get_address_of_dynamiteSprite_8() { return &___dynamiteSprite_8; }
	inline void set_dynamiteSprite_8(Sprite_t280657092 * value)
	{
		___dynamiteSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___dynamiteSprite_8), value);
	}

	inline static int32_t get_offset_of_colorBombSprite_9() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___colorBombSprite_9)); }
	inline Sprite_t280657092 * get_colorBombSprite_9() const { return ___colorBombSprite_9; }
	inline Sprite_t280657092 ** get_address_of_colorBombSprite_9() { return &___colorBombSprite_9; }
	inline void set_colorBombSprite_9(Sprite_t280657092 * value)
	{
		___colorBombSprite_9 = value;
		Il2CppCodeGenWriteBarrier((&___colorBombSprite_9), value);
	}

	inline static int32_t get_offset_of_boosterNameText_10() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___boosterNameText_10)); }
	inline Text_t1901882714 * get_boosterNameText_10() const { return ___boosterNameText_10; }
	inline Text_t1901882714 ** get_address_of_boosterNameText_10() { return &___boosterNameText_10; }
	inline void set_boosterNameText_10(Text_t1901882714 * value)
	{
		___boosterNameText_10 = value;
		Il2CppCodeGenWriteBarrier((&___boosterNameText_10), value);
	}

	inline static int32_t get_offset_of_boosterDescriptionText_11() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___boosterDescriptionText_11)); }
	inline Text_t1901882714 * get_boosterDescriptionText_11() const { return ___boosterDescriptionText_11; }
	inline Text_t1901882714 ** get_address_of_boosterDescriptionText_11() { return &___boosterDescriptionText_11; }
	inline void set_boosterDescriptionText_11(Text_t1901882714 * value)
	{
		___boosterDescriptionText_11 = value;
		Il2CppCodeGenWriteBarrier((&___boosterDescriptionText_11), value);
	}

	inline static int32_t get_offset_of_boosterImage_12() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___boosterImage_12)); }
	inline Image_t2670269651 * get_boosterImage_12() const { return ___boosterImage_12; }
	inline Image_t2670269651 ** get_address_of_boosterImage_12() { return &___boosterImage_12; }
	inline void set_boosterImage_12(Image_t2670269651 * value)
	{
		___boosterImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___boosterImage_12), value);
	}

	inline static int32_t get_offset_of_boosterAmountText_13() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___boosterAmountText_13)); }
	inline Text_t1901882714 * get_boosterAmountText_13() const { return ___boosterAmountText_13; }
	inline Text_t1901882714 ** get_address_of_boosterAmountText_13() { return &___boosterAmountText_13; }
	inline void set_boosterAmountText_13(Text_t1901882714 * value)
	{
		___boosterAmountText_13 = value;
		Il2CppCodeGenWriteBarrier((&___boosterAmountText_13), value);
	}

	inline static int32_t get_offset_of_boosterCostText_14() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___boosterCostText_14)); }
	inline Text_t1901882714 * get_boosterCostText_14() const { return ___boosterCostText_14; }
	inline Text_t1901882714 ** get_address_of_boosterCostText_14() { return &___boosterCostText_14; }
	inline void set_boosterCostText_14(Text_t1901882714 * value)
	{
		___boosterCostText_14 = value;
		Il2CppCodeGenWriteBarrier((&___boosterCostText_14), value);
	}

	inline static int32_t get_offset_of_numCoinsText_15() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___numCoinsText_15)); }
	inline Text_t1901882714 * get_numCoinsText_15() const { return ___numCoinsText_15; }
	inline Text_t1901882714 ** get_address_of_numCoinsText_15() { return &___numCoinsText_15; }
	inline void set_numCoinsText_15(Text_t1901882714 * value)
	{
		___numCoinsText_15 = value;
		Il2CppCodeGenWriteBarrier((&___numCoinsText_15), value);
	}

	inline static int32_t get_offset_of_coinParticles_16() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___coinParticles_16)); }
	inline ParticleSystem_t1800779281 * get_coinParticles_16() const { return ___coinParticles_16; }
	inline ParticleSystem_t1800779281 ** get_address_of_coinParticles_16() { return &___coinParticles_16; }
	inline void set_coinParticles_16(ParticleSystem_t1800779281 * value)
	{
		___coinParticles_16 = value;
		Il2CppCodeGenWriteBarrier((&___coinParticles_16), value);
	}

	inline static int32_t get_offset_of_buyButton_17() { return static_cast<int32_t>(offsetof(BuyBoostersPopup_t373990552, ___buyButton_17)); }
	inline BuyBoosterButton_t1892892637 * get_buyButton_17() const { return ___buyButton_17; }
	inline BuyBoosterButton_t1892892637 ** get_address_of_buyButton_17() { return &___buyButton_17; }
	inline void set_buyButton_17(BuyBoosterButton_t1892892637 * value)
	{
		___buyButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___buyButton_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYBOOSTERSPOPUP_T373990552_H
#ifndef HOMESCENE_T444677190_H
#define HOMESCENE_T444677190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Scenes.HomeScene
struct  HomeScene_t444677190  : public BaseScene_t2918414559
{
public:
	// GameVanilla.Core.AnimatedButton GameVanilla.Game.Scenes.HomeScene::soundButton
	AnimatedButton_t997585140 * ___soundButton_5;
	// GameVanilla.Core.AnimatedButton GameVanilla.Game.Scenes.HomeScene::musicButton
	AnimatedButton_t997585140 * ___musicButton_6;

public:
	inline static int32_t get_offset_of_soundButton_5() { return static_cast<int32_t>(offsetof(HomeScene_t444677190, ___soundButton_5)); }
	inline AnimatedButton_t997585140 * get_soundButton_5() const { return ___soundButton_5; }
	inline AnimatedButton_t997585140 ** get_address_of_soundButton_5() { return &___soundButton_5; }
	inline void set_soundButton_5(AnimatedButton_t997585140 * value)
	{
		___soundButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___soundButton_5), value);
	}

	inline static int32_t get_offset_of_musicButton_6() { return static_cast<int32_t>(offsetof(HomeScene_t444677190, ___musicButton_6)); }
	inline AnimatedButton_t997585140 * get_musicButton_6() const { return ___musicButton_6; }
	inline AnimatedButton_t997585140 ** get_address_of_musicButton_6() { return &___musicButton_6; }
	inline void set_musicButton_6(AnimatedButton_t997585140 * value)
	{
		___musicButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___musicButton_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOMESCENE_T444677190_H
#ifndef COLORBOMB_T1194853658_H
#define COLORBOMB_T1194853658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.ColorBomb
struct  ColorBomb_t1194853658  : public Booster_t4064586441
{
public:
	// GameVanilla.Game.Common.ColorBlockType GameVanilla.Game.Common.ColorBomb::colorBlockType
	int32_t ___colorBlockType_5;

public:
	inline static int32_t get_offset_of_colorBlockType_5() { return static_cast<int32_t>(offsetof(ColorBomb_t1194853658, ___colorBlockType_5)); }
	inline int32_t get_colorBlockType_5() const { return ___colorBlockType_5; }
	inline int32_t* get_address_of_colorBlockType_5() { return &___colorBlockType_5; }
	inline void set_colorBlockType_5(int32_t value)
	{
		___colorBlockType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBOMB_T1194853658_H
#ifndef LOSEPOPUP_T2917929317_H
#define LOSEPOPUP_T2917929317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.LosePopup
struct  LosePopup_t2917929317  : public EndGamePopup_t660757690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSEPOPUP_T2917929317_H
#ifndef BOMB_T1148931298_H
#define BOMB_T1148931298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Bomb
struct  Bomb_t1148931298  : public Booster_t4064586441
{
public:
	// GameVanilla.Game.Common.Bomb/Direction GameVanilla.Game.Common.Bomb::direction
	int32_t ___direction_5;
	// System.Boolean GameVanilla.Game.Common.Bomb::hasMatchingCombo
	bool ___hasMatchingCombo_6;
	// System.Boolean GameVanilla.Game.Common.Bomb::hasNonMatchingCombo
	bool ___hasNonMatchingCombo_7;

public:
	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(Bomb_t1148931298, ___direction_5)); }
	inline int32_t get_direction_5() const { return ___direction_5; }
	inline int32_t* get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(int32_t value)
	{
		___direction_5 = value;
	}

	inline static int32_t get_offset_of_hasMatchingCombo_6() { return static_cast<int32_t>(offsetof(Bomb_t1148931298, ___hasMatchingCombo_6)); }
	inline bool get_hasMatchingCombo_6() const { return ___hasMatchingCombo_6; }
	inline bool* get_address_of_hasMatchingCombo_6() { return &___hasMatchingCombo_6; }
	inline void set_hasMatchingCombo_6(bool value)
	{
		___hasMatchingCombo_6 = value;
	}

	inline static int32_t get_offset_of_hasNonMatchingCombo_7() { return static_cast<int32_t>(offsetof(Bomb_t1148931298, ___hasNonMatchingCombo_7)); }
	inline bool get_hasNonMatchingCombo_7() const { return ___hasNonMatchingCombo_7; }
	inline bool* get_address_of_hasNonMatchingCombo_7() { return &___hasNonMatchingCombo_7; }
	inline void set_hasNonMatchingCombo_7(bool value)
	{
		___hasNonMatchingCombo_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOMB_T1148931298_H
#ifndef WINPOPUP_T1686196265_H
#define WINPOPUP_T1686196265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Popups.WinPopup
struct  WinPopup_t1686196265  : public EndGamePopup_t660757690
{
public:
	// UnityEngine.UI.Image GameVanilla.Game.Popups.WinPopup::star1
	Image_t2670269651 * ___star1_9;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.WinPopup::star2
	Image_t2670269651 * ___star2_10;
	// UnityEngine.UI.Image GameVanilla.Game.Popups.WinPopup::star3
	Image_t2670269651 * ___star3_11;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.WinPopup::star1Particles
	ParticleSystem_t1800779281 * ___star1Particles_12;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.WinPopup::star1WhiteParticles
	ParticleSystem_t1800779281 * ___star1WhiteParticles_13;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.WinPopup::star2Particles
	ParticleSystem_t1800779281 * ___star2Particles_14;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.WinPopup::star2WhiteParticles
	ParticleSystem_t1800779281 * ___star2WhiteParticles_15;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.WinPopup::star3Particles
	ParticleSystem_t1800779281 * ___star3Particles_16;
	// UnityEngine.ParticleSystem GameVanilla.Game.Popups.WinPopup::star3WhiteParticles
	ParticleSystem_t1800779281 * ___star3WhiteParticles_17;
	// UnityEngine.Sprite GameVanilla.Game.Popups.WinPopup::disabledStarSprite
	Sprite_t280657092 * ___disabledStarSprite_18;

public:
	inline static int32_t get_offset_of_star1_9() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star1_9)); }
	inline Image_t2670269651 * get_star1_9() const { return ___star1_9; }
	inline Image_t2670269651 ** get_address_of_star1_9() { return &___star1_9; }
	inline void set_star1_9(Image_t2670269651 * value)
	{
		___star1_9 = value;
		Il2CppCodeGenWriteBarrier((&___star1_9), value);
	}

	inline static int32_t get_offset_of_star2_10() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star2_10)); }
	inline Image_t2670269651 * get_star2_10() const { return ___star2_10; }
	inline Image_t2670269651 ** get_address_of_star2_10() { return &___star2_10; }
	inline void set_star2_10(Image_t2670269651 * value)
	{
		___star2_10 = value;
		Il2CppCodeGenWriteBarrier((&___star2_10), value);
	}

	inline static int32_t get_offset_of_star3_11() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star3_11)); }
	inline Image_t2670269651 * get_star3_11() const { return ___star3_11; }
	inline Image_t2670269651 ** get_address_of_star3_11() { return &___star3_11; }
	inline void set_star3_11(Image_t2670269651 * value)
	{
		___star3_11 = value;
		Il2CppCodeGenWriteBarrier((&___star3_11), value);
	}

	inline static int32_t get_offset_of_star1Particles_12() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star1Particles_12)); }
	inline ParticleSystem_t1800779281 * get_star1Particles_12() const { return ___star1Particles_12; }
	inline ParticleSystem_t1800779281 ** get_address_of_star1Particles_12() { return &___star1Particles_12; }
	inline void set_star1Particles_12(ParticleSystem_t1800779281 * value)
	{
		___star1Particles_12 = value;
		Il2CppCodeGenWriteBarrier((&___star1Particles_12), value);
	}

	inline static int32_t get_offset_of_star1WhiteParticles_13() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star1WhiteParticles_13)); }
	inline ParticleSystem_t1800779281 * get_star1WhiteParticles_13() const { return ___star1WhiteParticles_13; }
	inline ParticleSystem_t1800779281 ** get_address_of_star1WhiteParticles_13() { return &___star1WhiteParticles_13; }
	inline void set_star1WhiteParticles_13(ParticleSystem_t1800779281 * value)
	{
		___star1WhiteParticles_13 = value;
		Il2CppCodeGenWriteBarrier((&___star1WhiteParticles_13), value);
	}

	inline static int32_t get_offset_of_star2Particles_14() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star2Particles_14)); }
	inline ParticleSystem_t1800779281 * get_star2Particles_14() const { return ___star2Particles_14; }
	inline ParticleSystem_t1800779281 ** get_address_of_star2Particles_14() { return &___star2Particles_14; }
	inline void set_star2Particles_14(ParticleSystem_t1800779281 * value)
	{
		___star2Particles_14 = value;
		Il2CppCodeGenWriteBarrier((&___star2Particles_14), value);
	}

	inline static int32_t get_offset_of_star2WhiteParticles_15() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star2WhiteParticles_15)); }
	inline ParticleSystem_t1800779281 * get_star2WhiteParticles_15() const { return ___star2WhiteParticles_15; }
	inline ParticleSystem_t1800779281 ** get_address_of_star2WhiteParticles_15() { return &___star2WhiteParticles_15; }
	inline void set_star2WhiteParticles_15(ParticleSystem_t1800779281 * value)
	{
		___star2WhiteParticles_15 = value;
		Il2CppCodeGenWriteBarrier((&___star2WhiteParticles_15), value);
	}

	inline static int32_t get_offset_of_star3Particles_16() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star3Particles_16)); }
	inline ParticleSystem_t1800779281 * get_star3Particles_16() const { return ___star3Particles_16; }
	inline ParticleSystem_t1800779281 ** get_address_of_star3Particles_16() { return &___star3Particles_16; }
	inline void set_star3Particles_16(ParticleSystem_t1800779281 * value)
	{
		___star3Particles_16 = value;
		Il2CppCodeGenWriteBarrier((&___star3Particles_16), value);
	}

	inline static int32_t get_offset_of_star3WhiteParticles_17() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___star3WhiteParticles_17)); }
	inline ParticleSystem_t1800779281 * get_star3WhiteParticles_17() const { return ___star3WhiteParticles_17; }
	inline ParticleSystem_t1800779281 ** get_address_of_star3WhiteParticles_17() { return &___star3WhiteParticles_17; }
	inline void set_star3WhiteParticles_17(ParticleSystem_t1800779281 * value)
	{
		___star3WhiteParticles_17 = value;
		Il2CppCodeGenWriteBarrier((&___star3WhiteParticles_17), value);
	}

	inline static int32_t get_offset_of_disabledStarSprite_18() { return static_cast<int32_t>(offsetof(WinPopup_t1686196265, ___disabledStarSprite_18)); }
	inline Sprite_t280657092 * get_disabledStarSprite_18() const { return ___disabledStarSprite_18; }
	inline Sprite_t280657092 ** get_address_of_disabledStarSprite_18() { return &___disabledStarSprite_18; }
	inline void set_disabledStarSprite_18(Sprite_t280657092 * value)
	{
		___disabledStarSprite_18 = value;
		Il2CppCodeGenWriteBarrier((&___disabledStarSprite_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINPOPUP_T1686196265_H
#ifndef DYNAMITE_T2997227385_H
#define DYNAMITE_T2997227385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.Dynamite
struct  Dynamite_t2997227385  : public Booster_t4064586441
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMITE_T2997227385_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (U3CRunFadeU3Ec__Iterator0_t3294870132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[14] = 
{
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3CbgTexU3E__0_0(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_fadeColor_1(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3CimageU3E__0_2(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3CrectU3E__0_3(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3CspriteU3E__0_4(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3CnewColorU3E__0_5(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3CtimeU3E__0_6(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_duration_7(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U3ChalfDurationU3E__0_8(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_level_9(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U24this_10(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U24current_11(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U24disposing_12(),
	U3CRunFadeU3Ec__Iterator0_t3294870132::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (Block_t3916805519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[1] = 
{
	Block_t3916805519::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (Blocker_t1923197640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[1] = 
{
	Blocker_t1923197640::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (BlockerType_t1933664911)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[3] = 
{
	BlockerType_t1933664911::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (BlockType_t4096884630)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2604[12] = 
{
	BlockType_t4096884630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (Bomb_t1148931298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[3] = 
{
	Bomb_t1148931298::get_offset_of_direction_5(),
	Bomb_t1148931298::get_offset_of_hasMatchingCombo_6(),
	Bomb_t1148931298::get_offset_of_hasNonMatchingCombo_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (Direction_t250950753)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2606[3] = 
{
	Direction_t250950753::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (ComboType_t4102589120)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2607[4] = 
{
	ComboType_t4102589120::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (Booster_t4064586441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[1] = 
{
	Booster_t4064586441::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (BoosterType_t3880018546)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[5] = 
{
	BoosterType_t3880018546::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (MatchType_t2040227041)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[10] = 
{
	MatchType_t2040227041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (CoinsSystem_t740186038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[1] = 
{
	CoinsSystem_t740186038::get_offset_of_onCoinsUpdated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (ColorBlockType_t3906382570)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2612[7] = 
{
	ColorBlockType_t3906382570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (ColorBomb_t1194853658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[1] = 
{
	ColorBomb_t1194853658::get_offset_of_colorBlockType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Dynamite_t2997227385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (TileScoreOverride_t3927633711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[1] = 
{
	TileScoreOverride_t3927633711::get_offset_of_score_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (BlockScore_t3740208527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[1] = 
{
	BlockScore_t3740208527::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (BlockerScore_t111246344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[1] = 
{
	BlockerScore_t111246344::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (BoosterScore_t1409186799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[1] = 
{
	BoosterScore_t1409186799::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (GameConfiguration_t2964563698), -1, sizeof(GameConfiguration_t2964563698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[18] = 
{
	GameConfiguration_t2964563698::get_offset_of_maxLives_0(),
	GameConfiguration_t2964563698::get_offset_of_timeToNextLife_1(),
	GameConfiguration_t2964563698::get_offset_of_livesRefillCost_2(),
	GameConfiguration_t2964563698::get_offset_of_initialCoins_3(),
	GameConfiguration_t2964563698::get_offset_of_numExtraMoves_4(),
	GameConfiguration_t2964563698::get_offset_of_extraMovesCost_5(),
	GameConfiguration_t2964563698::get_offset_of_numExtraTime_6(),
	GameConfiguration_t2964563698::get_offset_of_extraTimeCost_7(),
	GameConfiguration_t2964563698::get_offset_of_defaultTileScore_8(),
	GameConfiguration_t2964563698::get_offset_of_tileScoreOverrides_9(),
	GameConfiguration_t2964563698::get_offset_of_boosterNeededMatches_10(),
	GameConfiguration_t2964563698::get_offset_of_ingameBoosterAmount_11(),
	GameConfiguration_t2964563698::get_offset_of_ingameBoosterCost_12(),
	GameConfiguration_t2964563698::get_offset_of_rewardedAdCoins_13(),
	GameConfiguration_t2964563698::get_offset_of_iapItems_14(),
	GameConfiguration_t2964563698_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
	GameConfiguration_t2964563698_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_16(),
	GameConfiguration_t2964563698_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (GamePools_t2964324386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[31] = 
{
	GamePools_t2964324386::get_offset_of_block1Pool_2(),
	GamePools_t2964324386::get_offset_of_block2Pool_3(),
	GamePools_t2964324386::get_offset_of_block3Pool_4(),
	GamePools_t2964324386::get_offset_of_block4Pool_5(),
	GamePools_t2964324386::get_offset_of_block5Pool_6(),
	GamePools_t2964324386::get_offset_of_block6Pool_7(),
	GamePools_t2964324386::get_offset_of_emptyTilePool_8(),
	GamePools_t2964324386::get_offset_of_ballPool_9(),
	GamePools_t2964324386::get_offset_of_stonePool_10(),
	GamePools_t2964324386::get_offset_of_collectablePool_11(),
	GamePools_t2964324386::get_offset_of_horizontalBombPool_12(),
	GamePools_t2964324386::get_offset_of_verticalBombPool_13(),
	GamePools_t2964324386::get_offset_of_dynamitePool_14(),
	GamePools_t2964324386::get_offset_of_colorBombPool_15(),
	GamePools_t2964324386::get_offset_of_icePool_16(),
	GamePools_t2964324386::get_offset_of_block1ParticlesPool_17(),
	GamePools_t2964324386::get_offset_of_block2ParticlesPool_18(),
	GamePools_t2964324386::get_offset_of_block3ParticlesPool_19(),
	GamePools_t2964324386::get_offset_of_block4ParticlesPool_20(),
	GamePools_t2964324386::get_offset_of_block5ParticlesPool_21(),
	GamePools_t2964324386::get_offset_of_block6ParticlesPool_22(),
	GamePools_t2964324386::get_offset_of_ballParticlesPool_23(),
	GamePools_t2964324386::get_offset_of_stoneParticlesPool_24(),
	GamePools_t2964324386::get_offset_of_collectableParticlesPool_25(),
	GamePools_t2964324386::get_offset_of_boosterSpawnParticlesPool_26(),
	GamePools_t2964324386::get_offset_of_horizontalBombParticlesPool_27(),
	GamePools_t2964324386::get_offset_of_verticalBombParticlesPool_28(),
	GamePools_t2964324386::get_offset_of_dynamiteParticlesPool_29(),
	GamePools_t2964324386::get_offset_of_colorBombParticlesPool_30(),
	GamePools_t2964324386::get_offset_of_iceParticlesPool_31(),
	GamePools_t2964324386::get_offset_of_blockPools_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (GameState_t1141404161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[3] = 
{
	GameState_t1141404161::get_offset_of_score_0(),
	GameState_t1141404161::get_offset_of_collectedBlocks_1(),
	GameState_t1141404161::get_offset_of_collectedBlockers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (Goal_t1499691635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (ReachScoreGoal_t1810434159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[1] = 
{
	ReachScoreGoal_t1810434159::get_offset_of_score_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (CollectBlockGoal_t1015685037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[2] = 
{
	CollectBlockGoal_t1015685037::get_offset_of_blockType_0(),
	CollectBlockGoal_t1015685037::get_offset_of_amount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (CollectBlockerGoal_t1534435222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[2] = 
{
	CollectBlockerGoal_t1534435222::get_offset_of_blockerType_0(),
	CollectBlockerGoal_t1534435222::get_offset_of_amount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (CoinIcon_t1300102460)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2626[7] = 
{
	CoinIcon_t1300102460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (IapItem_t3448270226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[6] = 
{
	IapItem_t3448270226::get_offset_of_storeId_0(),
	IapItem_t3448270226::get_offset_of_numCoins_1(),
	IapItem_t3448270226::get_offset_of_discount_2(),
	IapItem_t3448270226::get_offset_of_mostPopular_3(),
	IapItem_t3448270226::get_offset_of_bestValue_4(),
	IapItem_t3448270226::get_offset_of_coinIcon_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (LimitType_t3278153677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2628[3] = 
{
	LimitType_t3278153677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (Level_t3869101417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[16] = 
{
	Level_t3869101417::get_offset_of_id_0(),
	Level_t3869101417::get_offset_of_width_1(),
	Level_t3869101417::get_offset_of_height_2(),
	Level_t3869101417::get_offset_of_tiles_3(),
	Level_t3869101417::get_offset_of_limitType_4(),
	Level_t3869101417::get_offset_of_limit_5(),
	Level_t3869101417::get_offset_of_penalty_6(),
	Level_t3869101417::get_offset_of_goals_7(),
	Level_t3869101417::get_offset_of_availableColors_8(),
	Level_t3869101417::get_offset_of_score1_9(),
	Level_t3869101417::get_offset_of_score2_10(),
	Level_t3869101417::get_offset_of_score3_11(),
	Level_t3869101417::get_offset_of_awardBoostersWithRemainingMoves_12(),
	Level_t3869101417::get_offset_of_awardedBoosterType_13(),
	Level_t3869101417::get_offset_of_collectableChance_14(),
	Level_t3869101417::get_offset_of_availableBoosters_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (LevelTile_t145158312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[1] = 
{
	LevelTile_t145158312::get_offset_of_blockerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (BlockTile_t985975110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[1] = 
{
	BlockTile_t985975110::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (BoosterTile_t3880804994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[1] = 
{
	BoosterTile_t3880804994::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (LivesSystem_t2420331318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[6] = 
{
	LivesSystem_t2420331318::get_offset_of_oldDate_2(),
	LivesSystem_t2420331318::get_offset_of_timeSpan_3(),
	LivesSystem_t2420331318::get_offset_of_runningCountdown_4(),
	LivesSystem_t2420331318::get_offset_of_accTime_5(),
	LivesSystem_t2420331318::get_offset_of_onCountdownUpdated_6(),
	LivesSystem_t2420331318::get_offset_of_onCountdownFinished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (Loader_t1548667593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[1] = 
{
	Loader_t1548667593::get_offset_of_gameManager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (PuzzleMatchManager_t2899242229), -1, sizeof(PuzzleMatchManager_t2899242229_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2635[6] = 
{
	PuzzleMatchManager_t2899242229_StaticFields::get_offset_of_instance_2(),
	PuzzleMatchManager_t2899242229::get_offset_of_gameConfig_3(),
	PuzzleMatchManager_t2899242229::get_offset_of_livesSystem_4(),
	PuzzleMatchManager_t2899242229::get_offset_of_coinsSystem_5(),
	PuzzleMatchManager_t2899242229::get_offset_of_lastSelectedLevel_6(),
	PuzzleMatchManager_t2899242229::get_offset_of_unlockedNextLevel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (TileDef_t1926324708)+ sizeof (RuntimeObject), sizeof(TileDef_t1926324708 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2636[2] = 
{
	TileDef_t1926324708::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TileDef_t1926324708::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (TileEntity_t3486638455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[2] = 
{
	TileEntity_t3486638455::get_offset_of_onSpawn_2(),
	TileEntity_t3486638455::get_offset_of_onExplode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (AlertPopup_t155184926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[2] = 
{
	AlertPopup_t155184926::get_offset_of_titleText_6(),
	AlertPopup_t155184926::get_offset_of_bodyText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (BoosterAwardPopup_t3255387682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (U3CAutoCloseU3Ec__Iterator0_t1565191538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[4] = 
{
	U3CAutoCloseU3Ec__Iterator0_t1565191538::get_offset_of_U24this_0(),
	U3CAutoCloseU3Ec__Iterator0_t1565191538::get_offset_of_U24current_1(),
	U3CAutoCloseU3Ec__Iterator0_t1565191538::get_offset_of_U24disposing_2(),
	U3CAutoCloseU3Ec__Iterator0_t1565191538::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (BuyBoostersPopup_t373990552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[12] = 
{
	BuyBoostersPopup_t373990552::get_offset_of_horizontalBombSprite_6(),
	BuyBoostersPopup_t373990552::get_offset_of_verticalBombSprite_7(),
	BuyBoostersPopup_t373990552::get_offset_of_dynamiteSprite_8(),
	BuyBoostersPopup_t373990552::get_offset_of_colorBombSprite_9(),
	BuyBoostersPopup_t373990552::get_offset_of_boosterNameText_10(),
	BuyBoostersPopup_t373990552::get_offset_of_boosterDescriptionText_11(),
	BuyBoostersPopup_t373990552::get_offset_of_boosterImage_12(),
	BuyBoostersPopup_t373990552::get_offset_of_boosterAmountText_13(),
	BuyBoostersPopup_t373990552::get_offset_of_boosterCostText_14(),
	BuyBoostersPopup_t373990552::get_offset_of_numCoinsText_15(),
	BuyBoostersPopup_t373990552::get_offset_of_coinParticles_16(),
	BuyBoostersPopup_t373990552::get_offset_of_buyButton_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[1] = 
{
	U3COnBuyButtonPressedU3Ec__AnonStorey0_t1761409172::get_offset_of_scene_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (U3COnBuyButtonPressedU3Ec__AnonStorey1_t3327493113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[2] = 
{
	U3COnBuyButtonPressedU3Ec__AnonStorey1_t3327493113::get_offset_of_button_0(),
	U3COnBuyButtonPressedU3Ec__AnonStorey1_t3327493113::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (BuyCoinsPopup_t414112668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[5] = 
{
	BuyCoinsPopup_t414112668::get_offset_of_iapItemsParent_6(),
	BuyCoinsPopup_t414112668::get_offset_of_iapRowPrefab_7(),
	BuyCoinsPopup_t414112668::get_offset_of_numCoinsText_8(),
	BuyCoinsPopup_t414112668::get_offset_of_coinsParticles_9(),
	BuyCoinsPopup_t414112668::get_offset_of_loadingPopup_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (BuyLivesPopup_t1157651407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[8] = 
{
	BuyLivesPopup_t1157651407::get_offset_of_lifeSprite_6(),
	BuyLivesPopup_t1157651407::get_offset_of_lifeImages_7(),
	BuyLivesPopup_t1157651407::get_offset_of_refillCostText_8(),
	BuyLivesPopup_t1157651407::get_offset_of_timeToNextLifeText_9(),
	BuyLivesPopup_t1157651407::get_offset_of_lifeParticles_10(),
	BuyLivesPopup_t1157651407::get_offset_of_refillButton_11(),
	BuyLivesPopup_t1157651407::get_offset_of_refillButtonImage_12(),
	BuyLivesPopup_t1157651407::get_offset_of_refillButtonDisabledSprite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (U3COnRefillButtonPressedU3Ec__AnonStorey0_t3808345845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[1] = 
{
	U3COnRefillButtonPressedU3Ec__AnonStorey0_t3808345845::get_offset_of_scene_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (EndGamePopup_t660757690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[3] = 
{
	EndGamePopup_t660757690::get_offset_of_levelText_6(),
	EndGamePopup_t660757690::get_offset_of_scoreText_7(),
	EndGamePopup_t660757690::get_offset_of_goalGroup_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (ExitGamePopup_t3936844710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (InGameSettingsPopup_t2985363043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[2] = 
{
	InGameSettingsPopup_t2985363043::get_offset_of_soundButton_6(),
	InGameSettingsPopup_t2985363043::get_offset_of_musicButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (LevelGoalsPopup_t649240731), -1, sizeof(LevelGoalsPopup_t649240731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2650[9] = 
{
	LevelGoalsPopup_t649240731::get_offset_of_girlAvatarSprite_6(),
	LevelGoalsPopup_t649240731::get_offset_of_boyAvatarSprite_7(),
	LevelGoalsPopup_t649240731::get_offset_of_avatarImage_8(),
	LevelGoalsPopup_t649240731::get_offset_of_goalGroup_9(),
	LevelGoalsPopup_t649240731::get_offset_of_goalPrefab_10(),
	LevelGoalsPopup_t649240731::get_offset_of_goalHeadline_11(),
	LevelGoalsPopup_t649240731::get_offset_of_scoreGoalHeadline_12(),
	LevelGoalsPopup_t649240731::get_offset_of_scoreGoalAmountText_13(),
	LevelGoalsPopup_t649240731_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (U3CAutoKillU3Ec__Iterator0_t994986258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[5] = 
{
	U3CAutoKillU3Ec__Iterator0_t994986258::get_offset_of_U3CgameSceneU3E__0_0(),
	U3CAutoKillU3Ec__Iterator0_t994986258::get_offset_of_U24this_1(),
	U3CAutoKillU3Ec__Iterator0_t994986258::get_offset_of_U24current_2(),
	U3CAutoKillU3Ec__Iterator0_t994986258::get_offset_of_U24disposing_3(),
	U3CAutoKillU3Ec__Iterator0_t994986258::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (LoadingPopup_t2279989808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (LosePopup_t2917929317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (NoMovesOrTimePopup_t3423578163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[9] = 
{
	NoMovesOrTimePopup_t3423578163::get_offset_of_numCoinsText_6(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_movesGroup_7(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_timeGroup_8(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_title1Text_9(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_title2Text_10(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_numExtraMovesText_11(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_costText_12(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_coinParticles_13(),
	NoMovesOrTimePopup_t3423578163::get_offset_of_gameScene_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (U3COpenCoinsPopupU3Ec__AnonStorey0_t1530218158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[1] = 
{
	U3COpenCoinsPopupU3Ec__AnonStorey0_t1530218158::get_offset_of_scene_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (RegenLevelPopup_t1421666675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[1] = 
{
	RegenLevelPopup_t1421666675::get_offset_of_text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (U3CAnimateTextU3Ec__Iterator0_t3670638568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[5] = 
{
	U3CAnimateTextU3Ec__Iterator0_t3670638568::get_offset_of_U3CiU3E__1_0(),
	U3CAnimateTextU3Ec__Iterator0_t3670638568::get_offset_of_U24this_1(),
	U3CAnimateTextU3Ec__Iterator0_t3670638568::get_offset_of_U24current_2(),
	U3CAnimateTextU3Ec__Iterator0_t3670638568::get_offset_of_U24disposing_3(),
	U3CAnimateTextU3Ec__Iterator0_t3670638568::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (U3CAutoKillU3Ec__Iterator1_t1621944280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[4] = 
{
	U3CAutoKillU3Ec__Iterator1_t1621944280::get_offset_of_U24this_0(),
	U3CAutoKillU3Ec__Iterator1_t1621944280::get_offset_of_U24current_1(),
	U3CAutoKillU3Ec__Iterator1_t1621944280::get_offset_of_U24disposing_2(),
	U3CAutoKillU3Ec__Iterator1_t1621944280::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (SettingsPopup_t536495157), -1, sizeof(SettingsPopup_t536495157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2659[13] = 
{
	SettingsPopup_t536495157::get_offset_of_avatarToggleGroup_6(),
	SettingsPopup_t536495157::get_offset_of_soundSlider_7(),
	SettingsPopup_t536495157::get_offset_of_musicSlider_8(),
	SettingsPopup_t536495157::get_offset_of_notificationsToggle_9(),
	SettingsPopup_t536495157::get_offset_of_resetProgressButton_10(),
	SettingsPopup_t536495157::get_offset_of_resetProgressImage_11(),
	SettingsPopup_t536495157::get_offset_of_resetProgressDisabledSprite_12(),
	SettingsPopup_t536495157::get_offset_of_currentAvatar_13(),
	SettingsPopup_t536495157::get_offset_of_currentSound_14(),
	SettingsPopup_t536495157::get_offset_of_currentMusic_15(),
	SettingsPopup_t536495157::get_offset_of_currentNotifications_16(),
	SettingsPopup_t536495157_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
	SettingsPopup_t536495157_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (StartGamePopup_t4281035199), -1, sizeof(StartGamePopup_t4281035199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2660[12] = 
{
	StartGamePopup_t4281035199::get_offset_of_levelText_6(),
	StartGamePopup_t4281035199::get_offset_of_enabledStarSprite_7(),
	StartGamePopup_t4281035199::get_offset_of_star1Image_8(),
	StartGamePopup_t4281035199::get_offset_of_star2Image_9(),
	StartGamePopup_t4281035199::get_offset_of_star3Image_10(),
	StartGamePopup_t4281035199::get_offset_of_goalPrefab_11(),
	StartGamePopup_t4281035199::get_offset_of_goalGroup_12(),
	StartGamePopup_t4281035199::get_offset_of_goalText_13(),
	StartGamePopup_t4281035199::get_offset_of_scoreGoalTitleText_14(),
	StartGamePopup_t4281035199::get_offset_of_scoreGoalAmountText_15(),
	StartGamePopup_t4281035199::get_offset_of_numLevel_16(),
	StartGamePopup_t4281035199_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (WinPopup_t1686196265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[10] = 
{
	WinPopup_t1686196265::get_offset_of_star1_9(),
	WinPopup_t1686196265::get_offset_of_star2_10(),
	WinPopup_t1686196265::get_offset_of_star3_11(),
	WinPopup_t1686196265::get_offset_of_star1Particles_12(),
	WinPopup_t1686196265::get_offset_of_star1WhiteParticles_13(),
	WinPopup_t1686196265::get_offset_of_star2Particles_14(),
	WinPopup_t1686196265::get_offset_of_star2WhiteParticles_15(),
	WinPopup_t1686196265::get_offset_of_star3Particles_16(),
	WinPopup_t1686196265::get_offset_of_star3WhiteParticles_17(),
	WinPopup_t1686196265::get_offset_of_disabledStarSprite_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (GameScene_t3472316885), -1, sizeof(GameScene_t3472316885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2662[46] = 
{
	GameScene_t3472316885::get_offset_of_gamePools_5(),
	GameScene_t3472316885::get_offset_of_gameUi_6(),
	GameScene_t3472316885::get_offset_of_goalPrefab_7(),
	GameScene_t3472316885::get_offset_of_scoreTextPool_8(),
	GameScene_t3472316885::get_offset_of_blockFallSpeed_9(),
	GameScene_t3472316885::get_offset_of_horizontalSpacing_10(),
	GameScene_t3472316885::get_offset_of_verticalSpacing_11(),
	GameScene_t3472316885::get_offset_of_levelLocation_12(),
	GameScene_t3472316885::get_offset_of_backgroundSprite_13(),
	GameScene_t3472316885::get_offset_of_backgroundColor_14(),
	GameScene_t3472316885::get_offset_of_gameSounds_15(),
	GameScene_t3472316885::get_offset_of_level_16(),
	GameScene_t3472316885::get_offset_of_tileEntities_17(),
	GameScene_t3472316885::get_offset_of_blockers_18(),
	GameScene_t3472316885::get_offset_of_tilePositions_19(),
	GameScene_t3472316885::get_offset_of_buyBooster1Button_20(),
	GameScene_t3472316885::get_offset_of_buyBooster2Button_21(),
	GameScene_t3472316885::get_offset_of_buyBooster3Button_22(),
	GameScene_t3472316885::get_offset_of_buyBooster4Button_23(),
	GameScene_t3472316885::get_offset_of_ingameBoosterPanel_24(),
	GameScene_t3472316885::get_offset_of_ingameBoosterText_25(),
	GameScene_t3472316885::get_offset_of_gameState_26(),
	GameScene_t3472316885::get_offset_of_gameConfig_27(),
	GameScene_t3472316885::get_offset_of_gameStarted_28(),
	GameScene_t3472316885::get_offset_of_gameFinished_29(),
	GameScene_t3472316885::get_offset_of_accTime_30(),
	GameScene_t3472316885::get_offset_of_blockWidth_31(),
	GameScene_t3472316885::get_offset_of_blockHeight_32(),
	GameScene_t3472316885::get_offset_of_suggestedMatch_33(),
	GameScene_t3472316885::get_offset_of_suggestedMatchBlocks_34(),
	GameScene_t3472316885::get_offset_of_currentLimit_35(),
	GameScene_t3472316885::get_offset_of_currentlyAwardingBoosters_36(),
	GameScene_t3472316885::get_offset_of_countdownCoroutine_37(),
	GameScene_t3472316885::get_offset_of_boosterMode_38(),
	GameScene_t3472316885::get_offset_of_currentBoosterButton_39(),
	0,
	0,
	GameScene_t3472316885::get_offset_of_applyingPenalty_42(),
	GameScene_t3472316885::get_offset_of_ingameBoosterBgTweenId_43(),
	GameScene_t3472316885::get_offset_of_generatedCollectables_44(),
	GameScene_t3472316885::get_offset_of_neededCollectables_45(),
	GameScene_t3472316885::get_offset_of_ghostSkillInfo_46(),
	GameScene_t3472316885::get_offset_of_scoreText_47(),
	GameScene_t3472316885::get_offset_of_goldText_48(),
	GameScene_t3472316885::get_offset_of_lineRenderer_49(),
	GameScene_t3472316885_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (U3CUpdateU3Ec__AnonStoreyA_t3216144777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[1] = 
{
	U3CUpdateU3Ec__AnonStoreyA_t3216144777::get_offset_of_hit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (U3CUpdateU3Ec__AnonStoreyB_t1642166665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[1] = 
{
	U3CUpdateU3Ec__AnonStoreyB_t1642166665::get_offset_of_hit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (U3CDestroyBlockU3Ec__AnonStoreyC_t3224521225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[1] = 
{
	U3CDestroyBlockU3Ec__AnonStoreyC_t3224521225::get_offset_of_blockToDestroy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CDestroyBlockU3Ec__AnonStoreyD_t3224586761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[1] = 
{
	U3CDestroyBlockU3Ec__AnonStoreyD_t3224586761::get_offset_of_block_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (U3CAnimateLimitDownU3Ec__Iterator0_t2094673283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[6] = 
{
	U3CAnimateLimitDownU3Ec__Iterator0_t2094673283::get_offset_of_penalty_0(),
	U3CAnimateLimitDownU3Ec__Iterator0_t2094673283::get_offset_of_U3CendValueU3E__0_1(),
	U3CAnimateLimitDownU3Ec__Iterator0_t2094673283::get_offset_of_U24this_2(),
	U3CAnimateLimitDownU3Ec__Iterator0_t2094673283::get_offset_of_U24current_3(),
	U3CAnimateLimitDownU3Ec__Iterator0_t2094673283::get_offset_of_U24disposing_4(),
	U3CAnimateLimitDownU3Ec__Iterator0_t2094673283::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978), -1, sizeof(U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2668[18] = 
{
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CmatchTypeU3E__0_0(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CscoreU3E__0_1(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_blockToDestroy_2(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CblockIdxU3E__0_3(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CblocksToDestroyU3E__0_4(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CusedBoostersU3E__0_5(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CboosterTypeListU3E__0_6(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CaroundObjectU3E__0_7(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_isColorBombCombo_8(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U24locvar0_9(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CmaxTypeU3E__1_10(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U3CminTypeU3E__1_11(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U24this_12(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U24current_13(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U24disposing_14(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U24PC_15(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978::get_offset_of_U24locvar2_16(),
	U3CDestroyBoosterCorutineU3Ec__Iterator1_t1074004978_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[2] = 
{
	U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677::get_offset_of_blockToDestroy_0(),
	U3CDestroyBoosterCorutineU3Ec__AnonStoreyE_t3071285677::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (U3CDestroyBoosterCorutineU3Ec__AnonStoreyF_t3071285680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[2] = 
{
	U3CDestroyBoosterCorutineU3Ec__AnonStoreyF_t3071285680::get_offset_of_block_0(),
	U3CDestroyBoosterCorutineU3Ec__AnonStoreyF_t3071285680::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[13] = 
{
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_blocksToDestroy_0(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24locvar0_1(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U3CidxU3E__2_2(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_aroundObject_3(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U3CtileU3E__2_4(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24locvar2_5(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U3CindexU3E__4_6(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24this_7(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24current_8(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24disposing_9(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24PC_10(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24locvar3_11(),
	U3CColorBomb_B_MatchU3Ec__Iterator2_t3457397951::get_offset_of_U24locvar5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[2] = 
{
	U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928::get_offset_of_block_0(),
	U3CColorBomb_B_MatchU3Ec__AnonStorey10_t2426847928::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CColorBomb_B_MatchU3Ec__AnonStorey11_t88195768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[2] = 
{
	U3CColorBomb_B_MatchU3Ec__AnonStorey11_t88195768::get_offset_of_obj_0(),
	U3CColorBomb_B_MatchU3Ec__AnonStorey11_t88195768::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[2] = 
{
	U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952::get_offset_of_block_0(),
	U3CColorBomb_B_MatchU3Ec__AnonStorey12_t2809184952::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[15] = 
{
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_blocksToDestroy_0(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24locvar0_1(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U3CidxU3E__2_2(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_aroundObject_3(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U3CtileU3E__2_4(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U3Cx_ListU3E__0_5(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U3Cy_ListU3E__0_6(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24locvar2_7(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U3CindexU3E__4_8(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24this_9(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24current_10(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24disposing_11(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24PC_12(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24locvar3_13(),
	U3CColorBomb_A_MatchU3Ec__Iterator3_t3856318673::get_offset_of_U24locvar5_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330::get_offset_of_block_0(),
	U3CColorBomb_A_MatchU3Ec__AnonStorey13_t1455563330::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (U3CColorBomb_A_MatchU3Ec__AnonStorey14_t2647204418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[2] = 
{
	U3CColorBomb_A_MatchU3Ec__AnonStorey14_t2647204418::get_offset_of_obj_0(),
	U3CColorBomb_A_MatchU3Ec__AnonStorey14_t2647204418::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[2] = 
{
	U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258::get_offset_of_block_0(),
	U3CColorBomb_A_MatchU3Ec__AnonStorey15_t308552258::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (U3CDestroyBoosterRecursiveU3Ec__AnonStorey16_t3723667014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[1] = 
{
	U3CDestroyBoosterRecursiveU3Ec__AnonStorey16_t3723667014::get_offset_of_blockToDestroy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (U3CHighlightRandomMatchU3Ec__AnonStorey17_t3640827137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[1] = 
{
	U3CHighlightRandomMatchU3Ec__AnonStorey17_t3640827137::get_offset_of_tile_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (U3CRegenerateLevelU3Ec__Iterator4_t3676849939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[4] = 
{
	U3CRegenerateLevelU3Ec__Iterator4_t3676849939::get_offset_of_U24this_0(),
	U3CRegenerateLevelU3Ec__Iterator4_t3676849939::get_offset_of_U24current_1(),
	U3CRegenerateLevelU3Ec__Iterator4_t3676849939::get_offset_of_U24disposing_2(),
	U3CRegenerateLevelU3Ec__Iterator4_t3676849939::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (U3CGetMatchesU3Ec__AnonStorey18_t620215367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	U3CGetMatchesU3Ec__AnonStorey18_t620215367::get_offset_of_go_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (U3CCreateBoosterU3Ec__AnonStorey19_t596934901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[1] = 
{
	U3CCreateBoosterU3Ec__AnonStorey19_t596934901::get_offset_of_max_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[4] = 
{
	U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416::get_offset_of_U24this_0(),
	U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416::get_offset_of_U24current_1(),
	U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416::get_offset_of_U24disposing_2(),
	U3CApplyGravityAsyncU3Ec__Iterator5_t2481046416::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (U3CApplyGravityU3Ec__AnonStorey1A_t1847438783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[1] = 
{
	U3CApplyGravityU3Ec__AnonStorey1A_t1847438783::get_offset_of_tile_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (U3CApplyGravityU3Ec__AnonStorey1B_t1444154256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[1] = 
{
	U3CApplyGravityU3Ec__AnonStorey1B_t1444154256::get_offset_of_tile_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (U3CStartCountdownU3Ec__Iterator6_t3776475245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[4] = 
{
	U3CStartCountdownU3Ec__Iterator6_t3776475245::get_offset_of_U24this_0(),
	U3CStartCountdownU3Ec__Iterator6_t3776475245::get_offset_of_U24current_1(),
	U3CStartCountdownU3Ec__Iterator6_t3776475245::get_offset_of_U24disposing_2(),
	U3CStartCountdownU3Ec__Iterator6_t3776475245::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U3CAwardBoostersU3Ec__Iterator7_t3395044180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[8] = 
{
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U3CrandomIdxU3E__1_0(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U3CtileU3E__2_1(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U24locvar0_2(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U3CblockU3E__3_3(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U24this_4(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U24current_5(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U24disposing_6(),
	U3CAwardBoostersU3Ec__Iterator7_t3395044180::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316::get_offset_of_U24this_0(),
	U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316::get_offset_of_U24current_1(),
	U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316::get_offset_of_U24disposing_2(),
	U3COpenWinPopupAsyncU3Ec__Iterator8_t2899540316::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[4] = 
{
	U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982::get_offset_of_U24this_0(),
	U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982::get_offset_of_U24current_1(),
	U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982::get_offset_of_U24disposing_2(),
	U3COpenNoMovesOrTimePopupAsyncU3Ec__Iterator9_t1912861982::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (U3COnBoosterButtonPressedU3Ec__AnonStorey1C_t306576499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[1] = 
{
	U3COnBoosterButtonPressedU3Ec__AnonStorey1C_t306576499::get_offset_of_button_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (HomeScene_t444677190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	HomeScene_t444677190::get_offset_of_soundButton_5(),
	HomeScene_t444677190::get_offset_of_musicButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (LevelScene_t3942611905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[4] = 
{
	LevelScene_t3942611905::get_offset_of_scrollRect_5(),
	LevelScene_t3942611905::get_offset_of_scrollView_6(),
	LevelScene_t3942611905::get_offset_of_avatarPrefab_7(),
	LevelScene_t3942611905::get_offset_of_rewardedAdButton_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (U3CStartU3Ec__AnonStorey0_t2227182473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[2] = 
{
	U3CStartU3Ec__AnonStorey0_t2227182473::get_offset_of_avatar_0(),
	U3CStartU3Ec__AnonStorey0_t2227182473::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (BuyBoosterButton_t1892892637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[4] = 
{
	BuyBoosterButton_t1892892637::get_offset_of_boosterType_2(),
	BuyBoosterButton_t1892892637::get_offset_of_amountGroup_3(),
	BuyBoosterButton_t1892892637::get_offset_of_moreGroup_4(),
	BuyBoosterButton_t1892892637::get_offset_of_amountText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (BuyCoinsBar_t669101563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[2] = 
{
	BuyCoinsBar_t669101563::get_offset_of_levelScene_2(),
	BuyCoinsBar_t669101563::get_offset_of_numCoinsText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (BuyLivesBar_t2747199656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[9] = 
{
	BuyLivesBar_t2747199656::get_offset_of_levelScene_2(),
	BuyLivesBar_t2747199656::get_offset_of_enabledLifeSprite_3(),
	BuyLivesBar_t2747199656::get_offset_of_disabledLifeSprite_4(),
	BuyLivesBar_t2747199656::get_offset_of_lifeImage_5(),
	BuyLivesBar_t2747199656::get_offset_of_numLivesText_6(),
	BuyLivesBar_t2747199656::get_offset_of_timeToNextLifeText_7(),
	BuyLivesBar_t2747199656::get_offset_of_buttonImage_8(),
	BuyLivesBar_t2747199656::get_offset_of_enabledButtonSprite_9(),
	BuyLivesBar_t2747199656::get_offset_of_disabledButtonSprite_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (GameUi_t3170959104), -1, sizeof(GameUi_t3170959104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2698[9] = 
{
	GameUi_t3170959104::get_offset_of_limitTitleText_2(),
	GameUi_t3170959104::get_offset_of_limitText_3(),
	GameUi_t3170959104::get_offset_of_scoreText_4(),
	GameUi_t3170959104::get_offset_of_progressBar_5(),
	GameUi_t3170959104::get_offset_of_goalUi_6(),
	GameUi_t3170959104::get_offset_of_goalHeadline_7(),
	GameUi_t3170959104::get_offset_of_scoreGoalHeadline_8(),
	GameUi_t3170959104::get_offset_of_scoreGoalAmountText_9(),
	GameUi_t3170959104_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (GoalUi_t912033724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[1] = 
{
	GoalUi_t912033724::get_offset_of_group_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
