void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.Advertisements.UnityAdsSettings

		//System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_enabled()
		void Register_UnityEngine_Advertisements_UnityAdsSettings_get_enabled();
		Register_UnityEngine_Advertisements_UnityAdsSettings_get_enabled();

		//System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_initializeOnStartup()
		void Register_UnityEngine_Advertisements_UnityAdsSettings_get_initializeOnStartup();
		Register_UnityEngine_Advertisements_UnityAdsSettings_get_initializeOnStartup();

		//System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_testMode()
		void Register_UnityEngine_Advertisements_UnityAdsSettings_get_testMode();
		Register_UnityEngine_Advertisements_UnityAdsSettings_get_testMode();

		//System.String UnityEngine.Advertisements.UnityAdsSettings::GetGameId(UnityEngine.RuntimePlatform)
		void Register_UnityEngine_Advertisements_UnityAdsSettings_GetGameId();
		Register_UnityEngine_Advertisements_UnityAdsSettings_GetGameId();

	//End Registrations for type : UnityEngine.Advertisements.UnityAdsSettings

	//Start Registrations for type : UnityEngine.Analytics.CustomEventData

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddBool(System.String,System.Boolean)
		void Register_UnityEngine_Analytics_CustomEventData_AddBool();
		Register_UnityEngine_Analytics_CustomEventData_AddBool();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddByte(System.String,System.Byte)
		void Register_UnityEngine_Analytics_CustomEventData_AddByte();
		Register_UnityEngine_Analytics_CustomEventData_AddByte();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddChar(System.String,System.Char)
		void Register_UnityEngine_Analytics_CustomEventData_AddChar();
		Register_UnityEngine_Analytics_CustomEventData_AddChar();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddDouble(System.String,System.Double)
		void Register_UnityEngine_Analytics_CustomEventData_AddDouble();
		Register_UnityEngine_Analytics_CustomEventData_AddDouble();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt16(System.String,System.Int16)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt16();
		Register_UnityEngine_Analytics_CustomEventData_AddInt16();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt32(System.String,System.Int32)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt32();
		Register_UnityEngine_Analytics_CustomEventData_AddInt32();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddInt64(System.String,System.Int64)
		void Register_UnityEngine_Analytics_CustomEventData_AddInt64();
		Register_UnityEngine_Analytics_CustomEventData_AddInt64();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddSByte(System.String,System.SByte)
		void Register_UnityEngine_Analytics_CustomEventData_AddSByte();
		Register_UnityEngine_Analytics_CustomEventData_AddSByte();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddString(System.String,System.String)
		void Register_UnityEngine_Analytics_CustomEventData_AddString();
		Register_UnityEngine_Analytics_CustomEventData_AddString();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt16(System.String,System.UInt16)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt16();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt16();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt32(System.String,System.UInt32)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt32();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt32();

		//System.Boolean UnityEngine.Analytics.CustomEventData::AddUInt64(System.String,System.UInt64)
		void Register_UnityEngine_Analytics_CustomEventData_AddUInt64();
		Register_UnityEngine_Analytics_CustomEventData_AddUInt64();

		//System.Void UnityEngine.Analytics.CustomEventData::InternalCreate(System.String)
		void Register_UnityEngine_Analytics_CustomEventData_InternalCreate();
		Register_UnityEngine_Analytics_CustomEventData_InternalCreate();

		//System.Void UnityEngine.Analytics.CustomEventData::InternalDestroy()
		void Register_UnityEngine_Analytics_CustomEventData_InternalDestroy();
		Register_UnityEngine_Analytics_CustomEventData_InternalDestroy();

	//End Registrations for type : UnityEngine.Analytics.CustomEventData

	//Start Registrations for type : UnityEngine.Analytics.UnityAnalyticsHandler

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::InternalCreate()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalCreate();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalCreate();

		//System.Void UnityEngine.Analytics.UnityAnalyticsHandler::InternalDestroy()
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalDestroy();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_InternalDestroy();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::SendCustomEvent(UnityEngine.Analytics.CustomEventData)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEvent();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEvent();

		//UnityEngine.Analytics.AnalyticsResult UnityEngine.Analytics.UnityAnalyticsHandler::SendCustomEventName(System.String)
		void Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEventName();
		Register_UnityEngine_Analytics_UnityAnalyticsHandler_SendCustomEventName();

	//End Registrations for type : UnityEngine.Analytics.UnityAnalyticsHandler

	//Start Registrations for type : UnityEngine.AnchoredJoint2D

		//System.Void UnityEngine.AnchoredJoint2D::set_connectedAnchor_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_AnchoredJoint2D_set_connectedAnchor_Injected();
		Register_UnityEngine_AnchoredJoint2D_set_connectedAnchor_Injected();

	//End Registrations for type : UnityEngine.AnchoredJoint2D

	//Start Registrations for type : UnityEngine.AndroidJNI

		//System.Boolean UnityEngine.AndroidJNI::CallBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallBooleanMethod();
		Register_UnityEngine_AndroidJNI_CallBooleanMethod();

		//System.Boolean UnityEngine.AndroidJNI::CallStaticBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticBooleanMethod();
		Register_UnityEngine_AndroidJNI_CallStaticBooleanMethod();

		//System.Boolean UnityEngine.AndroidJNI::GetStaticBooleanField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticBooleanField();
		Register_UnityEngine_AndroidJNI_GetStaticBooleanField();

		//System.Boolean[] UnityEngine.AndroidJNI::FromBooleanArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromBooleanArray();
		Register_UnityEngine_AndroidJNI_FromBooleanArray();

		//System.Byte UnityEngine.AndroidJNI::CallByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallByteMethod();
		Register_UnityEngine_AndroidJNI_CallByteMethod();

		//System.Byte UnityEngine.AndroidJNI::CallStaticByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticByteMethod();
		Register_UnityEngine_AndroidJNI_CallStaticByteMethod();

		//System.Byte UnityEngine.AndroidJNI::GetStaticByteField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticByteField();
		Register_UnityEngine_AndroidJNI_GetStaticByteField();

		//System.Byte[] UnityEngine.AndroidJNI::FromByteArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromByteArray();
		Register_UnityEngine_AndroidJNI_FromByteArray();

		//System.Char UnityEngine.AndroidJNI::CallCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallCharMethod();
		Register_UnityEngine_AndroidJNI_CallCharMethod();

		//System.Char UnityEngine.AndroidJNI::CallStaticCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticCharMethod();
		Register_UnityEngine_AndroidJNI_CallStaticCharMethod();

		//System.Char UnityEngine.AndroidJNI::GetStaticCharField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticCharField();
		Register_UnityEngine_AndroidJNI_GetStaticCharField();

		//System.Char[] UnityEngine.AndroidJNI::FromCharArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromCharArray();
		Register_UnityEngine_AndroidJNI_FromCharArray();

		//System.Double UnityEngine.AndroidJNI::CallDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallDoubleMethod();
		Register_UnityEngine_AndroidJNI_CallDoubleMethod();

		//System.Double UnityEngine.AndroidJNI::CallStaticDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticDoubleMethod();
		Register_UnityEngine_AndroidJNI_CallStaticDoubleMethod();

		//System.Double UnityEngine.AndroidJNI::GetStaticDoubleField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticDoubleField();
		Register_UnityEngine_AndroidJNI_GetStaticDoubleField();

		//System.Double[] UnityEngine.AndroidJNI::FromDoubleArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromDoubleArray();
		Register_UnityEngine_AndroidJNI_FromDoubleArray();

		//System.Int16 UnityEngine.AndroidJNI::CallShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallShortMethod();
		Register_UnityEngine_AndroidJNI_CallShortMethod();

		//System.Int16 UnityEngine.AndroidJNI::CallStaticShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticShortMethod();
		Register_UnityEngine_AndroidJNI_CallStaticShortMethod();

		//System.Int16 UnityEngine.AndroidJNI::GetStaticShortField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticShortField();
		Register_UnityEngine_AndroidJNI_GetStaticShortField();

		//System.Int16[] UnityEngine.AndroidJNI::FromShortArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromShortArray();
		Register_UnityEngine_AndroidJNI_FromShortArray();

		//System.Int32 UnityEngine.AndroidJNI::CallIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallIntMethod();
		Register_UnityEngine_AndroidJNI_CallIntMethod();

		//System.Int32 UnityEngine.AndroidJNI::CallStaticIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticIntMethod();
		Register_UnityEngine_AndroidJNI_CallStaticIntMethod();

		//System.Int32 UnityEngine.AndroidJNI::GetArrayLength(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetArrayLength();
		Register_UnityEngine_AndroidJNI_GetArrayLength();

		//System.Int32 UnityEngine.AndroidJNI::GetStaticIntField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticIntField();
		Register_UnityEngine_AndroidJNI_GetStaticIntField();

		//System.Int32[] UnityEngine.AndroidJNI::FromIntArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromIntArray();
		Register_UnityEngine_AndroidJNI_FromIntArray();

		//System.Int64 UnityEngine.AndroidJNI::CallLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallLongMethod();
		Register_UnityEngine_AndroidJNI_CallLongMethod();

		//System.Int64 UnityEngine.AndroidJNI::CallStaticLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticLongMethod();
		Register_UnityEngine_AndroidJNI_CallStaticLongMethod();

		//System.Int64 UnityEngine.AndroidJNI::GetStaticLongField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticLongField();
		Register_UnityEngine_AndroidJNI_GetStaticLongField();

		//System.Int64[] UnityEngine.AndroidJNI::FromLongArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromLongArray();
		Register_UnityEngine_AndroidJNI_FromLongArray();

		//System.Single UnityEngine.AndroidJNI::CallFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallFloatMethod();
		Register_UnityEngine_AndroidJNI_CallFloatMethod();

		//System.Single UnityEngine.AndroidJNI::CallStaticFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticFloatMethod();
		Register_UnityEngine_AndroidJNI_CallStaticFloatMethod();

		//System.Single UnityEngine.AndroidJNI::GetStaticFloatField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticFloatField();
		Register_UnityEngine_AndroidJNI_GetStaticFloatField();

		//System.Single[] UnityEngine.AndroidJNI::FromFloatArray(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_FromFloatArray();
		Register_UnityEngine_AndroidJNI_FromFloatArray();

		//System.String UnityEngine.AndroidJNI::CallStaticStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticStringMethod();
		Register_UnityEngine_AndroidJNI_CallStaticStringMethod();

		//System.String UnityEngine.AndroidJNI::CallStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStringMethod();
		Register_UnityEngine_AndroidJNI_CallStringMethod();

		//System.String UnityEngine.AndroidJNI::GetStaticStringField(System.IntPtr,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStaticStringField();
		Register_UnityEngine_AndroidJNI_GetStaticStringField();

		//System.String UnityEngine.AndroidJNI::GetStringUTFChars(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_GetStringUTFChars();
		Register_UnityEngine_AndroidJNI_GetStringUTFChars();

		//System.Void UnityEngine.AndroidJNI::CallStaticVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallStaticVoidMethod();
		Register_UnityEngine_AndroidJNI_CallStaticVoidMethod();

		//System.Void UnityEngine.AndroidJNI::CallVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
		void Register_UnityEngine_AndroidJNI_CallVoidMethod();
		Register_UnityEngine_AndroidJNI_CallVoidMethod();

		//System.Void UnityEngine.AndroidJNI::DeleteGlobalRef(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_DeleteGlobalRef();
		Register_UnityEngine_AndroidJNI_DeleteGlobalRef();

		//System.Void UnityEngine.AndroidJNI::DeleteLocalRef(System.IntPtr)
		void Register_UnityEngine_AndroidJNI_DeleteLocalRef();
		Register_UnityEngine_AndroidJNI_DeleteLocalRef();

		//System.Void UnityEngine.AndroidJNI::ExceptionClear()
		void Register_UnityEngine_AndroidJNI_ExceptionClear();
		Register_UnityEngine_AndroidJNI_ExceptionClear();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_CallObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallObjectMethod();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallObjectMethod();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_CallStaticObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallStaticObjectMethod();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_CallStaticObjectMethod();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ExceptionOccurred(System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ExceptionOccurred();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ExceptionOccurred();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_FindClass(System.String,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FindClass();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FindClass();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_FromReflectedField(System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedField();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedField();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_FromReflectedMethod(System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedMethod();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_FromReflectedMethod();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetFieldID(System.IntPtr,System.String,System.String,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetFieldID();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetFieldID();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetMethodID(System.IntPtr,System.String,System.String,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetMethodID();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetMethodID();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetObjectArrayElement(System.IntPtr,System.Int32,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectArrayElement();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectArrayElement();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetObjectClass(System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectClass();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetObjectClass();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetStaticFieldID(System.IntPtr,System.String,System.String,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticFieldID();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticFieldID();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetStaticMethodID(System.IntPtr,System.String,System.String,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticMethodID();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticMethodID();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_GetStaticObjectField(System.IntPtr,System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticObjectField();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_GetStaticObjectField();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_NewGlobalRef(System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewGlobalRef();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewGlobalRef();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_NewObject(System.IntPtr,System.IntPtr,UnityEngine.jvalue[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObject();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObject();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_NewObjectArray(System.Int32,System.IntPtr,System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObjectArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewObjectArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_NewStringUTF(System.String,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewStringUTF();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_NewStringUTF();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToBooleanArray(System.Boolean[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToBooleanArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToBooleanArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToByteArray(System.Byte[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToByteArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToByteArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToCharArray(System.Char[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToCharArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToCharArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToDoubleArray(System.Double[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToDoubleArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToDoubleArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToFloatArray(System.Single[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToFloatArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToFloatArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToIntArray(System.Int32[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToIntArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToIntArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToLongArray(System.Int64[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToLongArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToLongArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToObjectArray(System.IntPtr[],System.IntPtr,System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToObjectArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToObjectArray();

		//System.Void UnityEngine.AndroidJNI::INTERNAL_CALL_ToShortArray(System.Int16[],System.IntPtr&)
		void Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToShortArray();
		Register_UnityEngine_AndroidJNI_INTERNAL_CALL_ToShortArray();

		//System.Void UnityEngine.AndroidJNI::SetObjectArrayElement(System.IntPtr,System.Int32,System.IntPtr)
		void Register_UnityEngine_AndroidJNI_SetObjectArrayElement();
		Register_UnityEngine_AndroidJNI_SetObjectArrayElement();

	//End Registrations for type : UnityEngine.AndroidJNI

	//Start Registrations for type : UnityEngine.AndroidJNIHelper

		//System.Void UnityEngine.AndroidJNIHelper::INTERNAL_CALL_CreateJavaProxy(UnityEngine.AndroidJavaProxy,System.IntPtr&)
		void Register_UnityEngine_AndroidJNIHelper_INTERNAL_CALL_CreateJavaProxy();
		Register_UnityEngine_AndroidJNIHelper_INTERNAL_CALL_CreateJavaProxy();

	//End Registrations for type : UnityEngine.AndroidJNIHelper

	//Start Registrations for type : UnityEngine.AnimationClip

		//System.Single UnityEngine.AnimationClip::get_length()
		void Register_UnityEngine_AnimationClip_get_length();
		Register_UnityEngine_AnimationClip_get_length();

		//System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();
		Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();

	//End Registrations for type : UnityEngine.AnimationClip

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Int32 UnityEngine.AnimationCurve::get_length()
		void Register_UnityEngine_AnimationCurve_get_length();
		Register_UnityEngine_AnimationCurve_get_length();

		//System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
		void Register_UnityEngine_AnimationCurve_Evaluate();
		Register_UnityEngine_AnimationCurve_Evaluate();

		//System.Void UnityEngine.AnimationCurve::Cleanup()
		void Register_UnityEngine_AnimationCurve_Cleanup();
		Register_UnityEngine_AnimationCurve_Cleanup();

		//System.Void UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)
		void Register_UnityEngine_AnimationCurve_INTERNAL_CALL_GetKey_Internal();
		Register_UnityEngine_AnimationCurve_INTERNAL_CALL_GetKey_Internal();

		//System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Init();
		Register_UnityEngine_AnimationCurve_Init();

		//System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_SetKeys();
		Register_UnityEngine_AnimationCurve_SetKeys();

		//System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_postWrapMode();
		Register_UnityEngine_AnimationCurve_set_postWrapMode();

		//System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_preWrapMode();
		Register_UnityEngine_AnimationCurve_set_preWrapMode();

		//UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
		void Register_UnityEngine_AnimationCurve_GetKeys();
		Register_UnityEngine_AnimationCurve_GetKeys();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
		void Register_UnityEngine_AnimationCurve_get_postWrapMode();
		Register_UnityEngine_AnimationCurve_get_postWrapMode();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
		void Register_UnityEngine_AnimationCurve_get_preWrapMode();
		Register_UnityEngine_AnimationCurve_get_preWrapMode();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.Animator

		//System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
		void Register_UnityEngine_Animator_get_hasBoundPlayables();
		Register_UnityEngine_Animator_get_hasBoundPlayables();

		//System.Int32 UnityEngine.Animator::GetCurrentAnimatorClipInfoCount(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorClipInfoCount();
		Register_UnityEngine_Animator_GetCurrentAnimatorClipInfoCount();

		//System.Int32 UnityEngine.Animator::GetNextAnimatorClipInfoCount(System.Int32)
		void Register_UnityEngine_Animator_GetNextAnimatorClipInfoCount();
		Register_UnityEngine_Animator_GetNextAnimatorClipInfoCount();

		//System.Int32 UnityEngine.Animator::StringToHash(System.String)
		void Register_UnityEngine_Animator_StringToHash();
		Register_UnityEngine_Animator_StringToHash();

		//System.Int32 UnityEngine.Animator::get_layerCount()
		void Register_UnityEngine_Animator_get_layerCount();
		Register_UnityEngine_Animator_get_layerCount();

		//System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
		void Register_UnityEngine_Animator_GetLayerWeight();
		Register_UnityEngine_Animator_GetLayerWeight();

		//System.Void UnityEngine.Animator::GetAnimatorClipInfoInternal(System.Int32,System.Boolean,System.Object)
		void Register_UnityEngine_Animator_GetAnimatorClipInfoInternal();
		Register_UnityEngine_Animator_GetAnimatorClipInfoInternal();

		//System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Animator_Play();
		Register_UnityEngine_Animator_Play();

		//System.Void UnityEngine.Animator::ResetTriggerString(System.String)
		void Register_UnityEngine_Animator_ResetTriggerString();
		Register_UnityEngine_Animator_ResetTriggerString();

		//System.Void UnityEngine.Animator::SetTriggerString(System.String)
		void Register_UnityEngine_Animator_SetTriggerString();
		Register_UnityEngine_Animator_SetTriggerString();

		//System.Void UnityEngine.Animator::set_speed(System.Single)
		void Register_UnityEngine_Animator_set_speed();
		Register_UnityEngine_Animator_set_speed();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();
		Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetNextAnimatorStateInfo();
		Register_UnityEngine_Animator_GetNextAnimatorStateInfo();

	//End Registrations for type : UnityEngine.Animator

	//Start Registrations for type : UnityEngine.AnimatorClipInfo

		//UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::ClipInstanceToScriptingObject(System.Int32)
		void Register_UnityEngine_AnimatorClipInfo_ClipInstanceToScriptingObject();
		Register_UnityEngine_AnimatorClipInfo_ClipInstanceToScriptingObject();

	//End Registrations for type : UnityEngine.AnimatorClipInfo

	//Start Registrations for type : UnityEngine.Application

		//System.Boolean UnityEngine.Application::get_isEditor()
		void Register_UnityEngine_Application_get_isEditor();
		Register_UnityEngine_Application_get_isEditor();

		//System.Boolean UnityEngine.Application::get_isPlaying()
		void Register_UnityEngine_Application_get_isPlaying();
		Register_UnityEngine_Application_get_isPlaying();

		//System.String UnityEngine.Application::get_persistentDataPath()
		void Register_UnityEngine_Application_get_persistentDataPath();
		Register_UnityEngine_Application_get_persistentDataPath();

		//System.String UnityEngine.Application::get_unityVersion()
		void Register_UnityEngine_Application_get_unityVersion();
		Register_UnityEngine_Application_get_unityVersion();

		//System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
		void Register_UnityEngine_Application_set_targetFrameRate();
		Register_UnityEngine_Application_set_targetFrameRate();

		//UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
		void Register_UnityEngine_Application_get_internetReachability();
		Register_UnityEngine_Application_get_internetReachability();

		//UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
		void Register_UnityEngine_Application_get_platform();
		Register_UnityEngine_Application_get_platform();

	//End Registrations for type : UnityEngine.Application

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Boolean UnityEngine.AsyncOperation::get_isDone()
		void Register_UnityEngine_AsyncOperation_get_isDone();
		Register_UnityEngine_AsyncOperation_get_isDone();

		//System.Single UnityEngine.AsyncOperation::get_progress()
		void Register_UnityEngine_AsyncOperation_get_progress();
		Register_UnityEngine_AsyncOperation_get_progress();

		//System.Void UnityEngine.AsyncOperation::InternalDestroy()
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.AudioClip

		//System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
		void Register_UnityEngine_AudioClip_GetData();
		Register_UnityEngine_AudioClip_GetData();

		//System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
		void Register_UnityEngine_AudioClip_SetData();
		Register_UnityEngine_AudioClip_SetData();

		//System.Boolean UnityEngine.AudioClip::get_ambisonic()
		void Register_UnityEngine_AudioClip_get_ambisonic();
		Register_UnityEngine_AudioClip_get_ambisonic();

		//System.Int32 UnityEngine.AudioClip::get_channels()
		void Register_UnityEngine_AudioClip_get_channels();
		Register_UnityEngine_AudioClip_get_channels();

		//System.Int32 UnityEngine.AudioClip::get_samples()
		void Register_UnityEngine_AudioClip_get_samples();
		Register_UnityEngine_AudioClip_get_samples();

		//System.Single UnityEngine.AudioClip::get_length()
		void Register_UnityEngine_AudioClip_get_length();
		Register_UnityEngine_AudioClip_get_length();

		//System.Void UnityEngine.AudioClip::Init_Internal(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_AudioClip_Init_Internal();
		Register_UnityEngine_AudioClip_Init_Internal();

		//UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
		void Register_UnityEngine_AudioClip_Construct_Internal();
		Register_UnityEngine_AudioClip_Construct_Internal();

	//End Registrations for type : UnityEngine.AudioClip

	//Start Registrations for type : UnityEngine.AudioExtensionManager

		//UnityEngine.Object UnityEngine.AudioExtensionManager::GetAudioListener()
		void Register_UnityEngine_AudioExtensionManager_GetAudioListener();
		Register_UnityEngine_AudioExtensionManager_GetAudioListener();

	//End Registrations for type : UnityEngine.AudioExtensionManager

	//Start Registrations for type : UnityEngine.AudioListener

		//System.Int32 UnityEngine.AudioListener::GetNumExtensionProperties()
		void Register_UnityEngine_AudioListener_GetNumExtensionProperties();
		Register_UnityEngine_AudioListener_GetNumExtensionProperties();

		//System.Single UnityEngine.AudioListener::ReadExtensionPropertyValue(System.Int32)
		void Register_UnityEngine_AudioListener_ReadExtensionPropertyValue();
		Register_UnityEngine_AudioListener_ReadExtensionPropertyValue();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioListener,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ClearExtensionProperties();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ClearExtensionProperties();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionName();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionName();

		//System.Void UnityEngine.AudioListener::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioListener,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName();
		Register_UnityEngine_AudioListener_INTERNAL_CALL_ReadExtensionPropertyName();

	//End Registrations for type : UnityEngine.AudioListener

	//Start Registrations for type : UnityEngine.AudioSettings

		//System.String UnityEngine.AudioSettings::GetAmbisonicDecoderPluginName()
		void Register_UnityEngine_AudioSettings_GetAmbisonicDecoderPluginName();
		Register_UnityEngine_AudioSettings_GetAmbisonicDecoderPluginName();

		//System.String UnityEngine.AudioSettings::GetSpatializerPluginName()
		void Register_UnityEngine_AudioSettings_GetSpatializerPluginName();
		Register_UnityEngine_AudioSettings_GetSpatializerPluginName();

	//End Registrations for type : UnityEngine.AudioSettings

	//Start Registrations for type : UnityEngine.AudioSource

		//System.Boolean UnityEngine.AudioSource::get_isPlaying()
		void Register_UnityEngine_AudioSource_get_isPlaying();
		Register_UnityEngine_AudioSource_get_isPlaying();

		//System.Boolean UnityEngine.AudioSource::get_spatializeInternal()
		void Register_UnityEngine_AudioSource_get_spatializeInternal();
		Register_UnityEngine_AudioSource_get_spatializeInternal();

		//System.Int32 UnityEngine.AudioSource::GetNumExtensionProperties()
		void Register_UnityEngine_AudioSource_GetNumExtensionProperties();
		Register_UnityEngine_AudioSource_GetNumExtensionProperties();

		//System.Single UnityEngine.AudioSource::ReadExtensionPropertyValue(System.Int32)
		void Register_UnityEngine_AudioSource_ReadExtensionPropertyValue();
		Register_UnityEngine_AudioSource_ReadExtensionPropertyValue();

		//System.Single UnityEngine.AudioSource::get_pitch()
		void Register_UnityEngine_AudioSource_get_pitch();
		Register_UnityEngine_AudioSource_get_pitch();

		//System.Single UnityEngine.AudioSource::get_volume()
		void Register_UnityEngine_AudioSource_get_volume();
		Register_UnityEngine_AudioSource_get_volume();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ClearExtensionProperties(UnityEngine.AudioSource,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ClearExtensionProperties();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ClearExtensionProperties();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionName();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionName();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_ReadExtensionPropertyName(UnityEngine.AudioSource,System.Int32,UnityEngine.PropertyName&)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_ReadExtensionPropertyName();

		//System.Void UnityEngine.AudioSource::Play(System.UInt64)
		void Register_UnityEngine_AudioSource_Play();
		Register_UnityEngine_AudioSource_Play();

		//System.Void UnityEngine.AudioSource::Stop()
		void Register_UnityEngine_AudioSource_Stop();
		Register_UnityEngine_AudioSource_Stop();

		//System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
		void Register_UnityEngine_AudioSource_set_clip();
		Register_UnityEngine_AudioSource_set_clip();

		//System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
		void Register_UnityEngine_AudioSource_set_loop();
		Register_UnityEngine_AudioSource_set_loop();

		//System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
		void Register_UnityEngine_AudioSource_set_minDistance();
		Register_UnityEngine_AudioSource_set_minDistance();

		//System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
		void Register_UnityEngine_AudioSource_set_mute();
		Register_UnityEngine_AudioSource_set_mute();

		//System.Void UnityEngine.AudioSource::set_pitch(System.Single)
		void Register_UnityEngine_AudioSource_set_pitch();
		Register_UnityEngine_AudioSource_set_pitch();

		//System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
		void Register_UnityEngine_AudioSource_set_spatialBlend();
		Register_UnityEngine_AudioSource_set_spatialBlend();

		//System.Void UnityEngine.AudioSource::set_volume(System.Single)
		void Register_UnityEngine_AudioSource_set_volume();
		Register_UnityEngine_AudioSource_set_volume();

		//UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
		void Register_UnityEngine_AudioSource_get_clip();
		Register_UnityEngine_AudioSource_get_clip();

	//End Registrations for type : UnityEngine.AudioSource

	//Start Registrations for type : UnityEngine.Behaviour

		//System.Boolean UnityEngine.Behaviour::get_enabled()
		void Register_UnityEngine_Behaviour_get_enabled();
		Register_UnityEngine_Behaviour_get_enabled();

		//System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
		void Register_UnityEngine_Behaviour_get_isActiveAndEnabled();
		Register_UnityEngine_Behaviour_get_isActiveAndEnabled();

		//System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
		void Register_UnityEngine_Behaviour_set_enabled();
		Register_UnityEngine_Behaviour_set_enabled();

	//End Registrations for type : UnityEngine.Behaviour

	//Start Registrations for type : UnityEngine.BoxCollider

		//System.Void UnityEngine.BoxCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_INTERNAL_set_center();
		Register_UnityEngine_BoxCollider_INTERNAL_set_center();

		//System.Void UnityEngine.BoxCollider::INTERNAL_set_size(UnityEngine.Vector3&)
		void Register_UnityEngine_BoxCollider_INTERNAL_set_size();
		Register_UnityEngine_BoxCollider_INTERNAL_set_size();

	//End Registrations for type : UnityEngine.BoxCollider

	//Start Registrations for type : UnityEngine.BoxCollider2D

		//System.Void UnityEngine.BoxCollider2D::set_size_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_BoxCollider2D_set_size_Injected();
		Register_UnityEngine_BoxCollider2D_set_size_Injected();

	//End Registrations for type : UnityEngine.BoxCollider2D

	//Start Registrations for type : UnityEngine.Camera

		//System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCameras();
		Register_UnityEngine_Camera_GetAllCameras();

		//System.Int32 UnityEngine.Camera::get_allCamerasCount()
		void Register_UnityEngine_Camera_get_allCamerasCount();
		Register_UnityEngine_Camera_get_allCamerasCount();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Int32 UnityEngine.Camera::get_targetDisplay()
		void Register_UnityEngine_Camera_get_targetDisplay();
		Register_UnityEngine_Camera_get_targetDisplay();

		//System.Single UnityEngine.Camera::get_aspect()
		void Register_UnityEngine_Camera_get_aspect();
		Register_UnityEngine_Camera_get_aspect();

		//System.Single UnityEngine.Camera::get_depth()
		void Register_UnityEngine_Camera_get_depth();
		Register_UnityEngine_Camera_get_depth();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_fieldOfView()
		void Register_UnityEngine_Camera_get_fieldOfView();
		Register_UnityEngine_Camera_get_fieldOfView();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Single UnityEngine.Camera::get_orthographicSize()
		void Register_UnityEngine_Camera_get_orthographicSize();
		Register_UnityEngine_Camera_get_orthographicSize();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint();

		//System.Void UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_get_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_get_backgroundColor();

		//System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_get_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_rect();
		Register_UnityEngine_Camera_INTERNAL_get_rect();

		//System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();

		//System.Void UnityEngine.Camera::INTERNAL_set_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_set_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_set_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_set_rect();
		Register_UnityEngine_Camera_INTERNAL_set_rect();

		//System.Void UnityEngine.Camera::set_aspect(System.Single)
		void Register_UnityEngine_Camera_set_aspect();
		Register_UnityEngine_Camera_set_aspect();

		//System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
		void Register_UnityEngine_Camera_set_farClipPlane();
		Register_UnityEngine_Camera_set_farClipPlane();

		//System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
		void Register_UnityEngine_Camera_set_fieldOfView();
		Register_UnityEngine_Camera_set_fieldOfView();

		//System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
		void Register_UnityEngine_Camera_set_nearClipPlane();
		Register_UnityEngine_Camera_set_nearClipPlane();

		//System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
		void Register_UnityEngine_Camera_set_orthographicSize();
		Register_UnityEngine_Camera_set_orthographicSize();

		//UnityEngine.Camera UnityEngine.Camera::get_main()
		void Register_UnityEngine_Camera_get_main();
		Register_UnityEngine_Camera_get_main();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Canvas

		//System.Boolean UnityEngine.Canvas::get_isRootCanvas()
		void Register_UnityEngine_Canvas_get_isRootCanvas();
		Register_UnityEngine_Canvas_get_isRootCanvas();

		//System.Boolean UnityEngine.Canvas::get_overrideSorting()
		void Register_UnityEngine_Canvas_get_overrideSorting();
		Register_UnityEngine_Canvas_get_overrideSorting();

		//System.Boolean UnityEngine.Canvas::get_pixelPerfect()
		void Register_UnityEngine_Canvas_get_pixelPerfect();
		Register_UnityEngine_Canvas_get_pixelPerfect();

		//System.Int32 UnityEngine.Canvas::get_renderOrder()
		void Register_UnityEngine_Canvas_get_renderOrder();
		Register_UnityEngine_Canvas_get_renderOrder();

		//System.Int32 UnityEngine.Canvas::get_sortingLayerID()
		void Register_UnityEngine_Canvas_get_sortingLayerID();
		Register_UnityEngine_Canvas_get_sortingLayerID();

		//System.Int32 UnityEngine.Canvas::get_sortingOrder()
		void Register_UnityEngine_Canvas_get_sortingOrder();
		Register_UnityEngine_Canvas_get_sortingOrder();

		//System.Int32 UnityEngine.Canvas::get_targetDisplay()
		void Register_UnityEngine_Canvas_get_targetDisplay();
		Register_UnityEngine_Canvas_get_targetDisplay();

		//System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
		void Register_UnityEngine_Canvas_get_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_get_referencePixelsPerUnit();

		//System.Single UnityEngine.Canvas::get_scaleFactor()
		void Register_UnityEngine_Canvas_get_scaleFactor();
		Register_UnityEngine_Canvas_get_scaleFactor();

		//System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
		void Register_UnityEngine_Canvas_set_overrideSorting();
		Register_UnityEngine_Canvas_set_overrideSorting();

		//System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
		void Register_UnityEngine_Canvas_set_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_set_referencePixelsPerUnit();

		//System.Void UnityEngine.Canvas::set_renderMode(UnityEngine.RenderMode)
		void Register_UnityEngine_Canvas_set_renderMode();
		Register_UnityEngine_Canvas_set_renderMode();

		//System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
		void Register_UnityEngine_Canvas_set_scaleFactor();
		Register_UnityEngine_Canvas_set_scaleFactor();

		//System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingLayerID();
		Register_UnityEngine_Canvas_set_sortingLayerID();

		//System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingOrder();
		Register_UnityEngine_Canvas_set_sortingOrder();

		//UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
		void Register_UnityEngine_Canvas_get_worldCamera();
		Register_UnityEngine_Canvas_get_worldCamera();

		//UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
		void Register_UnityEngine_Canvas_get_rootCanvas();
		Register_UnityEngine_Canvas_get_rootCanvas();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();

		//UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
		void Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();
		Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();

		//UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
		void Register_UnityEngine_Canvas_get_renderMode();
		Register_UnityEngine_Canvas_get_renderMode();

	//End Registrations for type : UnityEngine.Canvas

	//Start Registrations for type : UnityEngine.CanvasGroup

		//System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
		void Register_UnityEngine_CanvasGroup_get_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_get_blocksRaycasts();

		//System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
		void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();

		//System.Boolean UnityEngine.CanvasGroup::get_interactable()
		void Register_UnityEngine_CanvasGroup_get_interactable();
		Register_UnityEngine_CanvasGroup_get_interactable();

		//System.Single UnityEngine.CanvasGroup::get_alpha()
		void Register_UnityEngine_CanvasGroup_get_alpha();
		Register_UnityEngine_CanvasGroup_get_alpha();

		//System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
		void Register_UnityEngine_CanvasGroup_set_alpha();
		Register_UnityEngine_CanvasGroup_set_alpha();

		//System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_set_blocksRaycasts();

	//End Registrations for type : UnityEngine.CanvasGroup

	//Start Registrations for type : UnityEngine.CanvasRenderer

		//System.Boolean UnityEngine.CanvasRenderer::get_cull()
		void Register_UnityEngine_CanvasRenderer_get_cull();
		Register_UnityEngine_CanvasRenderer_get_cull();

		//System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
		void Register_UnityEngine_CanvasRenderer_get_hasMoved();
		Register_UnityEngine_CanvasRenderer_get_hasMoved();

		//System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
		void Register_UnityEngine_CanvasRenderer_get_absoluteDepth();
		Register_UnityEngine_CanvasRenderer_get_absoluteDepth();

		//System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
		void Register_UnityEngine_CanvasRenderer_get_materialCount();
		Register_UnityEngine_CanvasRenderer_get_materialCount();

		//System.Void UnityEngine.CanvasRenderer::Clear()
		void Register_UnityEngine_CanvasRenderer_Clear();
		Register_UnityEngine_CanvasRenderer_Clear();

		//System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();
		Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();

		//System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
		void Register_UnityEngine_CanvasRenderer_DisableRectClipping();
		Register_UnityEngine_CanvasRenderer_DisableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.CanvasRenderer::SetAlpha(System.Single)
		void Register_UnityEngine_CanvasRenderer_SetAlpha();
		Register_UnityEngine_CanvasRenderer_SetAlpha();

		//System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetAlphaTexture();
		Register_UnityEngine_CanvasRenderer_SetAlphaTexture();

		//System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetMaterial();
		Register_UnityEngine_CanvasRenderer_SetMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
		void Register_UnityEngine_CanvasRenderer_SetMesh();
		Register_UnityEngine_CanvasRenderer_SetMesh();

		//System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetPopMaterial();
		Register_UnityEngine_CanvasRenderer_SetPopMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetTexture();
		Register_UnityEngine_CanvasRenderer_SetTexture();

		//System.Void UnityEngine.CanvasRenderer::SplitIndicesStreamsInternal(System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitIndicesStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitIndicesStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_cull();
		Register_UnityEngine_CanvasRenderer_set_cull();

		//System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();
		Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();

		//System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_materialCount();
		Register_UnityEngine_CanvasRenderer_set_materialCount();

		//System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_popMaterialCount();
		Register_UnityEngine_CanvasRenderer_set_popMaterialCount();

	//End Registrations for type : UnityEngine.CanvasRenderer

	//Start Registrations for type : UnityEngine.CircleCollider2D

		//System.Void UnityEngine.CircleCollider2D::set_radius(System.Single)
		void Register_UnityEngine_CircleCollider2D_set_radius();
		Register_UnityEngine_CircleCollider2D_set_radius();

	//End Registrations for type : UnityEngine.CircleCollider2D

	//Start Registrations for type : UnityEngine.Collider

		//System.Void UnityEngine.Collider::set_enabled(System.Boolean)
		void Register_UnityEngine_Collider_set_enabled();
		Register_UnityEngine_Collider_set_enabled();

	//End Registrations for type : UnityEngine.Collider

	//Start Registrations for type : UnityEngine.Collider2D

		//System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
		void Register_UnityEngine_Collider2D_set_isTrigger();
		Register_UnityEngine_Collider2D_set_isTrigger();

		//System.Void UnityEngine.Collider2D::set_offset_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Collider2D_set_offset_Injected();
		Register_UnityEngine_Collider2D_set_offset_Injected();

	//End Registrations for type : UnityEngine.Collider2D

	//Start Registrations for type : UnityEngine.Component

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
		void Register_UnityEngine_Component_GetComponentsForListInternal();
		Register_UnityEngine_Component_GetComponentsForListInternal();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

		//UnityEngine.Transform UnityEngine.Component::get_transform()
		void Register_UnityEngine_Component_get_transform();
		Register_UnityEngine_Component_get_transform();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine()
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.CullingGroup

		//System.Void UnityEngine.CullingGroup::Dispose()
		void Register_UnityEngine_CullingGroup_Dispose();
		Register_UnityEngine_CullingGroup_Dispose();

		//System.Void UnityEngine.CullingGroup::FinalizerFailure()
		void Register_UnityEngine_CullingGroup_FinalizerFailure();
		Register_UnityEngine_CullingGroup_FinalizerFailure();

	//End Registrations for type : UnityEngine.CullingGroup

	//Start Registrations for type : UnityEngine.Cursor

		//UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
		void Register_UnityEngine_Cursor_get_lockState();
		Register_UnityEngine_Cursor_get_lockState();

	//End Registrations for type : UnityEngine.Cursor

	//Start Registrations for type : UnityEngine.Debug

		//System.Boolean UnityEngine.Debug::get_isDebugBuild()
		void Register_UnityEngine_Debug_get_isDebugBuild();
		Register_UnityEngine_Debug_get_isDebugBuild();

		//System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
		void Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.DebugLogHandler

		//System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_Log();
		Register_UnityEngine_DebugLogHandler_Internal_Log();

		//System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_LogException();
		Register_UnityEngine_DebugLogHandler_Internal_LogException();

	//End Registrations for type : UnityEngine.DebugLogHandler

	//Start Registrations for type : UnityEngine.Display

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.Event

		//System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
		void Register_UnityEngine_Event_PopEvent();
		Register_UnityEngine_Event_PopEvent();

		//System.Char UnityEngine.Event::get_character()
		void Register_UnityEngine_Event_get_character();
		Register_UnityEngine_Event_get_character();

		//System.String UnityEngine.Event::get_commandName()
		void Register_UnityEngine_Event_get_commandName();
		Register_UnityEngine_Event_get_commandName();

		//System.Void UnityEngine.Event::Cleanup()
		void Register_UnityEngine_Event_Cleanup();
		Register_UnityEngine_Event_Cleanup();

		//System.Void UnityEngine.Event::Init(System.Int32)
		void Register_UnityEngine_Event_Init();
		Register_UnityEngine_Event_Init();

		//System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMousePosition();
		Register_UnityEngine_Event_Internal_GetMousePosition();

		//System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
		void Register_UnityEngine_Event_Internal_SetNativeEvent();
		Register_UnityEngine_Event_Internal_SetNativeEvent();

		//System.Void UnityEngine.Event::set_displayIndex(System.Int32)
		void Register_UnityEngine_Event_set_displayIndex();
		Register_UnityEngine_Event_set_displayIndex();

		//UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
		void Register_UnityEngine_Event_get_modifiers();
		Register_UnityEngine_Event_get_modifiers();

		//UnityEngine.EventType UnityEngine.Event::get_rawType()
		void Register_UnityEngine_Event_get_rawType();
		Register_UnityEngine_Event_get_rawType();

		//UnityEngine.EventType UnityEngine.Event::get_type()
		void Register_UnityEngine_Event_get_type();
		Register_UnityEngine_Event_get_type();

		//UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
		void Register_UnityEngine_Event_get_keyCode();
		Register_UnityEngine_Event_get_keyCode();

	//End Registrations for type : UnityEngine.Event

	//Start Registrations for type : UnityEngine.Font

		//System.Boolean UnityEngine.Font::HasCharacter(System.Char)
		void Register_UnityEngine_Font_HasCharacter();
		Register_UnityEngine_Font_HasCharacter();

		//System.Boolean UnityEngine.Font::get_dynamic()
		void Register_UnityEngine_Font_get_dynamic();
		Register_UnityEngine_Font_get_dynamic();

		//System.Int32 UnityEngine.Font::get_fontSize()
		void Register_UnityEngine_Font_get_fontSize();
		Register_UnityEngine_Font_get_fontSize();

		//System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
		void Register_UnityEngine_Font_Internal_CreateFont();
		Register_UnityEngine_Font_Internal_CreateFont();

		//UnityEngine.Material UnityEngine.Font::get_material()
		void Register_UnityEngine_Font_get_material();
		Register_UnityEngine_Font_get_material();

	//End Registrations for type : UnityEngine.Font

	//Start Registrations for type : UnityEngine.GameObject

		//System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
		void Register_UnityEngine_GameObject_GetComponentsInternal();
		Register_UnityEngine_GameObject_GetComponentsInternal();

		//System.Boolean UnityEngine.GameObject::CompareTag(System.String)
		void Register_UnityEngine_GameObject_CompareTag();
		Register_UnityEngine_GameObject_CompareTag();

		//System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
		void Register_UnityEngine_GameObject_get_activeInHierarchy();
		Register_UnityEngine_GameObject_get_activeInHierarchy();

		//System.Boolean UnityEngine.GameObject::get_activeSelf()
		void Register_UnityEngine_GameObject_get_activeSelf();
		Register_UnityEngine_GameObject_get_activeSelf();

		//System.Int32 UnityEngine.GameObject::get_layer()
		void Register_UnityEngine_GameObject_get_layer();
		Register_UnityEngine_GameObject_get_layer();

		//System.String UnityEngine.GameObject::get_tag()
		void Register_UnityEngine_GameObject_get_tag();
		Register_UnityEngine_GameObject_get_tag();

		//System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_GameObject_GetComponentFastPath();
		Register_UnityEngine_GameObject_GetComponentFastPath();

		//System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_GameObject_Internal_CreateGameObject();
		Register_UnityEngine_GameObject_Internal_CreateGameObject();

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

		//System.Void UnityEngine.GameObject::SetActive(System.Boolean)
		void Register_UnityEngine_GameObject_SetActive();
		Register_UnityEngine_GameObject_SetActive();

		//System.Void UnityEngine.GameObject::set_isStatic(System.Boolean)
		void Register_UnityEngine_GameObject_set_isStatic();
		Register_UnityEngine_GameObject_set_isStatic();

		//System.Void UnityEngine.GameObject::set_layer(System.Int32)
		void Register_UnityEngine_GameObject_set_layer();
		Register_UnityEngine_GameObject_set_layer();

		//UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
		void Register_UnityEngine_GameObject_GetComponent();
		Register_UnityEngine_GameObject_GetComponent();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
		void Register_UnityEngine_GameObject_GetComponentInChildren();
		Register_UnityEngine_GameObject_GetComponentInChildren();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
		void Register_UnityEngine_GameObject_GetComponentInParent();
		Register_UnityEngine_GameObject_GetComponentInParent();

		//UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
		void Register_UnityEngine_GameObject_Internal_AddComponentWithType();
		Register_UnityEngine_GameObject_Internal_AddComponentWithType();

		//UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
		void Register_UnityEngine_GameObject_Find();
		Register_UnityEngine_GameObject_Find();

		//UnityEngine.Transform UnityEngine.GameObject::get_transform()
		void Register_UnityEngine_GameObject_get_transform();
		Register_UnityEngine_GameObject_get_transform();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.Gizmos

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_get_color();
		Register_UnityEngine_Gizmos_INTERNAL_get_color();

		//System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_set_color();
		Register_UnityEngine_Gizmos_INTERNAL_set_color();

	//End Registrations for type : UnityEngine.Gizmos

	//Start Registrations for type : UnityEngine.GL

		//System.Void UnityEngine.GL::Begin(System.Int32)
		void Register_UnityEngine_GL_Begin();
		Register_UnityEngine_GL_Begin();

		//System.Void UnityEngine.GL::End()
		void Register_UnityEngine_GL_End();
		Register_UnityEngine_GL_End();

		//System.Void UnityEngine.GL::ImmediateColor(System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_ImmediateColor();
		Register_UnityEngine_GL_ImmediateColor();

		//System.Void UnityEngine.GL::LoadPixelMatrix()
		void Register_UnityEngine_GL_LoadPixelMatrix();
		Register_UnityEngine_GL_LoadPixelMatrix();

		//System.Void UnityEngine.GL::PopMatrix()
		void Register_UnityEngine_GL_PopMatrix();
		Register_UnityEngine_GL_PopMatrix();

		//System.Void UnityEngine.GL::PushMatrix()
		void Register_UnityEngine_GL_PushMatrix();
		Register_UnityEngine_GL_PushMatrix();

		//System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_Vertex3();
		Register_UnityEngine_GL_Vertex3();

	//End Registrations for type : UnityEngine.GL

	//Start Registrations for type : UnityEngine.Gradient

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

		//System.Void UnityEngine.Gradient::set_alphaKeys(UnityEngine.GradientAlphaKey[])
		void Register_UnityEngine_Gradient_set_alphaKeys();
		Register_UnityEngine_Gradient_set_alphaKeys();

		//System.Void UnityEngine.Gradient::set_colorKeys(UnityEngine.GradientColorKey[])
		void Register_UnityEngine_Gradient_set_colorKeys();
		Register_UnityEngine_Gradient_set_colorKeys();

		//UnityEngine.GradientAlphaKey[] UnityEngine.Gradient::get_alphaKeys()
		void Register_UnityEngine_Gradient_get_alphaKeys();
		Register_UnityEngine_Gradient_get_alphaKeys();

		//UnityEngine.GradientColorKey[] UnityEngine.Gradient::get_colorKeys()
		void Register_UnityEngine_Gradient_get_colorKeys();
		Register_UnityEngine_Gradient_get_colorKeys();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.Graphics

		//System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
		void Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();
		Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();

		//System.Void UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)
		void Register_UnityEngine_Graphics_Internal_DrawTexture();
		Register_UnityEngine_Graphics_Internal_DrawTexture();

	//End Registrations for type : UnityEngine.Graphics

	//Start Registrations for type : UnityEngine.GUI

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_Internal_DoModalWindow(System.Int32,System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,UnityEngine.Rect&)
		void Register_UnityEngine_GUI_INTERNAL_CALL_Internal_DoModalWindow();
		Register_UnityEngine_GUI_INTERNAL_CALL_Internal_DoModalWindow();

		//System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_color();
		Register_UnityEngine_GUI_INTERNAL_get_color();

		//System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_color();
		Register_UnityEngine_GUI_INTERNAL_set_color();

		//System.Void UnityEngine.GUI::set_changed(System.Boolean)
		void Register_UnityEngine_GUI_set_changed();
		Register_UnityEngine_GUI_set_changed();

		//UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
		void Register_UnityEngine_GUI_get_blendMaterial();
		Register_UnityEngine_GUI_get_blendMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
		void Register_UnityEngine_GUI_get_blitMaterial();
		Register_UnityEngine_GUI_get_blitMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_roundedRectMaterial()
		void Register_UnityEngine_GUI_get_roundedRectMaterial();
		Register_UnityEngine_GUI_get_roundedRectMaterial();

	//End Registrations for type : UnityEngine.GUI

	//Start Registrations for type : UnityEngine.GUIClip

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_GetMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_SetMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Unclip_Vector2(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2();

	//End Registrations for type : UnityEngine.GUIClip

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
		void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.GUILayoutUtility

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_GetWindowRect(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();

	//End Registrations for type : UnityEngine.GUILayoutUtility

	//Start Registrations for type : UnityEngine.GUIStyle

		//System.Boolean UnityEngine.GUIStyle::get_richText()
		void Register_UnityEngine_GUIStyle_get_richText();
		Register_UnityEngine_GUIStyle_get_richText();

		//System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
		void Register_UnityEngine_GUIStyle_get_stretchHeight();
		Register_UnityEngine_GUIStyle_get_stretchHeight();

		//System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
		void Register_UnityEngine_GUIStyle_get_stretchWidth();
		Register_UnityEngine_GUIStyle_get_stretchWidth();

		//System.Boolean UnityEngine.GUIStyle::get_wordWrap()
		void Register_UnityEngine_GUIStyle_get_wordWrap();
		Register_UnityEngine_GUIStyle_get_wordWrap();

		//System.Int32 UnityEngine.GUIStyle::get_fontSize()
		void Register_UnityEngine_GUIStyle_get_fontSize();
		Register_UnityEngine_GUIStyle_get_fontSize();

		//System.Single UnityEngine.GUIStyle::get_fixedHeight()
		void Register_UnityEngine_GUIStyle_get_fixedHeight();
		Register_UnityEngine_GUIStyle_get_fixedHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedWidth()
		void Register_UnityEngine_GUIStyle_get_fixedWidth();
		Register_UnityEngine_GUIStyle_get_fixedWidth();

		//System.String UnityEngine.GUIStyle::get_name()
		void Register_UnityEngine_GUIStyle_get_name();
		Register_UnityEngine_GUIStyle_get_name();

		//System.Void UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignRectOffset();
		Register_UnityEngine_GUIStyle_AssignRectOffset();

		//System.Void UnityEngine.GUIStyle::AssignStyleState(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignStyleState();
		Register_UnityEngine_GUIStyle_AssignStyleState();

		//System.Void UnityEngine.GUIStyle::Cleanup()
		void Register_UnityEngine_GUIStyle_Cleanup();
		Register_UnityEngine_GUIStyle_Cleanup();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_GetRectOffsetPtr(UnityEngine.GUIStyle,System.Int32,System.IntPtr&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_GetStyleStatePtr(UnityEngine.GUIStyle,System.Int32,System.IntPtr&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetStyleStatePtr();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetStyleStatePtr();

		//System.Void UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset();

		//System.Void UnityEngine.GUIStyle::Init()
		void Register_UnityEngine_GUIStyle_Init();
		Register_UnityEngine_GUIStyle_Init();

		//System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetDefaultFont();
		Register_UnityEngine_GUIStyle_SetDefaultFont();

		//System.Void UnityEngine.GUIStyle::SetFontInternal(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetFontInternal();
		Register_UnityEngine_GUIStyle_SetFontInternal();

		//System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
		void Register_UnityEngine_GUIStyle_set_alignment();
		Register_UnityEngine_GUIStyle_set_alignment();

		//System.Void UnityEngine.GUIStyle::set_clipping(UnityEngine.TextClipping)
		void Register_UnityEngine_GUIStyle_set_clipping();
		Register_UnityEngine_GUIStyle_set_clipping();

		//System.Void UnityEngine.GUIStyle::set_fixedHeight(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedHeight();
		Register_UnityEngine_GUIStyle_set_fixedHeight();

		//System.Void UnityEngine.GUIStyle::set_fixedWidth(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedWidth();
		Register_UnityEngine_GUIStyle_set_fixedWidth();

		//System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
		void Register_UnityEngine_GUIStyle_set_fontSize();
		Register_UnityEngine_GUIStyle_set_fontSize();

		//System.Void UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)
		void Register_UnityEngine_GUIStyle_set_fontStyle();
		Register_UnityEngine_GUIStyle_set_fontStyle();

		//System.Void UnityEngine.GUIStyle::set_imagePosition(UnityEngine.ImagePosition)
		void Register_UnityEngine_GUIStyle_set_imagePosition();
		Register_UnityEngine_GUIStyle_set_imagePosition();

		//System.Void UnityEngine.GUIStyle::set_name(System.String)
		void Register_UnityEngine_GUIStyle_set_name();
		Register_UnityEngine_GUIStyle_set_name();

		//System.Void UnityEngine.GUIStyle::set_richText(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_richText();
		Register_UnityEngine_GUIStyle_set_richText();

		//System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchHeight();
		Register_UnityEngine_GUIStyle_set_stretchHeight();

		//System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchWidth();
		Register_UnityEngine_GUIStyle_set_stretchWidth();

		//System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_wordWrap();
		Register_UnityEngine_GUIStyle_set_wordWrap();

		//UnityEngine.Font UnityEngine.GUIStyle::GetFontInternal()
		void Register_UnityEngine_GUIStyle_GetFontInternal();
		Register_UnityEngine_GUIStyle_GetFontInternal();

		//UnityEngine.FontStyle UnityEngine.GUIStyle::get_fontStyle()
		void Register_UnityEngine_GUIStyle_get_fontStyle();
		Register_UnityEngine_GUIStyle_get_fontStyle();

		//UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
		void Register_UnityEngine_GUIStyle_get_imagePosition();
		Register_UnityEngine_GUIStyle_get_imagePosition();

		//UnityEngine.TextAnchor UnityEngine.GUIStyle::get_alignment()
		void Register_UnityEngine_GUIStyle_get_alignment();
		Register_UnityEngine_GUIStyle_get_alignment();

		//UnityEngine.TextClipping UnityEngine.GUIStyle::get_clipping()
		void Register_UnityEngine_GUIStyle_get_clipping();
		Register_UnityEngine_GUIStyle_get_clipping();

	//End Registrations for type : UnityEngine.GUIStyle

	//Start Registrations for type : UnityEngine.GUIStyleState

		//System.Void UnityEngine.GUIStyleState::Cleanup()
		void Register_UnityEngine_GUIStyleState_Cleanup();
		Register_UnityEngine_GUIStyleState_Cleanup();

		//System.Void UnityEngine.GUIStyleState::INTERNAL_get_textColor(UnityEngine.Color&)
		void Register_UnityEngine_GUIStyleState_INTERNAL_get_textColor();
		Register_UnityEngine_GUIStyleState_INTERNAL_get_textColor();

		//System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
		void Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();
		Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();

		//System.Void UnityEngine.GUIStyleState::Init()
		void Register_UnityEngine_GUIStyleState_Init();
		Register_UnityEngine_GUIStyleState_Init();

		//System.Void UnityEngine.GUIStyleState::SetBackgroundInternal(UnityEngine.Texture2D)
		void Register_UnityEngine_GUIStyleState_SetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_SetBackgroundInternal();

		//UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
		void Register_UnityEngine_GUIStyleState_GetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_GetBackgroundInternal();

	//End Registrations for type : UnityEngine.GUIStyleState

	//Start Registrations for type : UnityEngine.GUIUtility

		//System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
		void Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();
		Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();

		//System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
		void Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();
		Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();

		//System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
		void Register_UnityEngine_GUIUtility_get_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_get_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
		void Register_UnityEngine_GUIUtility_Internal_ExitGUI();
		Register_UnityEngine_GUIUtility_Internal_ExitGUI();

		//System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
		void Register_UnityEngine_GUIUtility_set_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_set_systemCopyBuffer();

		//UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();
		Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();

	//End Registrations for type : UnityEngine.GUIUtility

	//Start Registrations for type : UnityEngine.Gyroscope

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_attitude_Internal(System.Int32,UnityEngine.Quaternion&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_attitude_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_attitude_Internal();

		//System.Void UnityEngine.Gyroscope::INTERNAL_CALL_rotationRateUnbiased_Internal(System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_Gyroscope_INTERNAL_CALL_rotationRateUnbiased_Internal();
		Register_UnityEngine_Gyroscope_INTERNAL_CALL_rotationRateUnbiased_Internal();

		//System.Void UnityEngine.Gyroscope::setEnabled_Internal(System.Int32,System.Boolean)
		void Register_UnityEngine_Gyroscope_setEnabled_Internal();
		Register_UnityEngine_Gyroscope_setEnabled_Internal();

	//End Registrations for type : UnityEngine.Gyroscope

	//Start Registrations for type : UnityEngine.HingeJoint

		//System.Void UnityEngine.HingeJoint::INTERNAL_set_limits(UnityEngine.JointLimits&)
		void Register_UnityEngine_HingeJoint_INTERNAL_set_limits();
		Register_UnityEngine_HingeJoint_INTERNAL_set_limits();

		//System.Void UnityEngine.HingeJoint::set_useLimits(System.Boolean)
		void Register_UnityEngine_HingeJoint_set_useLimits();
		Register_UnityEngine_HingeJoint_set_useLimits();

	//End Registrations for type : UnityEngine.HingeJoint

	//Start Registrations for type : UnityEngine.HingeJoint2D

		//System.Void UnityEngine.HingeJoint2D::set_limits_Injected(UnityEngine.JointAngleLimits2D&)
		void Register_UnityEngine_HingeJoint2D_set_limits_Injected();
		Register_UnityEngine_HingeJoint2D_set_limits_Injected();

		//System.Void UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)
		void Register_UnityEngine_HingeJoint2D_set_useLimits();
		Register_UnityEngine_HingeJoint2D_set_useLimits();

	//End Registrations for type : UnityEngine.HingeJoint2D

	//Start Registrations for type : UnityEngine.ImageConversion

		//System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[],System.Boolean)
		void Register_UnityEngine_ImageConversion_LoadImage();
		Register_UnityEngine_ImageConversion_LoadImage();

	//End Registrations for type : UnityEngine.ImageConversion

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetButtonDown(System.String)
		void Register_UnityEngine_Input_GetButtonDown();
		Register_UnityEngine_Input_GetButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonUp();
		Register_UnityEngine_Input_GetMouseButtonUp();

		//System.Boolean UnityEngine.Input::get_mousePresent()
		void Register_UnityEngine_Input_get_mousePresent();
		Register_UnityEngine_Input_get_mousePresent();

		//System.Boolean UnityEngine.Input::get_touchSupported()
		void Register_UnityEngine_Input_get_touchSupported();
		Register_UnityEngine_Input_get_touchSupported();

		//System.Int32 UnityEngine.Input::get_touchCount()
		void Register_UnityEngine_Input_get_touchCount();
		Register_UnityEngine_Input_get_touchCount();

		//System.Int32 UnityEngine.Input::mainGyroIndex_Internal()
		void Register_UnityEngine_Input_mainGyroIndex_Internal();
		Register_UnityEngine_Input_mainGyroIndex_Internal();

		//System.Single UnityEngine.Input::GetAxisRaw(System.String)
		void Register_UnityEngine_Input_GetAxisRaw();
		Register_UnityEngine_Input_GetAxisRaw();

		//System.String UnityEngine.Input::get_compositionString()
		void Register_UnityEngine_Input_get_compositionString();
		Register_UnityEngine_Input_get_compositionString();

		//System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
		void Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();
		Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();

		//System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();

		//System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_mousePosition();
		Register_UnityEngine_Input_INTERNAL_get_mousePosition();

		//System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();
		Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();

		//System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();

		//System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
		void Register_UnityEngine_Input_set_imeCompositionMode();
		Register_UnityEngine_Input_set_imeCompositionMode();

		//UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
		void Register_UnityEngine_Input_get_imeCompositionMode();
		Register_UnityEngine_Input_get_imeCompositionMode();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.iOS.NotificationHelper

		//System.Void UnityEngine.iOS.NotificationHelper::DestroyLocal(System.IntPtr)
		void Register_UnityEngine_iOS_NotificationHelper_DestroyLocal();
		Register_UnityEngine_iOS_NotificationHelper_DestroyLocal();

		//System.Void UnityEngine.iOS.NotificationHelper::DestroyRemote(System.IntPtr)
		void Register_UnityEngine_iOS_NotificationHelper_DestroyRemote();
		Register_UnityEngine_iOS_NotificationHelper_DestroyRemote();

	//End Registrations for type : UnityEngine.iOS.NotificationHelper

	//Start Registrations for type : UnityEngine.Joint

		//System.Void UnityEngine.Joint::INTERNAL_get_connectedAnchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_get_connectedAnchor();
		Register_UnityEngine_Joint_INTERNAL_get_connectedAnchor();

		//System.Void UnityEngine.Joint::INTERNAL_set_axis(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_set_axis();
		Register_UnityEngine_Joint_INTERNAL_set_axis();

		//System.Void UnityEngine.Joint::INTERNAL_set_connectedAnchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_set_connectedAnchor();
		Register_UnityEngine_Joint_INTERNAL_set_connectedAnchor();

		//System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
		void Register_UnityEngine_Joint_set_connectedBody();
		Register_UnityEngine_Joint_set_connectedBody();

		//System.Void UnityEngine.Joint::set_enableCollision(System.Boolean)
		void Register_UnityEngine_Joint_set_enableCollision();
		Register_UnityEngine_Joint_set_enableCollision();

		//UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
		void Register_UnityEngine_Joint_get_connectedBody();
		Register_UnityEngine_Joint_get_connectedBody();

	//End Registrations for type : UnityEngine.Joint

	//Start Registrations for type : UnityEngine.Joint2D

		//System.Void UnityEngine.Joint2D::set_connectedBody(UnityEngine.Rigidbody2D)
		void Register_UnityEngine_Joint2D_set_connectedBody();
		Register_UnityEngine_Joint2D_set_connectedBody();

		//UnityEngine.Rigidbody2D UnityEngine.Joint2D::get_connectedBody()
		void Register_UnityEngine_Joint2D_get_connectedBody();
		Register_UnityEngine_Joint2D_get_connectedBody();

	//End Registrations for type : UnityEngine.Joint2D

	//Start Registrations for type : UnityEngine.JsonUtility

		//System.Object UnityEngine.JsonUtility::FromJson(System.String,System.Type)
		void Register_UnityEngine_JsonUtility_FromJson();
		Register_UnityEngine_JsonUtility_FromJson();

		//System.String UnityEngine.JsonUtility::ToJson(System.Object,System.Boolean)
		void Register_UnityEngine_JsonUtility_ToJson();
		Register_UnityEngine_JsonUtility_ToJson();

	//End Registrations for type : UnityEngine.JsonUtility

	//Start Registrations for type : UnityEngine.Light

		//System.Single UnityEngine.Light::get_intensity()
		void Register_UnityEngine_Light_get_intensity();
		Register_UnityEngine_Light_get_intensity();

		//System.Single UnityEngine.Light::get_shadowStrength()
		void Register_UnityEngine_Light_get_shadowStrength();
		Register_UnityEngine_Light_get_shadowStrength();

		//System.Void UnityEngine.Light::get_color_Injected(UnityEngine.Color&)
		void Register_UnityEngine_Light_get_color_Injected();
		Register_UnityEngine_Light_get_color_Injected();

		//System.Void UnityEngine.Light::set_color_Injected(UnityEngine.Color&)
		void Register_UnityEngine_Light_set_color_Injected();
		Register_UnityEngine_Light_set_color_Injected();

		//System.Void UnityEngine.Light::set_intensity(System.Single)
		void Register_UnityEngine_Light_set_intensity();
		Register_UnityEngine_Light_set_intensity();

		//System.Void UnityEngine.Light::set_shadowStrength(System.Single)
		void Register_UnityEngine_Light_set_shadowStrength();
		Register_UnityEngine_Light_set_shadowStrength();

	//End Registrations for type : UnityEngine.Light

	//Start Registrations for type : UnityEngine.LineRenderer

		//System.Void UnityEngine.LineRenderer::set_endColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_LineRenderer_set_endColor_Injected();
		Register_UnityEngine_LineRenderer_set_endColor_Injected();

		//System.Void UnityEngine.LineRenderer::set_startColor_Injected(UnityEngine.Color&)
		void Register_UnityEngine_LineRenderer_set_startColor_Injected();
		Register_UnityEngine_LineRenderer_set_startColor_Injected();

	//End Registrations for type : UnityEngine.LineRenderer

	//Start Registrations for type : UnityEngine.Material

		//System.Boolean UnityEngine.Material::HasProperty(System.Int32)
		void Register_UnityEngine_Material_HasProperty();
		Register_UnityEngine_Material_HasProperty();

		//System.Boolean UnityEngine.Material::SetPass(System.Int32)
		void Register_UnityEngine_Material_SetPass();
		Register_UnityEngine_Material_SetPass();

		//System.Single UnityEngine.Material::GetFloatImpl(System.Int32)
		void Register_UnityEngine_Material_GetFloatImpl();
		Register_UnityEngine_Material_GetFloatImpl();

		//System.String[] UnityEngine.Material::get_shaderKeywords()
		void Register_UnityEngine_Material_get_shaderKeywords();
		Register_UnityEngine_Material_get_shaderKeywords();

		//System.Void UnityEngine.Material::CopyPropertiesFromMaterial(UnityEngine.Material)
		void Register_UnityEngine_Material_CopyPropertiesFromMaterial();
		Register_UnityEngine_Material_CopyPropertiesFromMaterial();

		//System.Void UnityEngine.Material::DisableKeyword(System.String)
		void Register_UnityEngine_Material_DisableKeyword();
		Register_UnityEngine_Material_DisableKeyword();

		//System.Void UnityEngine.Material::EnableKeyword(System.String)
		void Register_UnityEngine_Material_EnableKeyword();
		Register_UnityEngine_Material_EnableKeyword();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetColorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_GetColorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetTextureScaleAndOffsetImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetTextureScaleAndOffsetImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_GetTextureScaleAndOffsetImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetVectorImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetVectorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_GetVectorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetColorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetColorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffsetImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetTextureOffsetImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetTextureOffsetImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureScaleImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetTextureScaleImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetTextureScaleImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetVectorImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetVectorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetVectorImpl();

		//System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
		void Register_UnityEngine_Material_Internal_CreateWithMaterial();
		Register_UnityEngine_Material_Internal_CreateWithMaterial();

		//System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
		void Register_UnityEngine_Material_Internal_CreateWithShader();
		Register_UnityEngine_Material_Internal_CreateWithShader();

		//System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_Material_SetFloatImpl();
		Register_UnityEngine_Material_SetFloatImpl();

		//System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
		void Register_UnityEngine_Material_SetIntImpl();
		Register_UnityEngine_Material_SetIntImpl();

		//System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Material_SetTextureImpl();
		Register_UnityEngine_Material_SetTextureImpl();

		//System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
		void Register_UnityEngine_Material_set_shader();
		Register_UnityEngine_Material_set_shader();

		//System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
		void Register_UnityEngine_Material_set_shaderKeywords();
		Register_UnityEngine_Material_set_shaderKeywords();

		//UnityEngine.Shader UnityEngine.Material::get_shader()
		void Register_UnityEngine_Material_get_shader();
		Register_UnityEngine_Material_get_shader();

		//UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
		void Register_UnityEngine_Material_GetTextureImpl();
		Register_UnityEngine_Material_GetTextureImpl();

	//End Registrations for type : UnityEngine.Material

	//Start Registrations for type : UnityEngine.MaterialPropertyBlock

		//System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
		void Register_UnityEngine_MaterialPropertyBlock_DestroyBlock();
		Register_UnityEngine_MaterialPropertyBlock_DestroyBlock();

		//System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
		void Register_UnityEngine_MaterialPropertyBlock_InitBlock();
		Register_UnityEngine_MaterialPropertyBlock_InitBlock();

	//End Registrations for type : UnityEngine.MaterialPropertyBlock

	//Start Registrations for type : UnityEngine.Mathf

		//System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
		void Register_UnityEngine_Mathf_GammaToLinearSpace();
		Register_UnityEngine_Mathf_GammaToLinearSpace();

		//System.Single UnityEngine.Mathf::LinearToGammaSpace(System.Single)
		void Register_UnityEngine_Mathf_LinearToGammaSpace();
		Register_UnityEngine_Mathf_LinearToGammaSpace();

		//System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
		void Register_UnityEngine_Mathf_PerlinNoise();
		Register_UnityEngine_Mathf_PerlinNoise();

	//End Registrations for type : UnityEngine.Mathf

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Mesh

		//System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
		void Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();
		Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();

		//System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_HasChannel();
		Register_UnityEngine_Mesh_HasChannel();

		//System.Boolean UnityEngine.Mesh::get_canAccess()
		void Register_UnityEngine_Mesh_get_canAccess();
		Register_UnityEngine_Mesh_get_canAccess();

		//System.Int32 UnityEngine.Mesh::get_subMeshCount()
		void Register_UnityEngine_Mesh_get_subMeshCount();
		Register_UnityEngine_Mesh_get_subMeshCount();

		//System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32,System.Boolean)
		void Register_UnityEngine_Mesh_GetIndicesImpl();
		Register_UnityEngine_Mesh_GetIndicesImpl();

		//System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
		void Register_UnityEngine_Mesh_ClearImpl();
		Register_UnityEngine_Mesh_ClearImpl();

		//System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
		void Register_UnityEngine_Mesh_Internal_Create();
		Register_UnityEngine_Mesh_Internal_Create();

		//System.Void UnityEngine.Mesh::MarkDynamicImpl()
		void Register_UnityEngine_Mesh_MarkDynamicImpl();
		Register_UnityEngine_Mesh_MarkDynamicImpl();

		//System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_PrintErrorCantAccessChannel();
		Register_UnityEngine_Mesh_PrintErrorCantAccessChannel();

		//System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
		void Register_UnityEngine_Mesh_RecalculateBoundsImpl();
		Register_UnityEngine_Mesh_RecalculateBoundsImpl();

		//System.Void UnityEngine.Mesh::RecalculateNormalsImpl()
		void Register_UnityEngine_Mesh_RecalculateNormalsImpl();
		Register_UnityEngine_Mesh_RecalculateNormalsImpl();

		//System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
		void Register_UnityEngine_Mesh_SetArrayForChannelImpl();
		Register_UnityEngine_Mesh_SetArrayForChannelImpl();

		//System.Void UnityEngine.Mesh::SetIndicesImpl(System.Int32,UnityEngine.MeshTopology,System.Array,System.Int32,System.Boolean,System.Int32)
		void Register_UnityEngine_Mesh_SetIndicesImpl();
		Register_UnityEngine_Mesh_SetIndicesImpl();

		//System.Void UnityEngine.Mesh::set_bounds_Injected(UnityEngine.Bounds&)
		void Register_UnityEngine_Mesh_set_bounds_Injected();
		Register_UnityEngine_Mesh_set_bounds_Injected();

		//System.Void UnityEngine.Mesh::set_subMeshCount(System.Int32)
		void Register_UnityEngine_Mesh_set_subMeshCount();
		Register_UnityEngine_Mesh_set_subMeshCount();

	//End Registrations for type : UnityEngine.Mesh

	//Start Registrations for type : UnityEngine.MeshFilter

		//System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
		void Register_UnityEngine_MeshFilter_set_sharedMesh();
		Register_UnityEngine_MeshFilter_set_sharedMesh();

		//UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
		void Register_UnityEngine_MeshFilter_get_mesh();
		Register_UnityEngine_MeshFilter_get_mesh();

		//UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
		void Register_UnityEngine_MeshFilter_get_sharedMesh();
		Register_UnityEngine_MeshFilter_get_sharedMesh();

	//End Registrations for type : UnityEngine.MeshFilter

	//Start Registrations for type : UnityEngine.MonoBehaviour

		//System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
		void Register_UnityEngine_MonoBehaviour_CancelInvoke();
		Register_UnityEngine_MonoBehaviour_CancelInvoke();

		//System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
		void Register_UnityEngine_MonoBehaviour_Invoke();
		Register_UnityEngine_MonoBehaviour_Invoke();

		//System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
		void Register_UnityEngine_MonoBehaviour_InvokeRepeating();
		Register_UnityEngine_MonoBehaviour_InvokeRepeating();

		//System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
		void Register_UnityEngine_MonoBehaviour_StopAllCoroutines();
		Register_UnityEngine_MonoBehaviour_StopAllCoroutines();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine();
		Register_UnityEngine_MonoBehaviour_StopCoroutine();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine();
		Register_UnityEngine_MonoBehaviour_StartCoroutine();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal();
		Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal();

	//End Registrations for type : UnityEngine.MonoBehaviour

	//Start Registrations for type : UnityEngine.Networking.DownloadHandler

		//System.Byte[] UnityEngine.Networking.DownloadHandler::InternalGetByteArray(UnityEngine.Networking.DownloadHandler)
		void Register_UnityEngine_Networking_DownloadHandler_InternalGetByteArray();
		Register_UnityEngine_Networking_DownloadHandler_InternalGetByteArray();

		//System.String UnityEngine.Networking.DownloadHandler::GetContentType()
		void Register_UnityEngine_Networking_DownloadHandler_GetContentType();
		Register_UnityEngine_Networking_DownloadHandler_GetContentType();

		//System.Void UnityEngine.Networking.DownloadHandler::Release()
		void Register_UnityEngine_Networking_DownloadHandler_Release();
		Register_UnityEngine_Networking_DownloadHandler_Release();

	//End Registrations for type : UnityEngine.Networking.DownloadHandler

	//Start Registrations for type : UnityEngine.Networking.DownloadHandlerBuffer

		//System.IntPtr UnityEngine.Networking.DownloadHandlerBuffer::Create(UnityEngine.Networking.DownloadHandlerBuffer)
		void Register_UnityEngine_Networking_DownloadHandlerBuffer_Create();
		Register_UnityEngine_Networking_DownloadHandlerBuffer_Create();

	//End Registrations for type : UnityEngine.Networking.DownloadHandlerBuffer

	//Start Registrations for type : UnityEngine.Networking.UnityWebRequest

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isDone();
		Register_UnityEngine_Networking_UnityWebRequest_get_isDone();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isModifiable();
		Register_UnityEngine_Networking_UnityWebRequest_get_isModifiable();

		//System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
		void Register_UnityEngine_Networking_UnityWebRequest_get_isNetworkError();
		Register_UnityEngine_Networking_UnityWebRequest_get_isNetworkError();

		//System.IntPtr UnityEngine.Networking.UnityWebRequest::Create()
		void Register_UnityEngine_Networking_UnityWebRequest_Create();
		Register_UnityEngine_Networking_UnityWebRequest_Create();

		//System.String UnityEngine.Networking.UnityWebRequest::GetUrl()
		void Register_UnityEngine_Networking_UnityWebRequest_GetUrl();
		Register_UnityEngine_Networking_UnityWebRequest_GetUrl();

		//System.String UnityEngine.Networking.UnityWebRequest::GetWebErrorString(UnityEngine.Networking.UnityWebRequest/UnityWebRequestError)
		void Register_UnityEngine_Networking_UnityWebRequest_GetWebErrorString();
		Register_UnityEngine_Networking_UnityWebRequest_GetWebErrorString();

		//System.Void UnityEngine.Networking.UnityWebRequest::Abort()
		void Register_UnityEngine_Networking_UnityWebRequest_Abort();
		Register_UnityEngine_Networking_UnityWebRequest_Abort();

		//System.Void UnityEngine.Networking.UnityWebRequest::Release()
		void Register_UnityEngine_Networking_UnityWebRequest_Release();
		Register_UnityEngine_Networking_UnityWebRequest_Release();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCustomMethod(System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_SetCustomMethod();
		Register_UnityEngine_Networking_UnityWebRequest_SetCustomMethod();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetDownloadHandler(UnityEngine.Networking.DownloadHandler)
		void Register_UnityEngine_Networking_UnityWebRequest_SetDownloadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_SetDownloadHandler();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)
		void Register_UnityEngine_Networking_UnityWebRequest_SetMethod();
		Register_UnityEngine_Networking_UnityWebRequest_SetMethod();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUploadHandler(UnityEngine.Networking.UploadHandler)
		void Register_UnityEngine_Networking_UnityWebRequest_SetUploadHandler();
		Register_UnityEngine_Networking_UnityWebRequest_SetUploadHandler();

		//UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUrl(System.String)
		void Register_UnityEngine_Networking_UnityWebRequest_SetUrl();
		Register_UnityEngine_Networking_UnityWebRequest_SetUrl();

		//UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::BeginWebRequest()
		void Register_UnityEngine_Networking_UnityWebRequest_BeginWebRequest();
		Register_UnityEngine_Networking_UnityWebRequest_BeginWebRequest();

	//End Registrations for type : UnityEngine.Networking.UnityWebRequest

	//Start Registrations for type : UnityEngine.Networking.UploadHandler

		//System.Void UnityEngine.Networking.UploadHandler::Release()
		void Register_UnityEngine_Networking_UploadHandler_Release();
		Register_UnityEngine_Networking_UploadHandler_Release();

	//End Registrations for type : UnityEngine.Networking.UploadHandler

	//Start Registrations for type : UnityEngine.NoAllocHelpers

		//System.Array UnityEngine.NoAllocHelpers::ExtractArrayFromList(System.Object)
		void Register_UnityEngine_NoAllocHelpers_ExtractArrayFromList();
		Register_UnityEngine_NoAllocHelpers_ExtractArrayFromList();

	//End Registrations for type : UnityEngine.NoAllocHelpers

	//Start Registrations for type : UnityEngine.Object

		//System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
		void Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();
		Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();

		//System.String UnityEngine.Object::ToString()
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

		//System.String UnityEngine.Object::get_name()
		void Register_UnityEngine_Object_get_name();
		Register_UnityEngine_Object_get_name();

		//System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_Destroy();
		Register_UnityEngine_Object_Destroy();

		//System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
		void Register_UnityEngine_Object_DestroyImmediate();
		Register_UnityEngine_Object_DestroyImmediate();

		//System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
		void Register_UnityEngine_Object_DontDestroyOnLoad();
		Register_UnityEngine_Object_DontDestroyOnLoad();

		//System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
		void Register_UnityEngine_Object_set_hideFlags();
		Register_UnityEngine_Object_set_hideFlags();

		//System.Void UnityEngine.Object::set_name(System.String)
		void Register_UnityEngine_Object_set_name();
		Register_UnityEngine_Object_set_name();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
		void Register_UnityEngine_Object_Internal_CloneSingle();
		Register_UnityEngine_Object_Internal_CloneSingle();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Object_Internal_CloneSingleWithParent();
		Register_UnityEngine_Object_Internal_CloneSingleWithParent();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.ParticleSystem

		//System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
		void Register_UnityEngine_ParticleSystem_Play();
		Register_UnityEngine_ParticleSystem_Play();

	//End Registrations for type : UnityEngine.ParticleSystem

	//Start Registrations for type : UnityEngine.Physics

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_CapsuleCast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_CapsuleCast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();

		//System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
		void Register_UnityEngine_Physics_IgnoreCollision();
		Register_UnityEngine_Physics_IgnoreCollision();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics

	//Start Registrations for type : UnityEngine.Physics2D

		//System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
		void Register_UnityEngine_Physics2D_get_queriesHitTriggers();
		Register_UnityEngine_Physics2D_get_queriesHitTriggers();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_CircleCast(UnityEngine.Vector2&,System.Single,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_CircleCast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_CircleCast();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();

		//System.Void UnityEngine.Physics2D::IgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D,System.Boolean)
		void Register_UnityEngine_Physics2D_IgnoreCollision();
		Register_UnityEngine_Physics2D_IgnoreCollision();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();

	//End Registrations for type : UnityEngine.Physics2D

	//Start Registrations for type : UnityEngine.Playables.PlayableHandle

		//System.Boolean UnityEngine.Playables.PlayableHandle::IsValid_Injected(UnityEngine.Playables.PlayableHandle&)
		void Register_UnityEngine_Playables_PlayableHandle_IsValid_Injected();
		Register_UnityEngine_Playables_PlayableHandle_IsValid_Injected();

		//System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType_Injected(UnityEngine.Playables.PlayableHandle&)
		void Register_UnityEngine_Playables_PlayableHandle_GetPlayableType_Injected();
		Register_UnityEngine_Playables_PlayableHandle_GetPlayableType_Injected();

	//End Registrations for type : UnityEngine.Playables.PlayableHandle

	//Start Registrations for type : UnityEngine.PlayerPrefs

		//System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
		void Register_UnityEngine_PlayerPrefs_HasKey();
		Register_UnityEngine_PlayerPrefs_HasKey();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_TrySetInt();
		Register_UnityEngine_PlayerPrefs_TrySetInt();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_TrySetSetString();
		Register_UnityEngine_PlayerPrefs_TrySetSetString();

		//System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_GetInt();
		Register_UnityEngine_PlayerPrefs_GetInt();

		//System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_GetString();
		Register_UnityEngine_PlayerPrefs_GetString();

		//System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
		void Register_UnityEngine_PlayerPrefs_DeleteKey();
		Register_UnityEngine_PlayerPrefs_DeleteKey();

	//End Registrations for type : UnityEngine.PlayerPrefs

	//Start Registrations for type : UnityEngine.PolygonCollider2D

		//System.Void UnityEngine.PolygonCollider2D::SetPath(System.Int32,UnityEngine.Vector2[])
		void Register_UnityEngine_PolygonCollider2D_SetPath();
		Register_UnityEngine_PolygonCollider2D_SetPath();

	//End Registrations for type : UnityEngine.PolygonCollider2D

	//Start Registrations for type : UnityEngine.PropertyNameUtils

		//System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
		void Register_UnityEngine_PropertyNameUtils_PropertyNameFromString_Injected();
		Register_UnityEngine_PropertyNameUtils_PropertyNameFromString_Injected();

	//End Registrations for type : UnityEngine.PropertyNameUtils

	//Start Registrations for type : UnityEngine.Quaternion

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();

	//End Registrations for type : UnityEngine.Quaternion

	//Start Registrations for type : UnityEngine.Random

		//System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
		void Register_UnityEngine_Random_RandomRangeInt();
		Register_UnityEngine_Random_RandomRangeInt();

		//System.Single UnityEngine.Random::Range(System.Single,System.Single)
		void Register_UnityEngine_Random_Range();
		Register_UnityEngine_Random_Range();

		//System.Single UnityEngine.Random::get_value()
		void Register_UnityEngine_Random_get_value();
		Register_UnityEngine_Random_get_value();

	//End Registrations for type : UnityEngine.Random

	//Start Registrations for type : UnityEngine.RectOffset

		//System.Int32 UnityEngine.RectOffset::get_bottom()
		void Register_UnityEngine_RectOffset_get_bottom();
		Register_UnityEngine_RectOffset_get_bottom();

		//System.Int32 UnityEngine.RectOffset::get_horizontal()
		void Register_UnityEngine_RectOffset_get_horizontal();
		Register_UnityEngine_RectOffset_get_horizontal();

		//System.Int32 UnityEngine.RectOffset::get_left()
		void Register_UnityEngine_RectOffset_get_left();
		Register_UnityEngine_RectOffset_get_left();

		//System.Int32 UnityEngine.RectOffset::get_right()
		void Register_UnityEngine_RectOffset_get_right();
		Register_UnityEngine_RectOffset_get_right();

		//System.Int32 UnityEngine.RectOffset::get_top()
		void Register_UnityEngine_RectOffset_get_top();
		Register_UnityEngine_RectOffset_get_top();

		//System.Int32 UnityEngine.RectOffset::get_vertical()
		void Register_UnityEngine_RectOffset_get_vertical();
		Register_UnityEngine_RectOffset_get_vertical();

		//System.Void UnityEngine.RectOffset::Cleanup()
		void Register_UnityEngine_RectOffset_Cleanup();
		Register_UnityEngine_RectOffset_Cleanup();

		//System.Void UnityEngine.RectOffset::Init()
		void Register_UnityEngine_RectOffset_Init();
		Register_UnityEngine_RectOffset_Init();

		//System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
		void Register_UnityEngine_RectOffset_set_bottom();
		Register_UnityEngine_RectOffset_set_bottom();

		//System.Void UnityEngine.RectOffset::set_left(System.Int32)
		void Register_UnityEngine_RectOffset_set_left();
		Register_UnityEngine_RectOffset_set_left();

		//System.Void UnityEngine.RectOffset::set_right(System.Int32)
		void Register_UnityEngine_RectOffset_set_right();
		Register_UnityEngine_RectOffset_set_right();

		//System.Void UnityEngine.RectOffset::set_top(System.Int32)
		void Register_UnityEngine_RectOffset_set_top();
		Register_UnityEngine_RectOffset_set_top();

	//End Registrations for type : UnityEngine.RectOffset

	//Start Registrations for type : UnityEngine.RectTransform

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_get_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_rect();
		Register_UnityEngine_RectTransform_INTERNAL_get_rect();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_set_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();

	//End Registrations for type : UnityEngine.RectTransform

	//Start Registrations for type : UnityEngine.RectTransformUtility

		//System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();

	//End Registrations for type : UnityEngine.RectTransformUtility

	//Start Registrations for type : UnityEngine.Renderer

		//System.Boolean UnityEngine.Renderer::get_enabled()
		void Register_UnityEngine_Renderer_get_enabled();
		Register_UnityEngine_Renderer_get_enabled();

		//System.Boolean UnityEngine.Renderer::get_receiveShadows()
		void Register_UnityEngine_Renderer_get_receiveShadows();
		Register_UnityEngine_Renderer_get_receiveShadows();

		//System.Int32 UnityEngine.Renderer::get_sortingLayerID()
		void Register_UnityEngine_Renderer_get_sortingLayerID();
		Register_UnityEngine_Renderer_get_sortingLayerID();

		//System.Int32 UnityEngine.Renderer::get_sortingOrder()
		void Register_UnityEngine_Renderer_get_sortingOrder();
		Register_UnityEngine_Renderer_get_sortingOrder();

		//System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Renderer_GetPropertyBlock();
		Register_UnityEngine_Renderer_GetPropertyBlock();

		//System.Void UnityEngine.Renderer::SetMaterial(UnityEngine.Material)
		void Register_UnityEngine_Renderer_SetMaterial();
		Register_UnityEngine_Renderer_SetMaterial();

		//System.Void UnityEngine.Renderer::SetMaterialArrayImpl(UnityEngine.Material[])
		void Register_UnityEngine_Renderer_SetMaterialArrayImpl();
		Register_UnityEngine_Renderer_SetMaterialArrayImpl();

		//System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
		void Register_UnityEngine_Renderer_SetPropertyBlock();
		Register_UnityEngine_Renderer_SetPropertyBlock();

		//System.Void UnityEngine.Renderer::get_bounds_Injected(UnityEngine.Bounds&)
		void Register_UnityEngine_Renderer_get_bounds_Injected();
		Register_UnityEngine_Renderer_get_bounds_Injected();

		//System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
		void Register_UnityEngine_Renderer_set_enabled();
		Register_UnityEngine_Renderer_set_enabled();

		//System.Void UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)
		void Register_UnityEngine_Renderer_set_lightProbeUsage();
		Register_UnityEngine_Renderer_set_lightProbeUsage();

		//System.Void UnityEngine.Renderer::set_motionVectorGenerationMode(UnityEngine.MotionVectorGenerationMode)
		void Register_UnityEngine_Renderer_set_motionVectorGenerationMode();
		Register_UnityEngine_Renderer_set_motionVectorGenerationMode();

		//System.Void UnityEngine.Renderer::set_probeAnchor(UnityEngine.Transform)
		void Register_UnityEngine_Renderer_set_probeAnchor();
		Register_UnityEngine_Renderer_set_probeAnchor();

		//System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
		void Register_UnityEngine_Renderer_set_receiveShadows();
		Register_UnityEngine_Renderer_set_receiveShadows();

		//System.Void UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)
		void Register_UnityEngine_Renderer_set_reflectionProbeUsage();
		Register_UnityEngine_Renderer_set_reflectionProbeUsage();

		//System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
		void Register_UnityEngine_Renderer_set_shadowCastingMode();
		Register_UnityEngine_Renderer_set_shadowCastingMode();

		//System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingLayerID();
		Register_UnityEngine_Renderer_set_sortingLayerID();

		//System.Void UnityEngine.Renderer::set_sortingLayerName(System.String)
		void Register_UnityEngine_Renderer_set_sortingLayerName();
		Register_UnityEngine_Renderer_set_sortingLayerName();

		//System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Renderer_set_sortingOrder();
		Register_UnityEngine_Renderer_set_sortingOrder();

		//UnityEngine.Material UnityEngine.Renderer::GetMaterial()
		void Register_UnityEngine_Renderer_GetMaterial();
		Register_UnityEngine_Renderer_GetMaterial();

		//UnityEngine.Material[] UnityEngine.Renderer::GetMaterialArray()
		void Register_UnityEngine_Renderer_GetMaterialArray();
		Register_UnityEngine_Renderer_GetMaterialArray();

		//UnityEngine.Material[] UnityEngine.Renderer::GetSharedMaterialArray()
		void Register_UnityEngine_Renderer_GetSharedMaterialArray();
		Register_UnityEngine_Renderer_GetSharedMaterialArray();

		//UnityEngine.MotionVectorGenerationMode UnityEngine.Renderer::get_motionVectorGenerationMode()
		void Register_UnityEngine_Renderer_get_motionVectorGenerationMode();
		Register_UnityEngine_Renderer_get_motionVectorGenerationMode();

		//UnityEngine.Rendering.LightProbeUsage UnityEngine.Renderer::get_lightProbeUsage()
		void Register_UnityEngine_Renderer_get_lightProbeUsage();
		Register_UnityEngine_Renderer_get_lightProbeUsage();

		//UnityEngine.Rendering.ReflectionProbeUsage UnityEngine.Renderer::get_reflectionProbeUsage()
		void Register_UnityEngine_Renderer_get_reflectionProbeUsage();
		Register_UnityEngine_Renderer_get_reflectionProbeUsage();

		//UnityEngine.Rendering.ShadowCastingMode UnityEngine.Renderer::get_shadowCastingMode()
		void Register_UnityEngine_Renderer_get_shadowCastingMode();
		Register_UnityEngine_Renderer_get_shadowCastingMode();

		//UnityEngine.Transform UnityEngine.Renderer::get_probeAnchor()
		void Register_UnityEngine_Renderer_get_probeAnchor();
		Register_UnityEngine_Renderer_get_probeAnchor();

	//End Registrations for type : UnityEngine.Renderer

	//Start Registrations for type : UnityEngine.RenderTexture

		//System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetHeight();
		Register_UnityEngine_RenderTexture_Internal_GetHeight();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetWidth();
		Register_UnityEngine_RenderTexture_Internal_GetWidth();

	//End Registrations for type : UnityEngine.RenderTexture

	//Start Registrations for type : UnityEngine.Resources

		//UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
		void Register_UnityEngine_Resources_GetBuiltinResource();
		Register_UnityEngine_Resources_GetBuiltinResource();

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

		//UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsyncInternal(System.String,System.Type)
		void Register_UnityEngine_Resources_LoadAsyncInternal();
		Register_UnityEngine_Resources_LoadAsyncInternal();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.Rigidbody

		//System.Single UnityEngine.Rigidbody::get_mass()
		void Register_UnityEngine_Rigidbody_get_mass();
		Register_UnityEngine_Rigidbody_get_mass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_position();
		Register_UnityEngine_Rigidbody_INTERNAL_get_position();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_rotation();
		Register_UnityEngine_Rigidbody_INTERNAL_get_rotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_rotation();
		Register_UnityEngine_Rigidbody_INTERNAL_set_rotation();

		//System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
		void Register_UnityEngine_Rigidbody_set_constraints();
		Register_UnityEngine_Rigidbody_set_constraints();

		//System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_isKinematic();
		Register_UnityEngine_Rigidbody_set_isKinematic();

		//System.Void UnityEngine.Rigidbody::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody_set_mass();
		Register_UnityEngine_Rigidbody_set_mass();

	//End Registrations for type : UnityEngine.Rigidbody

	//Start Registrations for type : UnityEngine.Rigidbody2D

		//System.Single UnityEngine.Rigidbody2D::get_mass()
		void Register_UnityEngine_Rigidbody2D_get_mass();
		Register_UnityEngine_Rigidbody2D_get_mass();

		//System.Single UnityEngine.Rigidbody2D::get_rotation()
		void Register_UnityEngine_Rigidbody2D_get_rotation();
		Register_UnityEngine_Rigidbody2D_get_rotation();

		//System.Void UnityEngine.Rigidbody2D::MovePosition_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_MovePosition_Injected();
		Register_UnityEngine_Rigidbody2D_MovePosition_Injected();

		//System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
		void Register_UnityEngine_Rigidbody2D_MoveRotation();
		Register_UnityEngine_Rigidbody2D_MoveRotation();

		//System.Void UnityEngine.Rigidbody2D::get_position_Injected(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_get_position_Injected();
		Register_UnityEngine_Rigidbody2D_get_position_Injected();

		//System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
		void Register_UnityEngine_Rigidbody2D_set_bodyType();
		Register_UnityEngine_Rigidbody2D_set_bodyType();

		//System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_gravityScale();
		Register_UnityEngine_Rigidbody2D_set_gravityScale();

		//System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_mass();
		Register_UnityEngine_Rigidbody2D_set_mass();

	//End Registrations for type : UnityEngine.Rigidbody2D

	//Start Registrations for type : UnityEngine.SceneManagement.Scene

		//System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal();
		Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal();

		//System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetNameInternal();
		Register_UnityEngine_SceneManagement_Scene_GetNameInternal();

	//End Registrations for type : UnityEngine.SceneManagement.Scene

	//Start Registrations for type : UnityEngine.SceneManagement.SceneManager

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
		void Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();

	//End Registrations for type : UnityEngine.SceneManagement.SceneManager

	//Start Registrations for type : UnityEngine.Screen

		//System.Int32 UnityEngine.Screen::get_height()
		void Register_UnityEngine_Screen_get_height();
		Register_UnityEngine_Screen_get_height();

		//System.Int32 UnityEngine.Screen::get_width()
		void Register_UnityEngine_Screen_get_width();
		Register_UnityEngine_Screen_get_width();

		//System.Single UnityEngine.Screen::get_dpi()
		void Register_UnityEngine_Screen_get_dpi();
		Register_UnityEngine_Screen_get_dpi();

	//End Registrations for type : UnityEngine.Screen

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.Shader

		//System.Int32 UnityEngine.Shader::PropertyToID(System.String)
		void Register_UnityEngine_Shader_PropertyToID();
		Register_UnityEngine_Shader_PropertyToID();

		//UnityEngine.Shader UnityEngine.Shader::Find(System.String)
		void Register_UnityEngine_Shader_Find();
		Register_UnityEngine_Shader_Find();

	//End Registrations for type : UnityEngine.Shader

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[],System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.String[],System.Int32,System.Int32,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.SortingLayer

		//System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
		void Register_UnityEngine_SortingLayer_GetLayerValueFromID();
		Register_UnityEngine_SortingLayer_GetLayerValueFromID();

	//End Registrations for type : UnityEngine.SortingLayer

	//Start Registrations for type : UnityEngine.SphereCollider

		//System.Void UnityEngine.SphereCollider::set_radius(System.Single)
		void Register_UnityEngine_SphereCollider_set_radius();
		Register_UnityEngine_SphereCollider_set_radius();

	//End Registrations for type : UnityEngine.SphereCollider

	//Start Registrations for type : UnityEngine.Sprite

		//System.Boolean UnityEngine.Sprite::get_packed()
		void Register_UnityEngine_Sprite_get_packed();
		Register_UnityEngine_Sprite_get_packed();

		//System.Single UnityEngine.Sprite::get_pixelsPerUnit()
		void Register_UnityEngine_Sprite_get_pixelsPerUnit();
		Register_UnityEngine_Sprite_get_pixelsPerUnit();

		//System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_get_border();
		Register_UnityEngine_Sprite_INTERNAL_get_border();

		//System.Void UnityEngine.Sprite::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Sprite_INTERNAL_get_bounds();
		Register_UnityEngine_Sprite_INTERNAL_get_bounds();

		//System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_rect();
		Register_UnityEngine_Sprite_INTERNAL_get_rect();

		//System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_textureRect();
		Register_UnityEngine_Sprite_INTERNAL_get_textureRect();

		//UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_CALL_Create();
		Register_UnityEngine_Sprite_INTERNAL_CALL_Create();

		//UnityEngine.SpritePackingRotation UnityEngine.Sprite::get_packingRotation()
		void Register_UnityEngine_Sprite_get_packingRotation();
		Register_UnityEngine_Sprite_get_packingRotation();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
		void Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();
		Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
		void Register_UnityEngine_Sprite_get_texture();
		Register_UnityEngine_Sprite_get_texture();

	//End Registrations for type : UnityEngine.Sprite

	//Start Registrations for type : UnityEngine.SpriteRenderer

		//System.Void UnityEngine.SpriteRenderer::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_SpriteRenderer_INTERNAL_get_color();
		Register_UnityEngine_SpriteRenderer_INTERNAL_get_color();

		//System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_SpriteRenderer_INTERNAL_set_color();
		Register_UnityEngine_SpriteRenderer_INTERNAL_set_color();

		//System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
		void Register_UnityEngine_SpriteRenderer_SetSprite_INTERNAL();
		Register_UnityEngine_SpriteRenderer_SetSprite_INTERNAL();

		//UnityEngine.Sprite UnityEngine.SpriteRenderer::GetSprite_INTERNAL()
		void Register_UnityEngine_SpriteRenderer_GetSprite_INTERNAL();
		Register_UnityEngine_SpriteRenderer_GetSprite_INTERNAL();

	//End Registrations for type : UnityEngine.SpriteRenderer

	//Start Registrations for type : UnityEngine.Sprites.DataUtility

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();

		//System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();
		Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();

	//End Registrations for type : UnityEngine.Sprites.DataUtility

	//Start Registrations for type : UnityEngine.SystemInfo

		//System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
		void Register_UnityEngine_SystemInfo_get_supportsGyroscope();
		Register_UnityEngine_SystemInfo_get_supportsGyroscope();

		//UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
		void Register_UnityEngine_SystemInfo_get_operatingSystemFamily();
		Register_UnityEngine_SystemInfo_get_operatingSystemFamily();

	//End Registrations for type : UnityEngine.SystemInfo

	//Start Registrations for type : UnityEngine.TextAsset

		//System.Byte[] UnityEngine.TextAsset::get_bytes()
		void Register_UnityEngine_TextAsset_get_bytes();
		Register_UnityEngine_TextAsset_get_bytes();

		//System.String UnityEngine.TextAsset::get_text()
		void Register_UnityEngine_TextAsset_get_text();
		Register_UnityEngine_TextAsset_get_text();

	//End Registrations for type : UnityEngine.TextAsset

	//Start Registrations for type : UnityEngine.TextGenerator

		//System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
		void Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();
		Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();

		//System.Int32 UnityEngine.TextGenerator::get_characterCount()
		void Register_UnityEngine_TextGenerator_get_characterCount();
		Register_UnityEngine_TextGenerator_get_characterCount();

		//System.Int32 UnityEngine.TextGenerator::get_lineCount()
		void Register_UnityEngine_TextGenerator_get_lineCount();
		Register_UnityEngine_TextGenerator_get_lineCount();

		//System.Void UnityEngine.TextGenerator::Dispose_cpp()
		void Register_UnityEngine_TextGenerator_Dispose_cpp();
		Register_UnityEngine_TextGenerator_Dispose_cpp();

		//System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetCharactersInternal();
		Register_UnityEngine_TextGenerator_GetCharactersInternal();

		//System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetLinesInternal();
		Register_UnityEngine_TextGenerator_GetLinesInternal();

		//System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetVerticesInternal();
		Register_UnityEngine_TextGenerator_GetVerticesInternal();

		//System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
		void Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();
		Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();

		//System.Void UnityEngine.TextGenerator::Init()
		void Register_UnityEngine_TextGenerator_Init();
		Register_UnityEngine_TextGenerator_Init();

	//End Registrations for type : UnityEngine.TextGenerator

	//Start Registrations for type : UnityEngine.Texture

		//System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetHeight();
		Register_UnityEngine_Texture_Internal_GetHeight();

		//System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetWidth();
		Register_UnityEngine_Texture_Internal_GetWidth();

		//System.Int32 UnityEngine.Texture::get_anisoLevel()
		void Register_UnityEngine_Texture_get_anisoLevel();
		Register_UnityEngine_Texture_get_anisoLevel();

		//System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
		void Register_UnityEngine_Texture_INTERNAL_get_texelSize();
		Register_UnityEngine_Texture_INTERNAL_get_texelSize();

		//System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
		void Register_UnityEngine_Texture_set_anisoLevel();
		Register_UnityEngine_Texture_set_anisoLevel();

		//System.Void UnityEngine.Texture::set_mipMapBias(System.Single)
		void Register_UnityEngine_Texture_set_mipMapBias();
		Register_UnityEngine_Texture_set_mipMapBias();

		//UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
		void Register_UnityEngine_Texture_get_wrapMode();
		Register_UnityEngine_Texture_get_wrapMode();

	//End Registrations for type : UnityEngine.Texture

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture2D_Apply();
		Register_UnityEngine_Texture2D_Apply();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();

		//System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_Create();
		Register_UnityEngine_Texture2D_Internal_Create();

		//System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture2D_SetPixels();
		Register_UnityEngine_Texture2D_SetPixels();

		//UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Texture2D_GetPixels();
		Register_UnityEngine_Texture2D_GetPixels();

		//UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Texture2D_PackTextures();
		Register_UnityEngine_Texture2D_PackTextures();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
		void Register_UnityEngine_Texture2D_get_whiteTexture();
		Register_UnityEngine_Texture2D_get_whiteTexture();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Time

		//System.Int32 UnityEngine.Time::get_frameCount()
		void Register_UnityEngine_Time_get_frameCount();
		Register_UnityEngine_Time_get_frameCount();

		//System.Single UnityEngine.Time::get_deltaTime()
		void Register_UnityEngine_Time_get_deltaTime();
		Register_UnityEngine_Time_get_deltaTime();

		//System.Single UnityEngine.Time::get_realtimeSinceStartup()
		void Register_UnityEngine_Time_get_realtimeSinceStartup();
		Register_UnityEngine_Time_get_realtimeSinceStartup();

		//System.Single UnityEngine.Time::get_smoothDeltaTime()
		void Register_UnityEngine_Time_get_smoothDeltaTime();
		Register_UnityEngine_Time_get_smoothDeltaTime();

		//System.Single UnityEngine.Time::get_time()
		void Register_UnityEngine_Time_get_time();
		Register_UnityEngine_Time_get_time();

		//System.Single UnityEngine.Time::get_timeScale()
		void Register_UnityEngine_Time_get_timeScale();
		Register_UnityEngine_Time_get_timeScale();

		//System.Single UnityEngine.Time::get_unscaledDeltaTime()
		void Register_UnityEngine_Time_get_unscaledDeltaTime();
		Register_UnityEngine_Time_get_unscaledDeltaTime();

		//System.Single UnityEngine.Time::get_unscaledTime()
		void Register_UnityEngine_Time_get_unscaledTime();
		Register_UnityEngine_Time_get_unscaledTime();

		//System.Void UnityEngine.Time::set_fixedDeltaTime(System.Single)
		void Register_UnityEngine_Time_set_fixedDeltaTime();
		Register_UnityEngine_Time_set_fixedDeltaTime();

		//System.Void UnityEngine.Time::set_timeScale(System.Single)
		void Register_UnityEngine_Time_set_timeScale();
		Register_UnityEngine_Time_set_timeScale();

	//End Registrations for type : UnityEngine.Time

	//Start Registrations for type : UnityEngine.TouchScreenKeyboard

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
		void Register_UnityEngine_TouchScreenKeyboard_get_active();
		Register_UnityEngine_TouchScreenKeyboard_get_active();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
		void Register_UnityEngine_TouchScreenKeyboard_get_canGetSelection();
		Register_UnityEngine_TouchScreenKeyboard_get_canGetSelection();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
		void Register_UnityEngine_TouchScreenKeyboard_get_done();
		Register_UnityEngine_TouchScreenKeyboard_get_done();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
		void Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();
		Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();

		//System.String UnityEngine.TouchScreenKeyboard::get_text()
		void Register_UnityEngine_TouchScreenKeyboard_get_text();
		Register_UnityEngine_TouchScreenKeyboard_get_text();

		//System.Void UnityEngine.TouchScreenKeyboard::Destroy()
		void Register_UnityEngine_TouchScreenKeyboard_Destroy();
		Register_UnityEngine_TouchScreenKeyboard_Destroy();

		//System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
		void Register_UnityEngine_TouchScreenKeyboard_GetSelectionInternal();
		Register_UnityEngine_TouchScreenKeyboard_GetSelectionInternal();

		//System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
		void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();
		Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();

		//System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_active();
		Register_UnityEngine_TouchScreenKeyboard_set_active();

		//System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_hideInput();
		Register_UnityEngine_TouchScreenKeyboard_set_hideInput();

		//System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
		void Register_UnityEngine_TouchScreenKeyboard_set_text();
		Register_UnityEngine_TouchScreenKeyboard_set_text();

	//End Registrations for type : UnityEngine.TouchScreenKeyboard

	//Start Registrations for type : UnityEngine.TrailRenderer

		//System.Single UnityEngine.TrailRenderer::get_endWidth()
		void Register_UnityEngine_TrailRenderer_get_endWidth();
		Register_UnityEngine_TrailRenderer_get_endWidth();

		//System.Single UnityEngine.TrailRenderer::get_startWidth()
		void Register_UnityEngine_TrailRenderer_get_startWidth();
		Register_UnityEngine_TrailRenderer_get_startWidth();

		//System.Single UnityEngine.TrailRenderer::get_time()
		void Register_UnityEngine_TrailRenderer_get_time();
		Register_UnityEngine_TrailRenderer_get_time();

		//System.Void UnityEngine.TrailRenderer::set_endWidth(System.Single)
		void Register_UnityEngine_TrailRenderer_set_endWidth();
		Register_UnityEngine_TrailRenderer_set_endWidth();

		//System.Void UnityEngine.TrailRenderer::set_startWidth(System.Single)
		void Register_UnityEngine_TrailRenderer_set_startWidth();
		Register_UnityEngine_TrailRenderer_set_startWidth();

		//System.Void UnityEngine.TrailRenderer::set_time(System.Single)
		void Register_UnityEngine_TrailRenderer_set_time();
		Register_UnityEngine_TrailRenderer_set_time();

	//End Registrations for type : UnityEngine.TrailRenderer

	//Start Registrations for type : UnityEngine.Transform

		//System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
		void Register_UnityEngine_Transform_IsChildOf();
		Register_UnityEngine_Transform_IsChildOf();

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();
		Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundInternal();
		Register_UnityEngine_Transform_INTERNAL_CALL_RotateAroundInternal();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_SetPositionAndRotation(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_SetPositionAndRotation();
		Register_UnityEngine_Transform_INTERNAL_CALL_SetPositionAndRotation();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localPosition();
		Register_UnityEngine_Transform_INTERNAL_get_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_localRotation();
		Register_UnityEngine_Transform_INTERNAL_get_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localScale();
		Register_UnityEngine_Transform_INTERNAL_get_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_localToWorldMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_localToWorldMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_position();
		Register_UnityEngine_Transform_INTERNAL_get_position();

		//System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_rotation();
		Register_UnityEngine_Transform_INTERNAL_get_rotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localPosition();
		Register_UnityEngine_Transform_INTERNAL_set_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_localRotation();
		Register_UnityEngine_Transform_INTERNAL_set_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localScale();
		Register_UnityEngine_Transform_INTERNAL_set_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_position();
		Register_UnityEngine_Transform_INTERNAL_set_position();

		//System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_rotation();
		Register_UnityEngine_Transform_INTERNAL_set_rotation();

		//System.Void UnityEngine.Transform::SetAsFirstSibling()
		void Register_UnityEngine_Transform_SetAsFirstSibling();
		Register_UnityEngine_Transform_SetAsFirstSibling();

		//System.Void UnityEngine.Transform::SetAsLastSibling()
		void Register_UnityEngine_Transform_SetAsLastSibling();
		Register_UnityEngine_Transform_SetAsLastSibling();

		//System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Transform_SetParent();
		Register_UnityEngine_Transform_SetParent();

		//System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
		void Register_UnityEngine_Transform_set_parentInternal();
		Register_UnityEngine_Transform_set_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

		//UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
		void Register_UnityEngine_Transform_get_parentInternal();
		Register_UnityEngine_Transform_get_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::get_root()
		void Register_UnityEngine_Transform_get_root();
		Register_UnityEngine_Transform_get_root();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.U2D.SpriteAtlasManager

		//System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
		void Register_UnityEngine_U2D_SpriteAtlasManager_Register();
		Register_UnityEngine_U2D_SpriteAtlasManager_Register();

	//End Registrations for type : UnityEngine.U2D.SpriteAtlasManager

	//Start Registrations for type : UnityEngine.UISystemProfilerApi

		//System.Void UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)
		void Register_UnityEngine_UISystemProfilerApi_AddMarker();
		Register_UnityEngine_UISystemProfilerApi_AddMarker();

		//System.Void UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)
		void Register_UnityEngine_UISystemProfilerApi_BeginSample();
		Register_UnityEngine_UISystemProfilerApi_BeginSample();

		//System.Void UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)
		void Register_UnityEngine_UISystemProfilerApi_EndSample();
		Register_UnityEngine_UISystemProfilerApi_EndSample();

	//End Registrations for type : UnityEngine.UISystemProfilerApi

	//Start Registrations for type : UnityEngine.UnhandledExceptionHandler

		//System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
		void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();
		Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();

	//End Registrations for type : UnityEngine.UnhandledExceptionHandler

	//Start Registrations for type : UnityEngine.UnityLogWriter

		//System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
		void Register_UnityEngine_UnityLogWriter_WriteStringToUnityLog();
		Register_UnityEngine_UnityLogWriter_WriteStringToUnityLog();

	//End Registrations for type : UnityEngine.UnityLogWriter

	//Start Registrations for type : UnityEngine.WebCamTexture

		//System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
		void Register_UnityEngine_WebCamTexture_get_videoVerticallyMirrored();
		Register_UnityEngine_WebCamTexture_get_videoVerticallyMirrored();

		//System.Int32 UnityEngine.WebCamTexture::get_videoRotationAngle()
		void Register_UnityEngine_WebCamTexture_get_videoRotationAngle();
		Register_UnityEngine_WebCamTexture_get_videoRotationAngle();

		//System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
		void Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Play();
		Register_UnityEngine_WebCamTexture_INTERNAL_CALL_Play();

		//System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_WebCamTexture_Internal_CreateWebCamTexture();
		Register_UnityEngine_WebCamTexture_Internal_CreateWebCamTexture();

		//UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
		void Register_UnityEngine_WebCamTexture_get_devices();
		Register_UnityEngine_WebCamTexture_get_devices();

	//End Registrations for type : UnityEngine.WebCamTexture

}
