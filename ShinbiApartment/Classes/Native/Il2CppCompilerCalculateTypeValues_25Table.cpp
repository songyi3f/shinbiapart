﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t1229485932;
// System.String
struct String_t;
// FullSerializer.fsSerializer
struct fsSerializer_t4093814827;
// System.Func`2<FullSerializer.fsDataType,System.String>
struct Func_2_t2106788937;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>>
struct Dictionary_2_t843717378;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2572182361;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter>
struct Dictionary_2_t4012097003;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>>
struct Dictionary_2_t40823354;
// System.Collections.Generic.List`1<FullSerializer.fsConverter>
struct List_1_t3412709762;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsDirectConverter>
struct Dictionary_2_t1339064699;
// System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>
struct List_1_t1891443586;
// FullSerializer.Internal.fsCyclicReferenceManager
struct fsCyclicReferenceManager_t2963233308;
// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter
struct fsLazyCycleDefinitionWriter_t239783049;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t633324528;
// FullSerializer.fsContext
struct fsContext_t2261407774;
// FullSerializer.fsConfig
struct fsConfig_t14416447;
// System.Collections.Generic.Dictionary`2<System.Int32,FullSerializer.fsData>
struct Dictionary_2_t3590453914;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t1515895227;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// GameVanilla.Core.Popup
struct Popup_t1063720813;
// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>>
struct Func_2_t779105388;
// System.Func`3<System.Reflection.Assembly,System.Type,<>__AnonType0`2<System.Reflection.Assembly,System.Type>>
struct Func_3_t3987483980;
// System.Func`2<<>__AnonType0`2<System.Reflection.Assembly,System.Type>,System.Type>
struct Func_2_t3063509026;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.Assembly>
struct Dictionary_2_t3887689098;
// System.Collections.Generic.List`1<System.Reflection.Assembly>
struct List_1_t1279540245;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t107971893;
// FullSerializer.Internal.DirectConverters.AnimationCurve_DirectConverter
struct AnimationCurve_DirectConverter_t2921849045;
// FullSerializer.Internal.DirectConverters.Bounds_DirectConverter
struct Bounds_DirectConverter_t3871173093;
// FullSerializer.Internal.DirectConverters.Gradient_DirectConverter
struct Gradient_DirectConverter_t2745360243;
// FullSerializer.Internal.DirectConverters.GUIStyle_DirectConverter
struct GUIStyle_DirectConverter_t1060365653;
// FullSerializer.Internal.DirectConverters.GUIStyleState_DirectConverter
struct GUIStyleState_DirectConverter_t3967104754;
// FullSerializer.Internal.DirectConverters.Keyframe_DirectConverter
struct Keyframe_DirectConverter_t1448983566;
// FullSerializer.Internal.DirectConverters.LayerMask_DirectConverter
struct LayerMask_DirectConverter_t4211565801;
// FullSerializer.Internal.DirectConverters.Rect_DirectConverter
struct Rect_DirectConverter_t1861700375;
// FullSerializer.Internal.DirectConverters.RectOffset_DirectConverter
struct RectOffset_DirectConverter_t483038648;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// GameVanilla.Core.AnimatedButton
struct AnimatedButton_t997585140;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// FullSerializer.Internal.fsVersionedType[]
struct fsVersionedTypeU5BU5D_t710655465;
// System.Char[]
struct CharU5BU5D_t3528271667;
// FullSerializer.fsAotVersionInfo/Member[]
struct MemberU5BU5D_t44466300;
// UnityEngine.UI.Image
struct Image_t2670269651;
// System.Action
struct Action_t1264377477;
// System.Collections.Generic.Dictionary`2<FullSerializer.fsConfig,System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsMetaType>>
struct Dictionary_2_t2294030055;
// FullSerializer.Internal.fsMetaProperty[]
struct fsMetaPropertyU5BU5D_t1681951782;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.IDictionary`2<FullSerializer.Internal.fsPortableReflection/AttributeQuery,System.Attribute>
struct IDictionary_2_t111537787;
// System.Func`3<System.String,System.Reflection.MemberInfo,System.String>
struct Func_3_t2404287706;
// System.Collections.Generic.Stack`1<UnityEngine.GameObject>
struct Stack_1_t1957026074;
// GameVanilla.Core.BaseScene
struct BaseScene_t2918414559;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t857997111;
// GameVanilla.Core.ObjectPool
struct ObjectPool_t858432606;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip>
struct Dictionary_2_t3466145964;
// GameVanilla.Core.BackgroundMusic
struct BackgroundMusic_t4234539114;
// GameVanilla.Core.PooledObject
struct PooledObject_t1286137293;
// GameVanilla.Core.AnimatedButton/ButtonClickedEvent
struct ButtonClickedEvent_t849812677;

struct fsVersionedType_t1764735544_marshaled_pinvoke;
struct fsVersionedType_t1764735544_marshaled_com;
struct Member_t1335065313_marshaled_pinvoke;
struct Member_t1335065313_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCSHARPNAMEU3EC__ANONSTOREY0_T3622865521_H
#define U3CCSHARPNAMEU3EC__ANONSTOREY0_T3622865521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsTypeExtensions/<CSharpName>c__AnonStorey0
struct  U3CCSharpNameU3Ec__AnonStorey0_t3622865521  : public RuntimeObject
{
public:
	// System.Boolean FullSerializer.fsTypeExtensions/<CSharpName>c__AnonStorey0::includeNamespace
	bool ___includeNamespace_0;

public:
	inline static int32_t get_offset_of_includeNamespace_0() { return static_cast<int32_t>(offsetof(U3CCSharpNameU3Ec__AnonStorey0_t3622865521, ___includeNamespace_0)); }
	inline bool get_includeNamespace_0() const { return ___includeNamespace_0; }
	inline bool* get_address_of_includeNamespace_0() { return &___includeNamespace_0; }
	inline void set_includeNamespace_0(bool value)
	{
		___includeNamespace_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCSHARPNAMEU3EC__ANONSTOREY0_T3622865521_H
#ifndef FSCONTEXT_T2261407774_H
#define FSCONTEXT_T2261407774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsContext
struct  fsContext_t2261407774  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> FullSerializer.fsContext::_contextObjects
	Dictionary_2_t1229485932 * ____contextObjects_0;

public:
	inline static int32_t get_offset_of__contextObjects_0() { return static_cast<int32_t>(offsetof(fsContext_t2261407774, ____contextObjects_0)); }
	inline Dictionary_2_t1229485932 * get__contextObjects_0() const { return ____contextObjects_0; }
	inline Dictionary_2_t1229485932 ** get_address_of__contextObjects_0() { return &____contextObjects_0; }
	inline void set__contextObjects_0(Dictionary_2_t1229485932 * value)
	{
		____contextObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&____contextObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONTEXT_T2261407774_H
#ifndef FSTYPEEXTENSIONS_T1429624868_H
#define FSTYPEEXTENSIONS_T1429624868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsTypeExtensions
struct  fsTypeExtensions_t1429624868  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSTYPEEXTENSIONS_T1429624868_H
#ifndef FSGLOBALCONFIG_T438576193_H
#define FSGLOBALCONFIG_T438576193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsGlobalConfig
struct  fsGlobalConfig_t438576193  : public RuntimeObject
{
public:

public:
};

struct fsGlobalConfig_t438576193_StaticFields
{
public:
	// System.Boolean FullSerializer.fsGlobalConfig::IsCaseSensitive
	bool ___IsCaseSensitive_0;
	// System.Boolean FullSerializer.fsGlobalConfig::AllowInternalExceptions
	bool ___AllowInternalExceptions_1;
	// System.String FullSerializer.fsGlobalConfig::InternalFieldPrefix
	String_t* ___InternalFieldPrefix_2;

public:
	inline static int32_t get_offset_of_IsCaseSensitive_0() { return static_cast<int32_t>(offsetof(fsGlobalConfig_t438576193_StaticFields, ___IsCaseSensitive_0)); }
	inline bool get_IsCaseSensitive_0() const { return ___IsCaseSensitive_0; }
	inline bool* get_address_of_IsCaseSensitive_0() { return &___IsCaseSensitive_0; }
	inline void set_IsCaseSensitive_0(bool value)
	{
		___IsCaseSensitive_0 = value;
	}

	inline static int32_t get_offset_of_AllowInternalExceptions_1() { return static_cast<int32_t>(offsetof(fsGlobalConfig_t438576193_StaticFields, ___AllowInternalExceptions_1)); }
	inline bool get_AllowInternalExceptions_1() const { return ___AllowInternalExceptions_1; }
	inline bool* get_address_of_AllowInternalExceptions_1() { return &___AllowInternalExceptions_1; }
	inline void set_AllowInternalExceptions_1(bool value)
	{
		___AllowInternalExceptions_1 = value;
	}

	inline static int32_t get_offset_of_InternalFieldPrefix_2() { return static_cast<int32_t>(offsetof(fsGlobalConfig_t438576193_StaticFields, ___InternalFieldPrefix_2)); }
	inline String_t* get_InternalFieldPrefix_2() const { return ___InternalFieldPrefix_2; }
	inline String_t** get_address_of_InternalFieldPrefix_2() { return &___InternalFieldPrefix_2; }
	inline void set_InternalFieldPrefix_2(String_t* value)
	{
		___InternalFieldPrefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___InternalFieldPrefix_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSGLOBALCONFIG_T438576193_H
#ifndef FSBASECONVERTER_T1567749939_H
#define FSBASECONVERTER_T1567749939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsBaseConverter
struct  fsBaseConverter_t1567749939  : public RuntimeObject
{
public:
	// FullSerializer.fsSerializer FullSerializer.fsBaseConverter::Serializer
	fsSerializer_t4093814827 * ___Serializer_0;

public:
	inline static int32_t get_offset_of_Serializer_0() { return static_cast<int32_t>(offsetof(fsBaseConverter_t1567749939, ___Serializer_0)); }
	inline fsSerializer_t4093814827 * get_Serializer_0() const { return ___Serializer_0; }
	inline fsSerializer_t4093814827 ** get_address_of_Serializer_0() { return &___Serializer_0; }
	inline void set_Serializer_0(fsSerializer_t4093814827 * value)
	{
		___Serializer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_0), value);
	}
};

struct fsBaseConverter_t1567749939_StaticFields
{
public:
	// System.Func`2<FullSerializer.fsDataType,System.String> FullSerializer.fsBaseConverter::<>f__am$cache0
	Func_2_t2106788937 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(fsBaseConverter_t1567749939_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t2106788937 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t2106788937 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t2106788937 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSBASECONVERTER_T1567749939_H
#ifndef FSVERSIONMANAGER_T3974572827_H
#define FSVERSIONMANAGER_T3974572827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsVersionManager
struct  fsVersionManager_t3974572827  : public RuntimeObject
{
public:

public:
};

struct fsVersionManager_t3974572827_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>> FullSerializer.Internal.fsVersionManager::_cache
	Dictionary_2_t843717378 * ____cache_0;

public:
	inline static int32_t get_offset_of__cache_0() { return static_cast<int32_t>(offsetof(fsVersionManager_t3974572827_StaticFields, ____cache_0)); }
	inline Dictionary_2_t843717378 * get__cache_0() const { return ____cache_0; }
	inline Dictionary_2_t843717378 ** get_address_of__cache_0() { return &____cache_0; }
	inline void set__cache_0(Dictionary_2_t843717378 * value)
	{
		____cache_0 = value;
		Il2CppCodeGenWriteBarrier((&____cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSVERSIONMANAGER_T3974572827_H
#ifndef FSMETAPROPERTY_T1347894527_H
#define FSMETAPROPERTY_T1347894527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsMetaProperty
struct  fsMetaProperty_t1347894527  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo FullSerializer.Internal.fsMetaProperty::_memberInfo
	MemberInfo_t * ____memberInfo_0;
	// System.Type FullSerializer.Internal.fsMetaProperty::<StorageType>k__BackingField
	Type_t * ___U3CStorageTypeU3Ek__BackingField_1;
	// System.Type FullSerializer.Internal.fsMetaProperty::<OverrideConverterType>k__BackingField
	Type_t * ___U3COverrideConverterTypeU3Ek__BackingField_2;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<CanRead>k__BackingField
	bool ___U3CCanReadU3Ek__BackingField_3;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<CanWrite>k__BackingField
	bool ___U3CCanWriteU3Ek__BackingField_4;
	// System.String FullSerializer.Internal.fsMetaProperty::<JsonName>k__BackingField
	String_t* ___U3CJsonNameU3Ek__BackingField_5;
	// System.String FullSerializer.Internal.fsMetaProperty::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_6;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<IsPublic>k__BackingField
	bool ___U3CIsPublicU3Ek__BackingField_7;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}

	inline static int32_t get_offset_of_U3CStorageTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CStorageTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CStorageTypeU3Ek__BackingField_1() const { return ___U3CStorageTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CStorageTypeU3Ek__BackingField_1() { return &___U3CStorageTypeU3Ek__BackingField_1; }
	inline void set_U3CStorageTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CStorageTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStorageTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COverrideConverterTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3COverrideConverterTypeU3Ek__BackingField_2)); }
	inline Type_t * get_U3COverrideConverterTypeU3Ek__BackingField_2() const { return ___U3COverrideConverterTypeU3Ek__BackingField_2; }
	inline Type_t ** get_address_of_U3COverrideConverterTypeU3Ek__BackingField_2() { return &___U3COverrideConverterTypeU3Ek__BackingField_2; }
	inline void set_U3COverrideConverterTypeU3Ek__BackingField_2(Type_t * value)
	{
		___U3COverrideConverterTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COverrideConverterTypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCanReadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CCanReadU3Ek__BackingField_3)); }
	inline bool get_U3CCanReadU3Ek__BackingField_3() const { return ___U3CCanReadU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CCanReadU3Ek__BackingField_3() { return &___U3CCanReadU3Ek__BackingField_3; }
	inline void set_U3CCanReadU3Ek__BackingField_3(bool value)
	{
		___U3CCanReadU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCanWriteU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CCanWriteU3Ek__BackingField_4)); }
	inline bool get_U3CCanWriteU3Ek__BackingField_4() const { return ___U3CCanWriteU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CCanWriteU3Ek__BackingField_4() { return &___U3CCanWriteU3Ek__BackingField_4; }
	inline void set_U3CCanWriteU3Ek__BackingField_4(bool value)
	{
		___U3CCanWriteU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CJsonNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CJsonNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CJsonNameU3Ek__BackingField_5() const { return ___U3CJsonNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CJsonNameU3Ek__BackingField_5() { return &___U3CJsonNameU3Ek__BackingField_5; }
	inline void set_U3CJsonNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CJsonNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJsonNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CMemberNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_6() const { return ___U3CMemberNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_6() { return &___U3CMemberNameU3Ek__BackingField_6; }
	inline void set_U3CMemberNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsPublicU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CIsPublicU3Ek__BackingField_7)); }
	inline bool get_U3CIsPublicU3Ek__BackingField_7() const { return ___U3CIsPublicU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsPublicU3Ek__BackingField_7() { return &___U3CIsPublicU3Ek__BackingField_7; }
	inline void set_U3CIsPublicU3Ek__BackingField_7(bool value)
	{
		___U3CIsPublicU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsReadOnlyU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(fsMetaProperty_t1347894527, ___U3CIsReadOnlyU3Ek__BackingField_8)); }
	inline bool get_U3CIsReadOnlyU3Ek__BackingField_8() const { return ___U3CIsReadOnlyU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsReadOnlyU3Ek__BackingField_8() { return &___U3CIsReadOnlyU3Ek__BackingField_8; }
	inline void set_U3CIsReadOnlyU3Ek__BackingField_8(bool value)
	{
		___U3CIsReadOnlyU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMETAPROPERTY_T1347894527_H
#ifndef U3CCOLLECTPROPERTIESU3EC__ANONSTOREY0_T1189373392_H
#define U3CCOLLECTPROPERTIESU3EC__ANONSTOREY0_T1189373392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/<CollectProperties>c__AnonStorey0
struct  U3CCollectPropertiesU3Ec__AnonStorey0_t1189373392  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo FullSerializer.fsMetaType/<CollectProperties>c__AnonStorey0::member
	MemberInfo_t * ___member_0;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(U3CCollectPropertiesU3Ec__AnonStorey0_t1189373392, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLLECTPROPERTIESU3EC__ANONSTOREY0_T1189373392_H
#ifndef U3CCANSERIALIZEPROPERTYU3EC__ANONSTOREY1_T1124286439_H
#define U3CCANSERIALIZEPROPERTYU3EC__ANONSTOREY1_T1124286439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/<CanSerializeProperty>c__AnonStorey1
struct  U3CCanSerializePropertyU3Ec__AnonStorey1_t1124286439  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo FullSerializer.fsMetaType/<CanSerializeProperty>c__AnonStorey1::property
	PropertyInfo_t * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CCanSerializePropertyU3Ec__AnonStorey1_t1124286439, ___property_0)); }
	inline PropertyInfo_t * get_property_0() const { return ___property_0; }
	inline PropertyInfo_t ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(PropertyInfo_t * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANSERIALIZEPROPERTYU3EC__ANONSTOREY1_T1124286439_H
#ifndef U3CGETFLATTENEDMETHODSU3EC__ITERATOR0_T1229162269_H
#define U3CGETFLATTENEDMETHODSU3EC__ITERATOR0_T1229162269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0
struct  U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269  : public RuntimeObject
{
public:
	// System.Type FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::type
	Type_t * ___type_0;
	// System.Reflection.MethodInfo[] FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::<methods>__1
	MethodInfoU5BU5D_t2572182361* ___U3CmethodsU3E__1_1;
	// System.Int32 FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_2;
	// System.String FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::methodName
	String_t* ___methodName_3;
	// System.Reflection.MethodInfo FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::$current
	MethodInfo_t * ___U24current_4;
	// System.Boolean FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Type FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::<$>type
	Type_t * ___U3CU24U3Etype_6;
	// System.Int32 FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_U3CmethodsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___U3CmethodsU3E__1_1)); }
	inline MethodInfoU5BU5D_t2572182361* get_U3CmethodsU3E__1_1() const { return ___U3CmethodsU3E__1_1; }
	inline MethodInfoU5BU5D_t2572182361** get_address_of_U3CmethodsU3E__1_1() { return &___U3CmethodsU3E__1_1; }
	inline void set_U3CmethodsU3E__1_1(MethodInfoU5BU5D_t2572182361* value)
	{
		___U3CmethodsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmethodsU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___U3CiU3E__2_2)); }
	inline int32_t get_U3CiU3E__2_2() const { return ___U3CiU3E__2_2; }
	inline int32_t* get_address_of_U3CiU3E__2_2() { return &___U3CiU3E__2_2; }
	inline void set_U3CiU3E__2_2(int32_t value)
	{
		___U3CiU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_methodName_3() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___methodName_3)); }
	inline String_t* get_methodName_3() const { return ___methodName_3; }
	inline String_t** get_address_of_methodName_3() { return &___methodName_3; }
	inline void set_methodName_3(String_t* value)
	{
		___methodName_3 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___U24current_4)); }
	inline MethodInfo_t * get_U24current_4() const { return ___U24current_4; }
	inline MethodInfo_t ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(MethodInfo_t * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_6() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___U3CU24U3Etype_6)); }
	inline Type_t * get_U3CU24U3Etype_6() const { return ___U3CU24U3Etype_6; }
	inline Type_t ** get_address_of_U3CU24U3Etype_6() { return &___U3CU24U3Etype_6; }
	inline void set_U3CU24U3Etype_6(Type_t * value)
	{
		___U3CU24U3Etype_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Etype_6), value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFLATTENEDMETHODSU3EC__ITERATOR0_T1229162269_H
#ifndef FSJSONPRINTER_T3181599744_H
#define FSJSONPRINTER_T3181599744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsJsonPrinter
struct  fsJsonPrinter_t3181599744  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSJSONPRINTER_T3181599744_H
#ifndef FSJSONPARSER_T2842196407_H
#define FSJSONPARSER_T2842196407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsJsonParser
struct  fsJsonParser_t2842196407  : public RuntimeObject
{
public:
	// System.Int32 FullSerializer.fsJsonParser::_start
	int32_t ____start_0;
	// System.String FullSerializer.fsJsonParser::_input
	String_t* ____input_1;
	// System.Text.StringBuilder FullSerializer.fsJsonParser::_cachedStringBuilder
	StringBuilder_t * ____cachedStringBuilder_2;

public:
	inline static int32_t get_offset_of__start_0() { return static_cast<int32_t>(offsetof(fsJsonParser_t2842196407, ____start_0)); }
	inline int32_t get__start_0() const { return ____start_0; }
	inline int32_t* get_address_of__start_0() { return &____start_0; }
	inline void set__start_0(int32_t value)
	{
		____start_0 = value;
	}

	inline static int32_t get_offset_of__input_1() { return static_cast<int32_t>(offsetof(fsJsonParser_t2842196407, ____input_1)); }
	inline String_t* get__input_1() const { return ____input_1; }
	inline String_t** get_address_of__input_1() { return &____input_1; }
	inline void set__input_1(String_t* value)
	{
		____input_1 = value;
		Il2CppCodeGenWriteBarrier((&____input_1), value);
	}

	inline static int32_t get_offset_of__cachedStringBuilder_2() { return static_cast<int32_t>(offsetof(fsJsonParser_t2842196407, ____cachedStringBuilder_2)); }
	inline StringBuilder_t * get__cachedStringBuilder_2() const { return ____cachedStringBuilder_2; }
	inline StringBuilder_t ** get_address_of__cachedStringBuilder_2() { return &____cachedStringBuilder_2; }
	inline void set__cachedStringBuilder_2(StringBuilder_t * value)
	{
		____cachedStringBuilder_2 = value;
		Il2CppCodeGenWriteBarrier((&____cachedStringBuilder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSJSONPARSER_T2842196407_H
#ifndef FSSERIALIZER_T4093814827_H
#define FSSERIALIZER_T4093814827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsSerializer
struct  fsSerializer_t4093814827  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter> FullSerializer.fsSerializer::_cachedConverterTypeInstances
	Dictionary_2_t4012097003 * ____cachedConverterTypeInstances_6;
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter> FullSerializer.fsSerializer::_cachedConverters
	Dictionary_2_t4012097003 * ____cachedConverters_7;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>> FullSerializer.fsSerializer::_cachedProcessors
	Dictionary_2_t40823354 * ____cachedProcessors_8;
	// System.Collections.Generic.List`1<FullSerializer.fsConverter> FullSerializer.fsSerializer::_availableConverters
	List_1_t3412709762 * ____availableConverters_9;
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsDirectConverter> FullSerializer.fsSerializer::_availableDirectConverters
	Dictionary_2_t1339064699 * ____availableDirectConverters_10;
	// System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor> FullSerializer.fsSerializer::_processors
	List_1_t1891443586 * ____processors_11;
	// FullSerializer.Internal.fsCyclicReferenceManager FullSerializer.fsSerializer::_references
	fsCyclicReferenceManager_t2963233308 * ____references_12;
	// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter FullSerializer.fsSerializer::_lazyReferenceWriter
	fsLazyCycleDefinitionWriter_t239783049 * ____lazyReferenceWriter_13;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> FullSerializer.fsSerializer::_abstractTypeRemap
	Dictionary_2_t633324528 * ____abstractTypeRemap_14;
	// FullSerializer.fsContext FullSerializer.fsSerializer::Context
	fsContext_t2261407774 * ___Context_15;
	// FullSerializer.fsConfig FullSerializer.fsSerializer::Config
	fsConfig_t14416447 * ___Config_16;

public:
	inline static int32_t get_offset_of__cachedConverterTypeInstances_6() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____cachedConverterTypeInstances_6)); }
	inline Dictionary_2_t4012097003 * get__cachedConverterTypeInstances_6() const { return ____cachedConverterTypeInstances_6; }
	inline Dictionary_2_t4012097003 ** get_address_of__cachedConverterTypeInstances_6() { return &____cachedConverterTypeInstances_6; }
	inline void set__cachedConverterTypeInstances_6(Dictionary_2_t4012097003 * value)
	{
		____cachedConverterTypeInstances_6 = value;
		Il2CppCodeGenWriteBarrier((&____cachedConverterTypeInstances_6), value);
	}

	inline static int32_t get_offset_of__cachedConverters_7() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____cachedConverters_7)); }
	inline Dictionary_2_t4012097003 * get__cachedConverters_7() const { return ____cachedConverters_7; }
	inline Dictionary_2_t4012097003 ** get_address_of__cachedConverters_7() { return &____cachedConverters_7; }
	inline void set__cachedConverters_7(Dictionary_2_t4012097003 * value)
	{
		____cachedConverters_7 = value;
		Il2CppCodeGenWriteBarrier((&____cachedConverters_7), value);
	}

	inline static int32_t get_offset_of__cachedProcessors_8() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____cachedProcessors_8)); }
	inline Dictionary_2_t40823354 * get__cachedProcessors_8() const { return ____cachedProcessors_8; }
	inline Dictionary_2_t40823354 ** get_address_of__cachedProcessors_8() { return &____cachedProcessors_8; }
	inline void set__cachedProcessors_8(Dictionary_2_t40823354 * value)
	{
		____cachedProcessors_8 = value;
		Il2CppCodeGenWriteBarrier((&____cachedProcessors_8), value);
	}

	inline static int32_t get_offset_of__availableConverters_9() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____availableConverters_9)); }
	inline List_1_t3412709762 * get__availableConverters_9() const { return ____availableConverters_9; }
	inline List_1_t3412709762 ** get_address_of__availableConverters_9() { return &____availableConverters_9; }
	inline void set__availableConverters_9(List_1_t3412709762 * value)
	{
		____availableConverters_9 = value;
		Il2CppCodeGenWriteBarrier((&____availableConverters_9), value);
	}

	inline static int32_t get_offset_of__availableDirectConverters_10() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____availableDirectConverters_10)); }
	inline Dictionary_2_t1339064699 * get__availableDirectConverters_10() const { return ____availableDirectConverters_10; }
	inline Dictionary_2_t1339064699 ** get_address_of__availableDirectConverters_10() { return &____availableDirectConverters_10; }
	inline void set__availableDirectConverters_10(Dictionary_2_t1339064699 * value)
	{
		____availableDirectConverters_10 = value;
		Il2CppCodeGenWriteBarrier((&____availableDirectConverters_10), value);
	}

	inline static int32_t get_offset_of__processors_11() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____processors_11)); }
	inline List_1_t1891443586 * get__processors_11() const { return ____processors_11; }
	inline List_1_t1891443586 ** get_address_of__processors_11() { return &____processors_11; }
	inline void set__processors_11(List_1_t1891443586 * value)
	{
		____processors_11 = value;
		Il2CppCodeGenWriteBarrier((&____processors_11), value);
	}

	inline static int32_t get_offset_of__references_12() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____references_12)); }
	inline fsCyclicReferenceManager_t2963233308 * get__references_12() const { return ____references_12; }
	inline fsCyclicReferenceManager_t2963233308 ** get_address_of__references_12() { return &____references_12; }
	inline void set__references_12(fsCyclicReferenceManager_t2963233308 * value)
	{
		____references_12 = value;
		Il2CppCodeGenWriteBarrier((&____references_12), value);
	}

	inline static int32_t get_offset_of__lazyReferenceWriter_13() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____lazyReferenceWriter_13)); }
	inline fsLazyCycleDefinitionWriter_t239783049 * get__lazyReferenceWriter_13() const { return ____lazyReferenceWriter_13; }
	inline fsLazyCycleDefinitionWriter_t239783049 ** get_address_of__lazyReferenceWriter_13() { return &____lazyReferenceWriter_13; }
	inline void set__lazyReferenceWriter_13(fsLazyCycleDefinitionWriter_t239783049 * value)
	{
		____lazyReferenceWriter_13 = value;
		Il2CppCodeGenWriteBarrier((&____lazyReferenceWriter_13), value);
	}

	inline static int32_t get_offset_of__abstractTypeRemap_14() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ____abstractTypeRemap_14)); }
	inline Dictionary_2_t633324528 * get__abstractTypeRemap_14() const { return ____abstractTypeRemap_14; }
	inline Dictionary_2_t633324528 ** get_address_of__abstractTypeRemap_14() { return &____abstractTypeRemap_14; }
	inline void set__abstractTypeRemap_14(Dictionary_2_t633324528 * value)
	{
		____abstractTypeRemap_14 = value;
		Il2CppCodeGenWriteBarrier((&____abstractTypeRemap_14), value);
	}

	inline static int32_t get_offset_of_Context_15() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ___Context_15)); }
	inline fsContext_t2261407774 * get_Context_15() const { return ___Context_15; }
	inline fsContext_t2261407774 ** get_address_of_Context_15() { return &___Context_15; }
	inline void set_Context_15(fsContext_t2261407774 * value)
	{
		___Context_15 = value;
		Il2CppCodeGenWriteBarrier((&___Context_15), value);
	}

	inline static int32_t get_offset_of_Config_16() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827, ___Config_16)); }
	inline fsConfig_t14416447 * get_Config_16() const { return ___Config_16; }
	inline fsConfig_t14416447 ** get_address_of_Config_16() { return &___Config_16; }
	inline void set_Config_16(fsConfig_t14416447 * value)
	{
		___Config_16 = value;
		Il2CppCodeGenWriteBarrier((&___Config_16), value);
	}
};

struct fsSerializer_t4093814827_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> FullSerializer.fsSerializer::_reservedKeywords
	HashSet_1_t412400163 * ____reservedKeywords_0;
	// System.String FullSerializer.fsSerializer::Key_ObjectReference
	String_t* ___Key_ObjectReference_1;
	// System.String FullSerializer.fsSerializer::Key_ObjectDefinition
	String_t* ___Key_ObjectDefinition_2;
	// System.String FullSerializer.fsSerializer::Key_InstanceType
	String_t* ___Key_InstanceType_3;
	// System.String FullSerializer.fsSerializer::Key_Version
	String_t* ___Key_Version_4;
	// System.String FullSerializer.fsSerializer::Key_Content
	String_t* ___Key_Content_5;

public:
	inline static int32_t get_offset_of__reservedKeywords_0() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827_StaticFields, ____reservedKeywords_0)); }
	inline HashSet_1_t412400163 * get__reservedKeywords_0() const { return ____reservedKeywords_0; }
	inline HashSet_1_t412400163 ** get_address_of__reservedKeywords_0() { return &____reservedKeywords_0; }
	inline void set__reservedKeywords_0(HashSet_1_t412400163 * value)
	{
		____reservedKeywords_0 = value;
		Il2CppCodeGenWriteBarrier((&____reservedKeywords_0), value);
	}

	inline static int32_t get_offset_of_Key_ObjectReference_1() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827_StaticFields, ___Key_ObjectReference_1)); }
	inline String_t* get_Key_ObjectReference_1() const { return ___Key_ObjectReference_1; }
	inline String_t** get_address_of_Key_ObjectReference_1() { return &___Key_ObjectReference_1; }
	inline void set_Key_ObjectReference_1(String_t* value)
	{
		___Key_ObjectReference_1 = value;
		Il2CppCodeGenWriteBarrier((&___Key_ObjectReference_1), value);
	}

	inline static int32_t get_offset_of_Key_ObjectDefinition_2() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827_StaticFields, ___Key_ObjectDefinition_2)); }
	inline String_t* get_Key_ObjectDefinition_2() const { return ___Key_ObjectDefinition_2; }
	inline String_t** get_address_of_Key_ObjectDefinition_2() { return &___Key_ObjectDefinition_2; }
	inline void set_Key_ObjectDefinition_2(String_t* value)
	{
		___Key_ObjectDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___Key_ObjectDefinition_2), value);
	}

	inline static int32_t get_offset_of_Key_InstanceType_3() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827_StaticFields, ___Key_InstanceType_3)); }
	inline String_t* get_Key_InstanceType_3() const { return ___Key_InstanceType_3; }
	inline String_t** get_address_of_Key_InstanceType_3() { return &___Key_InstanceType_3; }
	inline void set_Key_InstanceType_3(String_t* value)
	{
		___Key_InstanceType_3 = value;
		Il2CppCodeGenWriteBarrier((&___Key_InstanceType_3), value);
	}

	inline static int32_t get_offset_of_Key_Version_4() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827_StaticFields, ___Key_Version_4)); }
	inline String_t* get_Key_Version_4() const { return ___Key_Version_4; }
	inline String_t** get_address_of_Key_Version_4() { return &___Key_Version_4; }
	inline void set_Key_Version_4(String_t* value)
	{
		___Key_Version_4 = value;
		Il2CppCodeGenWriteBarrier((&___Key_Version_4), value);
	}

	inline static int32_t get_offset_of_Key_Content_5() { return static_cast<int32_t>(offsetof(fsSerializer_t4093814827_StaticFields, ___Key_Content_5)); }
	inline String_t* get_Key_Content_5() const { return ___Key_Content_5; }
	inline String_t** get_address_of_Key_Content_5() { return &___Key_Content_5; }
	inline void set_Key_Content_5(String_t* value)
	{
		___Key_Content_5 = value;
		Il2CppCodeGenWriteBarrier((&___Key_Content_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSSERIALIZER_T4093814827_H
#ifndef FSLAZYCYCLEDEFINITIONWRITER_T239783049_H
#define FSLAZYCYCLEDEFINITIONWRITER_T239783049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter
struct  fsLazyCycleDefinitionWriter_t239783049  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,FullSerializer.fsData> FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter::_pendingDefinitions
	Dictionary_2_t3590453914 * ____pendingDefinitions_0;
	// System.Collections.Generic.HashSet`1<System.Int32> FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter::_references
	HashSet_1_t1515895227 * ____references_1;

public:
	inline static int32_t get_offset_of__pendingDefinitions_0() { return static_cast<int32_t>(offsetof(fsLazyCycleDefinitionWriter_t239783049, ____pendingDefinitions_0)); }
	inline Dictionary_2_t3590453914 * get__pendingDefinitions_0() const { return ____pendingDefinitions_0; }
	inline Dictionary_2_t3590453914 ** get_address_of__pendingDefinitions_0() { return &____pendingDefinitions_0; }
	inline void set__pendingDefinitions_0(Dictionary_2_t3590453914 * value)
	{
		____pendingDefinitions_0 = value;
		Il2CppCodeGenWriteBarrier((&____pendingDefinitions_0), value);
	}

	inline static int32_t get_offset_of__references_1() { return static_cast<int32_t>(offsetof(fsLazyCycleDefinitionWriter_t239783049, ____references_1)); }
	inline HashSet_1_t1515895227 * get__references_1() const { return ____references_1; }
	inline HashSet_1_t1515895227 ** get_address_of__references_1() { return &____references_1; }
	inline void set__references_1(HashSet_1_t1515895227 * value)
	{
		____references_1 = value;
		Il2CppCodeGenWriteBarrier((&____references_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSLAZYCYCLEDEFINITIONWRITER_T239783049_H
#ifndef FSCYCLICREFERENCEMANAGER_T2963233308_H
#define FSCYCLICREFERENCEMANAGER_T2963233308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsCyclicReferenceManager
struct  fsCyclicReferenceManager_t2963233308  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Object,System.Int32> FullSerializer.Internal.fsCyclicReferenceManager::_objectIds
	Dictionary_2_t3384741 * ____objectIds_0;
	// System.Int32 FullSerializer.Internal.fsCyclicReferenceManager::_nextId
	int32_t ____nextId_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object> FullSerializer.Internal.fsCyclicReferenceManager::_marked
	Dictionary_2_t1968819495 * ____marked_2;
	// System.Int32 FullSerializer.Internal.fsCyclicReferenceManager::_depth
	int32_t ____depth_3;

public:
	inline static int32_t get_offset_of__objectIds_0() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t2963233308, ____objectIds_0)); }
	inline Dictionary_2_t3384741 * get__objectIds_0() const { return ____objectIds_0; }
	inline Dictionary_2_t3384741 ** get_address_of__objectIds_0() { return &____objectIds_0; }
	inline void set__objectIds_0(Dictionary_2_t3384741 * value)
	{
		____objectIds_0 = value;
		Il2CppCodeGenWriteBarrier((&____objectIds_0), value);
	}

	inline static int32_t get_offset_of__nextId_1() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t2963233308, ____nextId_1)); }
	inline int32_t get__nextId_1() const { return ____nextId_1; }
	inline int32_t* get_address_of__nextId_1() { return &____nextId_1; }
	inline void set__nextId_1(int32_t value)
	{
		____nextId_1 = value;
	}

	inline static int32_t get_offset_of__marked_2() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t2963233308, ____marked_2)); }
	inline Dictionary_2_t1968819495 * get__marked_2() const { return ____marked_2; }
	inline Dictionary_2_t1968819495 ** get_address_of__marked_2() { return &____marked_2; }
	inline void set__marked_2(Dictionary_2_t1968819495 * value)
	{
		____marked_2 = value;
		Il2CppCodeGenWriteBarrier((&____marked_2), value);
	}

	inline static int32_t get_offset_of__depth_3() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t2963233308, ____depth_3)); }
	inline int32_t get__depth_3() const { return ____depth_3; }
	inline int32_t* get_address_of__depth_3() { return &____depth_3; }
	inline void set__depth_3(int32_t value)
	{
		____depth_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCYCLICREFERENCEMANAGER_T2963233308_H
#ifndef OBJECTREFERENCEEQUALITYCOMPARATOR_T1966295411_H
#define OBJECTREFERENCEEQUALITYCOMPARATOR_T1966295411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsCyclicReferenceManager/ObjectReferenceEqualityComparator
struct  ObjectReferenceEqualityComparator_t1966295411  : public RuntimeObject
{
public:

public:
};

struct ObjectReferenceEqualityComparator_t1966295411_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.Object> FullSerializer.Internal.fsCyclicReferenceManager/ObjectReferenceEqualityComparator::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(ObjectReferenceEqualityComparator_t1966295411_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTREFERENCEEQUALITYCOMPARATOR_T1966295411_H
#ifndef FSOPTION_T2623647891_H
#define FSOPTION_T2623647891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsOption
struct  fsOption_t2623647891  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSOPTION_T2623647891_H
#ifndef FSDATA_T406773287_H
#define FSDATA_T406773287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsData
struct  fsData_t406773287  : public RuntimeObject
{
public:
	// System.Object FullSerializer.fsData::_value
	RuntimeObject * ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(fsData_t406773287, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}
};

struct fsData_t406773287_StaticFields
{
public:
	// FullSerializer.fsData FullSerializer.fsData::True
	fsData_t406773287 * ___True_1;
	// FullSerializer.fsData FullSerializer.fsData::False
	fsData_t406773287 * ___False_2;
	// FullSerializer.fsData FullSerializer.fsData::Null
	fsData_t406773287 * ___Null_3;

public:
	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(fsData_t406773287_StaticFields, ___True_1)); }
	inline fsData_t406773287 * get_True_1() const { return ___True_1; }
	inline fsData_t406773287 ** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(fsData_t406773287 * value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((&___True_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(fsData_t406773287_StaticFields, ___False_2)); }
	inline fsData_t406773287 * get_False_2() const { return ___False_2; }
	inline fsData_t406773287 ** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(fsData_t406773287 * value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(fsData_t406773287_StaticFields, ___Null_3)); }
	inline fsData_t406773287 * get_Null_3() const { return ___Null_3; }
	inline fsData_t406773287 ** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(fsData_t406773287 * value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((&___Null_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATA_T406773287_H
#ifndef ATTRIBUTEQUERYCOMPARATOR_T2576872653_H
#define ATTRIBUTEQUERYCOMPARATOR_T2576872653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection/AttributeQueryComparator
struct  AttributeQueryComparator_t2576872653  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEQUERYCOMPARATOR_T2576872653_H
#ifndef U3CCANSERIALIZEFIELDU3EC__ANONSTOREY2_T1489263536_H
#define U3CCANSERIALIZEFIELDU3EC__ANONSTOREY2_T1489263536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/<CanSerializeField>c__AnonStorey2
struct  U3CCanSerializeFieldU3Ec__AnonStorey2_t1489263536  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo FullSerializer.fsMetaType/<CanSerializeField>c__AnonStorey2::field
	FieldInfo_t * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(U3CCanSerializeFieldU3Ec__AnonStorey2_t1489263536, ___field_0)); }
	inline FieldInfo_t * get_field_0() const { return ___field_0; }
	inline FieldInfo_t ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(FieldInfo_t * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANSERIALIZEFIELDU3EC__ANONSTOREY2_T1489263536_H
#ifndef U3CDESTROYPOPUPU3EC__ITERATOR0_T1470244993_H
#define U3CDESTROYPOPUPU3EC__ITERATOR0_T1470244993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.Popup/<DestroyPopup>c__Iterator0
struct  U3CDestroyPopupU3Ec__Iterator0_t1470244993  : public RuntimeObject
{
public:
	// GameVanilla.Core.Popup GameVanilla.Core.Popup/<DestroyPopup>c__Iterator0::$this
	Popup_t1063720813 * ___U24this_0;
	// System.Object GameVanilla.Core.Popup/<DestroyPopup>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Core.Popup/<DestroyPopup>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Core.Popup/<DestroyPopup>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDestroyPopupU3Ec__Iterator0_t1470244993, ___U24this_0)); }
	inline Popup_t1063720813 * get_U24this_0() const { return ___U24this_0; }
	inline Popup_t1063720813 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Popup_t1063720813 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDestroyPopupU3Ec__Iterator0_t1470244993, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDestroyPopupU3Ec__Iterator0_t1470244993, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDestroyPopupU3Ec__Iterator0_t1470244993, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYPOPUPU3EC__ITERATOR0_T1470244993_H
#ifndef REFLECTIONUTILS_T3994853556_H
#define REFLECTIONUTILS_T3994853556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.ReflectionUtils
struct  ReflectionUtils_t3994853556  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t3994853556_StaticFields
{
public:
	// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>> GameVanilla.Core.ReflectionUtils::<>f__am$cache0
	Func_2_t779105388 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`3<System.Reflection.Assembly,System.Type,<>__AnonType0`2<System.Reflection.Assembly,System.Type>> GameVanilla.Core.ReflectionUtils::<>f__am$cache1
	Func_3_t3987483980 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<<>__AnonType0`2<System.Reflection.Assembly,System.Type>,System.Type> GameVanilla.Core.ReflectionUtils::<>f__am$cache2
	Func_2_t3063509026 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3994853556_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t779105388 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t779105388 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t779105388 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3994853556_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_3_t3987483980 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_3_t3987483980 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_3_t3987483980 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3994853556_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t3063509026 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t3063509026 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t3063509026 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T3994853556_H
#ifndef U3CGETALLDERIVEDTYPESU3EC__ANONSTOREY0_T2222601090_H
#define U3CGETALLDERIVEDTYPESU3EC__ANONSTOREY0_T2222601090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.ReflectionUtils/<GetAllDerivedTypes>c__AnonStorey0
struct  U3CGetAllDerivedTypesU3Ec__AnonStorey0_t2222601090  : public RuntimeObject
{
public:
	// System.Type GameVanilla.Core.ReflectionUtils/<GetAllDerivedTypes>c__AnonStorey0::aType
	Type_t * ___aType_0;

public:
	inline static int32_t get_offset_of_aType_0() { return static_cast<int32_t>(offsetof(U3CGetAllDerivedTypesU3Ec__AnonStorey0_t2222601090, ___aType_0)); }
	inline Type_t * get_aType_0() const { return ___aType_0; }
	inline Type_t ** get_address_of_aType_0() { return &___aType_0; }
	inline void set_aType_0(Type_t * value)
	{
		___aType_0 = value;
		Il2CppCodeGenWriteBarrier((&___aType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLDERIVEDTYPESU3EC__ANONSTOREY0_T2222601090_H
#ifndef STRINGUTILS_T120023749_H
#define STRINGUTILS_T120023749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.StringUtils
struct  StringUtils_t120023749  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T120023749_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef FILEUTILS_T3666727464_H
#define FILEUTILS_T3666727464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.FileUtils
struct  FileUtils_t3666727464  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_T3666727464_H
#ifndef FSREFLECTIONUTILITY_T2837052610_H
#define FSREFLECTIONUTILITY_T2837052610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsReflectionUtility
struct  fsReflectionUtility_t2837052610  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSREFLECTIONUTILITY_T2837052610_H
#ifndef FSTYPECACHE_T3469578604_H
#define FSTYPECACHE_T3469578604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsTypeCache
struct  fsTypeCache_t3469578604  : public RuntimeObject
{
public:

public:
};

struct fsTypeCache_t3469578604_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> FullSerializer.Internal.fsTypeCache::_cachedTypes
	Dictionary_2_t2269201059 * ____cachedTypes_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.Assembly> FullSerializer.Internal.fsTypeCache::_assembliesByName
	Dictionary_2_t3887689098 * ____assembliesByName_1;
	// System.Collections.Generic.List`1<System.Reflection.Assembly> FullSerializer.Internal.fsTypeCache::_assembliesByIndex
	List_1_t1279540245 * ____assembliesByIndex_2;
	// System.AssemblyLoadEventHandler FullSerializer.Internal.fsTypeCache::<>f__mg$cache0
	AssemblyLoadEventHandler_t107971893 * ___U3CU3Ef__mgU24cache0_3;

public:
	inline static int32_t get_offset_of__cachedTypes_0() { return static_cast<int32_t>(offsetof(fsTypeCache_t3469578604_StaticFields, ____cachedTypes_0)); }
	inline Dictionary_2_t2269201059 * get__cachedTypes_0() const { return ____cachedTypes_0; }
	inline Dictionary_2_t2269201059 ** get_address_of__cachedTypes_0() { return &____cachedTypes_0; }
	inline void set__cachedTypes_0(Dictionary_2_t2269201059 * value)
	{
		____cachedTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____cachedTypes_0), value);
	}

	inline static int32_t get_offset_of__assembliesByName_1() { return static_cast<int32_t>(offsetof(fsTypeCache_t3469578604_StaticFields, ____assembliesByName_1)); }
	inline Dictionary_2_t3887689098 * get__assembliesByName_1() const { return ____assembliesByName_1; }
	inline Dictionary_2_t3887689098 ** get_address_of__assembliesByName_1() { return &____assembliesByName_1; }
	inline void set__assembliesByName_1(Dictionary_2_t3887689098 * value)
	{
		____assembliesByName_1 = value;
		Il2CppCodeGenWriteBarrier((&____assembliesByName_1), value);
	}

	inline static int32_t get_offset_of__assembliesByIndex_2() { return static_cast<int32_t>(offsetof(fsTypeCache_t3469578604_StaticFields, ____assembliesByIndex_2)); }
	inline List_1_t1279540245 * get__assembliesByIndex_2() const { return ____assembliesByIndex_2; }
	inline List_1_t1279540245 ** get_address_of__assembliesByIndex_2() { return &____assembliesByIndex_2; }
	inline void set__assembliesByIndex_2(List_1_t1279540245 * value)
	{
		____assembliesByIndex_2 = value;
		Il2CppCodeGenWriteBarrier((&____assembliesByIndex_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(fsTypeCache_t3469578604_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline AssemblyLoadEventHandler_t107971893 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline AssemblyLoadEventHandler_t107971893 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(AssemblyLoadEventHandler_t107971893 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSTYPECACHE_T3469578604_H
#ifndef FSCONVERTERREGISTRAR_T1190956781_H
#define FSCONVERTERREGISTRAR_T1190956781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsConverterRegistrar
struct  fsConverterRegistrar_t1190956781  : public RuntimeObject
{
public:

public:
};

struct fsConverterRegistrar_t1190956781_StaticFields
{
public:
	// FullSerializer.Internal.DirectConverters.AnimationCurve_DirectConverter FullSerializer.fsConverterRegistrar::Register_AnimationCurve_DirectConverter
	AnimationCurve_DirectConverter_t2921849045 * ___Register_AnimationCurve_DirectConverter_0;
	// FullSerializer.Internal.DirectConverters.Bounds_DirectConverter FullSerializer.fsConverterRegistrar::Register_Bounds_DirectConverter
	Bounds_DirectConverter_t3871173093 * ___Register_Bounds_DirectConverter_1;
	// FullSerializer.Internal.DirectConverters.Gradient_DirectConverter FullSerializer.fsConverterRegistrar::Register_Gradient_DirectConverter
	Gradient_DirectConverter_t2745360243 * ___Register_Gradient_DirectConverter_2;
	// FullSerializer.Internal.DirectConverters.GUIStyle_DirectConverter FullSerializer.fsConverterRegistrar::Register_GUIStyle_DirectConverter
	GUIStyle_DirectConverter_t1060365653 * ___Register_GUIStyle_DirectConverter_3;
	// FullSerializer.Internal.DirectConverters.GUIStyleState_DirectConverter FullSerializer.fsConverterRegistrar::Register_GUIStyleState_DirectConverter
	GUIStyleState_DirectConverter_t3967104754 * ___Register_GUIStyleState_DirectConverter_4;
	// FullSerializer.Internal.DirectConverters.Keyframe_DirectConverter FullSerializer.fsConverterRegistrar::Register_Keyframe_DirectConverter
	Keyframe_DirectConverter_t1448983566 * ___Register_Keyframe_DirectConverter_5;
	// FullSerializer.Internal.DirectConverters.LayerMask_DirectConverter FullSerializer.fsConverterRegistrar::Register_LayerMask_DirectConverter
	LayerMask_DirectConverter_t4211565801 * ___Register_LayerMask_DirectConverter_6;
	// FullSerializer.Internal.DirectConverters.Rect_DirectConverter FullSerializer.fsConverterRegistrar::Register_Rect_DirectConverter
	Rect_DirectConverter_t1861700375 * ___Register_Rect_DirectConverter_7;
	// FullSerializer.Internal.DirectConverters.RectOffset_DirectConverter FullSerializer.fsConverterRegistrar::Register_RectOffset_DirectConverter
	RectOffset_DirectConverter_t483038648 * ___Register_RectOffset_DirectConverter_8;
	// System.Collections.Generic.List`1<System.Type> FullSerializer.fsConverterRegistrar::Converters
	List_1_t3956019502 * ___Converters_9;

public:
	inline static int32_t get_offset_of_Register_AnimationCurve_DirectConverter_0() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_AnimationCurve_DirectConverter_0)); }
	inline AnimationCurve_DirectConverter_t2921849045 * get_Register_AnimationCurve_DirectConverter_0() const { return ___Register_AnimationCurve_DirectConverter_0; }
	inline AnimationCurve_DirectConverter_t2921849045 ** get_address_of_Register_AnimationCurve_DirectConverter_0() { return &___Register_AnimationCurve_DirectConverter_0; }
	inline void set_Register_AnimationCurve_DirectConverter_0(AnimationCurve_DirectConverter_t2921849045 * value)
	{
		___Register_AnimationCurve_DirectConverter_0 = value;
		Il2CppCodeGenWriteBarrier((&___Register_AnimationCurve_DirectConverter_0), value);
	}

	inline static int32_t get_offset_of_Register_Bounds_DirectConverter_1() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_Bounds_DirectConverter_1)); }
	inline Bounds_DirectConverter_t3871173093 * get_Register_Bounds_DirectConverter_1() const { return ___Register_Bounds_DirectConverter_1; }
	inline Bounds_DirectConverter_t3871173093 ** get_address_of_Register_Bounds_DirectConverter_1() { return &___Register_Bounds_DirectConverter_1; }
	inline void set_Register_Bounds_DirectConverter_1(Bounds_DirectConverter_t3871173093 * value)
	{
		___Register_Bounds_DirectConverter_1 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Bounds_DirectConverter_1), value);
	}

	inline static int32_t get_offset_of_Register_Gradient_DirectConverter_2() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_Gradient_DirectConverter_2)); }
	inline Gradient_DirectConverter_t2745360243 * get_Register_Gradient_DirectConverter_2() const { return ___Register_Gradient_DirectConverter_2; }
	inline Gradient_DirectConverter_t2745360243 ** get_address_of_Register_Gradient_DirectConverter_2() { return &___Register_Gradient_DirectConverter_2; }
	inline void set_Register_Gradient_DirectConverter_2(Gradient_DirectConverter_t2745360243 * value)
	{
		___Register_Gradient_DirectConverter_2 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Gradient_DirectConverter_2), value);
	}

	inline static int32_t get_offset_of_Register_GUIStyle_DirectConverter_3() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_GUIStyle_DirectConverter_3)); }
	inline GUIStyle_DirectConverter_t1060365653 * get_Register_GUIStyle_DirectConverter_3() const { return ___Register_GUIStyle_DirectConverter_3; }
	inline GUIStyle_DirectConverter_t1060365653 ** get_address_of_Register_GUIStyle_DirectConverter_3() { return &___Register_GUIStyle_DirectConverter_3; }
	inline void set_Register_GUIStyle_DirectConverter_3(GUIStyle_DirectConverter_t1060365653 * value)
	{
		___Register_GUIStyle_DirectConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Register_GUIStyle_DirectConverter_3), value);
	}

	inline static int32_t get_offset_of_Register_GUIStyleState_DirectConverter_4() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_GUIStyleState_DirectConverter_4)); }
	inline GUIStyleState_DirectConverter_t3967104754 * get_Register_GUIStyleState_DirectConverter_4() const { return ___Register_GUIStyleState_DirectConverter_4; }
	inline GUIStyleState_DirectConverter_t3967104754 ** get_address_of_Register_GUIStyleState_DirectConverter_4() { return &___Register_GUIStyleState_DirectConverter_4; }
	inline void set_Register_GUIStyleState_DirectConverter_4(GUIStyleState_DirectConverter_t3967104754 * value)
	{
		___Register_GUIStyleState_DirectConverter_4 = value;
		Il2CppCodeGenWriteBarrier((&___Register_GUIStyleState_DirectConverter_4), value);
	}

	inline static int32_t get_offset_of_Register_Keyframe_DirectConverter_5() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_Keyframe_DirectConverter_5)); }
	inline Keyframe_DirectConverter_t1448983566 * get_Register_Keyframe_DirectConverter_5() const { return ___Register_Keyframe_DirectConverter_5; }
	inline Keyframe_DirectConverter_t1448983566 ** get_address_of_Register_Keyframe_DirectConverter_5() { return &___Register_Keyframe_DirectConverter_5; }
	inline void set_Register_Keyframe_DirectConverter_5(Keyframe_DirectConverter_t1448983566 * value)
	{
		___Register_Keyframe_DirectConverter_5 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Keyframe_DirectConverter_5), value);
	}

	inline static int32_t get_offset_of_Register_LayerMask_DirectConverter_6() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_LayerMask_DirectConverter_6)); }
	inline LayerMask_DirectConverter_t4211565801 * get_Register_LayerMask_DirectConverter_6() const { return ___Register_LayerMask_DirectConverter_6; }
	inline LayerMask_DirectConverter_t4211565801 ** get_address_of_Register_LayerMask_DirectConverter_6() { return &___Register_LayerMask_DirectConverter_6; }
	inline void set_Register_LayerMask_DirectConverter_6(LayerMask_DirectConverter_t4211565801 * value)
	{
		___Register_LayerMask_DirectConverter_6 = value;
		Il2CppCodeGenWriteBarrier((&___Register_LayerMask_DirectConverter_6), value);
	}

	inline static int32_t get_offset_of_Register_Rect_DirectConverter_7() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_Rect_DirectConverter_7)); }
	inline Rect_DirectConverter_t1861700375 * get_Register_Rect_DirectConverter_7() const { return ___Register_Rect_DirectConverter_7; }
	inline Rect_DirectConverter_t1861700375 ** get_address_of_Register_Rect_DirectConverter_7() { return &___Register_Rect_DirectConverter_7; }
	inline void set_Register_Rect_DirectConverter_7(Rect_DirectConverter_t1861700375 * value)
	{
		___Register_Rect_DirectConverter_7 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Rect_DirectConverter_7), value);
	}

	inline static int32_t get_offset_of_Register_RectOffset_DirectConverter_8() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Register_RectOffset_DirectConverter_8)); }
	inline RectOffset_DirectConverter_t483038648 * get_Register_RectOffset_DirectConverter_8() const { return ___Register_RectOffset_DirectConverter_8; }
	inline RectOffset_DirectConverter_t483038648 ** get_address_of_Register_RectOffset_DirectConverter_8() { return &___Register_RectOffset_DirectConverter_8; }
	inline void set_Register_RectOffset_DirectConverter_8(RectOffset_DirectConverter_t483038648 * value)
	{
		___Register_RectOffset_DirectConverter_8 = value;
		Il2CppCodeGenWriteBarrier((&___Register_RectOffset_DirectConverter_8), value);
	}

	inline static int32_t get_offset_of_Converters_9() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t1190956781_StaticFields, ___Converters_9)); }
	inline List_1_t3956019502 * get_Converters_9() const { return ___Converters_9; }
	inline List_1_t3956019502 ** get_address_of_Converters_9() { return &___Converters_9; }
	inline void set_Converters_9(List_1_t3956019502 * value)
	{
		___Converters_9 = value;
		Il2CppCodeGenWriteBarrier((&___Converters_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONVERTERREGISTRAR_T1190956781_H
#ifndef FSOBJECTPROCESSOR_T419368844_H
#define FSOBJECTPROCESSOR_T419368844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsObjectProcessor
struct  fsObjectProcessor_t419368844  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSOBJECTPROCESSOR_T419368844_H
#ifndef U3CINVOKEONCLICKACTIONU3EC__ITERATOR0_T2423343608_H
#define U3CINVOKEONCLICKACTIONU3EC__ITERATOR0_T2423343608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.AnimatedButton/<InvokeOnClickAction>c__Iterator0
struct  U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608  : public RuntimeObject
{
public:
	// GameVanilla.Core.AnimatedButton GameVanilla.Core.AnimatedButton/<InvokeOnClickAction>c__Iterator0::$this
	AnimatedButton_t997585140 * ___U24this_0;
	// System.Object GameVanilla.Core.AnimatedButton/<InvokeOnClickAction>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Core.AnimatedButton/<InvokeOnClickAction>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Core.AnimatedButton/<InvokeOnClickAction>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608, ___U24this_0)); }
	inline AnimatedButton_t997585140 * get_U24this_0() const { return ___U24this_0; }
	inline AnimatedButton_t997585140 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimatedButton_t997585140 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEONCLICKACTIONU3EC__ITERATOR0_T2423343608_H
#ifndef U3CBLOCKINPUTTEMPORARILYU3EC__ITERATOR1_T1318499417_H
#define U3CBLOCKINPUTTEMPORARILYU3EC__ITERATOR1_T1318499417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.AnimatedButton/<BlockInputTemporarily>c__Iterator1
struct  U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417  : public RuntimeObject
{
public:
	// GameVanilla.Core.AnimatedButton GameVanilla.Core.AnimatedButton/<BlockInputTemporarily>c__Iterator1::$this
	AnimatedButton_t997585140 * ___U24this_0;
	// System.Object GameVanilla.Core.AnimatedButton/<BlockInputTemporarily>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameVanilla.Core.AnimatedButton/<BlockInputTemporarily>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameVanilla.Core.AnimatedButton/<BlockInputTemporarily>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417, ___U24this_0)); }
	inline AnimatedButton_t997585140 * get_U24this_0() const { return ___U24this_0; }
	inline AnimatedButton_t997585140 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimatedButton_t997585140 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBLOCKINPUTTEMPORARILYU3EC__ITERATOR1_T1318499417_H
#ifndef U3CCLOSEPOPUPU3EC__ANONSTOREY3_T2207380370_H
#define U3CCLOSEPOPUPU3EC__ANONSTOREY3_T2207380370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene/<ClosePopup>c__AnonStorey3
struct  U3CClosePopupU3Ec__AnonStorey3_t2207380370  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameVanilla.Core.BaseScene/<ClosePopup>c__AnonStorey3::topmostPanel
	GameObject_t1113636619 * ___topmostPanel_0;

public:
	inline static int32_t get_offset_of_topmostPanel_0() { return static_cast<int32_t>(offsetof(U3CClosePopupU3Ec__AnonStorey3_t2207380370, ___topmostPanel_0)); }
	inline GameObject_t1113636619 * get_topmostPanel_0() const { return ___topmostPanel_0; }
	inline GameObject_t1113636619 ** get_address_of_topmostPanel_0() { return &___topmostPanel_0; }
	inline void set_topmostPanel_0(GameObject_t1113636619 * value)
	{
		___topmostPanel_0 = value;
		Il2CppCodeGenWriteBarrier((&___topmostPanel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSEPOPUPU3EC__ANONSTOREY3_T2207380370_H
#ifndef FSPROPERTYATTRIBUTE_T3696058176_H
#define FSPROPERTYATTRIBUTE_T3696058176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsPropertyAttribute
struct  fsPropertyAttribute_t3696058176  : public Attribute_t861562559
{
public:
	// System.String FullSerializer.fsPropertyAttribute::Name
	String_t* ___Name_0;
	// System.Type FullSerializer.fsPropertyAttribute::Converter
	Type_t * ___Converter_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(fsPropertyAttribute_t3696058176, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Converter_1() { return static_cast<int32_t>(offsetof(fsPropertyAttribute_t3696058176, ___Converter_1)); }
	inline Type_t * get_Converter_1() const { return ___Converter_1; }
	inline Type_t ** get_address_of_Converter_1() { return &___Converter_1; }
	inline void set_Converter_1(Type_t * value)
	{
		___Converter_1 = value;
		Il2CppCodeGenWriteBarrier((&___Converter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSPROPERTYATTRIBUTE_T3696058176_H
#ifndef FSRESULT_T591339677_H
#define FSRESULT_T591339677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsResult
struct  fsResult_t591339677 
{
public:
	// System.Boolean FullSerializer.fsResult::_success
	bool ____success_1;
	// System.Collections.Generic.List`1<System.String> FullSerializer.fsResult::_messages
	List_1_t3319525431 * ____messages_2;

public:
	inline static int32_t get_offset_of__success_1() { return static_cast<int32_t>(offsetof(fsResult_t591339677, ____success_1)); }
	inline bool get__success_1() const { return ____success_1; }
	inline bool* get_address_of__success_1() { return &____success_1; }
	inline void set__success_1(bool value)
	{
		____success_1 = value;
	}

	inline static int32_t get_offset_of__messages_2() { return static_cast<int32_t>(offsetof(fsResult_t591339677, ____messages_2)); }
	inline List_1_t3319525431 * get__messages_2() const { return ____messages_2; }
	inline List_1_t3319525431 ** get_address_of__messages_2() { return &____messages_2; }
	inline void set__messages_2(List_1_t3319525431 * value)
	{
		____messages_2 = value;
		Il2CppCodeGenWriteBarrier((&____messages_2), value);
	}
};

struct fsResult_t591339677_StaticFields
{
public:
	// System.String[] FullSerializer.fsResult::EmptyStringArray
	StringU5BU5D_t1281789340* ___EmptyStringArray_0;
	// FullSerializer.fsResult FullSerializer.fsResult::Success
	fsResult_t591339677  ___Success_3;

public:
	inline static int32_t get_offset_of_EmptyStringArray_0() { return static_cast<int32_t>(offsetof(fsResult_t591339677_StaticFields, ___EmptyStringArray_0)); }
	inline StringU5BU5D_t1281789340* get_EmptyStringArray_0() const { return ___EmptyStringArray_0; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyStringArray_0() { return &___EmptyStringArray_0; }
	inline void set_EmptyStringArray_0(StringU5BU5D_t1281789340* value)
	{
		___EmptyStringArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyStringArray_0), value);
	}

	inline static int32_t get_offset_of_Success_3() { return static_cast<int32_t>(offsetof(fsResult_t591339677_StaticFields, ___Success_3)); }
	inline fsResult_t591339677  get_Success_3() const { return ___Success_3; }
	inline fsResult_t591339677 * get_address_of_Success_3() { return &___Success_3; }
	inline void set_Success_3(fsResult_t591339677  value)
	{
		___Success_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsResult
struct fsResult_t591339677_marshaled_pinvoke
{
	int32_t ____success_1;
	List_1_t3319525431 * ____messages_2;
};
// Native definition for COM marshalling of FullSerializer.fsResult
struct fsResult_t591339677_marshaled_com
{
	int32_t ____success_1;
	List_1_t3319525431 * ____messages_2;
};
#endif // FSRESULT_T591339677_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FSVERSIONEDTYPE_T1764735544_H
#define FSVERSIONEDTYPE_T1764735544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsVersionedType
struct  fsVersionedType_t1764735544 
{
public:
	// FullSerializer.Internal.fsVersionedType[] FullSerializer.Internal.fsVersionedType::Ancestors
	fsVersionedTypeU5BU5D_t710655465* ___Ancestors_0;
	// System.String FullSerializer.Internal.fsVersionedType::VersionString
	String_t* ___VersionString_1;
	// System.Type FullSerializer.Internal.fsVersionedType::ModelType
	Type_t * ___ModelType_2;

public:
	inline static int32_t get_offset_of_Ancestors_0() { return static_cast<int32_t>(offsetof(fsVersionedType_t1764735544, ___Ancestors_0)); }
	inline fsVersionedTypeU5BU5D_t710655465* get_Ancestors_0() const { return ___Ancestors_0; }
	inline fsVersionedTypeU5BU5D_t710655465** get_address_of_Ancestors_0() { return &___Ancestors_0; }
	inline void set_Ancestors_0(fsVersionedTypeU5BU5D_t710655465* value)
	{
		___Ancestors_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ancestors_0), value);
	}

	inline static int32_t get_offset_of_VersionString_1() { return static_cast<int32_t>(offsetof(fsVersionedType_t1764735544, ___VersionString_1)); }
	inline String_t* get_VersionString_1() const { return ___VersionString_1; }
	inline String_t** get_address_of_VersionString_1() { return &___VersionString_1; }
	inline void set_VersionString_1(String_t* value)
	{
		___VersionString_1 = value;
		Il2CppCodeGenWriteBarrier((&___VersionString_1), value);
	}

	inline static int32_t get_offset_of_ModelType_2() { return static_cast<int32_t>(offsetof(fsVersionedType_t1764735544, ___ModelType_2)); }
	inline Type_t * get_ModelType_2() const { return ___ModelType_2; }
	inline Type_t ** get_address_of_ModelType_2() { return &___ModelType_2; }
	inline void set_ModelType_2(Type_t * value)
	{
		___ModelType_2 = value;
		Il2CppCodeGenWriteBarrier((&___ModelType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.Internal.fsVersionedType
struct fsVersionedType_t1764735544_marshaled_pinvoke
{
	fsVersionedType_t1764735544_marshaled_pinvoke* ___Ancestors_0;
	char* ___VersionString_1;
	Type_t * ___ModelType_2;
};
// Native definition for COM marshalling of FullSerializer.Internal.fsVersionedType
struct fsVersionedType_t1764735544_marshaled_com
{
	fsVersionedType_t1764735544_marshaled_com* ___Ancestors_0;
	Il2CppChar* ___VersionString_1;
	Type_t * ___ModelType_2;
};
#endif // FSVERSIONEDTYPE_T1764735544_H
#ifndef ATTRIBUTEQUERY_T874673239_H
#define ATTRIBUTEQUERY_T874673239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection/AttributeQuery
struct  AttributeQuery_t874673239 
{
public:
	// System.Reflection.MemberInfo FullSerializer.Internal.fsPortableReflection/AttributeQuery::MemberInfo
	MemberInfo_t * ___MemberInfo_0;
	// System.Type FullSerializer.Internal.fsPortableReflection/AttributeQuery::AttributeType
	Type_t * ___AttributeType_1;

public:
	inline static int32_t get_offset_of_MemberInfo_0() { return static_cast<int32_t>(offsetof(AttributeQuery_t874673239, ___MemberInfo_0)); }
	inline MemberInfo_t * get_MemberInfo_0() const { return ___MemberInfo_0; }
	inline MemberInfo_t ** get_address_of_MemberInfo_0() { return &___MemberInfo_0; }
	inline void set_MemberInfo_0(MemberInfo_t * value)
	{
		___MemberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberInfo_0), value);
	}

	inline static int32_t get_offset_of_AttributeType_1() { return static_cast<int32_t>(offsetof(AttributeQuery_t874673239, ___AttributeType_1)); }
	inline Type_t * get_AttributeType_1() const { return ___AttributeType_1; }
	inline Type_t ** get_address_of_AttributeType_1() { return &___AttributeType_1; }
	inline void set_AttributeType_1(Type_t * value)
	{
		___AttributeType_1 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.Internal.fsPortableReflection/AttributeQuery
struct AttributeQuery_t874673239_marshaled_pinvoke
{
	MemberInfo_t * ___MemberInfo_0;
	Type_t * ___AttributeType_1;
};
// Native definition for COM marshalling of FullSerializer.Internal.fsPortableReflection/AttributeQuery
struct AttributeQuery_t874673239_marshaled_com
{
	MemberInfo_t * ___MemberInfo_0;
	Type_t * ___AttributeType_1;
};
#endif // ATTRIBUTEQUERY_T874673239_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef AOTFAILUREEXCEPTION_T3905637742_H
#define AOTFAILUREEXCEPTION_T3905637742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/AotFailureException
struct  AotFailureException_t3905637742  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOTFAILUREEXCEPTION_T3905637742_H
#ifndef FSFORWARDATTRIBUTE_T3729870757_H
#define FSFORWARDATTRIBUTE_T3729870757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsForwardAttribute
struct  fsForwardAttribute_t3729870757  : public Attribute_t861562559
{
public:
	// System.String FullSerializer.fsForwardAttribute::MemberName
	String_t* ___MemberName_0;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(fsForwardAttribute_t3729870757, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSFORWARDATTRIBUTE_T3729870757_H
#ifndef FSCONVERTER_T1940635020_H
#define FSCONVERTER_T1940635020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsConverter
struct  fsConverter_t1940635020  : public fsBaseConverter_t1567749939
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONVERTER_T1940635020_H
#ifndef FSDIRECTCONVERTER_T3189684931_H
#define FSDIRECTCONVERTER_T3189684931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter
struct  fsDirectConverter_t3189684931  : public fsBaseConverter_t1567749939
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_T3189684931_H
#ifndef MEMBER_T1335065313_H
#define MEMBER_T1335065313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotVersionInfo/Member
struct  Member_t1335065313 
{
public:
	// System.String FullSerializer.fsAotVersionInfo/Member::MemberName
	String_t* ___MemberName_0;
	// System.String FullSerializer.fsAotVersionInfo/Member::JsonName
	String_t* ___JsonName_1;
	// System.String FullSerializer.fsAotVersionInfo/Member::StorageType
	String_t* ___StorageType_2;
	// System.String FullSerializer.fsAotVersionInfo/Member::OverrideConverterType
	String_t* ___OverrideConverterType_3;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(Member_t1335065313, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}

	inline static int32_t get_offset_of_JsonName_1() { return static_cast<int32_t>(offsetof(Member_t1335065313, ___JsonName_1)); }
	inline String_t* get_JsonName_1() const { return ___JsonName_1; }
	inline String_t** get_address_of_JsonName_1() { return &___JsonName_1; }
	inline void set_JsonName_1(String_t* value)
	{
		___JsonName_1 = value;
		Il2CppCodeGenWriteBarrier((&___JsonName_1), value);
	}

	inline static int32_t get_offset_of_StorageType_2() { return static_cast<int32_t>(offsetof(Member_t1335065313, ___StorageType_2)); }
	inline String_t* get_StorageType_2() const { return ___StorageType_2; }
	inline String_t** get_address_of_StorageType_2() { return &___StorageType_2; }
	inline void set_StorageType_2(String_t* value)
	{
		___StorageType_2 = value;
		Il2CppCodeGenWriteBarrier((&___StorageType_2), value);
	}

	inline static int32_t get_offset_of_OverrideConverterType_3() { return static_cast<int32_t>(offsetof(Member_t1335065313, ___OverrideConverterType_3)); }
	inline String_t* get_OverrideConverterType_3() const { return ___OverrideConverterType_3; }
	inline String_t** get_address_of_OverrideConverterType_3() { return &___OverrideConverterType_3; }
	inline void set_OverrideConverterType_3(String_t* value)
	{
		___OverrideConverterType_3 = value;
		Il2CppCodeGenWriteBarrier((&___OverrideConverterType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsAotVersionInfo/Member
struct Member_t1335065313_marshaled_pinvoke
{
	char* ___MemberName_0;
	char* ___JsonName_1;
	char* ___StorageType_2;
	char* ___OverrideConverterType_3;
};
// Native definition for COM marshalling of FullSerializer.fsAotVersionInfo/Member
struct Member_t1335065313_marshaled_com
{
	Il2CppChar* ___MemberName_0;
	Il2CppChar* ___JsonName_1;
	Il2CppChar* ___StorageType_2;
	Il2CppChar* ___OverrideConverterType_3;
};
#endif // MEMBER_T1335065313_H
#ifndef FSMISSINGVERSIONCONSTRUCTOREXCEPTION_T4130735797_H
#define FSMISSINGVERSIONCONSTRUCTOREXCEPTION_T4130735797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMissingVersionConstructorException
struct  fsMissingVersionConstructorException_t4130735797  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMISSINGVERSIONCONSTRUCTOREXCEPTION_T4130735797_H
#ifndef FSDUPLICATEVERSIONNAMEEXCEPTION_T187941636_H
#define FSDUPLICATEVERSIONNAMEEXCEPTION_T187941636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDuplicateVersionNameException
struct  fsDuplicateVersionNameException_t187941636  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDUPLICATEVERSIONNAMEEXCEPTION_T187941636_H
#ifndef FSIGNOREATTRIBUTE_T2855408247_H
#define FSIGNOREATTRIBUTE_T2855408247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsIgnoreAttribute
struct  fsIgnoreAttribute_t2855408247  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSIGNOREATTRIBUTE_T2855408247_H
#ifndef FSSERIALIZATIONCALLBACKPROCESSOR_T3447229557_H
#define FSSERIALIZATIONCALLBACKPROCESSOR_T3447229557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsSerializationCallbackProcessor
struct  fsSerializationCallbackProcessor_t3447229557  : public fsObjectProcessor_t419368844
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSSERIALIZATIONCALLBACKPROCESSOR_T3447229557_H
#ifndef FSSERIALIZATIONCALLBACKRECEIVERPROCESSOR_T3854961681_H
#define FSSERIALIZATIONCALLBACKRECEIVERPROCESSOR_T3854961681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsSerializationCallbackReceiverProcessor
struct  fsSerializationCallbackReceiverProcessor_t3854961681  : public fsObjectProcessor_t419368844
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSSERIALIZATIONCALLBACKRECEIVERPROCESSOR_T3854961681_H
#ifndef FSAOTVERSIONINFO_T2025365886_H
#define FSAOTVERSIONINFO_T2025365886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotVersionInfo
struct  fsAotVersionInfo_t2025365886 
{
public:
	// System.Boolean FullSerializer.fsAotVersionInfo::IsConstructorPublic
	bool ___IsConstructorPublic_0;
	// FullSerializer.fsAotVersionInfo/Member[] FullSerializer.fsAotVersionInfo::Members
	MemberU5BU5D_t44466300* ___Members_1;

public:
	inline static int32_t get_offset_of_IsConstructorPublic_0() { return static_cast<int32_t>(offsetof(fsAotVersionInfo_t2025365886, ___IsConstructorPublic_0)); }
	inline bool get_IsConstructorPublic_0() const { return ___IsConstructorPublic_0; }
	inline bool* get_address_of_IsConstructorPublic_0() { return &___IsConstructorPublic_0; }
	inline void set_IsConstructorPublic_0(bool value)
	{
		___IsConstructorPublic_0 = value;
	}

	inline static int32_t get_offset_of_Members_1() { return static_cast<int32_t>(offsetof(fsAotVersionInfo_t2025365886, ___Members_1)); }
	inline MemberU5BU5D_t44466300* get_Members_1() const { return ___Members_1; }
	inline MemberU5BU5D_t44466300** get_address_of_Members_1() { return &___Members_1; }
	inline void set_Members_1(MemberU5BU5D_t44466300* value)
	{
		___Members_1 = value;
		Il2CppCodeGenWriteBarrier((&___Members_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsAotVersionInfo
struct fsAotVersionInfo_t2025365886_marshaled_pinvoke
{
	int32_t ___IsConstructorPublic_0;
	Member_t1335065313_marshaled_pinvoke* ___Members_1;
};
// Native definition for COM marshalling of FullSerializer.fsAotVersionInfo
struct fsAotVersionInfo_t2025365886_marshaled_com
{
	int32_t ___IsConstructorPublic_0;
	Member_t1335065313_marshaled_com* ___Members_1;
};
#endif // FSAOTVERSIONINFO_T2025365886_H
#ifndef FSFORWARDCONVERTER_T275048757_H
#define FSFORWARDCONVERTER_T275048757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsForwardConverter
struct  fsForwardConverter_t275048757  : public fsConverter_t1940635020
{
public:
	// System.String FullSerializer.Internal.fsForwardConverter::_memberName
	String_t* ____memberName_2;

public:
	inline static int32_t get_offset_of__memberName_2() { return static_cast<int32_t>(offsetof(fsForwardConverter_t275048757, ____memberName_2)); }
	inline String_t* get__memberName_2() const { return ____memberName_2; }
	inline String_t** get_address_of__memberName_2() { return &____memberName_2; }
	inline void set__memberName_2(String_t* value)
	{
		____memberName_2 = value;
		Il2CppCodeGenWriteBarrier((&____memberName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSFORWARDCONVERTER_T275048757_H
#ifndef FSDIRECTCONVERTER_1_T2769511324_H
#define FSDIRECTCONVERTER_1_T2769511324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.AnimationCurve>
struct  fsDirectConverter_1_t2769511324  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T2769511324_H
#ifndef FSENUMCONVERTER_T2820841052_H
#define FSENUMCONVERTER_T2820841052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsEnumConverter
struct  fsEnumConverter_t2820841052  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSENUMCONVERTER_T2820841052_H
#ifndef FSDICTIONARYCONVERTER_T3296500311_H
#define FSDICTIONARYCONVERTER_T3296500311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsDictionaryConverter
struct  fsDictionaryConverter_t3296500311  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDICTIONARYCONVERTER_T3296500311_H
#ifndef FSGUIDCONVERTER_T3436012287_H
#define FSGUIDCONVERTER_T3436012287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsGuidConverter
struct  fsGuidConverter_t3436012287  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSGUIDCONVERTER_T3436012287_H
#ifndef FSIENUMERABLECONVERTER_T409581950_H
#define FSIENUMERABLECONVERTER_T409581950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsIEnumerableConverter
struct  fsIEnumerableConverter_t409581950  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSIENUMERABLECONVERTER_T409581950_H
#ifndef FSKEYVALUEPAIRCONVERTER_T1178744676_H
#define FSKEYVALUEPAIRCONVERTER_T1178744676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsKeyValuePairConverter
struct  fsKeyValuePairConverter_t1178744676  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSKEYVALUEPAIRCONVERTER_T1178744676_H
#ifndef FSNULLABLECONVERTER_T759066596_H
#define FSNULLABLECONVERTER_T759066596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsNullableConverter
struct  fsNullableConverter_t759066596  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSNULLABLECONVERTER_T759066596_H
#ifndef AOTSTATE_T3372302302_H
#define AOTSTATE_T3372302302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotConfiguration/AotState
struct  AotState_t3372302302 
{
public:
	// System.Int32 FullSerializer.fsAotConfiguration/AotState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AotState_t3372302302, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOTSTATE_T3372302302_H
#ifndef FSPRIMITIVECONVERTER_T1335342894_H
#define FSPRIMITIVECONVERTER_T1335342894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPrimitiveConverter
struct  fsPrimitiveConverter_t1335342894  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSPRIMITIVECONVERTER_T1335342894_H
#ifndef FSDIRECTCONVERTER_1_T1989594868_H
#define FSDIRECTCONVERTER_1_T1989594868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Bounds>
struct  fsDirectConverter_1_t1989594868  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T1989594868_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef FSARRAYCONVERTER_T4258383025_H
#define FSARRAYCONVERTER_T4258383025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsArrayConverter
struct  fsArrayConverter_t4258383025  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSARRAYCONVERTER_T4258383025_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef FSDATECONVERTER_T965441267_H
#define FSDATECONVERTER_T965441267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsDateConverter
struct  fsDateConverter_t965441267  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATECONVERTER_T965441267_H
#ifndef FSDIRECTCONVERTER_1_T1092210634_H
#define FSDIRECTCONVERTER_1_T1092210634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.RectOffset>
struct  fsDirectConverter_1_t1092210634  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T1092210634_H
#ifndef FSDIRECTCONVERTER_1_T2083236817_H
#define FSDIRECTCONVERTER_1_T2083236817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Rect>
struct  fsDirectConverter_1_t2083236817  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T2083236817_H
#ifndef FSDIRECTCONVERTER_1_T3216691876_H
#define FSDIRECTCONVERTER_1_T3216691876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.LayerMask>
struct  fsDirectConverter_1_t3216691876  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T3216691876_H
#ifndef FSDIRECTCONVERTER_1_T3929167200_H
#define FSDIRECTCONVERTER_1_T3929167200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Keyframe>
struct  fsDirectConverter_1_t3929167200  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T3929167200_H
#ifndef FSDIRECTCONVERTER_1_T1120721373_H
#define FSDIRECTCONVERTER_1_T1120721373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.GUIStyleState>
struct  fsDirectConverter_1_t1120721373  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T1120721373_H
#ifndef FSDIRECTCONVERTER_1_T3679658469_H
#define FSDIRECTCONVERTER_1_T3679658469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.GUIStyle>
struct  fsDirectConverter_1_t3679658469  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T3679658469_H
#ifndef FSDIRECTCONVERTER_1_T2789856882_H
#define FSDIRECTCONVERTER_1_T2789856882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Gradient>
struct  fsDirectConverter_1_t2789856882  : public fsDirectConverter_t3189684931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T2789856882_H
#ifndef U3CFADEOUTU3EC__ITERATOR2_T2086620696_H
#define U3CFADEOUTU3EC__ITERATOR2_T2086620696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2
struct  U3CFadeOutU3Ec__Iterator2_t2086620696  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::image
	Image_t2670269651 * ___image_0;
	// System.Single GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::<alpha>__0
	float ___U3CalphaU3E__0_1;
	// System.Single GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::<t>__1
	float ___U3CtU3E__1_2;
	// UnityEngine.Color GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::<color>__2
	Color_t2555686324  ___U3CcolorU3E__2_3;
	// System.Single GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::time
	float ___time_4;
	// System.Action GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::onComplete
	Action_t1264377477 * ___onComplete_5;
	// System.Object GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::$disposing
	bool ___U24disposing_7;
	// System.Int32 GameVanilla.Core.BaseScene/<FadeOut>c__Iterator2::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___image_0)); }
	inline Image_t2670269651 * get_image_0() const { return ___image_0; }
	inline Image_t2670269651 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(Image_t2670269651 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___U3CalphaU3E__0_1)); }
	inline float get_U3CalphaU3E__0_1() const { return ___U3CalphaU3E__0_1; }
	inline float* get_address_of_U3CalphaU3E__0_1() { return &___U3CalphaU3E__0_1; }
	inline void set_U3CalphaU3E__0_1(float value)
	{
		___U3CalphaU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___U3CtU3E__1_2)); }
	inline float get_U3CtU3E__1_2() const { return ___U3CtU3E__1_2; }
	inline float* get_address_of_U3CtU3E__1_2() { return &___U3CtU3E__1_2; }
	inline void set_U3CtU3E__1_2(float value)
	{
		___U3CtU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CcolorU3E__2_3() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___U3CcolorU3E__2_3)); }
	inline Color_t2555686324  get_U3CcolorU3E__2_3() const { return ___U3CcolorU3E__2_3; }
	inline Color_t2555686324 * get_address_of_U3CcolorU3E__2_3() { return &___U3CcolorU3E__2_3; }
	inline void set_U3CcolorU3E__2_3(Color_t2555686324  value)
	{
		___U3CcolorU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_onComplete_5() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___onComplete_5)); }
	inline Action_t1264377477 * get_onComplete_5() const { return ___onComplete_5; }
	inline Action_t1264377477 ** get_address_of_onComplete_5() { return &___onComplete_5; }
	inline void set_onComplete_5(Action_t1264377477 * value)
	{
		___onComplete_5 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator2_t2086620696, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEOUTU3EC__ITERATOR2_T2086620696_H
#ifndef FSDATATYPE_T2663285942_H
#define FSDATATYPE_T2663285942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDataType
struct  fsDataType_t2663285942 
{
public:
	// System.Int32 FullSerializer.fsDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(fsDataType_t2663285942, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATATYPE_T2663285942_H
#ifndef UNITYEVENT_CONVERTER_T3778070767_H
#define UNITYEVENT_CONVERTER_T3778070767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.Converters.UnityEvent_Converter
struct  UnityEvent_Converter_t3778070767  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_CONVERTER_T3778070767_H
#ifndef FSTYPECONVERTER_T3131242195_H
#define FSTYPECONVERTER_T3131242195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsTypeConverter
struct  fsTypeConverter_t3131242195  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSTYPECONVERTER_T3131242195_H
#ifndef FSMETATYPE_T439135866_H
#define FSMETATYPE_T439135866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType
struct  fsMetaType_t439135866  : public RuntimeObject
{
public:
	// System.Type FullSerializer.fsMetaType::ReflectedType
	Type_t * ___ReflectedType_1;
	// FullSerializer.Internal.fsMetaProperty[] FullSerializer.fsMetaType::<Properties>k__BackingField
	fsMetaPropertyU5BU5D_t1681951782* ___U3CPropertiesU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> FullSerializer.fsMetaType::_hasDefaultConstructorCache
	Nullable_1_t1819850047  ____hasDefaultConstructorCache_3;
	// System.Nullable`1<System.Boolean> FullSerializer.fsMetaType::_isDefaultConstructorPublicCache
	Nullable_1_t1819850047  ____isDefaultConstructorPublicCache_4;

public:
	inline static int32_t get_offset_of_ReflectedType_1() { return static_cast<int32_t>(offsetof(fsMetaType_t439135866, ___ReflectedType_1)); }
	inline Type_t * get_ReflectedType_1() const { return ___ReflectedType_1; }
	inline Type_t ** get_address_of_ReflectedType_1() { return &___ReflectedType_1; }
	inline void set_ReflectedType_1(Type_t * value)
	{
		___ReflectedType_1 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectedType_1), value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(fsMetaType_t439135866, ___U3CPropertiesU3Ek__BackingField_2)); }
	inline fsMetaPropertyU5BU5D_t1681951782* get_U3CPropertiesU3Ek__BackingField_2() const { return ___U3CPropertiesU3Ek__BackingField_2; }
	inline fsMetaPropertyU5BU5D_t1681951782** get_address_of_U3CPropertiesU3Ek__BackingField_2() { return &___U3CPropertiesU3Ek__BackingField_2; }
	inline void set_U3CPropertiesU3Ek__BackingField_2(fsMetaPropertyU5BU5D_t1681951782* value)
	{
		___U3CPropertiesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__hasDefaultConstructorCache_3() { return static_cast<int32_t>(offsetof(fsMetaType_t439135866, ____hasDefaultConstructorCache_3)); }
	inline Nullable_1_t1819850047  get__hasDefaultConstructorCache_3() const { return ____hasDefaultConstructorCache_3; }
	inline Nullable_1_t1819850047 * get_address_of__hasDefaultConstructorCache_3() { return &____hasDefaultConstructorCache_3; }
	inline void set__hasDefaultConstructorCache_3(Nullable_1_t1819850047  value)
	{
		____hasDefaultConstructorCache_3 = value;
	}

	inline static int32_t get_offset_of__isDefaultConstructorPublicCache_4() { return static_cast<int32_t>(offsetof(fsMetaType_t439135866, ____isDefaultConstructorPublicCache_4)); }
	inline Nullable_1_t1819850047  get__isDefaultConstructorPublicCache_4() const { return ____isDefaultConstructorPublicCache_4; }
	inline Nullable_1_t1819850047 * get_address_of__isDefaultConstructorPublicCache_4() { return &____isDefaultConstructorPublicCache_4; }
	inline void set__isDefaultConstructorPublicCache_4(Nullable_1_t1819850047  value)
	{
		____isDefaultConstructorPublicCache_4 = value;
	}
};

struct fsMetaType_t439135866_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<FullSerializer.fsConfig,System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsMetaType>> FullSerializer.fsMetaType::_configMetaTypes
	Dictionary_2_t2294030055 * ____configMetaTypes_0;

public:
	inline static int32_t get_offset_of__configMetaTypes_0() { return static_cast<int32_t>(offsetof(fsMetaType_t439135866_StaticFields, ____configMetaTypes_0)); }
	inline Dictionary_2_t2294030055 * get__configMetaTypes_0() const { return ____configMetaTypes_0; }
	inline Dictionary_2_t2294030055 ** get_address_of__configMetaTypes_0() { return &____configMetaTypes_0; }
	inline void set__configMetaTypes_0(Dictionary_2_t2294030055 * value)
	{
		____configMetaTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____configMetaTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMETATYPE_T439135866_H
#ifndef FSWEAKREFERENCECONVERTER_T2853701403_H
#define FSWEAKREFERENCECONVERTER_T2853701403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsWeakReferenceConverter
struct  fsWeakReferenceConverter_t2853701403  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSWEAKREFERENCECONVERTER_T2853701403_H
#ifndef BUTTONCLICKEDEVENT_T849812677_H
#define BUTTONCLICKEDEVENT_T849812677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.AnimatedButton/ButtonClickedEvent
struct  ButtonClickedEvent_t849812677  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCLICKEDEVENT_T849812677_H
#ifndef FSREFLECTEDCONVERTER_T1873634900_H
#define FSREFLECTEDCONVERTER_T1873634900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsReflectedConverter
struct  fsReflectedConverter_t1873634900  : public fsConverter_t1940635020
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSREFLECTEDCONVERTER_T1873634900_H
#ifndef FSMEMBERSERIALIZATION_T1535378461_H
#define FSMEMBERSERIALIZATION_T1535378461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMemberSerialization
struct  fsMemberSerialization_t1535378461 
{
public:
	// System.Int32 FullSerializer.fsMemberSerialization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(fsMemberSerialization_t1535378461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEMBERSERIALIZATION_T1535378461_H
#ifndef U3CFADEINU3EC__ITERATOR1_T4170895653_H
#define U3CFADEINU3EC__ITERATOR1_T4170895653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1
struct  U3CFadeInU3Ec__Iterator1_t4170895653  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::image
	Image_t2670269651 * ___image_0;
	// System.Single GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_1;
	// System.Single GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::<t>__1
	float ___U3CtU3E__1_2;
	// UnityEngine.Color GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::<color>__2
	Color_t2555686324  ___U3CcolorU3E__2_3;
	// System.Single GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::time
	float ___time_4;
	// System.Object GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 GameVanilla.Core.BaseScene/<FadeIn>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___image_0)); }
	inline Image_t2670269651 * get_image_0() const { return ___image_0; }
	inline Image_t2670269651 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(Image_t2670269651 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___U3CalphaU3E__0_1)); }
	inline float get_U3CalphaU3E__0_1() const { return ___U3CalphaU3E__0_1; }
	inline float* get_address_of_U3CalphaU3E__0_1() { return &___U3CalphaU3E__0_1; }
	inline void set_U3CalphaU3E__0_1(float value)
	{
		___U3CalphaU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___U3CtU3E__1_2)); }
	inline float get_U3CtU3E__1_2() const { return ___U3CtU3E__1_2; }
	inline float* get_address_of_U3CtU3E__1_2() { return &___U3CtU3E__1_2; }
	inline void set_U3CtU3E__1_2(float value)
	{
		___U3CtU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CcolorU3E__2_3() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___U3CcolorU3E__2_3)); }
	inline Color_t2555686324  get_U3CcolorU3E__2_3() const { return ___U3CcolorU3E__2_3; }
	inline Color_t2555686324 * get_address_of_U3CcolorU3E__2_3() { return &___U3CcolorU3E__2_3; }
	inline void set_U3CcolorU3E__2_3(Color_t2555686324  value)
	{
		___U3CcolorU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__Iterator1_t4170895653, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEINU3EC__ITERATOR1_T4170895653_H
#ifndef FSOBJECTATTRIBUTE_T1137703855_H
#define FSOBJECTATTRIBUTE_T1137703855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsObjectAttribute
struct  fsObjectAttribute_t1137703855  : public Attribute_t861562559
{
public:
	// System.Type[] FullSerializer.fsObjectAttribute::PreviousModels
	TypeU5BU5D_t3940880105* ___PreviousModels_0;
	// System.String FullSerializer.fsObjectAttribute::VersionString
	String_t* ___VersionString_1;
	// FullSerializer.fsMemberSerialization FullSerializer.fsObjectAttribute::MemberSerialization
	int32_t ___MemberSerialization_2;
	// System.Type FullSerializer.fsObjectAttribute::Converter
	Type_t * ___Converter_3;
	// System.Type FullSerializer.fsObjectAttribute::Processor
	Type_t * ___Processor_4;

public:
	inline static int32_t get_offset_of_PreviousModels_0() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t1137703855, ___PreviousModels_0)); }
	inline TypeU5BU5D_t3940880105* get_PreviousModels_0() const { return ___PreviousModels_0; }
	inline TypeU5BU5D_t3940880105** get_address_of_PreviousModels_0() { return &___PreviousModels_0; }
	inline void set_PreviousModels_0(TypeU5BU5D_t3940880105* value)
	{
		___PreviousModels_0 = value;
		Il2CppCodeGenWriteBarrier((&___PreviousModels_0), value);
	}

	inline static int32_t get_offset_of_VersionString_1() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t1137703855, ___VersionString_1)); }
	inline String_t* get_VersionString_1() const { return ___VersionString_1; }
	inline String_t** get_address_of_VersionString_1() { return &___VersionString_1; }
	inline void set_VersionString_1(String_t* value)
	{
		___VersionString_1 = value;
		Il2CppCodeGenWriteBarrier((&___VersionString_1), value);
	}

	inline static int32_t get_offset_of_MemberSerialization_2() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t1137703855, ___MemberSerialization_2)); }
	inline int32_t get_MemberSerialization_2() const { return ___MemberSerialization_2; }
	inline int32_t* get_address_of_MemberSerialization_2() { return &___MemberSerialization_2; }
	inline void set_MemberSerialization_2(int32_t value)
	{
		___MemberSerialization_2 = value;
	}

	inline static int32_t get_offset_of_Converter_3() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t1137703855, ___Converter_3)); }
	inline Type_t * get_Converter_3() const { return ___Converter_3; }
	inline Type_t ** get_address_of_Converter_3() { return &___Converter_3; }
	inline void set_Converter_3(Type_t * value)
	{
		___Converter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Converter_3), value);
	}

	inline static int32_t get_offset_of_Processor_4() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t1137703855, ___Processor_4)); }
	inline Type_t * get_Processor_4() const { return ___Processor_4; }
	inline Type_t ** get_address_of_Processor_4() { return &___Processor_4; }
	inline void set_Processor_4(Type_t * value)
	{
		___Processor_4 = value;
		Il2CppCodeGenWriteBarrier((&___Processor_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSOBJECTATTRIBUTE_T1137703855_H
#ifndef KEYFRAME_DIRECTCONVERTER_T1448983566_H
#define KEYFRAME_DIRECTCONVERTER_T1448983566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Keyframe_DirectConverter
struct  Keyframe_DirectConverter_t1448983566  : public fsDirectConverter_1_t3929167200
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_DIRECTCONVERTER_T1448983566_H
#ifndef FSPORTABLEREFLECTION_T672844747_H
#define FSPORTABLEREFLECTION_T672844747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection
struct  fsPortableReflection_t672844747  : public RuntimeObject
{
public:

public:
};

struct fsPortableReflection_t672844747_StaticFields
{
public:
	// System.Type[] FullSerializer.Internal.fsPortableReflection::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_0;
	// System.Collections.Generic.IDictionary`2<FullSerializer.Internal.fsPortableReflection/AttributeQuery,System.Attribute> FullSerializer.Internal.fsPortableReflection::_cachedAttributeQueries
	RuntimeObject* ____cachedAttributeQueries_1;
	// System.Reflection.BindingFlags FullSerializer.Internal.fsPortableReflection::DeclaredFlags
	int32_t ___DeclaredFlags_2;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(fsPortableReflection_t672844747_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}

	inline static int32_t get_offset_of__cachedAttributeQueries_1() { return static_cast<int32_t>(offsetof(fsPortableReflection_t672844747_StaticFields, ____cachedAttributeQueries_1)); }
	inline RuntimeObject* get__cachedAttributeQueries_1() const { return ____cachedAttributeQueries_1; }
	inline RuntimeObject** get_address_of__cachedAttributeQueries_1() { return &____cachedAttributeQueries_1; }
	inline void set__cachedAttributeQueries_1(RuntimeObject* value)
	{
		____cachedAttributeQueries_1 = value;
		Il2CppCodeGenWriteBarrier((&____cachedAttributeQueries_1), value);
	}

	inline static int32_t get_offset_of_DeclaredFlags_2() { return static_cast<int32_t>(offsetof(fsPortableReflection_t672844747_StaticFields, ___DeclaredFlags_2)); }
	inline int32_t get_DeclaredFlags_2() const { return ___DeclaredFlags_2; }
	inline int32_t* get_address_of_DeclaredFlags_2() { return &___DeclaredFlags_2; }
	inline void set_DeclaredFlags_2(int32_t value)
	{
		___DeclaredFlags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSPORTABLEREFLECTION_T672844747_H
#ifndef GUISTYLE_DIRECTCONVERTER_T1060365653_H
#define GUISTYLE_DIRECTCONVERTER_T1060365653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.GUIStyle_DirectConverter
struct  GUIStyle_DirectConverter_t1060365653  : public fsDirectConverter_1_t3679658469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISTYLE_DIRECTCONVERTER_T1060365653_H
#ifndef GUISTYLESTATE_DIRECTCONVERTER_T3967104754_H
#define GUISTYLESTATE_DIRECTCONVERTER_T3967104754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.GUIStyleState_DirectConverter
struct  GUIStyleState_DirectConverter_t3967104754  : public fsDirectConverter_1_t1120721373
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISTYLESTATE_DIRECTCONVERTER_T3967104754_H
#ifndef FSCONFIG_T14416447_H
#define FSCONFIG_T14416447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsConfig
struct  fsConfig_t14416447  : public RuntimeObject
{
public:
	// System.Type[] FullSerializer.fsConfig::SerializeAttributes
	TypeU5BU5D_t3940880105* ___SerializeAttributes_0;
	// System.Type[] FullSerializer.fsConfig::IgnoreSerializeAttributes
	TypeU5BU5D_t3940880105* ___IgnoreSerializeAttributes_1;
	// System.Type[] FullSerializer.fsConfig::IgnoreSerializeTypeAttributes
	TypeU5BU5D_t3940880105* ___IgnoreSerializeTypeAttributes_2;
	// FullSerializer.fsMemberSerialization FullSerializer.fsConfig::DefaultMemberSerialization
	int32_t ___DefaultMemberSerialization_3;
	// System.Func`3<System.String,System.Reflection.MemberInfo,System.String> FullSerializer.fsConfig::GetJsonNameFromMemberName
	Func_3_t2404287706 * ___GetJsonNameFromMemberName_4;
	// System.Boolean FullSerializer.fsConfig::EnablePropertySerialization
	bool ___EnablePropertySerialization_5;
	// System.Boolean FullSerializer.fsConfig::SerializeNonAutoProperties
	bool ___SerializeNonAutoProperties_6;
	// System.Boolean FullSerializer.fsConfig::SerializeNonPublicSetProperties
	bool ___SerializeNonPublicSetProperties_7;
	// System.String FullSerializer.fsConfig::CustomDateTimeFormatString
	String_t* ___CustomDateTimeFormatString_8;
	// System.Boolean FullSerializer.fsConfig::Serialize64BitIntegerAsString
	bool ___Serialize64BitIntegerAsString_9;
	// System.Boolean FullSerializer.fsConfig::SerializeEnumsAsInteger
	bool ___SerializeEnumsAsInteger_10;

public:
	inline static int32_t get_offset_of_SerializeAttributes_0() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___SerializeAttributes_0)); }
	inline TypeU5BU5D_t3940880105* get_SerializeAttributes_0() const { return ___SerializeAttributes_0; }
	inline TypeU5BU5D_t3940880105** get_address_of_SerializeAttributes_0() { return &___SerializeAttributes_0; }
	inline void set_SerializeAttributes_0(TypeU5BU5D_t3940880105* value)
	{
		___SerializeAttributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeAttributes_0), value);
	}

	inline static int32_t get_offset_of_IgnoreSerializeAttributes_1() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___IgnoreSerializeAttributes_1)); }
	inline TypeU5BU5D_t3940880105* get_IgnoreSerializeAttributes_1() const { return ___IgnoreSerializeAttributes_1; }
	inline TypeU5BU5D_t3940880105** get_address_of_IgnoreSerializeAttributes_1() { return &___IgnoreSerializeAttributes_1; }
	inline void set_IgnoreSerializeAttributes_1(TypeU5BU5D_t3940880105* value)
	{
		___IgnoreSerializeAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&___IgnoreSerializeAttributes_1), value);
	}

	inline static int32_t get_offset_of_IgnoreSerializeTypeAttributes_2() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___IgnoreSerializeTypeAttributes_2)); }
	inline TypeU5BU5D_t3940880105* get_IgnoreSerializeTypeAttributes_2() const { return ___IgnoreSerializeTypeAttributes_2; }
	inline TypeU5BU5D_t3940880105** get_address_of_IgnoreSerializeTypeAttributes_2() { return &___IgnoreSerializeTypeAttributes_2; }
	inline void set_IgnoreSerializeTypeAttributes_2(TypeU5BU5D_t3940880105* value)
	{
		___IgnoreSerializeTypeAttributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___IgnoreSerializeTypeAttributes_2), value);
	}

	inline static int32_t get_offset_of_DefaultMemberSerialization_3() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___DefaultMemberSerialization_3)); }
	inline int32_t get_DefaultMemberSerialization_3() const { return ___DefaultMemberSerialization_3; }
	inline int32_t* get_address_of_DefaultMemberSerialization_3() { return &___DefaultMemberSerialization_3; }
	inline void set_DefaultMemberSerialization_3(int32_t value)
	{
		___DefaultMemberSerialization_3 = value;
	}

	inline static int32_t get_offset_of_GetJsonNameFromMemberName_4() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___GetJsonNameFromMemberName_4)); }
	inline Func_3_t2404287706 * get_GetJsonNameFromMemberName_4() const { return ___GetJsonNameFromMemberName_4; }
	inline Func_3_t2404287706 ** get_address_of_GetJsonNameFromMemberName_4() { return &___GetJsonNameFromMemberName_4; }
	inline void set_GetJsonNameFromMemberName_4(Func_3_t2404287706 * value)
	{
		___GetJsonNameFromMemberName_4 = value;
		Il2CppCodeGenWriteBarrier((&___GetJsonNameFromMemberName_4), value);
	}

	inline static int32_t get_offset_of_EnablePropertySerialization_5() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___EnablePropertySerialization_5)); }
	inline bool get_EnablePropertySerialization_5() const { return ___EnablePropertySerialization_5; }
	inline bool* get_address_of_EnablePropertySerialization_5() { return &___EnablePropertySerialization_5; }
	inline void set_EnablePropertySerialization_5(bool value)
	{
		___EnablePropertySerialization_5 = value;
	}

	inline static int32_t get_offset_of_SerializeNonAutoProperties_6() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___SerializeNonAutoProperties_6)); }
	inline bool get_SerializeNonAutoProperties_6() const { return ___SerializeNonAutoProperties_6; }
	inline bool* get_address_of_SerializeNonAutoProperties_6() { return &___SerializeNonAutoProperties_6; }
	inline void set_SerializeNonAutoProperties_6(bool value)
	{
		___SerializeNonAutoProperties_6 = value;
	}

	inline static int32_t get_offset_of_SerializeNonPublicSetProperties_7() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___SerializeNonPublicSetProperties_7)); }
	inline bool get_SerializeNonPublicSetProperties_7() const { return ___SerializeNonPublicSetProperties_7; }
	inline bool* get_address_of_SerializeNonPublicSetProperties_7() { return &___SerializeNonPublicSetProperties_7; }
	inline void set_SerializeNonPublicSetProperties_7(bool value)
	{
		___SerializeNonPublicSetProperties_7 = value;
	}

	inline static int32_t get_offset_of_CustomDateTimeFormatString_8() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___CustomDateTimeFormatString_8)); }
	inline String_t* get_CustomDateTimeFormatString_8() const { return ___CustomDateTimeFormatString_8; }
	inline String_t** get_address_of_CustomDateTimeFormatString_8() { return &___CustomDateTimeFormatString_8; }
	inline void set_CustomDateTimeFormatString_8(String_t* value)
	{
		___CustomDateTimeFormatString_8 = value;
		Il2CppCodeGenWriteBarrier((&___CustomDateTimeFormatString_8), value);
	}

	inline static int32_t get_offset_of_Serialize64BitIntegerAsString_9() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___Serialize64BitIntegerAsString_9)); }
	inline bool get_Serialize64BitIntegerAsString_9() const { return ___Serialize64BitIntegerAsString_9; }
	inline bool* get_address_of_Serialize64BitIntegerAsString_9() { return &___Serialize64BitIntegerAsString_9; }
	inline void set_Serialize64BitIntegerAsString_9(bool value)
	{
		___Serialize64BitIntegerAsString_9 = value;
	}

	inline static int32_t get_offset_of_SerializeEnumsAsInteger_10() { return static_cast<int32_t>(offsetof(fsConfig_t14416447, ___SerializeEnumsAsInteger_10)); }
	inline bool get_SerializeEnumsAsInteger_10() const { return ___SerializeEnumsAsInteger_10; }
	inline bool* get_address_of_SerializeEnumsAsInteger_10() { return &___SerializeEnumsAsInteger_10; }
	inline void set_SerializeEnumsAsInteger_10(bool value)
	{
		___SerializeEnumsAsInteger_10 = value;
	}
};

struct fsConfig_t14416447_StaticFields
{
public:
	// System.Func`3<System.String,System.Reflection.MemberInfo,System.String> FullSerializer.fsConfig::<>f__am$cache0
	Func_3_t2404287706 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(fsConfig_t14416447_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_3_t2404287706 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_3_t2404287706 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_3_t2404287706 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONFIG_T14416447_H
#ifndef ENTRY_T3254413976_H
#define ENTRY_T3254413976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotConfiguration/Entry
struct  Entry_t3254413976 
{
public:
	// FullSerializer.fsAotConfiguration/AotState FullSerializer.fsAotConfiguration/Entry::State
	int32_t ___State_0;
	// System.String FullSerializer.fsAotConfiguration/Entry::FullTypeName
	String_t* ___FullTypeName_1;

public:
	inline static int32_t get_offset_of_State_0() { return static_cast<int32_t>(offsetof(Entry_t3254413976, ___State_0)); }
	inline int32_t get_State_0() const { return ___State_0; }
	inline int32_t* get_address_of_State_0() { return &___State_0; }
	inline void set_State_0(int32_t value)
	{
		___State_0 = value;
	}

	inline static int32_t get_offset_of_FullTypeName_1() { return static_cast<int32_t>(offsetof(Entry_t3254413976, ___FullTypeName_1)); }
	inline String_t* get_FullTypeName_1() const { return ___FullTypeName_1; }
	inline String_t** get_address_of_FullTypeName_1() { return &___FullTypeName_1; }
	inline void set_FullTypeName_1(String_t* value)
	{
		___FullTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FullTypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsAotConfiguration/Entry
struct Entry_t3254413976_marshaled_pinvoke
{
	int32_t ___State_0;
	char* ___FullTypeName_1;
};
// Native definition for COM marshalling of FullSerializer.fsAotConfiguration/Entry
struct Entry_t3254413976_marshaled_com
{
	int32_t ___State_0;
	Il2CppChar* ___FullTypeName_1;
};
#endif // ENTRY_T3254413976_H
#ifndef RECTOFFSET_DIRECTCONVERTER_T483038648_H
#define RECTOFFSET_DIRECTCONVERTER_T483038648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.RectOffset_DirectConverter
struct  RectOffset_DirectConverter_t483038648  : public fsDirectConverter_1_t1092210634
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTOFFSET_DIRECTCONVERTER_T483038648_H
#ifndef RECT_DIRECTCONVERTER_T1861700375_H
#define RECT_DIRECTCONVERTER_T1861700375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Rect_DirectConverter
struct  Rect_DirectConverter_t1861700375  : public fsDirectConverter_1_t2083236817
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_DIRECTCONVERTER_T1861700375_H
#ifndef LAYERMASK_DIRECTCONVERTER_T4211565801_H
#define LAYERMASK_DIRECTCONVERTER_T4211565801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.LayerMask_DirectConverter
struct  LayerMask_DirectConverter_t4211565801  : public fsDirectConverter_1_t3216691876
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_DIRECTCONVERTER_T4211565801_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GRADIENT_DIRECTCONVERTER_T2745360243_H
#define GRADIENT_DIRECTCONVERTER_T2745360243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Gradient_DirectConverter
struct  Gradient_DirectConverter_t2745360243  : public fsDirectConverter_1_t2789856882
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT_DIRECTCONVERTER_T2745360243_H
#ifndef BOUNDS_DIRECTCONVERTER_T3871173093_H
#define BOUNDS_DIRECTCONVERTER_T3871173093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Bounds_DirectConverter
struct  Bounds_DirectConverter_t3871173093  : public fsDirectConverter_1_t1989594868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_DIRECTCONVERTER_T3871173093_H
#ifndef ANIMATIONCURVE_DIRECTCONVERTER_T2921849045_H
#define ANIMATIONCURVE_DIRECTCONVERTER_T2921849045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.AnimationCurve_DirectConverter
struct  AnimationCurve_DirectConverter_t2921849045  : public fsDirectConverter_1_t2769511324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVE_DIRECTCONVERTER_T2921849045_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PLAYSOUND_T1822700253_H
#define PLAYSOUND_T1822700253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.PlaySound
struct  PlaySound_t1822700253  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYSOUND_T1822700253_H
#ifndef OBJECTPOOL_T858432606_H
#define OBJECTPOOL_T858432606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.ObjectPool
struct  ObjectPool_t858432606  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GameVanilla.Core.ObjectPool::prefab
	GameObject_t1113636619 * ___prefab_2;
	// System.Int32 GameVanilla.Core.ObjectPool::initialSize
	int32_t ___initialSize_3;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.ObjectPool::instances
	Stack_1_t1957026074 * ___instances_4;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(ObjectPool_t858432606, ___prefab_2)); }
	inline GameObject_t1113636619 * get_prefab_2() const { return ___prefab_2; }
	inline GameObject_t1113636619 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObject_t1113636619 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_2), value);
	}

	inline static int32_t get_offset_of_initialSize_3() { return static_cast<int32_t>(offsetof(ObjectPool_t858432606, ___initialSize_3)); }
	inline int32_t get_initialSize_3() const { return ___initialSize_3; }
	inline int32_t* get_address_of_initialSize_3() { return &___initialSize_3; }
	inline void set_initialSize_3(int32_t value)
	{
		___initialSize_3 = value;
	}

	inline static int32_t get_offset_of_instances_4() { return static_cast<int32_t>(offsetof(ObjectPool_t858432606, ___instances_4)); }
	inline Stack_1_t1957026074 * get_instances_4() const { return ___instances_4; }
	inline Stack_1_t1957026074 ** get_address_of_instances_4() { return &___instances_4; }
	inline void set_instances_4(Stack_1_t1957026074 * value)
	{
		___instances_4 = value;
		Il2CppCodeGenWriteBarrier((&___instances_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_T858432606_H
#ifndef POPUP_T1063720813_H
#define POPUP_T1063720813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.Popup
struct  Popup_t1063720813  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Core.BaseScene GameVanilla.Core.Popup::parentScene
	BaseScene_t2918414559 * ___parentScene_2;
	// UnityEngine.Events.UnityEvent GameVanilla.Core.Popup::onOpen
	UnityEvent_t2581268647 * ___onOpen_3;
	// UnityEngine.Events.UnityEvent GameVanilla.Core.Popup::onClose
	UnityEvent_t2581268647 * ___onClose_4;
	// UnityEngine.Animator GameVanilla.Core.Popup::animator
	Animator_t434523843 * ___animator_5;

public:
	inline static int32_t get_offset_of_parentScene_2() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___parentScene_2)); }
	inline BaseScene_t2918414559 * get_parentScene_2() const { return ___parentScene_2; }
	inline BaseScene_t2918414559 ** get_address_of_parentScene_2() { return &___parentScene_2; }
	inline void set_parentScene_2(BaseScene_t2918414559 * value)
	{
		___parentScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentScene_2), value);
	}

	inline static int32_t get_offset_of_onOpen_3() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___onOpen_3)); }
	inline UnityEvent_t2581268647 * get_onOpen_3() const { return ___onOpen_3; }
	inline UnityEvent_t2581268647 ** get_address_of_onOpen_3() { return &___onOpen_3; }
	inline void set_onOpen_3(UnityEvent_t2581268647 * value)
	{
		___onOpen_3 = value;
		Il2CppCodeGenWriteBarrier((&___onOpen_3), value);
	}

	inline static int32_t get_offset_of_onClose_4() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___onClose_4)); }
	inline UnityEvent_t2581268647 * get_onClose_4() const { return ___onClose_4; }
	inline UnityEvent_t2581268647 ** get_address_of_onClose_4() { return &___onClose_4; }
	inline void set_onClose_4(UnityEvent_t2581268647 * value)
	{
		___onClose_4 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_4), value);
	}

	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUP_T1063720813_H
#ifndef BASESCENE_T2918414559_H
#define BASESCENE_T2918414559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene
struct  BaseScene_t2918414559  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas GameVanilla.Core.BaseScene::canvas
	Canvas_t3310196443 * ___canvas_2;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPopups
	Stack_1_t1957026074 * ___currentPopups_3;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPanels
	Stack_1_t1957026074 * ___currentPanels_4;

public:
	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___canvas_2)); }
	inline Canvas_t3310196443 * get_canvas_2() const { return ___canvas_2; }
	inline Canvas_t3310196443 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(Canvas_t3310196443 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}

	inline static int32_t get_offset_of_currentPopups_3() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPopups_3)); }
	inline Stack_1_t1957026074 * get_currentPopups_3() const { return ___currentPopups_3; }
	inline Stack_1_t1957026074 ** get_address_of_currentPopups_3() { return &___currentPopups_3; }
	inline void set_currentPopups_3(Stack_1_t1957026074 * value)
	{
		___currentPopups_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentPopups_3), value);
	}

	inline static int32_t get_offset_of_currentPanels_4() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPanels_4)); }
	inline Stack_1_t1957026074 * get_currentPanels_4() const { return ___currentPanels_4; }
	inline Stack_1_t1957026074 ** get_address_of_currentPanels_4() { return &___currentPanels_4; }
	inline void set_currentPanels_4(Stack_1_t1957026074 * value)
	{
		___currentPanels_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanels_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCENE_T2918414559_H
#ifndef BACKGROUNDMUSIC_T4234539114_H
#define BACKGROUNDMUSIC_T4234539114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BackgroundMusic
struct  BackgroundMusic_t4234539114  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource GameVanilla.Core.BackgroundMusic::audioSource
	AudioSource_t3935305588 * ___audioSource_3;

public:
	inline static int32_t get_offset_of_audioSource_3() { return static_cast<int32_t>(offsetof(BackgroundMusic_t4234539114, ___audioSource_3)); }
	inline AudioSource_t3935305588 * get_audioSource_3() const { return ___audioSource_3; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_3() { return &___audioSource_3; }
	inline void set_audioSource_3(AudioSource_t3935305588 * value)
	{
		___audioSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_3), value);
	}
};

struct BackgroundMusic_t4234539114_StaticFields
{
public:
	// GameVanilla.Core.BackgroundMusic GameVanilla.Core.BackgroundMusic::instance
	BackgroundMusic_t4234539114 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(BackgroundMusic_t4234539114_StaticFields, ___instance_2)); }
	inline BackgroundMusic_t4234539114 * get_instance_2() const { return ___instance_2; }
	inline BackgroundMusic_t4234539114 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(BackgroundMusic_t4234539114 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDMUSIC_T4234539114_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TRANSITION_T3232023259_H
#define TRANSITION_T3232023259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.Transition
struct  Transition_t3232023259  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GameVanilla.Core.Transition::m_overlay
	GameObject_t1113636619 * ___m_overlay_3;

public:
	inline static int32_t get_offset_of_m_overlay_3() { return static_cast<int32_t>(offsetof(Transition_t3232023259, ___m_overlay_3)); }
	inline GameObject_t1113636619 * get_m_overlay_3() const { return ___m_overlay_3; }
	inline GameObject_t1113636619 ** get_address_of_m_overlay_3() { return &___m_overlay_3; }
	inline void set_m_overlay_3(GameObject_t1113636619 * value)
	{
		___m_overlay_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_overlay_3), value);
	}
};

struct Transition_t3232023259_StaticFields
{
public:
	// UnityEngine.GameObject GameVanilla.Core.Transition::canvasObject
	GameObject_t1113636619 * ___canvasObject_2;

public:
	inline static int32_t get_offset_of_canvasObject_2() { return static_cast<int32_t>(offsetof(Transition_t3232023259_StaticFields, ___canvasObject_2)); }
	inline GameObject_t1113636619 * get_canvasObject_2() const { return ___canvasObject_2; }
	inline GameObject_t1113636619 ** get_address_of_canvasObject_2() { return &___canvasObject_2; }
	inline void set_canvasObject_2(GameObject_t1113636619 * value)
	{
		___canvasObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvasObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T3232023259_H
#ifndef SPRITESWAPPER_T2892354464_H
#define SPRITESWAPPER_T2892354464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.SpriteSwapper
struct  SpriteSwapper_t2892354464  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite GameVanilla.Core.SpriteSwapper::enabledSprite
	Sprite_t280657092 * ___enabledSprite_2;
	// UnityEngine.Sprite GameVanilla.Core.SpriteSwapper::disabledSprite
	Sprite_t280657092 * ___disabledSprite_3;
	// UnityEngine.UI.Image GameVanilla.Core.SpriteSwapper::image
	Image_t2670269651 * ___image_4;

public:
	inline static int32_t get_offset_of_enabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteSwapper_t2892354464, ___enabledSprite_2)); }
	inline Sprite_t280657092 * get_enabledSprite_2() const { return ___enabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_enabledSprite_2() { return &___enabledSprite_2; }
	inline void set_enabledSprite_2(Sprite_t280657092 * value)
	{
		___enabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___enabledSprite_2), value);
	}

	inline static int32_t get_offset_of_disabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteSwapper_t2892354464, ___disabledSprite_3)); }
	inline Sprite_t280657092 * get_disabledSprite_3() const { return ___disabledSprite_3; }
	inline Sprite_t280657092 ** get_address_of_disabledSprite_3() { return &___disabledSprite_3; }
	inline void set_disabledSprite_3(Sprite_t280657092 * value)
	{
		___disabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___disabledSprite_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SpriteSwapper_t2892354464, ___image_4)); }
	inline Image_t2670269651 * get_image_4() const { return ___image_4; }
	inline Image_t2670269651 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Image_t2670269651 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESWAPPER_T2892354464_H
#ifndef SOUNDMANAGER_T3887122299_H
#define SOUNDMANAGER_T3887122299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.SoundManager
struct  SoundManager_t3887122299  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> GameVanilla.Core.SoundManager::sounds
	List_1_t857997111 * ___sounds_2;
	// GameVanilla.Core.ObjectPool GameVanilla.Core.SoundManager::soundPool
	ObjectPool_t858432606 * ___soundPool_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip> GameVanilla.Core.SoundManager::nameToSound
	Dictionary_2_t3466145964 * ___nameToSound_5;
	// GameVanilla.Core.BackgroundMusic GameVanilla.Core.SoundManager::bgMusic
	BackgroundMusic_t4234539114 * ___bgMusic_6;

public:
	inline static int32_t get_offset_of_sounds_2() { return static_cast<int32_t>(offsetof(SoundManager_t3887122299, ___sounds_2)); }
	inline List_1_t857997111 * get_sounds_2() const { return ___sounds_2; }
	inline List_1_t857997111 ** get_address_of_sounds_2() { return &___sounds_2; }
	inline void set_sounds_2(List_1_t857997111 * value)
	{
		___sounds_2 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_2), value);
	}

	inline static int32_t get_offset_of_soundPool_4() { return static_cast<int32_t>(offsetof(SoundManager_t3887122299, ___soundPool_4)); }
	inline ObjectPool_t858432606 * get_soundPool_4() const { return ___soundPool_4; }
	inline ObjectPool_t858432606 ** get_address_of_soundPool_4() { return &___soundPool_4; }
	inline void set_soundPool_4(ObjectPool_t858432606 * value)
	{
		___soundPool_4 = value;
		Il2CppCodeGenWriteBarrier((&___soundPool_4), value);
	}

	inline static int32_t get_offset_of_nameToSound_5() { return static_cast<int32_t>(offsetof(SoundManager_t3887122299, ___nameToSound_5)); }
	inline Dictionary_2_t3466145964 * get_nameToSound_5() const { return ___nameToSound_5; }
	inline Dictionary_2_t3466145964 ** get_address_of_nameToSound_5() { return &___nameToSound_5; }
	inline void set_nameToSound_5(Dictionary_2_t3466145964 * value)
	{
		___nameToSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameToSound_5), value);
	}

	inline static int32_t get_offset_of_bgMusic_6() { return static_cast<int32_t>(offsetof(SoundManager_t3887122299, ___bgMusic_6)); }
	inline BackgroundMusic_t4234539114 * get_bgMusic_6() const { return ___bgMusic_6; }
	inline BackgroundMusic_t4234539114 ** get_address_of_bgMusic_6() { return &___bgMusic_6; }
	inline void set_bgMusic_6(BackgroundMusic_t4234539114 * value)
	{
		___bgMusic_6 = value;
		Il2CppCodeGenWriteBarrier((&___bgMusic_6), value);
	}
};

struct SoundManager_t3887122299_StaticFields
{
public:
	// GameVanilla.Core.SoundManager GameVanilla.Core.SoundManager::instance
	SoundManager_t3887122299 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SoundManager_t3887122299_StaticFields, ___instance_3)); }
	inline SoundManager_t3887122299 * get_instance_3() const { return ___instance_3; }
	inline SoundManager_t3887122299 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(SoundManager_t3887122299 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T3887122299_H
#ifndef SOUNDFX_T3944835473_H
#define SOUNDFX_T3944835473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.SoundFx
struct  SoundFx_t3944835473  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource GameVanilla.Core.SoundFx::audioSource
	AudioSource_t3935305588 * ___audioSource_2;

public:
	inline static int32_t get_offset_of_audioSource_2() { return static_cast<int32_t>(offsetof(SoundFx_t3944835473, ___audioSource_2)); }
	inline AudioSource_t3935305588 * get_audioSource_2() const { return ___audioSource_2; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_2() { return &___audioSource_2; }
	inline void set_audioSource_2(AudioSource_t3935305588 * value)
	{
		___audioSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDFX_T3944835473_H
#ifndef SETFPS_T3527653638_H
#define SETFPS_T3527653638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.SetFps
struct  SetFps_t3527653638  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GameVanilla.Core.SetFps::fps
	int32_t ___fps_2;

public:
	inline static int32_t get_offset_of_fps_2() { return static_cast<int32_t>(offsetof(SetFps_t3527653638, ___fps_2)); }
	inline int32_t get_fps_2() const { return ___fps_2; }
	inline int32_t* get_address_of_fps_2() { return &___fps_2; }
	inline void set_fps_2(int32_t value)
	{
		___fps_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFPS_T3527653638_H
#ifndef SCENETRANSITION_T1732318333_H
#define SCENETRANSITION_T1732318333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.SceneTransition
struct  SceneTransition_t1732318333  : public MonoBehaviour_t3962482529
{
public:
	// System.String GameVanilla.Core.SceneTransition::scene
	String_t* ___scene_2;
	// System.Single GameVanilla.Core.SceneTransition::duration
	float ___duration_3;
	// UnityEngine.Color GameVanilla.Core.SceneTransition::color
	Color_t2555686324  ___color_4;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(SceneTransition_t1732318333, ___scene_2)); }
	inline String_t* get_scene_2() const { return ___scene_2; }
	inline String_t** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(String_t* value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier((&___scene_2), value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(SceneTransition_t1732318333, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(SceneTransition_t1732318333, ___color_4)); }
	inline Color_t2555686324  get_color_4() const { return ___color_4; }
	inline Color_t2555686324 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_t2555686324  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENETRANSITION_T1732318333_H
#ifndef AUTOKILLPOOLED_T2600245440_H
#define AUTOKILLPOOLED_T2600245440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.AutoKillPooled
struct  AutoKillPooled_t2600245440  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GameVanilla.Core.AutoKillPooled::time
	float ___time_2;
	// GameVanilla.Core.PooledObject GameVanilla.Core.AutoKillPooled::pooledObject
	PooledObject_t1286137293 * ___pooledObject_3;
	// System.Single GameVanilla.Core.AutoKillPooled::accTime
	float ___accTime_4;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(AutoKillPooled_t2600245440, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_pooledObject_3() { return static_cast<int32_t>(offsetof(AutoKillPooled_t2600245440, ___pooledObject_3)); }
	inline PooledObject_t1286137293 * get_pooledObject_3() const { return ___pooledObject_3; }
	inline PooledObject_t1286137293 ** get_address_of_pooledObject_3() { return &___pooledObject_3; }
	inline void set_pooledObject_3(PooledObject_t1286137293 * value)
	{
		___pooledObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___pooledObject_3), value);
	}

	inline static int32_t get_offset_of_accTime_4() { return static_cast<int32_t>(offsetof(AutoKillPooled_t2600245440, ___accTime_4)); }
	inline float get_accTime_4() const { return ___accTime_4; }
	inline float* get_address_of_accTime_4() { return &___accTime_4; }
	inline void set_accTime_4(float value)
	{
		___accTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOKILLPOOLED_T2600245440_H
#ifndef POOLEDOBJECT_T1286137293_H
#define POOLEDOBJECT_T1286137293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.PooledObject
struct  PooledObject_t1286137293  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Core.ObjectPool GameVanilla.Core.PooledObject::pool
	ObjectPool_t858432606 * ___pool_2;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(PooledObject_t1286137293, ___pool_2)); }
	inline ObjectPool_t858432606 * get_pool_2() const { return ___pool_2; }
	inline ObjectPool_t858432606 ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(ObjectPool_t858432606 * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___pool_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEDOBJECT_T1286137293_H
#ifndef ANIMATEDBUTTON_T997585140_H
#define ANIMATEDBUTTON_T997585140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.AnimatedButton
struct  AnimatedButton_t997585140  : public UIBehaviour_t3495933518
{
public:
	// System.Boolean GameVanilla.Core.AnimatedButton::interactable
	bool ___interactable_2;
	// GameVanilla.Core.AnimatedButton/ButtonClickedEvent GameVanilla.Core.AnimatedButton::m_onClick
	ButtonClickedEvent_t849812677 * ___m_onClick_3;
	// UnityEngine.Animator GameVanilla.Core.AnimatedButton::animator
	Animator_t434523843 * ___animator_4;
	// System.Boolean GameVanilla.Core.AnimatedButton::blockInput
	bool ___blockInput_5;

public:
	inline static int32_t get_offset_of_interactable_2() { return static_cast<int32_t>(offsetof(AnimatedButton_t997585140, ___interactable_2)); }
	inline bool get_interactable_2() const { return ___interactable_2; }
	inline bool* get_address_of_interactable_2() { return &___interactable_2; }
	inline void set_interactable_2(bool value)
	{
		___interactable_2 = value;
	}

	inline static int32_t get_offset_of_m_onClick_3() { return static_cast<int32_t>(offsetof(AnimatedButton_t997585140, ___m_onClick_3)); }
	inline ButtonClickedEvent_t849812677 * get_m_onClick_3() const { return ___m_onClick_3; }
	inline ButtonClickedEvent_t849812677 ** get_address_of_m_onClick_3() { return &___m_onClick_3; }
	inline void set_m_onClick_3(ButtonClickedEvent_t849812677 * value)
	{
		___m_onClick_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_onClick_3), value);
	}

	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(AnimatedButton_t997585140, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_blockInput_5() { return static_cast<int32_t>(offsetof(AnimatedButton_t997585140, ___blockInput_5)); }
	inline bool get_blockInput_5() const { return ___blockInput_5; }
	inline bool* get_address_of_blockInput_5() { return &___blockInput_5; }
	inline void set_blockInput_5(bool value)
	{
		___blockInput_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDBUTTON_T997585140_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (AotState_t3372302302)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2500[4] = 
{
	AotState_t3372302302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Entry_t3254413976)+ sizeof (RuntimeObject), sizeof(Entry_t3254413976_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2501[2] = 
{
	Entry_t3254413976::get_offset_of_State_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Entry_t3254413976::get_offset_of_FullTypeName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (fsAotVersionInfo_t2025365886)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[2] = 
{
	fsAotVersionInfo_t2025365886::get_offset_of_IsConstructorPublic_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsAotVersionInfo_t2025365886::get_offset_of_Members_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Member_t1335065313)+ sizeof (RuntimeObject), sizeof(Member_t1335065313_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	Member_t1335065313::get_offset_of_MemberName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Member_t1335065313::get_offset_of_JsonName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Member_t1335065313::get_offset_of_StorageType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Member_t1335065313::get_offset_of_OverrideConverterType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (fsArrayConverter_t4258383025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (fsDateConverter_t965441267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (fsDictionaryConverter_t3296500311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (fsEnumConverter_t2820841052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (fsForwardAttribute_t3729870757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[1] = 
{
	fsForwardAttribute_t3729870757::get_offset_of_MemberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (fsForwardConverter_t275048757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	fsForwardConverter_t275048757::get_offset_of__memberName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (fsGuidConverter_t3436012287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (fsIEnumerableConverter_t409581950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (fsKeyValuePairConverter_t1178744676), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (fsNullableConverter_t759066596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (fsPrimitiveConverter_t1335342894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (fsReflectedConverter_t1873634900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (fsTypeConverter_t3131242195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (fsWeakReferenceConverter_t2853701403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (fsConverterRegistrar_t1190956781), -1, sizeof(fsConverterRegistrar_t1190956781_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2519[10] = 
{
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_AnimationCurve_DirectConverter_0(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_Bounds_DirectConverter_1(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_Gradient_DirectConverter_2(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_GUIStyle_DirectConverter_3(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_GUIStyleState_DirectConverter_4(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_Keyframe_DirectConverter_5(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_LayerMask_DirectConverter_6(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_Rect_DirectConverter_7(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Register_RectOffset_DirectConverter_8(),
	fsConverterRegistrar_t1190956781_StaticFields::get_offset_of_Converters_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (AnimationCurve_DirectConverter_t2921849045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (Bounds_DirectConverter_t3871173093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (Gradient_DirectConverter_t2745360243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (GUIStyle_DirectConverter_t1060365653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (GUIStyleState_DirectConverter_t3967104754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (Keyframe_DirectConverter_t1448983566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (LayerMask_DirectConverter_t4211565801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (Rect_DirectConverter_t1861700375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (RectOffset_DirectConverter_t483038648), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (UnityEvent_Converter_t3778070767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (fsBaseConverter_t1567749939), -1, sizeof(fsBaseConverter_t1567749939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2530[2] = 
{
	fsBaseConverter_t1567749939::get_offset_of_Serializer_0(),
	fsBaseConverter_t1567749939_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (fsGlobalConfig_t438576193), -1, sizeof(fsGlobalConfig_t438576193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	fsGlobalConfig_t438576193_StaticFields::get_offset_of_IsCaseSensitive_0(),
	fsGlobalConfig_t438576193_StaticFields::get_offset_of_AllowInternalExceptions_1(),
	fsGlobalConfig_t438576193_StaticFields::get_offset_of_InternalFieldPrefix_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (fsConfig_t14416447), -1, sizeof(fsConfig_t14416447_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[12] = 
{
	fsConfig_t14416447::get_offset_of_SerializeAttributes_0(),
	fsConfig_t14416447::get_offset_of_IgnoreSerializeAttributes_1(),
	fsConfig_t14416447::get_offset_of_IgnoreSerializeTypeAttributes_2(),
	fsConfig_t14416447::get_offset_of_DefaultMemberSerialization_3(),
	fsConfig_t14416447::get_offset_of_GetJsonNameFromMemberName_4(),
	fsConfig_t14416447::get_offset_of_EnablePropertySerialization_5(),
	fsConfig_t14416447::get_offset_of_SerializeNonAutoProperties_6(),
	fsConfig_t14416447::get_offset_of_SerializeNonPublicSetProperties_7(),
	fsConfig_t14416447::get_offset_of_CustomDateTimeFormatString_8(),
	fsConfig_t14416447::get_offset_of_Serialize64BitIntegerAsString_9(),
	fsConfig_t14416447::get_offset_of_SerializeEnumsAsInteger_10(),
	fsConfig_t14416447_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (fsContext_t2261407774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[1] = 
{
	fsContext_t2261407774::get_offset_of__contextObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (fsConverter_t1940635020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (fsDataType_t2663285942)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2535[8] = 
{
	fsDataType_t2663285942::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (fsData_t406773287), -1, sizeof(fsData_t406773287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2536[4] = 
{
	fsData_t406773287::get_offset_of__value_0(),
	fsData_t406773287_StaticFields::get_offset_of_True_1(),
	fsData_t406773287_StaticFields::get_offset_of_False_2(),
	fsData_t406773287_StaticFields::get_offset_of_Null_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (fsDirectConverter_t3189684931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (fsMissingVersionConstructorException_t4130735797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (fsDuplicateVersionNameException_t187941636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (fsIgnoreAttribute_t2855408247), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (fsSerializationCallbackProcessor_t3447229557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (fsSerializationCallbackReceiverProcessor_t3854961681), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (fsJsonParser_t2842196407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[3] = 
{
	fsJsonParser_t2842196407::get_offset_of__start_0(),
	fsJsonParser_t2842196407::get_offset_of__input_1(),
	fsJsonParser_t2842196407::get_offset_of__cachedStringBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (fsJsonPrinter_t3181599744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (fsMemberSerialization_t1535378461)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2547[4] = 
{
	fsMemberSerialization_t1535378461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (fsObjectAttribute_t1137703855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[5] = 
{
	fsObjectAttribute_t1137703855::get_offset_of_PreviousModels_0(),
	fsObjectAttribute_t1137703855::get_offset_of_VersionString_1(),
	fsObjectAttribute_t1137703855::get_offset_of_MemberSerialization_2(),
	fsObjectAttribute_t1137703855::get_offset_of_Converter_3(),
	fsObjectAttribute_t1137703855::get_offset_of_Processor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (fsObjectProcessor_t419368844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (fsPropertyAttribute_t3696058176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	fsPropertyAttribute_t3696058176::get_offset_of_Name_0(),
	fsPropertyAttribute_t3696058176::get_offset_of_Converter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (fsResult_t591339677)+ sizeof (RuntimeObject), -1, sizeof(fsResult_t591339677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2551[4] = 
{
	fsResult_t591339677_StaticFields::get_offset_of_EmptyStringArray_0(),
	fsResult_t591339677::get_offset_of__success_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsResult_t591339677::get_offset_of__messages_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsResult_t591339677_StaticFields::get_offset_of_Success_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (fsSerializer_t4093814827), -1, sizeof(fsSerializer_t4093814827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[17] = 
{
	fsSerializer_t4093814827_StaticFields::get_offset_of__reservedKeywords_0(),
	fsSerializer_t4093814827_StaticFields::get_offset_of_Key_ObjectReference_1(),
	fsSerializer_t4093814827_StaticFields::get_offset_of_Key_ObjectDefinition_2(),
	fsSerializer_t4093814827_StaticFields::get_offset_of_Key_InstanceType_3(),
	fsSerializer_t4093814827_StaticFields::get_offset_of_Key_Version_4(),
	fsSerializer_t4093814827_StaticFields::get_offset_of_Key_Content_5(),
	fsSerializer_t4093814827::get_offset_of__cachedConverterTypeInstances_6(),
	fsSerializer_t4093814827::get_offset_of__cachedConverters_7(),
	fsSerializer_t4093814827::get_offset_of__cachedProcessors_8(),
	fsSerializer_t4093814827::get_offset_of__availableConverters_9(),
	fsSerializer_t4093814827::get_offset_of__availableDirectConverters_10(),
	fsSerializer_t4093814827::get_offset_of__processors_11(),
	fsSerializer_t4093814827::get_offset_of__references_12(),
	fsSerializer_t4093814827::get_offset_of__lazyReferenceWriter_13(),
	fsSerializer_t4093814827::get_offset_of__abstractTypeRemap_14(),
	fsSerializer_t4093814827::get_offset_of_Context_15(),
	fsSerializer_t4093814827::get_offset_of_Config_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (fsLazyCycleDefinitionWriter_t239783049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[2] = 
{
	fsLazyCycleDefinitionWriter_t239783049::get_offset_of__pendingDefinitions_0(),
	fsLazyCycleDefinitionWriter_t239783049::get_offset_of__references_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (fsCyclicReferenceManager_t2963233308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[4] = 
{
	fsCyclicReferenceManager_t2963233308::get_offset_of__objectIds_0(),
	fsCyclicReferenceManager_t2963233308::get_offset_of__nextId_1(),
	fsCyclicReferenceManager_t2963233308::get_offset_of__marked_2(),
	fsCyclicReferenceManager_t2963233308::get_offset_of__depth_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (ObjectReferenceEqualityComparator_t1966295411), -1, sizeof(ObjectReferenceEqualityComparator_t1966295411_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[1] = 
{
	ObjectReferenceEqualityComparator_t1966295411_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (fsOption_t2623647891), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (fsPortableReflection_t672844747), -1, sizeof(fsPortableReflection_t672844747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[3] = 
{
	fsPortableReflection_t672844747_StaticFields::get_offset_of_EmptyTypes_0(),
	fsPortableReflection_t672844747_StaticFields::get_offset_of__cachedAttributeQueries_1(),
	fsPortableReflection_t672844747_StaticFields::get_offset_of_DeclaredFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (AttributeQuery_t874673239)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[2] = 
{
	AttributeQuery_t874673239::get_offset_of_MemberInfo_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttributeQuery_t874673239::get_offset_of_AttributeType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (AttributeQueryComparator_t2576872653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[8] = 
{
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_type_0(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_U3CmethodsU3E__1_1(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_U3CiU3E__2_2(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_methodName_3(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_U24current_4(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_U24disposing_5(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_U3CU24U3Etype_6(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t1229162269::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (fsTypeExtensions_t1429624868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (U3CCSharpNameU3Ec__AnonStorey0_t3622865521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	U3CCSharpNameU3Ec__AnonStorey0_t3622865521::get_offset_of_includeNamespace_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (fsVersionedType_t1764735544)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[3] = 
{
	fsVersionedType_t1764735544::get_offset_of_Ancestors_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsVersionedType_t1764735544::get_offset_of_VersionString_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsVersionedType_t1764735544::get_offset_of_ModelType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (fsVersionManager_t3974572827), -1, sizeof(fsVersionManager_t3974572827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2565[1] = 
{
	fsVersionManager_t3974572827_StaticFields::get_offset_of__cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (fsMetaProperty_t1347894527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[9] = 
{
	fsMetaProperty_t1347894527::get_offset_of__memberInfo_0(),
	fsMetaProperty_t1347894527::get_offset_of_U3CStorageTypeU3Ek__BackingField_1(),
	fsMetaProperty_t1347894527::get_offset_of_U3COverrideConverterTypeU3Ek__BackingField_2(),
	fsMetaProperty_t1347894527::get_offset_of_U3CCanReadU3Ek__BackingField_3(),
	fsMetaProperty_t1347894527::get_offset_of_U3CCanWriteU3Ek__BackingField_4(),
	fsMetaProperty_t1347894527::get_offset_of_U3CJsonNameU3Ek__BackingField_5(),
	fsMetaProperty_t1347894527::get_offset_of_U3CMemberNameU3Ek__BackingField_6(),
	fsMetaProperty_t1347894527::get_offset_of_U3CIsPublicU3Ek__BackingField_7(),
	fsMetaProperty_t1347894527::get_offset_of_U3CIsReadOnlyU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (fsMetaType_t439135866), -1, sizeof(fsMetaType_t439135866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2567[5] = 
{
	fsMetaType_t439135866_StaticFields::get_offset_of__configMetaTypes_0(),
	fsMetaType_t439135866::get_offset_of_ReflectedType_1(),
	fsMetaType_t439135866::get_offset_of_U3CPropertiesU3Ek__BackingField_2(),
	fsMetaType_t439135866::get_offset_of__hasDefaultConstructorCache_3(),
	fsMetaType_t439135866::get_offset_of__isDefaultConstructorPublicCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (AotFailureException_t3905637742), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (U3CCollectPropertiesU3Ec__AnonStorey0_t1189373392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[1] = 
{
	U3CCollectPropertiesU3Ec__AnonStorey0_t1189373392::get_offset_of_member_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (U3CCanSerializePropertyU3Ec__AnonStorey1_t1124286439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[1] = 
{
	U3CCanSerializePropertyU3Ec__AnonStorey1_t1124286439::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (U3CCanSerializeFieldU3Ec__AnonStorey2_t1489263536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[1] = 
{
	U3CCanSerializeFieldU3Ec__AnonStorey2_t1489263536::get_offset_of_field_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (fsReflectionUtility_t2837052610), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (fsTypeCache_t3469578604), -1, sizeof(fsTypeCache_t3469578604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	fsTypeCache_t3469578604_StaticFields::get_offset_of__cachedTypes_0(),
	fsTypeCache_t3469578604_StaticFields::get_offset_of__assembliesByName_1(),
	fsTypeCache_t3469578604_StaticFields::get_offset_of__assembliesByIndex_2(),
	fsTypeCache_t3469578604_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (AnimatedButton_t997585140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[4] = 
{
	AnimatedButton_t997585140::get_offset_of_interactable_2(),
	AnimatedButton_t997585140::get_offset_of_m_onClick_3(),
	AnimatedButton_t997585140::get_offset_of_animator_4(),
	AnimatedButton_t997585140::get_offset_of_blockInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (ButtonClickedEvent_t849812677), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[4] = 
{
	U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608::get_offset_of_U24this_0(),
	U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608::get_offset_of_U24current_1(),
	U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608::get_offset_of_U24disposing_2(),
	U3CInvokeOnClickActionU3Ec__Iterator0_t2423343608::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[4] = 
{
	U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417::get_offset_of_U24this_0(),
	U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417::get_offset_of_U24current_1(),
	U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417::get_offset_of_U24disposing_2(),
	U3CBlockInputTemporarilyU3Ec__Iterator1_t1318499417::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (AutoKillPooled_t2600245440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[3] = 
{
	AutoKillPooled_t2600245440::get_offset_of_time_2(),
	AutoKillPooled_t2600245440::get_offset_of_pooledObject_3(),
	AutoKillPooled_t2600245440::get_offset_of_accTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (BackgroundMusic_t4234539114), -1, sizeof(BackgroundMusic_t4234539114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2579[2] = 
{
	BackgroundMusic_t4234539114_StaticFields::get_offset_of_instance_2(),
	BackgroundMusic_t4234539114::get_offset_of_audioSource_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (BaseScene_t2918414559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[3] = 
{
	BaseScene_t2918414559::get_offset_of_canvas_2(),
	BaseScene_t2918414559::get_offset_of_currentPopups_3(),
	BaseScene_t2918414559::get_offset_of_currentPanels_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (U3CClosePopupU3Ec__AnonStorey3_t2207380370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[1] = 
{
	U3CClosePopupU3Ec__AnonStorey3_t2207380370::get_offset_of_topmostPanel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (U3CFadeInU3Ec__Iterator1_t4170895653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[8] = 
{
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_image_0(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_U3CalphaU3E__0_1(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_U3CtU3E__1_2(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_U3CcolorU3E__2_3(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_time_4(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_U24current_5(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_U24disposing_6(),
	U3CFadeInU3Ec__Iterator1_t4170895653::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (U3CFadeOutU3Ec__Iterator2_t2086620696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[9] = 
{
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_image_0(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_U3CalphaU3E__0_1(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_U3CtU3E__1_2(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_U3CcolorU3E__2_3(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_time_4(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_onComplete_5(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_U24current_6(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_U24disposing_7(),
	U3CFadeOutU3Ec__Iterator2_t2086620696::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (FileUtils_t3666727464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (ObjectPool_t858432606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[3] = 
{
	ObjectPool_t858432606::get_offset_of_prefab_2(),
	ObjectPool_t858432606::get_offset_of_initialSize_3(),
	ObjectPool_t858432606::get_offset_of_instances_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (PooledObject_t1286137293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[1] = 
{
	PooledObject_t1286137293::get_offset_of_pool_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (PlaySound_t1822700253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Popup_t1063720813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[4] = 
{
	Popup_t1063720813::get_offset_of_parentScene_2(),
	Popup_t1063720813::get_offset_of_onOpen_3(),
	Popup_t1063720813::get_offset_of_onClose_4(),
	Popup_t1063720813::get_offset_of_animator_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (U3CDestroyPopupU3Ec__Iterator0_t1470244993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[4] = 
{
	U3CDestroyPopupU3Ec__Iterator0_t1470244993::get_offset_of_U24this_0(),
	U3CDestroyPopupU3Ec__Iterator0_t1470244993::get_offset_of_U24current_1(),
	U3CDestroyPopupU3Ec__Iterator0_t1470244993::get_offset_of_U24disposing_2(),
	U3CDestroyPopupU3Ec__Iterator0_t1470244993::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (ReflectionUtils_t3994853556), -1, sizeof(ReflectionUtils_t3994853556_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[3] = 
{
	ReflectionUtils_t3994853556_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	ReflectionUtils_t3994853556_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	ReflectionUtils_t3994853556_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (U3CGetAllDerivedTypesU3Ec__AnonStorey0_t2222601090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[1] = 
{
	U3CGetAllDerivedTypesU3Ec__AnonStorey0_t2222601090::get_offset_of_aType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (SceneTransition_t1732318333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[3] = 
{
	SceneTransition_t1732318333::get_offset_of_scene_2(),
	SceneTransition_t1732318333::get_offset_of_duration_3(),
	SceneTransition_t1732318333::get_offset_of_color_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (SetFps_t3527653638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	SetFps_t3527653638::get_offset_of_fps_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (SoundFx_t3944835473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	SoundFx_t3944835473::get_offset_of_audioSource_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (SoundManager_t3887122299), -1, sizeof(SoundManager_t3887122299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[5] = 
{
	SoundManager_t3887122299::get_offset_of_sounds_2(),
	SoundManager_t3887122299_StaticFields::get_offset_of_instance_3(),
	SoundManager_t3887122299::get_offset_of_soundPool_4(),
	SoundManager_t3887122299::get_offset_of_nameToSound_5(),
	SoundManager_t3887122299::get_offset_of_bgMusic_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (SpriteSwapper_t2892354464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[3] = 
{
	SpriteSwapper_t2892354464::get_offset_of_enabledSprite_2(),
	SpriteSwapper_t2892354464::get_offset_of_disabledSprite_3(),
	SpriteSwapper_t2892354464::get_offset_of_image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (StringUtils_t120023749), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (Transition_t3232023259), -1, sizeof(Transition_t3232023259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2599[2] = 
{
	Transition_t3232023259_StaticFields::get_offset_of_canvasObject_2(),
	Transition_t3232023259::get_offset_of_m_overlay_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
