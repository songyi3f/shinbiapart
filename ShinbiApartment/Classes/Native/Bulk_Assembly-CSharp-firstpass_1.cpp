﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// LTSpline
struct LTSpline_t2431306763;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// LTUtility
struct LTUtility_t4124338238;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;

extern RuntimeClass* LeanTween_t1803894739_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* LTSpline_t2431306763_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1334014851;
extern const uint32_t LTSpline_init_m1124765653_MetadataUsageId;
extern const uint32_t LTSpline_map_m1823780591_MetadataUsageId;
extern const uint32_t LTSpline_interp_m2769368846_MetadataUsageId;
extern const uint32_t LTSpline_ratioAtPoint_m1723157471_MetadataUsageId;
extern const uint32_t LTSpline_place2d_m3939880521_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t LTSpline_placeLocal2d_m1627593543_MetadataUsageId;
extern const uint32_t LTSpline_place_m2468867645_MetadataUsageId;
extern const uint32_t LTSpline_placeLocal_m3314740694_MetadataUsageId;
extern const uint32_t LTSpline_drawGizmo_m2577804934_MetadataUsageId;
extern RuntimeClass* List_1_t899420910_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2503402603_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1524640104_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m466715028_RuntimeMethod_var;
extern const uint32_t LTSpline_generateVectors_m1739496936_MetadataUsageId;
extern const uint32_t LTSpline__cctor_m2372119498_MetadataUsageId;

struct Vector3U5BU5D_t1718750761;
struct TransformU5BU5D_t807237628;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T899420910_H
#define LIST_1_T899420910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t899420910  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1718750761* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____items_1)); }
	inline Vector3U5BU5D_t1718750761* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1718750761* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t899420910_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t1718750761* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t899420910_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t1718750761* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t1718750761* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T899420910_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LTUTILITY_T4124338238_H
#define LTUTILITY_T4124338238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTUtility
struct  LTUtility_t4124338238  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTUTILITY_T4124338238_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LTSPLINE_T2431306763_H
#define LTSPLINE_T2431306763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTSpline
struct  LTSpline_t2431306763  : public RuntimeObject
{
public:
	// System.Single LTSpline::distance
	float ___distance_2;
	// System.Boolean LTSpline::constantSpeed
	bool ___constantSpeed_3;
	// UnityEngine.Vector3[] LTSpline::pts
	Vector3U5BU5D_t1718750761* ___pts_4;
	// UnityEngine.Vector3[] LTSpline::ptsAdj
	Vector3U5BU5D_t1718750761* ___ptsAdj_5;
	// System.Int32 LTSpline::ptsAdjLength
	int32_t ___ptsAdjLength_6;
	// System.Boolean LTSpline::orientToPath
	bool ___orientToPath_7;
	// System.Boolean LTSpline::orientToPath2d
	bool ___orientToPath2d_8;
	// System.Int32 LTSpline::numSections
	int32_t ___numSections_9;
	// System.Int32 LTSpline::currPt
	int32_t ___currPt_10;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_3() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___constantSpeed_3)); }
	inline bool get_constantSpeed_3() const { return ___constantSpeed_3; }
	inline bool* get_address_of_constantSpeed_3() { return &___constantSpeed_3; }
	inline void set_constantSpeed_3(bool value)
	{
		___constantSpeed_3 = value;
	}

	inline static int32_t get_offset_of_pts_4() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___pts_4)); }
	inline Vector3U5BU5D_t1718750761* get_pts_4() const { return ___pts_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pts_4() { return &___pts_4; }
	inline void set_pts_4(Vector3U5BU5D_t1718750761* value)
	{
		___pts_4 = value;
		Il2CppCodeGenWriteBarrier((&___pts_4), value);
	}

	inline static int32_t get_offset_of_ptsAdj_5() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___ptsAdj_5)); }
	inline Vector3U5BU5D_t1718750761* get_ptsAdj_5() const { return ___ptsAdj_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_ptsAdj_5() { return &___ptsAdj_5; }
	inline void set_ptsAdj_5(Vector3U5BU5D_t1718750761* value)
	{
		___ptsAdj_5 = value;
		Il2CppCodeGenWriteBarrier((&___ptsAdj_5), value);
	}

	inline static int32_t get_offset_of_ptsAdjLength_6() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___ptsAdjLength_6)); }
	inline int32_t get_ptsAdjLength_6() const { return ___ptsAdjLength_6; }
	inline int32_t* get_address_of_ptsAdjLength_6() { return &___ptsAdjLength_6; }
	inline void set_ptsAdjLength_6(int32_t value)
	{
		___ptsAdjLength_6 = value;
	}

	inline static int32_t get_offset_of_orientToPath_7() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___orientToPath_7)); }
	inline bool get_orientToPath_7() const { return ___orientToPath_7; }
	inline bool* get_address_of_orientToPath_7() { return &___orientToPath_7; }
	inline void set_orientToPath_7(bool value)
	{
		___orientToPath_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_8() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___orientToPath2d_8)); }
	inline bool get_orientToPath2d_8() const { return ___orientToPath2d_8; }
	inline bool* get_address_of_orientToPath2d_8() { return &___orientToPath2d_8; }
	inline void set_orientToPath2d_8(bool value)
	{
		___orientToPath2d_8 = value;
	}

	inline static int32_t get_offset_of_numSections_9() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___numSections_9)); }
	inline int32_t get_numSections_9() const { return ___numSections_9; }
	inline int32_t* get_address_of_numSections_9() { return &___numSections_9; }
	inline void set_numSections_9(int32_t value)
	{
		___numSections_9 = value;
	}

	inline static int32_t get_offset_of_currPt_10() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___currPt_10)); }
	inline int32_t get_currPt_10() const { return ___currPt_10; }
	inline int32_t* get_address_of_currPt_10() { return &___currPt_10; }
	inline void set_currPt_10(int32_t value)
	{
		___currPt_10 = value;
	}
};

struct LTSpline_t2431306763_StaticFields
{
public:
	// System.Int32 LTSpline::DISTANCE_COUNT
	int32_t ___DISTANCE_COUNT_0;
	// System.Int32 LTSpline::SUBLINE_COUNT
	int32_t ___SUBLINE_COUNT_1;

public:
	inline static int32_t get_offset_of_DISTANCE_COUNT_0() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763_StaticFields, ___DISTANCE_COUNT_0)); }
	inline int32_t get_DISTANCE_COUNT_0() const { return ___DISTANCE_COUNT_0; }
	inline int32_t* get_address_of_DISTANCE_COUNT_0() { return &___DISTANCE_COUNT_0; }
	inline void set_DISTANCE_COUNT_0(int32_t value)
	{
		___DISTANCE_COUNT_0 = value;
	}

	inline static int32_t get_offset_of_SUBLINE_COUNT_1() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763_StaticFields, ___SUBLINE_COUNT_1)); }
	inline int32_t get_SUBLINE_COUNT_1() const { return ___SUBLINE_COUNT_1; }
	inline int32_t* get_address_of_SUBLINE_COUNT_1() { return &___SUBLINE_COUNT_1; }
	inline void set_SUBLINE_COUNT_1(int32_t value)
	{
		___SUBLINE_COUNT_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTSPLINE_T2431306763_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef TWEENACTION_T2598825989_H
#define TWEENACTION_T2598825989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAction
struct  TweenAction_t2598825989 
{
public:
	// System.Int32 TweenAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenAction_t2598825989, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENACTION_T2598825989_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_t3600365921 * m_Items[1];

public:
	inline Transform_t3600365921 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_t3600365921 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_t3600365921 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3600365921 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_t3600365921 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_t3600365921 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m2503402603_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C"  void List_1_Add_m1524640104_gshared (List_1_t899420910 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t1718750761* List_1_ToArray_m466715028_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LTSpline::init(UnityEngine.Vector3[],System.Boolean)
extern "C"  void LTSpline_init_m1124765653 (LTSpline_t2431306763 * __this, Vector3U5BU5D_t1718750761* ___pts0, bool ___constantSpeed1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object LeanTween::logError(System.String)
extern "C"  RuntimeObject * LeanTween_logError_m279732101 (RuntimeObject * __this /* static, unused */, String_t* ___error0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
extern "C"  void Array_Copy_m1988217701 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeArray * p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m886789632 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 LTSpline::interp(System.Single)
extern "C"  Vector3_t3722313464  LTSpline_interp_m2769368846 (LTSpline_t2431306763 * __this, float ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m1870542928 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m18103608 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_UnaryNegation_m1951478815 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m2104357790 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 LTSpline::map(System.Single)
extern "C"  Vector3_t3722313464  LTSpline_map_m1823780591 (LTSpline_t2431306763 * __this, float ___u0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 LTSpline::point(System.Single)
extern "C"  Vector3_t3722313464  LTSpline_point_m2976918333 (LTSpline_t2431306763 * __this, float ___ratio0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m135219616 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LTSpline::place2d(UnityEngine.Transform,System.Single)
extern "C"  void LTSpline_place2d_m3939880521 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3722313464  Transform_get_localPosition_m4234289348 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m4202601546 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LTSpline::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern "C"  void LTSpline_place_m3689574363 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, Vector3_t3722313464  ___worldUp2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m3639503211 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern "C"  void LTSpline_placeLocal_m3334513255 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, Vector3_t3722313464  ___worldUp2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformPoint_m226827784 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m3273476787 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Gizmos::get_color()
extern "C"  Color_t2555686324  Gizmos_get_color_m712612415 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m3399737545 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LTSpline::.ctor(UnityEngine.Vector3[])
extern "C"  void LTSpline__ctor_m3370541708 (LTSpline_t2431306763 * __this, Vector3U5BU5D_t1718750761* ___pts0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PushMatrix()
extern "C"  void GL_PushMatrix_m1848274883 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m686253719 (Material_t340375123 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::LoadPixelMatrix()
extern "C"  void GL_LoadPixelMatrix_m3084279490 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C"  void GL_Begin_m1290681325 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Color(UnityEngine.Color)
extern "C"  void GL_Color_m2127587175 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Vertex(UnityEngine.Vector3)
extern "C"  void GL_Vertex_m691990801 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::End()
extern "C"  void GL_End_m539612367 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PopMatrix()
extern "C"  void GL_PopMatrix_m3416050869 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m2503402603(__this, method) ((  void (*) (List_1_t899420910 *, const RuntimeMethod*))List_1__ctor_m2503402603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m1524640104(__this, p0, method) ((  void (*) (List_1_t899420910 *, Vector3_t3722313464 , const RuntimeMethod*))List_1_Add_m1524640104_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
#define List_1_ToArray_m466715028(__this, method) ((  Vector3U5BU5D_t1718750761* (*) (List_1_t899420910 *, const RuntimeMethod*))List_1_ToArray_m466715028_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LTSpline::.ctor(UnityEngine.Vector3[])
extern "C"  void LTSpline__ctor_m3370541708 (LTSpline_t2431306763 * __this, Vector3U5BU5D_t1718750761* ___pts0, const RuntimeMethod* method)
{
	{
		__this->set_constantSpeed_3((bool)1);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Vector3U5BU5D_t1718750761* L_0 = ___pts0;
		LTSpline_init_m1124765653(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LTSpline::.ctor(UnityEngine.Vector3[],System.Boolean)
extern "C"  void LTSpline__ctor_m1798028453 (LTSpline_t2431306763 * __this, Vector3U5BU5D_t1718750761* ___pts0, bool ___constantSpeed1, const RuntimeMethod* method)
{
	{
		__this->set_constantSpeed_3((bool)1);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		bool L_0 = ___constantSpeed1;
		__this->set_constantSpeed_3(L_0);
		Vector3U5BU5D_t1718750761* L_1 = ___pts0;
		bool L_2 = ___constantSpeed1;
		LTSpline_init_m1124765653(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LTSpline::init(UnityEngine.Vector3[],System.Boolean)
extern "C"  void LTSpline_init_m1124765653 (LTSpline_t2431306763 * __this, Vector3U5BU5D_t1718750761* ___pts0, bool ___constantSpeed1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_init_m1124765653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	{
		Vector3U5BU5D_t1718750761* L_0 = ___pts0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LeanTween_t1803894739_il2cpp_TypeInfo_var);
		LeanTween_logError_m279732101(NULL /*static, unused*/, _stringLiteral1334014851, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		Vector3U5BU5D_t1718750761* L_1 = ___pts0;
		NullCheck(L_1);
		__this->set_pts_4(((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))));
		Vector3U5BU5D_t1718750761* L_2 = ___pts0;
		Vector3U5BU5D_t1718750761* L_3 = __this->get_pts_4();
		Vector3U5BU5D_t1718750761* L_4 = ___pts0;
		NullCheck(L_4);
		Array_Copy_m1988217701(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_2, (RuntimeArray *)(RuntimeArray *)L_3, (((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))), /*hidden argument*/NULL);
		Vector3U5BU5D_t1718750761* L_5 = ___pts0;
		NullCheck(L_5);
		__this->set_numSections_9(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))), (int32_t)3)));
		V_0 = (std::numeric_limits<float>::infinity());
		Vector3U5BU5D_t1718750761* L_6 = __this->get_pts_4();
		NullCheck(L_6);
		V_1 = (*(Vector3_t3722313464 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		V_2 = (0.0f);
		V_3 = 1;
		goto IL_008f;
	}

IL_0062:
	{
		Vector3U5BU5D_t1718750761* L_7 = __this->get_pts_4();
		int32_t L_8 = V_3;
		NullCheck(L_7);
		Vector3_t3722313464  L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_10 = Vector3_Distance_m886789632(NULL /*static, unused*/, (*(Vector3_t3722313464 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_4;
		float L_12 = V_0;
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_0086;
		}
	}
	{
		float L_13 = V_4;
		V_0 = L_13;
	}

IL_0086:
	{
		float L_14 = V_2;
		float L_15 = V_4;
		V_2 = ((float)il2cpp_codegen_add((float)L_14, (float)L_15));
		int32_t L_16 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_008f:
	{
		int32_t L_17 = V_3;
		Vector3U5BU5D_t1718750761* L_18 = __this->get_pts_4();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))), (int32_t)1)))))
		{
			goto IL_0062;
		}
	}
	{
		bool L_19 = ___constantSpeed1;
		if (!L_19)
		{
			goto IL_0194;
		}
	}
	{
		float L_20 = V_2;
		int32_t L_21 = __this->get_numSections_9();
		IL2CPP_RUNTIME_CLASS_INIT(LTSpline_t2431306763_il2cpp_TypeInfo_var);
		int32_t L_22 = ((LTSpline_t2431306763_StaticFields*)il2cpp_codegen_static_fields_for(LTSpline_t2431306763_il2cpp_TypeInfo_var))->get_SUBLINE_COUNT_1();
		V_0 = ((float)((float)L_20/(float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)L_21, (int32_t)L_22)))))));
		float L_23 = V_0;
		int32_t L_24 = ((LTSpline_t2431306763_StaticFields*)il2cpp_codegen_static_fields_for(LTSpline_t2431306763_il2cpp_TypeInfo_var))->get_SUBLINE_COUNT_1();
		V_5 = ((float)((float)L_23/(float)(((float)((float)L_24)))));
		float L_25 = V_2;
		float L_26 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_27 = ceilf(((float)((float)L_25/(float)L_26)));
		int32_t L_28 = ((LTSpline_t2431306763_StaticFields*)il2cpp_codegen_static_fields_for(LTSpline_t2431306763_il2cpp_TypeInfo_var))->get_DISTANCE_COUNT_0();
		V_6 = ((int32_t)il2cpp_codegen_multiply((int32_t)(((int32_t)((int32_t)L_27))), (int32_t)L_28));
		int32_t L_29 = V_6;
		if ((((int32_t)L_29) > ((int32_t)1)))
		{
			goto IL_00dc;
		}
	}
	{
		V_6 = 2;
	}

IL_00dc:
	{
		int32_t L_30 = V_6;
		__this->set_ptsAdj_5(((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)L_30)));
		Vector3_t3722313464  L_31 = LTSpline_interp_m2769368846(__this, (0.0f), /*hidden argument*/NULL);
		V_1 = L_31;
		V_7 = 1;
		Vector3U5BU5D_t1718750761* L_32 = __this->get_ptsAdj_5();
		NullCheck(L_32);
		Vector3_t3722313464  L_33 = V_1;
		*(Vector3_t3722313464 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_33;
		__this->set_distance_2((0.0f));
		V_8 = 0;
		goto IL_0181;
	}

IL_011d:
	{
		int32_t L_34 = V_8;
		int32_t L_35 = V_6;
		V_9 = ((float)((float)(((float)((float)L_34)))/(float)(((float)((float)L_35)))));
		float L_36 = V_9;
		Vector3_t3722313464  L_37 = LTSpline_interp_m2769368846(__this, L_36, /*hidden argument*/NULL);
		V_10 = L_37;
		Vector3_t3722313464  L_38 = V_10;
		Vector3_t3722313464  L_39 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_40 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		V_11 = L_40;
		float L_41 = V_11;
		float L_42 = V_5;
		if ((((float)L_41) >= ((float)L_42)))
		{
			goto IL_014f;
		}
	}
	{
		float L_43 = V_9;
		if ((!(((float)L_43) >= ((float)(1.0f)))))
		{
			goto IL_017b;
		}
	}

IL_014f:
	{
		Vector3U5BU5D_t1718750761* L_44 = __this->get_ptsAdj_5();
		int32_t L_45 = V_7;
		NullCheck(L_44);
		Vector3_t3722313464  L_46 = V_10;
		*(Vector3_t3722313464 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45))) = L_46;
		float L_47 = __this->get_distance_2();
		float L_48 = V_11;
		__this->set_distance_2(((float)il2cpp_codegen_add((float)L_47, (float)L_48)));
		Vector3_t3722313464  L_49 = V_10;
		V_1 = L_49;
		int32_t L_50 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1));
	}

IL_017b:
	{
		int32_t L_51 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_0181:
	{
		int32_t L_52 = V_8;
		int32_t L_53 = V_6;
		if ((((int32_t)L_52) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)1)))))
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_54 = V_7;
		__this->set_ptsAdjLength_6(L_54);
	}

IL_0194:
	{
		return;
	}
}
// UnityEngine.Vector3 LTSpline::map(System.Single)
extern "C"  Vector3_t3722313464  LTSpline_map_m1823780591 (LTSpline_t2431306763 * __this, float ___u0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_map_m1823780591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	{
		float L_0 = ___u0;
		if ((!(((float)L_0) >= ((float)(1.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_1 = __this->get_pts_4();
		Vector3U5BU5D_t1718750761* L_2 = __this->get_pts_4();
		NullCheck(L_2);
		NullCheck(L_1);
		return (*(Vector3_t3722313464 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))), (int32_t)2))))));
	}

IL_0026:
	{
		float L_3 = ___u0;
		int32_t L_4 = __this->get_ptsAdjLength_6();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_3, (float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)))))));
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_6 = floorf(L_5);
		V_1 = (((int32_t)((int32_t)L_6)));
		float L_7 = V_0;
		float L_8 = ceilf(L_7);
		V_2 = (((int32_t)((int32_t)L_8)));
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_004b;
		}
	}
	{
		V_1 = 0;
	}

IL_004b:
	{
		Vector3U5BU5D_t1718750761* L_10 = __this->get_ptsAdj_5();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		V_3 = (*(Vector3_t3722313464 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11))));
		Vector3U5BU5D_t1718750761* L_12 = __this->get_ptsAdj_5();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		V_4 = (*(Vector3_t3722313464 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13))));
		float L_14 = V_0;
		int32_t L_15 = V_1;
		V_5 = ((float)il2cpp_codegen_subtract((float)L_14, (float)(((float)((float)L_15)))));
		Vector3_t3722313464  L_16 = V_3;
		Vector3_t3722313464  L_17 = V_4;
		Vector3_t3722313464  L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_19 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		float L_20 = V_5;
		Vector3_t3722313464  L_21 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_16, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		Vector3_t3722313464  L_23 = V_3;
		return L_23;
	}
}
// UnityEngine.Vector3 LTSpline::interp(System.Single)
extern "C"  Vector3_t3722313464  LTSpline_interp_m2769368846 (LTSpline_t2431306763 * __this, float ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_interp_m2769368846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		float L_0 = ___t0;
		int32_t L_1 = __this->get_numSections_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_0, (float)(((float)((float)L_1))))), /*hidden argument*/NULL);
		int32_t L_3 = __this->get_numSections_9();
		int32_t L_4 = Mathf_Min_m18103608(NULL /*static, unused*/, L_2, ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_currPt_10(L_4);
		float L_5 = ___t0;
		int32_t L_6 = __this->get_numSections_9();
		int32_t L_7 = __this->get_currPt_10();
		V_0 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_5, (float)(((float)((float)L_6))))), (float)(((float)((float)L_7)))));
		Vector3U5BU5D_t1718750761* L_8 = __this->get_pts_4();
		int32_t L_9 = __this->get_currPt_10();
		NullCheck(L_8);
		V_1 = (*(Vector3_t3722313464 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))));
		Vector3U5BU5D_t1718750761* L_10 = __this->get_pts_4();
		int32_t L_11 = __this->get_currPt_10();
		NullCheck(L_10);
		V_2 = (*(Vector3_t3722313464 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1))))));
		Vector3U5BU5D_t1718750761* L_12 = __this->get_pts_4();
		int32_t L_13 = __this->get_currPt_10();
		NullCheck(L_12);
		V_3 = (*(Vector3_t3722313464 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2))))));
		Vector3U5BU5D_t1718750761* L_14 = __this->get_pts_4();
		int32_t L_15 = __this->get_currPt_10();
		NullCheck(L_14);
		V_4 = (*(Vector3_t3722313464 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)3))))));
		Vector3_t3722313464  L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_17 = Vector3_op_UnaryNegation_m1951478815(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = V_2;
		Vector3_t3722313464  L_19 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (3.0f), L_18, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = V_3;
		Vector3_t3722313464  L_22 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (3.0f), L_21, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		Vector3_t3722313464  L_24 = V_4;
		Vector3_t3722313464  L_25 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		float L_26 = V_0;
		float L_27 = V_0;
		float L_28 = V_0;
		Vector3_t3722313464  L_29 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_25, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_26, (float)L_27)), (float)L_28)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_30 = V_1;
		Vector3_t3722313464  L_31 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (2.0f), L_30, /*hidden argument*/NULL);
		Vector3_t3722313464  L_32 = V_2;
		Vector3_t3722313464  L_33 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (5.0f), L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		Vector3_t3722313464  L_35 = V_3;
		Vector3_t3722313464  L_36 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (4.0f), L_35, /*hidden argument*/NULL);
		Vector3_t3722313464  L_37 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_34, L_36, /*hidden argument*/NULL);
		Vector3_t3722313464  L_38 = V_4;
		Vector3_t3722313464  L_39 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		float L_40 = V_0;
		float L_41 = V_0;
		Vector3_t3722313464  L_42 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_39, ((float)il2cpp_codegen_multiply((float)L_40, (float)L_41)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_43 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_29, L_42, /*hidden argument*/NULL);
		Vector3_t3722313464  L_44 = V_1;
		Vector3_t3722313464  L_45 = Vector3_op_UnaryNegation_m1951478815(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Vector3_t3722313464  L_46 = V_3;
		Vector3_t3722313464  L_47 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		float L_48 = V_0;
		Vector3_t3722313464  L_49 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		Vector3_t3722313464  L_50 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_43, L_49, /*hidden argument*/NULL);
		Vector3_t3722313464  L_51 = V_2;
		Vector3_t3722313464  L_52 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (2.0f), L_51, /*hidden argument*/NULL);
		Vector3_t3722313464  L_53 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		Vector3_t3722313464  L_54 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (0.5f), L_53, /*hidden argument*/NULL);
		V_5 = L_54;
		Vector3_t3722313464  L_55 = V_5;
		return L_55;
	}
}
// System.Single LTSpline::ratioAtPoint(UnityEngine.Vector3)
extern "C"  float LTSpline_ratioAtPoint_m1723157471 (LTSpline_t2431306763 * __this, Vector3_t3722313464  ___pt0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_ratioAtPoint_m1723157471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::max());
		V_1 = 0;
		V_2 = 0;
		goto IL_0036;
	}

IL_000f:
	{
		Vector3_t3722313464  L_0 = ___pt0;
		Vector3U5BU5D_t1718750761* L_1 = __this->get_ptsAdj_5();
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_3 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_0, (*(Vector3_t3722313464 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), /*hidden argument*/NULL);
		V_3 = L_3;
		float L_4 = V_3;
		float L_5 = V_0;
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_0032;
		}
	}
	{
		float L_6 = V_3;
		V_0 = L_6;
		int32_t L_7 = V_2;
		V_1 = L_7;
	}

IL_0032:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = __this->get_ptsAdjLength_6();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = __this->get_ptsAdjLength_6();
		return ((float)((float)(((float)((float)L_11)))/(float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1)))))));
	}
}
// UnityEngine.Vector3 LTSpline::point(System.Single)
extern "C"  Vector3_t3722313464  LTSpline_point_m2976918333 (LTSpline_t2431306763 * __this, float ___ratio0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	Vector3_t3722313464  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	{
		float L_0 = ___ratio0;
		if ((!(((float)L_0) > ((float)(1.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_0016;
	}

IL_0015:
	{
		float L_1 = ___ratio0;
		G_B3_0 = L_1;
	}

IL_0016:
	{
		V_0 = G_B3_0;
		bool L_2 = __this->get_constantSpeed_3();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		float L_3 = V_0;
		Vector3_t3722313464  L_4 = LTSpline_map_m1823780591(__this, L_3, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		goto IL_0035;
	}

IL_002e:
	{
		float L_5 = V_0;
		Vector3_t3722313464  L_6 = LTSpline_interp_m2769368846(__this, L_5, /*hidden argument*/NULL);
		G_B6_0 = L_6;
	}

IL_0035:
	{
		return G_B6_0;
	}
}
// System.Void LTSpline::place2d(UnityEngine.Transform,System.Single)
extern "C"  void LTSpline_place2d_m3939880521 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_place2d_m3939880521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Transform_t3600365921 * L_0 = ___transform0;
		float L_1 = ___ratio1;
		Vector3_t3722313464  L_2 = LTSpline_point_m2976918333(__this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3387557959(L_0, L_2, /*hidden argument*/NULL);
		float L_3 = ___ratio1;
		___ratio1 = ((float)il2cpp_codegen_add((float)L_3, (float)(0.001f)));
		float L_4 = ___ratio1;
		if ((!(((float)L_4) <= ((float)(1.0f)))))
		{
			goto IL_0064;
		}
	}
	{
		float L_5 = ___ratio1;
		Vector3_t3722313464  L_6 = LTSpline_point_m2976918333(__this, L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = ___transform0;
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = (&V_0)->get_y_2();
		float L_11 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_12 = atan2f(L_10, L_11);
		V_1 = ((float)il2cpp_codegen_multiply((float)L_12, (float)(57.29578f)));
		Transform_t3600365921 * L_13 = ___transform0;
		float L_14 = V_1;
		Vector3_t3722313464  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector3__ctor_m3353183577((&L_15), (0.0f), (0.0f), L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_eulerAngles_m135219616(L_13, L_15, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void LTSpline::placeLocal2d(UnityEngine.Transform,System.Single)
extern "C"  void LTSpline_placeLocal2d_m1627593543 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_placeLocal2d_m1627593543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Transform_t3600365921 * L_0 = ___transform0;
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_get_parent_m835071599(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t3600365921 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		Transform_t3600365921 * L_4 = ___transform0;
		float L_5 = ___ratio1;
		LTSpline_place2d_m3939880521(__this, L_4, L_5, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Transform_t3600365921 * L_6 = ___transform0;
		float L_7 = ___ratio1;
		Vector3_t3722313464  L_8 = LTSpline_point_m2976918333(__this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m4128471975(L_6, L_8, /*hidden argument*/NULL);
		float L_9 = ___ratio1;
		___ratio1 = ((float)il2cpp_codegen_add((float)L_9, (float)(0.001f)));
		float L_10 = ___ratio1;
		if ((!(((float)L_10) <= ((float)(1.0f)))))
		{
			goto IL_0082;
		}
	}
	{
		float L_11 = ___ratio1;
		Vector3_t3722313464  L_12 = LTSpline_point_m2976918333(__this, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Vector3_t3722313464  L_13 = V_1;
		Transform_t3600365921 * L_14 = ___transform0;
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_localPosition_m4234289348(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_16 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		float L_17 = (&V_2)->get_y_2();
		float L_18 = (&V_2)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_19 = atan2f(L_17, L_18);
		V_3 = ((float)il2cpp_codegen_multiply((float)L_19, (float)(57.29578f)));
		Transform_t3600365921 * L_20 = ___transform0;
		float L_21 = V_3;
		Vector3_t3722313464  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m3353183577((&L_22), (0.0f), (0.0f), L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localEulerAngles_m4202601546(L_20, L_22, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// System.Void LTSpline::place(UnityEngine.Transform,System.Single)
extern "C"  void LTSpline_place_m2468867645 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_place_m2468867645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = ___transform0;
		float L_1 = ___ratio1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		LTSpline_place_m3689574363(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LTSpline::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern "C"  void LTSpline_place_m3689574363 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, Vector3_t3722313464  ___worldUp2, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = ___transform0;
		float L_1 = ___ratio1;
		Vector3_t3722313464  L_2 = LTSpline_point_m2976918333(__this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3387557959(L_0, L_2, /*hidden argument*/NULL);
		float L_3 = ___ratio1;
		___ratio1 = ((float)il2cpp_codegen_add((float)L_3, (float)(0.001f)));
		float L_4 = ___ratio1;
		if ((!(((float)L_4) <= ((float)(1.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		Transform_t3600365921 * L_5 = ___transform0;
		float L_6 = ___ratio1;
		Vector3_t3722313464  L_7 = LTSpline_point_m2976918333(__this, L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = ___worldUp2;
		NullCheck(L_5);
		Transform_LookAt_m3639503211(L_5, L_7, L_8, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single)
extern "C"  void LTSpline_placeLocal_m3314740694 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_placeLocal_m3314740694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = ___transform0;
		float L_1 = ___ratio1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		LTSpline_placeLocal_m3334513255(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern "C"  void LTSpline_placeLocal_m3334513255 (LTSpline_t2431306763 * __this, Transform_t3600365921 * ___transform0, float ___ratio1, Vector3_t3722313464  ___worldUp2, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = ___transform0;
		float L_1 = ___ratio1;
		Vector3_t3722313464  L_2 = LTSpline_point_m2976918333(__this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localPosition_m4128471975(L_0, L_2, /*hidden argument*/NULL);
		float L_3 = ___ratio1;
		___ratio1 = ((float)il2cpp_codegen_add((float)L_3, (float)(0.001f)));
		float L_4 = ___ratio1;
		if ((!(((float)L_4) <= ((float)(1.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		Transform_t3600365921 * L_5 = ___transform0;
		Transform_t3600365921 * L_6 = ___transform0;
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = Transform_get_parent_m835071599(L_6, /*hidden argument*/NULL);
		float L_8 = ___ratio1;
		Vector3_t3722313464  L_9 = LTSpline_point_m2976918333(__this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_10 = Transform_TransformPoint_m226827784(L_7, L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = ___worldUp2;
		NullCheck(L_5);
		Transform_LookAt_m3639503211(L_5, L_10, L_11, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void LTSpline::gizmoDraw(System.Single)
extern "C"  void LTSpline_gizmoDraw_m3110875712 (LTSpline_t2431306763 * __this, float ___t0, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3U5BU5D_t1718750761* L_0 = __this->get_ptsAdj_5();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_1 = __this->get_ptsAdj_5();
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}

IL_0019:
	{
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t1718750761* L_2 = __this->get_ptsAdj_5();
		NullCheck(L_2);
		V_0 = (*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		V_1 = 0;
		goto IL_0052;
	}

IL_0033:
	{
		Vector3U5BU5D_t1718750761* L_3 = __this->get_ptsAdj_5();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		V_2 = (*(Vector3_t3722313464 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4))));
		Vector3_t3722313464  L_5 = V_0;
		Vector3_t3722313464  L_6 = V_2;
		Gizmos_DrawLine_m3273476787(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = V_2;
		V_0 = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0052:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = __this->get_ptsAdjLength_6();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0033;
		}
	}
	{
		return;
	}
}
// System.Void LTSpline::drawGizmo(UnityEngine.Color)
extern "C"  void LTSpline_drawGizmo_m1105876077 (LTSpline_t2431306763 * __this, Color_t2555686324  ___color0, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = __this->get_ptsAdjLength_6();
		if ((((int32_t)L_0) < ((int32_t)4)))
		{
			goto IL_0062;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_1 = __this->get_ptsAdj_5();
		NullCheck(L_1);
		V_0 = (*(Vector3_t3722313464 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Color_t2555686324  L_2 = Gizmos_get_color_m712612415(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		Color_t2555686324  L_3 = ___color0;
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0050;
	}

IL_0031:
	{
		Vector3U5BU5D_t1718750761* L_4 = __this->get_ptsAdj_5();
		int32_t L_5 = V_2;
		NullCheck(L_4);
		V_3 = (*(Vector3_t3722313464 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))));
		Vector3_t3722313464  L_6 = V_0;
		Vector3_t3722313464  L_7 = V_3;
		Gizmos_DrawLine_m3273476787(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = V_3;
		V_0 = L_8;
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0050:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = __this->get_ptsAdjLength_6();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0031;
		}
	}
	{
		Color_t2555686324  L_12 = V_1;
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void LTSpline::drawGizmo(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void LTSpline_drawGizmo_m2577804934 (RuntimeObject * __this /* static, unused */, TransformU5BU5D_t807237628* ___arr0, Color_t2555686324  ___color1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_drawGizmo_m2577804934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	int32_t V_1 = 0;
	LTSpline_t2431306763 * V_2 = NULL;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t2555686324  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t V_5 = 0;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		TransformU5BU5D_t807237628* L_0 = ___arr0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) < ((int32_t)4)))
		{
			goto IL_00a1;
		}
	}
	{
		TransformU5BU5D_t807237628* L_1 = ___arr0;
		NullCheck(L_1);
		V_0 = ((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_0031;
	}

IL_0019:
	{
		Vector3U5BU5D_t1718750761* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		TransformU5BU5D_t807237628* L_4 = ___arr0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Transform_t3600365921 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))) = L_8;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0031:
	{
		int32_t L_10 = V_1;
		TransformU5BU5D_t807237628* L_11 = ___arr0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_12 = V_0;
		LTSpline_t2431306763 * L_13 = (LTSpline_t2431306763 *)il2cpp_codegen_object_new(LTSpline_t2431306763_il2cpp_TypeInfo_var);
		LTSpline__ctor_m3370541708(L_13, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		LTSpline_t2431306763 * L_14 = V_2;
		NullCheck(L_14);
		Vector3U5BU5D_t1718750761* L_15 = L_14->get_ptsAdj_5();
		NullCheck(L_15);
		V_3 = (*(Vector3_t3722313464 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Color_t2555686324  L_16 = Gizmos_get_color_m712612415(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_16;
		Color_t2555686324  L_17 = ___color1;
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_008d;
	}

IL_0068:
	{
		LTSpline_t2431306763 * L_18 = V_2;
		NullCheck(L_18);
		Vector3U5BU5D_t1718750761* L_19 = L_18->get_ptsAdj_5();
		int32_t L_20 = V_5;
		NullCheck(L_19);
		V_6 = (*(Vector3_t3722313464 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20))));
		Vector3_t3722313464  L_21 = V_3;
		Vector3_t3722313464  L_22 = V_6;
		Gizmos_DrawLine_m3273476787(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = V_6;
		V_3 = L_23;
		int32_t L_24 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_008d:
	{
		int32_t L_25 = V_5;
		LTSpline_t2431306763 * L_26 = V_2;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_ptsAdjLength_6();
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_0068;
		}
	}
	{
		Color_t2555686324  L_28 = V_4;
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		return;
	}
}
// System.Void LTSpline::drawLine(UnityEngine.Transform[],System.Single,UnityEngine.Color)
extern "C"  void LTSpline_drawLine_m3309391910 (RuntimeObject * __this /* static, unused */, TransformU5BU5D_t807237628* ___arr0, float ___width1, Color_t2555686324  ___color2, const RuntimeMethod* method)
{
	{
		TransformU5BU5D_t807237628* L_0 = ___arr0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) < ((int32_t)4)))
		{
			goto IL_0009;
		}
	}

IL_0009:
	{
		return;
	}
}
// System.Void LTSpline::drawLinesGLLines(UnityEngine.Material,UnityEngine.Color,System.Single)
extern "C"  void LTSpline_drawLinesGLLines_m2173113263 (LTSpline_t2431306763 * __this, Material_t340375123 * ___outlineMaterial0, Color_t2555686324  ___color1, float ___width2, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		GL_PushMatrix_m1848274883(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t340375123 * L_0 = ___outlineMaterial0;
		NullCheck(L_0);
		Material_SetPass_m686253719(L_0, 0, /*hidden argument*/NULL);
		GL_LoadPixelMatrix_m3084279490(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Begin_m1290681325(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Color_t2555686324  L_1 = ___color1;
		GL_Color_m2127587175(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_constantSpeed_3();
		if (!L_2)
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_3 = __this->get_ptsAdjLength_6();
		if ((((int32_t)L_3) < ((int32_t)4)))
		{
			goto IL_007e;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_4 = __this->get_ptsAdj_5();
		NullCheck(L_4);
		V_0 = (*(Vector3_t3722313464 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		V_1 = 0;
		goto IL_0072;
	}

IL_004e:
	{
		Vector3U5BU5D_t1718750761* L_5 = __this->get_ptsAdj_5();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		V_2 = (*(Vector3_t3722313464 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		Vector3_t3722313464  L_7 = V_0;
		GL_Vertex_m691990801(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = V_2;
		GL_Vertex_m691990801(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = V_2;
		V_0 = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0072:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = __this->get_ptsAdjLength_6();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_004e;
		}
	}

IL_007e:
	{
		goto IL_00fd;
	}

IL_0083:
	{
		Vector3U5BU5D_t1718750761* L_13 = __this->get_pts_4();
		NullCheck(L_13);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))))) < ((int32_t)4)))
		{
			goto IL_00fd;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_14 = __this->get_pts_4();
		NullCheck(L_14);
		V_3 = (*(Vector3_t3722313464 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector3U5BU5D_t1718750761* L_15 = __this->get_pts_4();
		NullCheck(L_15);
		V_4 = ((float)((float)(1.0f)/(float)((float)il2cpp_codegen_multiply((float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length))))))), (float)(10.0f)))));
		V_5 = (0.0f);
		goto IL_00f1;
	}

IL_00c6:
	{
		float L_16 = V_5;
		V_6 = ((float)((float)L_16/(float)(1.0f)));
		float L_17 = V_6;
		Vector3_t3722313464  L_18 = LTSpline_interp_m2769368846(__this, L_17, /*hidden argument*/NULL);
		V_7 = L_18;
		Vector3_t3722313464  L_19 = V_3;
		GL_Vertex_m691990801(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = V_7;
		GL_Vertex_m691990801(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = V_7;
		V_3 = L_21;
		float L_22 = V_5;
		float L_23 = V_4;
		V_5 = ((float)il2cpp_codegen_add((float)L_22, (float)L_23));
	}

IL_00f1:
	{
		float L_24 = V_5;
		if ((((float)L_24) < ((float)(1.0f))))
		{
			goto IL_00c6;
		}
	}

IL_00fd:
	{
		GL_End_m539612367(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m3416050869(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3[] LTSpline::generateVectors()
extern "C"  Vector3U5BU5D_t1718750761* LTSpline_generateVectors_m1739496936 (LTSpline_t2431306763 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline_generateVectors_m1739496936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t899420910 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Vector3U5BU5D_t1718750761* L_0 = __this->get_pts_4();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) < ((int32_t)4)))
		{
			goto IL_007f;
		}
	}
	{
		List_1_t899420910 * L_1 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m2503402603(L_1, /*hidden argument*/List_1__ctor_m2503402603_RuntimeMethod_var);
		V_0 = L_1;
		Vector3U5BU5D_t1718750761* L_2 = __this->get_pts_4();
		NullCheck(L_2);
		V_1 = (*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		List_1_t899420910 * L_3 = V_0;
		Vector3_t3722313464  L_4 = V_1;
		NullCheck(L_3);
		List_1_Add_m1524640104(L_3, L_4, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		Vector3U5BU5D_t1718750761* L_5 = __this->get_pts_4();
		NullCheck(L_5);
		V_2 = ((float)((float)(1.0f)/(float)((float)il2cpp_codegen_multiply((float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length))))))), (float)(10.0f)))));
		V_3 = (0.0f);
		goto IL_006d;
	}

IL_004e:
	{
		float L_6 = V_3;
		V_4 = ((float)((float)L_6/(float)(1.0f)));
		float L_7 = V_4;
		Vector3_t3722313464  L_8 = LTSpline_interp_m2769368846(__this, L_7, /*hidden argument*/NULL);
		V_5 = L_8;
		List_1_t899420910 * L_9 = V_0;
		Vector3_t3722313464  L_10 = V_5;
		NullCheck(L_9);
		List_1_Add_m1524640104(L_9, L_10, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		float L_11 = V_3;
		float L_12 = V_2;
		V_3 = ((float)il2cpp_codegen_add((float)L_11, (float)L_12));
	}

IL_006d:
	{
		float L_13 = V_3;
		if ((((float)L_13) < ((float)(1.0f))))
		{
			goto IL_004e;
		}
	}
	{
		List_1_t899420910 * L_14 = V_0;
		NullCheck(L_14);
		List_1_ToArray_m466715028(L_14, /*hidden argument*/List_1_ToArray_m466715028_RuntimeMethod_var);
	}

IL_007f:
	{
		return (Vector3U5BU5D_t1718750761*)NULL;
	}
}
// System.Void LTSpline::.cctor()
extern "C"  void LTSpline__cctor_m2372119498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LTSpline__cctor_m2372119498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LTSpline_t2431306763_StaticFields*)il2cpp_codegen_static_fields_for(LTSpline_t2431306763_il2cpp_TypeInfo_var))->set_DISTANCE_COUNT_0(3);
		((LTSpline_t2431306763_StaticFields*)il2cpp_codegen_static_fields_for(LTSpline_t2431306763_il2cpp_TypeInfo_var))->set_SUBLINE_COUNT_1(((int32_t)20));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LTUtility::.ctor()
extern "C"  void LTUtility__ctor_m4280826030 (LTUtility_t4124338238 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3[] LTUtility::reverse(UnityEngine.Vector3[])
extern "C"  Vector3U5BU5D_t1718750761* LTUtility_reverse_m2226493195 (RuntimeObject * __this /* static, unused */, Vector3U5BU5D_t1718750761* ___arr0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3U5BU5D_t1718750761* L_0 = ___arr0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))));
		V_1 = 0;
		int32_t L_1 = V_0;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		goto IL_0049;
	}

IL_000f:
	{
		Vector3U5BU5D_t1718750761* L_2 = ___arr0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		V_3 = (*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
		Vector3U5BU5D_t1718750761* L_4 = ___arr0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Vector3U5BU5D_t1718750761* L_6 = ___arr0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		*(Vector3_t3722313464 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))) = (*(Vector3_t3722313464 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7))));
		Vector3U5BU5D_t1718750761* L_8 = ___arr0;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		Vector3_t3722313464  L_10 = V_3;
		*(Vector3_t3722313464 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))) = L_10;
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = V_2;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_15 = ___arr0;
		return L_15;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
