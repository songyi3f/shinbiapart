﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// LTDescr
struct LTDescr_t2043587347;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// GameManager
struct GameManager_t1536523654;
// LeanTester
struct LeanTester_t4080455132;
// System.String
struct String_t;
// LastAttackManager
struct LastAttackManager_t490974840;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// CharmGameManager
struct CharmGameManager_t204340045;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// LTBezier[]
struct LTBezierU5BU5D_t3652571610;
// UnityEngine.Object
struct Object_t631007953;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// System.Type
struct Type_t;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// GhostManager
struct GhostManager_t3188635936;
// LobbyManager
struct LobbyManager_t4136450456;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// SaveLoadManager
struct SaveLoadManager_t4010657880;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String[]
struct StringU5BU5D_t1281789340;
// LTRect[]
struct LTRectU5BU5D_t2099225848;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554;
// LTRect
struct LTRect_t2883103509;
// UnityEngine.Transform
struct Transform_t3600365921;
// LTBezierPath
struct LTBezierPath_t1817657086;
// LTSpline
struct LTSpline_t2431306763;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Action`2<System.Single,System.Single>
struct Action_2_t3683108670;
// System.Action`2<System.Single,System.Object>
struct Action_2_t1070980764;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2328697118;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3894781059;
// System.Action`2<UnityEngine.Vector3,System.Object>
struct Action_2_t1344820274;
// System.Action`1<UnityEngine.Color>
struct Action_1_t2728153919;
// System.Action`2<UnityEngine.Color,System.Object>
struct Action_2_t567985926;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// UnityEngine.Collider
struct Collider_t1773347010;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// LeanAudioStream
struct LeanAudioStream_t2242826187;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// LTDescr/EaseTypeDelegate
struct EaseTypeDelegate_t2201194898;
// LTDescr/ActionMethodDelegate
struct ActionMethodDelegate_t2422926000;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// LTDescrOptional
struct LTDescrOptional_t4257087022;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Sprite
struct Sprite_t280657092;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t3748144825;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.Collections.Generic.Stack`1<UnityEngine.GameObject>
struct Stack_1_t1957026074;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// CharmTrigger[]
struct CharmTriggerU5BU5D_t1898505999;
// RemainCharmUI
struct RemainCharmUI_t475548631;
// GhostInfoUI
struct GhostInfoUI_t4215450973;
// ResultUI
struct ResultUI_t719777132;
// ChangeCharmPopup
struct ChangeCharmPopup_t1980703935;
// System.Func`2<CharmTrigger,System.Boolean>
struct Func_2_t2813846145;
// Sound[]
struct SoundU5BU5D_t3647937991;
// LightSensorPluginScript
struct LightSensorPluginScript_t2582954711;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t3693186521;
// LTSeq[]
struct LTSeqU5BU5D_t1547789195;
// LTDescr[]
struct LTDescrU5BU5D_t547118914;
// System.Action`1<LTEvent>[]
struct Action_1U5BU5D_t1829790072;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2165061829;
// TimeManager
struct TimeManager_t1960693005;
// ItemCharmUI
struct ItemCharmUI_t437376365;
// FreeItemCharmUI
struct FreeItemCharmUI_t3416008829;
// UnityEngine.Gyroscope
struct Gyroscope_t3288342876;
// Ghost[]
struct GhostU5BU5D_t3381597533;
// Ghost
struct Ghost_t201992404;
// GhostInfo
struct GhostInfo_t3310990554;
// System.Action`1<GhostSchoolPopup>
struct Action_1_t1627743448;
// GyroManager
struct GyroManager_t2156710008;
// Charm[]
struct CharmU5BU5D_t2980613126;




#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef U3CMODULEU3E_T692745557_H
#define U3CMODULEU3E_T692745557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745557 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745557_H
#ifndef LTSEQ_T1322000318_H
#define LTSEQ_T1322000318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTSeq
struct  LTSeq_t1322000318  : public RuntimeObject
{
public:
	// LTSeq LTSeq::previous
	LTSeq_t1322000318 * ___previous_0;
	// LTSeq LTSeq::current
	LTSeq_t1322000318 * ___current_1;
	// LTDescr LTSeq::tween
	LTDescr_t2043587347 * ___tween_2;
	// System.Single LTSeq::totalDelay
	float ___totalDelay_3;
	// System.Single LTSeq::timeScale
	float ___timeScale_4;
	// System.Int32 LTSeq::debugIter
	int32_t ___debugIter_5;
	// System.UInt32 LTSeq::counter
	uint32_t ___counter_6;
	// System.Boolean LTSeq::toggle
	bool ___toggle_7;
	// System.UInt32 LTSeq::_id
	uint32_t ____id_8;

public:
	inline static int32_t get_offset_of_previous_0() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___previous_0)); }
	inline LTSeq_t1322000318 * get_previous_0() const { return ___previous_0; }
	inline LTSeq_t1322000318 ** get_address_of_previous_0() { return &___previous_0; }
	inline void set_previous_0(LTSeq_t1322000318 * value)
	{
		___previous_0 = value;
		Il2CppCodeGenWriteBarrier((&___previous_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___current_1)); }
	inline LTSeq_t1322000318 * get_current_1() const { return ___current_1; }
	inline LTSeq_t1322000318 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(LTSeq_t1322000318 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_tween_2() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___tween_2)); }
	inline LTDescr_t2043587347 * get_tween_2() const { return ___tween_2; }
	inline LTDescr_t2043587347 ** get_address_of_tween_2() { return &___tween_2; }
	inline void set_tween_2(LTDescr_t2043587347 * value)
	{
		___tween_2 = value;
		Il2CppCodeGenWriteBarrier((&___tween_2), value);
	}

	inline static int32_t get_offset_of_totalDelay_3() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___totalDelay_3)); }
	inline float get_totalDelay_3() const { return ___totalDelay_3; }
	inline float* get_address_of_totalDelay_3() { return &___totalDelay_3; }
	inline void set_totalDelay_3(float value)
	{
		___totalDelay_3 = value;
	}

	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_debugIter_5() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___debugIter_5)); }
	inline int32_t get_debugIter_5() const { return ___debugIter_5; }
	inline int32_t* get_address_of_debugIter_5() { return &___debugIter_5; }
	inline void set_debugIter_5(int32_t value)
	{
		___debugIter_5 = value;
	}

	inline static int32_t get_offset_of_counter_6() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___counter_6)); }
	inline uint32_t get_counter_6() const { return ___counter_6; }
	inline uint32_t* get_address_of_counter_6() { return &___counter_6; }
	inline void set_counter_6(uint32_t value)
	{
		___counter_6 = value;
	}

	inline static int32_t get_offset_of_toggle_7() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ___toggle_7)); }
	inline bool get_toggle_7() const { return ___toggle_7; }
	inline bool* get_address_of_toggle_7() { return &___toggle_7; }
	inline void set_toggle_7(bool value)
	{
		___toggle_7 = value;
	}

	inline static int32_t get_offset_of__id_8() { return static_cast<int32_t>(offsetof(LTSeq_t1322000318, ____id_8)); }
	inline uint32_t get__id_8() const { return ____id_8; }
	inline uint32_t* get_address_of__id_8() { return &____id_8; }
	inline void set__id_8(uint32_t value)
	{
		____id_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTSEQ_T1322000318_H
#ifndef U3CTURNONVR_AREAU3EC__ITERATOR3_T2506130967_H
#define U3CTURNONVR_AREAU3EC__ITERATOR3_T2506130967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<TurnOnVR_Area>c__Iterator3
struct  U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameManager/<TurnOnVR_Area>c__Iterator3::<newMonster>__0
	GameObject_t1113636619 * ___U3CnewMonsterU3E__0_0;
	// GameManager GameManager/<TurnOnVR_Area>c__Iterator3::$this
	GameManager_t1536523654 * ___U24this_1;
	// System.Object GameManager/<TurnOnVR_Area>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameManager/<TurnOnVR_Area>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameManager/<TurnOnVR_Area>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CnewMonsterU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967, ___U3CnewMonsterU3E__0_0)); }
	inline GameObject_t1113636619 * get_U3CnewMonsterU3E__0_0() const { return ___U3CnewMonsterU3E__0_0; }
	inline GameObject_t1113636619 ** get_address_of_U3CnewMonsterU3E__0_0() { return &___U3CnewMonsterU3E__0_0; }
	inline void set_U3CnewMonsterU3E__0_0(GameObject_t1113636619 * value)
	{
		___U3CnewMonsterU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewMonsterU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967, ___U24this_1)); }
	inline GameManager_t1536523654 * get_U24this_1() const { return ___U24this_1; }
	inline GameManager_t1536523654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameManager_t1536523654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTURNONVR_AREAU3EC__ITERATOR3_T2506130967_H
#ifndef U3CSTARTVRMODEU3EC__ITERATOR4_T1952977010_H
#define U3CSTARTVRMODEU3EC__ITERATOR4_T1952977010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<StartVRMode>c__Iterator4
struct  U3CStartVRModeU3Ec__Iterator4_t1952977010  : public RuntimeObject
{
public:
	// System.Single GameManager/<StartVRMode>c__Iterator4::waitTime
	float ___waitTime_0;
	// GameManager GameManager/<StartVRMode>c__Iterator4::$this
	GameManager_t1536523654 * ___U24this_1;
	// System.Object GameManager/<StartVRMode>c__Iterator4::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameManager/<StartVRMode>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameManager/<StartVRMode>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(U3CStartVRModeU3Ec__Iterator4_t1952977010, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartVRModeU3Ec__Iterator4_t1952977010, ___U24this_1)); }
	inline GameManager_t1536523654 * get_U24this_1() const { return ___U24this_1; }
	inline GameManager_t1536523654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameManager_t1536523654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartVRModeU3Ec__Iterator4_t1952977010, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartVRModeU3Ec__Iterator4_t1952977010, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartVRModeU3Ec__Iterator4_t1952977010, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTVRMODEU3EC__ITERATOR4_T1952977010_H
#ifndef LEANTEST_T591031114_H
#define LEANTEST_T591031114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanTest
struct  LeanTest_t591031114  : public RuntimeObject
{
public:

public:
};

struct LeanTest_t591031114_StaticFields
{
public:
	// System.Int32 LeanTest::expected
	int32_t ___expected_0;
	// System.Int32 LeanTest::tests
	int32_t ___tests_1;
	// System.Int32 LeanTest::passes
	int32_t ___passes_2;
	// System.Single LeanTest::timeout
	float ___timeout_3;
	// System.Boolean LeanTest::timeoutStarted
	bool ___timeoutStarted_4;
	// System.Boolean LeanTest::testsFinished
	bool ___testsFinished_5;

public:
	inline static int32_t get_offset_of_expected_0() { return static_cast<int32_t>(offsetof(LeanTest_t591031114_StaticFields, ___expected_0)); }
	inline int32_t get_expected_0() const { return ___expected_0; }
	inline int32_t* get_address_of_expected_0() { return &___expected_0; }
	inline void set_expected_0(int32_t value)
	{
		___expected_0 = value;
	}

	inline static int32_t get_offset_of_tests_1() { return static_cast<int32_t>(offsetof(LeanTest_t591031114_StaticFields, ___tests_1)); }
	inline int32_t get_tests_1() const { return ___tests_1; }
	inline int32_t* get_address_of_tests_1() { return &___tests_1; }
	inline void set_tests_1(int32_t value)
	{
		___tests_1 = value;
	}

	inline static int32_t get_offset_of_passes_2() { return static_cast<int32_t>(offsetof(LeanTest_t591031114_StaticFields, ___passes_2)); }
	inline int32_t get_passes_2() const { return ___passes_2; }
	inline int32_t* get_address_of_passes_2() { return &___passes_2; }
	inline void set_passes_2(int32_t value)
	{
		___passes_2 = value;
	}

	inline static int32_t get_offset_of_timeout_3() { return static_cast<int32_t>(offsetof(LeanTest_t591031114_StaticFields, ___timeout_3)); }
	inline float get_timeout_3() const { return ___timeout_3; }
	inline float* get_address_of_timeout_3() { return &___timeout_3; }
	inline void set_timeout_3(float value)
	{
		___timeout_3 = value;
	}

	inline static int32_t get_offset_of_timeoutStarted_4() { return static_cast<int32_t>(offsetof(LeanTest_t591031114_StaticFields, ___timeoutStarted_4)); }
	inline bool get_timeoutStarted_4() const { return ___timeoutStarted_4; }
	inline bool* get_address_of_timeoutStarted_4() { return &___timeoutStarted_4; }
	inline void set_timeoutStarted_4(bool value)
	{
		___timeoutStarted_4 = value;
	}

	inline static int32_t get_offset_of_testsFinished_5() { return static_cast<int32_t>(offsetof(LeanTest_t591031114_StaticFields, ___testsFinished_5)); }
	inline bool get_testsFinished_5() const { return ___testsFinished_5; }
	inline bool* get_address_of_testsFinished_5() { return &___testsFinished_5; }
	inline void set_testsFinished_5(bool value)
	{
		___testsFinished_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTEST_T591031114_H
#ifndef U3CTIMEOUTCHECKU3EC__ITERATOR0_T3914164071_H
#define U3CTIMEOUTCHECKU3EC__ITERATOR0_T3914164071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanTester/<timeoutCheck>c__Iterator0
struct  U3CtimeoutCheckU3Ec__Iterator0_t3914164071  : public RuntimeObject
{
public:
	// System.Single LeanTester/<timeoutCheck>c__Iterator0::<pauseEndTime>__0
	float ___U3CpauseEndTimeU3E__0_0;
	// LeanTester LeanTester/<timeoutCheck>c__Iterator0::$this
	LeanTester_t4080455132 * ___U24this_1;
	// System.Object LeanTester/<timeoutCheck>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LeanTester/<timeoutCheck>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 LeanTester/<timeoutCheck>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CpauseEndTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ec__Iterator0_t3914164071, ___U3CpauseEndTimeU3E__0_0)); }
	inline float get_U3CpauseEndTimeU3E__0_0() const { return ___U3CpauseEndTimeU3E__0_0; }
	inline float* get_address_of_U3CpauseEndTimeU3E__0_0() { return &___U3CpauseEndTimeU3E__0_0; }
	inline void set_U3CpauseEndTimeU3E__0_0(float value)
	{
		___U3CpauseEndTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ec__Iterator0_t3914164071, ___U24this_1)); }
	inline LeanTester_t4080455132 * get_U24this_1() const { return ___U24this_1; }
	inline LeanTester_t4080455132 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LeanTester_t4080455132 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ec__Iterator0_t3914164071, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ec__Iterator0_t3914164071, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ec__Iterator0_t3914164071, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEOUTCHECKU3EC__ITERATOR0_T3914164071_H
#ifndef U3CSTARTCHARMGAMEU3EC__ITERATOR5_T2645365450_H
#define U3CSTARTCHARMGAMEU3EC__ITERATOR5_T2645365450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<StartCharmGame>c__Iterator5
struct  U3CStartCharmGameU3Ec__Iterator5_t2645365450  : public RuntimeObject
{
public:
	// GameManager GameManager/<StartCharmGame>c__Iterator5::$this
	GameManager_t1536523654 * ___U24this_0;
	// System.Object GameManager/<StartCharmGame>c__Iterator5::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameManager/<StartCharmGame>c__Iterator5::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameManager/<StartCharmGame>c__Iterator5::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartCharmGameU3Ec__Iterator5_t2645365450, ___U24this_0)); }
	inline GameManager_t1536523654 * get_U24this_0() const { return ___U24this_0; }
	inline GameManager_t1536523654 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManager_t1536523654 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartCharmGameU3Ec__Iterator5_t2645365450, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartCharmGameU3Ec__Iterator5_t2645365450, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartCharmGameU3Ec__Iterator5_t2645365450, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTCHARMGAMEU3EC__ITERATOR5_T2645365450_H
#ifndef U3CMONSTERSOUNDU3EC__ITERATOR0_T1078325540_H
#define U3CMONSTERSOUNDU3EC__ITERATOR0_T1078325540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostManager/<MonsterSound>c__Iterator0
struct  U3CMonsterSoundU3Ec__Iterator0_t1078325540  : public RuntimeObject
{
public:
	// System.String GhostManager/<MonsterSound>c__Iterator0::<clipName>__1
	String_t* ___U3CclipNameU3E__1_0;
	// System.Object GhostManager/<MonsterSound>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GhostManager/<MonsterSound>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GhostManager/<MonsterSound>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U3CclipNameU3E__1_0() { return static_cast<int32_t>(offsetof(U3CMonsterSoundU3Ec__Iterator0_t1078325540, ___U3CclipNameU3E__1_0)); }
	inline String_t* get_U3CclipNameU3E__1_0() const { return ___U3CclipNameU3E__1_0; }
	inline String_t** get_address_of_U3CclipNameU3E__1_0() { return &___U3CclipNameU3E__1_0; }
	inline void set_U3CclipNameU3E__1_0(String_t* value)
	{
		___U3CclipNameU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclipNameU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CMonsterSoundU3Ec__Iterator0_t1078325540, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CMonsterSoundU3Ec__Iterator0_t1078325540, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CMonsterSoundU3Ec__Iterator0_t1078325540, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMONSTERSOUNDU3EC__ITERATOR0_T1078325540_H
#ifndef U3CSTARTANIMATIONU3EC__ITERATOR0_T4019716844_H
#define U3CSTARTANIMATIONU3EC__ITERATOR0_T4019716844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LastAttackManager/<StartAnimation>c__Iterator0
struct  U3CStartAnimationU3Ec__Iterator0_t4019716844  : public RuntimeObject
{
public:
	// LastAttackManager LastAttackManager/<StartAnimation>c__Iterator0::$this
	LastAttackManager_t490974840 * ___U24this_0;
	// System.Object LastAttackManager/<StartAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean LastAttackManager/<StartAnimation>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 LastAttackManager/<StartAnimation>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__Iterator0_t4019716844, ___U24this_0)); }
	inline LastAttackManager_t490974840 * get_U24this_0() const { return ___U24this_0; }
	inline LastAttackManager_t490974840 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LastAttackManager_t490974840 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__Iterator0_t4019716844, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__Iterator0_t4019716844, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__Iterator0_t4019716844, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTANIMATIONU3EC__ITERATOR0_T4019716844_H
#ifndef LEANAUDIO_T998158884_H
#define LEANAUDIO_T998158884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanAudio
struct  LeanAudio_t998158884  : public RuntimeObject
{
public:

public:
};

struct LeanAudio_t998158884_StaticFields
{
public:
	// System.Single LeanAudio::MIN_FREQEUNCY_PERIOD
	float ___MIN_FREQEUNCY_PERIOD_0;
	// System.Int32 LeanAudio::PROCESSING_ITERATIONS_MAX
	int32_t ___PROCESSING_ITERATIONS_MAX_1;
	// System.Single[] LeanAudio::generatedWaveDistances
	SingleU5BU5D_t1444911251* ___generatedWaveDistances_2;
	// System.Int32 LeanAudio::generatedWaveDistancesCount
	int32_t ___generatedWaveDistancesCount_3;
	// System.Single[] LeanAudio::longList
	SingleU5BU5D_t1444911251* ___longList_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback LeanAudio::<>f__mg$cache0
	PCMSetPositionCallback_t1059417452 * ___U3CU3Ef__mgU24cache0_5;

public:
	inline static int32_t get_offset_of_MIN_FREQEUNCY_PERIOD_0() { return static_cast<int32_t>(offsetof(LeanAudio_t998158884_StaticFields, ___MIN_FREQEUNCY_PERIOD_0)); }
	inline float get_MIN_FREQEUNCY_PERIOD_0() const { return ___MIN_FREQEUNCY_PERIOD_0; }
	inline float* get_address_of_MIN_FREQEUNCY_PERIOD_0() { return &___MIN_FREQEUNCY_PERIOD_0; }
	inline void set_MIN_FREQEUNCY_PERIOD_0(float value)
	{
		___MIN_FREQEUNCY_PERIOD_0 = value;
	}

	inline static int32_t get_offset_of_PROCESSING_ITERATIONS_MAX_1() { return static_cast<int32_t>(offsetof(LeanAudio_t998158884_StaticFields, ___PROCESSING_ITERATIONS_MAX_1)); }
	inline int32_t get_PROCESSING_ITERATIONS_MAX_1() const { return ___PROCESSING_ITERATIONS_MAX_1; }
	inline int32_t* get_address_of_PROCESSING_ITERATIONS_MAX_1() { return &___PROCESSING_ITERATIONS_MAX_1; }
	inline void set_PROCESSING_ITERATIONS_MAX_1(int32_t value)
	{
		___PROCESSING_ITERATIONS_MAX_1 = value;
	}

	inline static int32_t get_offset_of_generatedWaveDistances_2() { return static_cast<int32_t>(offsetof(LeanAudio_t998158884_StaticFields, ___generatedWaveDistances_2)); }
	inline SingleU5BU5D_t1444911251* get_generatedWaveDistances_2() const { return ___generatedWaveDistances_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_generatedWaveDistances_2() { return &___generatedWaveDistances_2; }
	inline void set_generatedWaveDistances_2(SingleU5BU5D_t1444911251* value)
	{
		___generatedWaveDistances_2 = value;
		Il2CppCodeGenWriteBarrier((&___generatedWaveDistances_2), value);
	}

	inline static int32_t get_offset_of_generatedWaveDistancesCount_3() { return static_cast<int32_t>(offsetof(LeanAudio_t998158884_StaticFields, ___generatedWaveDistancesCount_3)); }
	inline int32_t get_generatedWaveDistancesCount_3() const { return ___generatedWaveDistancesCount_3; }
	inline int32_t* get_address_of_generatedWaveDistancesCount_3() { return &___generatedWaveDistancesCount_3; }
	inline void set_generatedWaveDistancesCount_3(int32_t value)
	{
		___generatedWaveDistancesCount_3 = value;
	}

	inline static int32_t get_offset_of_longList_4() { return static_cast<int32_t>(offsetof(LeanAudio_t998158884_StaticFields, ___longList_4)); }
	inline SingleU5BU5D_t1444911251* get_longList_4() const { return ___longList_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_longList_4() { return &___longList_4; }
	inline void set_longList_4(SingleU5BU5D_t1444911251* value)
	{
		___longList_4 = value;
		Il2CppCodeGenWriteBarrier((&___longList_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(LeanAudio_t998158884_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline PCMSetPositionCallback_t1059417452 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(PCMSetPositionCallback_t1059417452 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANAUDIO_T998158884_H
#ifndef LEANAUDIOSTREAM_T2242826187_H
#define LEANAUDIOSTREAM_T2242826187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanAudioStream
struct  LeanAudioStream_t2242826187  : public RuntimeObject
{
public:
	// System.Int32 LeanAudioStream::position
	int32_t ___position_0;
	// UnityEngine.AudioClip LeanAudioStream::audioClip
	AudioClip_t3680889665 * ___audioClip_1;
	// System.Single[] LeanAudioStream::audioArr
	SingleU5BU5D_t1444911251* ___audioArr_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(LeanAudioStream_t2242826187, ___position_0)); }
	inline int32_t get_position_0() const { return ___position_0; }
	inline int32_t* get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(int32_t value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_audioClip_1() { return static_cast<int32_t>(offsetof(LeanAudioStream_t2242826187, ___audioClip_1)); }
	inline AudioClip_t3680889665 * get_audioClip_1() const { return ___audioClip_1; }
	inline AudioClip_t3680889665 ** get_address_of_audioClip_1() { return &___audioClip_1; }
	inline void set_audioClip_1(AudioClip_t3680889665 * value)
	{
		___audioClip_1 = value;
		Il2CppCodeGenWriteBarrier((&___audioClip_1), value);
	}

	inline static int32_t get_offset_of_audioArr_2() { return static_cast<int32_t>(offsetof(LeanAudioStream_t2242826187, ___audioArr_2)); }
	inline SingleU5BU5D_t1444911251* get_audioArr_2() const { return ___audioArr_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_audioArr_2() { return &___audioArr_2; }
	inline void set_audioArr_2(SingleU5BU5D_t1444911251* value)
	{
		___audioArr_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioArr_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANAUDIOSTREAM_T2242826187_H
#ifndef U3CSTARTGAMEU3EC__ITERATOR1_T814519062_H
#define U3CSTARTGAMEU3EC__ITERATOR1_T814519062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<StartGame>c__Iterator1
struct  U3CStartGameU3Ec__Iterator1_t814519062  : public RuntimeObject
{
public:
	// GameManager GameManager/<StartGame>c__Iterator1::$this
	GameManager_t1536523654 * ___U24this_0;
	// System.Object GameManager/<StartGame>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameManager/<StartGame>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameManager/<StartGame>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator1_t814519062, ___U24this_0)); }
	inline GameManager_t1536523654 * get_U24this_0() const { return ___U24this_0; }
	inline GameManager_t1536523654 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManager_t1536523654 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator1_t814519062, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator1_t814519062, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator1_t814519062, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTGAMEU3EC__ITERATOR1_T814519062_H
#ifndef U3CPLAYU3EC__ANONSTOREY0_T2108231134_H
#define U3CPLAYU3EC__ANONSTOREY0_T2108231134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/<Play>c__AnonStorey0
struct  U3CPlayU3Ec__AnonStorey0_t2108231134  : public RuntimeObject
{
public:
	// System.String AudioManager/<Play>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__AnonStorey0_t2108231134, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYU3EC__ANONSTOREY0_T2108231134_H
#ifndef U3CSTOPU3EC__ANONSTOREY1_T2155610042_H
#define U3CSTOPU3EC__ANONSTOREY1_T2155610042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/<Stop>c__AnonStorey1
struct  U3CStopU3Ec__AnonStorey1_t2155610042  : public RuntimeObject
{
public:
	// System.String AudioManager/<Stop>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CStopU3Ec__AnonStorey1_t2155610042, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPU3EC__ANONSTOREY1_T2155610042_H
#ifndef U3CGAMETIMEU3EC__ITERATOR0_T320282633_H
#define U3CGAMETIMEU3EC__ITERATOR0_T320282633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameManager/<GameTime>c__Iterator0
struct  U3CGameTimeU3Ec__Iterator0_t320282633  : public RuntimeObject
{
public:
	// System.Int32 CharmGameManager/<GameTime>c__Iterator0::<time>__0
	int32_t ___U3CtimeU3E__0_0;
	// CharmGameManager CharmGameManager/<GameTime>c__Iterator0::$this
	CharmGameManager_t204340045 * ___U24this_1;
	// System.Object CharmGameManager/<GameTime>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CharmGameManager/<GameTime>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CharmGameManager/<GameTime>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGameTimeU3Ec__Iterator0_t320282633, ___U3CtimeU3E__0_0)); }
	inline int32_t get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline int32_t* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(int32_t value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGameTimeU3Ec__Iterator0_t320282633, ___U24this_1)); }
	inline CharmGameManager_t204340045 * get_U24this_1() const { return ___U24this_1; }
	inline CharmGameManager_t204340045 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CharmGameManager_t204340045 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGameTimeU3Ec__Iterator0_t320282633, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGameTimeU3Ec__Iterator0_t320282633, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGameTimeU3Ec__Iterator0_t320282633, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGAMETIMEU3EC__ITERATOR0_T320282633_H
#ifndef LEANDUMMY_T4064590342_H
#define LEANDUMMY_T4064590342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DentedPixel.LeanDummy
struct  LeanDummy_t4064590342  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANDUMMY_T4064590342_H
#ifndef U3CPAUSEPOINTERU3EC__ANONSTOREY4_T722434170_H
#define U3CPAUSEPOINTERU3EC__ANONSTOREY4_T722434170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameManager/<PausePointer>c__AnonStorey4
struct  U3CPausePointerU3Ec__AnonStorey4_t722434170  : public RuntimeObject
{
public:
	// UnityEngine.GameObject CharmGameManager/<PausePointer>c__AnonStorey4::flyCharm
	GameObject_t1113636619 * ___flyCharm_0;
	// CharmGameManager CharmGameManager/<PausePointer>c__AnonStorey4::$this
	CharmGameManager_t204340045 * ___U24this_1;

public:
	inline static int32_t get_offset_of_flyCharm_0() { return static_cast<int32_t>(offsetof(U3CPausePointerU3Ec__AnonStorey4_t722434170, ___flyCharm_0)); }
	inline GameObject_t1113636619 * get_flyCharm_0() const { return ___flyCharm_0; }
	inline GameObject_t1113636619 ** get_address_of_flyCharm_0() { return &___flyCharm_0; }
	inline void set_flyCharm_0(GameObject_t1113636619 * value)
	{
		___flyCharm_0 = value;
		Il2CppCodeGenWriteBarrier((&___flyCharm_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPausePointerU3Ec__AnonStorey4_t722434170, ___U24this_1)); }
	inline CharmGameManager_t204340045 * get_U24this_1() const { return ___U24this_1; }
	inline CharmGameManager_t204340045 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CharmGameManager_t204340045 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPAUSEPOINTERU3EC__ANONSTOREY4_T722434170_H
#ifndef U3CBURNANIMATIONU3EC__ITERATOR2_T3560078157_H
#define U3CBURNANIMATIONU3EC__ITERATOR2_T3560078157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameManager/<BurnAnimation>c__Iterator2
struct  U3CBurnAnimationU3Ec__Iterator2_t3560078157  : public RuntimeObject
{
public:
	// UnityEngine.GameObject CharmGameManager/<BurnAnimation>c__Iterator2::flyCharm
	GameObject_t1113636619 * ___flyCharm_0;
	// System.Object CharmGameManager/<BurnAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CharmGameManager/<BurnAnimation>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 CharmGameManager/<BurnAnimation>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_flyCharm_0() { return static_cast<int32_t>(offsetof(U3CBurnAnimationU3Ec__Iterator2_t3560078157, ___flyCharm_0)); }
	inline GameObject_t1113636619 * get_flyCharm_0() const { return ___flyCharm_0; }
	inline GameObject_t1113636619 ** get_address_of_flyCharm_0() { return &___flyCharm_0; }
	inline void set_flyCharm_0(GameObject_t1113636619 * value)
	{
		___flyCharm_0 = value;
		Il2CppCodeGenWriteBarrier((&___flyCharm_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CBurnAnimationU3Ec__Iterator2_t3560078157, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CBurnAnimationU3Ec__Iterator2_t3560078157, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CBurnAnimationU3Ec__Iterator2_t3560078157, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBURNANIMATIONU3EC__ITERATOR2_T3560078157_H
#ifndef LTEVENT_T176071434_H
#define LTEVENT_T176071434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTEvent
struct  LTEvent_t176071434  : public RuntimeObject
{
public:
	// System.Int32 LTEvent::id
	int32_t ___id_0;
	// System.Object LTEvent::data
	RuntimeObject * ___data_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LTEvent_t176071434, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(LTEvent_t176071434, ___data_1)); }
	inline RuntimeObject * get_data_1() const { return ___data_1; }
	inline RuntimeObject ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RuntimeObject * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTEVENT_T176071434_H
#ifndef U3CDAMAGEMONSTERU3EC__ITERATOR3_T1032243821_H
#define U3CDAMAGEMONSTERU3EC__ITERATOR3_T1032243821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameManager/<DamageMonster>c__Iterator3
struct  U3CDamageMonsterU3Ec__Iterator3_t1032243821  : public RuntimeObject
{
public:
	// System.Int32 CharmGameManager/<DamageMonster>c__Iterator3::damageValue
	int32_t ___damageValue_0;
	// System.Single CharmGameManager/<DamageMonster>c__Iterator3::<hp>__0
	float ___U3ChpU3E__0_1;
	// CharmGameManager CharmGameManager/<DamageMonster>c__Iterator3::$this
	CharmGameManager_t204340045 * ___U24this_2;
	// System.Object CharmGameManager/<DamageMonster>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CharmGameManager/<DamageMonster>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 CharmGameManager/<DamageMonster>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_damageValue_0() { return static_cast<int32_t>(offsetof(U3CDamageMonsterU3Ec__Iterator3_t1032243821, ___damageValue_0)); }
	inline int32_t get_damageValue_0() const { return ___damageValue_0; }
	inline int32_t* get_address_of_damageValue_0() { return &___damageValue_0; }
	inline void set_damageValue_0(int32_t value)
	{
		___damageValue_0 = value;
	}

	inline static int32_t get_offset_of_U3ChpU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDamageMonsterU3Ec__Iterator3_t1032243821, ___U3ChpU3E__0_1)); }
	inline float get_U3ChpU3E__0_1() const { return ___U3ChpU3E__0_1; }
	inline float* get_address_of_U3ChpU3E__0_1() { return &___U3ChpU3E__0_1; }
	inline void set_U3ChpU3E__0_1(float value)
	{
		___U3ChpU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDamageMonsterU3Ec__Iterator3_t1032243821, ___U24this_2)); }
	inline CharmGameManager_t204340045 * get_U24this_2() const { return ___U24this_2; }
	inline CharmGameManager_t204340045 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CharmGameManager_t204340045 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDamageMonsterU3Ec__Iterator3_t1032243821, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDamageMonsterU3Ec__Iterator3_t1032243821, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDamageMonsterU3Ec__Iterator3_t1032243821, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDAMAGEMONSTERU3EC__ITERATOR3_T1032243821_H
#ifndef LTSPLINE_T2431306763_H
#define LTSPLINE_T2431306763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTSpline
struct  LTSpline_t2431306763  : public RuntimeObject
{
public:
	// System.Single LTSpline::distance
	float ___distance_2;
	// System.Boolean LTSpline::constantSpeed
	bool ___constantSpeed_3;
	// UnityEngine.Vector3[] LTSpline::pts
	Vector3U5BU5D_t1718750761* ___pts_4;
	// UnityEngine.Vector3[] LTSpline::ptsAdj
	Vector3U5BU5D_t1718750761* ___ptsAdj_5;
	// System.Int32 LTSpline::ptsAdjLength
	int32_t ___ptsAdjLength_6;
	// System.Boolean LTSpline::orientToPath
	bool ___orientToPath_7;
	// System.Boolean LTSpline::orientToPath2d
	bool ___orientToPath2d_8;
	// System.Int32 LTSpline::numSections
	int32_t ___numSections_9;
	// System.Int32 LTSpline::currPt
	int32_t ___currPt_10;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_3() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___constantSpeed_3)); }
	inline bool get_constantSpeed_3() const { return ___constantSpeed_3; }
	inline bool* get_address_of_constantSpeed_3() { return &___constantSpeed_3; }
	inline void set_constantSpeed_3(bool value)
	{
		___constantSpeed_3 = value;
	}

	inline static int32_t get_offset_of_pts_4() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___pts_4)); }
	inline Vector3U5BU5D_t1718750761* get_pts_4() const { return ___pts_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pts_4() { return &___pts_4; }
	inline void set_pts_4(Vector3U5BU5D_t1718750761* value)
	{
		___pts_4 = value;
		Il2CppCodeGenWriteBarrier((&___pts_4), value);
	}

	inline static int32_t get_offset_of_ptsAdj_5() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___ptsAdj_5)); }
	inline Vector3U5BU5D_t1718750761* get_ptsAdj_5() const { return ___ptsAdj_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_ptsAdj_5() { return &___ptsAdj_5; }
	inline void set_ptsAdj_5(Vector3U5BU5D_t1718750761* value)
	{
		___ptsAdj_5 = value;
		Il2CppCodeGenWriteBarrier((&___ptsAdj_5), value);
	}

	inline static int32_t get_offset_of_ptsAdjLength_6() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___ptsAdjLength_6)); }
	inline int32_t get_ptsAdjLength_6() const { return ___ptsAdjLength_6; }
	inline int32_t* get_address_of_ptsAdjLength_6() { return &___ptsAdjLength_6; }
	inline void set_ptsAdjLength_6(int32_t value)
	{
		___ptsAdjLength_6 = value;
	}

	inline static int32_t get_offset_of_orientToPath_7() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___orientToPath_7)); }
	inline bool get_orientToPath_7() const { return ___orientToPath_7; }
	inline bool* get_address_of_orientToPath_7() { return &___orientToPath_7; }
	inline void set_orientToPath_7(bool value)
	{
		___orientToPath_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_8() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___orientToPath2d_8)); }
	inline bool get_orientToPath2d_8() const { return ___orientToPath2d_8; }
	inline bool* get_address_of_orientToPath2d_8() { return &___orientToPath2d_8; }
	inline void set_orientToPath2d_8(bool value)
	{
		___orientToPath2d_8 = value;
	}

	inline static int32_t get_offset_of_numSections_9() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___numSections_9)); }
	inline int32_t get_numSections_9() const { return ___numSections_9; }
	inline int32_t* get_address_of_numSections_9() { return &___numSections_9; }
	inline void set_numSections_9(int32_t value)
	{
		___numSections_9 = value;
	}

	inline static int32_t get_offset_of_currPt_10() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763, ___currPt_10)); }
	inline int32_t get_currPt_10() const { return ___currPt_10; }
	inline int32_t* get_address_of_currPt_10() { return &___currPt_10; }
	inline void set_currPt_10(int32_t value)
	{
		___currPt_10 = value;
	}
};

struct LTSpline_t2431306763_StaticFields
{
public:
	// System.Int32 LTSpline::DISTANCE_COUNT
	int32_t ___DISTANCE_COUNT_0;
	// System.Int32 LTSpline::SUBLINE_COUNT
	int32_t ___SUBLINE_COUNT_1;

public:
	inline static int32_t get_offset_of_DISTANCE_COUNT_0() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763_StaticFields, ___DISTANCE_COUNT_0)); }
	inline int32_t get_DISTANCE_COUNT_0() const { return ___DISTANCE_COUNT_0; }
	inline int32_t* get_address_of_DISTANCE_COUNT_0() { return &___DISTANCE_COUNT_0; }
	inline void set_DISTANCE_COUNT_0(int32_t value)
	{
		___DISTANCE_COUNT_0 = value;
	}

	inline static int32_t get_offset_of_SUBLINE_COUNT_1() { return static_cast<int32_t>(offsetof(LTSpline_t2431306763_StaticFields, ___SUBLINE_COUNT_1)); }
	inline int32_t get_SUBLINE_COUNT_1() const { return ___SUBLINE_COUNT_1; }
	inline int32_t* get_address_of_SUBLINE_COUNT_1() { return &___SUBLINE_COUNT_1; }
	inline void set_SUBLINE_COUNT_1(int32_t value)
	{
		___SUBLINE_COUNT_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTSPLINE_T2431306763_H
#ifndef U3CUSECANDYCHECKU3EC__ITERATOR0_T992976987_H
#define U3CUSECANDYCHECKU3EC__ITERATOR0_T992976987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<UseCandyCheck>c__Iterator0
struct  U3CUseCandyCheckU3Ec__Iterator0_t992976987  : public RuntimeObject
{
public:
	// GameManager GameManager/<UseCandyCheck>c__Iterator0::$this
	GameManager_t1536523654 * ___U24this_0;
	// System.Object GameManager/<UseCandyCheck>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameManager/<UseCandyCheck>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameManager/<UseCandyCheck>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUseCandyCheckU3Ec__Iterator0_t992976987, ___U24this_0)); }
	inline GameManager_t1536523654 * get_U24this_0() const { return ___U24this_0; }
	inline GameManager_t1536523654 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManager_t1536523654 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUseCandyCheckU3Ec__Iterator0_t992976987, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUseCandyCheckU3Ec__Iterator0_t992976987, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUseCandyCheckU3Ec__Iterator0_t992976987, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUSECANDYCHECKU3EC__ITERATOR0_T992976987_H
#ifndef LTUTILITY_T4124338238_H
#define LTUTILITY_T4124338238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTUtility
struct  LTUtility_t4124338238  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTUTILITY_T4124338238_H
#ifndef LTBEZIERPATH_T1817657086_H
#define LTBEZIERPATH_T1817657086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTBezierPath
struct  LTBezierPath_t1817657086  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] LTBezierPath::pts
	Vector3U5BU5D_t1718750761* ___pts_0;
	// System.Single LTBezierPath::length
	float ___length_1;
	// System.Boolean LTBezierPath::orientToPath
	bool ___orientToPath_2;
	// System.Boolean LTBezierPath::orientToPath2d
	bool ___orientToPath2d_3;
	// LTBezier[] LTBezierPath::beziers
	LTBezierU5BU5D_t3652571610* ___beziers_4;
	// System.Single[] LTBezierPath::lengthRatio
	SingleU5BU5D_t1444911251* ___lengthRatio_5;
	// System.Int32 LTBezierPath::currentBezier
	int32_t ___currentBezier_6;
	// System.Int32 LTBezierPath::previousBezier
	int32_t ___previousBezier_7;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___pts_0)); }
	inline Vector3U5BU5D_t1718750761* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t1718750761* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((&___pts_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___length_1)); }
	inline float get_length_1() const { return ___length_1; }
	inline float* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(float value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_orientToPath_2() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___orientToPath_2)); }
	inline bool get_orientToPath_2() const { return ___orientToPath_2; }
	inline bool* get_address_of_orientToPath_2() { return &___orientToPath_2; }
	inline void set_orientToPath_2(bool value)
	{
		___orientToPath_2 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_3() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___orientToPath2d_3)); }
	inline bool get_orientToPath2d_3() const { return ___orientToPath2d_3; }
	inline bool* get_address_of_orientToPath2d_3() { return &___orientToPath2d_3; }
	inline void set_orientToPath2d_3(bool value)
	{
		___orientToPath2d_3 = value;
	}

	inline static int32_t get_offset_of_beziers_4() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___beziers_4)); }
	inline LTBezierU5BU5D_t3652571610* get_beziers_4() const { return ___beziers_4; }
	inline LTBezierU5BU5D_t3652571610** get_address_of_beziers_4() { return &___beziers_4; }
	inline void set_beziers_4(LTBezierU5BU5D_t3652571610* value)
	{
		___beziers_4 = value;
		Il2CppCodeGenWriteBarrier((&___beziers_4), value);
	}

	inline static int32_t get_offset_of_lengthRatio_5() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___lengthRatio_5)); }
	inline SingleU5BU5D_t1444911251* get_lengthRatio_5() const { return ___lengthRatio_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_lengthRatio_5() { return &___lengthRatio_5; }
	inline void set_lengthRatio_5(SingleU5BU5D_t1444911251* value)
	{
		___lengthRatio_5 = value;
		Il2CppCodeGenWriteBarrier((&___lengthRatio_5), value);
	}

	inline static int32_t get_offset_of_currentBezier_6() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___currentBezier_6)); }
	inline int32_t get_currentBezier_6() const { return ___currentBezier_6; }
	inline int32_t* get_address_of_currentBezier_6() { return &___currentBezier_6; }
	inline void set_currentBezier_6(int32_t value)
	{
		___currentBezier_6 = value;
	}

	inline static int32_t get_offset_of_previousBezier_7() { return static_cast<int32_t>(offsetof(LTBezierPath_t1817657086, ___previousBezier_7)); }
	inline int32_t get_previousBezier_7() const { return ___previousBezier_7; }
	inline int32_t* get_address_of_previousBezier_7() { return &___previousBezier_7; }
	inline void set_previousBezier_7(int32_t value)
	{
		___previousBezier_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTBEZIERPATH_T1817657086_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef U3CPAUSEPOINTERU3EC__ANONSTOREY2_T4213274272_H
#define U3CPAUSEPOINTERU3EC__ANONSTOREY2_T4213274272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LastAttackManager/<PausePointer>c__AnonStorey2
struct  U3CPausePointerU3Ec__AnonStorey2_t4213274272  : public RuntimeObject
{
public:
	// GhostManager LastAttackManager/<PausePointer>c__AnonStorey2::ghostManager
	GhostManager_t3188635936 * ___ghostManager_0;
	// UnityEngine.GameObject LastAttackManager/<PausePointer>c__AnonStorey2::flyCharm
	GameObject_t1113636619 * ___flyCharm_1;
	// LastAttackManager LastAttackManager/<PausePointer>c__AnonStorey2::$this
	LastAttackManager_t490974840 * ___U24this_2;

public:
	inline static int32_t get_offset_of_ghostManager_0() { return static_cast<int32_t>(offsetof(U3CPausePointerU3Ec__AnonStorey2_t4213274272, ___ghostManager_0)); }
	inline GhostManager_t3188635936 * get_ghostManager_0() const { return ___ghostManager_0; }
	inline GhostManager_t3188635936 ** get_address_of_ghostManager_0() { return &___ghostManager_0; }
	inline void set_ghostManager_0(GhostManager_t3188635936 * value)
	{
		___ghostManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___ghostManager_0), value);
	}

	inline static int32_t get_offset_of_flyCharm_1() { return static_cast<int32_t>(offsetof(U3CPausePointerU3Ec__AnonStorey2_t4213274272, ___flyCharm_1)); }
	inline GameObject_t1113636619 * get_flyCharm_1() const { return ___flyCharm_1; }
	inline GameObject_t1113636619 ** get_address_of_flyCharm_1() { return &___flyCharm_1; }
	inline void set_flyCharm_1(GameObject_t1113636619 * value)
	{
		___flyCharm_1 = value;
		Il2CppCodeGenWriteBarrier((&___flyCharm_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CPausePointerU3Ec__AnonStorey2_t4213274272, ___U24this_2)); }
	inline LastAttackManager_t490974840 * get_U24this_2() const { return ___U24this_2; }
	inline LastAttackManager_t490974840 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LastAttackManager_t490974840 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPAUSEPOINTERU3EC__ANONSTOREY2_T4213274272_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef U3CSHOWRESULTU3EC__ITERATOR1_T419679075_H
#define U3CSHOWRESULTU3EC__ITERATOR1_T419679075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LastAttackManager/<ShowResult>c__Iterator1
struct  U3CShowResultU3Ec__Iterator1_t419679075  : public RuntimeObject
{
public:
	// LastAttackManager LastAttackManager/<ShowResult>c__Iterator1::$this
	LastAttackManager_t490974840 * ___U24this_0;
	// System.Object LastAttackManager/<ShowResult>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean LastAttackManager/<ShowResult>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 LastAttackManager/<ShowResult>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CShowResultU3Ec__Iterator1_t419679075, ___U24this_0)); }
	inline LastAttackManager_t490974840 * get_U24this_0() const { return ___U24this_0; }
	inline LastAttackManager_t490974840 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LastAttackManager_t490974840 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CShowResultU3Ec__Iterator1_t419679075, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CShowResultU3Ec__Iterator1_t419679075, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowResultU3Ec__Iterator1_t419679075, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWRESULTU3EC__ITERATOR1_T419679075_H
#ifndef U3CMOVEARGAMESCENEU3EC__ITERATOR0_T909025706_H
#define U3CMOVEARGAMESCENEU3EC__ITERATOR0_T909025706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyManager/<MoveARGameScene>c__Iterator0
struct  U3CMoveARGameSceneU3Ec__Iterator0_t909025706  : public RuntimeObject
{
public:
	// LobbyManager LobbyManager/<MoveARGameScene>c__Iterator0::$this
	LobbyManager_t4136450456 * ___U24this_0;
	// System.Object LobbyManager/<MoveARGameScene>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean LobbyManager/<MoveARGameScene>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 LobbyManager/<MoveARGameScene>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CMoveARGameSceneU3Ec__Iterator0_t909025706, ___U24this_0)); }
	inline LobbyManager_t4136450456 * get_U24this_0() const { return ___U24this_0; }
	inline LobbyManager_t4136450456 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LobbyManager_t4136450456 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CMoveARGameSceneU3Ec__Iterator0_t909025706, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CMoveARGameSceneU3Ec__Iterator0_t909025706, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CMoveARGameSceneU3Ec__Iterator0_t909025706, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEARGAMESCENEU3EC__ITERATOR0_T909025706_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef U3CMOVEPUZZLEGAMESCENEU3EC__ITERATOR1_T2545055721_H
#define U3CMOVEPUZZLEGAMESCENEU3EC__ITERATOR1_T2545055721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyManager/<MovePuzzleGameScene>c__Iterator1
struct  U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721  : public RuntimeObject
{
public:
	// LobbyManager LobbyManager/<MovePuzzleGameScene>c__Iterator1::$this
	LobbyManager_t4136450456 * ___U24this_0;
	// System.Object LobbyManager/<MovePuzzleGameScene>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean LobbyManager/<MovePuzzleGameScene>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 LobbyManager/<MovePuzzleGameScene>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721, ___U24this_0)); }
	inline LobbyManager_t4136450456 * get_U24this_0() const { return ___U24this_0; }
	inline LobbyManager_t4136450456 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LobbyManager_t4136450456 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEPUZZLEGAMESCENEU3EC__ITERATOR1_T2545055721_H
#ifndef U3CLOOPU3EC__ITERATOR0_T3060558849_H
#define U3CLOOPU3EC__ITERATOR0_T3060558849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveLoadManager/<Loop>c__Iterator0
struct  U3CLoopU3Ec__Iterator0_t3060558849  : public RuntimeObject
{
public:
	// SaveLoadManager SaveLoadManager/<Loop>c__Iterator0::$this
	SaveLoadManager_t4010657880 * ___U24this_0;
	// System.Object SaveLoadManager/<Loop>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SaveLoadManager/<Loop>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SaveLoadManager/<Loop>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoopU3Ec__Iterator0_t3060558849, ___U24this_0)); }
	inline SaveLoadManager_t4010657880 * get_U24this_0() const { return ___U24this_0; }
	inline SaveLoadManager_t4010657880 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SaveLoadManager_t4010657880 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoopU3Ec__Iterator0_t3060558849, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoopU3Ec__Iterator0_t3060558849, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoopU3Ec__Iterator0_t3060558849, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOOPU3EC__ITERATOR0_T3060558849_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUMERATOR_T2017297076_H
#define ENUMERATOR_T2017297076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t2017297076 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t128053199 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___l_0)); }
	inline List_1_t128053199 * get_l_0() const { return ___l_0; }
	inline List_1_t128053199 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t128053199 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2017297076_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef U3CSHOWALLCHARMU3EC__ITERATOR1_T3091840906_H
#define U3CSHOWALLCHARMU3EC__ITERATOR1_T3091840906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameManager/<ShowAllCharm>c__Iterator1
struct  U3CShowAllCharmU3Ec__Iterator1_t3091840906  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> CharmGameManager/<ShowAllCharm>c__Iterator1::<list>__0
	List_1_t128053199 * ___U3ClistU3E__0_0;
	// System.Int32 CharmGameManager/<ShowAllCharm>c__Iterator1::<remainCharmCount>__0
	int32_t ___U3CremainCharmCountU3E__0_1;
	// System.Collections.Generic.List`1/Enumerator<System.Int32> CharmGameManager/<ShowAllCharm>c__Iterator1::$locvar0
	Enumerator_t2017297076  ___U24locvar0_2;
	// System.Int32 CharmGameManager/<ShowAllCharm>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_3;
	// CharmGameManager CharmGameManager/<ShowAllCharm>c__Iterator1::$this
	CharmGameManager_t204340045 * ___U24this_4;
	// System.Object CharmGameManager/<ShowAllCharm>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean CharmGameManager/<ShowAllCharm>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 CharmGameManager/<ShowAllCharm>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U3ClistU3E__0_0)); }
	inline List_1_t128053199 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t128053199 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t128053199 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CremainCharmCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U3CremainCharmCountU3E__0_1)); }
	inline int32_t get_U3CremainCharmCountU3E__0_1() const { return ___U3CremainCharmCountU3E__0_1; }
	inline int32_t* get_address_of_U3CremainCharmCountU3E__0_1() { return &___U3CremainCharmCountU3E__0_1; }
	inline void set_U3CremainCharmCountU3E__0_1(int32_t value)
	{
		___U3CremainCharmCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U24locvar0_2)); }
	inline Enumerator_t2017297076  get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline Enumerator_t2017297076 * get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(Enumerator_t2017297076  value)
	{
		___U24locvar0_2 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_3() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U3CiU3E__1_3)); }
	inline int32_t get_U3CiU3E__1_3() const { return ___U3CiU3E__1_3; }
	inline int32_t* get_address_of_U3CiU3E__1_3() { return &___U3CiU3E__1_3; }
	inline void set_U3CiU3E__1_3(int32_t value)
	{
		___U3CiU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U24this_4)); }
	inline CharmGameManager_t204340045 * get_U24this_4() const { return ___U24this_4; }
	inline CharmGameManager_t204340045 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(CharmGameManager_t204340045 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CShowAllCharmU3Ec__Iterator1_t3091840906, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWALLCHARMU3EC__ITERATOR1_T3091840906_H
#ifndef LEANAUDIOWAVESTYLE_T2891147723_H
#define LEANAUDIOWAVESTYLE_T2891147723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanAudioOptions/LeanAudioWaveStyle
struct  LeanAudioWaveStyle_t2891147723 
{
public:
	// System.Int32 LeanAudioOptions/LeanAudioWaveStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LeanAudioWaveStyle_t2891147723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANAUDIOWAVESTYLE_T2891147723_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef MOVEDIRECTION_T1208372469_H
#define MOVEDIRECTION_T1208372469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveDirection
struct  MoveDirection_t1208372469 
{
public:
	// System.Int32 MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1208372469, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1208372469_H
#ifndef CHARMGAMESTATE_T3120537021_H
#define CHARMGAMESTATE_T3120537021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameState
struct  CharmGameState_t3120537021 
{
public:
	// System.Int32 CharmGameState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharmGameState_t3120537021, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMGAMESTATE_T3120537021_H
#ifndef ELEMENT_TYPE_T1916908453_H
#define ELEMENT_TYPE_T1916908453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTGUI/Element_Type
struct  Element_Type_t1916908453 
{
public:
	// System.Int32 LTGUI/Element_Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Element_Type_t1916908453, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_TYPE_T1916908453_H
#ifndef LTGUI_T651246514_H
#define LTGUI_T651246514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTGUI
struct  LTGUI_t651246514  : public RuntimeObject
{
public:

public:
};

struct LTGUI_t651246514_StaticFields
{
public:
	// System.Int32 LTGUI::RECT_LEVELS
	int32_t ___RECT_LEVELS_0;
	// System.Int32 LTGUI::RECTS_PER_LEVEL
	int32_t ___RECTS_PER_LEVEL_1;
	// System.Int32 LTGUI::BUTTONS_MAX
	int32_t ___BUTTONS_MAX_2;
	// LTRect[] LTGUI::levels
	LTRectU5BU5D_t2099225848* ___levels_3;
	// System.Int32[] LTGUI::levelDepths
	Int32U5BU5D_t385246372* ___levelDepths_4;
	// UnityEngine.Rect[] LTGUI::buttons
	RectU5BU5D_t2936723554* ___buttons_5;
	// System.Int32[] LTGUI::buttonLevels
	Int32U5BU5D_t385246372* ___buttonLevels_6;
	// System.Int32[] LTGUI::buttonLastFrame
	Int32U5BU5D_t385246372* ___buttonLastFrame_7;
	// LTRect LTGUI::r
	LTRect_t2883103509 * ___r_8;
	// UnityEngine.Color LTGUI::color
	Color_t2555686324  ___color_9;
	// System.Boolean LTGUI::isGUIEnabled
	bool ___isGUIEnabled_10;
	// System.Int32 LTGUI::global_counter
	int32_t ___global_counter_11;

public:
	inline static int32_t get_offset_of_RECT_LEVELS_0() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___RECT_LEVELS_0)); }
	inline int32_t get_RECT_LEVELS_0() const { return ___RECT_LEVELS_0; }
	inline int32_t* get_address_of_RECT_LEVELS_0() { return &___RECT_LEVELS_0; }
	inline void set_RECT_LEVELS_0(int32_t value)
	{
		___RECT_LEVELS_0 = value;
	}

	inline static int32_t get_offset_of_RECTS_PER_LEVEL_1() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___RECTS_PER_LEVEL_1)); }
	inline int32_t get_RECTS_PER_LEVEL_1() const { return ___RECTS_PER_LEVEL_1; }
	inline int32_t* get_address_of_RECTS_PER_LEVEL_1() { return &___RECTS_PER_LEVEL_1; }
	inline void set_RECTS_PER_LEVEL_1(int32_t value)
	{
		___RECTS_PER_LEVEL_1 = value;
	}

	inline static int32_t get_offset_of_BUTTONS_MAX_2() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___BUTTONS_MAX_2)); }
	inline int32_t get_BUTTONS_MAX_2() const { return ___BUTTONS_MAX_2; }
	inline int32_t* get_address_of_BUTTONS_MAX_2() { return &___BUTTONS_MAX_2; }
	inline void set_BUTTONS_MAX_2(int32_t value)
	{
		___BUTTONS_MAX_2 = value;
	}

	inline static int32_t get_offset_of_levels_3() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___levels_3)); }
	inline LTRectU5BU5D_t2099225848* get_levels_3() const { return ___levels_3; }
	inline LTRectU5BU5D_t2099225848** get_address_of_levels_3() { return &___levels_3; }
	inline void set_levels_3(LTRectU5BU5D_t2099225848* value)
	{
		___levels_3 = value;
		Il2CppCodeGenWriteBarrier((&___levels_3), value);
	}

	inline static int32_t get_offset_of_levelDepths_4() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___levelDepths_4)); }
	inline Int32U5BU5D_t385246372* get_levelDepths_4() const { return ___levelDepths_4; }
	inline Int32U5BU5D_t385246372** get_address_of_levelDepths_4() { return &___levelDepths_4; }
	inline void set_levelDepths_4(Int32U5BU5D_t385246372* value)
	{
		___levelDepths_4 = value;
		Il2CppCodeGenWriteBarrier((&___levelDepths_4), value);
	}

	inline static int32_t get_offset_of_buttons_5() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___buttons_5)); }
	inline RectU5BU5D_t2936723554* get_buttons_5() const { return ___buttons_5; }
	inline RectU5BU5D_t2936723554** get_address_of_buttons_5() { return &___buttons_5; }
	inline void set_buttons_5(RectU5BU5D_t2936723554* value)
	{
		___buttons_5 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_5), value);
	}

	inline static int32_t get_offset_of_buttonLevels_6() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___buttonLevels_6)); }
	inline Int32U5BU5D_t385246372* get_buttonLevels_6() const { return ___buttonLevels_6; }
	inline Int32U5BU5D_t385246372** get_address_of_buttonLevels_6() { return &___buttonLevels_6; }
	inline void set_buttonLevels_6(Int32U5BU5D_t385246372* value)
	{
		___buttonLevels_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonLevels_6), value);
	}

	inline static int32_t get_offset_of_buttonLastFrame_7() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___buttonLastFrame_7)); }
	inline Int32U5BU5D_t385246372* get_buttonLastFrame_7() const { return ___buttonLastFrame_7; }
	inline Int32U5BU5D_t385246372** get_address_of_buttonLastFrame_7() { return &___buttonLastFrame_7; }
	inline void set_buttonLastFrame_7(Int32U5BU5D_t385246372* value)
	{
		___buttonLastFrame_7 = value;
		Il2CppCodeGenWriteBarrier((&___buttonLastFrame_7), value);
	}

	inline static int32_t get_offset_of_r_8() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___r_8)); }
	inline LTRect_t2883103509 * get_r_8() const { return ___r_8; }
	inline LTRect_t2883103509 ** get_address_of_r_8() { return &___r_8; }
	inline void set_r_8(LTRect_t2883103509 * value)
	{
		___r_8 = value;
		Il2CppCodeGenWriteBarrier((&___r_8), value);
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___color_9)); }
	inline Color_t2555686324  get_color_9() const { return ___color_9; }
	inline Color_t2555686324 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2555686324  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_isGUIEnabled_10() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___isGUIEnabled_10)); }
	inline bool get_isGUIEnabled_10() const { return ___isGUIEnabled_10; }
	inline bool* get_address_of_isGUIEnabled_10() { return &___isGUIEnabled_10; }
	inline void set_isGUIEnabled_10(bool value)
	{
		___isGUIEnabled_10 = value;
	}

	inline static int32_t get_offset_of_global_counter_11() { return static_cast<int32_t>(offsetof(LTGUI_t651246514_StaticFields, ___global_counter_11)); }
	inline int32_t get_global_counter_11() const { return ___global_counter_11; }
	inline int32_t* get_address_of_global_counter_11() { return &___global_counter_11; }
	inline void set_global_counter_11(int32_t value)
	{
		___global_counter_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTGUI_T651246514_H
#ifndef GAMEMODESTATE_T1521487089_H
#define GAMEMODESTATE_T1521487089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/GameModeState
struct  GameModeState_t1521487089 
{
public:
	// System.Int32 GameManager/GameModeState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameModeState_t1521487089, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMODESTATE_T1521487089_H
#ifndef LTBEZIER_T3079809627_H
#define LTBEZIER_T3079809627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTBezier
struct  LTBezier_t3079809627  : public RuntimeObject
{
public:
	// System.Single LTBezier::length
	float ___length_0;
	// UnityEngine.Vector3 LTBezier::a
	Vector3_t3722313464  ___a_1;
	// UnityEngine.Vector3 LTBezier::aa
	Vector3_t3722313464  ___aa_2;
	// UnityEngine.Vector3 LTBezier::bb
	Vector3_t3722313464  ___bb_3;
	// UnityEngine.Vector3 LTBezier::cc
	Vector3_t3722313464  ___cc_4;
	// System.Single LTBezier::len
	float ___len_5;
	// System.Single[] LTBezier::arcLengths
	SingleU5BU5D_t1444911251* ___arcLengths_6;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___length_0)); }
	inline float get_length_0() const { return ___length_0; }
	inline float* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(float value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___a_1)); }
	inline Vector3_t3722313464  get_a_1() const { return ___a_1; }
	inline Vector3_t3722313464 * get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(Vector3_t3722313464  value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_aa_2() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___aa_2)); }
	inline Vector3_t3722313464  get_aa_2() const { return ___aa_2; }
	inline Vector3_t3722313464 * get_address_of_aa_2() { return &___aa_2; }
	inline void set_aa_2(Vector3_t3722313464  value)
	{
		___aa_2 = value;
	}

	inline static int32_t get_offset_of_bb_3() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___bb_3)); }
	inline Vector3_t3722313464  get_bb_3() const { return ___bb_3; }
	inline Vector3_t3722313464 * get_address_of_bb_3() { return &___bb_3; }
	inline void set_bb_3(Vector3_t3722313464  value)
	{
		___bb_3 = value;
	}

	inline static int32_t get_offset_of_cc_4() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___cc_4)); }
	inline Vector3_t3722313464  get_cc_4() const { return ___cc_4; }
	inline Vector3_t3722313464 * get_address_of_cc_4() { return &___cc_4; }
	inline void set_cc_4(Vector3_t3722313464  value)
	{
		___cc_4 = value;
	}

	inline static int32_t get_offset_of_len_5() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___len_5)); }
	inline float get_len_5() const { return ___len_5; }
	inline float* get_address_of_len_5() { return &___len_5; }
	inline void set_len_5(float value)
	{
		___len_5 = value;
	}

	inline static int32_t get_offset_of_arcLengths_6() { return static_cast<int32_t>(offsetof(LTBezier_t3079809627, ___arcLengths_6)); }
	inline SingleU5BU5D_t1444911251* get_arcLengths_6() const { return ___arcLengths_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_arcLengths_6() { return &___arcLengths_6; }
	inline void set_arcLengths_6(SingleU5BU5D_t1444911251* value)
	{
		___arcLengths_6 = value;
		Il2CppCodeGenWriteBarrier((&___arcLengths_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTBEZIER_T3079809627_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TWEENACTION_T2598825989_H
#define TWEENACTION_T2598825989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenAction
struct  TweenAction_t2598825989 
{
public:
	// System.Int32 TweenAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenAction_t2598825989, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENACTION_T2598825989_H
#ifndef LEANTWEENTYPE_T619681147_H
#define LEANTWEENTYPE_T619681147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanTweenType
struct  LeanTweenType_t619681147 
{
public:
	// System.Int32 LeanTweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LeanTweenType_t619681147, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTWEENTYPE_T619681147_H
#ifndef ANIMATIONNAME_T1099644795_H
#define ANIMATIONNAME_T1099644795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationName
struct  AnimationName_t1099644795 
{
public:
	// System.Int32 AnimationName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationName_t1099644795, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONNAME_T1099644795_H
#ifndef LTDESCROPTIONAL_T4257087022_H
#define LTDESCROPTIONAL_T4257087022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTDescrOptional
struct  LTDescrOptional_t4257087022  : public RuntimeObject
{
public:
	// UnityEngine.Transform LTDescrOptional::<toTrans>k__BackingField
	Transform_t3600365921 * ___U3CtoTransU3Ek__BackingField_0;
	// UnityEngine.Vector3 LTDescrOptional::<point>k__BackingField
	Vector3_t3722313464  ___U3CpointU3Ek__BackingField_1;
	// UnityEngine.Vector3 LTDescrOptional::<axis>k__BackingField
	Vector3_t3722313464  ___U3CaxisU3Ek__BackingField_2;
	// System.Single LTDescrOptional::<lastVal>k__BackingField
	float ___U3ClastValU3Ek__BackingField_3;
	// UnityEngine.Quaternion LTDescrOptional::<origRotation>k__BackingField
	Quaternion_t2301928331  ___U3CorigRotationU3Ek__BackingField_4;
	// LTBezierPath LTDescrOptional::<path>k__BackingField
	LTBezierPath_t1817657086 * ___U3CpathU3Ek__BackingField_5;
	// LTSpline LTDescrOptional::<spline>k__BackingField
	LTSpline_t2431306763 * ___U3CsplineU3Ek__BackingField_6;
	// UnityEngine.AnimationCurve LTDescrOptional::animationCurve
	AnimationCurve_t3046754366 * ___animationCurve_7;
	// System.Int32 LTDescrOptional::initFrameCount
	int32_t ___initFrameCount_8;
	// LTRect LTDescrOptional::<ltRect>k__BackingField
	LTRect_t2883103509 * ___U3CltRectU3Ek__BackingField_9;
	// System.Action`1<System.Single> LTDescrOptional::<onUpdateFloat>k__BackingField
	Action_1_t1569734369 * ___U3ConUpdateFloatU3Ek__BackingField_10;
	// System.Action`2<System.Single,System.Single> LTDescrOptional::<onUpdateFloatRatio>k__BackingField
	Action_2_t3683108670 * ___U3ConUpdateFloatRatioU3Ek__BackingField_11;
	// System.Action`2<System.Single,System.Object> LTDescrOptional::<onUpdateFloatObject>k__BackingField
	Action_2_t1070980764 * ___U3ConUpdateFloatObjectU3Ek__BackingField_12;
	// System.Action`1<UnityEngine.Vector2> LTDescrOptional::<onUpdateVector2>k__BackingField
	Action_1_t2328697118 * ___U3ConUpdateVector2U3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Vector3> LTDescrOptional::<onUpdateVector3>k__BackingField
	Action_1_t3894781059 * ___U3ConUpdateVector3U3Ek__BackingField_14;
	// System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::<onUpdateVector3Object>k__BackingField
	Action_2_t1344820274 * ___U3ConUpdateVector3ObjectU3Ek__BackingField_15;
	// System.Action`1<UnityEngine.Color> LTDescrOptional::<onUpdateColor>k__BackingField
	Action_1_t2728153919 * ___U3ConUpdateColorU3Ek__BackingField_16;
	// System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::<onUpdateColorObject>k__BackingField
	Action_2_t567985926 * ___U3ConUpdateColorObjectU3Ek__BackingField_17;
	// System.Action LTDescrOptional::<onComplete>k__BackingField
	Action_t1264377477 * ___U3ConCompleteU3Ek__BackingField_18;
	// System.Action`1<System.Object> LTDescrOptional::<onCompleteObject>k__BackingField
	Action_1_t3252573759 * ___U3ConCompleteObjectU3Ek__BackingField_19;
	// System.Object LTDescrOptional::<onCompleteParam>k__BackingField
	RuntimeObject * ___U3ConCompleteParamU3Ek__BackingField_20;
	// System.Object LTDescrOptional::<onUpdateParam>k__BackingField
	RuntimeObject * ___U3ConUpdateParamU3Ek__BackingField_21;
	// System.Action LTDescrOptional::<onStart>k__BackingField
	Action_t1264377477 * ___U3ConStartU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CtoTransU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CtoTransU3Ek__BackingField_0)); }
	inline Transform_t3600365921 * get_U3CtoTransU3Ek__BackingField_0() const { return ___U3CtoTransU3Ek__BackingField_0; }
	inline Transform_t3600365921 ** get_address_of_U3CtoTransU3Ek__BackingField_0() { return &___U3CtoTransU3Ek__BackingField_0; }
	inline void set_U3CtoTransU3Ek__BackingField_0(Transform_t3600365921 * value)
	{
		___U3CtoTransU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtoTransU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CpointU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CpointU3Ek__BackingField_1() const { return ___U3CpointU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CpointU3Ek__BackingField_1() { return &___U3CpointU3Ek__BackingField_1; }
	inline void set_U3CpointU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CpointU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CaxisU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CaxisU3Ek__BackingField_2)); }
	inline Vector3_t3722313464  get_U3CaxisU3Ek__BackingField_2() const { return ___U3CaxisU3Ek__BackingField_2; }
	inline Vector3_t3722313464 * get_address_of_U3CaxisU3Ek__BackingField_2() { return &___U3CaxisU3Ek__BackingField_2; }
	inline void set_U3CaxisU3Ek__BackingField_2(Vector3_t3722313464  value)
	{
		___U3CaxisU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3ClastValU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ClastValU3Ek__BackingField_3)); }
	inline float get_U3ClastValU3Ek__BackingField_3() const { return ___U3ClastValU3Ek__BackingField_3; }
	inline float* get_address_of_U3ClastValU3Ek__BackingField_3() { return &___U3ClastValU3Ek__BackingField_3; }
	inline void set_U3ClastValU3Ek__BackingField_3(float value)
	{
		___U3ClastValU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CorigRotationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CorigRotationU3Ek__BackingField_4)); }
	inline Quaternion_t2301928331  get_U3CorigRotationU3Ek__BackingField_4() const { return ___U3CorigRotationU3Ek__BackingField_4; }
	inline Quaternion_t2301928331 * get_address_of_U3CorigRotationU3Ek__BackingField_4() { return &___U3CorigRotationU3Ek__BackingField_4; }
	inline void set_U3CorigRotationU3Ek__BackingField_4(Quaternion_t2301928331  value)
	{
		___U3CorigRotationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CpathU3Ek__BackingField_5)); }
	inline LTBezierPath_t1817657086 * get_U3CpathU3Ek__BackingField_5() const { return ___U3CpathU3Ek__BackingField_5; }
	inline LTBezierPath_t1817657086 ** get_address_of_U3CpathU3Ek__BackingField_5() { return &___U3CpathU3Ek__BackingField_5; }
	inline void set_U3CpathU3Ek__BackingField_5(LTBezierPath_t1817657086 * value)
	{
		___U3CpathU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsplineU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CsplineU3Ek__BackingField_6)); }
	inline LTSpline_t2431306763 * get_U3CsplineU3Ek__BackingField_6() const { return ___U3CsplineU3Ek__BackingField_6; }
	inline LTSpline_t2431306763 ** get_address_of_U3CsplineU3Ek__BackingField_6() { return &___U3CsplineU3Ek__BackingField_6; }
	inline void set_U3CsplineU3Ek__BackingField_6(LTSpline_t2431306763 * value)
	{
		___U3CsplineU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsplineU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_animationCurve_7() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___animationCurve_7)); }
	inline AnimationCurve_t3046754366 * get_animationCurve_7() const { return ___animationCurve_7; }
	inline AnimationCurve_t3046754366 ** get_address_of_animationCurve_7() { return &___animationCurve_7; }
	inline void set_animationCurve_7(AnimationCurve_t3046754366 * value)
	{
		___animationCurve_7 = value;
		Il2CppCodeGenWriteBarrier((&___animationCurve_7), value);
	}

	inline static int32_t get_offset_of_initFrameCount_8() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___initFrameCount_8)); }
	inline int32_t get_initFrameCount_8() const { return ___initFrameCount_8; }
	inline int32_t* get_address_of_initFrameCount_8() { return &___initFrameCount_8; }
	inline void set_initFrameCount_8(int32_t value)
	{
		___initFrameCount_8 = value;
	}

	inline static int32_t get_offset_of_U3CltRectU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3CltRectU3Ek__BackingField_9)); }
	inline LTRect_t2883103509 * get_U3CltRectU3Ek__BackingField_9() const { return ___U3CltRectU3Ek__BackingField_9; }
	inline LTRect_t2883103509 ** get_address_of_U3CltRectU3Ek__BackingField_9() { return &___U3CltRectU3Ek__BackingField_9; }
	inline void set_U3CltRectU3Ek__BackingField_9(LTRect_t2883103509 * value)
	{
		___U3CltRectU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CltRectU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateFloatU3Ek__BackingField_10)); }
	inline Action_1_t1569734369 * get_U3ConUpdateFloatU3Ek__BackingField_10() const { return ___U3ConUpdateFloatU3Ek__BackingField_10; }
	inline Action_1_t1569734369 ** get_address_of_U3ConUpdateFloatU3Ek__BackingField_10() { return &___U3ConUpdateFloatU3Ek__BackingField_10; }
	inline void set_U3ConUpdateFloatU3Ek__BackingField_10(Action_1_t1569734369 * value)
	{
		___U3ConUpdateFloatU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateFloatU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateFloatRatioU3Ek__BackingField_11)); }
	inline Action_2_t3683108670 * get_U3ConUpdateFloatRatioU3Ek__BackingField_11() const { return ___U3ConUpdateFloatRatioU3Ek__BackingField_11; }
	inline Action_2_t3683108670 ** get_address_of_U3ConUpdateFloatRatioU3Ek__BackingField_11() { return &___U3ConUpdateFloatRatioU3Ek__BackingField_11; }
	inline void set_U3ConUpdateFloatRatioU3Ek__BackingField_11(Action_2_t3683108670 * value)
	{
		___U3ConUpdateFloatRatioU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateFloatRatioU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateFloatObjectU3Ek__BackingField_12)); }
	inline Action_2_t1070980764 * get_U3ConUpdateFloatObjectU3Ek__BackingField_12() const { return ___U3ConUpdateFloatObjectU3Ek__BackingField_12; }
	inline Action_2_t1070980764 ** get_address_of_U3ConUpdateFloatObjectU3Ek__BackingField_12() { return &___U3ConUpdateFloatObjectU3Ek__BackingField_12; }
	inline void set_U3ConUpdateFloatObjectU3Ek__BackingField_12(Action_2_t1070980764 * value)
	{
		___U3ConUpdateFloatObjectU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateFloatObjectU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector2U3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateVector2U3Ek__BackingField_13)); }
	inline Action_1_t2328697118 * get_U3ConUpdateVector2U3Ek__BackingField_13() const { return ___U3ConUpdateVector2U3Ek__BackingField_13; }
	inline Action_1_t2328697118 ** get_address_of_U3ConUpdateVector2U3Ek__BackingField_13() { return &___U3ConUpdateVector2U3Ek__BackingField_13; }
	inline void set_U3ConUpdateVector2U3Ek__BackingField_13(Action_1_t2328697118 * value)
	{
		___U3ConUpdateVector2U3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateVector2U3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector3U3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateVector3U3Ek__BackingField_14)); }
	inline Action_1_t3894781059 * get_U3ConUpdateVector3U3Ek__BackingField_14() const { return ___U3ConUpdateVector3U3Ek__BackingField_14; }
	inline Action_1_t3894781059 ** get_address_of_U3ConUpdateVector3U3Ek__BackingField_14() { return &___U3ConUpdateVector3U3Ek__BackingField_14; }
	inline void set_U3ConUpdateVector3U3Ek__BackingField_14(Action_1_t3894781059 * value)
	{
		___U3ConUpdateVector3U3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateVector3U3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateVector3ObjectU3Ek__BackingField_15)); }
	inline Action_2_t1344820274 * get_U3ConUpdateVector3ObjectU3Ek__BackingField_15() const { return ___U3ConUpdateVector3ObjectU3Ek__BackingField_15; }
	inline Action_2_t1344820274 ** get_address_of_U3ConUpdateVector3ObjectU3Ek__BackingField_15() { return &___U3ConUpdateVector3ObjectU3Ek__BackingField_15; }
	inline void set_U3ConUpdateVector3ObjectU3Ek__BackingField_15(Action_2_t1344820274 * value)
	{
		___U3ConUpdateVector3ObjectU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateVector3ObjectU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateColorU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateColorU3Ek__BackingField_16)); }
	inline Action_1_t2728153919 * get_U3ConUpdateColorU3Ek__BackingField_16() const { return ___U3ConUpdateColorU3Ek__BackingField_16; }
	inline Action_1_t2728153919 ** get_address_of_U3ConUpdateColorU3Ek__BackingField_16() { return &___U3ConUpdateColorU3Ek__BackingField_16; }
	inline void set_U3ConUpdateColorU3Ek__BackingField_16(Action_1_t2728153919 * value)
	{
		___U3ConUpdateColorU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateColorU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateColorObjectU3Ek__BackingField_17)); }
	inline Action_2_t567985926 * get_U3ConUpdateColorObjectU3Ek__BackingField_17() const { return ___U3ConUpdateColorObjectU3Ek__BackingField_17; }
	inline Action_2_t567985926 ** get_address_of_U3ConUpdateColorObjectU3Ek__BackingField_17() { return &___U3ConUpdateColorObjectU3Ek__BackingField_17; }
	inline void set_U3ConUpdateColorObjectU3Ek__BackingField_17(Action_2_t567985926 * value)
	{
		___U3ConUpdateColorObjectU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateColorObjectU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3ConCompleteU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConCompleteU3Ek__BackingField_18)); }
	inline Action_t1264377477 * get_U3ConCompleteU3Ek__BackingField_18() const { return ___U3ConCompleteU3Ek__BackingField_18; }
	inline Action_t1264377477 ** get_address_of_U3ConCompleteU3Ek__BackingField_18() { return &___U3ConCompleteU3Ek__BackingField_18; }
	inline void set_U3ConCompleteU3Ek__BackingField_18(Action_t1264377477 * value)
	{
		___U3ConCompleteU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConCompleteU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3ConCompleteObjectU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConCompleteObjectU3Ek__BackingField_19)); }
	inline Action_1_t3252573759 * get_U3ConCompleteObjectU3Ek__BackingField_19() const { return ___U3ConCompleteObjectU3Ek__BackingField_19; }
	inline Action_1_t3252573759 ** get_address_of_U3ConCompleteObjectU3Ek__BackingField_19() { return &___U3ConCompleteObjectU3Ek__BackingField_19; }
	inline void set_U3ConCompleteObjectU3Ek__BackingField_19(Action_1_t3252573759 * value)
	{
		___U3ConCompleteObjectU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConCompleteObjectU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3ConCompleteParamU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConCompleteParamU3Ek__BackingField_20)); }
	inline RuntimeObject * get_U3ConCompleteParamU3Ek__BackingField_20() const { return ___U3ConCompleteParamU3Ek__BackingField_20; }
	inline RuntimeObject ** get_address_of_U3ConCompleteParamU3Ek__BackingField_20() { return &___U3ConCompleteParamU3Ek__BackingField_20; }
	inline void set_U3ConCompleteParamU3Ek__BackingField_20(RuntimeObject * value)
	{
		___U3ConCompleteParamU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConCompleteParamU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3ConUpdateParamU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConUpdateParamU3Ek__BackingField_21)); }
	inline RuntimeObject * get_U3ConUpdateParamU3Ek__BackingField_21() const { return ___U3ConUpdateParamU3Ek__BackingField_21; }
	inline RuntimeObject ** get_address_of_U3ConUpdateParamU3Ek__BackingField_21() { return &___U3ConUpdateParamU3Ek__BackingField_21; }
	inline void set_U3ConUpdateParamU3Ek__BackingField_21(RuntimeObject * value)
	{
		___U3ConUpdateParamU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConUpdateParamU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3ConStartU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(LTDescrOptional_t4257087022, ___U3ConStartU3Ek__BackingField_22)); }
	inline Action_t1264377477 * get_U3ConStartU3Ek__BackingField_22() const { return ___U3ConStartU3Ek__BackingField_22; }
	inline Action_t1264377477 ** get_address_of_U3ConStartU3Ek__BackingField_22() { return &___U3ConStartU3Ek__BackingField_22; }
	inline void set_U3ConStartU3Ek__BackingField_22(Action_t1264377477 * value)
	{
		___U3ConStartU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3ConStartU3Ek__BackingField_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTDESCROPTIONAL_T4257087022_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef CHARMTYPE_T3822141316_H
#define CHARMTYPE_T3822141316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmType
struct  CharmType_t3822141316 
{
public:
	// System.Int32 CharmType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharmType_t3822141316, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMTYPE_T3822141316_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef GHOSTGRADE_T2887092049_H
#define GHOSTGRADE_T2887092049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostGrade
struct  GhostGrade_t2887092049 
{
public:
	// System.Int32 GhostGrade::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GhostGrade_t2887092049, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTGRADE_T2887092049_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef LEANAUDIOOPTIONS_T2919880816_H
#define LEANAUDIOOPTIONS_T2919880816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanAudioOptions
struct  LeanAudioOptions_t2919880816  : public RuntimeObject
{
public:
	// LeanAudioOptions/LeanAudioWaveStyle LeanAudioOptions::waveStyle
	int32_t ___waveStyle_0;
	// UnityEngine.Vector3[] LeanAudioOptions::vibrato
	Vector3U5BU5D_t1718750761* ___vibrato_1;
	// UnityEngine.Vector3[] LeanAudioOptions::modulation
	Vector3U5BU5D_t1718750761* ___modulation_2;
	// System.Int32 LeanAudioOptions::frequencyRate
	int32_t ___frequencyRate_3;
	// System.Single LeanAudioOptions::waveNoiseScale
	float ___waveNoiseScale_4;
	// System.Single LeanAudioOptions::waveNoiseInfluence
	float ___waveNoiseInfluence_5;
	// System.Boolean LeanAudioOptions::useSetData
	bool ___useSetData_6;
	// LeanAudioStream LeanAudioOptions::stream
	LeanAudioStream_t2242826187 * ___stream_7;

public:
	inline static int32_t get_offset_of_waveStyle_0() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___waveStyle_0)); }
	inline int32_t get_waveStyle_0() const { return ___waveStyle_0; }
	inline int32_t* get_address_of_waveStyle_0() { return &___waveStyle_0; }
	inline void set_waveStyle_0(int32_t value)
	{
		___waveStyle_0 = value;
	}

	inline static int32_t get_offset_of_vibrato_1() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___vibrato_1)); }
	inline Vector3U5BU5D_t1718750761* get_vibrato_1() const { return ___vibrato_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vibrato_1() { return &___vibrato_1; }
	inline void set_vibrato_1(Vector3U5BU5D_t1718750761* value)
	{
		___vibrato_1 = value;
		Il2CppCodeGenWriteBarrier((&___vibrato_1), value);
	}

	inline static int32_t get_offset_of_modulation_2() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___modulation_2)); }
	inline Vector3U5BU5D_t1718750761* get_modulation_2() const { return ___modulation_2; }
	inline Vector3U5BU5D_t1718750761** get_address_of_modulation_2() { return &___modulation_2; }
	inline void set_modulation_2(Vector3U5BU5D_t1718750761* value)
	{
		___modulation_2 = value;
		Il2CppCodeGenWriteBarrier((&___modulation_2), value);
	}

	inline static int32_t get_offset_of_frequencyRate_3() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___frequencyRate_3)); }
	inline int32_t get_frequencyRate_3() const { return ___frequencyRate_3; }
	inline int32_t* get_address_of_frequencyRate_3() { return &___frequencyRate_3; }
	inline void set_frequencyRate_3(int32_t value)
	{
		___frequencyRate_3 = value;
	}

	inline static int32_t get_offset_of_waveNoiseScale_4() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___waveNoiseScale_4)); }
	inline float get_waveNoiseScale_4() const { return ___waveNoiseScale_4; }
	inline float* get_address_of_waveNoiseScale_4() { return &___waveNoiseScale_4; }
	inline void set_waveNoiseScale_4(float value)
	{
		___waveNoiseScale_4 = value;
	}

	inline static int32_t get_offset_of_waveNoiseInfluence_5() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___waveNoiseInfluence_5)); }
	inline float get_waveNoiseInfluence_5() const { return ___waveNoiseInfluence_5; }
	inline float* get_address_of_waveNoiseInfluence_5() { return &___waveNoiseInfluence_5; }
	inline void set_waveNoiseInfluence_5(float value)
	{
		___waveNoiseInfluence_5 = value;
	}

	inline static int32_t get_offset_of_useSetData_6() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___useSetData_6)); }
	inline bool get_useSetData_6() const { return ___useSetData_6; }
	inline bool* get_address_of_useSetData_6() { return &___useSetData_6; }
	inline void set_useSetData_6(bool value)
	{
		___useSetData_6 = value;
	}

	inline static int32_t get_offset_of_stream_7() { return static_cast<int32_t>(offsetof(LeanAudioOptions_t2919880816, ___stream_7)); }
	inline LeanAudioStream_t2242826187 * get_stream_7() const { return ___stream_7; }
	inline LeanAudioStream_t2242826187 ** get_address_of_stream_7() { return &___stream_7; }
	inline void set_stream_7(LeanAudioStream_t2242826187 * value)
	{
		___stream_7 = value;
		Il2CppCodeGenWriteBarrier((&___stream_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANAUDIOOPTIONS_T2919880816_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef LTDESCR_T2043587347_H
#define LTDESCR_T2043587347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTDescr
struct  LTDescr_t2043587347  : public RuntimeObject
{
public:
	// System.Boolean LTDescr::toggle
	bool ___toggle_0;
	// System.Boolean LTDescr::useEstimatedTime
	bool ___useEstimatedTime_1;
	// System.Boolean LTDescr::useFrames
	bool ___useFrames_2;
	// System.Boolean LTDescr::useManualTime
	bool ___useManualTime_3;
	// System.Boolean LTDescr::usesNormalDt
	bool ___usesNormalDt_4;
	// System.Boolean LTDescr::hasInitiliazed
	bool ___hasInitiliazed_5;
	// System.Boolean LTDescr::hasExtraOnCompletes
	bool ___hasExtraOnCompletes_6;
	// System.Boolean LTDescr::hasPhysics
	bool ___hasPhysics_7;
	// System.Boolean LTDescr::onCompleteOnRepeat
	bool ___onCompleteOnRepeat_8;
	// System.Boolean LTDescr::onCompleteOnStart
	bool ___onCompleteOnStart_9;
	// System.Boolean LTDescr::useRecursion
	bool ___useRecursion_10;
	// System.Single LTDescr::ratioPassed
	float ___ratioPassed_11;
	// System.Single LTDescr::passed
	float ___passed_12;
	// System.Single LTDescr::delay
	float ___delay_13;
	// System.Single LTDescr::time
	float ___time_14;
	// System.Single LTDescr::speed
	float ___speed_15;
	// System.Single LTDescr::lastVal
	float ___lastVal_16;
	// System.UInt32 LTDescr::_id
	uint32_t ____id_17;
	// System.Int32 LTDescr::loopCount
	int32_t ___loopCount_18;
	// System.UInt32 LTDescr::counter
	uint32_t ___counter_19;
	// System.Single LTDescr::direction
	float ___direction_20;
	// System.Single LTDescr::directionLast
	float ___directionLast_21;
	// System.Single LTDescr::overshoot
	float ___overshoot_22;
	// System.Single LTDescr::period
	float ___period_23;
	// System.Single LTDescr::scale
	float ___scale_24;
	// System.Boolean LTDescr::destroyOnComplete
	bool ___destroyOnComplete_25;
	// UnityEngine.Transform LTDescr::trans
	Transform_t3600365921 * ___trans_26;
	// UnityEngine.Vector3 LTDescr::fromInternal
	Vector3_t3722313464  ___fromInternal_27;
	// UnityEngine.Vector3 LTDescr::toInternal
	Vector3_t3722313464  ___toInternal_28;
	// UnityEngine.Vector3 LTDescr::diff
	Vector3_t3722313464  ___diff_29;
	// UnityEngine.Vector3 LTDescr::diffDiv2
	Vector3_t3722313464  ___diffDiv2_30;
	// TweenAction LTDescr::type
	int32_t ___type_31;
	// LeanTweenType LTDescr::easeType
	int32_t ___easeType_32;
	// LeanTweenType LTDescr::loopType
	int32_t ___loopType_33;
	// System.Boolean LTDescr::hasUpdateCallback
	bool ___hasUpdateCallback_34;
	// LTDescr/EaseTypeDelegate LTDescr::easeMethod
	EaseTypeDelegate_t2201194898 * ___easeMethod_35;
	// LTDescr/ActionMethodDelegate LTDescr::<easeInternal>k__BackingField
	ActionMethodDelegate_t2422926000 * ___U3CeaseInternalU3Ek__BackingField_36;
	// LTDescr/ActionMethodDelegate LTDescr::<initInternal>k__BackingField
	ActionMethodDelegate_t2422926000 * ___U3CinitInternalU3Ek__BackingField_37;
	// UnityEngine.SpriteRenderer LTDescr::spriteRen
	SpriteRenderer_t3235626157 * ___spriteRen_38;
	// UnityEngine.RectTransform LTDescr::rectTransform
	RectTransform_t3704657025 * ___rectTransform_39;
	// UnityEngine.UI.Text LTDescr::uiText
	Text_t1901882714 * ___uiText_40;
	// UnityEngine.UI.Image LTDescr::uiImage
	Image_t2670269651 * ___uiImage_41;
	// UnityEngine.UI.RawImage LTDescr::rawImage
	RawImage_t3182918964 * ___rawImage_42;
	// UnityEngine.Sprite[] LTDescr::sprites
	SpriteU5BU5D_t2581906349* ___sprites_43;
	// LTDescrOptional LTDescr::_optional
	LTDescrOptional_t4257087022 * ____optional_44;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___toggle_0)); }
	inline bool get_toggle_0() const { return ___toggle_0; }
	inline bool* get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(bool value)
	{
		___toggle_0 = value;
	}

	inline static int32_t get_offset_of_useEstimatedTime_1() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___useEstimatedTime_1)); }
	inline bool get_useEstimatedTime_1() const { return ___useEstimatedTime_1; }
	inline bool* get_address_of_useEstimatedTime_1() { return &___useEstimatedTime_1; }
	inline void set_useEstimatedTime_1(bool value)
	{
		___useEstimatedTime_1 = value;
	}

	inline static int32_t get_offset_of_useFrames_2() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___useFrames_2)); }
	inline bool get_useFrames_2() const { return ___useFrames_2; }
	inline bool* get_address_of_useFrames_2() { return &___useFrames_2; }
	inline void set_useFrames_2(bool value)
	{
		___useFrames_2 = value;
	}

	inline static int32_t get_offset_of_useManualTime_3() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___useManualTime_3)); }
	inline bool get_useManualTime_3() const { return ___useManualTime_3; }
	inline bool* get_address_of_useManualTime_3() { return &___useManualTime_3; }
	inline void set_useManualTime_3(bool value)
	{
		___useManualTime_3 = value;
	}

	inline static int32_t get_offset_of_usesNormalDt_4() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___usesNormalDt_4)); }
	inline bool get_usesNormalDt_4() const { return ___usesNormalDt_4; }
	inline bool* get_address_of_usesNormalDt_4() { return &___usesNormalDt_4; }
	inline void set_usesNormalDt_4(bool value)
	{
		___usesNormalDt_4 = value;
	}

	inline static int32_t get_offset_of_hasInitiliazed_5() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___hasInitiliazed_5)); }
	inline bool get_hasInitiliazed_5() const { return ___hasInitiliazed_5; }
	inline bool* get_address_of_hasInitiliazed_5() { return &___hasInitiliazed_5; }
	inline void set_hasInitiliazed_5(bool value)
	{
		___hasInitiliazed_5 = value;
	}

	inline static int32_t get_offset_of_hasExtraOnCompletes_6() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___hasExtraOnCompletes_6)); }
	inline bool get_hasExtraOnCompletes_6() const { return ___hasExtraOnCompletes_6; }
	inline bool* get_address_of_hasExtraOnCompletes_6() { return &___hasExtraOnCompletes_6; }
	inline void set_hasExtraOnCompletes_6(bool value)
	{
		___hasExtraOnCompletes_6 = value;
	}

	inline static int32_t get_offset_of_hasPhysics_7() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___hasPhysics_7)); }
	inline bool get_hasPhysics_7() const { return ___hasPhysics_7; }
	inline bool* get_address_of_hasPhysics_7() { return &___hasPhysics_7; }
	inline void set_hasPhysics_7(bool value)
	{
		___hasPhysics_7 = value;
	}

	inline static int32_t get_offset_of_onCompleteOnRepeat_8() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___onCompleteOnRepeat_8)); }
	inline bool get_onCompleteOnRepeat_8() const { return ___onCompleteOnRepeat_8; }
	inline bool* get_address_of_onCompleteOnRepeat_8() { return &___onCompleteOnRepeat_8; }
	inline void set_onCompleteOnRepeat_8(bool value)
	{
		___onCompleteOnRepeat_8 = value;
	}

	inline static int32_t get_offset_of_onCompleteOnStart_9() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___onCompleteOnStart_9)); }
	inline bool get_onCompleteOnStart_9() const { return ___onCompleteOnStart_9; }
	inline bool* get_address_of_onCompleteOnStart_9() { return &___onCompleteOnStart_9; }
	inline void set_onCompleteOnStart_9(bool value)
	{
		___onCompleteOnStart_9 = value;
	}

	inline static int32_t get_offset_of_useRecursion_10() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___useRecursion_10)); }
	inline bool get_useRecursion_10() const { return ___useRecursion_10; }
	inline bool* get_address_of_useRecursion_10() { return &___useRecursion_10; }
	inline void set_useRecursion_10(bool value)
	{
		___useRecursion_10 = value;
	}

	inline static int32_t get_offset_of_ratioPassed_11() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___ratioPassed_11)); }
	inline float get_ratioPassed_11() const { return ___ratioPassed_11; }
	inline float* get_address_of_ratioPassed_11() { return &___ratioPassed_11; }
	inline void set_ratioPassed_11(float value)
	{
		___ratioPassed_11 = value;
	}

	inline static int32_t get_offset_of_passed_12() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___passed_12)); }
	inline float get_passed_12() const { return ___passed_12; }
	inline float* get_address_of_passed_12() { return &___passed_12; }
	inline void set_passed_12(float value)
	{
		___passed_12 = value;
	}

	inline static int32_t get_offset_of_delay_13() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___delay_13)); }
	inline float get_delay_13() const { return ___delay_13; }
	inline float* get_address_of_delay_13() { return &___delay_13; }
	inline void set_delay_13(float value)
	{
		___delay_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___speed_15)); }
	inline float get_speed_15() const { return ___speed_15; }
	inline float* get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(float value)
	{
		___speed_15 = value;
	}

	inline static int32_t get_offset_of_lastVal_16() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___lastVal_16)); }
	inline float get_lastVal_16() const { return ___lastVal_16; }
	inline float* get_address_of_lastVal_16() { return &___lastVal_16; }
	inline void set_lastVal_16(float value)
	{
		___lastVal_16 = value;
	}

	inline static int32_t get_offset_of__id_17() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ____id_17)); }
	inline uint32_t get__id_17() const { return ____id_17; }
	inline uint32_t* get_address_of__id_17() { return &____id_17; }
	inline void set__id_17(uint32_t value)
	{
		____id_17 = value;
	}

	inline static int32_t get_offset_of_loopCount_18() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___loopCount_18)); }
	inline int32_t get_loopCount_18() const { return ___loopCount_18; }
	inline int32_t* get_address_of_loopCount_18() { return &___loopCount_18; }
	inline void set_loopCount_18(int32_t value)
	{
		___loopCount_18 = value;
	}

	inline static int32_t get_offset_of_counter_19() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___counter_19)); }
	inline uint32_t get_counter_19() const { return ___counter_19; }
	inline uint32_t* get_address_of_counter_19() { return &___counter_19; }
	inline void set_counter_19(uint32_t value)
	{
		___counter_19 = value;
	}

	inline static int32_t get_offset_of_direction_20() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___direction_20)); }
	inline float get_direction_20() const { return ___direction_20; }
	inline float* get_address_of_direction_20() { return &___direction_20; }
	inline void set_direction_20(float value)
	{
		___direction_20 = value;
	}

	inline static int32_t get_offset_of_directionLast_21() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___directionLast_21)); }
	inline float get_directionLast_21() const { return ___directionLast_21; }
	inline float* get_address_of_directionLast_21() { return &___directionLast_21; }
	inline void set_directionLast_21(float value)
	{
		___directionLast_21 = value;
	}

	inline static int32_t get_offset_of_overshoot_22() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___overshoot_22)); }
	inline float get_overshoot_22() const { return ___overshoot_22; }
	inline float* get_address_of_overshoot_22() { return &___overshoot_22; }
	inline void set_overshoot_22(float value)
	{
		___overshoot_22 = value;
	}

	inline static int32_t get_offset_of_period_23() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___period_23)); }
	inline float get_period_23() const { return ___period_23; }
	inline float* get_address_of_period_23() { return &___period_23; }
	inline void set_period_23(float value)
	{
		___period_23 = value;
	}

	inline static int32_t get_offset_of_scale_24() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___scale_24)); }
	inline float get_scale_24() const { return ___scale_24; }
	inline float* get_address_of_scale_24() { return &___scale_24; }
	inline void set_scale_24(float value)
	{
		___scale_24 = value;
	}

	inline static int32_t get_offset_of_destroyOnComplete_25() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___destroyOnComplete_25)); }
	inline bool get_destroyOnComplete_25() const { return ___destroyOnComplete_25; }
	inline bool* get_address_of_destroyOnComplete_25() { return &___destroyOnComplete_25; }
	inline void set_destroyOnComplete_25(bool value)
	{
		___destroyOnComplete_25 = value;
	}

	inline static int32_t get_offset_of_trans_26() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___trans_26)); }
	inline Transform_t3600365921 * get_trans_26() const { return ___trans_26; }
	inline Transform_t3600365921 ** get_address_of_trans_26() { return &___trans_26; }
	inline void set_trans_26(Transform_t3600365921 * value)
	{
		___trans_26 = value;
		Il2CppCodeGenWriteBarrier((&___trans_26), value);
	}

	inline static int32_t get_offset_of_fromInternal_27() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___fromInternal_27)); }
	inline Vector3_t3722313464  get_fromInternal_27() const { return ___fromInternal_27; }
	inline Vector3_t3722313464 * get_address_of_fromInternal_27() { return &___fromInternal_27; }
	inline void set_fromInternal_27(Vector3_t3722313464  value)
	{
		___fromInternal_27 = value;
	}

	inline static int32_t get_offset_of_toInternal_28() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___toInternal_28)); }
	inline Vector3_t3722313464  get_toInternal_28() const { return ___toInternal_28; }
	inline Vector3_t3722313464 * get_address_of_toInternal_28() { return &___toInternal_28; }
	inline void set_toInternal_28(Vector3_t3722313464  value)
	{
		___toInternal_28 = value;
	}

	inline static int32_t get_offset_of_diff_29() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___diff_29)); }
	inline Vector3_t3722313464  get_diff_29() const { return ___diff_29; }
	inline Vector3_t3722313464 * get_address_of_diff_29() { return &___diff_29; }
	inline void set_diff_29(Vector3_t3722313464  value)
	{
		___diff_29 = value;
	}

	inline static int32_t get_offset_of_diffDiv2_30() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___diffDiv2_30)); }
	inline Vector3_t3722313464  get_diffDiv2_30() const { return ___diffDiv2_30; }
	inline Vector3_t3722313464 * get_address_of_diffDiv2_30() { return &___diffDiv2_30; }
	inline void set_diffDiv2_30(Vector3_t3722313464  value)
	{
		___diffDiv2_30 = value;
	}

	inline static int32_t get_offset_of_type_31() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___type_31)); }
	inline int32_t get_type_31() const { return ___type_31; }
	inline int32_t* get_address_of_type_31() { return &___type_31; }
	inline void set_type_31(int32_t value)
	{
		___type_31 = value;
	}

	inline static int32_t get_offset_of_easeType_32() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___easeType_32)); }
	inline int32_t get_easeType_32() const { return ___easeType_32; }
	inline int32_t* get_address_of_easeType_32() { return &___easeType_32; }
	inline void set_easeType_32(int32_t value)
	{
		___easeType_32 = value;
	}

	inline static int32_t get_offset_of_loopType_33() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___loopType_33)); }
	inline int32_t get_loopType_33() const { return ___loopType_33; }
	inline int32_t* get_address_of_loopType_33() { return &___loopType_33; }
	inline void set_loopType_33(int32_t value)
	{
		___loopType_33 = value;
	}

	inline static int32_t get_offset_of_hasUpdateCallback_34() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___hasUpdateCallback_34)); }
	inline bool get_hasUpdateCallback_34() const { return ___hasUpdateCallback_34; }
	inline bool* get_address_of_hasUpdateCallback_34() { return &___hasUpdateCallback_34; }
	inline void set_hasUpdateCallback_34(bool value)
	{
		___hasUpdateCallback_34 = value;
	}

	inline static int32_t get_offset_of_easeMethod_35() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___easeMethod_35)); }
	inline EaseTypeDelegate_t2201194898 * get_easeMethod_35() const { return ___easeMethod_35; }
	inline EaseTypeDelegate_t2201194898 ** get_address_of_easeMethod_35() { return &___easeMethod_35; }
	inline void set_easeMethod_35(EaseTypeDelegate_t2201194898 * value)
	{
		___easeMethod_35 = value;
		Il2CppCodeGenWriteBarrier((&___easeMethod_35), value);
	}

	inline static int32_t get_offset_of_U3CeaseInternalU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___U3CeaseInternalU3Ek__BackingField_36)); }
	inline ActionMethodDelegate_t2422926000 * get_U3CeaseInternalU3Ek__BackingField_36() const { return ___U3CeaseInternalU3Ek__BackingField_36; }
	inline ActionMethodDelegate_t2422926000 ** get_address_of_U3CeaseInternalU3Ek__BackingField_36() { return &___U3CeaseInternalU3Ek__BackingField_36; }
	inline void set_U3CeaseInternalU3Ek__BackingField_36(ActionMethodDelegate_t2422926000 * value)
	{
		___U3CeaseInternalU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeaseInternalU3Ek__BackingField_36), value);
	}

	inline static int32_t get_offset_of_U3CinitInternalU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___U3CinitInternalU3Ek__BackingField_37)); }
	inline ActionMethodDelegate_t2422926000 * get_U3CinitInternalU3Ek__BackingField_37() const { return ___U3CinitInternalU3Ek__BackingField_37; }
	inline ActionMethodDelegate_t2422926000 ** get_address_of_U3CinitInternalU3Ek__BackingField_37() { return &___U3CinitInternalU3Ek__BackingField_37; }
	inline void set_U3CinitInternalU3Ek__BackingField_37(ActionMethodDelegate_t2422926000 * value)
	{
		___U3CinitInternalU3Ek__BackingField_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinitInternalU3Ek__BackingField_37), value);
	}

	inline static int32_t get_offset_of_spriteRen_38() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___spriteRen_38)); }
	inline SpriteRenderer_t3235626157 * get_spriteRen_38() const { return ___spriteRen_38; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRen_38() { return &___spriteRen_38; }
	inline void set_spriteRen_38(SpriteRenderer_t3235626157 * value)
	{
		___spriteRen_38 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRen_38), value);
	}

	inline static int32_t get_offset_of_rectTransform_39() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___rectTransform_39)); }
	inline RectTransform_t3704657025 * get_rectTransform_39() const { return ___rectTransform_39; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_39() { return &___rectTransform_39; }
	inline void set_rectTransform_39(RectTransform_t3704657025 * value)
	{
		___rectTransform_39 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_39), value);
	}

	inline static int32_t get_offset_of_uiText_40() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___uiText_40)); }
	inline Text_t1901882714 * get_uiText_40() const { return ___uiText_40; }
	inline Text_t1901882714 ** get_address_of_uiText_40() { return &___uiText_40; }
	inline void set_uiText_40(Text_t1901882714 * value)
	{
		___uiText_40 = value;
		Il2CppCodeGenWriteBarrier((&___uiText_40), value);
	}

	inline static int32_t get_offset_of_uiImage_41() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___uiImage_41)); }
	inline Image_t2670269651 * get_uiImage_41() const { return ___uiImage_41; }
	inline Image_t2670269651 ** get_address_of_uiImage_41() { return &___uiImage_41; }
	inline void set_uiImage_41(Image_t2670269651 * value)
	{
		___uiImage_41 = value;
		Il2CppCodeGenWriteBarrier((&___uiImage_41), value);
	}

	inline static int32_t get_offset_of_rawImage_42() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___rawImage_42)); }
	inline RawImage_t3182918964 * get_rawImage_42() const { return ___rawImage_42; }
	inline RawImage_t3182918964 ** get_address_of_rawImage_42() { return &___rawImage_42; }
	inline void set_rawImage_42(RawImage_t3182918964 * value)
	{
		___rawImage_42 = value;
		Il2CppCodeGenWriteBarrier((&___rawImage_42), value);
	}

	inline static int32_t get_offset_of_sprites_43() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ___sprites_43)); }
	inline SpriteU5BU5D_t2581906349* get_sprites_43() const { return ___sprites_43; }
	inline SpriteU5BU5D_t2581906349** get_address_of_sprites_43() { return &___sprites_43; }
	inline void set_sprites_43(SpriteU5BU5D_t2581906349* value)
	{
		___sprites_43 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_43), value);
	}

	inline static int32_t get_offset_of__optional_44() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347, ____optional_44)); }
	inline LTDescrOptional_t4257087022 * get__optional_44() const { return ____optional_44; }
	inline LTDescrOptional_t4257087022 ** get_address_of__optional_44() { return &____optional_44; }
	inline void set__optional_44(LTDescrOptional_t4257087022 * value)
	{
		____optional_44 = value;
		Il2CppCodeGenWriteBarrier((&____optional_44), value);
	}
};

struct LTDescr_t2043587347_StaticFields
{
public:
	// System.Single LTDescr::val
	float ___val_45;
	// System.Single LTDescr::dt
	float ___dt_46;
	// UnityEngine.Vector3 LTDescr::newVect
	Vector3_t3722313464  ___newVect_47;
	// LTDescr/ActionMethodDelegate LTDescr::<>f__am$cache0
	ActionMethodDelegate_t2422926000 * ___U3CU3Ef__amU24cache0_48;
	// LTDescr/ActionMethodDelegate LTDescr::<>f__am$cache1
	ActionMethodDelegate_t2422926000 * ___U3CU3Ef__amU24cache1_49;

public:
	inline static int32_t get_offset_of_val_45() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347_StaticFields, ___val_45)); }
	inline float get_val_45() const { return ___val_45; }
	inline float* get_address_of_val_45() { return &___val_45; }
	inline void set_val_45(float value)
	{
		___val_45 = value;
	}

	inline static int32_t get_offset_of_dt_46() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347_StaticFields, ___dt_46)); }
	inline float get_dt_46() const { return ___dt_46; }
	inline float* get_address_of_dt_46() { return &___dt_46; }
	inline void set_dt_46(float value)
	{
		___dt_46 = value;
	}

	inline static int32_t get_offset_of_newVect_47() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347_StaticFields, ___newVect_47)); }
	inline Vector3_t3722313464  get_newVect_47() const { return ___newVect_47; }
	inline Vector3_t3722313464 * get_address_of_newVect_47() { return &___newVect_47; }
	inline void set_newVect_47(Vector3_t3722313464  value)
	{
		___newVect_47 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_48() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347_StaticFields, ___U3CU3Ef__amU24cache0_48)); }
	inline ActionMethodDelegate_t2422926000 * get_U3CU3Ef__amU24cache0_48() const { return ___U3CU3Ef__amU24cache0_48; }
	inline ActionMethodDelegate_t2422926000 ** get_address_of_U3CU3Ef__amU24cache0_48() { return &___U3CU3Ef__amU24cache0_48; }
	inline void set_U3CU3Ef__amU24cache0_48(ActionMethodDelegate_t2422926000 * value)
	{
		___U3CU3Ef__amU24cache0_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_48), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_49() { return static_cast<int32_t>(offsetof(LTDescr_t2043587347_StaticFields, ___U3CU3Ef__amU24cache1_49)); }
	inline ActionMethodDelegate_t2422926000 * get_U3CU3Ef__amU24cache1_49() const { return ___U3CU3Ef__amU24cache1_49; }
	inline ActionMethodDelegate_t2422926000 ** get_address_of_U3CU3Ef__amU24cache1_49() { return &___U3CU3Ef__amU24cache1_49; }
	inline void set_U3CU3Ef__amU24cache1_49(ActionMethodDelegate_t2422926000 * value)
	{
		___U3CU3Ef__amU24cache1_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTDESCR_T2043587347_H
#ifndef U3CSHOTBUTTONU3EC__ITERATOR2_T1510887057_H
#define U3CSHOTBUTTONU3EC__ITERATOR2_T1510887057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager/<ShotButton>c__Iterator2
struct  U3CShotButtonU3Ec__Iterator2_t1510887057  : public RuntimeObject
{
public:
	// UnityEngine.RaycastHit GameManager/<ShotButton>c__Iterator2::<hit>__0
	RaycastHit_t1056001966  ___U3ChitU3E__0_0;
	// GameManager GameManager/<ShotButton>c__Iterator2::$this
	GameManager_t1536523654 * ___U24this_1;
	// System.Object GameManager/<ShotButton>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GameManager/<ShotButton>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameManager/<ShotButton>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3ChitU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShotButtonU3Ec__Iterator2_t1510887057, ___U3ChitU3E__0_0)); }
	inline RaycastHit_t1056001966  get_U3ChitU3E__0_0() const { return ___U3ChitU3E__0_0; }
	inline RaycastHit_t1056001966 * get_address_of_U3ChitU3E__0_0() { return &___U3ChitU3E__0_0; }
	inline void set_U3ChitU3E__0_0(RaycastHit_t1056001966  value)
	{
		___U3ChitU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShotButtonU3Ec__Iterator2_t1510887057, ___U24this_1)); }
	inline GameManager_t1536523654 * get_U24this_1() const { return ___U24this_1; }
	inline GameManager_t1536523654 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameManager_t1536523654 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShotButtonU3Ec__Iterator2_t1510887057, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShotButtonU3Ec__Iterator2_t1510887057, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShotButtonU3Ec__Iterator2_t1510887057, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOTBUTTONU3EC__ITERATOR2_T1510887057_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef LTRECT_T2883103509_H
#define LTRECT_T2883103509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTRect
struct  LTRect_t2883103509  : public RuntimeObject
{
public:
	// UnityEngine.Rect LTRect::_rect
	Rect_t2360479859  ____rect_0;
	// System.Single LTRect::alpha
	float ___alpha_1;
	// System.Single LTRect::rotation
	float ___rotation_2;
	// UnityEngine.Vector2 LTRect::pivot
	Vector2_t2156229523  ___pivot_3;
	// UnityEngine.Vector2 LTRect::margin
	Vector2_t2156229523  ___margin_4;
	// UnityEngine.Rect LTRect::relativeRect
	Rect_t2360479859  ___relativeRect_5;
	// System.Boolean LTRect::rotateEnabled
	bool ___rotateEnabled_6;
	// System.Boolean LTRect::rotateFinished
	bool ___rotateFinished_7;
	// System.Boolean LTRect::alphaEnabled
	bool ___alphaEnabled_8;
	// System.String LTRect::labelStr
	String_t* ___labelStr_9;
	// LTGUI/Element_Type LTRect::type
	int32_t ___type_10;
	// UnityEngine.GUIStyle LTRect::style
	GUIStyle_t3956901511 * ___style_11;
	// System.Boolean LTRect::useColor
	bool ___useColor_12;
	// UnityEngine.Color LTRect::color
	Color_t2555686324  ___color_13;
	// System.Boolean LTRect::fontScaleToFit
	bool ___fontScaleToFit_14;
	// System.Boolean LTRect::useSimpleScale
	bool ___useSimpleScale_15;
	// System.Boolean LTRect::sizeByHeight
	bool ___sizeByHeight_16;
	// UnityEngine.Texture LTRect::texture
	Texture_t3661962703 * ___texture_17;
	// System.Int32 LTRect::_id
	int32_t ____id_18;
	// System.Int32 LTRect::counter
	int32_t ___counter_19;

public:
	inline static int32_t get_offset_of__rect_0() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ____rect_0)); }
	inline Rect_t2360479859  get__rect_0() const { return ____rect_0; }
	inline Rect_t2360479859 * get_address_of__rect_0() { return &____rect_0; }
	inline void set__rect_0(Rect_t2360479859  value)
	{
		____rect_0 = value;
	}

	inline static int32_t get_offset_of_alpha_1() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___alpha_1)); }
	inline float get_alpha_1() const { return ___alpha_1; }
	inline float* get_address_of_alpha_1() { return &___alpha_1; }
	inline void set_alpha_1(float value)
	{
		___alpha_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_pivot_3() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___pivot_3)); }
	inline Vector2_t2156229523  get_pivot_3() const { return ___pivot_3; }
	inline Vector2_t2156229523 * get_address_of_pivot_3() { return &___pivot_3; }
	inline void set_pivot_3(Vector2_t2156229523  value)
	{
		___pivot_3 = value;
	}

	inline static int32_t get_offset_of_margin_4() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___margin_4)); }
	inline Vector2_t2156229523  get_margin_4() const { return ___margin_4; }
	inline Vector2_t2156229523 * get_address_of_margin_4() { return &___margin_4; }
	inline void set_margin_4(Vector2_t2156229523  value)
	{
		___margin_4 = value;
	}

	inline static int32_t get_offset_of_relativeRect_5() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___relativeRect_5)); }
	inline Rect_t2360479859  get_relativeRect_5() const { return ___relativeRect_5; }
	inline Rect_t2360479859 * get_address_of_relativeRect_5() { return &___relativeRect_5; }
	inline void set_relativeRect_5(Rect_t2360479859  value)
	{
		___relativeRect_5 = value;
	}

	inline static int32_t get_offset_of_rotateEnabled_6() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___rotateEnabled_6)); }
	inline bool get_rotateEnabled_6() const { return ___rotateEnabled_6; }
	inline bool* get_address_of_rotateEnabled_6() { return &___rotateEnabled_6; }
	inline void set_rotateEnabled_6(bool value)
	{
		___rotateEnabled_6 = value;
	}

	inline static int32_t get_offset_of_rotateFinished_7() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___rotateFinished_7)); }
	inline bool get_rotateFinished_7() const { return ___rotateFinished_7; }
	inline bool* get_address_of_rotateFinished_7() { return &___rotateFinished_7; }
	inline void set_rotateFinished_7(bool value)
	{
		___rotateFinished_7 = value;
	}

	inline static int32_t get_offset_of_alphaEnabled_8() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___alphaEnabled_8)); }
	inline bool get_alphaEnabled_8() const { return ___alphaEnabled_8; }
	inline bool* get_address_of_alphaEnabled_8() { return &___alphaEnabled_8; }
	inline void set_alphaEnabled_8(bool value)
	{
		___alphaEnabled_8 = value;
	}

	inline static int32_t get_offset_of_labelStr_9() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___labelStr_9)); }
	inline String_t* get_labelStr_9() const { return ___labelStr_9; }
	inline String_t** get_address_of_labelStr_9() { return &___labelStr_9; }
	inline void set_labelStr_9(String_t* value)
	{
		___labelStr_9 = value;
		Il2CppCodeGenWriteBarrier((&___labelStr_9), value);
	}

	inline static int32_t get_offset_of_type_10() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___type_10)); }
	inline int32_t get_type_10() const { return ___type_10; }
	inline int32_t* get_address_of_type_10() { return &___type_10; }
	inline void set_type_10(int32_t value)
	{
		___type_10 = value;
	}

	inline static int32_t get_offset_of_style_11() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___style_11)); }
	inline GUIStyle_t3956901511 * get_style_11() const { return ___style_11; }
	inline GUIStyle_t3956901511 ** get_address_of_style_11() { return &___style_11; }
	inline void set_style_11(GUIStyle_t3956901511 * value)
	{
		___style_11 = value;
		Il2CppCodeGenWriteBarrier((&___style_11), value);
	}

	inline static int32_t get_offset_of_useColor_12() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___useColor_12)); }
	inline bool get_useColor_12() const { return ___useColor_12; }
	inline bool* get_address_of_useColor_12() { return &___useColor_12; }
	inline void set_useColor_12(bool value)
	{
		___useColor_12 = value;
	}

	inline static int32_t get_offset_of_color_13() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___color_13)); }
	inline Color_t2555686324  get_color_13() const { return ___color_13; }
	inline Color_t2555686324 * get_address_of_color_13() { return &___color_13; }
	inline void set_color_13(Color_t2555686324  value)
	{
		___color_13 = value;
	}

	inline static int32_t get_offset_of_fontScaleToFit_14() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___fontScaleToFit_14)); }
	inline bool get_fontScaleToFit_14() const { return ___fontScaleToFit_14; }
	inline bool* get_address_of_fontScaleToFit_14() { return &___fontScaleToFit_14; }
	inline void set_fontScaleToFit_14(bool value)
	{
		___fontScaleToFit_14 = value;
	}

	inline static int32_t get_offset_of_useSimpleScale_15() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___useSimpleScale_15)); }
	inline bool get_useSimpleScale_15() const { return ___useSimpleScale_15; }
	inline bool* get_address_of_useSimpleScale_15() { return &___useSimpleScale_15; }
	inline void set_useSimpleScale_15(bool value)
	{
		___useSimpleScale_15 = value;
	}

	inline static int32_t get_offset_of_sizeByHeight_16() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___sizeByHeight_16)); }
	inline bool get_sizeByHeight_16() const { return ___sizeByHeight_16; }
	inline bool* get_address_of_sizeByHeight_16() { return &___sizeByHeight_16; }
	inline void set_sizeByHeight_16(bool value)
	{
		___sizeByHeight_16 = value;
	}

	inline static int32_t get_offset_of_texture_17() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___texture_17)); }
	inline Texture_t3661962703 * get_texture_17() const { return ___texture_17; }
	inline Texture_t3661962703 ** get_address_of_texture_17() { return &___texture_17; }
	inline void set_texture_17(Texture_t3661962703 * value)
	{
		___texture_17 = value;
		Il2CppCodeGenWriteBarrier((&___texture_17), value);
	}

	inline static int32_t get_offset_of__id_18() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ____id_18)); }
	inline int32_t get__id_18() const { return ____id_18; }
	inline int32_t* get_address_of__id_18() { return &____id_18; }
	inline void set__id_18(int32_t value)
	{
		____id_18 = value;
	}

	inline static int32_t get_offset_of_counter_19() { return static_cast<int32_t>(offsetof(LTRect_t2883103509, ___counter_19)); }
	inline int32_t get_counter_19() const { return ___counter_19; }
	inline int32_t* get_address_of_counter_19() { return &___counter_19; }
	inline void set_counter_19(int32_t value)
	{
		___counter_19 = value;
	}
};

struct LTRect_t2883103509_StaticFields
{
public:
	// System.Boolean LTRect::colorTouched
	bool ___colorTouched_20;

public:
	inline static int32_t get_offset_of_colorTouched_20() { return static_cast<int32_t>(offsetof(LTRect_t2883103509_StaticFields, ___colorTouched_20)); }
	inline bool get_colorTouched_20() const { return ___colorTouched_20; }
	inline bool* get_address_of_colorTouched_20() { return &___colorTouched_20; }
	inline void set_colorTouched_20(bool value)
	{
		___colorTouched_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LTRECT_T2883103509_H
#ifndef CHARM_T3703444127_H
#define CHARM_T3703444127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Charm
struct  Charm_t3703444127  : public ScriptableObject_t2528358522
{
public:
	// CharmType Charm::charmType
	int32_t ___charmType_2;
	// System.String Charm::name
	String_t* ___name_3;
	// UnityEngine.Sprite Charm::image
	Sprite_t280657092 * ___image_4;
	// System.Int32 Charm::damage
	int32_t ___damage_5;
	// System.Int32 Charm::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_charmType_2() { return static_cast<int32_t>(offsetof(Charm_t3703444127, ___charmType_2)); }
	inline int32_t get_charmType_2() const { return ___charmType_2; }
	inline int32_t* get_address_of_charmType_2() { return &___charmType_2; }
	inline void set_charmType_2(int32_t value)
	{
		___charmType_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Charm_t3703444127, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(Charm_t3703444127, ___image_4)); }
	inline Sprite_t280657092 * get_image_4() const { return ___image_4; }
	inline Sprite_t280657092 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Sprite_t280657092 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_damage_5() { return static_cast<int32_t>(offsetof(Charm_t3703444127, ___damage_5)); }
	inline int32_t get_damage_5() const { return ___damage_5; }
	inline int32_t* get_address_of_damage_5() { return &___damage_5; }
	inline void set_damage_5(int32_t value)
	{
		___damage_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(Charm_t3703444127, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARM_T3703444127_H
#ifndef GHOST_T201992404_H
#define GHOST_T201992404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ghost
struct  Ghost_t201992404  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.GameObject Ghost::monsterPrefab
	GameObject_t1113636619 * ___monsterPrefab_2;
	// Spine.Unity.SkeletonDataAsset Ghost::SD_DataAsset
	SkeletonDataAsset_t3748144825 * ___SD_DataAsset_3;
	// System.String Ghost::name
	String_t* ___name_4;
	// System.String Ghost::description
	String_t* ___description_5;
	// GhostGrade Ghost::grade
	int32_t ___grade_6;
	// System.Int32 Ghost::hp
	int32_t ___hp_7;
	// UnityEngine.Vector3 Ghost::VR_Mode_Pos
	Vector3_t3722313464  ___VR_Mode_Pos_8;
	// UnityEngine.Vector3 Ghost::VR_Mode_Scale
	Vector3_t3722313464  ___VR_Mode_Scale_9;

public:
	inline static int32_t get_offset_of_monsterPrefab_2() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___monsterPrefab_2)); }
	inline GameObject_t1113636619 * get_monsterPrefab_2() const { return ___monsterPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_monsterPrefab_2() { return &___monsterPrefab_2; }
	inline void set_monsterPrefab_2(GameObject_t1113636619 * value)
	{
		___monsterPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___monsterPrefab_2), value);
	}

	inline static int32_t get_offset_of_SD_DataAsset_3() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___SD_DataAsset_3)); }
	inline SkeletonDataAsset_t3748144825 * get_SD_DataAsset_3() const { return ___SD_DataAsset_3; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_SD_DataAsset_3() { return &___SD_DataAsset_3; }
	inline void set_SD_DataAsset_3(SkeletonDataAsset_t3748144825 * value)
	{
		___SD_DataAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___SD_DataAsset_3), value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_description_5() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___description_5)); }
	inline String_t* get_description_5() const { return ___description_5; }
	inline String_t** get_address_of_description_5() { return &___description_5; }
	inline void set_description_5(String_t* value)
	{
		___description_5 = value;
		Il2CppCodeGenWriteBarrier((&___description_5), value);
	}

	inline static int32_t get_offset_of_grade_6() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___grade_6)); }
	inline int32_t get_grade_6() const { return ___grade_6; }
	inline int32_t* get_address_of_grade_6() { return &___grade_6; }
	inline void set_grade_6(int32_t value)
	{
		___grade_6 = value;
	}

	inline static int32_t get_offset_of_hp_7() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___hp_7)); }
	inline int32_t get_hp_7() const { return ___hp_7; }
	inline int32_t* get_address_of_hp_7() { return &___hp_7; }
	inline void set_hp_7(int32_t value)
	{
		___hp_7 = value;
	}

	inline static int32_t get_offset_of_VR_Mode_Pos_8() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___VR_Mode_Pos_8)); }
	inline Vector3_t3722313464  get_VR_Mode_Pos_8() const { return ___VR_Mode_Pos_8; }
	inline Vector3_t3722313464 * get_address_of_VR_Mode_Pos_8() { return &___VR_Mode_Pos_8; }
	inline void set_VR_Mode_Pos_8(Vector3_t3722313464  value)
	{
		___VR_Mode_Pos_8 = value;
	}

	inline static int32_t get_offset_of_VR_Mode_Scale_9() { return static_cast<int32_t>(offsetof(Ghost_t201992404, ___VR_Mode_Scale_9)); }
	inline Vector3_t3722313464  get_VR_Mode_Scale_9() const { return ___VR_Mode_Scale_9; }
	inline Vector3_t3722313464 * get_address_of_VR_Mode_Scale_9() { return &___VR_Mode_Scale_9; }
	inline void set_VR_Mode_Scale_9(Vector3_t3722313464  value)
	{
		___VR_Mode_Scale_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOST_T201992404_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ACTIONMETHODDELEGATE_T2422926000_H
#define ACTIONMETHODDELEGATE_T2422926000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTDescr/ActionMethodDelegate
struct  ActionMethodDelegate_t2422926000  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONMETHODDELEGATE_T2422926000_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef EASETYPEDELEGATE_T2201194898_H
#define EASETYPEDELEGATE_T2201194898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LTDescr/EaseTypeDelegate
struct  EaseTypeDelegate_t2201194898  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPEDELEGATE_T2201194898_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_3)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef BASESCENE_T2918414559_H
#define BASESCENE_T2918414559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene
struct  BaseScene_t2918414559  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas GameVanilla.Core.BaseScene::canvas
	Canvas_t3310196443 * ___canvas_2;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPopups
	Stack_1_t1957026074 * ___currentPopups_3;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPanels
	Stack_1_t1957026074 * ___currentPanels_4;

public:
	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___canvas_2)); }
	inline Canvas_t3310196443 * get_canvas_2() const { return ___canvas_2; }
	inline Canvas_t3310196443 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(Canvas_t3310196443 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}

	inline static int32_t get_offset_of_currentPopups_3() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPopups_3)); }
	inline Stack_1_t1957026074 * get_currentPopups_3() const { return ___currentPopups_3; }
	inline Stack_1_t1957026074 ** get_address_of_currentPopups_3() { return &___currentPopups_3; }
	inline void set_currentPopups_3(Stack_1_t1957026074 * value)
	{
		___currentPopups_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentPopups_3), value);
	}

	inline static int32_t get_offset_of_currentPanels_4() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPanels_4)); }
	inline Stack_1_t1957026074 * get_currentPanels_4() const { return ___currentPanels_4; }
	inline Stack_1_t1957026074 ** get_address_of_currentPanels_4() { return &___currentPanels_4; }
	inline void set_currentPanels_4(Stack_1_t1957026074 * value)
	{
		___currentPanels_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanels_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCENE_T2918414559_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_2;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_3;

public:
	inline static int32_t get_offset_of_m_Trigger_2() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_2)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_2() const { return ___m_Trigger_2; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_2() { return &___m_Trigger_2; }
	inline void set_m_Trigger_2(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_2), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_3() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_3)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_3() const { return ___m_EventPayload_3; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_3() { return &___m_EventPayload_3; }
	inline void set_m_EventPayload_3(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef CAMGYRO_T2619086136_H
#define CAMGYRO_T2619086136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamGyro
struct  CamGyro_t2619086136  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CamGyro::camParent
	GameObject_t1113636619 * ___camParent_2;

public:
	inline static int32_t get_offset_of_camParent_2() { return static_cast<int32_t>(offsetof(CamGyro_t2619086136, ___camParent_2)); }
	inline GameObject_t1113636619 * get_camParent_2() const { return ___camParent_2; }
	inline GameObject_t1113636619 ** get_address_of_camParent_2() { return &___camParent_2; }
	inline void set_camParent_2(GameObject_t1113636619 * value)
	{
		___camParent_2 = value;
		Il2CppCodeGenWriteBarrier((&___camParent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMGYRO_T2619086136_H
#ifndef CHARMGAMEMANAGER_T204340045_H
#define CHARMGAMEMANAGER_T204340045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmGameManager
struct  CharmGameManager_t204340045  : public MonoBehaviour_t3962482529
{
public:
	// CharmGameState CharmGameManager::gameState
	int32_t ___gameState_2;
	// CharmTrigger[] CharmGameManager::CharmList
	CharmTriggerU5BU5D_t1898505999* ___CharmList_3;
	// System.Int32 CharmGameManager::targetCharmIdx
	int32_t ___targetCharmIdx_4;
	// System.Int32 CharmGameManager::newCharmIndex
	int32_t ___newCharmIndex_5;
	// UnityEngine.GameObject CharmGameManager::charmPrefab
	GameObject_t1113636619 * ___charmPrefab_6;
	// System.Int32 CharmGameManager::enabledCharmCount
	int32_t ___enabledCharmCount_7;
	// RemainCharmUI CharmGameManager::remainCharm_UI
	RemainCharmUI_t475548631 * ___remainCharm_UI_8;
	// UnityEngine.Transform CharmGameManager::pointer
	Transform_t3600365921 * ___pointer_9;
	// MoveDirection CharmGameManager::moveDirection
	int32_t ___moveDirection_10;
	// UnityEngine.GameObject CharmGameManager::touchArea
	GameObject_t1113636619 * ___touchArea_11;
	// GhostManager CharmGameManager::ghostManager
	GhostManager_t3188635936 * ___ghostManager_12;
	// GhostInfoUI CharmGameManager::ghostInfoUI
	GhostInfoUI_t4215450973 * ___ghostInfoUI_13;
	// UnityEngine.GameObject CharmGameManager::GameMode_UI
	GameObject_t1113636619 * ___GameMode_UI_14;
	// LastAttackManager CharmGameManager::LastAttack_UI
	LastAttackManager_t490974840 * ___LastAttack_UI_15;
	// ResultUI CharmGameManager::Result_UI
	ResultUI_t719777132 * ___Result_UI_16;
	// UnityEngine.GameObject CharmGameManager::GameOverPopup
	GameObject_t1113636619 * ___GameOverPopup_17;
	// UnityEngine.GameObject CharmGameManager::ConfirmOutPopup
	GameObject_t1113636619 * ___ConfirmOutPopup_18;
	// UnityEngine.GameObject CharmGameManager::RefillPopup
	GameObject_t1113636619 * ___RefillPopup_19;
	// ChangeCharmPopup CharmGameManager::ChangeCharmPopup
	ChangeCharmPopup_t1980703935 * ___ChangeCharmPopup_20;
	// UnityEngine.GameObject CharmGameManager::damagePopText
	GameObject_t1113636619 * ___damagePopText_21;
	// UnityEngine.UI.Text CharmGameManager::timeText
	Text_t1901882714 * ___timeText_22;
	// System.Boolean CharmGameManager::stopTime
	bool ___stopTime_23;

public:
	inline static int32_t get_offset_of_gameState_2() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___gameState_2)); }
	inline int32_t get_gameState_2() const { return ___gameState_2; }
	inline int32_t* get_address_of_gameState_2() { return &___gameState_2; }
	inline void set_gameState_2(int32_t value)
	{
		___gameState_2 = value;
	}

	inline static int32_t get_offset_of_CharmList_3() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___CharmList_3)); }
	inline CharmTriggerU5BU5D_t1898505999* get_CharmList_3() const { return ___CharmList_3; }
	inline CharmTriggerU5BU5D_t1898505999** get_address_of_CharmList_3() { return &___CharmList_3; }
	inline void set_CharmList_3(CharmTriggerU5BU5D_t1898505999* value)
	{
		___CharmList_3 = value;
		Il2CppCodeGenWriteBarrier((&___CharmList_3), value);
	}

	inline static int32_t get_offset_of_targetCharmIdx_4() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___targetCharmIdx_4)); }
	inline int32_t get_targetCharmIdx_4() const { return ___targetCharmIdx_4; }
	inline int32_t* get_address_of_targetCharmIdx_4() { return &___targetCharmIdx_4; }
	inline void set_targetCharmIdx_4(int32_t value)
	{
		___targetCharmIdx_4 = value;
	}

	inline static int32_t get_offset_of_newCharmIndex_5() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___newCharmIndex_5)); }
	inline int32_t get_newCharmIndex_5() const { return ___newCharmIndex_5; }
	inline int32_t* get_address_of_newCharmIndex_5() { return &___newCharmIndex_5; }
	inline void set_newCharmIndex_5(int32_t value)
	{
		___newCharmIndex_5 = value;
	}

	inline static int32_t get_offset_of_charmPrefab_6() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___charmPrefab_6)); }
	inline GameObject_t1113636619 * get_charmPrefab_6() const { return ___charmPrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_charmPrefab_6() { return &___charmPrefab_6; }
	inline void set_charmPrefab_6(GameObject_t1113636619 * value)
	{
		___charmPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___charmPrefab_6), value);
	}

	inline static int32_t get_offset_of_enabledCharmCount_7() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___enabledCharmCount_7)); }
	inline int32_t get_enabledCharmCount_7() const { return ___enabledCharmCount_7; }
	inline int32_t* get_address_of_enabledCharmCount_7() { return &___enabledCharmCount_7; }
	inline void set_enabledCharmCount_7(int32_t value)
	{
		___enabledCharmCount_7 = value;
	}

	inline static int32_t get_offset_of_remainCharm_UI_8() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___remainCharm_UI_8)); }
	inline RemainCharmUI_t475548631 * get_remainCharm_UI_8() const { return ___remainCharm_UI_8; }
	inline RemainCharmUI_t475548631 ** get_address_of_remainCharm_UI_8() { return &___remainCharm_UI_8; }
	inline void set_remainCharm_UI_8(RemainCharmUI_t475548631 * value)
	{
		___remainCharm_UI_8 = value;
		Il2CppCodeGenWriteBarrier((&___remainCharm_UI_8), value);
	}

	inline static int32_t get_offset_of_pointer_9() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___pointer_9)); }
	inline Transform_t3600365921 * get_pointer_9() const { return ___pointer_9; }
	inline Transform_t3600365921 ** get_address_of_pointer_9() { return &___pointer_9; }
	inline void set_pointer_9(Transform_t3600365921 * value)
	{
		___pointer_9 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_9), value);
	}

	inline static int32_t get_offset_of_moveDirection_10() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___moveDirection_10)); }
	inline int32_t get_moveDirection_10() const { return ___moveDirection_10; }
	inline int32_t* get_address_of_moveDirection_10() { return &___moveDirection_10; }
	inline void set_moveDirection_10(int32_t value)
	{
		___moveDirection_10 = value;
	}

	inline static int32_t get_offset_of_touchArea_11() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___touchArea_11)); }
	inline GameObject_t1113636619 * get_touchArea_11() const { return ___touchArea_11; }
	inline GameObject_t1113636619 ** get_address_of_touchArea_11() { return &___touchArea_11; }
	inline void set_touchArea_11(GameObject_t1113636619 * value)
	{
		___touchArea_11 = value;
		Il2CppCodeGenWriteBarrier((&___touchArea_11), value);
	}

	inline static int32_t get_offset_of_ghostManager_12() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___ghostManager_12)); }
	inline GhostManager_t3188635936 * get_ghostManager_12() const { return ___ghostManager_12; }
	inline GhostManager_t3188635936 ** get_address_of_ghostManager_12() { return &___ghostManager_12; }
	inline void set_ghostManager_12(GhostManager_t3188635936 * value)
	{
		___ghostManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___ghostManager_12), value);
	}

	inline static int32_t get_offset_of_ghostInfoUI_13() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___ghostInfoUI_13)); }
	inline GhostInfoUI_t4215450973 * get_ghostInfoUI_13() const { return ___ghostInfoUI_13; }
	inline GhostInfoUI_t4215450973 ** get_address_of_ghostInfoUI_13() { return &___ghostInfoUI_13; }
	inline void set_ghostInfoUI_13(GhostInfoUI_t4215450973 * value)
	{
		___ghostInfoUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___ghostInfoUI_13), value);
	}

	inline static int32_t get_offset_of_GameMode_UI_14() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___GameMode_UI_14)); }
	inline GameObject_t1113636619 * get_GameMode_UI_14() const { return ___GameMode_UI_14; }
	inline GameObject_t1113636619 ** get_address_of_GameMode_UI_14() { return &___GameMode_UI_14; }
	inline void set_GameMode_UI_14(GameObject_t1113636619 * value)
	{
		___GameMode_UI_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameMode_UI_14), value);
	}

	inline static int32_t get_offset_of_LastAttack_UI_15() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___LastAttack_UI_15)); }
	inline LastAttackManager_t490974840 * get_LastAttack_UI_15() const { return ___LastAttack_UI_15; }
	inline LastAttackManager_t490974840 ** get_address_of_LastAttack_UI_15() { return &___LastAttack_UI_15; }
	inline void set_LastAttack_UI_15(LastAttackManager_t490974840 * value)
	{
		___LastAttack_UI_15 = value;
		Il2CppCodeGenWriteBarrier((&___LastAttack_UI_15), value);
	}

	inline static int32_t get_offset_of_Result_UI_16() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___Result_UI_16)); }
	inline ResultUI_t719777132 * get_Result_UI_16() const { return ___Result_UI_16; }
	inline ResultUI_t719777132 ** get_address_of_Result_UI_16() { return &___Result_UI_16; }
	inline void set_Result_UI_16(ResultUI_t719777132 * value)
	{
		___Result_UI_16 = value;
		Il2CppCodeGenWriteBarrier((&___Result_UI_16), value);
	}

	inline static int32_t get_offset_of_GameOverPopup_17() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___GameOverPopup_17)); }
	inline GameObject_t1113636619 * get_GameOverPopup_17() const { return ___GameOverPopup_17; }
	inline GameObject_t1113636619 ** get_address_of_GameOverPopup_17() { return &___GameOverPopup_17; }
	inline void set_GameOverPopup_17(GameObject_t1113636619 * value)
	{
		___GameOverPopup_17 = value;
		Il2CppCodeGenWriteBarrier((&___GameOverPopup_17), value);
	}

	inline static int32_t get_offset_of_ConfirmOutPopup_18() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___ConfirmOutPopup_18)); }
	inline GameObject_t1113636619 * get_ConfirmOutPopup_18() const { return ___ConfirmOutPopup_18; }
	inline GameObject_t1113636619 ** get_address_of_ConfirmOutPopup_18() { return &___ConfirmOutPopup_18; }
	inline void set_ConfirmOutPopup_18(GameObject_t1113636619 * value)
	{
		___ConfirmOutPopup_18 = value;
		Il2CppCodeGenWriteBarrier((&___ConfirmOutPopup_18), value);
	}

	inline static int32_t get_offset_of_RefillPopup_19() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___RefillPopup_19)); }
	inline GameObject_t1113636619 * get_RefillPopup_19() const { return ___RefillPopup_19; }
	inline GameObject_t1113636619 ** get_address_of_RefillPopup_19() { return &___RefillPopup_19; }
	inline void set_RefillPopup_19(GameObject_t1113636619 * value)
	{
		___RefillPopup_19 = value;
		Il2CppCodeGenWriteBarrier((&___RefillPopup_19), value);
	}

	inline static int32_t get_offset_of_ChangeCharmPopup_20() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___ChangeCharmPopup_20)); }
	inline ChangeCharmPopup_t1980703935 * get_ChangeCharmPopup_20() const { return ___ChangeCharmPopup_20; }
	inline ChangeCharmPopup_t1980703935 ** get_address_of_ChangeCharmPopup_20() { return &___ChangeCharmPopup_20; }
	inline void set_ChangeCharmPopup_20(ChangeCharmPopup_t1980703935 * value)
	{
		___ChangeCharmPopup_20 = value;
		Il2CppCodeGenWriteBarrier((&___ChangeCharmPopup_20), value);
	}

	inline static int32_t get_offset_of_damagePopText_21() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___damagePopText_21)); }
	inline GameObject_t1113636619 * get_damagePopText_21() const { return ___damagePopText_21; }
	inline GameObject_t1113636619 ** get_address_of_damagePopText_21() { return &___damagePopText_21; }
	inline void set_damagePopText_21(GameObject_t1113636619 * value)
	{
		___damagePopText_21 = value;
		Il2CppCodeGenWriteBarrier((&___damagePopText_21), value);
	}

	inline static int32_t get_offset_of_timeText_22() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___timeText_22)); }
	inline Text_t1901882714 * get_timeText_22() const { return ___timeText_22; }
	inline Text_t1901882714 ** get_address_of_timeText_22() { return &___timeText_22; }
	inline void set_timeText_22(Text_t1901882714 * value)
	{
		___timeText_22 = value;
		Il2CppCodeGenWriteBarrier((&___timeText_22), value);
	}

	inline static int32_t get_offset_of_stopTime_23() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045, ___stopTime_23)); }
	inline bool get_stopTime_23() const { return ___stopTime_23; }
	inline bool* get_address_of_stopTime_23() { return &___stopTime_23; }
	inline void set_stopTime_23(bool value)
	{
		___stopTime_23 = value;
	}
};

struct CharmGameManager_t204340045_StaticFields
{
public:
	// System.Func`2<CharmTrigger,System.Boolean> CharmGameManager::<>f__am$cache0
	Func_2_t2813846145 * ___U3CU3Ef__amU24cache0_24;
	// System.Func`2<CharmTrigger,System.Boolean> CharmGameManager::<>f__am$cache1
	Func_2_t2813846145 * ___U3CU3Ef__amU24cache1_25;
	// System.Func`2<CharmTrigger,System.Boolean> CharmGameManager::<>f__am$cache2
	Func_2_t2813846145 * ___U3CU3Ef__amU24cache2_26;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_24() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045_StaticFields, ___U3CU3Ef__amU24cache0_24)); }
	inline Func_2_t2813846145 * get_U3CU3Ef__amU24cache0_24() const { return ___U3CU3Ef__amU24cache0_24; }
	inline Func_2_t2813846145 ** get_address_of_U3CU3Ef__amU24cache0_24() { return &___U3CU3Ef__amU24cache0_24; }
	inline void set_U3CU3Ef__amU24cache0_24(Func_2_t2813846145 * value)
	{
		___U3CU3Ef__amU24cache0_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_25() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045_StaticFields, ___U3CU3Ef__amU24cache1_25)); }
	inline Func_2_t2813846145 * get_U3CU3Ef__amU24cache1_25() const { return ___U3CU3Ef__amU24cache1_25; }
	inline Func_2_t2813846145 ** get_address_of_U3CU3Ef__amU24cache1_25() { return &___U3CU3Ef__amU24cache1_25; }
	inline void set_U3CU3Ef__amU24cache1_25(Func_2_t2813846145 * value)
	{
		___U3CU3Ef__amU24cache1_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_26() { return static_cast<int32_t>(offsetof(CharmGameManager_t204340045_StaticFields, ___U3CU3Ef__amU24cache2_26)); }
	inline Func_2_t2813846145 * get_U3CU3Ef__amU24cache2_26() const { return ___U3CU3Ef__amU24cache2_26; }
	inline Func_2_t2813846145 ** get_address_of_U3CU3Ef__amU24cache2_26() { return &___U3CU3Ef__amU24cache2_26; }
	inline void set_U3CU3Ef__amU24cache2_26(Func_2_t2813846145 * value)
	{
		___U3CU3Ef__amU24cache2_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMGAMEMANAGER_T204340045_H
#ifndef AUDIOMANAGER_T3267510698_H
#define AUDIOMANAGER_T3267510698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t3267510698  : public MonoBehaviour_t3962482529
{
public:
	// Sound[] AudioManager::sounds
	SoundU5BU5D_t3647937991* ___sounds_2;

public:
	inline static int32_t get_offset_of_sounds_2() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698, ___sounds_2)); }
	inline SoundU5BU5D_t3647937991* get_sounds_2() const { return ___sounds_2; }
	inline SoundU5BU5D_t3647937991** get_address_of_sounds_2() { return &___sounds_2; }
	inline void set_sounds_2(SoundU5BU5D_t3647937991* value)
	{
		___sounds_2 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_2), value);
	}
};

struct AudioManager_t3267510698_StaticFields
{
public:
	// AudioManager AudioManager::instance
	AudioManager_t3267510698 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698_StaticFields, ___instance_3)); }
	inline AudioManager_t3267510698 * get_instance_3() const { return ___instance_3; }
	inline AudioManager_t3267510698 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(AudioManager_t3267510698 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T3267510698_H
#ifndef LIGHTSENSORTEST_T2954693399_H
#define LIGHTSENSORTEST_T2954693399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightSensorTest
struct  LightSensorTest_t2954693399  : public MonoBehaviour_t3962482529
{
public:
	// LightSensorPluginScript LightSensorTest::lightSensor
	LightSensorPluginScript_t2582954711 * ___lightSensor_2;
	// UnityEngine.UI.Text LightSensorTest::text
	Text_t1901882714 * ___text_3;

public:
	inline static int32_t get_offset_of_lightSensor_2() { return static_cast<int32_t>(offsetof(LightSensorTest_t2954693399, ___lightSensor_2)); }
	inline LightSensorPluginScript_t2582954711 * get_lightSensor_2() const { return ___lightSensor_2; }
	inline LightSensorPluginScript_t2582954711 ** get_address_of_lightSensor_2() { return &___lightSensor_2; }
	inline void set_lightSensor_2(LightSensorPluginScript_t2582954711 * value)
	{
		___lightSensor_2 = value;
		Il2CppCodeGenWriteBarrier((&___lightSensor_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(LightSensorTest_t2954693399, ___text_3)); }
	inline Text_t1901882714 * get_text_3() const { return ___text_3; }
	inline Text_t1901882714 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_t1901882714 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSENSORTEST_T2954693399_H
#ifndef LIGHTSENSORPLUGINSCRIPT_T2582954711_H
#define LIGHTSENSORPLUGINSCRIPT_T2582954711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightSensorPluginScript
struct  LightSensorPluginScript_t2582954711  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AndroidJavaObject LightSensorPluginScript::activityContext
	AndroidJavaObject_t4131667876 * ___activityContext_2;
	// UnityEngine.AndroidJavaObject LightSensorPluginScript::jo
	AndroidJavaObject_t4131667876 * ___jo_3;
	// UnityEngine.AndroidJavaClass LightSensorPluginScript::activityClass
	AndroidJavaClass_t32045322 * ___activityClass_4;

public:
	inline static int32_t get_offset_of_activityContext_2() { return static_cast<int32_t>(offsetof(LightSensorPluginScript_t2582954711, ___activityContext_2)); }
	inline AndroidJavaObject_t4131667876 * get_activityContext_2() const { return ___activityContext_2; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_activityContext_2() { return &___activityContext_2; }
	inline void set_activityContext_2(AndroidJavaObject_t4131667876 * value)
	{
		___activityContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___activityContext_2), value);
	}

	inline static int32_t get_offset_of_jo_3() { return static_cast<int32_t>(offsetof(LightSensorPluginScript_t2582954711, ___jo_3)); }
	inline AndroidJavaObject_t4131667876 * get_jo_3() const { return ___jo_3; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_jo_3() { return &___jo_3; }
	inline void set_jo_3(AndroidJavaObject_t4131667876 * value)
	{
		___jo_3 = value;
		Il2CppCodeGenWriteBarrier((&___jo_3), value);
	}

	inline static int32_t get_offset_of_activityClass_4() { return static_cast<int32_t>(offsetof(LightSensorPluginScript_t2582954711, ___activityClass_4)); }
	inline AndroidJavaClass_t32045322 * get_activityClass_4() const { return ___activityClass_4; }
	inline AndroidJavaClass_t32045322 ** get_address_of_activityClass_4() { return &___activityClass_4; }
	inline void set_activityClass_4(AndroidJavaClass_t32045322 * value)
	{
		___activityClass_4 = value;
		Il2CppCodeGenWriteBarrier((&___activityClass_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSENSORPLUGINSCRIPT_T2582954711_H
#ifndef HIT_T3558828664_H
#define HIT_T3558828664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hit
struct  Hit_t3558828664  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIT_T3558828664_H
#ifndef GHOSTINFO_T3310990554_H
#define GHOSTINFO_T3310990554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostInfo
struct  GhostInfo_t3310990554  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform GhostInfo::targetPoint
	Transform_t3600365921 * ___targetPoint_2;
	// UnityEngine.Transform GhostInfo::charmTransform
	Transform_t3600365921 * ___charmTransform_3;
	// Spine.Unity.SkeletonAnimation GhostInfo::spainAnimation
	SkeletonAnimation_t3693186521 * ___spainAnimation_4;

public:
	inline static int32_t get_offset_of_targetPoint_2() { return static_cast<int32_t>(offsetof(GhostInfo_t3310990554, ___targetPoint_2)); }
	inline Transform_t3600365921 * get_targetPoint_2() const { return ___targetPoint_2; }
	inline Transform_t3600365921 ** get_address_of_targetPoint_2() { return &___targetPoint_2; }
	inline void set_targetPoint_2(Transform_t3600365921 * value)
	{
		___targetPoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetPoint_2), value);
	}

	inline static int32_t get_offset_of_charmTransform_3() { return static_cast<int32_t>(offsetof(GhostInfo_t3310990554, ___charmTransform_3)); }
	inline Transform_t3600365921 * get_charmTransform_3() const { return ___charmTransform_3; }
	inline Transform_t3600365921 ** get_address_of_charmTransform_3() { return &___charmTransform_3; }
	inline void set_charmTransform_3(Transform_t3600365921 * value)
	{
		___charmTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___charmTransform_3), value);
	}

	inline static int32_t get_offset_of_spainAnimation_4() { return static_cast<int32_t>(offsetof(GhostInfo_t3310990554, ___spainAnimation_4)); }
	inline SkeletonAnimation_t3693186521 * get_spainAnimation_4() const { return ___spainAnimation_4; }
	inline SkeletonAnimation_t3693186521 ** get_address_of_spainAnimation_4() { return &___spainAnimation_4; }
	inline void set_spainAnimation_4(SkeletonAnimation_t3693186521 * value)
	{
		___spainAnimation_4 = value;
		Il2CppCodeGenWriteBarrier((&___spainAnimation_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTINFO_T3310990554_H
#ifndef CHARMTRIGGER_T2026203018_H
#define CHARMTRIGGER_T2026203018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmTrigger
struct  CharmTrigger_t2026203018  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CharmTrigger::isSelected
	bool ___isSelected_2;
	// System.Single CharmTrigger::xPos
	float ___xPos_3;
	// UnityEngine.UI.Image CharmTrigger::image
	Image_t2670269651 * ___image_4;

public:
	inline static int32_t get_offset_of_isSelected_2() { return static_cast<int32_t>(offsetof(CharmTrigger_t2026203018, ___isSelected_2)); }
	inline bool get_isSelected_2() const { return ___isSelected_2; }
	inline bool* get_address_of_isSelected_2() { return &___isSelected_2; }
	inline void set_isSelected_2(bool value)
	{
		___isSelected_2 = value;
	}

	inline static int32_t get_offset_of_xPos_3() { return static_cast<int32_t>(offsetof(CharmTrigger_t2026203018, ___xPos_3)); }
	inline float get_xPos_3() const { return ___xPos_3; }
	inline float* get_address_of_xPos_3() { return &___xPos_3; }
	inline void set_xPos_3(float value)
	{
		___xPos_3 = value;
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(CharmTrigger_t2026203018, ___image_4)); }
	inline Image_t2670269651 * get_image_4() const { return ___image_4; }
	inline Image_t2670269651 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Image_t2670269651 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMTRIGGER_T2026203018_H
#ifndef CANDYSHOPITEM_T1546810590_H
#define CANDYSHOPITEM_T1546810590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CandyShopItem
struct  CandyShopItem_t1546810590  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CandyShopItem::count
	int32_t ___count_2;
	// UnityEngine.UI.Text CandyShopItem::titleText
	Text_t1901882714 * ___titleText_3;
	// UnityEngine.UI.Text CandyShopItem::countText
	Text_t1901882714 * ___countText_4;
	// UnityEngine.UI.Text CandyShopItem::priceText
	Text_t1901882714 * ___priceText_5;

public:
	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(CandyShopItem_t1546810590, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_titleText_3() { return static_cast<int32_t>(offsetof(CandyShopItem_t1546810590, ___titleText_3)); }
	inline Text_t1901882714 * get_titleText_3() const { return ___titleText_3; }
	inline Text_t1901882714 ** get_address_of_titleText_3() { return &___titleText_3; }
	inline void set_titleText_3(Text_t1901882714 * value)
	{
		___titleText_3 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_3), value);
	}

	inline static int32_t get_offset_of_countText_4() { return static_cast<int32_t>(offsetof(CandyShopItem_t1546810590, ___countText_4)); }
	inline Text_t1901882714 * get_countText_4() const { return ___countText_4; }
	inline Text_t1901882714 ** get_address_of_countText_4() { return &___countText_4; }
	inline void set_countText_4(Text_t1901882714 * value)
	{
		___countText_4 = value;
		Il2CppCodeGenWriteBarrier((&___countText_4), value);
	}

	inline static int32_t get_offset_of_priceText_5() { return static_cast<int32_t>(offsetof(CandyShopItem_t1546810590, ___priceText_5)); }
	inline Text_t1901882714 * get_priceText_5() const { return ___priceText_5; }
	inline Text_t1901882714 ** get_address_of_priceText_5() { return &___priceText_5; }
	inline void set_priceText_5(Text_t1901882714 * value)
	{
		___priceText_5 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANDYSHOPITEM_T1546810590_H
#ifndef LEANTWEEN_T1803894739_H
#define LEANTWEEN_T1803894739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanTween
struct  LeanTween_t1803894739  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct LeanTween_t1803894739_StaticFields
{
public:
	// System.Boolean LeanTween::throwErrors
	bool ___throwErrors_2;
	// System.Single LeanTween::tau
	float ___tau_3;
	// System.Single LeanTween::PI_DIV2
	float ___PI_DIV2_4;
	// LTSeq[] LeanTween::sequences
	LTSeqU5BU5D_t1547789195* ___sequences_5;
	// LTDescr[] LeanTween::tweens
	LTDescrU5BU5D_t547118914* ___tweens_6;
	// System.Int32[] LeanTween::tweensFinished
	Int32U5BU5D_t385246372* ___tweensFinished_7;
	// System.Int32[] LeanTween::tweensFinishedIds
	Int32U5BU5D_t385246372* ___tweensFinishedIds_8;
	// LTDescr LeanTween::tween
	LTDescr_t2043587347 * ___tween_9;
	// System.Int32 LeanTween::tweenMaxSearch
	int32_t ___tweenMaxSearch_10;
	// System.Int32 LeanTween::maxTweens
	int32_t ___maxTweens_11;
	// System.Int32 LeanTween::maxSequences
	int32_t ___maxSequences_12;
	// System.Int32 LeanTween::frameRendered
	int32_t ___frameRendered_13;
	// UnityEngine.GameObject LeanTween::_tweenEmpty
	GameObject_t1113636619 * ____tweenEmpty_14;
	// System.Single LeanTween::dtEstimated
	float ___dtEstimated_15;
	// System.Single LeanTween::dtManual
	float ___dtManual_16;
	// System.Single LeanTween::dtActual
	float ___dtActual_17;
	// System.UInt32 LeanTween::global_counter
	uint32_t ___global_counter_18;
	// System.Int32 LeanTween::i
	int32_t ___i_19;
	// System.Int32 LeanTween::j
	int32_t ___j_20;
	// System.Int32 LeanTween::finishedCnt
	int32_t ___finishedCnt_21;
	// UnityEngine.AnimationCurve LeanTween::punch
	AnimationCurve_t3046754366 * ___punch_22;
	// UnityEngine.AnimationCurve LeanTween::shake
	AnimationCurve_t3046754366 * ___shake_23;
	// System.Int32 LeanTween::maxTweenReached
	int32_t ___maxTweenReached_24;
	// System.Int32 LeanTween::startSearch
	int32_t ___startSearch_25;
	// LTDescr LeanTween::d
	LTDescr_t2043587347 * ___d_26;
	// System.Action`1<LTEvent>[] LeanTween::eventListeners
	Action_1U5BU5D_t1829790072* ___eventListeners_27;
	// UnityEngine.GameObject[] LeanTween::goListeners
	GameObjectU5BU5D_t3328599146* ___goListeners_28;
	// System.Int32 LeanTween::eventsMaxSearch
	int32_t ___eventsMaxSearch_29;
	// System.Int32 LeanTween::EVENTS_MAX
	int32_t ___EVENTS_MAX_30;
	// System.Int32 LeanTween::LISTENERS_MAX
	int32_t ___LISTENERS_MAX_31;
	// System.Int32 LeanTween::INIT_LISTENERS_MAX
	int32_t ___INIT_LISTENERS_MAX_32;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> LeanTween::<>f__mg$cache0
	UnityAction_2_t2165061829 * ___U3CU3Ef__mgU24cache0_33;

public:
	inline static int32_t get_offset_of_throwErrors_2() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___throwErrors_2)); }
	inline bool get_throwErrors_2() const { return ___throwErrors_2; }
	inline bool* get_address_of_throwErrors_2() { return &___throwErrors_2; }
	inline void set_throwErrors_2(bool value)
	{
		___throwErrors_2 = value;
	}

	inline static int32_t get_offset_of_tau_3() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___tau_3)); }
	inline float get_tau_3() const { return ___tau_3; }
	inline float* get_address_of_tau_3() { return &___tau_3; }
	inline void set_tau_3(float value)
	{
		___tau_3 = value;
	}

	inline static int32_t get_offset_of_PI_DIV2_4() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___PI_DIV2_4)); }
	inline float get_PI_DIV2_4() const { return ___PI_DIV2_4; }
	inline float* get_address_of_PI_DIV2_4() { return &___PI_DIV2_4; }
	inline void set_PI_DIV2_4(float value)
	{
		___PI_DIV2_4 = value;
	}

	inline static int32_t get_offset_of_sequences_5() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___sequences_5)); }
	inline LTSeqU5BU5D_t1547789195* get_sequences_5() const { return ___sequences_5; }
	inline LTSeqU5BU5D_t1547789195** get_address_of_sequences_5() { return &___sequences_5; }
	inline void set_sequences_5(LTSeqU5BU5D_t1547789195* value)
	{
		___sequences_5 = value;
		Il2CppCodeGenWriteBarrier((&___sequences_5), value);
	}

	inline static int32_t get_offset_of_tweens_6() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___tweens_6)); }
	inline LTDescrU5BU5D_t547118914* get_tweens_6() const { return ___tweens_6; }
	inline LTDescrU5BU5D_t547118914** get_address_of_tweens_6() { return &___tweens_6; }
	inline void set_tweens_6(LTDescrU5BU5D_t547118914* value)
	{
		___tweens_6 = value;
		Il2CppCodeGenWriteBarrier((&___tweens_6), value);
	}

	inline static int32_t get_offset_of_tweensFinished_7() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___tweensFinished_7)); }
	inline Int32U5BU5D_t385246372* get_tweensFinished_7() const { return ___tweensFinished_7; }
	inline Int32U5BU5D_t385246372** get_address_of_tweensFinished_7() { return &___tweensFinished_7; }
	inline void set_tweensFinished_7(Int32U5BU5D_t385246372* value)
	{
		___tweensFinished_7 = value;
		Il2CppCodeGenWriteBarrier((&___tweensFinished_7), value);
	}

	inline static int32_t get_offset_of_tweensFinishedIds_8() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___tweensFinishedIds_8)); }
	inline Int32U5BU5D_t385246372* get_tweensFinishedIds_8() const { return ___tweensFinishedIds_8; }
	inline Int32U5BU5D_t385246372** get_address_of_tweensFinishedIds_8() { return &___tweensFinishedIds_8; }
	inline void set_tweensFinishedIds_8(Int32U5BU5D_t385246372* value)
	{
		___tweensFinishedIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___tweensFinishedIds_8), value);
	}

	inline static int32_t get_offset_of_tween_9() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___tween_9)); }
	inline LTDescr_t2043587347 * get_tween_9() const { return ___tween_9; }
	inline LTDescr_t2043587347 ** get_address_of_tween_9() { return &___tween_9; }
	inline void set_tween_9(LTDescr_t2043587347 * value)
	{
		___tween_9 = value;
		Il2CppCodeGenWriteBarrier((&___tween_9), value);
	}

	inline static int32_t get_offset_of_tweenMaxSearch_10() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___tweenMaxSearch_10)); }
	inline int32_t get_tweenMaxSearch_10() const { return ___tweenMaxSearch_10; }
	inline int32_t* get_address_of_tweenMaxSearch_10() { return &___tweenMaxSearch_10; }
	inline void set_tweenMaxSearch_10(int32_t value)
	{
		___tweenMaxSearch_10 = value;
	}

	inline static int32_t get_offset_of_maxTweens_11() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___maxTweens_11)); }
	inline int32_t get_maxTweens_11() const { return ___maxTweens_11; }
	inline int32_t* get_address_of_maxTweens_11() { return &___maxTweens_11; }
	inline void set_maxTweens_11(int32_t value)
	{
		___maxTweens_11 = value;
	}

	inline static int32_t get_offset_of_maxSequences_12() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___maxSequences_12)); }
	inline int32_t get_maxSequences_12() const { return ___maxSequences_12; }
	inline int32_t* get_address_of_maxSequences_12() { return &___maxSequences_12; }
	inline void set_maxSequences_12(int32_t value)
	{
		___maxSequences_12 = value;
	}

	inline static int32_t get_offset_of_frameRendered_13() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___frameRendered_13)); }
	inline int32_t get_frameRendered_13() const { return ___frameRendered_13; }
	inline int32_t* get_address_of_frameRendered_13() { return &___frameRendered_13; }
	inline void set_frameRendered_13(int32_t value)
	{
		___frameRendered_13 = value;
	}

	inline static int32_t get_offset_of__tweenEmpty_14() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ____tweenEmpty_14)); }
	inline GameObject_t1113636619 * get__tweenEmpty_14() const { return ____tweenEmpty_14; }
	inline GameObject_t1113636619 ** get_address_of__tweenEmpty_14() { return &____tweenEmpty_14; }
	inline void set__tweenEmpty_14(GameObject_t1113636619 * value)
	{
		____tweenEmpty_14 = value;
		Il2CppCodeGenWriteBarrier((&____tweenEmpty_14), value);
	}

	inline static int32_t get_offset_of_dtEstimated_15() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___dtEstimated_15)); }
	inline float get_dtEstimated_15() const { return ___dtEstimated_15; }
	inline float* get_address_of_dtEstimated_15() { return &___dtEstimated_15; }
	inline void set_dtEstimated_15(float value)
	{
		___dtEstimated_15 = value;
	}

	inline static int32_t get_offset_of_dtManual_16() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___dtManual_16)); }
	inline float get_dtManual_16() const { return ___dtManual_16; }
	inline float* get_address_of_dtManual_16() { return &___dtManual_16; }
	inline void set_dtManual_16(float value)
	{
		___dtManual_16 = value;
	}

	inline static int32_t get_offset_of_dtActual_17() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___dtActual_17)); }
	inline float get_dtActual_17() const { return ___dtActual_17; }
	inline float* get_address_of_dtActual_17() { return &___dtActual_17; }
	inline void set_dtActual_17(float value)
	{
		___dtActual_17 = value;
	}

	inline static int32_t get_offset_of_global_counter_18() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___global_counter_18)); }
	inline uint32_t get_global_counter_18() const { return ___global_counter_18; }
	inline uint32_t* get_address_of_global_counter_18() { return &___global_counter_18; }
	inline void set_global_counter_18(uint32_t value)
	{
		___global_counter_18 = value;
	}

	inline static int32_t get_offset_of_i_19() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___i_19)); }
	inline int32_t get_i_19() const { return ___i_19; }
	inline int32_t* get_address_of_i_19() { return &___i_19; }
	inline void set_i_19(int32_t value)
	{
		___i_19 = value;
	}

	inline static int32_t get_offset_of_j_20() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___j_20)); }
	inline int32_t get_j_20() const { return ___j_20; }
	inline int32_t* get_address_of_j_20() { return &___j_20; }
	inline void set_j_20(int32_t value)
	{
		___j_20 = value;
	}

	inline static int32_t get_offset_of_finishedCnt_21() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___finishedCnt_21)); }
	inline int32_t get_finishedCnt_21() const { return ___finishedCnt_21; }
	inline int32_t* get_address_of_finishedCnt_21() { return &___finishedCnt_21; }
	inline void set_finishedCnt_21(int32_t value)
	{
		___finishedCnt_21 = value;
	}

	inline static int32_t get_offset_of_punch_22() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___punch_22)); }
	inline AnimationCurve_t3046754366 * get_punch_22() const { return ___punch_22; }
	inline AnimationCurve_t3046754366 ** get_address_of_punch_22() { return &___punch_22; }
	inline void set_punch_22(AnimationCurve_t3046754366 * value)
	{
		___punch_22 = value;
		Il2CppCodeGenWriteBarrier((&___punch_22), value);
	}

	inline static int32_t get_offset_of_shake_23() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___shake_23)); }
	inline AnimationCurve_t3046754366 * get_shake_23() const { return ___shake_23; }
	inline AnimationCurve_t3046754366 ** get_address_of_shake_23() { return &___shake_23; }
	inline void set_shake_23(AnimationCurve_t3046754366 * value)
	{
		___shake_23 = value;
		Il2CppCodeGenWriteBarrier((&___shake_23), value);
	}

	inline static int32_t get_offset_of_maxTweenReached_24() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___maxTweenReached_24)); }
	inline int32_t get_maxTweenReached_24() const { return ___maxTweenReached_24; }
	inline int32_t* get_address_of_maxTweenReached_24() { return &___maxTweenReached_24; }
	inline void set_maxTweenReached_24(int32_t value)
	{
		___maxTweenReached_24 = value;
	}

	inline static int32_t get_offset_of_startSearch_25() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___startSearch_25)); }
	inline int32_t get_startSearch_25() const { return ___startSearch_25; }
	inline int32_t* get_address_of_startSearch_25() { return &___startSearch_25; }
	inline void set_startSearch_25(int32_t value)
	{
		___startSearch_25 = value;
	}

	inline static int32_t get_offset_of_d_26() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___d_26)); }
	inline LTDescr_t2043587347 * get_d_26() const { return ___d_26; }
	inline LTDescr_t2043587347 ** get_address_of_d_26() { return &___d_26; }
	inline void set_d_26(LTDescr_t2043587347 * value)
	{
		___d_26 = value;
		Il2CppCodeGenWriteBarrier((&___d_26), value);
	}

	inline static int32_t get_offset_of_eventListeners_27() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___eventListeners_27)); }
	inline Action_1U5BU5D_t1829790072* get_eventListeners_27() const { return ___eventListeners_27; }
	inline Action_1U5BU5D_t1829790072** get_address_of_eventListeners_27() { return &___eventListeners_27; }
	inline void set_eventListeners_27(Action_1U5BU5D_t1829790072* value)
	{
		___eventListeners_27 = value;
		Il2CppCodeGenWriteBarrier((&___eventListeners_27), value);
	}

	inline static int32_t get_offset_of_goListeners_28() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___goListeners_28)); }
	inline GameObjectU5BU5D_t3328599146* get_goListeners_28() const { return ___goListeners_28; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_goListeners_28() { return &___goListeners_28; }
	inline void set_goListeners_28(GameObjectU5BU5D_t3328599146* value)
	{
		___goListeners_28 = value;
		Il2CppCodeGenWriteBarrier((&___goListeners_28), value);
	}

	inline static int32_t get_offset_of_eventsMaxSearch_29() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___eventsMaxSearch_29)); }
	inline int32_t get_eventsMaxSearch_29() const { return ___eventsMaxSearch_29; }
	inline int32_t* get_address_of_eventsMaxSearch_29() { return &___eventsMaxSearch_29; }
	inline void set_eventsMaxSearch_29(int32_t value)
	{
		___eventsMaxSearch_29 = value;
	}

	inline static int32_t get_offset_of_EVENTS_MAX_30() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___EVENTS_MAX_30)); }
	inline int32_t get_EVENTS_MAX_30() const { return ___EVENTS_MAX_30; }
	inline int32_t* get_address_of_EVENTS_MAX_30() { return &___EVENTS_MAX_30; }
	inline void set_EVENTS_MAX_30(int32_t value)
	{
		___EVENTS_MAX_30 = value;
	}

	inline static int32_t get_offset_of_LISTENERS_MAX_31() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___LISTENERS_MAX_31)); }
	inline int32_t get_LISTENERS_MAX_31() const { return ___LISTENERS_MAX_31; }
	inline int32_t* get_address_of_LISTENERS_MAX_31() { return &___LISTENERS_MAX_31; }
	inline void set_LISTENERS_MAX_31(int32_t value)
	{
		___LISTENERS_MAX_31 = value;
	}

	inline static int32_t get_offset_of_INIT_LISTENERS_MAX_32() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___INIT_LISTENERS_MAX_32)); }
	inline int32_t get_INIT_LISTENERS_MAX_32() const { return ___INIT_LISTENERS_MAX_32; }
	inline int32_t* get_address_of_INIT_LISTENERS_MAX_32() { return &___INIT_LISTENERS_MAX_32; }
	inline void set_INIT_LISTENERS_MAX_32(int32_t value)
	{
		___INIT_LISTENERS_MAX_32 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_33() { return static_cast<int32_t>(offsetof(LeanTween_t1803894739_StaticFields, ___U3CU3Ef__mgU24cache0_33)); }
	inline UnityAction_2_t2165061829 * get_U3CU3Ef__mgU24cache0_33() const { return ___U3CU3Ef__mgU24cache0_33; }
	inline UnityAction_2_t2165061829 ** get_address_of_U3CU3Ef__mgU24cache0_33() { return &___U3CU3Ef__mgU24cache0_33; }
	inline void set_U3CU3Ef__mgU24cache0_33(UnityAction_2_t2165061829 * value)
	{
		___U3CU3Ef__mgU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTWEEN_T1803894739_H
#ifndef SAVELOADMANAGER_T4010657880_H
#define SAVELOADMANAGER_T4010657880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveLoadManager
struct  SaveLoadManager_t4010657880  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct SaveLoadManager_t4010657880_StaticFields
{
public:
	// SaveLoadManager SaveLoadManager::Instance
	SaveLoadManager_t4010657880 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(SaveLoadManager_t4010657880_StaticFields, ___Instance_2)); }
	inline SaveLoadManager_t4010657880 * get_Instance_2() const { return ___Instance_2; }
	inline SaveLoadManager_t4010657880 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(SaveLoadManager_t4010657880 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVELOADMANAGER_T4010657880_H
#ifndef MASTERAGENT_T3288722434_H
#define MASTERAGENT_T3288722434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MasterAgent
struct  MasterAgent_t3288722434  : public MonoBehaviour_t3962482529
{
public:
	// TimeManager MasterAgent::timeManager
	TimeManager_t1960693005 * ___timeManager_3;

public:
	inline static int32_t get_offset_of_timeManager_3() { return static_cast<int32_t>(offsetof(MasterAgent_t3288722434, ___timeManager_3)); }
	inline TimeManager_t1960693005 * get_timeManager_3() const { return ___timeManager_3; }
	inline TimeManager_t1960693005 ** get_address_of_timeManager_3() { return &___timeManager_3; }
	inline void set_timeManager_3(TimeManager_t1960693005 * value)
	{
		___timeManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___timeManager_3), value);
	}
};

struct MasterAgent_t3288722434_StaticFields
{
public:
	// MasterAgent MasterAgent::Instance
	MasterAgent_t3288722434 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(MasterAgent_t3288722434_StaticFields, ___Instance_2)); }
	inline MasterAgent_t3288722434 * get_Instance_2() const { return ___Instance_2; }
	inline MasterAgent_t3288722434 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(MasterAgent_t3288722434 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASTERAGENT_T3288722434_H
#ifndef LOGINMANAGER_T1249555276_H
#define LOGINMANAGER_T1249555276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginManager
struct  LoginManager_t1249555276  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text LoginManager::debugText
	Text_t1901882714 * ___debugText_2;

public:
	inline static int32_t get_offset_of_debugText_2() { return static_cast<int32_t>(offsetof(LoginManager_t1249555276, ___debugText_2)); }
	inline Text_t1901882714 * get_debugText_2() const { return ___debugText_2; }
	inline Text_t1901882714 ** get_address_of_debugText_2() { return &___debugText_2; }
	inline void set_debugText_2(Text_t1901882714 * value)
	{
		___debugText_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugText_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINMANAGER_T1249555276_H
#ifndef LASTATTACKMANAGER_T490974840_H
#define LASTATTACKMANAGER_T490974840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LastAttackManager
struct  LastAttackManager_t490974840  : public MonoBehaviour_t3962482529
{
public:
	// CharmGameManager LastAttackManager::charmGameManager
	CharmGameManager_t204340045 * ___charmGameManager_2;
	// CharmGameState LastAttackManager::gameState
	int32_t ___gameState_3;
	// UnityEngine.GameObject LastAttackManager::lastAttack_UI
	GameObject_t1113636619 * ___lastAttack_UI_4;
	// UnityEngine.Transform LastAttackManager::pointer
	Transform_t3600365921 * ___pointer_5;
	// UnityEngine.Transform LastAttackManager::getItemArea
	Transform_t3600365921 * ___getItemArea_6;
	// UnityEngine.Transform LastAttackManager::normalArea
	Transform_t3600365921 * ___normalArea_7;
	// UnityEngine.GameObject LastAttackManager::charmPrefab
	GameObject_t1113636619 * ___charmPrefab_8;
	// ItemCharmUI LastAttackManager::itemCharm_UI
	ItemCharmUI_t437376365 * ___itemCharm_UI_9;
	// FreeItemCharmUI LastAttackManager::freeCharm_UI
	FreeItemCharmUI_t3416008829 * ___freeCharm_UI_10;

public:
	inline static int32_t get_offset_of_charmGameManager_2() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___charmGameManager_2)); }
	inline CharmGameManager_t204340045 * get_charmGameManager_2() const { return ___charmGameManager_2; }
	inline CharmGameManager_t204340045 ** get_address_of_charmGameManager_2() { return &___charmGameManager_2; }
	inline void set_charmGameManager_2(CharmGameManager_t204340045 * value)
	{
		___charmGameManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___charmGameManager_2), value);
	}

	inline static int32_t get_offset_of_gameState_3() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___gameState_3)); }
	inline int32_t get_gameState_3() const { return ___gameState_3; }
	inline int32_t* get_address_of_gameState_3() { return &___gameState_3; }
	inline void set_gameState_3(int32_t value)
	{
		___gameState_3 = value;
	}

	inline static int32_t get_offset_of_lastAttack_UI_4() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___lastAttack_UI_4)); }
	inline GameObject_t1113636619 * get_lastAttack_UI_4() const { return ___lastAttack_UI_4; }
	inline GameObject_t1113636619 ** get_address_of_lastAttack_UI_4() { return &___lastAttack_UI_4; }
	inline void set_lastAttack_UI_4(GameObject_t1113636619 * value)
	{
		___lastAttack_UI_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastAttack_UI_4), value);
	}

	inline static int32_t get_offset_of_pointer_5() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___pointer_5)); }
	inline Transform_t3600365921 * get_pointer_5() const { return ___pointer_5; }
	inline Transform_t3600365921 ** get_address_of_pointer_5() { return &___pointer_5; }
	inline void set_pointer_5(Transform_t3600365921 * value)
	{
		___pointer_5 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_5), value);
	}

	inline static int32_t get_offset_of_getItemArea_6() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___getItemArea_6)); }
	inline Transform_t3600365921 * get_getItemArea_6() const { return ___getItemArea_6; }
	inline Transform_t3600365921 ** get_address_of_getItemArea_6() { return &___getItemArea_6; }
	inline void set_getItemArea_6(Transform_t3600365921 * value)
	{
		___getItemArea_6 = value;
		Il2CppCodeGenWriteBarrier((&___getItemArea_6), value);
	}

	inline static int32_t get_offset_of_normalArea_7() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___normalArea_7)); }
	inline Transform_t3600365921 * get_normalArea_7() const { return ___normalArea_7; }
	inline Transform_t3600365921 ** get_address_of_normalArea_7() { return &___normalArea_7; }
	inline void set_normalArea_7(Transform_t3600365921 * value)
	{
		___normalArea_7 = value;
		Il2CppCodeGenWriteBarrier((&___normalArea_7), value);
	}

	inline static int32_t get_offset_of_charmPrefab_8() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___charmPrefab_8)); }
	inline GameObject_t1113636619 * get_charmPrefab_8() const { return ___charmPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_charmPrefab_8() { return &___charmPrefab_8; }
	inline void set_charmPrefab_8(GameObject_t1113636619 * value)
	{
		___charmPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___charmPrefab_8), value);
	}

	inline static int32_t get_offset_of_itemCharm_UI_9() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___itemCharm_UI_9)); }
	inline ItemCharmUI_t437376365 * get_itemCharm_UI_9() const { return ___itemCharm_UI_9; }
	inline ItemCharmUI_t437376365 ** get_address_of_itemCharm_UI_9() { return &___itemCharm_UI_9; }
	inline void set_itemCharm_UI_9(ItemCharmUI_t437376365 * value)
	{
		___itemCharm_UI_9 = value;
		Il2CppCodeGenWriteBarrier((&___itemCharm_UI_9), value);
	}

	inline static int32_t get_offset_of_freeCharm_UI_10() { return static_cast<int32_t>(offsetof(LastAttackManager_t490974840, ___freeCharm_UI_10)); }
	inline FreeItemCharmUI_t3416008829 * get_freeCharm_UI_10() const { return ___freeCharm_UI_10; }
	inline FreeItemCharmUI_t3416008829 ** get_address_of_freeCharm_UI_10() { return &___freeCharm_UI_10; }
	inline void set_freeCharm_UI_10(FreeItemCharmUI_t3416008829 * value)
	{
		___freeCharm_UI_10 = value;
		Il2CppCodeGenWriteBarrier((&___freeCharm_UI_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LASTATTACKMANAGER_T490974840_H
#ifndef GYROMANAGER_T2156710008_H
#define GYROMANAGER_T2156710008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroManager
struct  GyroManager_t2156710008  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GyroManager::isEnable
	bool ___isEnable_2;
	// System.Boolean GyroManager::gyroEnabled
	bool ___gyroEnabled_3;
	// UnityEngine.Gyroscope GyroManager::gyro
	Gyroscope_t3288342876 * ___gyro_4;
	// UnityEngine.GameObject GyroManager::cameraContainer
	GameObject_t1113636619 * ___cameraContainer_5;
	// UnityEngine.Quaternion GyroManager::rot
	Quaternion_t2301928331  ___rot_6;

public:
	inline static int32_t get_offset_of_isEnable_2() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___isEnable_2)); }
	inline bool get_isEnable_2() const { return ___isEnable_2; }
	inline bool* get_address_of_isEnable_2() { return &___isEnable_2; }
	inline void set_isEnable_2(bool value)
	{
		___isEnable_2 = value;
	}

	inline static int32_t get_offset_of_gyroEnabled_3() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___gyroEnabled_3)); }
	inline bool get_gyroEnabled_3() const { return ___gyroEnabled_3; }
	inline bool* get_address_of_gyroEnabled_3() { return &___gyroEnabled_3; }
	inline void set_gyroEnabled_3(bool value)
	{
		___gyroEnabled_3 = value;
	}

	inline static int32_t get_offset_of_gyro_4() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___gyro_4)); }
	inline Gyroscope_t3288342876 * get_gyro_4() const { return ___gyro_4; }
	inline Gyroscope_t3288342876 ** get_address_of_gyro_4() { return &___gyro_4; }
	inline void set_gyro_4(Gyroscope_t3288342876 * value)
	{
		___gyro_4 = value;
		Il2CppCodeGenWriteBarrier((&___gyro_4), value);
	}

	inline static int32_t get_offset_of_cameraContainer_5() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___cameraContainer_5)); }
	inline GameObject_t1113636619 * get_cameraContainer_5() const { return ___cameraContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_cameraContainer_5() { return &___cameraContainer_5; }
	inline void set_cameraContainer_5(GameObject_t1113636619 * value)
	{
		___cameraContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraContainer_5), value);
	}

	inline static int32_t get_offset_of_rot_6() { return static_cast<int32_t>(offsetof(GyroManager_t2156710008, ___rot_6)); }
	inline Quaternion_t2301928331  get_rot_6() const { return ___rot_6; }
	inline Quaternion_t2301928331 * get_address_of_rot_6() { return &___rot_6; }
	inline void set_rot_6(Quaternion_t2301928331  value)
	{
		___rot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROMANAGER_T2156710008_H
#ifndef GHOSTMANAGER_T3188635936_H
#define GHOSTMANAGER_T3188635936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostManager
struct  GhostManager_t3188635936  : public MonoBehaviour_t3962482529
{
public:
	// Ghost[] GhostManager::GhostList
	GhostU5BU5D_t3381597533* ___GhostList_2;
	// Ghost GhostManager::ghost
	Ghost_t201992404 * ___ghost_3;
	// UnityEngine.GameObject GhostManager::ghostGO
	GameObject_t1113636619 * ___ghostGO_4;
	// GhostInfo GhostManager::ghostInfo
	GhostInfo_t3310990554 * ___ghostInfo_5;
	// System.Boolean GhostManager::isMove
	bool ___isMove_6;
	// System.Single GhostManager::spawnRange
	float ___spawnRange_7;
	// System.Single GhostManager::spawnTime
	float ___spawnTime_8;

public:
	inline static int32_t get_offset_of_GhostList_2() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___GhostList_2)); }
	inline GhostU5BU5D_t3381597533* get_GhostList_2() const { return ___GhostList_2; }
	inline GhostU5BU5D_t3381597533** get_address_of_GhostList_2() { return &___GhostList_2; }
	inline void set_GhostList_2(GhostU5BU5D_t3381597533* value)
	{
		___GhostList_2 = value;
		Il2CppCodeGenWriteBarrier((&___GhostList_2), value);
	}

	inline static int32_t get_offset_of_ghost_3() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___ghost_3)); }
	inline Ghost_t201992404 * get_ghost_3() const { return ___ghost_3; }
	inline Ghost_t201992404 ** get_address_of_ghost_3() { return &___ghost_3; }
	inline void set_ghost_3(Ghost_t201992404 * value)
	{
		___ghost_3 = value;
		Il2CppCodeGenWriteBarrier((&___ghost_3), value);
	}

	inline static int32_t get_offset_of_ghostGO_4() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___ghostGO_4)); }
	inline GameObject_t1113636619 * get_ghostGO_4() const { return ___ghostGO_4; }
	inline GameObject_t1113636619 ** get_address_of_ghostGO_4() { return &___ghostGO_4; }
	inline void set_ghostGO_4(GameObject_t1113636619 * value)
	{
		___ghostGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___ghostGO_4), value);
	}

	inline static int32_t get_offset_of_ghostInfo_5() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___ghostInfo_5)); }
	inline GhostInfo_t3310990554 * get_ghostInfo_5() const { return ___ghostInfo_5; }
	inline GhostInfo_t3310990554 ** get_address_of_ghostInfo_5() { return &___ghostInfo_5; }
	inline void set_ghostInfo_5(GhostInfo_t3310990554 * value)
	{
		___ghostInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___ghostInfo_5), value);
	}

	inline static int32_t get_offset_of_isMove_6() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___isMove_6)); }
	inline bool get_isMove_6() const { return ___isMove_6; }
	inline bool* get_address_of_isMove_6() { return &___isMove_6; }
	inline void set_isMove_6(bool value)
	{
		___isMove_6 = value;
	}

	inline static int32_t get_offset_of_spawnRange_7() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___spawnRange_7)); }
	inline float get_spawnRange_7() const { return ___spawnRange_7; }
	inline float* get_address_of_spawnRange_7() { return &___spawnRange_7; }
	inline void set_spawnRange_7(float value)
	{
		___spawnRange_7 = value;
	}

	inline static int32_t get_offset_of_spawnTime_8() { return static_cast<int32_t>(offsetof(GhostManager_t3188635936, ___spawnTime_8)); }
	inline float get_spawnTime_8() const { return ___spawnTime_8; }
	inline float* get_address_of_spawnTime_8() { return &___spawnTime_8; }
	inline void set_spawnTime_8(float value)
	{
		___spawnTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTMANAGER_T3188635936_H
#ifndef LEANTESTER_T4080455132_H
#define LEANTESTER_T4080455132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanTester
struct  LeanTester_t4080455132  : public MonoBehaviour_t3962482529
{
public:
	// System.Single LeanTester::timeout
	float ___timeout_2;

public:
	inline static int32_t get_offset_of_timeout_2() { return static_cast<int32_t>(offsetof(LeanTester_t4080455132, ___timeout_2)); }
	inline float get_timeout_2() const { return ___timeout_2; }
	inline float* get_address_of_timeout_2() { return &___timeout_2; }
	inline void set_timeout_2(float value)
	{
		___timeout_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTESTER_T4080455132_H
#ifndef DISABLEOBJECT_T4022553226_H
#define DISABLEOBJECT_T4022553226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableObject
struct  DisableObject_t4022553226  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DisableObject::disableTime
	float ___disableTime_2;

public:
	inline static int32_t get_offset_of_disableTime_2() { return static_cast<int32_t>(offsetof(DisableObject_t4022553226, ___disableTime_2)); }
	inline float get_disableTime_2() const { return ___disableTime_2; }
	inline float* get_address_of_disableTime_2() { return &___disableTime_2; }
	inline void set_disableTime_2(float value)
	{
		___disableTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEOBJECT_T4022553226_H
#ifndef LOBBYMANAGER_T4136450456_H
#define LOBBYMANAGER_T4136450456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyManager
struct  LobbyManager_t4136450456  : public BaseScene_t2918414559
{
public:
	// UnityEngine.GameObject LobbyManager::candyShopPopup
	GameObject_t1113636619 * ___candyShopPopup_5;
	// UnityEngine.GameObject LobbyManager::candyRemainTime
	GameObject_t1113636619 * ___candyRemainTime_6;
	// UnityEngine.UI.Text LobbyManager::candyRemainTimeText
	Text_t1901882714 * ___candyRemainTimeText_7;
	// UnityEngine.UI.Text LobbyManager::candyCountText
	Text_t1901882714 * ___candyCountText_8;
	// UnityEngine.GameObject LobbyManager::liveShopPopup
	GameObject_t1113636619 * ___liveShopPopup_9;
	// UnityEngine.GameObject LobbyManager::liveRemainTime
	GameObject_t1113636619 * ___liveRemainTime_10;
	// UnityEngine.UI.Text LobbyManager::liveRemainTimeText
	Text_t1901882714 * ___liveRemainTimeText_11;
	// UnityEngine.UI.Text LobbyManager::liveCountText
	Text_t1901882714 * ___liveCountText_12;

public:
	inline static int32_t get_offset_of_candyShopPopup_5() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___candyShopPopup_5)); }
	inline GameObject_t1113636619 * get_candyShopPopup_5() const { return ___candyShopPopup_5; }
	inline GameObject_t1113636619 ** get_address_of_candyShopPopup_5() { return &___candyShopPopup_5; }
	inline void set_candyShopPopup_5(GameObject_t1113636619 * value)
	{
		___candyShopPopup_5 = value;
		Il2CppCodeGenWriteBarrier((&___candyShopPopup_5), value);
	}

	inline static int32_t get_offset_of_candyRemainTime_6() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___candyRemainTime_6)); }
	inline GameObject_t1113636619 * get_candyRemainTime_6() const { return ___candyRemainTime_6; }
	inline GameObject_t1113636619 ** get_address_of_candyRemainTime_6() { return &___candyRemainTime_6; }
	inline void set_candyRemainTime_6(GameObject_t1113636619 * value)
	{
		___candyRemainTime_6 = value;
		Il2CppCodeGenWriteBarrier((&___candyRemainTime_6), value);
	}

	inline static int32_t get_offset_of_candyRemainTimeText_7() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___candyRemainTimeText_7)); }
	inline Text_t1901882714 * get_candyRemainTimeText_7() const { return ___candyRemainTimeText_7; }
	inline Text_t1901882714 ** get_address_of_candyRemainTimeText_7() { return &___candyRemainTimeText_7; }
	inline void set_candyRemainTimeText_7(Text_t1901882714 * value)
	{
		___candyRemainTimeText_7 = value;
		Il2CppCodeGenWriteBarrier((&___candyRemainTimeText_7), value);
	}

	inline static int32_t get_offset_of_candyCountText_8() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___candyCountText_8)); }
	inline Text_t1901882714 * get_candyCountText_8() const { return ___candyCountText_8; }
	inline Text_t1901882714 ** get_address_of_candyCountText_8() { return &___candyCountText_8; }
	inline void set_candyCountText_8(Text_t1901882714 * value)
	{
		___candyCountText_8 = value;
		Il2CppCodeGenWriteBarrier((&___candyCountText_8), value);
	}

	inline static int32_t get_offset_of_liveShopPopup_9() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___liveShopPopup_9)); }
	inline GameObject_t1113636619 * get_liveShopPopup_9() const { return ___liveShopPopup_9; }
	inline GameObject_t1113636619 ** get_address_of_liveShopPopup_9() { return &___liveShopPopup_9; }
	inline void set_liveShopPopup_9(GameObject_t1113636619 * value)
	{
		___liveShopPopup_9 = value;
		Il2CppCodeGenWriteBarrier((&___liveShopPopup_9), value);
	}

	inline static int32_t get_offset_of_liveRemainTime_10() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___liveRemainTime_10)); }
	inline GameObject_t1113636619 * get_liveRemainTime_10() const { return ___liveRemainTime_10; }
	inline GameObject_t1113636619 ** get_address_of_liveRemainTime_10() { return &___liveRemainTime_10; }
	inline void set_liveRemainTime_10(GameObject_t1113636619 * value)
	{
		___liveRemainTime_10 = value;
		Il2CppCodeGenWriteBarrier((&___liveRemainTime_10), value);
	}

	inline static int32_t get_offset_of_liveRemainTimeText_11() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___liveRemainTimeText_11)); }
	inline Text_t1901882714 * get_liveRemainTimeText_11() const { return ___liveRemainTimeText_11; }
	inline Text_t1901882714 ** get_address_of_liveRemainTimeText_11() { return &___liveRemainTimeText_11; }
	inline void set_liveRemainTimeText_11(Text_t1901882714 * value)
	{
		___liveRemainTimeText_11 = value;
		Il2CppCodeGenWriteBarrier((&___liveRemainTimeText_11), value);
	}

	inline static int32_t get_offset_of_liveCountText_12() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456, ___liveCountText_12)); }
	inline Text_t1901882714 * get_liveCountText_12() const { return ___liveCountText_12; }
	inline Text_t1901882714 ** get_address_of_liveCountText_12() { return &___liveCountText_12; }
	inline void set_liveCountText_12(Text_t1901882714 * value)
	{
		___liveCountText_12 = value;
		Il2CppCodeGenWriteBarrier((&___liveCountText_12), value);
	}
};

struct LobbyManager_t4136450456_StaticFields
{
public:
	// System.Action`1<GhostSchoolPopup> LobbyManager::<>f__am$cache0
	Action_1_t1627743448 * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(LobbyManager_t4136450456_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Action_1_t1627743448 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Action_1_t1627743448 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Action_1_t1627743448 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYMANAGER_T4136450456_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public BaseScene_t2918414559
{
public:
	// GameManager/GameModeState GameManager::gameState
	int32_t ___gameState_6;
	// GyroManager GameManager::gyroManager
	GyroManager_t2156710008 * ___gyroManager_7;
	// CharmGameManager GameManager::charmGM
	CharmGameManager_t204340045 * ___charmGM_8;
	// GhostManager GameManager::ghostManager
	GhostManager_t3188635936 * ___ghostManager_9;
	// UnityEngine.GameObject GameManager::TargetTriangle
	GameObject_t1113636619 * ___TargetTriangle_10;
	// UnityEngine.SpriteRenderer GameManager::AimSpriteRenderer
	SpriteRenderer_t3235626157 * ___AimSpriteRenderer_11;
	// System.Boolean GameManager::isShowTargetTriangle
	bool ___isShowTargetTriangle_12;
	// System.Boolean GameManager::isShot
	bool ___isShot_13;
	// UnityEngine.GameObject GameManager::ShotEffect
	GameObject_t1113636619 * ___ShotEffect_14;
	// UnityEngine.GameObject GameManager::DamageEffect
	GameObject_t1113636619 * ___DamageEffect_15;
	// UnityEngine.GameObject GameManager::FadeEffect
	GameObject_t1113636619 * ___FadeEffect_16;
	// UnityEngine.GameObject GameManager::AR_Canvas
	GameObject_t1113636619 * ___AR_Canvas_17;
	// UnityEngine.Transform GameManager::VR_Area
	Transform_t3600365921 * ___VR_Area_18;
	// UnityEngine.GameObject GameManager::AR_UI
	GameObject_t1113636619 * ___AR_UI_19;
	// UnityEngine.GameObject GameManager::VR_UI
	GameObject_t1113636619 * ___VR_UI_20;
	// UnityEngine.GameObject GameManager::takePhotoUI
	GameObject_t1113636619 * ___takePhotoUI_21;
	// UnityEngine.GameObject GameManager::takePhotoButton
	GameObject_t1113636619 * ___takePhotoButton_22;
	// UnityEngine.GameObject GameManager::candyTutorialButton
	GameObject_t1113636619 * ___candyTutorialButton_23;
	// Charm[] GameManager::charmList
	CharmU5BU5D_t2980613126* ___charmList_24;
	// UnityEngine.Transform GameManager::ghostSpawn
	Transform_t3600365921 * ___ghostSpawn_25;
	// System.Boolean GameManager::isFirst
	bool ___isFirst_26;

public:
	inline static int32_t get_offset_of_gameState_6() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___gameState_6)); }
	inline int32_t get_gameState_6() const { return ___gameState_6; }
	inline int32_t* get_address_of_gameState_6() { return &___gameState_6; }
	inline void set_gameState_6(int32_t value)
	{
		___gameState_6 = value;
	}

	inline static int32_t get_offset_of_gyroManager_7() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___gyroManager_7)); }
	inline GyroManager_t2156710008 * get_gyroManager_7() const { return ___gyroManager_7; }
	inline GyroManager_t2156710008 ** get_address_of_gyroManager_7() { return &___gyroManager_7; }
	inline void set_gyroManager_7(GyroManager_t2156710008 * value)
	{
		___gyroManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___gyroManager_7), value);
	}

	inline static int32_t get_offset_of_charmGM_8() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___charmGM_8)); }
	inline CharmGameManager_t204340045 * get_charmGM_8() const { return ___charmGM_8; }
	inline CharmGameManager_t204340045 ** get_address_of_charmGM_8() { return &___charmGM_8; }
	inline void set_charmGM_8(CharmGameManager_t204340045 * value)
	{
		___charmGM_8 = value;
		Il2CppCodeGenWriteBarrier((&___charmGM_8), value);
	}

	inline static int32_t get_offset_of_ghostManager_9() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ghostManager_9)); }
	inline GhostManager_t3188635936 * get_ghostManager_9() const { return ___ghostManager_9; }
	inline GhostManager_t3188635936 ** get_address_of_ghostManager_9() { return &___ghostManager_9; }
	inline void set_ghostManager_9(GhostManager_t3188635936 * value)
	{
		___ghostManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___ghostManager_9), value);
	}

	inline static int32_t get_offset_of_TargetTriangle_10() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___TargetTriangle_10)); }
	inline GameObject_t1113636619 * get_TargetTriangle_10() const { return ___TargetTriangle_10; }
	inline GameObject_t1113636619 ** get_address_of_TargetTriangle_10() { return &___TargetTriangle_10; }
	inline void set_TargetTriangle_10(GameObject_t1113636619 * value)
	{
		___TargetTriangle_10 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTriangle_10), value);
	}

	inline static int32_t get_offset_of_AimSpriteRenderer_11() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___AimSpriteRenderer_11)); }
	inline SpriteRenderer_t3235626157 * get_AimSpriteRenderer_11() const { return ___AimSpriteRenderer_11; }
	inline SpriteRenderer_t3235626157 ** get_address_of_AimSpriteRenderer_11() { return &___AimSpriteRenderer_11; }
	inline void set_AimSpriteRenderer_11(SpriteRenderer_t3235626157 * value)
	{
		___AimSpriteRenderer_11 = value;
		Il2CppCodeGenWriteBarrier((&___AimSpriteRenderer_11), value);
	}

	inline static int32_t get_offset_of_isShowTargetTriangle_12() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isShowTargetTriangle_12)); }
	inline bool get_isShowTargetTriangle_12() const { return ___isShowTargetTriangle_12; }
	inline bool* get_address_of_isShowTargetTriangle_12() { return &___isShowTargetTriangle_12; }
	inline void set_isShowTargetTriangle_12(bool value)
	{
		___isShowTargetTriangle_12 = value;
	}

	inline static int32_t get_offset_of_isShot_13() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isShot_13)); }
	inline bool get_isShot_13() const { return ___isShot_13; }
	inline bool* get_address_of_isShot_13() { return &___isShot_13; }
	inline void set_isShot_13(bool value)
	{
		___isShot_13 = value;
	}

	inline static int32_t get_offset_of_ShotEffect_14() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ShotEffect_14)); }
	inline GameObject_t1113636619 * get_ShotEffect_14() const { return ___ShotEffect_14; }
	inline GameObject_t1113636619 ** get_address_of_ShotEffect_14() { return &___ShotEffect_14; }
	inline void set_ShotEffect_14(GameObject_t1113636619 * value)
	{
		___ShotEffect_14 = value;
		Il2CppCodeGenWriteBarrier((&___ShotEffect_14), value);
	}

	inline static int32_t get_offset_of_DamageEffect_15() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___DamageEffect_15)); }
	inline GameObject_t1113636619 * get_DamageEffect_15() const { return ___DamageEffect_15; }
	inline GameObject_t1113636619 ** get_address_of_DamageEffect_15() { return &___DamageEffect_15; }
	inline void set_DamageEffect_15(GameObject_t1113636619 * value)
	{
		___DamageEffect_15 = value;
		Il2CppCodeGenWriteBarrier((&___DamageEffect_15), value);
	}

	inline static int32_t get_offset_of_FadeEffect_16() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___FadeEffect_16)); }
	inline GameObject_t1113636619 * get_FadeEffect_16() const { return ___FadeEffect_16; }
	inline GameObject_t1113636619 ** get_address_of_FadeEffect_16() { return &___FadeEffect_16; }
	inline void set_FadeEffect_16(GameObject_t1113636619 * value)
	{
		___FadeEffect_16 = value;
		Il2CppCodeGenWriteBarrier((&___FadeEffect_16), value);
	}

	inline static int32_t get_offset_of_AR_Canvas_17() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___AR_Canvas_17)); }
	inline GameObject_t1113636619 * get_AR_Canvas_17() const { return ___AR_Canvas_17; }
	inline GameObject_t1113636619 ** get_address_of_AR_Canvas_17() { return &___AR_Canvas_17; }
	inline void set_AR_Canvas_17(GameObject_t1113636619 * value)
	{
		___AR_Canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&___AR_Canvas_17), value);
	}

	inline static int32_t get_offset_of_VR_Area_18() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___VR_Area_18)); }
	inline Transform_t3600365921 * get_VR_Area_18() const { return ___VR_Area_18; }
	inline Transform_t3600365921 ** get_address_of_VR_Area_18() { return &___VR_Area_18; }
	inline void set_VR_Area_18(Transform_t3600365921 * value)
	{
		___VR_Area_18 = value;
		Il2CppCodeGenWriteBarrier((&___VR_Area_18), value);
	}

	inline static int32_t get_offset_of_AR_UI_19() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___AR_UI_19)); }
	inline GameObject_t1113636619 * get_AR_UI_19() const { return ___AR_UI_19; }
	inline GameObject_t1113636619 ** get_address_of_AR_UI_19() { return &___AR_UI_19; }
	inline void set_AR_UI_19(GameObject_t1113636619 * value)
	{
		___AR_UI_19 = value;
		Il2CppCodeGenWriteBarrier((&___AR_UI_19), value);
	}

	inline static int32_t get_offset_of_VR_UI_20() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___VR_UI_20)); }
	inline GameObject_t1113636619 * get_VR_UI_20() const { return ___VR_UI_20; }
	inline GameObject_t1113636619 ** get_address_of_VR_UI_20() { return &___VR_UI_20; }
	inline void set_VR_UI_20(GameObject_t1113636619 * value)
	{
		___VR_UI_20 = value;
		Il2CppCodeGenWriteBarrier((&___VR_UI_20), value);
	}

	inline static int32_t get_offset_of_takePhotoUI_21() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___takePhotoUI_21)); }
	inline GameObject_t1113636619 * get_takePhotoUI_21() const { return ___takePhotoUI_21; }
	inline GameObject_t1113636619 ** get_address_of_takePhotoUI_21() { return &___takePhotoUI_21; }
	inline void set_takePhotoUI_21(GameObject_t1113636619 * value)
	{
		___takePhotoUI_21 = value;
		Il2CppCodeGenWriteBarrier((&___takePhotoUI_21), value);
	}

	inline static int32_t get_offset_of_takePhotoButton_22() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___takePhotoButton_22)); }
	inline GameObject_t1113636619 * get_takePhotoButton_22() const { return ___takePhotoButton_22; }
	inline GameObject_t1113636619 ** get_address_of_takePhotoButton_22() { return &___takePhotoButton_22; }
	inline void set_takePhotoButton_22(GameObject_t1113636619 * value)
	{
		___takePhotoButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___takePhotoButton_22), value);
	}

	inline static int32_t get_offset_of_candyTutorialButton_23() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___candyTutorialButton_23)); }
	inline GameObject_t1113636619 * get_candyTutorialButton_23() const { return ___candyTutorialButton_23; }
	inline GameObject_t1113636619 ** get_address_of_candyTutorialButton_23() { return &___candyTutorialButton_23; }
	inline void set_candyTutorialButton_23(GameObject_t1113636619 * value)
	{
		___candyTutorialButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___candyTutorialButton_23), value);
	}

	inline static int32_t get_offset_of_charmList_24() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___charmList_24)); }
	inline CharmU5BU5D_t2980613126* get_charmList_24() const { return ___charmList_24; }
	inline CharmU5BU5D_t2980613126** get_address_of_charmList_24() { return &___charmList_24; }
	inline void set_charmList_24(CharmU5BU5D_t2980613126* value)
	{
		___charmList_24 = value;
		Il2CppCodeGenWriteBarrier((&___charmList_24), value);
	}

	inline static int32_t get_offset_of_ghostSpawn_25() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___ghostSpawn_25)); }
	inline Transform_t3600365921 * get_ghostSpawn_25() const { return ___ghostSpawn_25; }
	inline Transform_t3600365921 ** get_address_of_ghostSpawn_25() { return &___ghostSpawn_25; }
	inline void set_ghostSpawn_25(Transform_t3600365921 * value)
	{
		___ghostSpawn_25 = value;
		Il2CppCodeGenWriteBarrier((&___ghostSpawn_25), value);
	}

	inline static int32_t get_offset_of_isFirst_26() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isFirst_26)); }
	inline bool get_isFirst_26() const { return ___isFirst_26; }
	inline bool* get_address_of_isFirst_26() { return &___isFirst_26; }
	inline void set_isFirst_26(bool value)
	{
		___isFirst_26 = value;
	}
};

struct GameManager_t1536523654_StaticFields
{
public:
	// GameManager GameManager::instance
	GameManager_t1536523654 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___instance_5)); }
	inline GameManager_t1536523654 * get_instance_5() const { return ___instance_5; }
	inline GameManager_t1536523654 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(GameManager_t1536523654 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_2(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2305[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2307[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2309[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2319[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2320[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2321[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (U3CModuleU3E_t692745556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (LeanAudioStream_t2242826187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[3] = 
{
	LeanAudioStream_t2242826187::get_offset_of_position_0(),
	LeanAudioStream_t2242826187::get_offset_of_audioClip_1(),
	LeanAudioStream_t2242826187::get_offset_of_audioArr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (LeanAudio_t998158884), -1, sizeof(LeanAudio_t998158884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2330[6] = 
{
	LeanAudio_t998158884_StaticFields::get_offset_of_MIN_FREQEUNCY_PERIOD_0(),
	LeanAudio_t998158884_StaticFields::get_offset_of_PROCESSING_ITERATIONS_MAX_1(),
	LeanAudio_t998158884_StaticFields::get_offset_of_generatedWaveDistances_2(),
	LeanAudio_t998158884_StaticFields::get_offset_of_generatedWaveDistancesCount_3(),
	LeanAudio_t998158884_StaticFields::get_offset_of_longList_4(),
	LeanAudio_t998158884_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (LeanAudioOptions_t2919880816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[8] = 
{
	LeanAudioOptions_t2919880816::get_offset_of_waveStyle_0(),
	LeanAudioOptions_t2919880816::get_offset_of_vibrato_1(),
	LeanAudioOptions_t2919880816::get_offset_of_modulation_2(),
	LeanAudioOptions_t2919880816::get_offset_of_frequencyRate_3(),
	LeanAudioOptions_t2919880816::get_offset_of_waveNoiseScale_4(),
	LeanAudioOptions_t2919880816::get_offset_of_waveNoiseInfluence_5(),
	LeanAudioOptions_t2919880816::get_offset_of_useSetData_6(),
	LeanAudioOptions_t2919880816::get_offset_of_stream_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (LeanAudioWaveStyle_t2891147723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2332[5] = 
{
	LeanAudioWaveStyle_t2891147723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (LeanTester_t4080455132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[1] = 
{
	LeanTester_t4080455132::get_offset_of_timeout_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CtimeoutCheckU3Ec__Iterator0_t3914164071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[5] = 
{
	U3CtimeoutCheckU3Ec__Iterator0_t3914164071::get_offset_of_U3CpauseEndTimeU3E__0_0(),
	U3CtimeoutCheckU3Ec__Iterator0_t3914164071::get_offset_of_U24this_1(),
	U3CtimeoutCheckU3Ec__Iterator0_t3914164071::get_offset_of_U24current_2(),
	U3CtimeoutCheckU3Ec__Iterator0_t3914164071::get_offset_of_U24disposing_3(),
	U3CtimeoutCheckU3Ec__Iterator0_t3914164071::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (LeanTest_t591031114), -1, sizeof(LeanTest_t591031114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2335[6] = 
{
	LeanTest_t591031114_StaticFields::get_offset_of_expected_0(),
	LeanTest_t591031114_StaticFields::get_offset_of_tests_1(),
	LeanTest_t591031114_StaticFields::get_offset_of_passes_2(),
	LeanTest_t591031114_StaticFields::get_offset_of_timeout_3(),
	LeanTest_t591031114_StaticFields::get_offset_of_timeoutStarted_4(),
	LeanTest_t591031114_StaticFields::get_offset_of_testsFinished_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (TweenAction_t2598825989)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2336[51] = 
{
	TweenAction_t2598825989::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (LeanTweenType_t619681147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2337[40] = 
{
	LeanTweenType_t619681147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (LeanTween_t1803894739), -1, sizeof(LeanTween_t1803894739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[32] = 
{
	LeanTween_t1803894739_StaticFields::get_offset_of_throwErrors_2(),
	LeanTween_t1803894739_StaticFields::get_offset_of_tau_3(),
	LeanTween_t1803894739_StaticFields::get_offset_of_PI_DIV2_4(),
	LeanTween_t1803894739_StaticFields::get_offset_of_sequences_5(),
	LeanTween_t1803894739_StaticFields::get_offset_of_tweens_6(),
	LeanTween_t1803894739_StaticFields::get_offset_of_tweensFinished_7(),
	LeanTween_t1803894739_StaticFields::get_offset_of_tweensFinishedIds_8(),
	LeanTween_t1803894739_StaticFields::get_offset_of_tween_9(),
	LeanTween_t1803894739_StaticFields::get_offset_of_tweenMaxSearch_10(),
	LeanTween_t1803894739_StaticFields::get_offset_of_maxTweens_11(),
	LeanTween_t1803894739_StaticFields::get_offset_of_maxSequences_12(),
	LeanTween_t1803894739_StaticFields::get_offset_of_frameRendered_13(),
	LeanTween_t1803894739_StaticFields::get_offset_of__tweenEmpty_14(),
	LeanTween_t1803894739_StaticFields::get_offset_of_dtEstimated_15(),
	LeanTween_t1803894739_StaticFields::get_offset_of_dtManual_16(),
	LeanTween_t1803894739_StaticFields::get_offset_of_dtActual_17(),
	LeanTween_t1803894739_StaticFields::get_offset_of_global_counter_18(),
	LeanTween_t1803894739_StaticFields::get_offset_of_i_19(),
	LeanTween_t1803894739_StaticFields::get_offset_of_j_20(),
	LeanTween_t1803894739_StaticFields::get_offset_of_finishedCnt_21(),
	LeanTween_t1803894739_StaticFields::get_offset_of_punch_22(),
	LeanTween_t1803894739_StaticFields::get_offset_of_shake_23(),
	LeanTween_t1803894739_StaticFields::get_offset_of_maxTweenReached_24(),
	LeanTween_t1803894739_StaticFields::get_offset_of_startSearch_25(),
	LeanTween_t1803894739_StaticFields::get_offset_of_d_26(),
	LeanTween_t1803894739_StaticFields::get_offset_of_eventListeners_27(),
	LeanTween_t1803894739_StaticFields::get_offset_of_goListeners_28(),
	LeanTween_t1803894739_StaticFields::get_offset_of_eventsMaxSearch_29(),
	LeanTween_t1803894739_StaticFields::get_offset_of_EVENTS_MAX_30(),
	LeanTween_t1803894739_StaticFields::get_offset_of_LISTENERS_MAX_31(),
	LeanTween_t1803894739_StaticFields::get_offset_of_INIT_LISTENERS_MAX_32(),
	LeanTween_t1803894739_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (LTUtility_t4124338238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (LTBezier_t3079809627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[7] = 
{
	LTBezier_t3079809627::get_offset_of_length_0(),
	LTBezier_t3079809627::get_offset_of_a_1(),
	LTBezier_t3079809627::get_offset_of_aa_2(),
	LTBezier_t3079809627::get_offset_of_bb_3(),
	LTBezier_t3079809627::get_offset_of_cc_4(),
	LTBezier_t3079809627::get_offset_of_len_5(),
	LTBezier_t3079809627::get_offset_of_arcLengths_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (LTBezierPath_t1817657086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[8] = 
{
	LTBezierPath_t1817657086::get_offset_of_pts_0(),
	LTBezierPath_t1817657086::get_offset_of_length_1(),
	LTBezierPath_t1817657086::get_offset_of_orientToPath_2(),
	LTBezierPath_t1817657086::get_offset_of_orientToPath2d_3(),
	LTBezierPath_t1817657086::get_offset_of_beziers_4(),
	LTBezierPath_t1817657086::get_offset_of_lengthRatio_5(),
	LTBezierPath_t1817657086::get_offset_of_currentBezier_6(),
	LTBezierPath_t1817657086::get_offset_of_previousBezier_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (LTSpline_t2431306763), -1, sizeof(LTSpline_t2431306763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2342[11] = 
{
	LTSpline_t2431306763_StaticFields::get_offset_of_DISTANCE_COUNT_0(),
	LTSpline_t2431306763_StaticFields::get_offset_of_SUBLINE_COUNT_1(),
	LTSpline_t2431306763::get_offset_of_distance_2(),
	LTSpline_t2431306763::get_offset_of_constantSpeed_3(),
	LTSpline_t2431306763::get_offset_of_pts_4(),
	LTSpline_t2431306763::get_offset_of_ptsAdj_5(),
	LTSpline_t2431306763::get_offset_of_ptsAdjLength_6(),
	LTSpline_t2431306763::get_offset_of_orientToPath_7(),
	LTSpline_t2431306763::get_offset_of_orientToPath2d_8(),
	LTSpline_t2431306763::get_offset_of_numSections_9(),
	LTSpline_t2431306763::get_offset_of_currPt_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (LTRect_t2883103509), -1, sizeof(LTRect_t2883103509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2343[21] = 
{
	LTRect_t2883103509::get_offset_of__rect_0(),
	LTRect_t2883103509::get_offset_of_alpha_1(),
	LTRect_t2883103509::get_offset_of_rotation_2(),
	LTRect_t2883103509::get_offset_of_pivot_3(),
	LTRect_t2883103509::get_offset_of_margin_4(),
	LTRect_t2883103509::get_offset_of_relativeRect_5(),
	LTRect_t2883103509::get_offset_of_rotateEnabled_6(),
	LTRect_t2883103509::get_offset_of_rotateFinished_7(),
	LTRect_t2883103509::get_offset_of_alphaEnabled_8(),
	LTRect_t2883103509::get_offset_of_labelStr_9(),
	LTRect_t2883103509::get_offset_of_type_10(),
	LTRect_t2883103509::get_offset_of_style_11(),
	LTRect_t2883103509::get_offset_of_useColor_12(),
	LTRect_t2883103509::get_offset_of_color_13(),
	LTRect_t2883103509::get_offset_of_fontScaleToFit_14(),
	LTRect_t2883103509::get_offset_of_useSimpleScale_15(),
	LTRect_t2883103509::get_offset_of_sizeByHeight_16(),
	LTRect_t2883103509::get_offset_of_texture_17(),
	LTRect_t2883103509::get_offset_of__id_18(),
	LTRect_t2883103509::get_offset_of_counter_19(),
	LTRect_t2883103509_StaticFields::get_offset_of_colorTouched_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (LTEvent_t176071434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[2] = 
{
	LTEvent_t176071434::get_offset_of_id_0(),
	LTEvent_t176071434::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (LTGUI_t651246514), -1, sizeof(LTGUI_t651246514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2345[12] = 
{
	LTGUI_t651246514_StaticFields::get_offset_of_RECT_LEVELS_0(),
	LTGUI_t651246514_StaticFields::get_offset_of_RECTS_PER_LEVEL_1(),
	LTGUI_t651246514_StaticFields::get_offset_of_BUTTONS_MAX_2(),
	LTGUI_t651246514_StaticFields::get_offset_of_levels_3(),
	LTGUI_t651246514_StaticFields::get_offset_of_levelDepths_4(),
	LTGUI_t651246514_StaticFields::get_offset_of_buttons_5(),
	LTGUI_t651246514_StaticFields::get_offset_of_buttonLevels_6(),
	LTGUI_t651246514_StaticFields::get_offset_of_buttonLastFrame_7(),
	LTGUI_t651246514_StaticFields::get_offset_of_r_8(),
	LTGUI_t651246514_StaticFields::get_offset_of_color_9(),
	LTGUI_t651246514_StaticFields::get_offset_of_isGUIEnabled_10(),
	LTGUI_t651246514_StaticFields::get_offset_of_global_counter_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (Element_Type_t1916908453)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2346[3] = 
{
	Element_Type_t1916908453::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (LeanDummy_t4064590342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (LTDescr_t2043587347), -1, sizeof(LTDescr_t2043587347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2348[50] = 
{
	LTDescr_t2043587347::get_offset_of_toggle_0(),
	LTDescr_t2043587347::get_offset_of_useEstimatedTime_1(),
	LTDescr_t2043587347::get_offset_of_useFrames_2(),
	LTDescr_t2043587347::get_offset_of_useManualTime_3(),
	LTDescr_t2043587347::get_offset_of_usesNormalDt_4(),
	LTDescr_t2043587347::get_offset_of_hasInitiliazed_5(),
	LTDescr_t2043587347::get_offset_of_hasExtraOnCompletes_6(),
	LTDescr_t2043587347::get_offset_of_hasPhysics_7(),
	LTDescr_t2043587347::get_offset_of_onCompleteOnRepeat_8(),
	LTDescr_t2043587347::get_offset_of_onCompleteOnStart_9(),
	LTDescr_t2043587347::get_offset_of_useRecursion_10(),
	LTDescr_t2043587347::get_offset_of_ratioPassed_11(),
	LTDescr_t2043587347::get_offset_of_passed_12(),
	LTDescr_t2043587347::get_offset_of_delay_13(),
	LTDescr_t2043587347::get_offset_of_time_14(),
	LTDescr_t2043587347::get_offset_of_speed_15(),
	LTDescr_t2043587347::get_offset_of_lastVal_16(),
	LTDescr_t2043587347::get_offset_of__id_17(),
	LTDescr_t2043587347::get_offset_of_loopCount_18(),
	LTDescr_t2043587347::get_offset_of_counter_19(),
	LTDescr_t2043587347::get_offset_of_direction_20(),
	LTDescr_t2043587347::get_offset_of_directionLast_21(),
	LTDescr_t2043587347::get_offset_of_overshoot_22(),
	LTDescr_t2043587347::get_offset_of_period_23(),
	LTDescr_t2043587347::get_offset_of_scale_24(),
	LTDescr_t2043587347::get_offset_of_destroyOnComplete_25(),
	LTDescr_t2043587347::get_offset_of_trans_26(),
	LTDescr_t2043587347::get_offset_of_fromInternal_27(),
	LTDescr_t2043587347::get_offset_of_toInternal_28(),
	LTDescr_t2043587347::get_offset_of_diff_29(),
	LTDescr_t2043587347::get_offset_of_diffDiv2_30(),
	LTDescr_t2043587347::get_offset_of_type_31(),
	LTDescr_t2043587347::get_offset_of_easeType_32(),
	LTDescr_t2043587347::get_offset_of_loopType_33(),
	LTDescr_t2043587347::get_offset_of_hasUpdateCallback_34(),
	LTDescr_t2043587347::get_offset_of_easeMethod_35(),
	LTDescr_t2043587347::get_offset_of_U3CeaseInternalU3Ek__BackingField_36(),
	LTDescr_t2043587347::get_offset_of_U3CinitInternalU3Ek__BackingField_37(),
	LTDescr_t2043587347::get_offset_of_spriteRen_38(),
	LTDescr_t2043587347::get_offset_of_rectTransform_39(),
	LTDescr_t2043587347::get_offset_of_uiText_40(),
	LTDescr_t2043587347::get_offset_of_uiImage_41(),
	LTDescr_t2043587347::get_offset_of_rawImage_42(),
	LTDescr_t2043587347::get_offset_of_sprites_43(),
	LTDescr_t2043587347::get_offset_of__optional_44(),
	LTDescr_t2043587347_StaticFields::get_offset_of_val_45(),
	LTDescr_t2043587347_StaticFields::get_offset_of_dt_46(),
	LTDescr_t2043587347_StaticFields::get_offset_of_newVect_47(),
	LTDescr_t2043587347_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_48(),
	LTDescr_t2043587347_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (EaseTypeDelegate_t2201194898), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (ActionMethodDelegate_t2422926000), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (LTDescrOptional_t4257087022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[23] = 
{
	LTDescrOptional_t4257087022::get_offset_of_U3CtoTransU3Ek__BackingField_0(),
	LTDescrOptional_t4257087022::get_offset_of_U3CpointU3Ek__BackingField_1(),
	LTDescrOptional_t4257087022::get_offset_of_U3CaxisU3Ek__BackingField_2(),
	LTDescrOptional_t4257087022::get_offset_of_U3ClastValU3Ek__BackingField_3(),
	LTDescrOptional_t4257087022::get_offset_of_U3CorigRotationU3Ek__BackingField_4(),
	LTDescrOptional_t4257087022::get_offset_of_U3CpathU3Ek__BackingField_5(),
	LTDescrOptional_t4257087022::get_offset_of_U3CsplineU3Ek__BackingField_6(),
	LTDescrOptional_t4257087022::get_offset_of_animationCurve_7(),
	LTDescrOptional_t4257087022::get_offset_of_initFrameCount_8(),
	LTDescrOptional_t4257087022::get_offset_of_U3CltRectU3Ek__BackingField_9(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateFloatU3Ek__BackingField_10(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_11(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_12(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateVector2U3Ek__BackingField_13(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateVector3U3Ek__BackingField_14(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_15(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateColorU3Ek__BackingField_16(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_17(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConCompleteU3Ek__BackingField_18(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConCompleteObjectU3Ek__BackingField_19(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConCompleteParamU3Ek__BackingField_20(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConUpdateParamU3Ek__BackingField_21(),
	LTDescrOptional_t4257087022::get_offset_of_U3ConStartU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (LTSeq_t1322000318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[9] = 
{
	LTSeq_t1322000318::get_offset_of_previous_0(),
	LTSeq_t1322000318::get_offset_of_current_1(),
	LTSeq_t1322000318::get_offset_of_tween_2(),
	LTSeq_t1322000318::get_offset_of_totalDelay_3(),
	LTSeq_t1322000318::get_offset_of_timeScale_4(),
	LTSeq_t1322000318::get_offset_of_debugIter_5(),
	LTSeq_t1322000318::get_offset_of_counter_6(),
	LTSeq_t1322000318::get_offset_of_toggle_7(),
	LTSeq_t1322000318::get_offset_of__id_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (U3CModuleU3E_t692745557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (CamGyro_t2619086136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[1] = 
{
	CamGyro_t2619086136::get_offset_of_camParent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (CandyShopItem_t1546810590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[4] = 
{
	CandyShopItem_t1546810590::get_offset_of_count_2(),
	CandyShopItem_t1546810590::get_offset_of_titleText_3(),
	CandyShopItem_t1546810590::get_offset_of_countText_4(),
	CandyShopItem_t1546810590::get_offset_of_priceText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (CharmType_t3822141316)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[5] = 
{
	CharmType_t3822141316::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (Charm_t3703444127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[5] = 
{
	Charm_t3703444127::get_offset_of_charmType_2(),
	Charm_t3703444127::get_offset_of_name_3(),
	Charm_t3703444127::get_offset_of_image_4(),
	Charm_t3703444127::get_offset_of_damage_5(),
	Charm_t3703444127::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (CharmTrigger_t2026203018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[3] = 
{
	CharmTrigger_t2026203018::get_offset_of_isSelected_2(),
	CharmTrigger_t2026203018::get_offset_of_xPos_3(),
	CharmTrigger_t2026203018::get_offset_of_image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (DisableObject_t4022553226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[1] = 
{
	DisableObject_t4022553226::get_offset_of_disableTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (AnimationName_t1099644795)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2360[5] = 
{
	AnimationName_t1099644795::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (GhostGrade_t2887092049)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2361[4] = 
{
	GhostGrade_t2887092049::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (Ghost_t201992404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[8] = 
{
	Ghost_t201992404::get_offset_of_monsterPrefab_2(),
	Ghost_t201992404::get_offset_of_SD_DataAsset_3(),
	Ghost_t201992404::get_offset_of_name_4(),
	Ghost_t201992404::get_offset_of_description_5(),
	Ghost_t201992404::get_offset_of_grade_6(),
	Ghost_t201992404::get_offset_of_hp_7(),
	Ghost_t201992404::get_offset_of_VR_Mode_Pos_8(),
	Ghost_t201992404::get_offset_of_VR_Mode_Scale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (GhostInfo_t3310990554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[3] = 
{
	GhostInfo_t3310990554::get_offset_of_targetPoint_2(),
	GhostInfo_t3310990554::get_offset_of_charmTransform_3(),
	GhostInfo_t3310990554::get_offset_of_spainAnimation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (Hit_t3558828664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (LightSensorPluginScript_t2582954711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[3] = 
{
	LightSensorPluginScript_t2582954711::get_offset_of_activityContext_2(),
	LightSensorPluginScript_t2582954711::get_offset_of_jo_3(),
	LightSensorPluginScript_t2582954711::get_offset_of_activityClass_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (LightSensorTest_t2954693399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[2] = 
{
	LightSensorTest_t2954693399::get_offset_of_lightSensor_2(),
	LightSensorTest_t2954693399::get_offset_of_text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (AudioManager_t3267510698), -1, sizeof(AudioManager_t3267510698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[2] = 
{
	AudioManager_t3267510698::get_offset_of_sounds_2(),
	AudioManager_t3267510698_StaticFields::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (U3CPlayU3Ec__AnonStorey0_t2108231134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[1] = 
{
	U3CPlayU3Ec__AnonStorey0_t2108231134::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (U3CStopU3Ec__AnonStorey1_t2155610042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[1] = 
{
	U3CStopU3Ec__AnonStorey1_t2155610042::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (CharmGameState_t3120537021)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2370[4] = 
{
	CharmGameState_t3120537021::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (MoveDirection_t1208372469)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[3] = 
{
	MoveDirection_t1208372469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (CharmGameManager_t204340045), -1, sizeof(CharmGameManager_t204340045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[25] = 
{
	CharmGameManager_t204340045::get_offset_of_gameState_2(),
	CharmGameManager_t204340045::get_offset_of_CharmList_3(),
	CharmGameManager_t204340045::get_offset_of_targetCharmIdx_4(),
	CharmGameManager_t204340045::get_offset_of_newCharmIndex_5(),
	CharmGameManager_t204340045::get_offset_of_charmPrefab_6(),
	CharmGameManager_t204340045::get_offset_of_enabledCharmCount_7(),
	CharmGameManager_t204340045::get_offset_of_remainCharm_UI_8(),
	CharmGameManager_t204340045::get_offset_of_pointer_9(),
	CharmGameManager_t204340045::get_offset_of_moveDirection_10(),
	CharmGameManager_t204340045::get_offset_of_touchArea_11(),
	CharmGameManager_t204340045::get_offset_of_ghostManager_12(),
	CharmGameManager_t204340045::get_offset_of_ghostInfoUI_13(),
	CharmGameManager_t204340045::get_offset_of_GameMode_UI_14(),
	CharmGameManager_t204340045::get_offset_of_LastAttack_UI_15(),
	CharmGameManager_t204340045::get_offset_of_Result_UI_16(),
	CharmGameManager_t204340045::get_offset_of_GameOverPopup_17(),
	CharmGameManager_t204340045::get_offset_of_ConfirmOutPopup_18(),
	CharmGameManager_t204340045::get_offset_of_RefillPopup_19(),
	CharmGameManager_t204340045::get_offset_of_ChangeCharmPopup_20(),
	CharmGameManager_t204340045::get_offset_of_damagePopText_21(),
	CharmGameManager_t204340045::get_offset_of_timeText_22(),
	CharmGameManager_t204340045::get_offset_of_stopTime_23(),
	CharmGameManager_t204340045_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_24(),
	CharmGameManager_t204340045_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_25(),
	CharmGameManager_t204340045_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (U3CGameTimeU3Ec__Iterator0_t320282633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[5] = 
{
	U3CGameTimeU3Ec__Iterator0_t320282633::get_offset_of_U3CtimeU3E__0_0(),
	U3CGameTimeU3Ec__Iterator0_t320282633::get_offset_of_U24this_1(),
	U3CGameTimeU3Ec__Iterator0_t320282633::get_offset_of_U24current_2(),
	U3CGameTimeU3Ec__Iterator0_t320282633::get_offset_of_U24disposing_3(),
	U3CGameTimeU3Ec__Iterator0_t320282633::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (U3CShowAllCharmU3Ec__Iterator1_t3091840906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[8] = 
{
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U3ClistU3E__0_0(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U3CremainCharmCountU3E__0_1(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U24locvar0_2(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U3CiU3E__1_3(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U24this_4(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U24current_5(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U24disposing_6(),
	U3CShowAllCharmU3Ec__Iterator1_t3091840906::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (U3CPausePointerU3Ec__AnonStorey4_t722434170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[2] = 
{
	U3CPausePointerU3Ec__AnonStorey4_t722434170::get_offset_of_flyCharm_0(),
	U3CPausePointerU3Ec__AnonStorey4_t722434170::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (U3CBurnAnimationU3Ec__Iterator2_t3560078157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	U3CBurnAnimationU3Ec__Iterator2_t3560078157::get_offset_of_flyCharm_0(),
	U3CBurnAnimationU3Ec__Iterator2_t3560078157::get_offset_of_U24current_1(),
	U3CBurnAnimationU3Ec__Iterator2_t3560078157::get_offset_of_U24disposing_2(),
	U3CBurnAnimationU3Ec__Iterator2_t3560078157::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (U3CDamageMonsterU3Ec__Iterator3_t1032243821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[6] = 
{
	U3CDamageMonsterU3Ec__Iterator3_t1032243821::get_offset_of_damageValue_0(),
	U3CDamageMonsterU3Ec__Iterator3_t1032243821::get_offset_of_U3ChpU3E__0_1(),
	U3CDamageMonsterU3Ec__Iterator3_t1032243821::get_offset_of_U24this_2(),
	U3CDamageMonsterU3Ec__Iterator3_t1032243821::get_offset_of_U24current_3(),
	U3CDamageMonsterU3Ec__Iterator3_t1032243821::get_offset_of_U24disposing_4(),
	U3CDamageMonsterU3Ec__Iterator3_t1032243821::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (GameManager_t1536523654), -1, sizeof(GameManager_t1536523654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2378[22] = 
{
	GameManager_t1536523654_StaticFields::get_offset_of_instance_5(),
	GameManager_t1536523654::get_offset_of_gameState_6(),
	GameManager_t1536523654::get_offset_of_gyroManager_7(),
	GameManager_t1536523654::get_offset_of_charmGM_8(),
	GameManager_t1536523654::get_offset_of_ghostManager_9(),
	GameManager_t1536523654::get_offset_of_TargetTriangle_10(),
	GameManager_t1536523654::get_offset_of_AimSpriteRenderer_11(),
	GameManager_t1536523654::get_offset_of_isShowTargetTriangle_12(),
	GameManager_t1536523654::get_offset_of_isShot_13(),
	GameManager_t1536523654::get_offset_of_ShotEffect_14(),
	GameManager_t1536523654::get_offset_of_DamageEffect_15(),
	GameManager_t1536523654::get_offset_of_FadeEffect_16(),
	GameManager_t1536523654::get_offset_of_AR_Canvas_17(),
	GameManager_t1536523654::get_offset_of_VR_Area_18(),
	GameManager_t1536523654::get_offset_of_AR_UI_19(),
	GameManager_t1536523654::get_offset_of_VR_UI_20(),
	GameManager_t1536523654::get_offset_of_takePhotoUI_21(),
	GameManager_t1536523654::get_offset_of_takePhotoButton_22(),
	GameManager_t1536523654::get_offset_of_candyTutorialButton_23(),
	GameManager_t1536523654::get_offset_of_charmList_24(),
	GameManager_t1536523654::get_offset_of_ghostSpawn_25(),
	GameManager_t1536523654::get_offset_of_isFirst_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (GameModeState_t1521487089)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2379[3] = 
{
	GameModeState_t1521487089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (U3CUseCandyCheckU3Ec__Iterator0_t992976987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[4] = 
{
	U3CUseCandyCheckU3Ec__Iterator0_t992976987::get_offset_of_U24this_0(),
	U3CUseCandyCheckU3Ec__Iterator0_t992976987::get_offset_of_U24current_1(),
	U3CUseCandyCheckU3Ec__Iterator0_t992976987::get_offset_of_U24disposing_2(),
	U3CUseCandyCheckU3Ec__Iterator0_t992976987::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (U3CStartGameU3Ec__Iterator1_t814519062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[4] = 
{
	U3CStartGameU3Ec__Iterator1_t814519062::get_offset_of_U24this_0(),
	U3CStartGameU3Ec__Iterator1_t814519062::get_offset_of_U24current_1(),
	U3CStartGameU3Ec__Iterator1_t814519062::get_offset_of_U24disposing_2(),
	U3CStartGameU3Ec__Iterator1_t814519062::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CShotButtonU3Ec__Iterator2_t1510887057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[5] = 
{
	U3CShotButtonU3Ec__Iterator2_t1510887057::get_offset_of_U3ChitU3E__0_0(),
	U3CShotButtonU3Ec__Iterator2_t1510887057::get_offset_of_U24this_1(),
	U3CShotButtonU3Ec__Iterator2_t1510887057::get_offset_of_U24current_2(),
	U3CShotButtonU3Ec__Iterator2_t1510887057::get_offset_of_U24disposing_3(),
	U3CShotButtonU3Ec__Iterator2_t1510887057::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[5] = 
{
	U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967::get_offset_of_U3CnewMonsterU3E__0_0(),
	U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967::get_offset_of_U24this_1(),
	U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967::get_offset_of_U24current_2(),
	U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967::get_offset_of_U24disposing_3(),
	U3CTurnOnVR_AreaU3Ec__Iterator3_t2506130967::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (U3CStartVRModeU3Ec__Iterator4_t1952977010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[5] = 
{
	U3CStartVRModeU3Ec__Iterator4_t1952977010::get_offset_of_waitTime_0(),
	U3CStartVRModeU3Ec__Iterator4_t1952977010::get_offset_of_U24this_1(),
	U3CStartVRModeU3Ec__Iterator4_t1952977010::get_offset_of_U24current_2(),
	U3CStartVRModeU3Ec__Iterator4_t1952977010::get_offset_of_U24disposing_3(),
	U3CStartVRModeU3Ec__Iterator4_t1952977010::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (U3CStartCharmGameU3Ec__Iterator5_t2645365450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[4] = 
{
	U3CStartCharmGameU3Ec__Iterator5_t2645365450::get_offset_of_U24this_0(),
	U3CStartCharmGameU3Ec__Iterator5_t2645365450::get_offset_of_U24current_1(),
	U3CStartCharmGameU3Ec__Iterator5_t2645365450::get_offset_of_U24disposing_2(),
	U3CStartCharmGameU3Ec__Iterator5_t2645365450::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (GhostManager_t3188635936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[7] = 
{
	GhostManager_t3188635936::get_offset_of_GhostList_2(),
	GhostManager_t3188635936::get_offset_of_ghost_3(),
	GhostManager_t3188635936::get_offset_of_ghostGO_4(),
	GhostManager_t3188635936::get_offset_of_ghostInfo_5(),
	GhostManager_t3188635936::get_offset_of_isMove_6(),
	GhostManager_t3188635936::get_offset_of_spawnRange_7(),
	GhostManager_t3188635936::get_offset_of_spawnTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (U3CMonsterSoundU3Ec__Iterator0_t1078325540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[4] = 
{
	U3CMonsterSoundU3Ec__Iterator0_t1078325540::get_offset_of_U3CclipNameU3E__1_0(),
	U3CMonsterSoundU3Ec__Iterator0_t1078325540::get_offset_of_U24current_1(),
	U3CMonsterSoundU3Ec__Iterator0_t1078325540::get_offset_of_U24disposing_2(),
	U3CMonsterSoundU3Ec__Iterator0_t1078325540::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (GyroManager_t2156710008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[5] = 
{
	GyroManager_t2156710008::get_offset_of_isEnable_2(),
	GyroManager_t2156710008::get_offset_of_gyroEnabled_3(),
	GyroManager_t2156710008::get_offset_of_gyro_4(),
	GyroManager_t2156710008::get_offset_of_cameraContainer_5(),
	GyroManager_t2156710008::get_offset_of_rot_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (LastAttackManager_t490974840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[9] = 
{
	LastAttackManager_t490974840::get_offset_of_charmGameManager_2(),
	LastAttackManager_t490974840::get_offset_of_gameState_3(),
	LastAttackManager_t490974840::get_offset_of_lastAttack_UI_4(),
	LastAttackManager_t490974840::get_offset_of_pointer_5(),
	LastAttackManager_t490974840::get_offset_of_getItemArea_6(),
	LastAttackManager_t490974840::get_offset_of_normalArea_7(),
	LastAttackManager_t490974840::get_offset_of_charmPrefab_8(),
	LastAttackManager_t490974840::get_offset_of_itemCharm_UI_9(),
	LastAttackManager_t490974840::get_offset_of_freeCharm_UI_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (U3CStartAnimationU3Ec__Iterator0_t4019716844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	U3CStartAnimationU3Ec__Iterator0_t4019716844::get_offset_of_U24this_0(),
	U3CStartAnimationU3Ec__Iterator0_t4019716844::get_offset_of_U24current_1(),
	U3CStartAnimationU3Ec__Iterator0_t4019716844::get_offset_of_U24disposing_2(),
	U3CStartAnimationU3Ec__Iterator0_t4019716844::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (U3CPausePointerU3Ec__AnonStorey2_t4213274272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[3] = 
{
	U3CPausePointerU3Ec__AnonStorey2_t4213274272::get_offset_of_ghostManager_0(),
	U3CPausePointerU3Ec__AnonStorey2_t4213274272::get_offset_of_flyCharm_1(),
	U3CPausePointerU3Ec__AnonStorey2_t4213274272::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (U3CShowResultU3Ec__Iterator1_t419679075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[4] = 
{
	U3CShowResultU3Ec__Iterator1_t419679075::get_offset_of_U24this_0(),
	U3CShowResultU3Ec__Iterator1_t419679075::get_offset_of_U24current_1(),
	U3CShowResultU3Ec__Iterator1_t419679075::get_offset_of_U24disposing_2(),
	U3CShowResultU3Ec__Iterator1_t419679075::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (LobbyManager_t4136450456), -1, sizeof(LobbyManager_t4136450456_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2393[9] = 
{
	LobbyManager_t4136450456::get_offset_of_candyShopPopup_5(),
	LobbyManager_t4136450456::get_offset_of_candyRemainTime_6(),
	LobbyManager_t4136450456::get_offset_of_candyRemainTimeText_7(),
	LobbyManager_t4136450456::get_offset_of_candyCountText_8(),
	LobbyManager_t4136450456::get_offset_of_liveShopPopup_9(),
	LobbyManager_t4136450456::get_offset_of_liveRemainTime_10(),
	LobbyManager_t4136450456::get_offset_of_liveRemainTimeText_11(),
	LobbyManager_t4136450456::get_offset_of_liveCountText_12(),
	LobbyManager_t4136450456_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (U3CMoveARGameSceneU3Ec__Iterator0_t909025706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[4] = 
{
	U3CMoveARGameSceneU3Ec__Iterator0_t909025706::get_offset_of_U24this_0(),
	U3CMoveARGameSceneU3Ec__Iterator0_t909025706::get_offset_of_U24current_1(),
	U3CMoveARGameSceneU3Ec__Iterator0_t909025706::get_offset_of_U24disposing_2(),
	U3CMoveARGameSceneU3Ec__Iterator0_t909025706::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721::get_offset_of_U24this_0(),
	U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721::get_offset_of_U24current_1(),
	U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721::get_offset_of_U24disposing_2(),
	U3CMovePuzzleGameSceneU3Ec__Iterator1_t2545055721::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (LoginManager_t1249555276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[1] = 
{
	LoginManager_t1249555276::get_offset_of_debugText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (MasterAgent_t3288722434), -1, sizeof(MasterAgent_t3288722434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2397[2] = 
{
	MasterAgent_t3288722434_StaticFields::get_offset_of_Instance_2(),
	MasterAgent_t3288722434::get_offset_of_timeManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (SaveLoadManager_t4010657880), -1, sizeof(SaveLoadManager_t4010657880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2398[1] = 
{
	SaveLoadManager_t4010657880_StaticFields::get_offset_of_Instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (U3CLoopU3Ec__Iterator0_t3060558849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[4] = 
{
	U3CLoopU3Ec__Iterator0_t3060558849::get_offset_of_U24this_0(),
	U3CLoopU3Ec__Iterator0_t3060558849::get_offset_of_U24current_1(),
	U3CLoopU3Ec__Iterator0_t3060558849::get_offset_of_U24disposing_2(),
	U3CLoopU3Ec__Iterator0_t3060558849::get_offset_of_U24PC_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
