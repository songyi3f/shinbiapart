﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Spine.SkeletonData
struct SkeletonData_t2032710716;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_t3793468194;
// Spine.ExposedList`1<Spine.Slot>
struct ExposedList_1_t1927085270;
// Spine.ExposedList`1<Spine.IkConstraint>
struct ExposedList_1_t87334839;
// Spine.ExposedList`1<Spine.TransformConstraint>
struct ExposedList_1_t3161142095;
// Spine.ExposedList`1<Spine.PathConstraint>
struct ExposedList_1_t3687966776;
// Spine.ExposedList`1<Spine.IUpdatable>
struct ExposedList_1_t2549315650;
// Spine.Skin
struct Skin_t1174584606;
// Spine.AtlasPage
struct AtlasPage_t4077017671;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Spine.Atlas[]
struct AtlasU5BU5D_t3463999232;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3365920845;
// Spine.Event[]
struct EventU5BU5D_t1966750348;
// System.String[]
struct StringU5BU5D_t1281789340;
// Spine.PathConstraintData
struct PathConstraintData_t981297034;
// Spine.Slot
struct Slot_t3514940700;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_t4104378640;
// Spine.Triangulator
struct Triangulator_t2502879214;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1363090323;
// Spine.ClippingAttachment
struct ClippingAttachment_t2586274570;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>>
struct ExposedList_1_t2516523210;
// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single>
struct Dictionary_2_t680659097;
// Spine.AttachmentLoader
struct AttachmentLoader_t3324778997;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh>
struct List_1_t3168994201;
// Spine.TransformMode[]
struct TransformModeU5BU5D_t3210249249;
// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry>
struct List_1_t1823906703;
// Spine.AnimationState
struct AnimationState_t3637309382;
// Spine.Pool`1<Spine.TrackEntry>
struct Pool_1_t287387900;
// System.Action
struct Action_t1264377477;
// Spine.Animation
struct Animation_t615783283;
// Spine.ExposedList`1<Spine.TrackEntry>
struct ExposedList_1_t4023600307;
// Spine.AnimationState/TrackEntryDelegate
struct TrackEntryDelegate_t363257942;
// Spine.AnimationState/TrackEntryEventDelegate
struct TrackEntryEventDelegate_t1653995044;
// Spine.ExposedList`1<Spine.Polygon>
struct ExposedList_1_t1727260827;
// Spine.ExposedList`1<Spine.BoundingBoxAttachment>
struct ExposedList_1_t1209651080;
// Spine.AnimationStateData
struct AnimationStateData_t3010651567;
// Spine.ExposedList`1<Spine.Event>
struct ExposedList_1_t4085685707;
// Spine.EventQueue
struct EventQueue_t808091267;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t1515895227;
// System.Collections.Generic.List`1<Spine.AtlasPage>
struct List_1_t1254125117;
// System.Collections.Generic.List`1<Spine.AtlasRegion>
struct List_1_t1485978026;
// Spine.TextureLoader
struct TextureLoader_t3496536928;
// Spine.ExposedList`1<Spine.BoneData>
struct ExposedList_1_t1542319060;
// Spine.ExposedList`1<Spine.SlotData>
struct ExposedList_1_t2861913768;
// Spine.ExposedList`1<Spine.Skin>
struct ExposedList_1_t3881696472;
// Spine.ExposedList`1<Spine.EventData>
struct ExposedList_1_t3431871853;
// Spine.ExposedList`1<Spine.Animation>
struct ExposedList_1_t3322895149;
// Spine.ExposedList`1<Spine.IkConstraintData>
struct ExposedList_1_t3166231995;
// Spine.ExposedList`1<Spine.TransformConstraintData>
struct ExposedList_1_t3236185212;
// Spine.ExposedList`1<Spine.PathConstraintData>
struct ExposedList_1_t3688408900;
// Spine.SlotData
struct SlotData_t154801902;
// Spine.Bone
struct Bone_t1086356328;
// Spine.Attachment
struct Attachment_t3043756552;
// Spine.BoneData
struct BoneData_t3130174490;
// Spine.Skeleton
struct Skeleton_t3686076450;
// System.Collections.Generic.List`1<Spine.BoneData>
struct List_1_t307281936;
// Spine.IkConstraintData
struct IkConstraintData_t459120129;
// Spine.EventData
struct EventData_t724759987;
// SharpJson.Lexer
struct Lexer_t3897031801;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Spine.MeshAttachment
struct MeshAttachment_t1975337962;
// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment>
struct Dictionary_2_t968660385;
// Spine.ExposedList`1<Spine.Timeline>
struct ExposedList_1_t383950901;
// System.Void
struct Void_t1185182177;
// System.Single[][]
struct SingleU5BU5DU5BU5D_t3206712258;
// Spine.VertexAttachment
struct VertexAttachment_t4074366829;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// GameVanilla.Game.UI.ScoreText
struct ScoreText_t753417783;
// Spine.TrackEntry
struct TrackEntry_t1316488441;
// Spine.Event
struct Event_t1378573841;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Action`1<GameVanilla.Game.Popups.AlertPopup>
struct Action_1_t327652521;
// GameVanilla.Game.Popups.BuyCoinsPopup
struct BuyCoinsPopup_t414112668;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t1752731834;
// GameVanilla.Game.Common.IapItem
struct IapItem_t3448270226;
// GameVanilla.Game.UI.ProgressStar
struct ProgressStar_t2895061392;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// GameVanilla.Game.Common.Goal
struct Goal_t1499691635;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SKELETON_T3686076450_H
#define SKELETON_T3686076450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skeleton
struct  Skeleton_t3686076450  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.Skeleton::data
	SkeletonData_t2032710716 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::bones
	ExposedList_1_t3793468194 * ___bones_1;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::slots
	ExposedList_1_t1927085270 * ___slots_2;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::drawOrder
	ExposedList_1_t1927085270 * ___drawOrder_3;
	// Spine.ExposedList`1<Spine.IkConstraint> Spine.Skeleton::ikConstraints
	ExposedList_1_t87334839 * ___ikConstraints_4;
	// Spine.ExposedList`1<Spine.TransformConstraint> Spine.Skeleton::transformConstraints
	ExposedList_1_t3161142095 * ___transformConstraints_5;
	// Spine.ExposedList`1<Spine.PathConstraint> Spine.Skeleton::pathConstraints
	ExposedList_1_t3687966776 * ___pathConstraints_6;
	// Spine.ExposedList`1<Spine.IUpdatable> Spine.Skeleton::updateCache
	ExposedList_1_t2549315650 * ___updateCache_7;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::updateCacheReset
	ExposedList_1_t3793468194 * ___updateCacheReset_8;
	// Spine.Skin Spine.Skeleton::skin
	Skin_t1174584606 * ___skin_9;
	// System.Single Spine.Skeleton::r
	float ___r_10;
	// System.Single Spine.Skeleton::g
	float ___g_11;
	// System.Single Spine.Skeleton::b
	float ___b_12;
	// System.Single Spine.Skeleton::a
	float ___a_13;
	// System.Single Spine.Skeleton::time
	float ___time_14;
	// System.Boolean Spine.Skeleton::flipX
	bool ___flipX_15;
	// System.Boolean Spine.Skeleton::flipY
	bool ___flipY_16;
	// System.Single Spine.Skeleton::x
	float ___x_17;
	// System.Single Spine.Skeleton::y
	float ___y_18;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___data_0)); }
	inline SkeletonData_t2032710716 * get_data_0() const { return ___data_0; }
	inline SkeletonData_t2032710716 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SkeletonData_t2032710716 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___bones_1)); }
	inline ExposedList_1_t3793468194 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3793468194 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___slots_2)); }
	inline ExposedList_1_t1927085270 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t1927085270 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t1927085270 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_drawOrder_3() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___drawOrder_3)); }
	inline ExposedList_1_t1927085270 * get_drawOrder_3() const { return ___drawOrder_3; }
	inline ExposedList_1_t1927085270 ** get_address_of_drawOrder_3() { return &___drawOrder_3; }
	inline void set_drawOrder_3(ExposedList_1_t1927085270 * value)
	{
		___drawOrder_3 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrder_3), value);
	}

	inline static int32_t get_offset_of_ikConstraints_4() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___ikConstraints_4)); }
	inline ExposedList_1_t87334839 * get_ikConstraints_4() const { return ___ikConstraints_4; }
	inline ExposedList_1_t87334839 ** get_address_of_ikConstraints_4() { return &___ikConstraints_4; }
	inline void set_ikConstraints_4(ExposedList_1_t87334839 * value)
	{
		___ikConstraints_4 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_4), value);
	}

	inline static int32_t get_offset_of_transformConstraints_5() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___transformConstraints_5)); }
	inline ExposedList_1_t3161142095 * get_transformConstraints_5() const { return ___transformConstraints_5; }
	inline ExposedList_1_t3161142095 ** get_address_of_transformConstraints_5() { return &___transformConstraints_5; }
	inline void set_transformConstraints_5(ExposedList_1_t3161142095 * value)
	{
		___transformConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_5), value);
	}

	inline static int32_t get_offset_of_pathConstraints_6() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___pathConstraints_6)); }
	inline ExposedList_1_t3687966776 * get_pathConstraints_6() const { return ___pathConstraints_6; }
	inline ExposedList_1_t3687966776 ** get_address_of_pathConstraints_6() { return &___pathConstraints_6; }
	inline void set_pathConstraints_6(ExposedList_1_t3687966776 * value)
	{
		___pathConstraints_6 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_6), value);
	}

	inline static int32_t get_offset_of_updateCache_7() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___updateCache_7)); }
	inline ExposedList_1_t2549315650 * get_updateCache_7() const { return ___updateCache_7; }
	inline ExposedList_1_t2549315650 ** get_address_of_updateCache_7() { return &___updateCache_7; }
	inline void set_updateCache_7(ExposedList_1_t2549315650 * value)
	{
		___updateCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___updateCache_7), value);
	}

	inline static int32_t get_offset_of_updateCacheReset_8() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___updateCacheReset_8)); }
	inline ExposedList_1_t3793468194 * get_updateCacheReset_8() const { return ___updateCacheReset_8; }
	inline ExposedList_1_t3793468194 ** get_address_of_updateCacheReset_8() { return &___updateCacheReset_8; }
	inline void set_updateCacheReset_8(ExposedList_1_t3793468194 * value)
	{
		___updateCacheReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___updateCacheReset_8), value);
	}

	inline static int32_t get_offset_of_skin_9() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___skin_9)); }
	inline Skin_t1174584606 * get_skin_9() const { return ___skin_9; }
	inline Skin_t1174584606 ** get_address_of_skin_9() { return &___skin_9; }
	inline void set_skin_9(Skin_t1174584606 * value)
	{
		___skin_9 = value;
		Il2CppCodeGenWriteBarrier((&___skin_9), value);
	}

	inline static int32_t get_offset_of_r_10() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___r_10)); }
	inline float get_r_10() const { return ___r_10; }
	inline float* get_address_of_r_10() { return &___r_10; }
	inline void set_r_10(float value)
	{
		___r_10 = value;
	}

	inline static int32_t get_offset_of_g_11() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___g_11)); }
	inline float get_g_11() const { return ___g_11; }
	inline float* get_address_of_g_11() { return &___g_11; }
	inline void set_g_11(float value)
	{
		___g_11 = value;
	}

	inline static int32_t get_offset_of_b_12() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___b_12)); }
	inline float get_b_12() const { return ___b_12; }
	inline float* get_address_of_b_12() { return &___b_12; }
	inline void set_b_12(float value)
	{
		___b_12 = value;
	}

	inline static int32_t get_offset_of_a_13() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___a_13)); }
	inline float get_a_13() const { return ___a_13; }
	inline float* get_address_of_a_13() { return &___a_13; }
	inline void set_a_13(float value)
	{
		___a_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_flipX_15() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___flipX_15)); }
	inline bool get_flipX_15() const { return ___flipX_15; }
	inline bool* get_address_of_flipX_15() { return &___flipX_15; }
	inline void set_flipX_15(bool value)
	{
		___flipX_15 = value;
	}

	inline static int32_t get_offset_of_flipY_16() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___flipY_16)); }
	inline bool get_flipY_16() const { return ___flipY_16; }
	inline bool* get_address_of_flipY_16() { return &___flipY_16; }
	inline void set_flipY_16(bool value)
	{
		___flipY_16 = value;
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___x_17)); }
	inline float get_x_17() const { return ___x_17; }
	inline float* get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(float value)
	{
		___x_17 = value;
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(Skeleton_t3686076450, ___y_18)); }
	inline float get_y_18() const { return ___y_18; }
	inline float* get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(float value)
	{
		___y_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETON_T3686076450_H
#ifndef ATLASREGION_T13903284_H
#define ATLASREGION_T13903284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasRegion
struct  AtlasRegion_t13903284  : public RuntimeObject
{
public:
	// Spine.AtlasPage Spine.AtlasRegion::page
	AtlasPage_t4077017671 * ___page_0;
	// System.String Spine.AtlasRegion::name
	String_t* ___name_1;
	// System.Int32 Spine.AtlasRegion::x
	int32_t ___x_2;
	// System.Int32 Spine.AtlasRegion::y
	int32_t ___y_3;
	// System.Int32 Spine.AtlasRegion::width
	int32_t ___width_4;
	// System.Int32 Spine.AtlasRegion::height
	int32_t ___height_5;
	// System.Single Spine.AtlasRegion::u
	float ___u_6;
	// System.Single Spine.AtlasRegion::v
	float ___v_7;
	// System.Single Spine.AtlasRegion::u2
	float ___u2_8;
	// System.Single Spine.AtlasRegion::v2
	float ___v2_9;
	// System.Single Spine.AtlasRegion::offsetX
	float ___offsetX_10;
	// System.Single Spine.AtlasRegion::offsetY
	float ___offsetY_11;
	// System.Int32 Spine.AtlasRegion::originalWidth
	int32_t ___originalWidth_12;
	// System.Int32 Spine.AtlasRegion::originalHeight
	int32_t ___originalHeight_13;
	// System.Int32 Spine.AtlasRegion::index
	int32_t ___index_14;
	// System.Boolean Spine.AtlasRegion::rotate
	bool ___rotate_15;
	// System.Int32[] Spine.AtlasRegion::splits
	Int32U5BU5D_t385246372* ___splits_16;
	// System.Int32[] Spine.AtlasRegion::pads
	Int32U5BU5D_t385246372* ___pads_17;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___page_0)); }
	inline AtlasPage_t4077017671 * get_page_0() const { return ___page_0; }
	inline AtlasPage_t4077017671 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(AtlasPage_t4077017671 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_u_6() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___u_6)); }
	inline float get_u_6() const { return ___u_6; }
	inline float* get_address_of_u_6() { return &___u_6; }
	inline void set_u_6(float value)
	{
		___u_6 = value;
	}

	inline static int32_t get_offset_of_v_7() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___v_7)); }
	inline float get_v_7() const { return ___v_7; }
	inline float* get_address_of_v_7() { return &___v_7; }
	inline void set_v_7(float value)
	{
		___v_7 = value;
	}

	inline static int32_t get_offset_of_u2_8() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___u2_8)); }
	inline float get_u2_8() const { return ___u2_8; }
	inline float* get_address_of_u2_8() { return &___u2_8; }
	inline void set_u2_8(float value)
	{
		___u2_8 = value;
	}

	inline static int32_t get_offset_of_v2_9() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___v2_9)); }
	inline float get_v2_9() const { return ___v2_9; }
	inline float* get_address_of_v2_9() { return &___v2_9; }
	inline void set_v2_9(float value)
	{
		___v2_9 = value;
	}

	inline static int32_t get_offset_of_offsetX_10() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___offsetX_10)); }
	inline float get_offsetX_10() const { return ___offsetX_10; }
	inline float* get_address_of_offsetX_10() { return &___offsetX_10; }
	inline void set_offsetX_10(float value)
	{
		___offsetX_10 = value;
	}

	inline static int32_t get_offset_of_offsetY_11() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___offsetY_11)); }
	inline float get_offsetY_11() const { return ___offsetY_11; }
	inline float* get_address_of_offsetY_11() { return &___offsetY_11; }
	inline void set_offsetY_11(float value)
	{
		___offsetY_11 = value;
	}

	inline static int32_t get_offset_of_originalWidth_12() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___originalWidth_12)); }
	inline int32_t get_originalWidth_12() const { return ___originalWidth_12; }
	inline int32_t* get_address_of_originalWidth_12() { return &___originalWidth_12; }
	inline void set_originalWidth_12(int32_t value)
	{
		___originalWidth_12 = value;
	}

	inline static int32_t get_offset_of_originalHeight_13() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___originalHeight_13)); }
	inline int32_t get_originalHeight_13() const { return ___originalHeight_13; }
	inline int32_t* get_address_of_originalHeight_13() { return &___originalHeight_13; }
	inline void set_originalHeight_13(int32_t value)
	{
		___originalHeight_13 = value;
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of_rotate_15() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___rotate_15)); }
	inline bool get_rotate_15() const { return ___rotate_15; }
	inline bool* get_address_of_rotate_15() { return &___rotate_15; }
	inline void set_rotate_15(bool value)
	{
		___rotate_15 = value;
	}

	inline static int32_t get_offset_of_splits_16() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___splits_16)); }
	inline Int32U5BU5D_t385246372* get_splits_16() const { return ___splits_16; }
	inline Int32U5BU5D_t385246372** get_address_of_splits_16() { return &___splits_16; }
	inline void set_splits_16(Int32U5BU5D_t385246372* value)
	{
		___splits_16 = value;
		Il2CppCodeGenWriteBarrier((&___splits_16), value);
	}

	inline static int32_t get_offset_of_pads_17() { return static_cast<int32_t>(offsetof(AtlasRegion_t13903284, ___pads_17)); }
	inline Int32U5BU5D_t385246372* get_pads_17() const { return ___pads_17; }
	inline Int32U5BU5D_t385246372** get_address_of_pads_17() { return &___pads_17; }
	inline void set_pads_17(Int32U5BU5D_t385246372* value)
	{
		___pads_17 = value;
		Il2CppCodeGenWriteBarrier((&___pads_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASREGION_T13903284_H
#ifndef ATLASATTACHMENTLOADER_T3966790320_H
#define ATLASATTACHMENTLOADER_T3966790320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasAttachmentLoader
struct  AtlasAttachmentLoader_t3966790320  : public RuntimeObject
{
public:
	// Spine.Atlas[] Spine.AtlasAttachmentLoader::atlasArray
	AtlasU5BU5D_t3463999232* ___atlasArray_0;

public:
	inline static int32_t get_offset_of_atlasArray_0() { return static_cast<int32_t>(offsetof(AtlasAttachmentLoader_t3966790320, ___atlasArray_0)); }
	inline AtlasU5BU5D_t3463999232* get_atlasArray_0() const { return ___atlasArray_0; }
	inline AtlasU5BU5D_t3463999232** get_address_of_atlasArray_0() { return &___atlasArray_0; }
	inline void set_atlasArray_0(AtlasU5BU5D_t3463999232* value)
	{
		___atlasArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASATTACHMENTLOADER_T3966790320_H
#ifndef DRAWORDERTIMELINE_T1092982325_H
#define DRAWORDERTIMELINE_T1092982325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DrawOrderTimeline
struct  DrawOrderTimeline_t1092982325  : public RuntimeObject
{
public:
	// System.Single[] Spine.DrawOrderTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_0;
	// System.Int32[][] Spine.DrawOrderTimeline::drawOrders
	Int32U5BU5DU5BU5D_t3365920845* ___drawOrders_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t1092982325, ___frames_0)); }
	inline SingleU5BU5D_t1444911251* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_t1444911251* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_drawOrders_1() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t1092982325, ___drawOrders_1)); }
	inline Int32U5BU5DU5BU5D_t3365920845* get_drawOrders_1() const { return ___drawOrders_1; }
	inline Int32U5BU5DU5BU5D_t3365920845** get_address_of_drawOrders_1() { return &___drawOrders_1; }
	inline void set_drawOrders_1(Int32U5BU5DU5BU5D_t3365920845* value)
	{
		___drawOrders_1 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrders_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWORDERTIMELINE_T1092982325_H
#ifndef EVENTTIMELINE_T2341881094_H
#define EVENTTIMELINE_T2341881094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventTimeline
struct  EventTimeline_t2341881094  : public RuntimeObject
{
public:
	// System.Single[] Spine.EventTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_0;
	// Spine.Event[] Spine.EventTimeline::events
	EventU5BU5D_t1966750348* ___events_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(EventTimeline_t2341881094, ___frames_0)); }
	inline SingleU5BU5D_t1444911251* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_t1444911251* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_events_1() { return static_cast<int32_t>(offsetof(EventTimeline_t2341881094, ___events_1)); }
	inline EventU5BU5D_t1966750348* get_events_1() const { return ___events_1; }
	inline EventU5BU5D_t1966750348** get_address_of_events_1() { return &___events_1; }
	inline void set_events_1(EventU5BU5D_t1966750348* value)
	{
		___events_1 = value;
		Il2CppCodeGenWriteBarrier((&___events_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTIMELINE_T2341881094_H
#ifndef ATTACHMENT_T3043756552_H
#define ATTACHMENT_T3043756552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Attachment
struct  Attachment_t3043756552  : public RuntimeObject
{
public:
	// System.String Spine.Attachment::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Attachment_t3043756552, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENT_T3043756552_H
#ifndef ATTACHMENTTIMELINE_T2048728856_H
#define ATTACHMENTTIMELINE_T2048728856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentTimeline
struct  AttachmentTimeline_t2048728856  : public RuntimeObject
{
public:
	// System.Int32 Spine.AttachmentTimeline::slotIndex
	int32_t ___slotIndex_0;
	// System.Single[] Spine.AttachmentTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_1;
	// System.String[] Spine.AttachmentTimeline::attachmentNames
	StringU5BU5D_t1281789340* ___attachmentNames_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2048728856, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2048728856, ___frames_1)); }
	inline SingleU5BU5D_t1444911251* get_frames_1() const { return ___frames_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(SingleU5BU5D_t1444911251* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_attachmentNames_2() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2048728856, ___attachmentNames_2)); }
	inline StringU5BU5D_t1281789340* get_attachmentNames_2() const { return ___attachmentNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_attachmentNames_2() { return &___attachmentNames_2; }
	inline void set_attachmentNames_2(StringU5BU5D_t1281789340* value)
	{
		___attachmentNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentNames_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTIMELINE_T2048728856_H
#ifndef PATHCONSTRAINT_T980854910_H
#define PATHCONSTRAINT_T980854910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraint
struct  PathConstraint_t980854910  : public RuntimeObject
{
public:
	// Spine.PathConstraintData Spine.PathConstraint::data
	PathConstraintData_t981297034 * ___data_4;
	// Spine.ExposedList`1<Spine.Bone> Spine.PathConstraint::bones
	ExposedList_1_t3793468194 * ___bones_5;
	// Spine.Slot Spine.PathConstraint::target
	Slot_t3514940700 * ___target_6;
	// System.Single Spine.PathConstraint::position
	float ___position_7;
	// System.Single Spine.PathConstraint::spacing
	float ___spacing_8;
	// System.Single Spine.PathConstraint::rotateMix
	float ___rotateMix_9;
	// System.Single Spine.PathConstraint::translateMix
	float ___translateMix_10;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::spaces
	ExposedList_1_t4104378640 * ___spaces_11;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::positions
	ExposedList_1_t4104378640 * ___positions_12;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::world
	ExposedList_1_t4104378640 * ___world_13;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::curves
	ExposedList_1_t4104378640 * ___curves_14;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::lengths
	ExposedList_1_t4104378640 * ___lengths_15;
	// System.Single[] Spine.PathConstraint::segments
	SingleU5BU5D_t1444911251* ___segments_16;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___data_4)); }
	inline PathConstraintData_t981297034 * get_data_4() const { return ___data_4; }
	inline PathConstraintData_t981297034 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(PathConstraintData_t981297034 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_bones_5() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___bones_5)); }
	inline ExposedList_1_t3793468194 * get_bones_5() const { return ___bones_5; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_5() { return &___bones_5; }
	inline void set_bones_5(ExposedList_1_t3793468194 * value)
	{
		___bones_5 = value;
		Il2CppCodeGenWriteBarrier((&___bones_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___target_6)); }
	inline Slot_t3514940700 * get_target_6() const { return ___target_6; }
	inline Slot_t3514940700 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Slot_t3514940700 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___position_7)); }
	inline float get_position_7() const { return ___position_7; }
	inline float* get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(float value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_spacing_8() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___spacing_8)); }
	inline float get_spacing_8() const { return ___spacing_8; }
	inline float* get_address_of_spacing_8() { return &___spacing_8; }
	inline void set_spacing_8(float value)
	{
		___spacing_8 = value;
	}

	inline static int32_t get_offset_of_rotateMix_9() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___rotateMix_9)); }
	inline float get_rotateMix_9() const { return ___rotateMix_9; }
	inline float* get_address_of_rotateMix_9() { return &___rotateMix_9; }
	inline void set_rotateMix_9(float value)
	{
		___rotateMix_9 = value;
	}

	inline static int32_t get_offset_of_translateMix_10() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___translateMix_10)); }
	inline float get_translateMix_10() const { return ___translateMix_10; }
	inline float* get_address_of_translateMix_10() { return &___translateMix_10; }
	inline void set_translateMix_10(float value)
	{
		___translateMix_10 = value;
	}

	inline static int32_t get_offset_of_spaces_11() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___spaces_11)); }
	inline ExposedList_1_t4104378640 * get_spaces_11() const { return ___spaces_11; }
	inline ExposedList_1_t4104378640 ** get_address_of_spaces_11() { return &___spaces_11; }
	inline void set_spaces_11(ExposedList_1_t4104378640 * value)
	{
		___spaces_11 = value;
		Il2CppCodeGenWriteBarrier((&___spaces_11), value);
	}

	inline static int32_t get_offset_of_positions_12() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___positions_12)); }
	inline ExposedList_1_t4104378640 * get_positions_12() const { return ___positions_12; }
	inline ExposedList_1_t4104378640 ** get_address_of_positions_12() { return &___positions_12; }
	inline void set_positions_12(ExposedList_1_t4104378640 * value)
	{
		___positions_12 = value;
		Il2CppCodeGenWriteBarrier((&___positions_12), value);
	}

	inline static int32_t get_offset_of_world_13() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___world_13)); }
	inline ExposedList_1_t4104378640 * get_world_13() const { return ___world_13; }
	inline ExposedList_1_t4104378640 ** get_address_of_world_13() { return &___world_13; }
	inline void set_world_13(ExposedList_1_t4104378640 * value)
	{
		___world_13 = value;
		Il2CppCodeGenWriteBarrier((&___world_13), value);
	}

	inline static int32_t get_offset_of_curves_14() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___curves_14)); }
	inline ExposedList_1_t4104378640 * get_curves_14() const { return ___curves_14; }
	inline ExposedList_1_t4104378640 ** get_address_of_curves_14() { return &___curves_14; }
	inline void set_curves_14(ExposedList_1_t4104378640 * value)
	{
		___curves_14 = value;
		Il2CppCodeGenWriteBarrier((&___curves_14), value);
	}

	inline static int32_t get_offset_of_lengths_15() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___lengths_15)); }
	inline ExposedList_1_t4104378640 * get_lengths_15() const { return ___lengths_15; }
	inline ExposedList_1_t4104378640 ** get_address_of_lengths_15() { return &___lengths_15; }
	inline void set_lengths_15(ExposedList_1_t4104378640 * value)
	{
		___lengths_15 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_15), value);
	}

	inline static int32_t get_offset_of_segments_16() { return static_cast<int32_t>(offsetof(PathConstraint_t980854910, ___segments_16)); }
	inline SingleU5BU5D_t1444911251* get_segments_16() const { return ___segments_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_segments_16() { return &___segments_16; }
	inline void set_segments_16(SingleU5BU5D_t1444911251* value)
	{
		___segments_16 = value;
		Il2CppCodeGenWriteBarrier((&___segments_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINT_T980854910_H
#ifndef MATHUTILS_T3604673275_H
#define MATHUTILS_T3604673275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MathUtils
struct  MathUtils_t3604673275  : public RuntimeObject
{
public:

public:
};

struct MathUtils_t3604673275_StaticFields
{
public:
	// System.Single[] Spine.MathUtils::sin
	SingleU5BU5D_t1444911251* ___sin_11;

public:
	inline static int32_t get_offset_of_sin_11() { return static_cast<int32_t>(offsetof(MathUtils_t3604673275_StaticFields, ___sin_11)); }
	inline SingleU5BU5D_t1444911251* get_sin_11() const { return ___sin_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_sin_11() { return &___sin_11; }
	inline void set_sin_11(SingleU5BU5D_t1444911251* value)
	{
		___sin_11 = value;
		Il2CppCodeGenWriteBarrier((&___sin_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T3604673275_H
#ifndef SKELETONCLIPPING_T1669006083_H
#define SKELETONCLIPPING_T1669006083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonClipping
struct  SkeletonClipping_t1669006083  : public RuntimeObject
{
public:
	// Spine.Triangulator Spine.SkeletonClipping::triangulator
	Triangulator_t2502879214 * ___triangulator_0;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippingPolygon
	ExposedList_1_t4104378640 * ___clippingPolygon_1;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clipOutput
	ExposedList_1_t4104378640 * ___clipOutput_2;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedVertices
	ExposedList_1_t4104378640 * ___clippedVertices_3;
	// Spine.ExposedList`1<System.Int32> Spine.SkeletonClipping::clippedTriangles
	ExposedList_1_t1363090323 * ___clippedTriangles_4;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedUVs
	ExposedList_1_t4104378640 * ___clippedUVs_5;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::scratch
	ExposedList_1_t4104378640 * ___scratch_6;
	// Spine.ClippingAttachment Spine.SkeletonClipping::clipAttachment
	ClippingAttachment_t2586274570 * ___clipAttachment_7;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.SkeletonClipping::clippingPolygons
	ExposedList_1_t2516523210 * ___clippingPolygons_8;

public:
	inline static int32_t get_offset_of_triangulator_0() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___triangulator_0)); }
	inline Triangulator_t2502879214 * get_triangulator_0() const { return ___triangulator_0; }
	inline Triangulator_t2502879214 ** get_address_of_triangulator_0() { return &___triangulator_0; }
	inline void set_triangulator_0(Triangulator_t2502879214 * value)
	{
		___triangulator_0 = value;
		Il2CppCodeGenWriteBarrier((&___triangulator_0), value);
	}

	inline static int32_t get_offset_of_clippingPolygon_1() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippingPolygon_1)); }
	inline ExposedList_1_t4104378640 * get_clippingPolygon_1() const { return ___clippingPolygon_1; }
	inline ExposedList_1_t4104378640 ** get_address_of_clippingPolygon_1() { return &___clippingPolygon_1; }
	inline void set_clippingPolygon_1(ExposedList_1_t4104378640 * value)
	{
		___clippingPolygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygon_1), value);
	}

	inline static int32_t get_offset_of_clipOutput_2() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clipOutput_2)); }
	inline ExposedList_1_t4104378640 * get_clipOutput_2() const { return ___clipOutput_2; }
	inline ExposedList_1_t4104378640 ** get_address_of_clipOutput_2() { return &___clipOutput_2; }
	inline void set_clipOutput_2(ExposedList_1_t4104378640 * value)
	{
		___clipOutput_2 = value;
		Il2CppCodeGenWriteBarrier((&___clipOutput_2), value);
	}

	inline static int32_t get_offset_of_clippedVertices_3() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippedVertices_3)); }
	inline ExposedList_1_t4104378640 * get_clippedVertices_3() const { return ___clippedVertices_3; }
	inline ExposedList_1_t4104378640 ** get_address_of_clippedVertices_3() { return &___clippedVertices_3; }
	inline void set_clippedVertices_3(ExposedList_1_t4104378640 * value)
	{
		___clippedVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___clippedVertices_3), value);
	}

	inline static int32_t get_offset_of_clippedTriangles_4() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippedTriangles_4)); }
	inline ExposedList_1_t1363090323 * get_clippedTriangles_4() const { return ___clippedTriangles_4; }
	inline ExposedList_1_t1363090323 ** get_address_of_clippedTriangles_4() { return &___clippedTriangles_4; }
	inline void set_clippedTriangles_4(ExposedList_1_t1363090323 * value)
	{
		___clippedTriangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___clippedTriangles_4), value);
	}

	inline static int32_t get_offset_of_clippedUVs_5() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippedUVs_5)); }
	inline ExposedList_1_t4104378640 * get_clippedUVs_5() const { return ___clippedUVs_5; }
	inline ExposedList_1_t4104378640 ** get_address_of_clippedUVs_5() { return &___clippedUVs_5; }
	inline void set_clippedUVs_5(ExposedList_1_t4104378640 * value)
	{
		___clippedUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___clippedUVs_5), value);
	}

	inline static int32_t get_offset_of_scratch_6() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___scratch_6)); }
	inline ExposedList_1_t4104378640 * get_scratch_6() const { return ___scratch_6; }
	inline ExposedList_1_t4104378640 ** get_address_of_scratch_6() { return &___scratch_6; }
	inline void set_scratch_6(ExposedList_1_t4104378640 * value)
	{
		___scratch_6 = value;
		Il2CppCodeGenWriteBarrier((&___scratch_6), value);
	}

	inline static int32_t get_offset_of_clipAttachment_7() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clipAttachment_7)); }
	inline ClippingAttachment_t2586274570 * get_clipAttachment_7() const { return ___clipAttachment_7; }
	inline ClippingAttachment_t2586274570 ** get_address_of_clipAttachment_7() { return &___clipAttachment_7; }
	inline void set_clipAttachment_7(ClippingAttachment_t2586274570 * value)
	{
		___clipAttachment_7 = value;
		Il2CppCodeGenWriteBarrier((&___clipAttachment_7), value);
	}

	inline static int32_t get_offset_of_clippingPolygons_8() { return static_cast<int32_t>(offsetof(SkeletonClipping_t1669006083, ___clippingPolygons_8)); }
	inline ExposedList_1_t2516523210 * get_clippingPolygons_8() const { return ___clippingPolygons_8; }
	inline ExposedList_1_t2516523210 ** get_address_of_clippingPolygons_8() { return &___clippingPolygons_8; }
	inline void set_clippingPolygons_8(ExposedList_1_t2516523210 * value)
	{
		___clippingPolygons_8 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygons_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONCLIPPING_T1669006083_H
#ifndef ANIMATIONSTATEDATA_T3010651567_H
#define ANIMATIONSTATEDATA_T3010651567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData
struct  AnimationStateData_t3010651567  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.AnimationStateData::skeletonData
	SkeletonData_t2032710716 * ___skeletonData_0;
	// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single> Spine.AnimationStateData::animationToMixTime
	Dictionary_2_t680659097 * ___animationToMixTime_1;
	// System.Single Spine.AnimationStateData::defaultMix
	float ___defaultMix_2;

public:
	inline static int32_t get_offset_of_skeletonData_0() { return static_cast<int32_t>(offsetof(AnimationStateData_t3010651567, ___skeletonData_0)); }
	inline SkeletonData_t2032710716 * get_skeletonData_0() const { return ___skeletonData_0; }
	inline SkeletonData_t2032710716 ** get_address_of_skeletonData_0() { return &___skeletonData_0; }
	inline void set_skeletonData_0(SkeletonData_t2032710716 * value)
	{
		___skeletonData_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_0), value);
	}

	inline static int32_t get_offset_of_animationToMixTime_1() { return static_cast<int32_t>(offsetof(AnimationStateData_t3010651567, ___animationToMixTime_1)); }
	inline Dictionary_2_t680659097 * get_animationToMixTime_1() const { return ___animationToMixTime_1; }
	inline Dictionary_2_t680659097 ** get_address_of_animationToMixTime_1() { return &___animationToMixTime_1; }
	inline void set_animationToMixTime_1(Dictionary_2_t680659097 * value)
	{
		___animationToMixTime_1 = value;
		Il2CppCodeGenWriteBarrier((&___animationToMixTime_1), value);
	}

	inline static int32_t get_offset_of_defaultMix_2() { return static_cast<int32_t>(offsetof(AnimationStateData_t3010651567, ___defaultMix_2)); }
	inline float get_defaultMix_2() const { return ___defaultMix_2; }
	inline float* get_address_of_defaultMix_2() { return &___defaultMix_2; }
	inline void set_defaultMix_2(float value)
	{
		___defaultMix_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATEDATA_T3010651567_H
#ifndef SKELETONBINARY_T1796686580_H
#define SKELETONBINARY_T1796686580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary
struct  SkeletonBinary_t1796686580  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonBinary::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_13;
	// Spine.AttachmentLoader Spine.SkeletonBinary::attachmentLoader
	RuntimeObject* ___attachmentLoader_14;
	// System.Byte[] Spine.SkeletonBinary::buffer
	ByteU5BU5D_t4116647657* ___buffer_15;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonBinary::linkedMeshes
	List_1_t3168994201 * ___linkedMeshes_16;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___U3CScaleU3Ek__BackingField_13)); }
	inline float get_U3CScaleU3Ek__BackingField_13() const { return ___U3CScaleU3Ek__BackingField_13; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_13() { return &___U3CScaleU3Ek__BackingField_13; }
	inline void set_U3CScaleU3Ek__BackingField_13(float value)
	{
		___U3CScaleU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_14() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___attachmentLoader_14)); }
	inline RuntimeObject* get_attachmentLoader_14() const { return ___attachmentLoader_14; }
	inline RuntimeObject** get_address_of_attachmentLoader_14() { return &___attachmentLoader_14; }
	inline void set_attachmentLoader_14(RuntimeObject* value)
	{
		___attachmentLoader_14 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_14), value);
	}

	inline static int32_t get_offset_of_buffer_15() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___buffer_15)); }
	inline ByteU5BU5D_t4116647657* get_buffer_15() const { return ___buffer_15; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_15() { return &___buffer_15; }
	inline void set_buffer_15(ByteU5BU5D_t4116647657* value)
	{
		___buffer_15 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_15), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_16() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580, ___linkedMeshes_16)); }
	inline List_1_t3168994201 * get_linkedMeshes_16() const { return ___linkedMeshes_16; }
	inline List_1_t3168994201 ** get_address_of_linkedMeshes_16() { return &___linkedMeshes_16; }
	inline void set_linkedMeshes_16(List_1_t3168994201 * value)
	{
		___linkedMeshes_16 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_16), value);
	}
};

struct SkeletonBinary_t1796686580_StaticFields
{
public:
	// Spine.TransformMode[] Spine.SkeletonBinary::TransformModeValues
	TransformModeU5BU5D_t3210249249* ___TransformModeValues_17;

public:
	inline static int32_t get_offset_of_TransformModeValues_17() { return static_cast<int32_t>(offsetof(SkeletonBinary_t1796686580_StaticFields, ___TransformModeValues_17)); }
	inline TransformModeU5BU5D_t3210249249* get_TransformModeValues_17() const { return ___TransformModeValues_17; }
	inline TransformModeU5BU5D_t3210249249** get_address_of_TransformModeValues_17() { return &___TransformModeValues_17; }
	inline void set_TransformModeValues_17(TransformModeU5BU5D_t3210249249* value)
	{
		___TransformModeValues_17 = value;
		Il2CppCodeGenWriteBarrier((&___TransformModeValues_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBINARY_T1796686580_H
#ifndef VERTICES_T807797978_H
#define VERTICES_T807797978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary/Vertices
struct  Vertices_t807797978  : public RuntimeObject
{
public:
	// System.Int32[] Spine.SkeletonBinary/Vertices::bones
	Int32U5BU5D_t385246372* ___bones_0;
	// System.Single[] Spine.SkeletonBinary/Vertices::vertices
	SingleU5BU5D_t1444911251* ___vertices_1;

public:
	inline static int32_t get_offset_of_bones_0() { return static_cast<int32_t>(offsetof(Vertices_t807797978, ___bones_0)); }
	inline Int32U5BU5D_t385246372* get_bones_0() const { return ___bones_0; }
	inline Int32U5BU5D_t385246372** get_address_of_bones_0() { return &___bones_0; }
	inline void set_bones_0(Int32U5BU5D_t385246372* value)
	{
		___bones_0 = value;
		Il2CppCodeGenWriteBarrier((&___bones_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(Vertices_t807797978, ___vertices_1)); }
	inline SingleU5BU5D_t1444911251* get_vertices_1() const { return ___vertices_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(SingleU5BU5D_t1444911251* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICES_T807797978_H
#ifndef EVENTQUEUE_T808091267_H
#define EVENTQUEUE_T808091267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue
struct  EventQueue_t808091267  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry> Spine.EventQueue::eventQueueEntries
	List_1_t1823906703 * ___eventQueueEntries_0;
	// System.Boolean Spine.EventQueue::drainDisabled
	bool ___drainDisabled_1;
	// Spine.AnimationState Spine.EventQueue::state
	AnimationState_t3637309382 * ___state_2;
	// Spine.Pool`1<Spine.TrackEntry> Spine.EventQueue::trackEntryPool
	Pool_1_t287387900 * ___trackEntryPool_3;
	// System.Action Spine.EventQueue::AnimationsChanged
	Action_t1264377477 * ___AnimationsChanged_4;

public:
	inline static int32_t get_offset_of_eventQueueEntries_0() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___eventQueueEntries_0)); }
	inline List_1_t1823906703 * get_eventQueueEntries_0() const { return ___eventQueueEntries_0; }
	inline List_1_t1823906703 ** get_address_of_eventQueueEntries_0() { return &___eventQueueEntries_0; }
	inline void set_eventQueueEntries_0(List_1_t1823906703 * value)
	{
		___eventQueueEntries_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventQueueEntries_0), value);
	}

	inline static int32_t get_offset_of_drainDisabled_1() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___drainDisabled_1)); }
	inline bool get_drainDisabled_1() const { return ___drainDisabled_1; }
	inline bool* get_address_of_drainDisabled_1() { return &___drainDisabled_1; }
	inline void set_drainDisabled_1(bool value)
	{
		___drainDisabled_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___state_2)); }
	inline AnimationState_t3637309382 * get_state_2() const { return ___state_2; }
	inline AnimationState_t3637309382 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(AnimationState_t3637309382 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_3() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___trackEntryPool_3)); }
	inline Pool_1_t287387900 * get_trackEntryPool_3() const { return ___trackEntryPool_3; }
	inline Pool_1_t287387900 ** get_address_of_trackEntryPool_3() { return &___trackEntryPool_3; }
	inline void set_trackEntryPool_3(Pool_1_t287387900 * value)
	{
		___trackEntryPool_3 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_3), value);
	}

	inline static int32_t get_offset_of_AnimationsChanged_4() { return static_cast<int32_t>(offsetof(EventQueue_t808091267, ___AnimationsChanged_4)); }
	inline Action_t1264377477 * get_AnimationsChanged_4() const { return ___AnimationsChanged_4; }
	inline Action_t1264377477 ** get_address_of_AnimationsChanged_4() { return &___AnimationsChanged_4; }
	inline void set_AnimationsChanged_4(Action_t1264377477 * value)
	{
		___AnimationsChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsChanged_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTQUEUE_T808091267_H
#ifndef TRACKENTRY_T1316488441_H
#define TRACKENTRY_T1316488441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TrackEntry
struct  TrackEntry_t1316488441  : public RuntimeObject
{
public:
	// Spine.Animation Spine.TrackEntry::animation
	Animation_t615783283 * ___animation_0;
	// Spine.TrackEntry Spine.TrackEntry::next
	TrackEntry_t1316488441 * ___next_1;
	// Spine.TrackEntry Spine.TrackEntry::mixingFrom
	TrackEntry_t1316488441 * ___mixingFrom_2;
	// System.Int32 Spine.TrackEntry::trackIndex
	int32_t ___trackIndex_3;
	// System.Boolean Spine.TrackEntry::loop
	bool ___loop_4;
	// System.Single Spine.TrackEntry::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.TrackEntry::attachmentThreshold
	float ___attachmentThreshold_6;
	// System.Single Spine.TrackEntry::drawOrderThreshold
	float ___drawOrderThreshold_7;
	// System.Single Spine.TrackEntry::animationStart
	float ___animationStart_8;
	// System.Single Spine.TrackEntry::animationEnd
	float ___animationEnd_9;
	// System.Single Spine.TrackEntry::animationLast
	float ___animationLast_10;
	// System.Single Spine.TrackEntry::nextAnimationLast
	float ___nextAnimationLast_11;
	// System.Single Spine.TrackEntry::delay
	float ___delay_12;
	// System.Single Spine.TrackEntry::trackTime
	float ___trackTime_13;
	// System.Single Spine.TrackEntry::trackLast
	float ___trackLast_14;
	// System.Single Spine.TrackEntry::nextTrackLast
	float ___nextTrackLast_15;
	// System.Single Spine.TrackEntry::trackEnd
	float ___trackEnd_16;
	// System.Single Spine.TrackEntry::timeScale
	float ___timeScale_17;
	// System.Single Spine.TrackEntry::alpha
	float ___alpha_18;
	// System.Single Spine.TrackEntry::mixTime
	float ___mixTime_19;
	// System.Single Spine.TrackEntry::mixDuration
	float ___mixDuration_20;
	// System.Single Spine.TrackEntry::interruptAlpha
	float ___interruptAlpha_21;
	// System.Single Spine.TrackEntry::totalAlpha
	float ___totalAlpha_22;
	// Spine.ExposedList`1<System.Int32> Spine.TrackEntry::timelineData
	ExposedList_1_t1363090323 * ___timelineData_23;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.TrackEntry::timelineDipMix
	ExposedList_1_t4023600307 * ___timelineDipMix_24;
	// Spine.ExposedList`1<System.Single> Spine.TrackEntry::timelinesRotation
	ExposedList_1_t4104378640 * ___timelinesRotation_25;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Interrupt
	TrackEntryDelegate_t363257942 * ___Interrupt_26;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::End
	TrackEntryDelegate_t363257942 * ___End_27;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Dispose
	TrackEntryDelegate_t363257942 * ___Dispose_28;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Complete
	TrackEntryDelegate_t363257942 * ___Complete_29;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Start
	TrackEntryDelegate_t363257942 * ___Start_30;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.TrackEntry::Event
	TrackEntryEventDelegate_t1653995044 * ___Event_31;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animation_0)); }
	inline Animation_t615783283 * get_animation_0() const { return ___animation_0; }
	inline Animation_t615783283 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_t615783283 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___next_1)); }
	inline TrackEntry_t1316488441 * get_next_1() const { return ___next_1; }
	inline TrackEntry_t1316488441 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(TrackEntry_t1316488441 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_mixingFrom_2() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___mixingFrom_2)); }
	inline TrackEntry_t1316488441 * get_mixingFrom_2() const { return ___mixingFrom_2; }
	inline TrackEntry_t1316488441 ** get_address_of_mixingFrom_2() { return &___mixingFrom_2; }
	inline void set_mixingFrom_2(TrackEntry_t1316488441 * value)
	{
		___mixingFrom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mixingFrom_2), value);
	}

	inline static int32_t get_offset_of_trackIndex_3() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackIndex_3)); }
	inline int32_t get_trackIndex_3() const { return ___trackIndex_3; }
	inline int32_t* get_address_of_trackIndex_3() { return &___trackIndex_3; }
	inline void set_trackIndex_3(int32_t value)
	{
		___trackIndex_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_6() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___attachmentThreshold_6)); }
	inline float get_attachmentThreshold_6() const { return ___attachmentThreshold_6; }
	inline float* get_address_of_attachmentThreshold_6() { return &___attachmentThreshold_6; }
	inline void set_attachmentThreshold_6(float value)
	{
		___attachmentThreshold_6 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_7() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___drawOrderThreshold_7)); }
	inline float get_drawOrderThreshold_7() const { return ___drawOrderThreshold_7; }
	inline float* get_address_of_drawOrderThreshold_7() { return &___drawOrderThreshold_7; }
	inline void set_drawOrderThreshold_7(float value)
	{
		___drawOrderThreshold_7 = value;
	}

	inline static int32_t get_offset_of_animationStart_8() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animationStart_8)); }
	inline float get_animationStart_8() const { return ___animationStart_8; }
	inline float* get_address_of_animationStart_8() { return &___animationStart_8; }
	inline void set_animationStart_8(float value)
	{
		___animationStart_8 = value;
	}

	inline static int32_t get_offset_of_animationEnd_9() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animationEnd_9)); }
	inline float get_animationEnd_9() const { return ___animationEnd_9; }
	inline float* get_address_of_animationEnd_9() { return &___animationEnd_9; }
	inline void set_animationEnd_9(float value)
	{
		___animationEnd_9 = value;
	}

	inline static int32_t get_offset_of_animationLast_10() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___animationLast_10)); }
	inline float get_animationLast_10() const { return ___animationLast_10; }
	inline float* get_address_of_animationLast_10() { return &___animationLast_10; }
	inline void set_animationLast_10(float value)
	{
		___animationLast_10 = value;
	}

	inline static int32_t get_offset_of_nextAnimationLast_11() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___nextAnimationLast_11)); }
	inline float get_nextAnimationLast_11() const { return ___nextAnimationLast_11; }
	inline float* get_address_of_nextAnimationLast_11() { return &___nextAnimationLast_11; }
	inline void set_nextAnimationLast_11(float value)
	{
		___nextAnimationLast_11 = value;
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___delay_12)); }
	inline float get_delay_12() const { return ___delay_12; }
	inline float* get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(float value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_trackTime_13() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackTime_13)); }
	inline float get_trackTime_13() const { return ___trackTime_13; }
	inline float* get_address_of_trackTime_13() { return &___trackTime_13; }
	inline void set_trackTime_13(float value)
	{
		___trackTime_13 = value;
	}

	inline static int32_t get_offset_of_trackLast_14() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackLast_14)); }
	inline float get_trackLast_14() const { return ___trackLast_14; }
	inline float* get_address_of_trackLast_14() { return &___trackLast_14; }
	inline void set_trackLast_14(float value)
	{
		___trackLast_14 = value;
	}

	inline static int32_t get_offset_of_nextTrackLast_15() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___nextTrackLast_15)); }
	inline float get_nextTrackLast_15() const { return ___nextTrackLast_15; }
	inline float* get_address_of_nextTrackLast_15() { return &___nextTrackLast_15; }
	inline void set_nextTrackLast_15(float value)
	{
		___nextTrackLast_15 = value;
	}

	inline static int32_t get_offset_of_trackEnd_16() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___trackEnd_16)); }
	inline float get_trackEnd_16() const { return ___trackEnd_16; }
	inline float* get_address_of_trackEnd_16() { return &___trackEnd_16; }
	inline void set_trackEnd_16(float value)
	{
		___trackEnd_16 = value;
	}

	inline static int32_t get_offset_of_timeScale_17() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timeScale_17)); }
	inline float get_timeScale_17() const { return ___timeScale_17; }
	inline float* get_address_of_timeScale_17() { return &___timeScale_17; }
	inline void set_timeScale_17(float value)
	{
		___timeScale_17 = value;
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___alpha_18)); }
	inline float get_alpha_18() const { return ___alpha_18; }
	inline float* get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(float value)
	{
		___alpha_18 = value;
	}

	inline static int32_t get_offset_of_mixTime_19() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___mixTime_19)); }
	inline float get_mixTime_19() const { return ___mixTime_19; }
	inline float* get_address_of_mixTime_19() { return &___mixTime_19; }
	inline void set_mixTime_19(float value)
	{
		___mixTime_19 = value;
	}

	inline static int32_t get_offset_of_mixDuration_20() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___mixDuration_20)); }
	inline float get_mixDuration_20() const { return ___mixDuration_20; }
	inline float* get_address_of_mixDuration_20() { return &___mixDuration_20; }
	inline void set_mixDuration_20(float value)
	{
		___mixDuration_20 = value;
	}

	inline static int32_t get_offset_of_interruptAlpha_21() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___interruptAlpha_21)); }
	inline float get_interruptAlpha_21() const { return ___interruptAlpha_21; }
	inline float* get_address_of_interruptAlpha_21() { return &___interruptAlpha_21; }
	inline void set_interruptAlpha_21(float value)
	{
		___interruptAlpha_21 = value;
	}

	inline static int32_t get_offset_of_totalAlpha_22() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___totalAlpha_22)); }
	inline float get_totalAlpha_22() const { return ___totalAlpha_22; }
	inline float* get_address_of_totalAlpha_22() { return &___totalAlpha_22; }
	inline void set_totalAlpha_22(float value)
	{
		___totalAlpha_22 = value;
	}

	inline static int32_t get_offset_of_timelineData_23() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timelineData_23)); }
	inline ExposedList_1_t1363090323 * get_timelineData_23() const { return ___timelineData_23; }
	inline ExposedList_1_t1363090323 ** get_address_of_timelineData_23() { return &___timelineData_23; }
	inline void set_timelineData_23(ExposedList_1_t1363090323 * value)
	{
		___timelineData_23 = value;
		Il2CppCodeGenWriteBarrier((&___timelineData_23), value);
	}

	inline static int32_t get_offset_of_timelineDipMix_24() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timelineDipMix_24)); }
	inline ExposedList_1_t4023600307 * get_timelineDipMix_24() const { return ___timelineDipMix_24; }
	inline ExposedList_1_t4023600307 ** get_address_of_timelineDipMix_24() { return &___timelineDipMix_24; }
	inline void set_timelineDipMix_24(ExposedList_1_t4023600307 * value)
	{
		___timelineDipMix_24 = value;
		Il2CppCodeGenWriteBarrier((&___timelineDipMix_24), value);
	}

	inline static int32_t get_offset_of_timelinesRotation_25() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___timelinesRotation_25)); }
	inline ExposedList_1_t4104378640 * get_timelinesRotation_25() const { return ___timelinesRotation_25; }
	inline ExposedList_1_t4104378640 ** get_address_of_timelinesRotation_25() { return &___timelinesRotation_25; }
	inline void set_timelinesRotation_25(ExposedList_1_t4104378640 * value)
	{
		___timelinesRotation_25 = value;
		Il2CppCodeGenWriteBarrier((&___timelinesRotation_25), value);
	}

	inline static int32_t get_offset_of_Interrupt_26() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Interrupt_26)); }
	inline TrackEntryDelegate_t363257942 * get_Interrupt_26() const { return ___Interrupt_26; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Interrupt_26() { return &___Interrupt_26; }
	inline void set_Interrupt_26(TrackEntryDelegate_t363257942 * value)
	{
		___Interrupt_26 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_26), value);
	}

	inline static int32_t get_offset_of_End_27() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___End_27)); }
	inline TrackEntryDelegate_t363257942 * get_End_27() const { return ___End_27; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_End_27() { return &___End_27; }
	inline void set_End_27(TrackEntryDelegate_t363257942 * value)
	{
		___End_27 = value;
		Il2CppCodeGenWriteBarrier((&___End_27), value);
	}

	inline static int32_t get_offset_of_Dispose_28() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Dispose_28)); }
	inline TrackEntryDelegate_t363257942 * get_Dispose_28() const { return ___Dispose_28; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Dispose_28() { return &___Dispose_28; }
	inline void set_Dispose_28(TrackEntryDelegate_t363257942 * value)
	{
		___Dispose_28 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_28), value);
	}

	inline static int32_t get_offset_of_Complete_29() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Complete_29)); }
	inline TrackEntryDelegate_t363257942 * get_Complete_29() const { return ___Complete_29; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Complete_29() { return &___Complete_29; }
	inline void set_Complete_29(TrackEntryDelegate_t363257942 * value)
	{
		___Complete_29 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_29), value);
	}

	inline static int32_t get_offset_of_Start_30() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Start_30)); }
	inline TrackEntryDelegate_t363257942 * get_Start_30() const { return ___Start_30; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Start_30() { return &___Start_30; }
	inline void set_Start_30(TrackEntryDelegate_t363257942 * value)
	{
		___Start_30 = value;
		Il2CppCodeGenWriteBarrier((&___Start_30), value);
	}

	inline static int32_t get_offset_of_Event_31() { return static_cast<int32_t>(offsetof(TrackEntry_t1316488441, ___Event_31)); }
	inline TrackEntryEventDelegate_t1653995044 * get_Event_31() const { return ___Event_31; }
	inline TrackEntryEventDelegate_t1653995044 ** get_address_of_Event_31() { return &___Event_31; }
	inline void set_Event_31(TrackEntryEventDelegate_t1653995044 * value)
	{
		___Event_31 = value;
		Il2CppCodeGenWriteBarrier((&___Event_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRY_T1316488441_H
#ifndef SKELETONBOUNDS_T352077915_H
#define SKELETONBOUNDS_T352077915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBounds
struct  SkeletonBounds_t352077915  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::polygonPool
	ExposedList_1_t1727260827 * ___polygonPool_0;
	// System.Single Spine.SkeletonBounds::minX
	float ___minX_1;
	// System.Single Spine.SkeletonBounds::minY
	float ___minY_2;
	// System.Single Spine.SkeletonBounds::maxX
	float ___maxX_3;
	// System.Single Spine.SkeletonBounds::maxY
	float ___maxY_4;
	// Spine.ExposedList`1<Spine.BoundingBoxAttachment> Spine.SkeletonBounds::<BoundingBoxes>k__BackingField
	ExposedList_1_t1209651080 * ___U3CBoundingBoxesU3Ek__BackingField_5;
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::<Polygons>k__BackingField
	ExposedList_1_t1727260827 * ___U3CPolygonsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_polygonPool_0() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___polygonPool_0)); }
	inline ExposedList_1_t1727260827 * get_polygonPool_0() const { return ___polygonPool_0; }
	inline ExposedList_1_t1727260827 ** get_address_of_polygonPool_0() { return &___polygonPool_0; }
	inline void set_polygonPool_0(ExposedList_1_t1727260827 * value)
	{
		___polygonPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_0), value);
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___minX_1)); }
	inline float get_minX_1() const { return ___minX_1; }
	inline float* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(float value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___minY_2)); }
	inline float get_minY_2() const { return ___minY_2; }
	inline float* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(float value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_maxX_3() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___maxX_3)); }
	inline float get_maxX_3() const { return ___maxX_3; }
	inline float* get_address_of_maxX_3() { return &___maxX_3; }
	inline void set_maxX_3(float value)
	{
		___maxX_3 = value;
	}

	inline static int32_t get_offset_of_maxY_4() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___maxY_4)); }
	inline float get_maxY_4() const { return ___maxY_4; }
	inline float* get_address_of_maxY_4() { return &___maxY_4; }
	inline void set_maxY_4(float value)
	{
		___maxY_4 = value;
	}

	inline static int32_t get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___U3CBoundingBoxesU3Ek__BackingField_5)); }
	inline ExposedList_1_t1209651080 * get_U3CBoundingBoxesU3Ek__BackingField_5() const { return ___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline ExposedList_1_t1209651080 ** get_address_of_U3CBoundingBoxesU3Ek__BackingField_5() { return &___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline void set_U3CBoundingBoxesU3Ek__BackingField_5(ExposedList_1_t1209651080 * value)
	{
		___U3CBoundingBoxesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundingBoxesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPolygonsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SkeletonBounds_t352077915, ___U3CPolygonsU3Ek__BackingField_6)); }
	inline ExposedList_1_t1727260827 * get_U3CPolygonsU3Ek__BackingField_6() const { return ___U3CPolygonsU3Ek__BackingField_6; }
	inline ExposedList_1_t1727260827 ** get_address_of_U3CPolygonsU3Ek__BackingField_6() { return &___U3CPolygonsU3Ek__BackingField_6; }
	inline void set_U3CPolygonsU3Ek__BackingField_6(ExposedList_1_t1727260827 * value)
	{
		___U3CPolygonsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPolygonsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBOUNDS_T352077915_H
#ifndef POLYGON_T3315116257_H
#define POLYGON_T3315116257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Polygon
struct  Polygon_t3315116257  : public RuntimeObject
{
public:
	// System.Single[] Spine.Polygon::<Vertices>k__BackingField
	SingleU5BU5D_t1444911251* ___U3CVerticesU3Ek__BackingField_0;
	// System.Int32 Spine.Polygon::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CVerticesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Polygon_t3315116257, ___U3CVerticesU3Ek__BackingField_0)); }
	inline SingleU5BU5D_t1444911251* get_U3CVerticesU3Ek__BackingField_0() const { return ___U3CVerticesU3Ek__BackingField_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_U3CVerticesU3Ek__BackingField_0() { return &___U3CVerticesU3Ek__BackingField_0; }
	inline void set_U3CVerticesU3Ek__BackingField_0(SingleU5BU5D_t1444911251* value)
	{
		___U3CVerticesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVerticesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Polygon_t3315116257, ___U3CCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CCountU3Ek__BackingField_1() const { return ___U3CCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_1() { return &___U3CCountU3Ek__BackingField_1; }
	inline void set_U3CCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CCountU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T3315116257_H
#ifndef ANIMATIONSTATE_T3637309382_H
#define ANIMATIONSTATE_T3637309382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState
struct  AnimationState_t3637309382  : public RuntimeObject
{
public:
	// Spine.AnimationStateData Spine.AnimationState::data
	AnimationStateData_t3010651567 * ___data_5;
	// Spine.Pool`1<Spine.TrackEntry> Spine.AnimationState::trackEntryPool
	Pool_1_t287387900 * ___trackEntryPool_6;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::tracks
	ExposedList_1_t4023600307 * ___tracks_7;
	// Spine.ExposedList`1<Spine.Event> Spine.AnimationState::events
	ExposedList_1_t4085685707 * ___events_8;
	// Spine.EventQueue Spine.AnimationState::queue
	EventQueue_t808091267 * ___queue_9;
	// System.Collections.Generic.HashSet`1<System.Int32> Spine.AnimationState::propertyIDs
	HashSet_1_t1515895227 * ___propertyIDs_10;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::mixingTo
	ExposedList_1_t4023600307 * ___mixingTo_11;
	// System.Boolean Spine.AnimationState::animationsChanged
	bool ___animationsChanged_12;
	// System.Single Spine.AnimationState::timeScale
	float ___timeScale_13;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Interrupt
	TrackEntryDelegate_t363257942 * ___Interrupt_14;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::End
	TrackEntryDelegate_t363257942 * ___End_15;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Dispose
	TrackEntryDelegate_t363257942 * ___Dispose_16;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Complete
	TrackEntryDelegate_t363257942 * ___Complete_17;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Start
	TrackEntryDelegate_t363257942 * ___Start_18;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.AnimationState::Event
	TrackEntryEventDelegate_t1653995044 * ___Event_19;

public:
	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___data_5)); }
	inline AnimationStateData_t3010651567 * get_data_5() const { return ___data_5; }
	inline AnimationStateData_t3010651567 ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(AnimationStateData_t3010651567 * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier((&___data_5), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_6() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___trackEntryPool_6)); }
	inline Pool_1_t287387900 * get_trackEntryPool_6() const { return ___trackEntryPool_6; }
	inline Pool_1_t287387900 ** get_address_of_trackEntryPool_6() { return &___trackEntryPool_6; }
	inline void set_trackEntryPool_6(Pool_1_t287387900 * value)
	{
		___trackEntryPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_6), value);
	}

	inline static int32_t get_offset_of_tracks_7() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___tracks_7)); }
	inline ExposedList_1_t4023600307 * get_tracks_7() const { return ___tracks_7; }
	inline ExposedList_1_t4023600307 ** get_address_of_tracks_7() { return &___tracks_7; }
	inline void set_tracks_7(ExposedList_1_t4023600307 * value)
	{
		___tracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracks_7), value);
	}

	inline static int32_t get_offset_of_events_8() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___events_8)); }
	inline ExposedList_1_t4085685707 * get_events_8() const { return ___events_8; }
	inline ExposedList_1_t4085685707 ** get_address_of_events_8() { return &___events_8; }
	inline void set_events_8(ExposedList_1_t4085685707 * value)
	{
		___events_8 = value;
		Il2CppCodeGenWriteBarrier((&___events_8), value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___queue_9)); }
	inline EventQueue_t808091267 * get_queue_9() const { return ___queue_9; }
	inline EventQueue_t808091267 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(EventQueue_t808091267 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___queue_9), value);
	}

	inline static int32_t get_offset_of_propertyIDs_10() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___propertyIDs_10)); }
	inline HashSet_1_t1515895227 * get_propertyIDs_10() const { return ___propertyIDs_10; }
	inline HashSet_1_t1515895227 ** get_address_of_propertyIDs_10() { return &___propertyIDs_10; }
	inline void set_propertyIDs_10(HashSet_1_t1515895227 * value)
	{
		___propertyIDs_10 = value;
		Il2CppCodeGenWriteBarrier((&___propertyIDs_10), value);
	}

	inline static int32_t get_offset_of_mixingTo_11() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___mixingTo_11)); }
	inline ExposedList_1_t4023600307 * get_mixingTo_11() const { return ___mixingTo_11; }
	inline ExposedList_1_t4023600307 ** get_address_of_mixingTo_11() { return &___mixingTo_11; }
	inline void set_mixingTo_11(ExposedList_1_t4023600307 * value)
	{
		___mixingTo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mixingTo_11), value);
	}

	inline static int32_t get_offset_of_animationsChanged_12() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___animationsChanged_12)); }
	inline bool get_animationsChanged_12() const { return ___animationsChanged_12; }
	inline bool* get_address_of_animationsChanged_12() { return &___animationsChanged_12; }
	inline void set_animationsChanged_12(bool value)
	{
		___animationsChanged_12 = value;
	}

	inline static int32_t get_offset_of_timeScale_13() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___timeScale_13)); }
	inline float get_timeScale_13() const { return ___timeScale_13; }
	inline float* get_address_of_timeScale_13() { return &___timeScale_13; }
	inline void set_timeScale_13(float value)
	{
		___timeScale_13 = value;
	}

	inline static int32_t get_offset_of_Interrupt_14() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Interrupt_14)); }
	inline TrackEntryDelegate_t363257942 * get_Interrupt_14() const { return ___Interrupt_14; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Interrupt_14() { return &___Interrupt_14; }
	inline void set_Interrupt_14(TrackEntryDelegate_t363257942 * value)
	{
		___Interrupt_14 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_14), value);
	}

	inline static int32_t get_offset_of_End_15() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___End_15)); }
	inline TrackEntryDelegate_t363257942 * get_End_15() const { return ___End_15; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_End_15() { return &___End_15; }
	inline void set_End_15(TrackEntryDelegate_t363257942 * value)
	{
		___End_15 = value;
		Il2CppCodeGenWriteBarrier((&___End_15), value);
	}

	inline static int32_t get_offset_of_Dispose_16() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Dispose_16)); }
	inline TrackEntryDelegate_t363257942 * get_Dispose_16() const { return ___Dispose_16; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Dispose_16() { return &___Dispose_16; }
	inline void set_Dispose_16(TrackEntryDelegate_t363257942 * value)
	{
		___Dispose_16 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_16), value);
	}

	inline static int32_t get_offset_of_Complete_17() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Complete_17)); }
	inline TrackEntryDelegate_t363257942 * get_Complete_17() const { return ___Complete_17; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Complete_17() { return &___Complete_17; }
	inline void set_Complete_17(TrackEntryDelegate_t363257942 * value)
	{
		___Complete_17 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_17), value);
	}

	inline static int32_t get_offset_of_Start_18() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Start_18)); }
	inline TrackEntryDelegate_t363257942 * get_Start_18() const { return ___Start_18; }
	inline TrackEntryDelegate_t363257942 ** get_address_of_Start_18() { return &___Start_18; }
	inline void set_Start_18(TrackEntryDelegate_t363257942 * value)
	{
		___Start_18 = value;
		Il2CppCodeGenWriteBarrier((&___Start_18), value);
	}

	inline static int32_t get_offset_of_Event_19() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382, ___Event_19)); }
	inline TrackEntryEventDelegate_t1653995044 * get_Event_19() const { return ___Event_19; }
	inline TrackEntryEventDelegate_t1653995044 ** get_address_of_Event_19() { return &___Event_19; }
	inline void set_Event_19(TrackEntryEventDelegate_t1653995044 * value)
	{
		___Event_19 = value;
		Il2CppCodeGenWriteBarrier((&___Event_19), value);
	}
};

struct AnimationState_t3637309382_StaticFields
{
public:
	// Spine.Animation Spine.AnimationState::EmptyAnimation
	Animation_t615783283 * ___EmptyAnimation_0;

public:
	inline static int32_t get_offset_of_EmptyAnimation_0() { return static_cast<int32_t>(offsetof(AnimationState_t3637309382_StaticFields, ___EmptyAnimation_0)); }
	inline Animation_t615783283 * get_EmptyAnimation_0() const { return ___EmptyAnimation_0; }
	inline Animation_t615783283 ** get_address_of_EmptyAnimation_0() { return &___EmptyAnimation_0; }
	inline void set_EmptyAnimation_0(Animation_t615783283 * value)
	{
		___EmptyAnimation_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAnimation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T3637309382_H
#ifndef ATLAS_T4040192941_H
#define ATLAS_T4040192941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Atlas
struct  Atlas_t4040192941  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.AtlasPage> Spine.Atlas::pages
	List_1_t1254125117 * ___pages_0;
	// System.Collections.Generic.List`1<Spine.AtlasRegion> Spine.Atlas::regions
	List_1_t1485978026 * ___regions_1;
	// Spine.TextureLoader Spine.Atlas::textureLoader
	RuntimeObject* ___textureLoader_2;

public:
	inline static int32_t get_offset_of_pages_0() { return static_cast<int32_t>(offsetof(Atlas_t4040192941, ___pages_0)); }
	inline List_1_t1254125117 * get_pages_0() const { return ___pages_0; }
	inline List_1_t1254125117 ** get_address_of_pages_0() { return &___pages_0; }
	inline void set_pages_0(List_1_t1254125117 * value)
	{
		___pages_0 = value;
		Il2CppCodeGenWriteBarrier((&___pages_0), value);
	}

	inline static int32_t get_offset_of_regions_1() { return static_cast<int32_t>(offsetof(Atlas_t4040192941, ___regions_1)); }
	inline List_1_t1485978026 * get_regions_1() const { return ___regions_1; }
	inline List_1_t1485978026 ** get_address_of_regions_1() { return &___regions_1; }
	inline void set_regions_1(List_1_t1485978026 * value)
	{
		___regions_1 = value;
		Il2CppCodeGenWriteBarrier((&___regions_1), value);
	}

	inline static int32_t get_offset_of_textureLoader_2() { return static_cast<int32_t>(offsetof(Atlas_t4040192941, ___textureLoader_2)); }
	inline RuntimeObject* get_textureLoader_2() const { return ___textureLoader_2; }
	inline RuntimeObject** get_address_of_textureLoader_2() { return &___textureLoader_2; }
	inline void set_textureLoader_2(RuntimeObject* value)
	{
		___textureLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureLoader_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLAS_T4040192941_H
#ifndef EVENTDATA_T724759987_H
#define EVENTDATA_T724759987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventData
struct  EventData_t724759987  : public RuntimeObject
{
public:
	// System.String Spine.EventData::name
	String_t* ___name_0;
	// System.Int32 Spine.EventData::<Int>k__BackingField
	int32_t ___U3CIntU3Ek__BackingField_1;
	// System.Single Spine.EventData::<Float>k__BackingField
	float ___U3CFloatU3Ek__BackingField_2;
	// System.String Spine.EventData::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CIntU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___U3CIntU3Ek__BackingField_1)); }
	inline int32_t get_U3CIntU3Ek__BackingField_1() const { return ___U3CIntU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIntU3Ek__BackingField_1() { return &___U3CIntU3Ek__BackingField_1; }
	inline void set_U3CIntU3Ek__BackingField_1(int32_t value)
	{
		___U3CIntU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFloatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___U3CFloatU3Ek__BackingField_2)); }
	inline float get_U3CFloatU3Ek__BackingField_2() const { return ___U3CFloatU3Ek__BackingField_2; }
	inline float* get_address_of_U3CFloatU3Ek__BackingField_2() { return &___U3CFloatU3Ek__BackingField_2; }
	inline void set_U3CFloatU3Ek__BackingField_2(float value)
	{
		___U3CFloatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EventData_t724759987, ___U3CStringU3Ek__BackingField_3)); }
	inline String_t* get_U3CStringU3Ek__BackingField_3() const { return ___U3CStringU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_3() { return &___U3CStringU3Ek__BackingField_3; }
	inline void set_U3CStringU3Ek__BackingField_3(String_t* value)
	{
		___U3CStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T724759987_H
#ifndef SKELETONDATA_T2032710716_H
#define SKELETONDATA_T2032710716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonData
struct  SkeletonData_t2032710716  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonData::name
	String_t* ___name_0;
	// Spine.ExposedList`1<Spine.BoneData> Spine.SkeletonData::bones
	ExposedList_1_t1542319060 * ___bones_1;
	// Spine.ExposedList`1<Spine.SlotData> Spine.SkeletonData::slots
	ExposedList_1_t2861913768 * ___slots_2;
	// Spine.ExposedList`1<Spine.Skin> Spine.SkeletonData::skins
	ExposedList_1_t3881696472 * ___skins_3;
	// Spine.Skin Spine.SkeletonData::defaultSkin
	Skin_t1174584606 * ___defaultSkin_4;
	// Spine.ExposedList`1<Spine.EventData> Spine.SkeletonData::events
	ExposedList_1_t3431871853 * ___events_5;
	// Spine.ExposedList`1<Spine.Animation> Spine.SkeletonData::animations
	ExposedList_1_t3322895149 * ___animations_6;
	// Spine.ExposedList`1<Spine.IkConstraintData> Spine.SkeletonData::ikConstraints
	ExposedList_1_t3166231995 * ___ikConstraints_7;
	// Spine.ExposedList`1<Spine.TransformConstraintData> Spine.SkeletonData::transformConstraints
	ExposedList_1_t3236185212 * ___transformConstraints_8;
	// Spine.ExposedList`1<Spine.PathConstraintData> Spine.SkeletonData::pathConstraints
	ExposedList_1_t3688408900 * ___pathConstraints_9;
	// System.Single Spine.SkeletonData::width
	float ___width_10;
	// System.Single Spine.SkeletonData::height
	float ___height_11;
	// System.String Spine.SkeletonData::version
	String_t* ___version_12;
	// System.String Spine.SkeletonData::hash
	String_t* ___hash_13;
	// System.Single Spine.SkeletonData::fps
	float ___fps_14;
	// System.String Spine.SkeletonData::imagesPath
	String_t* ___imagesPath_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___bones_1)); }
	inline ExposedList_1_t1542319060 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t1542319060 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t1542319060 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___slots_2)); }
	inline ExposedList_1_t2861913768 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t2861913768 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t2861913768 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_skins_3() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___skins_3)); }
	inline ExposedList_1_t3881696472 * get_skins_3() const { return ___skins_3; }
	inline ExposedList_1_t3881696472 ** get_address_of_skins_3() { return &___skins_3; }
	inline void set_skins_3(ExposedList_1_t3881696472 * value)
	{
		___skins_3 = value;
		Il2CppCodeGenWriteBarrier((&___skins_3), value);
	}

	inline static int32_t get_offset_of_defaultSkin_4() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___defaultSkin_4)); }
	inline Skin_t1174584606 * get_defaultSkin_4() const { return ___defaultSkin_4; }
	inline Skin_t1174584606 ** get_address_of_defaultSkin_4() { return &___defaultSkin_4; }
	inline void set_defaultSkin_4(Skin_t1174584606 * value)
	{
		___defaultSkin_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSkin_4), value);
	}

	inline static int32_t get_offset_of_events_5() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___events_5)); }
	inline ExposedList_1_t3431871853 * get_events_5() const { return ___events_5; }
	inline ExposedList_1_t3431871853 ** get_address_of_events_5() { return &___events_5; }
	inline void set_events_5(ExposedList_1_t3431871853 * value)
	{
		___events_5 = value;
		Il2CppCodeGenWriteBarrier((&___events_5), value);
	}

	inline static int32_t get_offset_of_animations_6() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___animations_6)); }
	inline ExposedList_1_t3322895149 * get_animations_6() const { return ___animations_6; }
	inline ExposedList_1_t3322895149 ** get_address_of_animations_6() { return &___animations_6; }
	inline void set_animations_6(ExposedList_1_t3322895149 * value)
	{
		___animations_6 = value;
		Il2CppCodeGenWriteBarrier((&___animations_6), value);
	}

	inline static int32_t get_offset_of_ikConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___ikConstraints_7)); }
	inline ExposedList_1_t3166231995 * get_ikConstraints_7() const { return ___ikConstraints_7; }
	inline ExposedList_1_t3166231995 ** get_address_of_ikConstraints_7() { return &___ikConstraints_7; }
	inline void set_ikConstraints_7(ExposedList_1_t3166231995 * value)
	{
		___ikConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_7), value);
	}

	inline static int32_t get_offset_of_transformConstraints_8() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___transformConstraints_8)); }
	inline ExposedList_1_t3236185212 * get_transformConstraints_8() const { return ___transformConstraints_8; }
	inline ExposedList_1_t3236185212 ** get_address_of_transformConstraints_8() { return &___transformConstraints_8; }
	inline void set_transformConstraints_8(ExposedList_1_t3236185212 * value)
	{
		___transformConstraints_8 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_8), value);
	}

	inline static int32_t get_offset_of_pathConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___pathConstraints_9)); }
	inline ExposedList_1_t3688408900 * get_pathConstraints_9() const { return ___pathConstraints_9; }
	inline ExposedList_1_t3688408900 ** get_address_of_pathConstraints_9() { return &___pathConstraints_9; }
	inline void set_pathConstraints_9(ExposedList_1_t3688408900 * value)
	{
		___pathConstraints_9 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_9), value);
	}

	inline static int32_t get_offset_of_width_10() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___width_10)); }
	inline float get_width_10() const { return ___width_10; }
	inline float* get_address_of_width_10() { return &___width_10; }
	inline void set_width_10(float value)
	{
		___width_10 = value;
	}

	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___height_11)); }
	inline float get_height_11() const { return ___height_11; }
	inline float* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(float value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_version_12() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___version_12)); }
	inline String_t* get_version_12() const { return ___version_12; }
	inline String_t** get_address_of_version_12() { return &___version_12; }
	inline void set_version_12(String_t* value)
	{
		___version_12 = value;
		Il2CppCodeGenWriteBarrier((&___version_12), value);
	}

	inline static int32_t get_offset_of_hash_13() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___hash_13)); }
	inline String_t* get_hash_13() const { return ___hash_13; }
	inline String_t** get_address_of_hash_13() { return &___hash_13; }
	inline void set_hash_13(String_t* value)
	{
		___hash_13 = value;
		Il2CppCodeGenWriteBarrier((&___hash_13), value);
	}

	inline static int32_t get_offset_of_fps_14() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___fps_14)); }
	inline float get_fps_14() const { return ___fps_14; }
	inline float* get_address_of_fps_14() { return &___fps_14; }
	inline void set_fps_14(float value)
	{
		___fps_14 = value;
	}

	inline static int32_t get_offset_of_imagesPath_15() { return static_cast<int32_t>(offsetof(SkeletonData_t2032710716, ___imagesPath_15)); }
	inline String_t* get_imagesPath_15() const { return ___imagesPath_15; }
	inline String_t** get_address_of_imagesPath_15() { return &___imagesPath_15; }
	inline void set_imagesPath_15(String_t* value)
	{
		___imagesPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___imagesPath_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATA_T2032710716_H
#ifndef SLOT_T3514940700_H
#define SLOT_T3514940700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Slot
struct  Slot_t3514940700  : public RuntimeObject
{
public:
	// Spine.SlotData Spine.Slot::data
	SlotData_t154801902 * ___data_0;
	// Spine.Bone Spine.Slot::bone
	Bone_t1086356328 * ___bone_1;
	// System.Single Spine.Slot::r
	float ___r_2;
	// System.Single Spine.Slot::g
	float ___g_3;
	// System.Single Spine.Slot::b
	float ___b_4;
	// System.Single Spine.Slot::a
	float ___a_5;
	// System.Single Spine.Slot::r2
	float ___r2_6;
	// System.Single Spine.Slot::g2
	float ___g2_7;
	// System.Single Spine.Slot::b2
	float ___b2_8;
	// System.Boolean Spine.Slot::hasSecondColor
	bool ___hasSecondColor_9;
	// Spine.Attachment Spine.Slot::attachment
	Attachment_t3043756552 * ___attachment_10;
	// System.Single Spine.Slot::attachmentTime
	float ___attachmentTime_11;
	// Spine.ExposedList`1<System.Single> Spine.Slot::attachmentVertices
	ExposedList_1_t4104378640 * ___attachmentVertices_12;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___data_0)); }
	inline SlotData_t154801902 * get_data_0() const { return ___data_0; }
	inline SlotData_t154801902 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SlotData_t154801902 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bone_1() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___bone_1)); }
	inline Bone_t1086356328 * get_bone_1() const { return ___bone_1; }
	inline Bone_t1086356328 ** get_address_of_bone_1() { return &___bone_1; }
	inline void set_bone_1(Bone_t1086356328 * value)
	{
		___bone_1 = value;
		Il2CppCodeGenWriteBarrier((&___bone_1), value);
	}

	inline static int32_t get_offset_of_r_2() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___r_2)); }
	inline float get_r_2() const { return ___r_2; }
	inline float* get_address_of_r_2() { return &___r_2; }
	inline void set_r_2(float value)
	{
		___r_2 = value;
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___g_3)); }
	inline float get_g_3() const { return ___g_3; }
	inline float* get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(float value)
	{
		___g_3 = value;
	}

	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___b_4)); }
	inline float get_b_4() const { return ___b_4; }
	inline float* get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(float value)
	{
		___b_4 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_r2_6() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___r2_6)); }
	inline float get_r2_6() const { return ___r2_6; }
	inline float* get_address_of_r2_6() { return &___r2_6; }
	inline void set_r2_6(float value)
	{
		___r2_6 = value;
	}

	inline static int32_t get_offset_of_g2_7() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___g2_7)); }
	inline float get_g2_7() const { return ___g2_7; }
	inline float* get_address_of_g2_7() { return &___g2_7; }
	inline void set_g2_7(float value)
	{
		___g2_7 = value;
	}

	inline static int32_t get_offset_of_b2_8() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___b2_8)); }
	inline float get_b2_8() const { return ___b2_8; }
	inline float* get_address_of_b2_8() { return &___b2_8; }
	inline void set_b2_8(float value)
	{
		___b2_8 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_9() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___hasSecondColor_9)); }
	inline bool get_hasSecondColor_9() const { return ___hasSecondColor_9; }
	inline bool* get_address_of_hasSecondColor_9() { return &___hasSecondColor_9; }
	inline void set_hasSecondColor_9(bool value)
	{
		___hasSecondColor_9 = value;
	}

	inline static int32_t get_offset_of_attachment_10() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___attachment_10)); }
	inline Attachment_t3043756552 * get_attachment_10() const { return ___attachment_10; }
	inline Attachment_t3043756552 ** get_address_of_attachment_10() { return &___attachment_10; }
	inline void set_attachment_10(Attachment_t3043756552 * value)
	{
		___attachment_10 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_10), value);
	}

	inline static int32_t get_offset_of_attachmentTime_11() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___attachmentTime_11)); }
	inline float get_attachmentTime_11() const { return ___attachmentTime_11; }
	inline float* get_address_of_attachmentTime_11() { return &___attachmentTime_11; }
	inline void set_attachmentTime_11(float value)
	{
		___attachmentTime_11 = value;
	}

	inline static int32_t get_offset_of_attachmentVertices_12() { return static_cast<int32_t>(offsetof(Slot_t3514940700, ___attachmentVertices_12)); }
	inline ExposedList_1_t4104378640 * get_attachmentVertices_12() const { return ___attachmentVertices_12; }
	inline ExposedList_1_t4104378640 ** get_address_of_attachmentVertices_12() { return &___attachmentVertices_12; }
	inline void set_attachmentVertices_12(ExposedList_1_t4104378640 * value)
	{
		___attachmentVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentVertices_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3514940700_H
#ifndef U3CHANDLESHOWRESULTU3EC__ANONSTOREY0_T2282433997_H
#define U3CHANDLESHOWRESULTU3EC__ANONSTOREY0_T2282433997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.RewardedAdButton/<HandleShowResult>c__AnonStorey0
struct  U3CHandleShowResultU3Ec__AnonStorey0_t2282433997  : public RuntimeObject
{
public:
	// System.Int32 GameVanilla.Game.UI.RewardedAdButton/<HandleShowResult>c__AnonStorey0::rewardCoins
	int32_t ___rewardCoins_0;

public:
	inline static int32_t get_offset_of_rewardCoins_0() { return static_cast<int32_t>(offsetof(U3CHandleShowResultU3Ec__AnonStorey0_t2282433997, ___rewardCoins_0)); }
	inline int32_t get_rewardCoins_0() const { return ___rewardCoins_0; }
	inline int32_t* get_address_of_rewardCoins_0() { return &___rewardCoins_0; }
	inline void set_rewardCoins_0(int32_t value)
	{
		___rewardCoins_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLESHOWRESULTU3EC__ANONSTOREY0_T2282433997_H
#ifndef JSON_T1494079119_H
#define JSON_T1494079119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Json
struct  Json_t1494079119  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T1494079119_H
#ifndef BONE_T1086356328_H
#define BONE_T1086356328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Bone
struct  Bone_t1086356328  : public RuntimeObject
{
public:
	// Spine.BoneData Spine.Bone::data
	BoneData_t3130174490 * ___data_1;
	// Spine.Skeleton Spine.Bone::skeleton
	Skeleton_t3686076450 * ___skeleton_2;
	// Spine.Bone Spine.Bone::parent
	Bone_t1086356328 * ___parent_3;
	// Spine.ExposedList`1<Spine.Bone> Spine.Bone::children
	ExposedList_1_t3793468194 * ___children_4;
	// System.Single Spine.Bone::x
	float ___x_5;
	// System.Single Spine.Bone::y
	float ___y_6;
	// System.Single Spine.Bone::rotation
	float ___rotation_7;
	// System.Single Spine.Bone::scaleX
	float ___scaleX_8;
	// System.Single Spine.Bone::scaleY
	float ___scaleY_9;
	// System.Single Spine.Bone::shearX
	float ___shearX_10;
	// System.Single Spine.Bone::shearY
	float ___shearY_11;
	// System.Single Spine.Bone::ax
	float ___ax_12;
	// System.Single Spine.Bone::ay
	float ___ay_13;
	// System.Single Spine.Bone::arotation
	float ___arotation_14;
	// System.Single Spine.Bone::ascaleX
	float ___ascaleX_15;
	// System.Single Spine.Bone::ascaleY
	float ___ascaleY_16;
	// System.Single Spine.Bone::ashearX
	float ___ashearX_17;
	// System.Single Spine.Bone::ashearY
	float ___ashearY_18;
	// System.Boolean Spine.Bone::appliedValid
	bool ___appliedValid_19;
	// System.Single Spine.Bone::a
	float ___a_20;
	// System.Single Spine.Bone::b
	float ___b_21;
	// System.Single Spine.Bone::worldX
	float ___worldX_22;
	// System.Single Spine.Bone::c
	float ___c_23;
	// System.Single Spine.Bone::d
	float ___d_24;
	// System.Single Spine.Bone::worldY
	float ___worldY_25;
	// System.Boolean Spine.Bone::sorted
	bool ___sorted_26;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___data_1)); }
	inline BoneData_t3130174490 * get_data_1() const { return ___data_1; }
	inline BoneData_t3130174490 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(BoneData_t3130174490 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_skeleton_2() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___skeleton_2)); }
	inline Skeleton_t3686076450 * get_skeleton_2() const { return ___skeleton_2; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_2() { return &___skeleton_2; }
	inline void set_skeleton_2(Skeleton_t3686076450 * value)
	{
		___skeleton_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___parent_3)); }
	inline Bone_t1086356328 * get_parent_3() const { return ___parent_3; }
	inline Bone_t1086356328 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Bone_t1086356328 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___children_4)); }
	inline ExposedList_1_t3793468194 * get_children_4() const { return ___children_4; }
	inline ExposedList_1_t3793468194 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(ExposedList_1_t3793468194 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_4), value);
	}

	inline static int32_t get_offset_of_x_5() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___x_5)); }
	inline float get_x_5() const { return ___x_5; }
	inline float* get_address_of_x_5() { return &___x_5; }
	inline void set_x_5(float value)
	{
		___x_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_scaleX_8() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___scaleX_8)); }
	inline float get_scaleX_8() const { return ___scaleX_8; }
	inline float* get_address_of_scaleX_8() { return &___scaleX_8; }
	inline void set_scaleX_8(float value)
	{
		___scaleX_8 = value;
	}

	inline static int32_t get_offset_of_scaleY_9() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___scaleY_9)); }
	inline float get_scaleY_9() const { return ___scaleY_9; }
	inline float* get_address_of_scaleY_9() { return &___scaleY_9; }
	inline void set_scaleY_9(float value)
	{
		___scaleY_9 = value;
	}

	inline static int32_t get_offset_of_shearX_10() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___shearX_10)); }
	inline float get_shearX_10() const { return ___shearX_10; }
	inline float* get_address_of_shearX_10() { return &___shearX_10; }
	inline void set_shearX_10(float value)
	{
		___shearX_10 = value;
	}

	inline static int32_t get_offset_of_shearY_11() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___shearY_11)); }
	inline float get_shearY_11() const { return ___shearY_11; }
	inline float* get_address_of_shearY_11() { return &___shearY_11; }
	inline void set_shearY_11(float value)
	{
		___shearY_11 = value;
	}

	inline static int32_t get_offset_of_ax_12() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ax_12)); }
	inline float get_ax_12() const { return ___ax_12; }
	inline float* get_address_of_ax_12() { return &___ax_12; }
	inline void set_ax_12(float value)
	{
		___ax_12 = value;
	}

	inline static int32_t get_offset_of_ay_13() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ay_13)); }
	inline float get_ay_13() const { return ___ay_13; }
	inline float* get_address_of_ay_13() { return &___ay_13; }
	inline void set_ay_13(float value)
	{
		___ay_13 = value;
	}

	inline static int32_t get_offset_of_arotation_14() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___arotation_14)); }
	inline float get_arotation_14() const { return ___arotation_14; }
	inline float* get_address_of_arotation_14() { return &___arotation_14; }
	inline void set_arotation_14(float value)
	{
		___arotation_14 = value;
	}

	inline static int32_t get_offset_of_ascaleX_15() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ascaleX_15)); }
	inline float get_ascaleX_15() const { return ___ascaleX_15; }
	inline float* get_address_of_ascaleX_15() { return &___ascaleX_15; }
	inline void set_ascaleX_15(float value)
	{
		___ascaleX_15 = value;
	}

	inline static int32_t get_offset_of_ascaleY_16() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ascaleY_16)); }
	inline float get_ascaleY_16() const { return ___ascaleY_16; }
	inline float* get_address_of_ascaleY_16() { return &___ascaleY_16; }
	inline void set_ascaleY_16(float value)
	{
		___ascaleY_16 = value;
	}

	inline static int32_t get_offset_of_ashearX_17() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ashearX_17)); }
	inline float get_ashearX_17() const { return ___ashearX_17; }
	inline float* get_address_of_ashearX_17() { return &___ashearX_17; }
	inline void set_ashearX_17(float value)
	{
		___ashearX_17 = value;
	}

	inline static int32_t get_offset_of_ashearY_18() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___ashearY_18)); }
	inline float get_ashearY_18() const { return ___ashearY_18; }
	inline float* get_address_of_ashearY_18() { return &___ashearY_18; }
	inline void set_ashearY_18(float value)
	{
		___ashearY_18 = value;
	}

	inline static int32_t get_offset_of_appliedValid_19() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___appliedValid_19)); }
	inline bool get_appliedValid_19() const { return ___appliedValid_19; }
	inline bool* get_address_of_appliedValid_19() { return &___appliedValid_19; }
	inline void set_appliedValid_19(bool value)
	{
		___appliedValid_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_b_21() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___b_21)); }
	inline float get_b_21() const { return ___b_21; }
	inline float* get_address_of_b_21() { return &___b_21; }
	inline void set_b_21(float value)
	{
		___b_21 = value;
	}

	inline static int32_t get_offset_of_worldX_22() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___worldX_22)); }
	inline float get_worldX_22() const { return ___worldX_22; }
	inline float* get_address_of_worldX_22() { return &___worldX_22; }
	inline void set_worldX_22(float value)
	{
		___worldX_22 = value;
	}

	inline static int32_t get_offset_of_c_23() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___c_23)); }
	inline float get_c_23() const { return ___c_23; }
	inline float* get_address_of_c_23() { return &___c_23; }
	inline void set_c_23(float value)
	{
		___c_23 = value;
	}

	inline static int32_t get_offset_of_d_24() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___d_24)); }
	inline float get_d_24() const { return ___d_24; }
	inline float* get_address_of_d_24() { return &___d_24; }
	inline void set_d_24(float value)
	{
		___d_24 = value;
	}

	inline static int32_t get_offset_of_worldY_25() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___worldY_25)); }
	inline float get_worldY_25() const { return ___worldY_25; }
	inline float* get_address_of_worldY_25() { return &___worldY_25; }
	inline void set_worldY_25(float value)
	{
		___worldY_25 = value;
	}

	inline static int32_t get_offset_of_sorted_26() { return static_cast<int32_t>(offsetof(Bone_t1086356328, ___sorted_26)); }
	inline bool get_sorted_26() const { return ___sorted_26; }
	inline bool* get_address_of_sorted_26() { return &___sorted_26; }
	inline void set_sorted_26(bool value)
	{
		___sorted_26 = value;
	}
};

struct Bone_t1086356328_StaticFields
{
public:
	// System.Boolean Spine.Bone::yDown
	bool ___yDown_0;

public:
	inline static int32_t get_offset_of_yDown_0() { return static_cast<int32_t>(offsetof(Bone_t1086356328_StaticFields, ___yDown_0)); }
	inline bool get_yDown_0() const { return ___yDown_0; }
	inline bool* get_address_of_yDown_0() { return &___yDown_0; }
	inline void set_yDown_0(bool value)
	{
		___yDown_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONE_T1086356328_H
#ifndef IKCONSTRAINTDATA_T459120129_H
#define IKCONSTRAINTDATA_T459120129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintData
struct  IkConstraintData_t459120129  : public RuntimeObject
{
public:
	// System.String Spine.IkConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.IkConstraintData::order
	int32_t ___order_1;
	// System.Collections.Generic.List`1<Spine.BoneData> Spine.IkConstraintData::bones
	List_1_t307281936 * ___bones_2;
	// Spine.BoneData Spine.IkConstraintData::target
	BoneData_t3130174490 * ___target_3;
	// System.Int32 Spine.IkConstraintData::bendDirection
	int32_t ___bendDirection_4;
	// System.Single Spine.IkConstraintData::mix
	float ___mix_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___bones_2)); }
	inline List_1_t307281936 * get_bones_2() const { return ___bones_2; }
	inline List_1_t307281936 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(List_1_t307281936 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___target_3)); }
	inline BoneData_t3130174490 * get_target_3() const { return ___target_3; }
	inline BoneData_t3130174490 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t3130174490 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}

	inline static int32_t get_offset_of_mix_5() { return static_cast<int32_t>(offsetof(IkConstraintData_t459120129, ___mix_5)); }
	inline float get_mix_5() const { return ___mix_5; }
	inline float* get_address_of_mix_5() { return &___mix_5; }
	inline void set_mix_5(float value)
	{
		___mix_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTDATA_T459120129_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef IKCONSTRAINT_T1675190269_H
#define IKCONSTRAINT_T1675190269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraint
struct  IkConstraint_t1675190269  : public RuntimeObject
{
public:
	// Spine.IkConstraintData Spine.IkConstraint::data
	IkConstraintData_t459120129 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.IkConstraint::bones
	ExposedList_1_t3793468194 * ___bones_1;
	// Spine.Bone Spine.IkConstraint::target
	Bone_t1086356328 * ___target_2;
	// System.Single Spine.IkConstraint::mix
	float ___mix_3;
	// System.Int32 Spine.IkConstraint::bendDirection
	int32_t ___bendDirection_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___data_0)); }
	inline IkConstraintData_t459120129 * get_data_0() const { return ___data_0; }
	inline IkConstraintData_t459120129 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IkConstraintData_t459120129 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___bones_1)); }
	inline ExposedList_1_t3793468194 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3793468194 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___target_2)); }
	inline Bone_t1086356328 * get_target_2() const { return ___target_2; }
	inline Bone_t1086356328 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t1086356328 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_mix_3() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___mix_3)); }
	inline float get_mix_3() const { return ___mix_3; }
	inline float* get_address_of_mix_3() { return &___mix_3; }
	inline void set_mix_3(float value)
	{
		___mix_3 = value;
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraint_t1675190269, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINT_T1675190269_H
#ifndef EVENT_T1378573841_H
#define EVENT_T1378573841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Event
struct  Event_t1378573841  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Event::data
	EventData_t724759987 * ___data_0;
	// System.Single Spine.Event::time
	float ___time_1;
	// System.Int32 Spine.Event::intValue
	int32_t ___intValue_2;
	// System.Single Spine.Event::floatValue
	float ___floatValue_3;
	// System.String Spine.Event::stringValue
	String_t* ___stringValue_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___data_0)); }
	inline EventData_t724759987 * get_data_0() const { return ___data_0; }
	inline EventData_t724759987 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(EventData_t724759987 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_intValue_2() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___intValue_2)); }
	inline int32_t get_intValue_2() const { return ___intValue_2; }
	inline int32_t* get_address_of_intValue_2() { return &___intValue_2; }
	inline void set_intValue_2(int32_t value)
	{
		___intValue_2 = value;
	}

	inline static int32_t get_offset_of_floatValue_3() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___floatValue_3)); }
	inline float get_floatValue_3() const { return ___floatValue_3; }
	inline float* get_address_of_floatValue_3() { return &___floatValue_3; }
	inline void set_floatValue_3(float value)
	{
		___floatValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(Event_t1378573841, ___stringValue_4)); }
	inline String_t* get_stringValue_4() const { return ___stringValue_4; }
	inline String_t** get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(String_t* value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENT_T1378573841_H
#ifndef ATTACHMENTKEYTUPLECOMPARER_T1167996044_H
#define ATTACHMENTKEYTUPLECOMPARER_T1167996044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTupleComparer
struct  AttachmentKeyTupleComparer_t1167996044  : public RuntimeObject
{
public:

public:
};

struct AttachmentKeyTupleComparer_t1167996044_StaticFields
{
public:
	// Spine.Skin/AttachmentKeyTupleComparer Spine.Skin/AttachmentKeyTupleComparer::Instance
	AttachmentKeyTupleComparer_t1167996044 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTupleComparer_t1167996044_StaticFields, ___Instance_0)); }
	inline AttachmentKeyTupleComparer_t1167996044 * get_Instance_0() const { return ___Instance_0; }
	inline AttachmentKeyTupleComparer_t1167996044 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AttachmentKeyTupleComparer_t1167996044 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTKEYTUPLECOMPARER_T1167996044_H
#ifndef SKELETONJSON_T4049771867_H
#define SKELETONJSON_T4049771867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson
struct  SkeletonJson_t4049771867  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonJson::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_0;
	// Spine.AttachmentLoader Spine.SkeletonJson::attachmentLoader
	RuntimeObject* ___attachmentLoader_1;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonJson::linkedMeshes
	List_1_t3168994201 * ___linkedMeshes_2;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SkeletonJson_t4049771867, ___U3CScaleU3Ek__BackingField_0)); }
	inline float get_U3CScaleU3Ek__BackingField_0() const { return ___U3CScaleU3Ek__BackingField_0; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_0() { return &___U3CScaleU3Ek__BackingField_0; }
	inline void set_U3CScaleU3Ek__BackingField_0(float value)
	{
		___U3CScaleU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_1() { return static_cast<int32_t>(offsetof(SkeletonJson_t4049771867, ___attachmentLoader_1)); }
	inline RuntimeObject* get_attachmentLoader_1() const { return ___attachmentLoader_1; }
	inline RuntimeObject** get_address_of_attachmentLoader_1() { return &___attachmentLoader_1; }
	inline void set_attachmentLoader_1(RuntimeObject* value)
	{
		___attachmentLoader_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_1), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_2() { return static_cast<int32_t>(offsetof(SkeletonJson_t4049771867, ___linkedMeshes_2)); }
	inline List_1_t3168994201 * get_linkedMeshes_2() const { return ___linkedMeshes_2; }
	inline List_1_t3168994201 ** get_address_of_linkedMeshes_2() { return &___linkedMeshes_2; }
	inline void set_linkedMeshes_2(List_1_t3168994201 * value)
	{
		___linkedMeshes_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONJSON_T4049771867_H
#ifndef JSONDECODER_T2143190644_H
#define JSONDECODER_T2143190644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.JsonDecoder
struct  JsonDecoder_t2143190644  : public RuntimeObject
{
public:
	// System.String SharpJson.JsonDecoder::<errorMessage>k__BackingField
	String_t* ___U3CerrorMessageU3Ek__BackingField_0;
	// System.Boolean SharpJson.JsonDecoder::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// SharpJson.Lexer SharpJson.JsonDecoder::lexer
	Lexer_t3897031801 * ___lexer_2;

public:
	inline static int32_t get_offset_of_U3CerrorMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonDecoder_t2143190644, ___U3CerrorMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CerrorMessageU3Ek__BackingField_0() const { return ___U3CerrorMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CerrorMessageU3Ek__BackingField_0() { return &___U3CerrorMessageU3Ek__BackingField_0; }
	inline void set_U3CerrorMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CerrorMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonDecoder_t2143190644, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_lexer_2() { return static_cast<int32_t>(offsetof(JsonDecoder_t2143190644, ___lexer_2)); }
	inline Lexer_t3897031801 * get_lexer_2() const { return ___lexer_2; }
	inline Lexer_t3897031801 ** get_address_of_lexer_2() { return &___lexer_2; }
	inline void set_lexer_2(Lexer_t3897031801 * value)
	{
		___lexer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDECODER_T2143190644_H
#ifndef LEXER_T3897031801_H
#define LEXER_T3897031801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer
struct  Lexer_t3897031801  : public RuntimeObject
{
public:
	// System.Int32 SharpJson.Lexer::<lineNumber>k__BackingField
	int32_t ___U3ClineNumberU3Ek__BackingField_0;
	// System.Boolean SharpJson.Lexer::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// System.Char[] SharpJson.Lexer::json
	CharU5BU5D_t3528271667* ___json_2;
	// System.Int32 SharpJson.Lexer::index
	int32_t ___index_3;
	// System.Boolean SharpJson.Lexer::success
	bool ___success_4;
	// System.Char[] SharpJson.Lexer::stringBuffer
	CharU5BU5D_t3528271667* ___stringBuffer_5;

public:
	inline static int32_t get_offset_of_U3ClineNumberU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___U3ClineNumberU3Ek__BackingField_0)); }
	inline int32_t get_U3ClineNumberU3Ek__BackingField_0() const { return ___U3ClineNumberU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ClineNumberU3Ek__BackingField_0() { return &___U3ClineNumberU3Ek__BackingField_0; }
	inline void set_U3ClineNumberU3Ek__BackingField_0(int32_t value)
	{
		___U3ClineNumberU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___json_2)); }
	inline CharU5BU5D_t3528271667* get_json_2() const { return ___json_2; }
	inline CharU5BU5D_t3528271667** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(CharU5BU5D_t3528271667* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier((&___json_2), value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_stringBuffer_5() { return static_cast<int32_t>(offsetof(Lexer_t3897031801, ___stringBuffer_5)); }
	inline CharU5BU5D_t3528271667* get_stringBuffer_5() const { return ___stringBuffer_5; }
	inline CharU5BU5D_t3528271667** get_address_of_stringBuffer_5() { return &___stringBuffer_5; }
	inline void set_stringBuffer_5(CharU5BU5D_t3528271667* value)
	{
		___stringBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T3897031801_H
#ifndef CURVETIMELINE_T3673209699_H
#define CURVETIMELINE_T3673209699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.CurveTimeline
struct  CurveTimeline_t3673209699  : public RuntimeObject
{
public:
	// System.Single[] Spine.CurveTimeline::curves
	SingleU5BU5D_t1444911251* ___curves_4;

public:
	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(CurveTimeline_t3673209699, ___curves_4)); }
	inline SingleU5BU5D_t1444911251* get_curves_4() const { return ___curves_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(SingleU5BU5D_t1444911251* value)
	{
		___curves_4 = value;
		Il2CppCodeGenWriteBarrier((&___curves_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVETIMELINE_T3673209699_H
#ifndef LINKEDMESH_T1696919459_H
#define LINKEDMESH_T1696919459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson/LinkedMesh
struct  LinkedMesh_t1696919459  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonJson/LinkedMesh::parent
	String_t* ___parent_0;
	// System.String Spine.SkeletonJson/LinkedMesh::skin
	String_t* ___skin_1;
	// System.Int32 Spine.SkeletonJson/LinkedMesh::slotIndex
	int32_t ___slotIndex_2;
	// Spine.MeshAttachment Spine.SkeletonJson/LinkedMesh::mesh
	MeshAttachment_t1975337962 * ___mesh_3;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___parent_0)); }
	inline String_t* get_parent_0() const { return ___parent_0; }
	inline String_t** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(String_t* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_skin_1() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___skin_1)); }
	inline String_t* get_skin_1() const { return ___skin_1; }
	inline String_t** get_address_of_skin_1() { return &___skin_1; }
	inline void set_skin_1(String_t* value)
	{
		___skin_1 = value;
		Il2CppCodeGenWriteBarrier((&___skin_1), value);
	}

	inline static int32_t get_offset_of_slotIndex_2() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___slotIndex_2)); }
	inline int32_t get_slotIndex_2() const { return ___slotIndex_2; }
	inline int32_t* get_address_of_slotIndex_2() { return &___slotIndex_2; }
	inline void set_slotIndex_2(int32_t value)
	{
		___slotIndex_2 = value;
	}

	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(LinkedMesh_t1696919459, ___mesh_3)); }
	inline MeshAttachment_t1975337962 * get_mesh_3() const { return ___mesh_3; }
	inline MeshAttachment_t1975337962 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(MeshAttachment_t1975337962 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDMESH_T1696919459_H
#ifndef SKIN_T1174584606_H
#define SKIN_T1174584606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin
struct  Skin_t1174584606  : public RuntimeObject
{
public:
	// System.String Spine.Skin::name
	String_t* ___name_0;
	// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment> Spine.Skin::attachments
	Dictionary_2_t968660385 * ___attachments_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Skin_t1174584606, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_attachments_1() { return static_cast<int32_t>(offsetof(Skin_t1174584606, ___attachments_1)); }
	inline Dictionary_2_t968660385 * get_attachments_1() const { return ___attachments_1; }
	inline Dictionary_2_t968660385 ** get_address_of_attachments_1() { return &___attachments_1; }
	inline void set_attachments_1(Dictionary_2_t968660385 * value)
	{
		___attachments_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_T1174584606_H
#ifndef ANIMATION_T615783283_H
#define ANIMATION_T615783283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Animation
struct  Animation_t615783283  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Timeline> Spine.Animation::timelines
	ExposedList_1_t383950901 * ___timelines_0;
	// System.Single Spine.Animation::duration
	float ___duration_1;
	// System.String Spine.Animation::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_timelines_0() { return static_cast<int32_t>(offsetof(Animation_t615783283, ___timelines_0)); }
	inline ExposedList_1_t383950901 * get_timelines_0() const { return ___timelines_0; }
	inline ExposedList_1_t383950901 ** get_address_of_timelines_0() { return &___timelines_0; }
	inline void set_timelines_0(ExposedList_1_t383950901 * value)
	{
		___timelines_0 = value;
		Il2CppCodeGenWriteBarrier((&___timelines_0), value);
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(Animation_t615783283, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Animation_t615783283, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T615783283_H
#ifndef ANIMATIONPAIRCOMPARER_T175378255_H
#define ANIMATIONPAIRCOMPARER_T175378255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPairComparer
struct  AnimationPairComparer_t175378255  : public RuntimeObject
{
public:

public:
};

struct AnimationPairComparer_t175378255_StaticFields
{
public:
	// Spine.AnimationStateData/AnimationPairComparer Spine.AnimationStateData/AnimationPairComparer::Instance
	AnimationPairComparer_t175378255 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationPairComparer_t175378255_StaticFields, ___Instance_0)); }
	inline AnimationPairComparer_t175378255 * get_Instance_0() const { return ___Instance_0; }
	inline AnimationPairComparer_t175378255 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AnimationPairComparer_t175378255 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPAIRCOMPARER_T175378255_H
#ifndef POINTATTACHMENT_T2275020146_H
#define POINTATTACHMENT_T2275020146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PointAttachment
struct  PointAttachment_t2275020146  : public Attachment_t3043756552
{
public:
	// System.Single Spine.PointAttachment::x
	float ___x_1;
	// System.Single Spine.PointAttachment::y
	float ___y_2;
	// System.Single Spine.PointAttachment::rotation
	float ___rotation_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(PointAttachment_t2275020146, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(PointAttachment_t2275020146, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(PointAttachment_t2275020146, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTATTACHMENT_T2275020146_H
#ifndef VERTEXATTACHMENT_T4074366829_H
#define VERTEXATTACHMENT_T4074366829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.VertexAttachment
struct  VertexAttachment_t4074366829  : public Attachment_t3043756552
{
public:
	// System.Int32 Spine.VertexAttachment::id
	int32_t ___id_3;
	// System.Int32[] Spine.VertexAttachment::bones
	Int32U5BU5D_t385246372* ___bones_4;
	// System.Single[] Spine.VertexAttachment::vertices
	SingleU5BU5D_t1444911251* ___vertices_5;
	// System.Int32 Spine.VertexAttachment::worldVerticesLength
	int32_t ___worldVerticesLength_6;

public:
	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_bones_4() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___bones_4)); }
	inline Int32U5BU5D_t385246372* get_bones_4() const { return ___bones_4; }
	inline Int32U5BU5D_t385246372** get_address_of_bones_4() { return &___bones_4; }
	inline void set_bones_4(Int32U5BU5D_t385246372* value)
	{
		___bones_4 = value;
		Il2CppCodeGenWriteBarrier((&___bones_4), value);
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___vertices_5)); }
	inline SingleU5BU5D_t1444911251* get_vertices_5() const { return ___vertices_5; }
	inline SingleU5BU5D_t1444911251** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(SingleU5BU5D_t1444911251* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_worldVerticesLength_6() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829, ___worldVerticesLength_6)); }
	inline int32_t get_worldVerticesLength_6() const { return ___worldVerticesLength_6; }
	inline int32_t* get_address_of_worldVerticesLength_6() { return &___worldVerticesLength_6; }
	inline void set_worldVerticesLength_6(int32_t value)
	{
		___worldVerticesLength_6 = value;
	}
};

struct VertexAttachment_t4074366829_StaticFields
{
public:
	// System.Int32 Spine.VertexAttachment::nextID
	int32_t ___nextID_1;
	// System.Object Spine.VertexAttachment::nextIdLock
	RuntimeObject * ___nextIdLock_2;

public:
	inline static int32_t get_offset_of_nextID_1() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829_StaticFields, ___nextID_1)); }
	inline int32_t get_nextID_1() const { return ___nextID_1; }
	inline int32_t* get_address_of_nextID_1() { return &___nextID_1; }
	inline void set_nextID_1(int32_t value)
	{
		___nextID_1 = value;
	}

	inline static int32_t get_offset_of_nextIdLock_2() { return static_cast<int32_t>(offsetof(VertexAttachment_t4074366829_StaticFields, ___nextIdLock_2)); }
	inline RuntimeObject * get_nextIdLock_2() const { return ___nextIdLock_2; }
	inline RuntimeObject ** get_address_of_nextIdLock_2() { return &___nextIdLock_2; }
	inline void set_nextIdLock_2(RuntimeObject * value)
	{
		___nextIdLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextIdLock_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXATTACHMENT_T4074366829_H
#ifndef REGIONATTACHMENT_T1770147391_H
#define REGIONATTACHMENT_T1770147391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RegionAttachment
struct  RegionAttachment_t1770147391  : public Attachment_t3043756552
{
public:
	// System.Single Spine.RegionAttachment::x
	float ___x_9;
	// System.Single Spine.RegionAttachment::y
	float ___y_10;
	// System.Single Spine.RegionAttachment::rotation
	float ___rotation_11;
	// System.Single Spine.RegionAttachment::scaleX
	float ___scaleX_12;
	// System.Single Spine.RegionAttachment::scaleY
	float ___scaleY_13;
	// System.Single Spine.RegionAttachment::width
	float ___width_14;
	// System.Single Spine.RegionAttachment::height
	float ___height_15;
	// System.Single Spine.RegionAttachment::regionOffsetX
	float ___regionOffsetX_16;
	// System.Single Spine.RegionAttachment::regionOffsetY
	float ___regionOffsetY_17;
	// System.Single Spine.RegionAttachment::regionWidth
	float ___regionWidth_18;
	// System.Single Spine.RegionAttachment::regionHeight
	float ___regionHeight_19;
	// System.Single Spine.RegionAttachment::regionOriginalWidth
	float ___regionOriginalWidth_20;
	// System.Single Spine.RegionAttachment::regionOriginalHeight
	float ___regionOriginalHeight_21;
	// System.Single[] Spine.RegionAttachment::offset
	SingleU5BU5D_t1444911251* ___offset_22;
	// System.Single[] Spine.RegionAttachment::uvs
	SingleU5BU5D_t1444911251* ___uvs_23;
	// System.Single Spine.RegionAttachment::r
	float ___r_24;
	// System.Single Spine.RegionAttachment::g
	float ___g_25;
	// System.Single Spine.RegionAttachment::b
	float ___b_26;
	// System.Single Spine.RegionAttachment::a
	float ___a_27;
	// System.String Spine.RegionAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_28;
	// System.Object Spine.RegionAttachment::<RendererObject>k__BackingField
	RuntimeObject * ___U3CRendererObjectU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_x_9() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___x_9)); }
	inline float get_x_9() const { return ___x_9; }
	inline float* get_address_of_x_9() { return &___x_9; }
	inline void set_x_9(float value)
	{
		___x_9 = value;
	}

	inline static int32_t get_offset_of_y_10() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___y_10)); }
	inline float get_y_10() const { return ___y_10; }
	inline float* get_address_of_y_10() { return &___y_10; }
	inline void set_y_10(float value)
	{
		___y_10 = value;
	}

	inline static int32_t get_offset_of_rotation_11() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___rotation_11)); }
	inline float get_rotation_11() const { return ___rotation_11; }
	inline float* get_address_of_rotation_11() { return &___rotation_11; }
	inline void set_rotation_11(float value)
	{
		___rotation_11 = value;
	}

	inline static int32_t get_offset_of_scaleX_12() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___scaleX_12)); }
	inline float get_scaleX_12() const { return ___scaleX_12; }
	inline float* get_address_of_scaleX_12() { return &___scaleX_12; }
	inline void set_scaleX_12(float value)
	{
		___scaleX_12 = value;
	}

	inline static int32_t get_offset_of_scaleY_13() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___scaleY_13)); }
	inline float get_scaleY_13() const { return ___scaleY_13; }
	inline float* get_address_of_scaleY_13() { return &___scaleY_13; }
	inline void set_scaleY_13(float value)
	{
		___scaleY_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___height_15)); }
	inline float get_height_15() const { return ___height_15; }
	inline float* get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(float value)
	{
		___height_15 = value;
	}

	inline static int32_t get_offset_of_regionOffsetX_16() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOffsetX_16)); }
	inline float get_regionOffsetX_16() const { return ___regionOffsetX_16; }
	inline float* get_address_of_regionOffsetX_16() { return &___regionOffsetX_16; }
	inline void set_regionOffsetX_16(float value)
	{
		___regionOffsetX_16 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_17() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOffsetY_17)); }
	inline float get_regionOffsetY_17() const { return ___regionOffsetY_17; }
	inline float* get_address_of_regionOffsetY_17() { return &___regionOffsetY_17; }
	inline void set_regionOffsetY_17(float value)
	{
		___regionOffsetY_17 = value;
	}

	inline static int32_t get_offset_of_regionWidth_18() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionWidth_18)); }
	inline float get_regionWidth_18() const { return ___regionWidth_18; }
	inline float* get_address_of_regionWidth_18() { return &___regionWidth_18; }
	inline void set_regionWidth_18(float value)
	{
		___regionWidth_18 = value;
	}

	inline static int32_t get_offset_of_regionHeight_19() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionHeight_19)); }
	inline float get_regionHeight_19() const { return ___regionHeight_19; }
	inline float* get_address_of_regionHeight_19() { return &___regionHeight_19; }
	inline void set_regionHeight_19(float value)
	{
		___regionHeight_19 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_20() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOriginalWidth_20)); }
	inline float get_regionOriginalWidth_20() const { return ___regionOriginalWidth_20; }
	inline float* get_address_of_regionOriginalWidth_20() { return &___regionOriginalWidth_20; }
	inline void set_regionOriginalWidth_20(float value)
	{
		___regionOriginalWidth_20 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_21() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___regionOriginalHeight_21)); }
	inline float get_regionOriginalHeight_21() const { return ___regionOriginalHeight_21; }
	inline float* get_address_of_regionOriginalHeight_21() { return &___regionOriginalHeight_21; }
	inline void set_regionOriginalHeight_21(float value)
	{
		___regionOriginalHeight_21 = value;
	}

	inline static int32_t get_offset_of_offset_22() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___offset_22)); }
	inline SingleU5BU5D_t1444911251* get_offset_22() const { return ___offset_22; }
	inline SingleU5BU5D_t1444911251** get_address_of_offset_22() { return &___offset_22; }
	inline void set_offset_22(SingleU5BU5D_t1444911251* value)
	{
		___offset_22 = value;
		Il2CppCodeGenWriteBarrier((&___offset_22), value);
	}

	inline static int32_t get_offset_of_uvs_23() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___uvs_23)); }
	inline SingleU5BU5D_t1444911251* get_uvs_23() const { return ___uvs_23; }
	inline SingleU5BU5D_t1444911251** get_address_of_uvs_23() { return &___uvs_23; }
	inline void set_uvs_23(SingleU5BU5D_t1444911251* value)
	{
		___uvs_23 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_23), value);
	}

	inline static int32_t get_offset_of_r_24() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___r_24)); }
	inline float get_r_24() const { return ___r_24; }
	inline float* get_address_of_r_24() { return &___r_24; }
	inline void set_r_24(float value)
	{
		___r_24 = value;
	}

	inline static int32_t get_offset_of_g_25() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___g_25)); }
	inline float get_g_25() const { return ___g_25; }
	inline float* get_address_of_g_25() { return &___g_25; }
	inline void set_g_25(float value)
	{
		___g_25 = value;
	}

	inline static int32_t get_offset_of_b_26() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___b_26)); }
	inline float get_b_26() const { return ___b_26; }
	inline float* get_address_of_b_26() { return &___b_26; }
	inline void set_b_26(float value)
	{
		___b_26 = value;
	}

	inline static int32_t get_offset_of_a_27() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___a_27)); }
	inline float get_a_27() const { return ___a_27; }
	inline float* get_address_of_a_27() { return &___a_27; }
	inline void set_a_27(float value)
	{
		___a_27 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___U3CPathU3Ek__BackingField_28)); }
	inline String_t* get_U3CPathU3Ek__BackingField_28() const { return ___U3CPathU3Ek__BackingField_28; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_28() { return &___U3CPathU3Ek__BackingField_28; }
	inline void set_U3CPathU3Ek__BackingField_28(String_t* value)
	{
		___U3CPathU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CRendererObjectU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(RegionAttachment_t1770147391, ___U3CRendererObjectU3Ek__BackingField_29)); }
	inline RuntimeObject * get_U3CRendererObjectU3Ek__BackingField_29() const { return ___U3CRendererObjectU3Ek__BackingField_29; }
	inline RuntimeObject ** get_address_of_U3CRendererObjectU3Ek__BackingField_29() { return &___U3CRendererObjectU3Ek__BackingField_29; }
	inline void set_U3CRendererObjectU3Ek__BackingField_29(RuntimeObject * value)
	{
		___U3CRendererObjectU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRendererObjectU3Ek__BackingField_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONATTACHMENT_T1770147391_H
#ifndef TRANSLATETIMELINE_T2733426737_H
#define TRANSLATETIMELINE_T2733426737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TranslateTimeline
struct  TranslateTimeline_t2733426737  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.TranslateTimeline::boneIndex
	int32_t ___boneIndex_11;
	// System.Single[] Spine.TranslateTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_12;

public:
	inline static int32_t get_offset_of_boneIndex_11() { return static_cast<int32_t>(offsetof(TranslateTimeline_t2733426737, ___boneIndex_11)); }
	inline int32_t get_boneIndex_11() const { return ___boneIndex_11; }
	inline int32_t* get_address_of_boneIndex_11() { return &___boneIndex_11; }
	inline void set_boneIndex_11(int32_t value)
	{
		___boneIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(TranslateTimeline_t2733426737, ___frames_12)); }
	inline SingleU5BU5D_t1444911251* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t1444911251* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATETIMELINE_T2733426737_H
#ifndef ROTATETIMELINE_T860518009_H
#define ROTATETIMELINE_T860518009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateTimeline
struct  RotateTimeline_t860518009  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.RotateTimeline::boneIndex
	int32_t ___boneIndex_9;
	// System.Single[] Spine.RotateTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_10;

public:
	inline static int32_t get_offset_of_boneIndex_9() { return static_cast<int32_t>(offsetof(RotateTimeline_t860518009, ___boneIndex_9)); }
	inline int32_t get_boneIndex_9() const { return ___boneIndex_9; }
	inline int32_t* get_address_of_boneIndex_9() { return &___boneIndex_9; }
	inline void set_boneIndex_9(int32_t value)
	{
		___boneIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(RotateTimeline_t860518009, ___frames_10)); }
	inline SingleU5BU5D_t1444911251* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_t1444911251* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATETIMELINE_T860518009_H
#ifndef ATTACHMENTKEYTUPLE_T1548106539_H
#define ATTACHMENTKEYTUPLE_T1548106539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTuple
struct  AttachmentKeyTuple_t1548106539 
{
public:
	// System.Int32 Spine.Skin/AttachmentKeyTuple::slotIndex
	int32_t ___slotIndex_0;
	// System.String Spine.Skin/AttachmentKeyTuple::name
	String_t* ___name_1;
	// System.Int32 Spine.Skin/AttachmentKeyTuple::nameHashCode
	int32_t ___nameHashCode_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t1548106539, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t1548106539, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_nameHashCode_2() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t1548106539, ___nameHashCode_2)); }
	inline int32_t get_nameHashCode_2() const { return ___nameHashCode_2; }
	inline int32_t* get_address_of_nameHashCode_2() { return &___nameHashCode_2; }
	inline void set_nameHashCode_2(int32_t value)
	{
		___nameHashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_t1548106539_marshaled_pinvoke
{
	int32_t ___slotIndex_0;
	char* ___name_1;
	int32_t ___nameHashCode_2;
};
// Native definition for COM marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_t1548106539_marshaled_com
{
	int32_t ___slotIndex_0;
	Il2CppChar* ___name_1;
	int32_t ___nameHashCode_2;
};
#endif // ATTACHMENTKEYTUPLE_T1548106539_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLORTIMELINE_T3754116780_H
#define COLORTIMELINE_T3754116780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ColorTimeline
struct  ColorTimeline_t3754116780  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.ColorTimeline::slotIndex
	int32_t ___slotIndex_15;
	// System.Single[] Spine.ColorTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_16;

public:
	inline static int32_t get_offset_of_slotIndex_15() { return static_cast<int32_t>(offsetof(ColorTimeline_t3754116780, ___slotIndex_15)); }
	inline int32_t get_slotIndex_15() const { return ___slotIndex_15; }
	inline int32_t* get_address_of_slotIndex_15() { return &___slotIndex_15; }
	inline void set_slotIndex_15(int32_t value)
	{
		___slotIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(ColorTimeline_t3754116780, ___frames_16)); }
	inline SingleU5BU5D_t1444911251* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_t1444911251* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTIMELINE_T3754116780_H
#ifndef PATHCONSTRAINTMIXTIMELINE_T534890125_H
#define PATHCONSTRAINTMIXTIMELINE_T534890125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintMixTimeline
struct  PathConstraintMixTimeline_t534890125  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.PathConstraintMixTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_11;
	// System.Single[] Spine.PathConstraintMixTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_12;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_11() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t534890125, ___pathConstraintIndex_11)); }
	inline int32_t get_pathConstraintIndex_11() const { return ___pathConstraintIndex_11; }
	inline int32_t* get_address_of_pathConstraintIndex_11() { return &___pathConstraintIndex_11; }
	inline void set_pathConstraintIndex_11(int32_t value)
	{
		___pathConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t534890125, ___frames_12)); }
	inline SingleU5BU5D_t1444911251* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t1444911251* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTMIXTIMELINE_T534890125_H
#ifndef TRANSFORMCONSTRAINTTIMELINE_T410042030_H
#define TRANSFORMCONSTRAINTTIMELINE_T410042030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintTimeline
struct  TransformConstraintTimeline_t410042030  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.TransformConstraintTimeline::transformConstraintIndex
	int32_t ___transformConstraintIndex_15;
	// System.Single[] Spine.TransformConstraintTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_16;

public:
	inline static int32_t get_offset_of_transformConstraintIndex_15() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t410042030, ___transformConstraintIndex_15)); }
	inline int32_t get_transformConstraintIndex_15() const { return ___transformConstraintIndex_15; }
	inline int32_t* get_address_of_transformConstraintIndex_15() { return &___transformConstraintIndex_15; }
	inline void set_transformConstraintIndex_15(int32_t value)
	{
		___transformConstraintIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t410042030, ___frames_16)); }
	inline SingleU5BU5D_t1444911251* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_t1444911251* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTTIMELINE_T410042030_H
#ifndef IKCONSTRAINTTIMELINE_T1094182104_H
#define IKCONSTRAINTTIMELINE_T1094182104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintTimeline
struct  IkConstraintTimeline_t1094182104  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.IkConstraintTimeline::ikConstraintIndex
	int32_t ___ikConstraintIndex_11;
	// System.Single[] Spine.IkConstraintTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_12;

public:
	inline static int32_t get_offset_of_ikConstraintIndex_11() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t1094182104, ___ikConstraintIndex_11)); }
	inline int32_t get_ikConstraintIndex_11() const { return ___ikConstraintIndex_11; }
	inline int32_t* get_address_of_ikConstraintIndex_11() { return &___ikConstraintIndex_11; }
	inline void set_ikConstraintIndex_11(int32_t value)
	{
		___ikConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t1094182104, ___frames_12)); }
	inline SingleU5BU5D_t1444911251* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t1444911251* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTTIMELINE_T1094182104_H
#ifndef ANIMATIONPAIR_T1784808777_H
#define ANIMATIONPAIR_T1784808777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPair
struct  AnimationPair_t1784808777 
{
public:
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a1
	Animation_t615783283 * ___a1_0;
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a2
	Animation_t615783283 * ___a2_1;

public:
	inline static int32_t get_offset_of_a1_0() { return static_cast<int32_t>(offsetof(AnimationPair_t1784808777, ___a1_0)); }
	inline Animation_t615783283 * get_a1_0() const { return ___a1_0; }
	inline Animation_t615783283 ** get_address_of_a1_0() { return &___a1_0; }
	inline void set_a1_0(Animation_t615783283 * value)
	{
		___a1_0 = value;
		Il2CppCodeGenWriteBarrier((&___a1_0), value);
	}

	inline static int32_t get_offset_of_a2_1() { return static_cast<int32_t>(offsetof(AnimationPair_t1784808777, ___a2_1)); }
	inline Animation_t615783283 * get_a2_1() const { return ___a2_1; }
	inline Animation_t615783283 ** get_address_of_a2_1() { return &___a2_1; }
	inline void set_a2_1(Animation_t615783283 * value)
	{
		___a2_1 = value;
		Il2CppCodeGenWriteBarrier((&___a2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_t1784808777_marshaled_pinvoke
{
	Animation_t615783283 * ___a1_0;
	Animation_t615783283 * ___a2_1;
};
// Native definition for COM marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_t1784808777_marshaled_com
{
	Animation_t615783283 * ___a1_0;
	Animation_t615783283 * ___a2_1;
};
#endif // ANIMATIONPAIR_T1784808777_H
#ifndef DEFORMTIMELINE_T3552075840_H
#define DEFORMTIMELINE_T3552075840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DeformTimeline
struct  DeformTimeline_t3552075840  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.DeformTimeline::slotIndex
	int32_t ___slotIndex_5;
	// System.Single[] Spine.DeformTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_6;
	// System.Single[][] Spine.DeformTimeline::frameVertices
	SingleU5BU5DU5BU5D_t3206712258* ___frameVertices_7;
	// Spine.VertexAttachment Spine.DeformTimeline::attachment
	VertexAttachment_t4074366829 * ___attachment_8;

public:
	inline static int32_t get_offset_of_slotIndex_5() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___slotIndex_5)); }
	inline int32_t get_slotIndex_5() const { return ___slotIndex_5; }
	inline int32_t* get_address_of_slotIndex_5() { return &___slotIndex_5; }
	inline void set_slotIndex_5(int32_t value)
	{
		___slotIndex_5 = value;
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___frames_6)); }
	inline SingleU5BU5D_t1444911251* get_frames_6() const { return ___frames_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(SingleU5BU5D_t1444911251* value)
	{
		___frames_6 = value;
		Il2CppCodeGenWriteBarrier((&___frames_6), value);
	}

	inline static int32_t get_offset_of_frameVertices_7() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___frameVertices_7)); }
	inline SingleU5BU5DU5BU5D_t3206712258* get_frameVertices_7() const { return ___frameVertices_7; }
	inline SingleU5BU5DU5BU5D_t3206712258** get_address_of_frameVertices_7() { return &___frameVertices_7; }
	inline void set_frameVertices_7(SingleU5BU5DU5BU5D_t3206712258* value)
	{
		___frameVertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameVertices_7), value);
	}

	inline static int32_t get_offset_of_attachment_8() { return static_cast<int32_t>(offsetof(DeformTimeline_t3552075840, ___attachment_8)); }
	inline VertexAttachment_t4074366829 * get_attachment_8() const { return ___attachment_8; }
	inline VertexAttachment_t4074366829 ** get_address_of_attachment_8() { return &___attachment_8; }
	inline void set_attachment_8(VertexAttachment_t4074366829 * value)
	{
		___attachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFORMTIMELINE_T3552075840_H
#ifndef TWOCOLORTIMELINE_T1834727230_H
#define TWOCOLORTIMELINE_T1834727230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TwoColorTimeline
struct  TwoColorTimeline_t1834727230  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.TwoColorTimeline::slotIndex
	int32_t ___slotIndex_21;
	// System.Single[] Spine.TwoColorTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_22;

public:
	inline static int32_t get_offset_of_slotIndex_21() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t1834727230, ___slotIndex_21)); }
	inline int32_t get_slotIndex_21() const { return ___slotIndex_21; }
	inline int32_t* get_address_of_slotIndex_21() { return &___slotIndex_21; }
	inline void set_slotIndex_21(int32_t value)
	{
		___slotIndex_21 = value;
	}

	inline static int32_t get_offset_of_frames_22() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t1834727230, ___frames_22)); }
	inline SingleU5BU5D_t1444911251* get_frames_22() const { return ___frames_22; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_22() { return &___frames_22; }
	inline void set_frames_22(SingleU5BU5D_t1444911251* value)
	{
		___frames_22 = value;
		Il2CppCodeGenWriteBarrier((&___frames_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOCOLORTIMELINE_T1834727230_H
#ifndef PATHCONSTRAINTPOSITIONTIMELINE_T1963796833_H
#define PATHCONSTRAINTPOSITIONTIMELINE_T1963796833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintPositionTimeline
struct  PathConstraintPositionTimeline_t1963796833  : public CurveTimeline_t3673209699
{
public:
	// System.Int32 Spine.PathConstraintPositionTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_9;
	// System.Single[] Spine.PathConstraintPositionTimeline::frames
	SingleU5BU5D_t1444911251* ___frames_10;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_9() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t1963796833, ___pathConstraintIndex_9)); }
	inline int32_t get_pathConstraintIndex_9() const { return ___pathConstraintIndex_9; }
	inline int32_t* get_address_of_pathConstraintIndex_9() { return &___pathConstraintIndex_9; }
	inline void set_pathConstraintIndex_9(int32_t value)
	{
		___pathConstraintIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t1963796833, ___frames_10)); }
	inline SingleU5BU5D_t1444911251* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_t1444911251* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTPOSITIONTIMELINE_T1963796833_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ROTATEMODE_T2880286220_H
#define ROTATEMODE_T2880286220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateMode
struct  RotateMode_t2880286220 
{
public:
	// System.Int32 Spine.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2880286220, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2880286220_H
#ifndef SPACINGMODE_T3020059698_H
#define SPACINGMODE_T3020059698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SpacingMode
struct  SpacingMode_t3020059698 
{
public:
	// System.Int32 Spine.SpacingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpacingMode_t3020059698, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACINGMODE_T3020059698_H
#ifndef POSITIONMODE_T2435325583_H
#define POSITIONMODE_T2435325583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PositionMode
struct  PositionMode_t2435325583 
{
public:
	// System.Int32 Spine.PositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PositionMode_t2435325583, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONMODE_T2435325583_H
#ifndef TOKEN_T2243639121_H
#define TOKEN_T2243639121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer/Token
struct  Token_t2243639121 
{
public:
	// System.Int32 SharpJson.Lexer/Token::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Token_t2243639121, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2243639121_H
#ifndef ATTACHMENTTYPE_T1460157544_H
#define ATTACHMENTTYPE_T1460157544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentType
struct  AttachmentType_t1460157544 
{
public:
	// System.Int32 Spine.AttachmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttachmentType_t1460157544, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTYPE_T1460157544_H
#ifndef FORMAT_T2262486444_H
#define FORMAT_T2262486444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Format
struct  Format_t2262486444 
{
public:
	// System.Int32 Spine.Format::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Format_t2262486444, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMAT_T2262486444_H
#ifndef EVENTTYPE_T1835192406_H
#define EVENTTYPE_T1835192406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventType
struct  EventType_t1835192406 
{
public:
	// System.Int32 Spine.EventQueue/EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t1835192406, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T1835192406_H
#ifndef PATHCONSTRAINTSPACINGTIMELINE_T1125597164_H
#define PATHCONSTRAINTSPACINGTIMELINE_T1125597164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintSpacingTimeline
struct  PathConstraintSpacingTimeline_t1125597164  : public PathConstraintPositionTimeline_t1963796833
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTSPACINGTIMELINE_T1125597164_H
#ifndef SHEARTIMELINE_T3812859450_H
#define SHEARTIMELINE_T3812859450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ShearTimeline
struct  ShearTimeline_t3812859450  : public TranslateTimeline_t2733426737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHEARTIMELINE_T3812859450_H
#ifndef SCALETIMELINE_T1810411048_H
#define SCALETIMELINE_T1810411048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ScaleTimeline
struct  ScaleTimeline_t1810411048  : public TranslateTimeline_t2733426737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALETIMELINE_T1810411048_H
#ifndef TIMELINETYPE_T3142998273_H
#define TIMELINETYPE_T3142998273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TimelineType
struct  TimelineType_t3142998273 
{
public:
	// System.Int32 Spine.TimelineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TimelineType_t3142998273, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINETYPE_T3142998273_H
#ifndef MIXDIRECTION_T3149175205_H
#define MIXDIRECTION_T3149175205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixDirection
struct  MixDirection_t3149175205 
{
public:
	// System.Int32 Spine.MixDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixDirection_t3149175205, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXDIRECTION_T3149175205_H
#ifndef MIXPOSE_T983230599_H
#define MIXPOSE_T983230599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixPose
struct  MixPose_t983230599 
{
public:
	// System.Int32 Spine.MixPose::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixPose_t983230599, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXPOSE_T983230599_H
#ifndef U3CSMOOTHFADEU3EC__ITERATOR1_T1888227318_H
#define U3CSMOOTHFADEU3EC__ITERATOR1_T1888227318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1
struct  U3CSmoothFadeU3Ec__Iterator1_t1888227318  : public RuntimeObject
{
public:
	// UnityEngine.Color GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::<startColor>__0
	Color_t2555686324  ___U3CstartColorU3E__0_0;
	// System.Single GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::<t>__0
	float ___U3CtU3E__0_1;
	// System.Single GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::time
	float ___time_2;
	// UnityEngine.Color GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::<newColor>__1
	Color_t2555686324  ___U3CnewColorU3E__1_3;
	// System.Single GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::alpha
	float ___alpha_4;
	// GameVanilla.Game.UI.ScoreText GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::$this
	ScoreText_t753417783 * ___U24this_5;
	// System.Object GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 GameVanilla.Game.UI.ScoreText/<SmoothFade>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CstartColorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U3CstartColorU3E__0_0)); }
	inline Color_t2555686324  get_U3CstartColorU3E__0_0() const { return ___U3CstartColorU3E__0_0; }
	inline Color_t2555686324 * get_address_of_U3CstartColorU3E__0_0() { return &___U3CstartColorU3E__0_0; }
	inline void set_U3CstartColorU3E__0_0(Color_t2555686324  value)
	{
		___U3CstartColorU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U3CtU3E__0_1)); }
	inline float get_U3CtU3E__0_1() const { return ___U3CtU3E__0_1; }
	inline float* get_address_of_U3CtU3E__0_1() { return &___U3CtU3E__0_1; }
	inline void set_U3CtU3E__0_1(float value)
	{
		___U3CtU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__1_3() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U3CnewColorU3E__1_3)); }
	inline Color_t2555686324  get_U3CnewColorU3E__1_3() const { return ___U3CnewColorU3E__1_3; }
	inline Color_t2555686324 * get_address_of_U3CnewColorU3E__1_3() { return &___U3CnewColorU3E__1_3; }
	inline void set_U3CnewColorU3E__1_3(Color_t2555686324  value)
	{
		___U3CnewColorU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_alpha_4() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___alpha_4)); }
	inline float get_alpha_4() const { return ___alpha_4; }
	inline float* get_address_of_alpha_4() { return &___alpha_4; }
	inline void set_alpha_4(float value)
	{
		___alpha_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U24this_5)); }
	inline ScoreText_t753417783 * get_U24this_5() const { return ___U24this_5; }
	inline ScoreText_t753417783 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ScoreText_t753417783 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CSmoothFadeU3Ec__Iterator1_t1888227318, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHFADEU3EC__ITERATOR1_T1888227318_H
#ifndef U3CSMOOTHMOVEU3EC__ITERATOR0_T1363484482_H
#define U3CSMOOTHMOVEU3EC__ITERATOR0_T1363484482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0
struct  U3CSmoothMoveU3Ec__Iterator0_t1363484482  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::<startPos>__0
	Vector2_t2156229523  ___U3CstartPosU3E__0_0;
	// System.Single GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::<t>__0
	float ___U3CtU3E__0_1;
	// System.Single GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::time
	float ___time_2;
	// UnityEngine.Vector3 GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::pos
	Vector3_t3722313464  ___pos_3;
	// GameVanilla.Game.UI.ScoreText GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::$this
	ScoreText_t753417783 * ___U24this_4;
	// System.Object GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 GameVanilla.Game.UI.ScoreText/<SmoothMove>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartPosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___U3CstartPosU3E__0_0)); }
	inline Vector2_t2156229523  get_U3CstartPosU3E__0_0() const { return ___U3CstartPosU3E__0_0; }
	inline Vector2_t2156229523 * get_address_of_U3CstartPosU3E__0_0() { return &___U3CstartPosU3E__0_0; }
	inline void set_U3CstartPosU3E__0_0(Vector2_t2156229523  value)
	{
		___U3CstartPosU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___U3CtU3E__0_1)); }
	inline float get_U3CtU3E__0_1() const { return ___U3CtU3E__0_1; }
	inline float* get_address_of_U3CtU3E__0_1() { return &___U3CtU3E__0_1; }
	inline void set_U3CtU3E__0_1(float value)
	{
		___U3CtU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___pos_3)); }
	inline Vector3_t3722313464  get_pos_3() const { return ___pos_3; }
	inline Vector3_t3722313464 * get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(Vector3_t3722313464  value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___U24this_4)); }
	inline ScoreText_t753417783 * get_U24this_4() const { return ___U24this_4; }
	inline ScoreText_t753417783 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ScoreText_t753417783 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMoveU3Ec__Iterator0_t1363484482, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMOVEU3EC__ITERATOR0_T1363484482_H
#ifndef TEXTUREFILTER_T1610186802_H
#define TEXTUREFILTER_T1610186802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureFilter
struct  TextureFilter_t1610186802 
{
public:
	// System.Int32 Spine.TextureFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFilter_t1610186802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFILTER_T1610186802_H
#ifndef BOUNDINGBOXATTACHMENT_T2797506510_H
#define BOUNDINGBOXATTACHMENT_T2797506510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoundingBoxAttachment
struct  BoundingBoxAttachment_t2797506510  : public VertexAttachment_t4074366829
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXATTACHMENT_T2797506510_H
#ifndef CLIPPINGATTACHMENT_T2586274570_H
#define CLIPPINGATTACHMENT_T2586274570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ClippingAttachment
struct  ClippingAttachment_t2586274570  : public VertexAttachment_t4074366829
{
public:
	// Spine.SlotData Spine.ClippingAttachment::endSlot
	SlotData_t154801902 * ___endSlot_7;

public:
	inline static int32_t get_offset_of_endSlot_7() { return static_cast<int32_t>(offsetof(ClippingAttachment_t2586274570, ___endSlot_7)); }
	inline SlotData_t154801902 * get_endSlot_7() const { return ___endSlot_7; }
	inline SlotData_t154801902 ** get_address_of_endSlot_7() { return &___endSlot_7; }
	inline void set_endSlot_7(SlotData_t154801902 * value)
	{
		___endSlot_7 = value;
		Il2CppCodeGenWriteBarrier((&___endSlot_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPINGATTACHMENT_T2586274570_H
#ifndef MESHATTACHMENT_T1975337962_H
#define MESHATTACHMENT_T1975337962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MeshAttachment
struct  MeshAttachment_t1975337962  : public VertexAttachment_t4074366829
{
public:
	// System.Single Spine.MeshAttachment::regionOffsetX
	float ___regionOffsetX_7;
	// System.Single Spine.MeshAttachment::regionOffsetY
	float ___regionOffsetY_8;
	// System.Single Spine.MeshAttachment::regionWidth
	float ___regionWidth_9;
	// System.Single Spine.MeshAttachment::regionHeight
	float ___regionHeight_10;
	// System.Single Spine.MeshAttachment::regionOriginalWidth
	float ___regionOriginalWidth_11;
	// System.Single Spine.MeshAttachment::regionOriginalHeight
	float ___regionOriginalHeight_12;
	// Spine.MeshAttachment Spine.MeshAttachment::parentMesh
	MeshAttachment_t1975337962 * ___parentMesh_13;
	// System.Single[] Spine.MeshAttachment::uvs
	SingleU5BU5D_t1444911251* ___uvs_14;
	// System.Single[] Spine.MeshAttachment::regionUVs
	SingleU5BU5D_t1444911251* ___regionUVs_15;
	// System.Int32[] Spine.MeshAttachment::triangles
	Int32U5BU5D_t385246372* ___triangles_16;
	// System.Single Spine.MeshAttachment::r
	float ___r_17;
	// System.Single Spine.MeshAttachment::g
	float ___g_18;
	// System.Single Spine.MeshAttachment::b
	float ___b_19;
	// System.Single Spine.MeshAttachment::a
	float ___a_20;
	// System.Int32 Spine.MeshAttachment::hulllength
	int32_t ___hulllength_21;
	// System.Boolean Spine.MeshAttachment::inheritDeform
	bool ___inheritDeform_22;
	// System.String Spine.MeshAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_23;
	// System.Object Spine.MeshAttachment::<RendererObject>k__BackingField
	RuntimeObject * ___U3CRendererObjectU3Ek__BackingField_24;
	// System.Single Spine.MeshAttachment::<RegionU>k__BackingField
	float ___U3CRegionUU3Ek__BackingField_25;
	// System.Single Spine.MeshAttachment::<RegionV>k__BackingField
	float ___U3CRegionVU3Ek__BackingField_26;
	// System.Single Spine.MeshAttachment::<RegionU2>k__BackingField
	float ___U3CRegionU2U3Ek__BackingField_27;
	// System.Single Spine.MeshAttachment::<RegionV2>k__BackingField
	float ___U3CRegionV2U3Ek__BackingField_28;
	// System.Boolean Spine.MeshAttachment::<RegionRotate>k__BackingField
	bool ___U3CRegionRotateU3Ek__BackingField_29;
	// System.Int32[] Spine.MeshAttachment::<Edges>k__BackingField
	Int32U5BU5D_t385246372* ___U3CEdgesU3Ek__BackingField_30;
	// System.Single Spine.MeshAttachment::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_31;
	// System.Single Spine.MeshAttachment::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_regionOffsetX_7() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOffsetX_7)); }
	inline float get_regionOffsetX_7() const { return ___regionOffsetX_7; }
	inline float* get_address_of_regionOffsetX_7() { return &___regionOffsetX_7; }
	inline void set_regionOffsetX_7(float value)
	{
		___regionOffsetX_7 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_8() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOffsetY_8)); }
	inline float get_regionOffsetY_8() const { return ___regionOffsetY_8; }
	inline float* get_address_of_regionOffsetY_8() { return &___regionOffsetY_8; }
	inline void set_regionOffsetY_8(float value)
	{
		___regionOffsetY_8 = value;
	}

	inline static int32_t get_offset_of_regionWidth_9() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionWidth_9)); }
	inline float get_regionWidth_9() const { return ___regionWidth_9; }
	inline float* get_address_of_regionWidth_9() { return &___regionWidth_9; }
	inline void set_regionWidth_9(float value)
	{
		___regionWidth_9 = value;
	}

	inline static int32_t get_offset_of_regionHeight_10() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionHeight_10)); }
	inline float get_regionHeight_10() const { return ___regionHeight_10; }
	inline float* get_address_of_regionHeight_10() { return &___regionHeight_10; }
	inline void set_regionHeight_10(float value)
	{
		___regionHeight_10 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_11() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOriginalWidth_11)); }
	inline float get_regionOriginalWidth_11() const { return ___regionOriginalWidth_11; }
	inline float* get_address_of_regionOriginalWidth_11() { return &___regionOriginalWidth_11; }
	inline void set_regionOriginalWidth_11(float value)
	{
		___regionOriginalWidth_11 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_12() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionOriginalHeight_12)); }
	inline float get_regionOriginalHeight_12() const { return ___regionOriginalHeight_12; }
	inline float* get_address_of_regionOriginalHeight_12() { return &___regionOriginalHeight_12; }
	inline void set_regionOriginalHeight_12(float value)
	{
		___regionOriginalHeight_12 = value;
	}

	inline static int32_t get_offset_of_parentMesh_13() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___parentMesh_13)); }
	inline MeshAttachment_t1975337962 * get_parentMesh_13() const { return ___parentMesh_13; }
	inline MeshAttachment_t1975337962 ** get_address_of_parentMesh_13() { return &___parentMesh_13; }
	inline void set_parentMesh_13(MeshAttachment_t1975337962 * value)
	{
		___parentMesh_13 = value;
		Il2CppCodeGenWriteBarrier((&___parentMesh_13), value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___uvs_14)); }
	inline SingleU5BU5D_t1444911251* get_uvs_14() const { return ___uvs_14; }
	inline SingleU5BU5D_t1444911251** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(SingleU5BU5D_t1444911251* value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_14), value);
	}

	inline static int32_t get_offset_of_regionUVs_15() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___regionUVs_15)); }
	inline SingleU5BU5D_t1444911251* get_regionUVs_15() const { return ___regionUVs_15; }
	inline SingleU5BU5D_t1444911251** get_address_of_regionUVs_15() { return &___regionUVs_15; }
	inline void set_regionUVs_15(SingleU5BU5D_t1444911251* value)
	{
		___regionUVs_15 = value;
		Il2CppCodeGenWriteBarrier((&___regionUVs_15), value);
	}

	inline static int32_t get_offset_of_triangles_16() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___triangles_16)); }
	inline Int32U5BU5D_t385246372* get_triangles_16() const { return ___triangles_16; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_16() { return &___triangles_16; }
	inline void set_triangles_16(Int32U5BU5D_t385246372* value)
	{
		___triangles_16 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_16), value);
	}

	inline static int32_t get_offset_of_r_17() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___r_17)); }
	inline float get_r_17() const { return ___r_17; }
	inline float* get_address_of_r_17() { return &___r_17; }
	inline void set_r_17(float value)
	{
		___r_17 = value;
	}

	inline static int32_t get_offset_of_g_18() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___g_18)); }
	inline float get_g_18() const { return ___g_18; }
	inline float* get_address_of_g_18() { return &___g_18; }
	inline void set_g_18(float value)
	{
		___g_18 = value;
	}

	inline static int32_t get_offset_of_b_19() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___b_19)); }
	inline float get_b_19() const { return ___b_19; }
	inline float* get_address_of_b_19() { return &___b_19; }
	inline void set_b_19(float value)
	{
		___b_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_hulllength_21() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___hulllength_21)); }
	inline int32_t get_hulllength_21() const { return ___hulllength_21; }
	inline int32_t* get_address_of_hulllength_21() { return &___hulllength_21; }
	inline void set_hulllength_21(int32_t value)
	{
		___hulllength_21 = value;
	}

	inline static int32_t get_offset_of_inheritDeform_22() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___inheritDeform_22)); }
	inline bool get_inheritDeform_22() const { return ___inheritDeform_22; }
	inline bool* get_address_of_inheritDeform_22() { return &___inheritDeform_22; }
	inline void set_inheritDeform_22(bool value)
	{
		___inheritDeform_22 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CPathU3Ek__BackingField_23)); }
	inline String_t* get_U3CPathU3Ek__BackingField_23() const { return ___U3CPathU3Ek__BackingField_23; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_23() { return &___U3CPathU3Ek__BackingField_23; }
	inline void set_U3CPathU3Ek__BackingField_23(String_t* value)
	{
		___U3CPathU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CRendererObjectU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRendererObjectU3Ek__BackingField_24)); }
	inline RuntimeObject * get_U3CRendererObjectU3Ek__BackingField_24() const { return ___U3CRendererObjectU3Ek__BackingField_24; }
	inline RuntimeObject ** get_address_of_U3CRendererObjectU3Ek__BackingField_24() { return &___U3CRendererObjectU3Ek__BackingField_24; }
	inline void set_U3CRendererObjectU3Ek__BackingField_24(RuntimeObject * value)
	{
		___U3CRendererObjectU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRendererObjectU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CRegionUU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionUU3Ek__BackingField_25)); }
	inline float get_U3CRegionUU3Ek__BackingField_25() const { return ___U3CRegionUU3Ek__BackingField_25; }
	inline float* get_address_of_U3CRegionUU3Ek__BackingField_25() { return &___U3CRegionUU3Ek__BackingField_25; }
	inline void set_U3CRegionUU3Ek__BackingField_25(float value)
	{
		___U3CRegionUU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CRegionVU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionVU3Ek__BackingField_26)); }
	inline float get_U3CRegionVU3Ek__BackingField_26() const { return ___U3CRegionVU3Ek__BackingField_26; }
	inline float* get_address_of_U3CRegionVU3Ek__BackingField_26() { return &___U3CRegionVU3Ek__BackingField_26; }
	inline void set_U3CRegionVU3Ek__BackingField_26(float value)
	{
		___U3CRegionVU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CRegionU2U3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionU2U3Ek__BackingField_27)); }
	inline float get_U3CRegionU2U3Ek__BackingField_27() const { return ___U3CRegionU2U3Ek__BackingField_27; }
	inline float* get_address_of_U3CRegionU2U3Ek__BackingField_27() { return &___U3CRegionU2U3Ek__BackingField_27; }
	inline void set_U3CRegionU2U3Ek__BackingField_27(float value)
	{
		___U3CRegionU2U3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CRegionV2U3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionV2U3Ek__BackingField_28)); }
	inline float get_U3CRegionV2U3Ek__BackingField_28() const { return ___U3CRegionV2U3Ek__BackingField_28; }
	inline float* get_address_of_U3CRegionV2U3Ek__BackingField_28() { return &___U3CRegionV2U3Ek__BackingField_28; }
	inline void set_U3CRegionV2U3Ek__BackingField_28(float value)
	{
		___U3CRegionV2U3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CRegionRotateU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CRegionRotateU3Ek__BackingField_29)); }
	inline bool get_U3CRegionRotateU3Ek__BackingField_29() const { return ___U3CRegionRotateU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CRegionRotateU3Ek__BackingField_29() { return &___U3CRegionRotateU3Ek__BackingField_29; }
	inline void set_U3CRegionRotateU3Ek__BackingField_29(bool value)
	{
		___U3CRegionRotateU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CEdgesU3Ek__BackingField_30)); }
	inline Int32U5BU5D_t385246372* get_U3CEdgesU3Ek__BackingField_30() const { return ___U3CEdgesU3Ek__BackingField_30; }
	inline Int32U5BU5D_t385246372** get_address_of_U3CEdgesU3Ek__BackingField_30() { return &___U3CEdgesU3Ek__BackingField_30; }
	inline void set_U3CEdgesU3Ek__BackingField_30(Int32U5BU5D_t385246372* value)
	{
		___U3CEdgesU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CWidthU3Ek__BackingField_31)); }
	inline float get_U3CWidthU3Ek__BackingField_31() const { return ___U3CWidthU3Ek__BackingField_31; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_31() { return &___U3CWidthU3Ek__BackingField_31; }
	inline void set_U3CWidthU3Ek__BackingField_31(float value)
	{
		___U3CWidthU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(MeshAttachment_t1975337962, ___U3CHeightU3Ek__BackingField_32)); }
	inline float get_U3CHeightU3Ek__BackingField_32() const { return ___U3CHeightU3Ek__BackingField_32; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_32() { return &___U3CHeightU3Ek__BackingField_32; }
	inline void set_U3CHeightU3Ek__BackingField_32(float value)
	{
		___U3CHeightU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHATTACHMENT_T1975337962_H
#ifndef PATHATTACHMENT_T3565151060_H
#define PATHATTACHMENT_T3565151060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathAttachment
struct  PathAttachment_t3565151060  : public VertexAttachment_t4074366829
{
public:
	// System.Single[] Spine.PathAttachment::lengths
	SingleU5BU5D_t1444911251* ___lengths_7;
	// System.Boolean Spine.PathAttachment::closed
	bool ___closed_8;
	// System.Boolean Spine.PathAttachment::constantSpeed
	bool ___constantSpeed_9;

public:
	inline static int32_t get_offset_of_lengths_7() { return static_cast<int32_t>(offsetof(PathAttachment_t3565151060, ___lengths_7)); }
	inline SingleU5BU5D_t1444911251* get_lengths_7() const { return ___lengths_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_lengths_7() { return &___lengths_7; }
	inline void set_lengths_7(SingleU5BU5D_t1444911251* value)
	{
		___lengths_7 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_7), value);
	}

	inline static int32_t get_offset_of_closed_8() { return static_cast<int32_t>(offsetof(PathAttachment_t3565151060, ___closed_8)); }
	inline bool get_closed_8() const { return ___closed_8; }
	inline bool* get_address_of_closed_8() { return &___closed_8; }
	inline void set_closed_8(bool value)
	{
		___closed_8 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_9() { return static_cast<int32_t>(offsetof(PathAttachment_t3565151060, ___constantSpeed_9)); }
	inline bool get_constantSpeed_9() const { return ___constantSpeed_9; }
	inline bool* get_address_of_constantSpeed_9() { return &___constantSpeed_9; }
	inline void set_constantSpeed_9(bool value)
	{
		___constantSpeed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHATTACHMENT_T3565151060_H
#ifndef BLENDMODE_T358635744_H
#define BLENDMODE_T358635744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t358635744 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendMode_t358635744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T358635744_H
#ifndef TRANSFORMMODE_T614987360_H
#define TRANSFORMMODE_T614987360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformMode
struct  TransformMode_t614987360 
{
public:
	// System.Int32 Spine.TransformMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformMode_t614987360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMODE_T614987360_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TEXTUREWRAP_T2819935362_H
#define TEXTUREWRAP_T2819935362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureWrap
struct  TextureWrap_t2819935362 
{
public:
	// System.Int32 Spine.TextureWrap::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrap_t2819935362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAP_T2819935362_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BONEDATA_T3130174490_H
#define BONEDATA_T3130174490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoneData
struct  BoneData_t3130174490  : public RuntimeObject
{
public:
	// System.Int32 Spine.BoneData::index
	int32_t ___index_0;
	// System.String Spine.BoneData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.BoneData::parent
	BoneData_t3130174490 * ___parent_2;
	// System.Single Spine.BoneData::length
	float ___length_3;
	// System.Single Spine.BoneData::x
	float ___x_4;
	// System.Single Spine.BoneData::y
	float ___y_5;
	// System.Single Spine.BoneData::rotation
	float ___rotation_6;
	// System.Single Spine.BoneData::scaleX
	float ___scaleX_7;
	// System.Single Spine.BoneData::scaleY
	float ___scaleY_8;
	// System.Single Spine.BoneData::shearX
	float ___shearX_9;
	// System.Single Spine.BoneData::shearY
	float ___shearY_10;
	// Spine.TransformMode Spine.BoneData::transformMode
	int32_t ___transformMode_11;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___parent_2)); }
	inline BoneData_t3130174490 * get_parent_2() const { return ___parent_2; }
	inline BoneData_t3130174490 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BoneData_t3130174490 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___rotation_6)); }
	inline float get_rotation_6() const { return ___rotation_6; }
	inline float* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(float value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scaleX_7() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___scaleX_7)); }
	inline float get_scaleX_7() const { return ___scaleX_7; }
	inline float* get_address_of_scaleX_7() { return &___scaleX_7; }
	inline void set_scaleX_7(float value)
	{
		___scaleX_7 = value;
	}

	inline static int32_t get_offset_of_scaleY_8() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___scaleY_8)); }
	inline float get_scaleY_8() const { return ___scaleY_8; }
	inline float* get_address_of_scaleY_8() { return &___scaleY_8; }
	inline void set_scaleY_8(float value)
	{
		___scaleY_8 = value;
	}

	inline static int32_t get_offset_of_shearX_9() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___shearX_9)); }
	inline float get_shearX_9() const { return ___shearX_9; }
	inline float* get_address_of_shearX_9() { return &___shearX_9; }
	inline void set_shearX_9(float value)
	{
		___shearX_9 = value;
	}

	inline static int32_t get_offset_of_shearY_10() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___shearY_10)); }
	inline float get_shearY_10() const { return ___shearY_10; }
	inline float* get_address_of_shearY_10() { return &___shearY_10; }
	inline void set_shearY_10(float value)
	{
		___shearY_10 = value;
	}

	inline static int32_t get_offset_of_transformMode_11() { return static_cast<int32_t>(offsetof(BoneData_t3130174490, ___transformMode_11)); }
	inline int32_t get_transformMode_11() const { return ___transformMode_11; }
	inline int32_t* get_address_of_transformMode_11() { return &___transformMode_11; }
	inline void set_transformMode_11(int32_t value)
	{
		___transformMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEDATA_T3130174490_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ATLASPAGE_T4077017671_H
#define ATLASPAGE_T4077017671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasPage
struct  AtlasPage_t4077017671  : public RuntimeObject
{
public:
	// System.String Spine.AtlasPage::name
	String_t* ___name_0;
	// Spine.Format Spine.AtlasPage::format
	int32_t ___format_1;
	// Spine.TextureFilter Spine.AtlasPage::minFilter
	int32_t ___minFilter_2;
	// Spine.TextureFilter Spine.AtlasPage::magFilter
	int32_t ___magFilter_3;
	// Spine.TextureWrap Spine.AtlasPage::uWrap
	int32_t ___uWrap_4;
	// Spine.TextureWrap Spine.AtlasPage::vWrap
	int32_t ___vWrap_5;
	// System.Object Spine.AtlasPage::rendererObject
	RuntimeObject * ___rendererObject_6;
	// System.Int32 Spine.AtlasPage::width
	int32_t ___width_7;
	// System.Int32 Spine.AtlasPage::height
	int32_t ___height_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_format_1() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___format_1)); }
	inline int32_t get_format_1() const { return ___format_1; }
	inline int32_t* get_address_of_format_1() { return &___format_1; }
	inline void set_format_1(int32_t value)
	{
		___format_1 = value;
	}

	inline static int32_t get_offset_of_minFilter_2() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___minFilter_2)); }
	inline int32_t get_minFilter_2() const { return ___minFilter_2; }
	inline int32_t* get_address_of_minFilter_2() { return &___minFilter_2; }
	inline void set_minFilter_2(int32_t value)
	{
		___minFilter_2 = value;
	}

	inline static int32_t get_offset_of_magFilter_3() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___magFilter_3)); }
	inline int32_t get_magFilter_3() const { return ___magFilter_3; }
	inline int32_t* get_address_of_magFilter_3() { return &___magFilter_3; }
	inline void set_magFilter_3(int32_t value)
	{
		___magFilter_3 = value;
	}

	inline static int32_t get_offset_of_uWrap_4() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___uWrap_4)); }
	inline int32_t get_uWrap_4() const { return ___uWrap_4; }
	inline int32_t* get_address_of_uWrap_4() { return &___uWrap_4; }
	inline void set_uWrap_4(int32_t value)
	{
		___uWrap_4 = value;
	}

	inline static int32_t get_offset_of_vWrap_5() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___vWrap_5)); }
	inline int32_t get_vWrap_5() const { return ___vWrap_5; }
	inline int32_t* get_address_of_vWrap_5() { return &___vWrap_5; }
	inline void set_vWrap_5(int32_t value)
	{
		___vWrap_5 = value;
	}

	inline static int32_t get_offset_of_rendererObject_6() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___rendererObject_6)); }
	inline RuntimeObject * get_rendererObject_6() const { return ___rendererObject_6; }
	inline RuntimeObject ** get_address_of_rendererObject_6() { return &___rendererObject_6; }
	inline void set_rendererObject_6(RuntimeObject * value)
	{
		___rendererObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___rendererObject_6), value);
	}

	inline static int32_t get_offset_of_width_7() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___width_7)); }
	inline int32_t get_width_7() const { return ___width_7; }
	inline int32_t* get_address_of_width_7() { return &___width_7; }
	inline void set_width_7(int32_t value)
	{
		___width_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(AtlasPage_t4077017671, ___height_8)); }
	inline int32_t get_height_8() const { return ___height_8; }
	inline int32_t* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(int32_t value)
	{
		___height_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPAGE_T4077017671_H
#ifndef PATHCONSTRAINTDATA_T981297034_H
#define PATHCONSTRAINTDATA_T981297034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintData
struct  PathConstraintData_t981297034  : public RuntimeObject
{
public:
	// System.String Spine.PathConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.PathConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.PathConstraintData::bones
	ExposedList_1_t1542319060 * ___bones_2;
	// Spine.SlotData Spine.PathConstraintData::target
	SlotData_t154801902 * ___target_3;
	// Spine.PositionMode Spine.PathConstraintData::positionMode
	int32_t ___positionMode_4;
	// Spine.SpacingMode Spine.PathConstraintData::spacingMode
	int32_t ___spacingMode_5;
	// Spine.RotateMode Spine.PathConstraintData::rotateMode
	int32_t ___rotateMode_6;
	// System.Single Spine.PathConstraintData::offsetRotation
	float ___offsetRotation_7;
	// System.Single Spine.PathConstraintData::position
	float ___position_8;
	// System.Single Spine.PathConstraintData::spacing
	float ___spacing_9;
	// System.Single Spine.PathConstraintData::rotateMix
	float ___rotateMix_10;
	// System.Single Spine.PathConstraintData::translateMix
	float ___translateMix_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___bones_2)); }
	inline ExposedList_1_t1542319060 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t1542319060 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t1542319060 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___target_3)); }
	inline SlotData_t154801902 * get_target_3() const { return ___target_3; }
	inline SlotData_t154801902 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(SlotData_t154801902 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_positionMode_4() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___positionMode_4)); }
	inline int32_t get_positionMode_4() const { return ___positionMode_4; }
	inline int32_t* get_address_of_positionMode_4() { return &___positionMode_4; }
	inline void set_positionMode_4(int32_t value)
	{
		___positionMode_4 = value;
	}

	inline static int32_t get_offset_of_spacingMode_5() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___spacingMode_5)); }
	inline int32_t get_spacingMode_5() const { return ___spacingMode_5; }
	inline int32_t* get_address_of_spacingMode_5() { return &___spacingMode_5; }
	inline void set_spacingMode_5(int32_t value)
	{
		___spacingMode_5 = value;
	}

	inline static int32_t get_offset_of_rotateMode_6() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___rotateMode_6)); }
	inline int32_t get_rotateMode_6() const { return ___rotateMode_6; }
	inline int32_t* get_address_of_rotateMode_6() { return &___rotateMode_6; }
	inline void set_rotateMode_6(int32_t value)
	{
		___rotateMode_6 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_7() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___offsetRotation_7)); }
	inline float get_offsetRotation_7() const { return ___offsetRotation_7; }
	inline float* get_address_of_offsetRotation_7() { return &___offsetRotation_7; }
	inline void set_offsetRotation_7(float value)
	{
		___offsetRotation_7 = value;
	}

	inline static int32_t get_offset_of_position_8() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___position_8)); }
	inline float get_position_8() const { return ___position_8; }
	inline float* get_address_of_position_8() { return &___position_8; }
	inline void set_position_8(float value)
	{
		___position_8 = value;
	}

	inline static int32_t get_offset_of_spacing_9() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___spacing_9)); }
	inline float get_spacing_9() const { return ___spacing_9; }
	inline float* get_address_of_spacing_9() { return &___spacing_9; }
	inline void set_spacing_9(float value)
	{
		___spacing_9 = value;
	}

	inline static int32_t get_offset_of_rotateMix_10() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___rotateMix_10)); }
	inline float get_rotateMix_10() const { return ___rotateMix_10; }
	inline float* get_address_of_rotateMix_10() { return &___rotateMix_10; }
	inline void set_rotateMix_10(float value)
	{
		___rotateMix_10 = value;
	}

	inline static int32_t get_offset_of_translateMix_11() { return static_cast<int32_t>(offsetof(PathConstraintData_t981297034, ___translateMix_11)); }
	inline float get_translateMix_11() const { return ___translateMix_11; }
	inline float* get_address_of_translateMix_11() { return &___translateMix_11; }
	inline void set_translateMix_11(float value)
	{
		___translateMix_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTDATA_T981297034_H
#ifndef EVENTQUEUEENTRY_T351831961_H
#define EVENTQUEUEENTRY_T351831961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventQueueEntry
struct  EventQueueEntry_t351831961 
{
public:
	// Spine.EventQueue/EventType Spine.EventQueue/EventQueueEntry::type
	int32_t ___type_0;
	// Spine.TrackEntry Spine.EventQueue/EventQueueEntry::entry
	TrackEntry_t1316488441 * ___entry_1;
	// Spine.Event Spine.EventQueue/EventQueueEntry::e
	Event_t1378573841 * ___e_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(EventQueueEntry_t351831961, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_entry_1() { return static_cast<int32_t>(offsetof(EventQueueEntry_t351831961, ___entry_1)); }
	inline TrackEntry_t1316488441 * get_entry_1() const { return ___entry_1; }
	inline TrackEntry_t1316488441 ** get_address_of_entry_1() { return &___entry_1; }
	inline void set_entry_1(TrackEntry_t1316488441 * value)
	{
		___entry_1 = value;
		Il2CppCodeGenWriteBarrier((&___entry_1), value);
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(EventQueueEntry_t351831961, ___e_2)); }
	inline Event_t1378573841 * get_e_2() const { return ___e_2; }
	inline Event_t1378573841 ** get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(Event_t1378573841 * value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((&___e_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t351831961_marshaled_pinvoke
{
	int32_t ___type_0;
	TrackEntry_t1316488441 * ___entry_1;
	Event_t1378573841 * ___e_2;
};
// Native definition for COM marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t351831961_marshaled_com
{
	int32_t ___type_0;
	TrackEntry_t1316488441 * ___entry_1;
	Event_t1378573841 * ___e_2;
};
#endif // EVENTQUEUEENTRY_T351831961_H
#ifndef TRACKENTRYDELEGATE_T363257942_H
#define TRACKENTRYDELEGATE_T363257942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryDelegate
struct  TrackEntryDelegate_t363257942  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYDELEGATE_T363257942_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TRACKENTRYEVENTDELEGATE_T1653995044_H
#define TRACKENTRYEVENTDELEGATE_T1653995044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryEventDelegate
struct  TrackEntryEventDelegate_t1653995044  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYEVENTDELEGATE_T1653995044_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef LEVELAVATAR_T2279482836_H
#define LEVELAVATAR_T2279482836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.LevelAvatar
struct  LevelAvatar_t2279482836  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite GameVanilla.Game.UI.LevelAvatar::girlAvatarSprite
	Sprite_t280657092 * ___girlAvatarSprite_2;
	// UnityEngine.Sprite GameVanilla.Game.UI.LevelAvatar::boyAvatarSprite
	Sprite_t280657092 * ___boyAvatarSprite_3;
	// UnityEngine.UI.Image GameVanilla.Game.UI.LevelAvatar::avatarImage
	Image_t2670269651 * ___avatarImage_4;
	// System.Boolean GameVanilla.Game.UI.LevelAvatar::floating
	bool ___floating_5;
	// System.Single GameVanilla.Game.UI.LevelAvatar::runningTime
	float ___runningTime_6;

public:
	inline static int32_t get_offset_of_girlAvatarSprite_2() { return static_cast<int32_t>(offsetof(LevelAvatar_t2279482836, ___girlAvatarSprite_2)); }
	inline Sprite_t280657092 * get_girlAvatarSprite_2() const { return ___girlAvatarSprite_2; }
	inline Sprite_t280657092 ** get_address_of_girlAvatarSprite_2() { return &___girlAvatarSprite_2; }
	inline void set_girlAvatarSprite_2(Sprite_t280657092 * value)
	{
		___girlAvatarSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___girlAvatarSprite_2), value);
	}

	inline static int32_t get_offset_of_boyAvatarSprite_3() { return static_cast<int32_t>(offsetof(LevelAvatar_t2279482836, ___boyAvatarSprite_3)); }
	inline Sprite_t280657092 * get_boyAvatarSprite_3() const { return ___boyAvatarSprite_3; }
	inline Sprite_t280657092 ** get_address_of_boyAvatarSprite_3() { return &___boyAvatarSprite_3; }
	inline void set_boyAvatarSprite_3(Sprite_t280657092 * value)
	{
		___boyAvatarSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___boyAvatarSprite_3), value);
	}

	inline static int32_t get_offset_of_avatarImage_4() { return static_cast<int32_t>(offsetof(LevelAvatar_t2279482836, ___avatarImage_4)); }
	inline Image_t2670269651 * get_avatarImage_4() const { return ___avatarImage_4; }
	inline Image_t2670269651 ** get_address_of_avatarImage_4() { return &___avatarImage_4; }
	inline void set_avatarImage_4(Image_t2670269651 * value)
	{
		___avatarImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___avatarImage_4), value);
	}

	inline static int32_t get_offset_of_floating_5() { return static_cast<int32_t>(offsetof(LevelAvatar_t2279482836, ___floating_5)); }
	inline bool get_floating_5() const { return ___floating_5; }
	inline bool* get_address_of_floating_5() { return &___floating_5; }
	inline void set_floating_5(bool value)
	{
		___floating_5 = value;
	}

	inline static int32_t get_offset_of_runningTime_6() { return static_cast<int32_t>(offsetof(LevelAvatar_t2279482836, ___runningTime_6)); }
	inline float get_runningTime_6() const { return ___runningTime_6; }
	inline float* get_address_of_runningTime_6() { return &___runningTime_6; }
	inline void set_runningTime_6(float value)
	{
		___runningTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELAVATAR_T2279482836_H
#ifndef LEVELBUTTON_T1543636365_H
#define LEVELBUTTON_T1543636365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.LevelButton
struct  LevelButton_t1543636365  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GameVanilla.Game.UI.LevelButton::numLevel
	int32_t ___numLevel_2;
	// UnityEngine.Sprite GameVanilla.Game.UI.LevelButton::currentButtonSprite
	Sprite_t280657092 * ___currentButtonSprite_3;
	// UnityEngine.Sprite GameVanilla.Game.UI.LevelButton::playedButtonSprite
	Sprite_t280657092 * ___playedButtonSprite_4;
	// UnityEngine.Sprite GameVanilla.Game.UI.LevelButton::lockedButtonSprite
	Sprite_t280657092 * ___lockedButtonSprite_5;
	// UnityEngine.Sprite GameVanilla.Game.UI.LevelButton::yellowStarSprite
	Sprite_t280657092 * ___yellowStarSprite_6;
	// UnityEngine.UI.Image GameVanilla.Game.UI.LevelButton::buttonImage
	Image_t2670269651 * ___buttonImage_7;
	// UnityEngine.UI.Text GameVanilla.Game.UI.LevelButton::numLevelText
	Text_t1901882714 * ___numLevelText_8;
	// UnityEngine.GameObject GameVanilla.Game.UI.LevelButton::star1
	GameObject_t1113636619 * ___star1_9;
	// UnityEngine.GameObject GameVanilla.Game.UI.LevelButton::star2
	GameObject_t1113636619 * ___star2_10;
	// UnityEngine.GameObject GameVanilla.Game.UI.LevelButton::star3
	GameObject_t1113636619 * ___star3_11;
	// UnityEngine.GameObject GameVanilla.Game.UI.LevelButton::shineAnimation
	GameObject_t1113636619 * ___shineAnimation_12;

public:
	inline static int32_t get_offset_of_numLevel_2() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___numLevel_2)); }
	inline int32_t get_numLevel_2() const { return ___numLevel_2; }
	inline int32_t* get_address_of_numLevel_2() { return &___numLevel_2; }
	inline void set_numLevel_2(int32_t value)
	{
		___numLevel_2 = value;
	}

	inline static int32_t get_offset_of_currentButtonSprite_3() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___currentButtonSprite_3)); }
	inline Sprite_t280657092 * get_currentButtonSprite_3() const { return ___currentButtonSprite_3; }
	inline Sprite_t280657092 ** get_address_of_currentButtonSprite_3() { return &___currentButtonSprite_3; }
	inline void set_currentButtonSprite_3(Sprite_t280657092 * value)
	{
		___currentButtonSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentButtonSprite_3), value);
	}

	inline static int32_t get_offset_of_playedButtonSprite_4() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___playedButtonSprite_4)); }
	inline Sprite_t280657092 * get_playedButtonSprite_4() const { return ___playedButtonSprite_4; }
	inline Sprite_t280657092 ** get_address_of_playedButtonSprite_4() { return &___playedButtonSprite_4; }
	inline void set_playedButtonSprite_4(Sprite_t280657092 * value)
	{
		___playedButtonSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___playedButtonSprite_4), value);
	}

	inline static int32_t get_offset_of_lockedButtonSprite_5() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___lockedButtonSprite_5)); }
	inline Sprite_t280657092 * get_lockedButtonSprite_5() const { return ___lockedButtonSprite_5; }
	inline Sprite_t280657092 ** get_address_of_lockedButtonSprite_5() { return &___lockedButtonSprite_5; }
	inline void set_lockedButtonSprite_5(Sprite_t280657092 * value)
	{
		___lockedButtonSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___lockedButtonSprite_5), value);
	}

	inline static int32_t get_offset_of_yellowStarSprite_6() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___yellowStarSprite_6)); }
	inline Sprite_t280657092 * get_yellowStarSprite_6() const { return ___yellowStarSprite_6; }
	inline Sprite_t280657092 ** get_address_of_yellowStarSprite_6() { return &___yellowStarSprite_6; }
	inline void set_yellowStarSprite_6(Sprite_t280657092 * value)
	{
		___yellowStarSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___yellowStarSprite_6), value);
	}

	inline static int32_t get_offset_of_buttonImage_7() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___buttonImage_7)); }
	inline Image_t2670269651 * get_buttonImage_7() const { return ___buttonImage_7; }
	inline Image_t2670269651 ** get_address_of_buttonImage_7() { return &___buttonImage_7; }
	inline void set_buttonImage_7(Image_t2670269651 * value)
	{
		___buttonImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___buttonImage_7), value);
	}

	inline static int32_t get_offset_of_numLevelText_8() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___numLevelText_8)); }
	inline Text_t1901882714 * get_numLevelText_8() const { return ___numLevelText_8; }
	inline Text_t1901882714 ** get_address_of_numLevelText_8() { return &___numLevelText_8; }
	inline void set_numLevelText_8(Text_t1901882714 * value)
	{
		___numLevelText_8 = value;
		Il2CppCodeGenWriteBarrier((&___numLevelText_8), value);
	}

	inline static int32_t get_offset_of_star1_9() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___star1_9)); }
	inline GameObject_t1113636619 * get_star1_9() const { return ___star1_9; }
	inline GameObject_t1113636619 ** get_address_of_star1_9() { return &___star1_9; }
	inline void set_star1_9(GameObject_t1113636619 * value)
	{
		___star1_9 = value;
		Il2CppCodeGenWriteBarrier((&___star1_9), value);
	}

	inline static int32_t get_offset_of_star2_10() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___star2_10)); }
	inline GameObject_t1113636619 * get_star2_10() const { return ___star2_10; }
	inline GameObject_t1113636619 ** get_address_of_star2_10() { return &___star2_10; }
	inline void set_star2_10(GameObject_t1113636619 * value)
	{
		___star2_10 = value;
		Il2CppCodeGenWriteBarrier((&___star2_10), value);
	}

	inline static int32_t get_offset_of_star3_11() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___star3_11)); }
	inline GameObject_t1113636619 * get_star3_11() const { return ___star3_11; }
	inline GameObject_t1113636619 ** get_address_of_star3_11() { return &___star3_11; }
	inline void set_star3_11(GameObject_t1113636619 * value)
	{
		___star3_11 = value;
		Il2CppCodeGenWriteBarrier((&___star3_11), value);
	}

	inline static int32_t get_offset_of_shineAnimation_12() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365, ___shineAnimation_12)); }
	inline GameObject_t1113636619 * get_shineAnimation_12() const { return ___shineAnimation_12; }
	inline GameObject_t1113636619 ** get_address_of_shineAnimation_12() { return &___shineAnimation_12; }
	inline void set_shineAnimation_12(GameObject_t1113636619 * value)
	{
		___shineAnimation_12 = value;
		Il2CppCodeGenWriteBarrier((&___shineAnimation_12), value);
	}
};

struct LevelButton_t1543636365_StaticFields
{
public:
	// System.Action`1<GameVanilla.Game.Popups.AlertPopup> GameVanilla.Game.UI.LevelButton::<>f__am$cache0
	Action_1_t327652521 * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(LevelButton_t1543636365_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Action_1_t327652521 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Action_1_t327652521 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Action_1_t327652521 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELBUTTON_T1543636365_H
#ifndef IAPROW_T4269885083_H
#define IAPROW_T4269885083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.IapRow
struct  IapRow_t4269885083  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Popups.BuyCoinsPopup GameVanilla.Game.UI.IapRow::buyCoinsPopup
	BuyCoinsPopup_t414112668 * ___buyCoinsPopup_2;
	// UnityEngine.GameObject GameVanilla.Game.UI.IapRow::mostPopular
	GameObject_t1113636619 * ___mostPopular_3;
	// UnityEngine.GameObject GameVanilla.Game.UI.IapRow::bestValue
	GameObject_t1113636619 * ___bestValue_4;
	// UnityEngine.GameObject GameVanilla.Game.UI.IapRow::discount
	GameObject_t1113636619 * ___discount_5;
	// UnityEngine.UI.Text GameVanilla.Game.UI.IapRow::discountText
	Text_t1901882714 * ___discountText_6;
	// UnityEngine.UI.Text GameVanilla.Game.UI.IapRow::numCoinsText
	Text_t1901882714 * ___numCoinsText_7;
	// UnityEngine.UI.Text GameVanilla.Game.UI.IapRow::priceText
	Text_t1901882714 * ___priceText_8;
	// UnityEngine.UI.Image GameVanilla.Game.UI.IapRow::coinsImage
	Image_t2670269651 * ___coinsImage_9;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> GameVanilla.Game.UI.IapRow::coinIcons
	List_1_t1752731834 * ___coinIcons_10;
	// GameVanilla.Game.Common.IapItem GameVanilla.Game.UI.IapRow::cachedItem
	IapItem_t3448270226 * ___cachedItem_11;

public:
	inline static int32_t get_offset_of_buyCoinsPopup_2() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___buyCoinsPopup_2)); }
	inline BuyCoinsPopup_t414112668 * get_buyCoinsPopup_2() const { return ___buyCoinsPopup_2; }
	inline BuyCoinsPopup_t414112668 ** get_address_of_buyCoinsPopup_2() { return &___buyCoinsPopup_2; }
	inline void set_buyCoinsPopup_2(BuyCoinsPopup_t414112668 * value)
	{
		___buyCoinsPopup_2 = value;
		Il2CppCodeGenWriteBarrier((&___buyCoinsPopup_2), value);
	}

	inline static int32_t get_offset_of_mostPopular_3() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___mostPopular_3)); }
	inline GameObject_t1113636619 * get_mostPopular_3() const { return ___mostPopular_3; }
	inline GameObject_t1113636619 ** get_address_of_mostPopular_3() { return &___mostPopular_3; }
	inline void set_mostPopular_3(GameObject_t1113636619 * value)
	{
		___mostPopular_3 = value;
		Il2CppCodeGenWriteBarrier((&___mostPopular_3), value);
	}

	inline static int32_t get_offset_of_bestValue_4() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___bestValue_4)); }
	inline GameObject_t1113636619 * get_bestValue_4() const { return ___bestValue_4; }
	inline GameObject_t1113636619 ** get_address_of_bestValue_4() { return &___bestValue_4; }
	inline void set_bestValue_4(GameObject_t1113636619 * value)
	{
		___bestValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___bestValue_4), value);
	}

	inline static int32_t get_offset_of_discount_5() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___discount_5)); }
	inline GameObject_t1113636619 * get_discount_5() const { return ___discount_5; }
	inline GameObject_t1113636619 ** get_address_of_discount_5() { return &___discount_5; }
	inline void set_discount_5(GameObject_t1113636619 * value)
	{
		___discount_5 = value;
		Il2CppCodeGenWriteBarrier((&___discount_5), value);
	}

	inline static int32_t get_offset_of_discountText_6() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___discountText_6)); }
	inline Text_t1901882714 * get_discountText_6() const { return ___discountText_6; }
	inline Text_t1901882714 ** get_address_of_discountText_6() { return &___discountText_6; }
	inline void set_discountText_6(Text_t1901882714 * value)
	{
		___discountText_6 = value;
		Il2CppCodeGenWriteBarrier((&___discountText_6), value);
	}

	inline static int32_t get_offset_of_numCoinsText_7() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___numCoinsText_7)); }
	inline Text_t1901882714 * get_numCoinsText_7() const { return ___numCoinsText_7; }
	inline Text_t1901882714 ** get_address_of_numCoinsText_7() { return &___numCoinsText_7; }
	inline void set_numCoinsText_7(Text_t1901882714 * value)
	{
		___numCoinsText_7 = value;
		Il2CppCodeGenWriteBarrier((&___numCoinsText_7), value);
	}

	inline static int32_t get_offset_of_priceText_8() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___priceText_8)); }
	inline Text_t1901882714 * get_priceText_8() const { return ___priceText_8; }
	inline Text_t1901882714 ** get_address_of_priceText_8() { return &___priceText_8; }
	inline void set_priceText_8(Text_t1901882714 * value)
	{
		___priceText_8 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_8), value);
	}

	inline static int32_t get_offset_of_coinsImage_9() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___coinsImage_9)); }
	inline Image_t2670269651 * get_coinsImage_9() const { return ___coinsImage_9; }
	inline Image_t2670269651 ** get_address_of_coinsImage_9() { return &___coinsImage_9; }
	inline void set_coinsImage_9(Image_t2670269651 * value)
	{
		___coinsImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___coinsImage_9), value);
	}

	inline static int32_t get_offset_of_coinIcons_10() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___coinIcons_10)); }
	inline List_1_t1752731834 * get_coinIcons_10() const { return ___coinIcons_10; }
	inline List_1_t1752731834 ** get_address_of_coinIcons_10() { return &___coinIcons_10; }
	inline void set_coinIcons_10(List_1_t1752731834 * value)
	{
		___coinIcons_10 = value;
		Il2CppCodeGenWriteBarrier((&___coinIcons_10), value);
	}

	inline static int32_t get_offset_of_cachedItem_11() { return static_cast<int32_t>(offsetof(IapRow_t4269885083, ___cachedItem_11)); }
	inline IapItem_t3448270226 * get_cachedItem_11() const { return ___cachedItem_11; }
	inline IapItem_t3448270226 ** get_address_of_cachedItem_11() { return &___cachedItem_11; }
	inline void set_cachedItem_11(IapItem_t3448270226 * value)
	{
		___cachedItem_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedItem_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPROW_T4269885083_H
#ifndef PROGRESSBAR_T959013686_H
#define PROGRESSBAR_T959013686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.ProgressBar
struct  ProgressBar_t959013686  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image GameVanilla.Game.UI.ProgressBar::progressBarImage
	Image_t2670269651 * ___progressBarImage_2;
	// GameVanilla.Game.UI.ProgressStar GameVanilla.Game.UI.ProgressBar::star1Image
	ProgressStar_t2895061392 * ___star1Image_3;
	// GameVanilla.Game.UI.ProgressStar GameVanilla.Game.UI.ProgressBar::star2Image
	ProgressStar_t2895061392 * ___star2Image_4;
	// GameVanilla.Game.UI.ProgressStar GameVanilla.Game.UI.ProgressBar::star3Image
	ProgressStar_t2895061392 * ___star3Image_5;
	// System.Int32 GameVanilla.Game.UI.ProgressBar::star1
	int32_t ___star1_6;
	// System.Int32 GameVanilla.Game.UI.ProgressBar::star2
	int32_t ___star2_7;
	// System.Int32 GameVanilla.Game.UI.ProgressBar::star3
	int32_t ___star3_8;

public:
	inline static int32_t get_offset_of_progressBarImage_2() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___progressBarImage_2)); }
	inline Image_t2670269651 * get_progressBarImage_2() const { return ___progressBarImage_2; }
	inline Image_t2670269651 ** get_address_of_progressBarImage_2() { return &___progressBarImage_2; }
	inline void set_progressBarImage_2(Image_t2670269651 * value)
	{
		___progressBarImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___progressBarImage_2), value);
	}

	inline static int32_t get_offset_of_star1Image_3() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___star1Image_3)); }
	inline ProgressStar_t2895061392 * get_star1Image_3() const { return ___star1Image_3; }
	inline ProgressStar_t2895061392 ** get_address_of_star1Image_3() { return &___star1Image_3; }
	inline void set_star1Image_3(ProgressStar_t2895061392 * value)
	{
		___star1Image_3 = value;
		Il2CppCodeGenWriteBarrier((&___star1Image_3), value);
	}

	inline static int32_t get_offset_of_star2Image_4() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___star2Image_4)); }
	inline ProgressStar_t2895061392 * get_star2Image_4() const { return ___star2Image_4; }
	inline ProgressStar_t2895061392 ** get_address_of_star2Image_4() { return &___star2Image_4; }
	inline void set_star2Image_4(ProgressStar_t2895061392 * value)
	{
		___star2Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___star2Image_4), value);
	}

	inline static int32_t get_offset_of_star3Image_5() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___star3Image_5)); }
	inline ProgressStar_t2895061392 * get_star3Image_5() const { return ___star3Image_5; }
	inline ProgressStar_t2895061392 ** get_address_of_star3Image_5() { return &___star3Image_5; }
	inline void set_star3Image_5(ProgressStar_t2895061392 * value)
	{
		___star3Image_5 = value;
		Il2CppCodeGenWriteBarrier((&___star3Image_5), value);
	}

	inline static int32_t get_offset_of_star1_6() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___star1_6)); }
	inline int32_t get_star1_6() const { return ___star1_6; }
	inline int32_t* get_address_of_star1_6() { return &___star1_6; }
	inline void set_star1_6(int32_t value)
	{
		___star1_6 = value;
	}

	inline static int32_t get_offset_of_star2_7() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___star2_7)); }
	inline int32_t get_star2_7() const { return ___star2_7; }
	inline int32_t* get_address_of_star2_7() { return &___star2_7; }
	inline void set_star2_7(int32_t value)
	{
		___star2_7 = value;
	}

	inline static int32_t get_offset_of_star3_8() { return static_cast<int32_t>(offsetof(ProgressBar_t959013686, ___star3_8)); }
	inline int32_t get_star3_8() const { return ___star3_8; }
	inline int32_t* get_address_of_star3_8() { return &___star3_8; }
	inline void set_star3_8(int32_t value)
	{
		___star3_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBAR_T959013686_H
#ifndef PROGRESSSTAR_T2895061392_H
#define PROGRESSSTAR_T2895061392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.ProgressStar
struct  ProgressStar_t2895061392  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image GameVanilla.Game.UI.ProgressStar::image
	Image_t2670269651 * ___image_2;
	// UnityEngine.Sprite GameVanilla.Game.UI.ProgressStar::onSprite
	Sprite_t280657092 * ___onSprite_3;
	// UnityEngine.Sprite GameVanilla.Game.UI.ProgressStar::offSprite
	Sprite_t280657092 * ___offSprite_4;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(ProgressStar_t2895061392, ___image_2)); }
	inline Image_t2670269651 * get_image_2() const { return ___image_2; }
	inline Image_t2670269651 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(Image_t2670269651 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier((&___image_2), value);
	}

	inline static int32_t get_offset_of_onSprite_3() { return static_cast<int32_t>(offsetof(ProgressStar_t2895061392, ___onSprite_3)); }
	inline Sprite_t280657092 * get_onSprite_3() const { return ___onSprite_3; }
	inline Sprite_t280657092 ** get_address_of_onSprite_3() { return &___onSprite_3; }
	inline void set_onSprite_3(Sprite_t280657092 * value)
	{
		___onSprite_3 = value;
		Il2CppCodeGenWriteBarrier((&___onSprite_3), value);
	}

	inline static int32_t get_offset_of_offSprite_4() { return static_cast<int32_t>(offsetof(ProgressStar_t2895061392, ___offSprite_4)); }
	inline Sprite_t280657092 * get_offSprite_4() const { return ___offSprite_4; }
	inline Sprite_t280657092 ** get_address_of_offSprite_4() { return &___offSprite_4; }
	inline void set_offSprite_4(Sprite_t280657092 * value)
	{
		___offSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___offSprite_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTAR_T2895061392_H
#ifndef REWARDEDADBUTTON_T2682102439_H
#define REWARDEDADBUTTON_T2682102439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.RewardedAdButton
struct  RewardedAdButton_t2682102439  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDADBUTTON_T2682102439_H
#ifndef SCORETEXT_T753417783_H
#define SCORETEXT_T753417783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.ScoreText
struct  ScoreText_t753417783  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform GameVanilla.Game.UI.ScoreText::rect
	RectTransform_t3704657025 * ___rect_2;
	// UnityEngine.UI.Text GameVanilla.Game.UI.ScoreText::text
	Text_t1901882714 * ___text_3;

public:
	inline static int32_t get_offset_of_rect_2() { return static_cast<int32_t>(offsetof(ScoreText_t753417783, ___rect_2)); }
	inline RectTransform_t3704657025 * get_rect_2() const { return ___rect_2; }
	inline RectTransform_t3704657025 ** get_address_of_rect_2() { return &___rect_2; }
	inline void set_rect_2(RectTransform_t3704657025 * value)
	{
		___rect_2 = value;
		Il2CppCodeGenWriteBarrier((&___rect_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(ScoreText_t753417783, ___text_3)); }
	inline Text_t1901882714 * get_text_3() const { return ___text_3; }
	inline Text_t1901882714 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_t1901882714 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCORETEXT_T753417783_H
#ifndef TILEPARTICLES_T3446409083_H
#define TILEPARTICLES_T3446409083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.TileParticles
struct  TileParticles_t3446409083  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem GameVanilla.Game.UI.TileParticles::fragmentParticles
	ParticleSystem_t1800779281 * ___fragmentParticles_2;

public:
	inline static int32_t get_offset_of_fragmentParticles_2() { return static_cast<int32_t>(offsetof(TileParticles_t3446409083, ___fragmentParticles_2)); }
	inline ParticleSystem_t1800779281 * get_fragmentParticles_2() const { return ___fragmentParticles_2; }
	inline ParticleSystem_t1800779281 ** get_address_of_fragmentParticles_2() { return &___fragmentParticles_2; }
	inline void set_fragmentParticles_2(ParticleSystem_t1800779281 * value)
	{
		___fragmentParticles_2 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentParticles_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEPARTICLES_T3446409083_H
#ifndef ICONPREVIEW_T486595085_H
#define ICONPREVIEW_T486595085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconPreview
struct  IconPreview_t486595085  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] IconPreview::icons
	SpriteU5BU5D_t2581906349* ___icons_2;
	// UnityEngine.GameObject IconPreview::icon
	GameObject_t1113636619 * ___icon_3;

public:
	inline static int32_t get_offset_of_icons_2() { return static_cast<int32_t>(offsetof(IconPreview_t486595085, ___icons_2)); }
	inline SpriteU5BU5D_t2581906349* get_icons_2() const { return ___icons_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_icons_2() { return &___icons_2; }
	inline void set_icons_2(SpriteU5BU5D_t2581906349* value)
	{
		___icons_2 = value;
		Il2CppCodeGenWriteBarrier((&___icons_2), value);
	}

	inline static int32_t get_offset_of_icon_3() { return static_cast<int32_t>(offsetof(IconPreview_t486595085, ___icon_3)); }
	inline GameObject_t1113636619 * get_icon_3() const { return ___icon_3; }
	inline GameObject_t1113636619 ** get_address_of_icon_3() { return &___icon_3; }
	inline void set_icon_3(GameObject_t1113636619 * value)
	{
		___icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___icon_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONPREVIEW_T486595085_H
#ifndef GOALUIELEMENT_T162174964_H
#define GOALUIELEMENT_T162174964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.UI.GoalUiElement
struct  GoalUiElement_t162174964  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image GameVanilla.Game.UI.GoalUiElement::image
	Image_t2670269651 * ___image_2;
	// UnityEngine.UI.Text GameVanilla.Game.UI.GoalUiElement::amountText
	Text_t1901882714 * ___amountText_3;
	// UnityEngine.UI.Image GameVanilla.Game.UI.GoalUiElement::tickImage
	Image_t2670269651 * ___tickImage_4;
	// UnityEngine.UI.Image GameVanilla.Game.UI.GoalUiElement::crossImage
	Image_t2670269651 * ___crossImage_5;
	// UnityEngine.ParticleSystem GameVanilla.Game.UI.GoalUiElement::shineParticles
	ParticleSystem_t1800779281 * ___shineParticles_6;
	// UnityEngine.ParticleSystem GameVanilla.Game.UI.GoalUiElement::starParticles
	ParticleSystem_t1800779281 * ___starParticles_7;
	// System.Boolean GameVanilla.Game.UI.GoalUiElement::<isCompleted>k__BackingField
	bool ___U3CisCompletedU3Ek__BackingField_8;
	// GameVanilla.Game.Common.Goal GameVanilla.Game.UI.GoalUiElement::currentGoal
	Goal_t1499691635 * ___currentGoal_9;
	// System.Int32 GameVanilla.Game.UI.GoalUiElement::targetAmount
	int32_t ___targetAmount_10;
	// System.Int32 GameVanilla.Game.UI.GoalUiElement::currentAmount
	int32_t ___currentAmount_11;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___image_2)); }
	inline Image_t2670269651 * get_image_2() const { return ___image_2; }
	inline Image_t2670269651 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(Image_t2670269651 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier((&___image_2), value);
	}

	inline static int32_t get_offset_of_amountText_3() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___amountText_3)); }
	inline Text_t1901882714 * get_amountText_3() const { return ___amountText_3; }
	inline Text_t1901882714 ** get_address_of_amountText_3() { return &___amountText_3; }
	inline void set_amountText_3(Text_t1901882714 * value)
	{
		___amountText_3 = value;
		Il2CppCodeGenWriteBarrier((&___amountText_3), value);
	}

	inline static int32_t get_offset_of_tickImage_4() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___tickImage_4)); }
	inline Image_t2670269651 * get_tickImage_4() const { return ___tickImage_4; }
	inline Image_t2670269651 ** get_address_of_tickImage_4() { return &___tickImage_4; }
	inline void set_tickImage_4(Image_t2670269651 * value)
	{
		___tickImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___tickImage_4), value);
	}

	inline static int32_t get_offset_of_crossImage_5() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___crossImage_5)); }
	inline Image_t2670269651 * get_crossImage_5() const { return ___crossImage_5; }
	inline Image_t2670269651 ** get_address_of_crossImage_5() { return &___crossImage_5; }
	inline void set_crossImage_5(Image_t2670269651 * value)
	{
		___crossImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___crossImage_5), value);
	}

	inline static int32_t get_offset_of_shineParticles_6() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___shineParticles_6)); }
	inline ParticleSystem_t1800779281 * get_shineParticles_6() const { return ___shineParticles_6; }
	inline ParticleSystem_t1800779281 ** get_address_of_shineParticles_6() { return &___shineParticles_6; }
	inline void set_shineParticles_6(ParticleSystem_t1800779281 * value)
	{
		___shineParticles_6 = value;
		Il2CppCodeGenWriteBarrier((&___shineParticles_6), value);
	}

	inline static int32_t get_offset_of_starParticles_7() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___starParticles_7)); }
	inline ParticleSystem_t1800779281 * get_starParticles_7() const { return ___starParticles_7; }
	inline ParticleSystem_t1800779281 ** get_address_of_starParticles_7() { return &___starParticles_7; }
	inline void set_starParticles_7(ParticleSystem_t1800779281 * value)
	{
		___starParticles_7 = value;
		Il2CppCodeGenWriteBarrier((&___starParticles_7), value);
	}

	inline static int32_t get_offset_of_U3CisCompletedU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___U3CisCompletedU3Ek__BackingField_8)); }
	inline bool get_U3CisCompletedU3Ek__BackingField_8() const { return ___U3CisCompletedU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CisCompletedU3Ek__BackingField_8() { return &___U3CisCompletedU3Ek__BackingField_8; }
	inline void set_U3CisCompletedU3Ek__BackingField_8(bool value)
	{
		___U3CisCompletedU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_currentGoal_9() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___currentGoal_9)); }
	inline Goal_t1499691635 * get_currentGoal_9() const { return ___currentGoal_9; }
	inline Goal_t1499691635 ** get_address_of_currentGoal_9() { return &___currentGoal_9; }
	inline void set_currentGoal_9(Goal_t1499691635 * value)
	{
		___currentGoal_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentGoal_9), value);
	}

	inline static int32_t get_offset_of_targetAmount_10() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___targetAmount_10)); }
	inline int32_t get_targetAmount_10() const { return ___targetAmount_10; }
	inline int32_t* get_address_of_targetAmount_10() { return &___targetAmount_10; }
	inline void set_targetAmount_10(int32_t value)
	{
		___targetAmount_10 = value;
	}

	inline static int32_t get_offset_of_currentAmount_11() { return static_cast<int32_t>(offsetof(GoalUiElement_t162174964, ___currentAmount_11)); }
	inline int32_t get_currentAmount_11() const { return ___currentAmount_11; }
	inline int32_t* get_address_of_currentAmount_11() { return &___currentAmount_11; }
	inline void set_currentAmount_11(int32_t value)
	{
		___currentAmount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOALUIELEMENT_T162174964_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (GoalUiElement_t162174964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[10] = 
{
	GoalUiElement_t162174964::get_offset_of_image_2(),
	GoalUiElement_t162174964::get_offset_of_amountText_3(),
	GoalUiElement_t162174964::get_offset_of_tickImage_4(),
	GoalUiElement_t162174964::get_offset_of_crossImage_5(),
	GoalUiElement_t162174964::get_offset_of_shineParticles_6(),
	GoalUiElement_t162174964::get_offset_of_starParticles_7(),
	GoalUiElement_t162174964::get_offset_of_U3CisCompletedU3Ek__BackingField_8(),
	GoalUiElement_t162174964::get_offset_of_currentGoal_9(),
	GoalUiElement_t162174964::get_offset_of_targetAmount_10(),
	GoalUiElement_t162174964::get_offset_of_currentAmount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (IapRow_t4269885083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[10] = 
{
	IapRow_t4269885083::get_offset_of_buyCoinsPopup_2(),
	IapRow_t4269885083::get_offset_of_mostPopular_3(),
	IapRow_t4269885083::get_offset_of_bestValue_4(),
	IapRow_t4269885083::get_offset_of_discount_5(),
	IapRow_t4269885083::get_offset_of_discountText_6(),
	IapRow_t4269885083::get_offset_of_numCoinsText_7(),
	IapRow_t4269885083::get_offset_of_priceText_8(),
	IapRow_t4269885083::get_offset_of_coinsImage_9(),
	IapRow_t4269885083::get_offset_of_coinIcons_10(),
	IapRow_t4269885083::get_offset_of_cachedItem_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (LevelAvatar_t2279482836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[5] = 
{
	LevelAvatar_t2279482836::get_offset_of_girlAvatarSprite_2(),
	LevelAvatar_t2279482836::get_offset_of_boyAvatarSprite_3(),
	LevelAvatar_t2279482836::get_offset_of_avatarImage_4(),
	LevelAvatar_t2279482836::get_offset_of_floating_5(),
	LevelAvatar_t2279482836::get_offset_of_runningTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (LevelButton_t1543636365), -1, sizeof(LevelButton_t1543636365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2703[12] = 
{
	LevelButton_t1543636365::get_offset_of_numLevel_2(),
	LevelButton_t1543636365::get_offset_of_currentButtonSprite_3(),
	LevelButton_t1543636365::get_offset_of_playedButtonSprite_4(),
	LevelButton_t1543636365::get_offset_of_lockedButtonSprite_5(),
	LevelButton_t1543636365::get_offset_of_yellowStarSprite_6(),
	LevelButton_t1543636365::get_offset_of_buttonImage_7(),
	LevelButton_t1543636365::get_offset_of_numLevelText_8(),
	LevelButton_t1543636365::get_offset_of_star1_9(),
	LevelButton_t1543636365::get_offset_of_star2_10(),
	LevelButton_t1543636365::get_offset_of_star3_11(),
	LevelButton_t1543636365::get_offset_of_shineAnimation_12(),
	LevelButton_t1543636365_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ProgressBar_t959013686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[7] = 
{
	ProgressBar_t959013686::get_offset_of_progressBarImage_2(),
	ProgressBar_t959013686::get_offset_of_star1Image_3(),
	ProgressBar_t959013686::get_offset_of_star2Image_4(),
	ProgressBar_t959013686::get_offset_of_star3Image_5(),
	ProgressBar_t959013686::get_offset_of_star1_6(),
	ProgressBar_t959013686::get_offset_of_star2_7(),
	ProgressBar_t959013686::get_offset_of_star3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (ProgressStar_t2895061392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[3] = 
{
	ProgressStar_t2895061392::get_offset_of_image_2(),
	ProgressStar_t2895061392::get_offset_of_onSprite_3(),
	ProgressStar_t2895061392::get_offset_of_offSprite_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (RewardedAdButton_t2682102439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (U3CHandleShowResultU3Ec__AnonStorey0_t2282433997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[1] = 
{
	U3CHandleShowResultU3Ec__AnonStorey0_t2282433997::get_offset_of_rewardCoins_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (ScoreText_t753417783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[2] = 
{
	ScoreText_t753417783::get_offset_of_rect_2(),
	ScoreText_t753417783::get_offset_of_text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (U3CSmoothMoveU3Ec__Iterator0_t1363484482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[8] = 
{
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_U3CstartPosU3E__0_0(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_U3CtU3E__0_1(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_time_2(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_pos_3(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_U24this_4(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_U24current_5(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_U24disposing_6(),
	U3CSmoothMoveU3Ec__Iterator0_t1363484482::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (U3CSmoothFadeU3Ec__Iterator1_t1888227318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[9] = 
{
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U3CstartColorU3E__0_0(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U3CtU3E__0_1(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_time_2(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U3CnewColorU3E__1_3(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_alpha_4(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U24this_5(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U24current_6(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U24disposing_7(),
	U3CSmoothFadeU3Ec__Iterator1_t1888227318::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (TileParticles_t3446409083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[1] = 
{
	TileParticles_t3446409083::get_offset_of_fragmentParticles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (IconPreview_t486595085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[2] = 
{
	IconPreview_t486595085::get_offset_of_icons_2(),
	IconPreview_t486595085::get_offset_of_icon_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (Animation_t615783283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[3] = 
{
	Animation_t615783283::get_offset_of_timelines_0(),
	Animation_t615783283::get_offset_of_duration_1(),
	Animation_t615783283::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (MixPose_t983230599)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[4] = 
{
	MixPose_t983230599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (MixDirection_t3149175205)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[3] = 
{
	MixDirection_t3149175205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (TimelineType_t3142998273)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2717[16] = 
{
	TimelineType_t3142998273::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (CurveTimeline_t3673209699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[5] = 
{
	0,
	0,
	0,
	0,
	CurveTimeline_t3673209699::get_offset_of_curves_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (RotateTimeline_t860518009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[6] = 
{
	0,
	0,
	0,
	0,
	RotateTimeline_t860518009::get_offset_of_boneIndex_9(),
	RotateTimeline_t860518009::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (TranslateTimeline_t2733426737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TranslateTimeline_t2733426737::get_offset_of_boneIndex_11(),
	TranslateTimeline_t2733426737::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (ScaleTimeline_t1810411048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (ShearTimeline_t3812859450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (ColorTimeline_t3754116780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ColorTimeline_t3754116780::get_offset_of_slotIndex_15(),
	ColorTimeline_t3754116780::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (TwoColorTimeline_t1834727230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TwoColorTimeline_t1834727230::get_offset_of_slotIndex_21(),
	TwoColorTimeline_t1834727230::get_offset_of_frames_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (AttachmentTimeline_t2048728856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[3] = 
{
	AttachmentTimeline_t2048728856::get_offset_of_slotIndex_0(),
	AttachmentTimeline_t2048728856::get_offset_of_frames_1(),
	AttachmentTimeline_t2048728856::get_offset_of_attachmentNames_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (DeformTimeline_t3552075840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[4] = 
{
	DeformTimeline_t3552075840::get_offset_of_slotIndex_5(),
	DeformTimeline_t3552075840::get_offset_of_frames_6(),
	DeformTimeline_t3552075840::get_offset_of_frameVertices_7(),
	DeformTimeline_t3552075840::get_offset_of_attachment_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (EventTimeline_t2341881094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[2] = 
{
	EventTimeline_t2341881094::get_offset_of_frames_0(),
	EventTimeline_t2341881094::get_offset_of_events_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (DrawOrderTimeline_t1092982325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[2] = 
{
	DrawOrderTimeline_t1092982325::get_offset_of_frames_0(),
	DrawOrderTimeline_t1092982325::get_offset_of_drawOrders_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (IkConstraintTimeline_t1094182104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	IkConstraintTimeline_t1094182104::get_offset_of_ikConstraintIndex_11(),
	IkConstraintTimeline_t1094182104::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (TransformConstraintTimeline_t410042030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TransformConstraintTimeline_t410042030::get_offset_of_transformConstraintIndex_15(),
	TransformConstraintTimeline_t410042030::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (PathConstraintPositionTimeline_t1963796833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[6] = 
{
	0,
	0,
	0,
	0,
	PathConstraintPositionTimeline_t1963796833::get_offset_of_pathConstraintIndex_9(),
	PathConstraintPositionTimeline_t1963796833::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (PathConstraintSpacingTimeline_t1125597164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (PathConstraintMixTimeline_t534890125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	PathConstraintMixTimeline_t534890125::get_offset_of_pathConstraintIndex_11(),
	PathConstraintMixTimeline_t534890125::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (AnimationState_t3637309382), -1, sizeof(AnimationState_t3637309382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2734[20] = 
{
	AnimationState_t3637309382_StaticFields::get_offset_of_EmptyAnimation_0(),
	0,
	0,
	0,
	0,
	AnimationState_t3637309382::get_offset_of_data_5(),
	AnimationState_t3637309382::get_offset_of_trackEntryPool_6(),
	AnimationState_t3637309382::get_offset_of_tracks_7(),
	AnimationState_t3637309382::get_offset_of_events_8(),
	AnimationState_t3637309382::get_offset_of_queue_9(),
	AnimationState_t3637309382::get_offset_of_propertyIDs_10(),
	AnimationState_t3637309382::get_offset_of_mixingTo_11(),
	AnimationState_t3637309382::get_offset_of_animationsChanged_12(),
	AnimationState_t3637309382::get_offset_of_timeScale_13(),
	AnimationState_t3637309382::get_offset_of_Interrupt_14(),
	AnimationState_t3637309382::get_offset_of_End_15(),
	AnimationState_t3637309382::get_offset_of_Dispose_16(),
	AnimationState_t3637309382::get_offset_of_Complete_17(),
	AnimationState_t3637309382::get_offset_of_Start_18(),
	AnimationState_t3637309382::get_offset_of_Event_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (TrackEntryDelegate_t363257942), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (TrackEntryEventDelegate_t1653995044), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (TrackEntry_t1316488441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[32] = 
{
	TrackEntry_t1316488441::get_offset_of_animation_0(),
	TrackEntry_t1316488441::get_offset_of_next_1(),
	TrackEntry_t1316488441::get_offset_of_mixingFrom_2(),
	TrackEntry_t1316488441::get_offset_of_trackIndex_3(),
	TrackEntry_t1316488441::get_offset_of_loop_4(),
	TrackEntry_t1316488441::get_offset_of_eventThreshold_5(),
	TrackEntry_t1316488441::get_offset_of_attachmentThreshold_6(),
	TrackEntry_t1316488441::get_offset_of_drawOrderThreshold_7(),
	TrackEntry_t1316488441::get_offset_of_animationStart_8(),
	TrackEntry_t1316488441::get_offset_of_animationEnd_9(),
	TrackEntry_t1316488441::get_offset_of_animationLast_10(),
	TrackEntry_t1316488441::get_offset_of_nextAnimationLast_11(),
	TrackEntry_t1316488441::get_offset_of_delay_12(),
	TrackEntry_t1316488441::get_offset_of_trackTime_13(),
	TrackEntry_t1316488441::get_offset_of_trackLast_14(),
	TrackEntry_t1316488441::get_offset_of_nextTrackLast_15(),
	TrackEntry_t1316488441::get_offset_of_trackEnd_16(),
	TrackEntry_t1316488441::get_offset_of_timeScale_17(),
	TrackEntry_t1316488441::get_offset_of_alpha_18(),
	TrackEntry_t1316488441::get_offset_of_mixTime_19(),
	TrackEntry_t1316488441::get_offset_of_mixDuration_20(),
	TrackEntry_t1316488441::get_offset_of_interruptAlpha_21(),
	TrackEntry_t1316488441::get_offset_of_totalAlpha_22(),
	TrackEntry_t1316488441::get_offset_of_timelineData_23(),
	TrackEntry_t1316488441::get_offset_of_timelineDipMix_24(),
	TrackEntry_t1316488441::get_offset_of_timelinesRotation_25(),
	TrackEntry_t1316488441::get_offset_of_Interrupt_26(),
	TrackEntry_t1316488441::get_offset_of_End_27(),
	TrackEntry_t1316488441::get_offset_of_Dispose_28(),
	TrackEntry_t1316488441::get_offset_of_Complete_29(),
	TrackEntry_t1316488441::get_offset_of_Start_30(),
	TrackEntry_t1316488441::get_offset_of_Event_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (EventQueue_t808091267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	EventQueue_t808091267::get_offset_of_eventQueueEntries_0(),
	EventQueue_t808091267::get_offset_of_drainDisabled_1(),
	EventQueue_t808091267::get_offset_of_state_2(),
	EventQueue_t808091267::get_offset_of_trackEntryPool_3(),
	EventQueue_t808091267::get_offset_of_AnimationsChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (EventQueueEntry_t351831961)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[3] = 
{
	EventQueueEntry_t351831961::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t351831961::get_offset_of_entry_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t351831961::get_offset_of_e_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (EventType_t1835192406)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[7] = 
{
	EventType_t1835192406::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (AnimationStateData_t3010651567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[3] = 
{
	AnimationStateData_t3010651567::get_offset_of_skeletonData_0(),
	AnimationStateData_t3010651567::get_offset_of_animationToMixTime_1(),
	AnimationStateData_t3010651567::get_offset_of_defaultMix_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (AnimationPair_t1784808777)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[2] = 
{
	AnimationPair_t1784808777::get_offset_of_a1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationPair_t1784808777::get_offset_of_a2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (AnimationPairComparer_t175378255), -1, sizeof(AnimationPairComparer_t175378255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[1] = 
{
	AnimationPairComparer_t175378255_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (Atlas_t4040192941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[3] = 
{
	Atlas_t4040192941::get_offset_of_pages_0(),
	Atlas_t4040192941::get_offset_of_regions_1(),
	Atlas_t4040192941::get_offset_of_textureLoader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (Format_t2262486444)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[8] = 
{
	Format_t2262486444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (TextureFilter_t1610186802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[8] = 
{
	TextureFilter_t1610186802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (TextureWrap_t2819935362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2749[4] = 
{
	TextureWrap_t2819935362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (AtlasPage_t4077017671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[9] = 
{
	AtlasPage_t4077017671::get_offset_of_name_0(),
	AtlasPage_t4077017671::get_offset_of_format_1(),
	AtlasPage_t4077017671::get_offset_of_minFilter_2(),
	AtlasPage_t4077017671::get_offset_of_magFilter_3(),
	AtlasPage_t4077017671::get_offset_of_uWrap_4(),
	AtlasPage_t4077017671::get_offset_of_vWrap_5(),
	AtlasPage_t4077017671::get_offset_of_rendererObject_6(),
	AtlasPage_t4077017671::get_offset_of_width_7(),
	AtlasPage_t4077017671::get_offset_of_height_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (AtlasRegion_t13903284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[18] = 
{
	AtlasRegion_t13903284::get_offset_of_page_0(),
	AtlasRegion_t13903284::get_offset_of_name_1(),
	AtlasRegion_t13903284::get_offset_of_x_2(),
	AtlasRegion_t13903284::get_offset_of_y_3(),
	AtlasRegion_t13903284::get_offset_of_width_4(),
	AtlasRegion_t13903284::get_offset_of_height_5(),
	AtlasRegion_t13903284::get_offset_of_u_6(),
	AtlasRegion_t13903284::get_offset_of_v_7(),
	AtlasRegion_t13903284::get_offset_of_u2_8(),
	AtlasRegion_t13903284::get_offset_of_v2_9(),
	AtlasRegion_t13903284::get_offset_of_offsetX_10(),
	AtlasRegion_t13903284::get_offset_of_offsetY_11(),
	AtlasRegion_t13903284::get_offset_of_originalWidth_12(),
	AtlasRegion_t13903284::get_offset_of_originalHeight_13(),
	AtlasRegion_t13903284::get_offset_of_index_14(),
	AtlasRegion_t13903284::get_offset_of_rotate_15(),
	AtlasRegion_t13903284::get_offset_of_splits_16(),
	AtlasRegion_t13903284::get_offset_of_pads_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (AtlasAttachmentLoader_t3966790320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[1] = 
{
	AtlasAttachmentLoader_t3966790320::get_offset_of_atlasArray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (Attachment_t3043756552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[1] = 
{
	Attachment_t3043756552::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (AttachmentType_t1460157544)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2757[8] = 
{
	AttachmentType_t1460157544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (BoundingBoxAttachment_t2797506510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (ClippingAttachment_t2586274570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	ClippingAttachment_t2586274570::get_offset_of_endSlot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (MeshAttachment_t1975337962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[26] = 
{
	MeshAttachment_t1975337962::get_offset_of_regionOffsetX_7(),
	MeshAttachment_t1975337962::get_offset_of_regionOffsetY_8(),
	MeshAttachment_t1975337962::get_offset_of_regionWidth_9(),
	MeshAttachment_t1975337962::get_offset_of_regionHeight_10(),
	MeshAttachment_t1975337962::get_offset_of_regionOriginalWidth_11(),
	MeshAttachment_t1975337962::get_offset_of_regionOriginalHeight_12(),
	MeshAttachment_t1975337962::get_offset_of_parentMesh_13(),
	MeshAttachment_t1975337962::get_offset_of_uvs_14(),
	MeshAttachment_t1975337962::get_offset_of_regionUVs_15(),
	MeshAttachment_t1975337962::get_offset_of_triangles_16(),
	MeshAttachment_t1975337962::get_offset_of_r_17(),
	MeshAttachment_t1975337962::get_offset_of_g_18(),
	MeshAttachment_t1975337962::get_offset_of_b_19(),
	MeshAttachment_t1975337962::get_offset_of_a_20(),
	MeshAttachment_t1975337962::get_offset_of_hulllength_21(),
	MeshAttachment_t1975337962::get_offset_of_inheritDeform_22(),
	MeshAttachment_t1975337962::get_offset_of_U3CPathU3Ek__BackingField_23(),
	MeshAttachment_t1975337962::get_offset_of_U3CRendererObjectU3Ek__BackingField_24(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionUU3Ek__BackingField_25(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionVU3Ek__BackingField_26(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionU2U3Ek__BackingField_27(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionV2U3Ek__BackingField_28(),
	MeshAttachment_t1975337962::get_offset_of_U3CRegionRotateU3Ek__BackingField_29(),
	MeshAttachment_t1975337962::get_offset_of_U3CEdgesU3Ek__BackingField_30(),
	MeshAttachment_t1975337962::get_offset_of_U3CWidthU3Ek__BackingField_31(),
	MeshAttachment_t1975337962::get_offset_of_U3CHeightU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (PathAttachment_t3565151060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[3] = 
{
	PathAttachment_t3565151060::get_offset_of_lengths_7(),
	PathAttachment_t3565151060::get_offset_of_closed_8(),
	PathAttachment_t3565151060::get_offset_of_constantSpeed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (PointAttachment_t2275020146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[3] = 
{
	PointAttachment_t2275020146::get_offset_of_x_1(),
	PointAttachment_t2275020146::get_offset_of_y_2(),
	PointAttachment_t2275020146::get_offset_of_rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (RegionAttachment_t1770147391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RegionAttachment_t1770147391::get_offset_of_x_9(),
	RegionAttachment_t1770147391::get_offset_of_y_10(),
	RegionAttachment_t1770147391::get_offset_of_rotation_11(),
	RegionAttachment_t1770147391::get_offset_of_scaleX_12(),
	RegionAttachment_t1770147391::get_offset_of_scaleY_13(),
	RegionAttachment_t1770147391::get_offset_of_width_14(),
	RegionAttachment_t1770147391::get_offset_of_height_15(),
	RegionAttachment_t1770147391::get_offset_of_regionOffsetX_16(),
	RegionAttachment_t1770147391::get_offset_of_regionOffsetY_17(),
	RegionAttachment_t1770147391::get_offset_of_regionWidth_18(),
	RegionAttachment_t1770147391::get_offset_of_regionHeight_19(),
	RegionAttachment_t1770147391::get_offset_of_regionOriginalWidth_20(),
	RegionAttachment_t1770147391::get_offset_of_regionOriginalHeight_21(),
	RegionAttachment_t1770147391::get_offset_of_offset_22(),
	RegionAttachment_t1770147391::get_offset_of_uvs_23(),
	RegionAttachment_t1770147391::get_offset_of_r_24(),
	RegionAttachment_t1770147391::get_offset_of_g_25(),
	RegionAttachment_t1770147391::get_offset_of_b_26(),
	RegionAttachment_t1770147391::get_offset_of_a_27(),
	RegionAttachment_t1770147391::get_offset_of_U3CPathU3Ek__BackingField_28(),
	RegionAttachment_t1770147391::get_offset_of_U3CRendererObjectU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (VertexAttachment_t4074366829), -1, sizeof(VertexAttachment_t4074366829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2764[6] = 
{
	VertexAttachment_t4074366829_StaticFields::get_offset_of_nextID_1(),
	VertexAttachment_t4074366829_StaticFields::get_offset_of_nextIdLock_2(),
	VertexAttachment_t4074366829::get_offset_of_id_3(),
	VertexAttachment_t4074366829::get_offset_of_bones_4(),
	VertexAttachment_t4074366829::get_offset_of_vertices_5(),
	VertexAttachment_t4074366829::get_offset_of_worldVerticesLength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (BlendMode_t358635744)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2765[5] = 
{
	BlendMode_t358635744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (Bone_t1086356328), -1, sizeof(Bone_t1086356328_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2766[27] = 
{
	Bone_t1086356328_StaticFields::get_offset_of_yDown_0(),
	Bone_t1086356328::get_offset_of_data_1(),
	Bone_t1086356328::get_offset_of_skeleton_2(),
	Bone_t1086356328::get_offset_of_parent_3(),
	Bone_t1086356328::get_offset_of_children_4(),
	Bone_t1086356328::get_offset_of_x_5(),
	Bone_t1086356328::get_offset_of_y_6(),
	Bone_t1086356328::get_offset_of_rotation_7(),
	Bone_t1086356328::get_offset_of_scaleX_8(),
	Bone_t1086356328::get_offset_of_scaleY_9(),
	Bone_t1086356328::get_offset_of_shearX_10(),
	Bone_t1086356328::get_offset_of_shearY_11(),
	Bone_t1086356328::get_offset_of_ax_12(),
	Bone_t1086356328::get_offset_of_ay_13(),
	Bone_t1086356328::get_offset_of_arotation_14(),
	Bone_t1086356328::get_offset_of_ascaleX_15(),
	Bone_t1086356328::get_offset_of_ascaleY_16(),
	Bone_t1086356328::get_offset_of_ashearX_17(),
	Bone_t1086356328::get_offset_of_ashearY_18(),
	Bone_t1086356328::get_offset_of_appliedValid_19(),
	Bone_t1086356328::get_offset_of_a_20(),
	Bone_t1086356328::get_offset_of_b_21(),
	Bone_t1086356328::get_offset_of_worldX_22(),
	Bone_t1086356328::get_offset_of_c_23(),
	Bone_t1086356328::get_offset_of_d_24(),
	Bone_t1086356328::get_offset_of_worldY_25(),
	Bone_t1086356328::get_offset_of_sorted_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (BoneData_t3130174490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[12] = 
{
	BoneData_t3130174490::get_offset_of_index_0(),
	BoneData_t3130174490::get_offset_of_name_1(),
	BoneData_t3130174490::get_offset_of_parent_2(),
	BoneData_t3130174490::get_offset_of_length_3(),
	BoneData_t3130174490::get_offset_of_x_4(),
	BoneData_t3130174490::get_offset_of_y_5(),
	BoneData_t3130174490::get_offset_of_rotation_6(),
	BoneData_t3130174490::get_offset_of_scaleX_7(),
	BoneData_t3130174490::get_offset_of_scaleY_8(),
	BoneData_t3130174490::get_offset_of_shearX_9(),
	BoneData_t3130174490::get_offset_of_shearY_10(),
	BoneData_t3130174490::get_offset_of_transformMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (TransformMode_t614987360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2768[6] = 
{
	TransformMode_t614987360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (Event_t1378573841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[5] = 
{
	Event_t1378573841::get_offset_of_data_0(),
	Event_t1378573841::get_offset_of_time_1(),
	Event_t1378573841::get_offset_of_intValue_2(),
	Event_t1378573841::get_offset_of_floatValue_3(),
	Event_t1378573841::get_offset_of_stringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (EventData_t724759987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[4] = 
{
	EventData_t724759987::get_offset_of_name_0(),
	EventData_t724759987::get_offset_of_U3CIntU3Ek__BackingField_1(),
	EventData_t724759987::get_offset_of_U3CFloatU3Ek__BackingField_2(),
	EventData_t724759987::get_offset_of_U3CStringU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (IkConstraint_t1675190269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[5] = 
{
	IkConstraint_t1675190269::get_offset_of_data_0(),
	IkConstraint_t1675190269::get_offset_of_bones_1(),
	IkConstraint_t1675190269::get_offset_of_target_2(),
	IkConstraint_t1675190269::get_offset_of_mix_3(),
	IkConstraint_t1675190269::get_offset_of_bendDirection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (IkConstraintData_t459120129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[6] = 
{
	IkConstraintData_t459120129::get_offset_of_name_0(),
	IkConstraintData_t459120129::get_offset_of_order_1(),
	IkConstraintData_t459120129::get_offset_of_bones_2(),
	IkConstraintData_t459120129::get_offset_of_target_3(),
	IkConstraintData_t459120129::get_offset_of_bendDirection_4(),
	IkConstraintData_t459120129::get_offset_of_mix_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (Json_t1494079119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (Lexer_t3897031801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[6] = 
{
	Lexer_t3897031801::get_offset_of_U3ClineNumberU3Ek__BackingField_0(),
	Lexer_t3897031801::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	Lexer_t3897031801::get_offset_of_json_2(),
	Lexer_t3897031801::get_offset_of_index_3(),
	Lexer_t3897031801::get_offset_of_success_4(),
	Lexer_t3897031801::get_offset_of_stringBuffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (Token_t2243639121)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2779[13] = 
{
	Token_t2243639121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (JsonDecoder_t2143190644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[3] = 
{
	JsonDecoder_t2143190644::get_offset_of_U3CerrorMessageU3Ek__BackingField_0(),
	JsonDecoder_t2143190644::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	JsonDecoder_t2143190644::get_offset_of_lexer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (MathUtils_t3604673275), -1, sizeof(MathUtils_t3604673275_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2781[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MathUtils_t3604673275_StaticFields::get_offset_of_sin_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (PathConstraint_t980854910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[17] = 
{
	0,
	0,
	0,
	0,
	PathConstraint_t980854910::get_offset_of_data_4(),
	PathConstraint_t980854910::get_offset_of_bones_5(),
	PathConstraint_t980854910::get_offset_of_target_6(),
	PathConstraint_t980854910::get_offset_of_position_7(),
	PathConstraint_t980854910::get_offset_of_spacing_8(),
	PathConstraint_t980854910::get_offset_of_rotateMix_9(),
	PathConstraint_t980854910::get_offset_of_translateMix_10(),
	PathConstraint_t980854910::get_offset_of_spaces_11(),
	PathConstraint_t980854910::get_offset_of_positions_12(),
	PathConstraint_t980854910::get_offset_of_world_13(),
	PathConstraint_t980854910::get_offset_of_curves_14(),
	PathConstraint_t980854910::get_offset_of_lengths_15(),
	PathConstraint_t980854910::get_offset_of_segments_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (PathConstraintData_t981297034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[12] = 
{
	PathConstraintData_t981297034::get_offset_of_name_0(),
	PathConstraintData_t981297034::get_offset_of_order_1(),
	PathConstraintData_t981297034::get_offset_of_bones_2(),
	PathConstraintData_t981297034::get_offset_of_target_3(),
	PathConstraintData_t981297034::get_offset_of_positionMode_4(),
	PathConstraintData_t981297034::get_offset_of_spacingMode_5(),
	PathConstraintData_t981297034::get_offset_of_rotateMode_6(),
	PathConstraintData_t981297034::get_offset_of_offsetRotation_7(),
	PathConstraintData_t981297034::get_offset_of_position_8(),
	PathConstraintData_t981297034::get_offset_of_spacing_9(),
	PathConstraintData_t981297034::get_offset_of_rotateMix_10(),
	PathConstraintData_t981297034::get_offset_of_translateMix_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (PositionMode_t2435325583)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2784[3] = 
{
	PositionMode_t2435325583::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (SpacingMode_t3020059698)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2785[4] = 
{
	SpacingMode_t3020059698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (RotateMode_t2880286220)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2786[4] = 
{
	RotateMode_t2880286220::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (Skeleton_t3686076450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[19] = 
{
	Skeleton_t3686076450::get_offset_of_data_0(),
	Skeleton_t3686076450::get_offset_of_bones_1(),
	Skeleton_t3686076450::get_offset_of_slots_2(),
	Skeleton_t3686076450::get_offset_of_drawOrder_3(),
	Skeleton_t3686076450::get_offset_of_ikConstraints_4(),
	Skeleton_t3686076450::get_offset_of_transformConstraints_5(),
	Skeleton_t3686076450::get_offset_of_pathConstraints_6(),
	Skeleton_t3686076450::get_offset_of_updateCache_7(),
	Skeleton_t3686076450::get_offset_of_updateCacheReset_8(),
	Skeleton_t3686076450::get_offset_of_skin_9(),
	Skeleton_t3686076450::get_offset_of_r_10(),
	Skeleton_t3686076450::get_offset_of_g_11(),
	Skeleton_t3686076450::get_offset_of_b_12(),
	Skeleton_t3686076450::get_offset_of_a_13(),
	Skeleton_t3686076450::get_offset_of_time_14(),
	Skeleton_t3686076450::get_offset_of_flipX_15(),
	Skeleton_t3686076450::get_offset_of_flipY_16(),
	Skeleton_t3686076450::get_offset_of_x_17(),
	Skeleton_t3686076450::get_offset_of_y_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (SkeletonBinary_t1796686580), -1, sizeof(SkeletonBinary_t1796686580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2788[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SkeletonBinary_t1796686580::get_offset_of_U3CScaleU3Ek__BackingField_13(),
	SkeletonBinary_t1796686580::get_offset_of_attachmentLoader_14(),
	SkeletonBinary_t1796686580::get_offset_of_buffer_15(),
	SkeletonBinary_t1796686580::get_offset_of_linkedMeshes_16(),
	SkeletonBinary_t1796686580_StaticFields::get_offset_of_TransformModeValues_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (Vertices_t807797978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[2] = 
{
	Vertices_t807797978::get_offset_of_bones_0(),
	Vertices_t807797978::get_offset_of_vertices_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (SkeletonBounds_t352077915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[7] = 
{
	SkeletonBounds_t352077915::get_offset_of_polygonPool_0(),
	SkeletonBounds_t352077915::get_offset_of_minX_1(),
	SkeletonBounds_t352077915::get_offset_of_minY_2(),
	SkeletonBounds_t352077915::get_offset_of_maxX_3(),
	SkeletonBounds_t352077915::get_offset_of_maxY_4(),
	SkeletonBounds_t352077915::get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5(),
	SkeletonBounds_t352077915::get_offset_of_U3CPolygonsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (Polygon_t3315116257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[2] = 
{
	Polygon_t3315116257::get_offset_of_U3CVerticesU3Ek__BackingField_0(),
	Polygon_t3315116257::get_offset_of_U3CCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (SkeletonClipping_t1669006083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[9] = 
{
	SkeletonClipping_t1669006083::get_offset_of_triangulator_0(),
	SkeletonClipping_t1669006083::get_offset_of_clippingPolygon_1(),
	SkeletonClipping_t1669006083::get_offset_of_clipOutput_2(),
	SkeletonClipping_t1669006083::get_offset_of_clippedVertices_3(),
	SkeletonClipping_t1669006083::get_offset_of_clippedTriangles_4(),
	SkeletonClipping_t1669006083::get_offset_of_clippedUVs_5(),
	SkeletonClipping_t1669006083::get_offset_of_scratch_6(),
	SkeletonClipping_t1669006083::get_offset_of_clipAttachment_7(),
	SkeletonClipping_t1669006083::get_offset_of_clippingPolygons_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (SkeletonData_t2032710716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[16] = 
{
	SkeletonData_t2032710716::get_offset_of_name_0(),
	SkeletonData_t2032710716::get_offset_of_bones_1(),
	SkeletonData_t2032710716::get_offset_of_slots_2(),
	SkeletonData_t2032710716::get_offset_of_skins_3(),
	SkeletonData_t2032710716::get_offset_of_defaultSkin_4(),
	SkeletonData_t2032710716::get_offset_of_events_5(),
	SkeletonData_t2032710716::get_offset_of_animations_6(),
	SkeletonData_t2032710716::get_offset_of_ikConstraints_7(),
	SkeletonData_t2032710716::get_offset_of_transformConstraints_8(),
	SkeletonData_t2032710716::get_offset_of_pathConstraints_9(),
	SkeletonData_t2032710716::get_offset_of_width_10(),
	SkeletonData_t2032710716::get_offset_of_height_11(),
	SkeletonData_t2032710716::get_offset_of_version_12(),
	SkeletonData_t2032710716::get_offset_of_hash_13(),
	SkeletonData_t2032710716::get_offset_of_fps_14(),
	SkeletonData_t2032710716::get_offset_of_imagesPath_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (SkeletonJson_t4049771867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[3] = 
{
	SkeletonJson_t4049771867::get_offset_of_U3CScaleU3Ek__BackingField_0(),
	SkeletonJson_t4049771867::get_offset_of_attachmentLoader_1(),
	SkeletonJson_t4049771867::get_offset_of_linkedMeshes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (LinkedMesh_t1696919459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[4] = 
{
	LinkedMesh_t1696919459::get_offset_of_parent_0(),
	LinkedMesh_t1696919459::get_offset_of_skin_1(),
	LinkedMesh_t1696919459::get_offset_of_slotIndex_2(),
	LinkedMesh_t1696919459::get_offset_of_mesh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (Skin_t1174584606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[2] = 
{
	Skin_t1174584606::get_offset_of_name_0(),
	Skin_t1174584606::get_offset_of_attachments_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (AttachmentKeyTuple_t1548106539)+ sizeof (RuntimeObject), sizeof(AttachmentKeyTuple_t1548106539_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2797[3] = 
{
	AttachmentKeyTuple_t1548106539::get_offset_of_slotIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_t1548106539::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_t1548106539::get_offset_of_nameHashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (AttachmentKeyTupleComparer_t1167996044), -1, sizeof(AttachmentKeyTupleComparer_t1167996044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2798[1] = 
{
	AttachmentKeyTupleComparer_t1167996044_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (Slot_t3514940700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[13] = 
{
	Slot_t3514940700::get_offset_of_data_0(),
	Slot_t3514940700::get_offset_of_bone_1(),
	Slot_t3514940700::get_offset_of_r_2(),
	Slot_t3514940700::get_offset_of_g_3(),
	Slot_t3514940700::get_offset_of_b_4(),
	Slot_t3514940700::get_offset_of_a_5(),
	Slot_t3514940700::get_offset_of_r2_6(),
	Slot_t3514940700::get_offset_of_g2_7(),
	Slot_t3514940700::get_offset_of_b2_8(),
	Slot_t3514940700::get_offset_of_hasSecondColor_9(),
	Slot_t3514940700::get_offset_of_attachment_10(),
	Slot_t3514940700::get_offset_of_attachmentTime_11(),
	Slot_t3514940700::get_offset_of_attachmentVertices_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
