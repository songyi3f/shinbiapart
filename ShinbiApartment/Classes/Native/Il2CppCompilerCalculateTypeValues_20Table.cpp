﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t3995630009;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t2867327688;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t64614563;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t3256600500;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t3111972472;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t3587542510;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t1977848392;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t972960537;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t3277009892;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t2311673543;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t2886331738;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t2950825503;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t955952873;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t3373214253;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t3912835512;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t1475332338;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t2658898854;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t231414508;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// DG.Tweening.Tween
struct Tween_t2342918553;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t1327982029;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_t2329214678;
// System.String
struct String_t;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t1320466404;
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t3186308008;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t3814993295;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// DG.Tweening.Plugins.Core.ITweenPlugin
struct ITweenPlugin_t1960132748;
// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin>
struct Dictionary_2_t109512516;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_t2053048079;
// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_t2708327777;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t1567961855;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t2613982196;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t3190347560;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t1121741130;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t3867320123;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t521873611;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t3491343620;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t2019268878;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2475741330;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t384203932;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t830933145;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;




#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXECUTEEVENTS_T3484638744_H
#define EXECUTEEVENTS_T3484638744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t3484638744  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t3484638744_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t3995630009 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t2867327688 * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t64614563 * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_t3256600500 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t3111972472 * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_t3587542510 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t1977848392 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t972960537 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t3277009892 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t2311673543 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t2886331738 * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_t2950825503 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t955952873 * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t3373214253 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_t3912835512 * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t1475332338 * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_t2658898854 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_t231414508 * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t777473367 * ___s_InternalTransformList_18;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache0
	EventFunction_1_t3995630009 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache1
	EventFunction_1_t2867327688 * ___U3CU3Ef__mgU24cache1_20;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache2
	EventFunction_1_t64614563 * ___U3CU3Ef__mgU24cache2_21;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache3
	EventFunction_1_t3256600500 * ___U3CU3Ef__mgU24cache3_22;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache4
	EventFunction_1_t3111972472 * ___U3CU3Ef__mgU24cache4_23;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache5
	EventFunction_1_t3587542510 * ___U3CU3Ef__mgU24cache5_24;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache6
	EventFunction_1_t1977848392 * ___U3CU3Ef__mgU24cache6_25;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache7
	EventFunction_1_t972960537 * ___U3CU3Ef__mgU24cache7_26;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache8
	EventFunction_1_t3277009892 * ___U3CU3Ef__mgU24cache8_27;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache9
	EventFunction_1_t2311673543 * ___U3CU3Ef__mgU24cache9_28;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheA
	EventFunction_1_t2886331738 * ___U3CU3Ef__mgU24cacheA_29;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheB
	EventFunction_1_t2950825503 * ___U3CU3Ef__mgU24cacheB_30;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheC
	EventFunction_1_t955952873 * ___U3CU3Ef__mgU24cacheC_31;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheD
	EventFunction_1_t3373214253 * ___U3CU3Ef__mgU24cacheD_32;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheE
	EventFunction_1_t3912835512 * ___U3CU3Ef__mgU24cacheE_33;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheF
	EventFunction_1_t1475332338 * ___U3CU3Ef__mgU24cacheF_34;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache10
	EventFunction_1_t2658898854 * ___U3CU3Ef__mgU24cache10_35;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t3995630009 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t3995630009 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t3995630009 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerEnterHandler_0), value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t2867327688 * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t2867327688 ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t2867327688 * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerExitHandler_1), value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t64614563 * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t64614563 ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t64614563 * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerDownHandler_2), value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_t3256600500 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_t3256600500 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_t3256600500 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerUpHandler_3), value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t3111972472 * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t3111972472 ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t3111972472 * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerClickHandler_4), value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_t3587542510 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_t3587542510 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_t3587542510 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_InitializePotentialDragHandler_5), value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t1977848392 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t1977848392 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t1977848392 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_BeginDragHandler_6), value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t972960537 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t972960537 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t972960537 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_DragHandler_7), value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t3277009892 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t3277009892 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t3277009892 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EndDragHandler_8), value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t2311673543 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t2311673543 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t2311673543 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_DropHandler_9), value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t2886331738 * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t2886331738 ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t2886331738 * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ScrollHandler_10), value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_t2950825503 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_t2950825503 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_t2950825503 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateSelectedHandler_11), value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t955952873 * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t955952873 ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t955952873 * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_SelectHandler_12), value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t3373214253 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t3373214253 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t3373214253 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_DeselectHandler_13), value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_t3912835512 * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_t3912835512 ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_t3912835512 * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_MoveHandler_14), value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t1475332338 * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t1475332338 ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t1475332338 * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubmitHandler_15), value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_t2658898854 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_t2658898854 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_t2658898854 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelHandler_16), value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_t231414508 * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_t231414508 ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_t231414508 * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_HandlerListPool_17), value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t777473367 * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t777473367 ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t777473367 * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalTransformList_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline EventFunction_1_t3995630009 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline EventFunction_1_t3995630009 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(EventFunction_1_t3995630009 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline EventFunction_1_t2867327688 * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline EventFunction_1_t2867327688 ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(EventFunction_1_t2867327688 * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_21() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache2_21)); }
	inline EventFunction_1_t64614563 * get_U3CU3Ef__mgU24cache2_21() const { return ___U3CU3Ef__mgU24cache2_21; }
	inline EventFunction_1_t64614563 ** get_address_of_U3CU3Ef__mgU24cache2_21() { return &___U3CU3Ef__mgU24cache2_21; }
	inline void set_U3CU3Ef__mgU24cache2_21(EventFunction_1_t64614563 * value)
	{
		___U3CU3Ef__mgU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_22() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache3_22)); }
	inline EventFunction_1_t3256600500 * get_U3CU3Ef__mgU24cache3_22() const { return ___U3CU3Ef__mgU24cache3_22; }
	inline EventFunction_1_t3256600500 ** get_address_of_U3CU3Ef__mgU24cache3_22() { return &___U3CU3Ef__mgU24cache3_22; }
	inline void set_U3CU3Ef__mgU24cache3_22(EventFunction_1_t3256600500 * value)
	{
		___U3CU3Ef__mgU24cache3_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_23() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache4_23)); }
	inline EventFunction_1_t3111972472 * get_U3CU3Ef__mgU24cache4_23() const { return ___U3CU3Ef__mgU24cache4_23; }
	inline EventFunction_1_t3111972472 ** get_address_of_U3CU3Ef__mgU24cache4_23() { return &___U3CU3Ef__mgU24cache4_23; }
	inline void set_U3CU3Ef__mgU24cache4_23(EventFunction_1_t3111972472 * value)
	{
		___U3CU3Ef__mgU24cache4_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_24() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache5_24)); }
	inline EventFunction_1_t3587542510 * get_U3CU3Ef__mgU24cache5_24() const { return ___U3CU3Ef__mgU24cache5_24; }
	inline EventFunction_1_t3587542510 ** get_address_of_U3CU3Ef__mgU24cache5_24() { return &___U3CU3Ef__mgU24cache5_24; }
	inline void set_U3CU3Ef__mgU24cache5_24(EventFunction_1_t3587542510 * value)
	{
		___U3CU3Ef__mgU24cache5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_25() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache6_25)); }
	inline EventFunction_1_t1977848392 * get_U3CU3Ef__mgU24cache6_25() const { return ___U3CU3Ef__mgU24cache6_25; }
	inline EventFunction_1_t1977848392 ** get_address_of_U3CU3Ef__mgU24cache6_25() { return &___U3CU3Ef__mgU24cache6_25; }
	inline void set_U3CU3Ef__mgU24cache6_25(EventFunction_1_t1977848392 * value)
	{
		___U3CU3Ef__mgU24cache6_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_26() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache7_26)); }
	inline EventFunction_1_t972960537 * get_U3CU3Ef__mgU24cache7_26() const { return ___U3CU3Ef__mgU24cache7_26; }
	inline EventFunction_1_t972960537 ** get_address_of_U3CU3Ef__mgU24cache7_26() { return &___U3CU3Ef__mgU24cache7_26; }
	inline void set_U3CU3Ef__mgU24cache7_26(EventFunction_1_t972960537 * value)
	{
		___U3CU3Ef__mgU24cache7_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_27() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache8_27)); }
	inline EventFunction_1_t3277009892 * get_U3CU3Ef__mgU24cache8_27() const { return ___U3CU3Ef__mgU24cache8_27; }
	inline EventFunction_1_t3277009892 ** get_address_of_U3CU3Ef__mgU24cache8_27() { return &___U3CU3Ef__mgU24cache8_27; }
	inline void set_U3CU3Ef__mgU24cache8_27(EventFunction_1_t3277009892 * value)
	{
		___U3CU3Ef__mgU24cache8_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_28() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache9_28)); }
	inline EventFunction_1_t2311673543 * get_U3CU3Ef__mgU24cache9_28() const { return ___U3CU3Ef__mgU24cache9_28; }
	inline EventFunction_1_t2311673543 ** get_address_of_U3CU3Ef__mgU24cache9_28() { return &___U3CU3Ef__mgU24cache9_28; }
	inline void set_U3CU3Ef__mgU24cache9_28(EventFunction_1_t2311673543 * value)
	{
		___U3CU3Ef__mgU24cache9_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_29() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheA_29)); }
	inline EventFunction_1_t2886331738 * get_U3CU3Ef__mgU24cacheA_29() const { return ___U3CU3Ef__mgU24cacheA_29; }
	inline EventFunction_1_t2886331738 ** get_address_of_U3CU3Ef__mgU24cacheA_29() { return &___U3CU3Ef__mgU24cacheA_29; }
	inline void set_U3CU3Ef__mgU24cacheA_29(EventFunction_1_t2886331738 * value)
	{
		___U3CU3Ef__mgU24cacheA_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_30() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheB_30)); }
	inline EventFunction_1_t2950825503 * get_U3CU3Ef__mgU24cacheB_30() const { return ___U3CU3Ef__mgU24cacheB_30; }
	inline EventFunction_1_t2950825503 ** get_address_of_U3CU3Ef__mgU24cacheB_30() { return &___U3CU3Ef__mgU24cacheB_30; }
	inline void set_U3CU3Ef__mgU24cacheB_30(EventFunction_1_t2950825503 * value)
	{
		___U3CU3Ef__mgU24cacheB_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_31() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheC_31)); }
	inline EventFunction_1_t955952873 * get_U3CU3Ef__mgU24cacheC_31() const { return ___U3CU3Ef__mgU24cacheC_31; }
	inline EventFunction_1_t955952873 ** get_address_of_U3CU3Ef__mgU24cacheC_31() { return &___U3CU3Ef__mgU24cacheC_31; }
	inline void set_U3CU3Ef__mgU24cacheC_31(EventFunction_1_t955952873 * value)
	{
		___U3CU3Ef__mgU24cacheC_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_32() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheD_32)); }
	inline EventFunction_1_t3373214253 * get_U3CU3Ef__mgU24cacheD_32() const { return ___U3CU3Ef__mgU24cacheD_32; }
	inline EventFunction_1_t3373214253 ** get_address_of_U3CU3Ef__mgU24cacheD_32() { return &___U3CU3Ef__mgU24cacheD_32; }
	inline void set_U3CU3Ef__mgU24cacheD_32(EventFunction_1_t3373214253 * value)
	{
		___U3CU3Ef__mgU24cacheD_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_33() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheE_33)); }
	inline EventFunction_1_t3912835512 * get_U3CU3Ef__mgU24cacheE_33() const { return ___U3CU3Ef__mgU24cacheE_33; }
	inline EventFunction_1_t3912835512 ** get_address_of_U3CU3Ef__mgU24cacheE_33() { return &___U3CU3Ef__mgU24cacheE_33; }
	inline void set_U3CU3Ef__mgU24cacheE_33(EventFunction_1_t3912835512 * value)
	{
		___U3CU3Ef__mgU24cacheE_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_34() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheF_34)); }
	inline EventFunction_1_t1475332338 * get_U3CU3Ef__mgU24cacheF_34() const { return ___U3CU3Ef__mgU24cacheF_34; }
	inline EventFunction_1_t1475332338 ** get_address_of_U3CU3Ef__mgU24cacheF_34() { return &___U3CU3Ef__mgU24cacheF_34; }
	inline void set_U3CU3Ef__mgU24cacheF_34(EventFunction_1_t1475332338 * value)
	{
		___U3CU3Ef__mgU24cacheF_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_35() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache10_35)); }
	inline EventFunction_1_t2658898854 * get_U3CU3Ef__mgU24cache10_35() const { return ___U3CU3Ef__mgU24cache10_35; }
	inline EventFunction_1_t2658898854 ** get_address_of_U3CU3Ef__mgU24cache10_35() { return &___U3CU3Ef__mgU24cache10_35; }
	inline void set_U3CU3Ef__mgU24cache10_35(EventFunction_1_t2658898854 * value)
	{
		___U3CU3Ef__mgU24cache10_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEEVENTS_T3484638744_H
#ifndef BOUNCE_T852251727_H
#define BOUNCE_T852251727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.Bounce
struct  Bounce_t852251727  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNCE_T852251727_H
#ifndef U3CWAITFORSTARTU3ED__19_T3428776308_H
#define U3CWAITFORSTARTU3ED__19_T3428776308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19
struct  U3CWaitForStartU3Ed__19_t3428776308  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t3428776308, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t3428776308, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForStartU3Ed__19_t3428776308, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSTARTU3ED__19_T3428776308_H
#ifndef U3CWAITFORPOSITIONU3ED__18_T1811625055_H
#define U3CWAITFORPOSITIONU3ED__18_T1811625055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18
struct  U3CWaitForPositionU3Ed__18_t1811625055  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::t
	Tween_t2342918553 * ___t_2;
	// System.Single DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::position
	float ___position_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(U3CWaitForPositionU3Ed__18_t1811625055, ___position_3)); }
	inline float get_position_3() const { return ___position_3; }
	inline float* get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(float value)
	{
		___position_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPOSITIONU3ED__18_T1811625055_H
#ifndef U3CWAITFORELAPSEDLOOPSU3ED__17_T1514468280_H
#define U3CWAITFORELAPSEDLOOPSU3ED__17_T1514468280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17
struct  U3CWaitForElapsedLoopsU3Ed__17_t1514468280  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::t
	Tween_t2342918553 * ___t_2;
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::elapsedLoops
	int32_t ___elapsedLoops_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}

	inline static int32_t get_offset_of_elapsedLoops_3() { return static_cast<int32_t>(offsetof(U3CWaitForElapsedLoopsU3Ed__17_t1514468280, ___elapsedLoops_3)); }
	inline int32_t get_elapsedLoops_3() const { return ___elapsedLoops_3; }
	inline int32_t* get_address_of_elapsedLoops_3() { return &___elapsedLoops_3; }
	inline void set_elapsedLoops_3(int32_t value)
	{
		___elapsedLoops_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORELAPSEDLOOPSU3ED__17_T1514468280_H
#ifndef EXTENSIONS_T3835977652_H
#define EXTENSIONS_T3835977652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Extensions
struct  Extensions_t3835977652  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T3835977652_H
#ifndef RAYCASTERMANAGER_T2536340562_H
#define RAYCASTERMANAGER_T2536340562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t2536340562  : public RuntimeObject
{
public:

public:
};

struct RaycasterManager_t2536340562_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t1327982029 * ___s_Raycasters_0;

public:
	inline static int32_t get_offset_of_s_Raycasters_0() { return static_cast<int32_t>(offsetof(RaycasterManager_t2536340562_StaticFields, ___s_Raycasters_0)); }
	inline List_1_t1327982029 * get_s_Raycasters_0() const { return ___s_Raycasters_0; }
	inline List_1_t1327982029 ** get_address_of_s_Raycasters_0() { return &___s_Raycasters_0; }
	inline void set_s_Raycasters_0(List_1_t1327982029 * value)
	{
		___s_Raycasters_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Raycasters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTERMANAGER_T2536340562_H
#ifndef MOUSESTATE_T384203932_H
#define MOUSESTATE_T384203932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseState
struct  MouseState_t384203932  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState> UnityEngine.EventSystems.PointerInputModule/MouseState::m_TrackedButtons
	List_1_t2329214678 * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_t384203932, ___m_TrackedButtons_0)); }
	inline List_1_t2329214678 * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_t2329214678 ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_t2329214678 * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedButtons_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSESTATE_T384203932_H
#ifndef UTILS_T849021263_H
#define UTILS_T849021263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Utils
struct  Utils_t849021263  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T849021263_H
#ifndef TWEENMANAGER_T374091826_H
#define TWEENMANAGER_T374091826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenManager
struct  TweenManager_t374091826  : public RuntimeObject
{
public:

public:
};

struct TweenManager_t374091826_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.TweenManager::maxActive
	int32_t ___maxActive_3;
	// System.Int32 DG.Tweening.Core.TweenManager::maxTweeners
	int32_t ___maxTweeners_4;
	// System.Int32 DG.Tweening.Core.TweenManager::maxSequences
	int32_t ___maxSequences_5;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveTweens
	bool ___hasActiveTweens_6;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveDefaultTweens
	bool ___hasActiveDefaultTweens_7;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveLateTweens
	bool ___hasActiveLateTweens_8;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveFixedTweens
	bool ___hasActiveFixedTweens_9;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveManualTweens
	bool ___hasActiveManualTweens_10;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweens
	int32_t ___totActiveTweens_11;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveDefaultTweens
	int32_t ___totActiveDefaultTweens_12;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveLateTweens
	int32_t ___totActiveLateTweens_13;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveFixedTweens
	int32_t ___totActiveFixedTweens_14;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveManualTweens
	int32_t ___totActiveManualTweens_15;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweeners
	int32_t ___totActiveTweeners_16;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveSequences
	int32_t ___totActiveSequences_17;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledTweeners
	int32_t ___totPooledTweeners_18;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledSequences
	int32_t ___totPooledSequences_19;
	// System.Int32 DG.Tweening.Core.TweenManager::totTweeners
	int32_t ___totTweeners_20;
	// System.Int32 DG.Tweening.Core.TweenManager::totSequences
	int32_t ___totSequences_21;
	// System.Boolean DG.Tweening.Core.TweenManager::isUpdateLoop
	bool ___isUpdateLoop_22;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_activeTweens
	TweenU5BU5D_t1320466404* ____activeTweens_23;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_pooledTweeners
	TweenU5BU5D_t1320466404* ____pooledTweeners_24;
	// System.Collections.Generic.Stack`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_PooledSequences
	Stack_1_t3186308008 * ____PooledSequences_25;
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_KillList
	List_1_t3814993295 * ____KillList_26;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxActiveLookupId
	int32_t ____maxActiveLookupId_27;
	// System.Boolean DG.Tweening.Core.TweenManager::_requiresActiveReorganization
	bool ____requiresActiveReorganization_28;
	// System.Int32 DG.Tweening.Core.TweenManager::_reorganizeFromId
	int32_t ____reorganizeFromId_29;
	// System.Int32 DG.Tweening.Core.TweenManager::_minPooledTweenerId
	int32_t ____minPooledTweenerId_30;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxPooledTweenerId
	int32_t ____maxPooledTweenerId_31;
	// System.Boolean DG.Tweening.Core.TweenManager::_despawnAllCalledFromUpdateLoopCallback
	bool ____despawnAllCalledFromUpdateLoopCallback_32;

public:
	inline static int32_t get_offset_of_maxActive_3() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___maxActive_3)); }
	inline int32_t get_maxActive_3() const { return ___maxActive_3; }
	inline int32_t* get_address_of_maxActive_3() { return &___maxActive_3; }
	inline void set_maxActive_3(int32_t value)
	{
		___maxActive_3 = value;
	}

	inline static int32_t get_offset_of_maxTweeners_4() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___maxTweeners_4)); }
	inline int32_t get_maxTweeners_4() const { return ___maxTweeners_4; }
	inline int32_t* get_address_of_maxTweeners_4() { return &___maxTweeners_4; }
	inline void set_maxTweeners_4(int32_t value)
	{
		___maxTweeners_4 = value;
	}

	inline static int32_t get_offset_of_maxSequences_5() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___maxSequences_5)); }
	inline int32_t get_maxSequences_5() const { return ___maxSequences_5; }
	inline int32_t* get_address_of_maxSequences_5() { return &___maxSequences_5; }
	inline void set_maxSequences_5(int32_t value)
	{
		___maxSequences_5 = value;
	}

	inline static int32_t get_offset_of_hasActiveTweens_6() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveTweens_6)); }
	inline bool get_hasActiveTweens_6() const { return ___hasActiveTweens_6; }
	inline bool* get_address_of_hasActiveTweens_6() { return &___hasActiveTweens_6; }
	inline void set_hasActiveTweens_6(bool value)
	{
		___hasActiveTweens_6 = value;
	}

	inline static int32_t get_offset_of_hasActiveDefaultTweens_7() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveDefaultTweens_7)); }
	inline bool get_hasActiveDefaultTweens_7() const { return ___hasActiveDefaultTweens_7; }
	inline bool* get_address_of_hasActiveDefaultTweens_7() { return &___hasActiveDefaultTweens_7; }
	inline void set_hasActiveDefaultTweens_7(bool value)
	{
		___hasActiveDefaultTweens_7 = value;
	}

	inline static int32_t get_offset_of_hasActiveLateTweens_8() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveLateTweens_8)); }
	inline bool get_hasActiveLateTweens_8() const { return ___hasActiveLateTweens_8; }
	inline bool* get_address_of_hasActiveLateTweens_8() { return &___hasActiveLateTweens_8; }
	inline void set_hasActiveLateTweens_8(bool value)
	{
		___hasActiveLateTweens_8 = value;
	}

	inline static int32_t get_offset_of_hasActiveFixedTweens_9() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveFixedTweens_9)); }
	inline bool get_hasActiveFixedTweens_9() const { return ___hasActiveFixedTweens_9; }
	inline bool* get_address_of_hasActiveFixedTweens_9() { return &___hasActiveFixedTweens_9; }
	inline void set_hasActiveFixedTweens_9(bool value)
	{
		___hasActiveFixedTweens_9 = value;
	}

	inline static int32_t get_offset_of_hasActiveManualTweens_10() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___hasActiveManualTweens_10)); }
	inline bool get_hasActiveManualTweens_10() const { return ___hasActiveManualTweens_10; }
	inline bool* get_address_of_hasActiveManualTweens_10() { return &___hasActiveManualTweens_10; }
	inline void set_hasActiveManualTweens_10(bool value)
	{
		___hasActiveManualTweens_10 = value;
	}

	inline static int32_t get_offset_of_totActiveTweens_11() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveTweens_11)); }
	inline int32_t get_totActiveTweens_11() const { return ___totActiveTweens_11; }
	inline int32_t* get_address_of_totActiveTweens_11() { return &___totActiveTweens_11; }
	inline void set_totActiveTweens_11(int32_t value)
	{
		___totActiveTweens_11 = value;
	}

	inline static int32_t get_offset_of_totActiveDefaultTweens_12() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveDefaultTweens_12)); }
	inline int32_t get_totActiveDefaultTweens_12() const { return ___totActiveDefaultTweens_12; }
	inline int32_t* get_address_of_totActiveDefaultTweens_12() { return &___totActiveDefaultTweens_12; }
	inline void set_totActiveDefaultTweens_12(int32_t value)
	{
		___totActiveDefaultTweens_12 = value;
	}

	inline static int32_t get_offset_of_totActiveLateTweens_13() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveLateTweens_13)); }
	inline int32_t get_totActiveLateTweens_13() const { return ___totActiveLateTweens_13; }
	inline int32_t* get_address_of_totActiveLateTweens_13() { return &___totActiveLateTweens_13; }
	inline void set_totActiveLateTweens_13(int32_t value)
	{
		___totActiveLateTweens_13 = value;
	}

	inline static int32_t get_offset_of_totActiveFixedTweens_14() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveFixedTweens_14)); }
	inline int32_t get_totActiveFixedTweens_14() const { return ___totActiveFixedTweens_14; }
	inline int32_t* get_address_of_totActiveFixedTweens_14() { return &___totActiveFixedTweens_14; }
	inline void set_totActiveFixedTweens_14(int32_t value)
	{
		___totActiveFixedTweens_14 = value;
	}

	inline static int32_t get_offset_of_totActiveManualTweens_15() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveManualTweens_15)); }
	inline int32_t get_totActiveManualTweens_15() const { return ___totActiveManualTweens_15; }
	inline int32_t* get_address_of_totActiveManualTweens_15() { return &___totActiveManualTweens_15; }
	inline void set_totActiveManualTweens_15(int32_t value)
	{
		___totActiveManualTweens_15 = value;
	}

	inline static int32_t get_offset_of_totActiveTweeners_16() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveTweeners_16)); }
	inline int32_t get_totActiveTweeners_16() const { return ___totActiveTweeners_16; }
	inline int32_t* get_address_of_totActiveTweeners_16() { return &___totActiveTweeners_16; }
	inline void set_totActiveTweeners_16(int32_t value)
	{
		___totActiveTweeners_16 = value;
	}

	inline static int32_t get_offset_of_totActiveSequences_17() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totActiveSequences_17)); }
	inline int32_t get_totActiveSequences_17() const { return ___totActiveSequences_17; }
	inline int32_t* get_address_of_totActiveSequences_17() { return &___totActiveSequences_17; }
	inline void set_totActiveSequences_17(int32_t value)
	{
		___totActiveSequences_17 = value;
	}

	inline static int32_t get_offset_of_totPooledTweeners_18() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totPooledTweeners_18)); }
	inline int32_t get_totPooledTweeners_18() const { return ___totPooledTweeners_18; }
	inline int32_t* get_address_of_totPooledTweeners_18() { return &___totPooledTweeners_18; }
	inline void set_totPooledTweeners_18(int32_t value)
	{
		___totPooledTweeners_18 = value;
	}

	inline static int32_t get_offset_of_totPooledSequences_19() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totPooledSequences_19)); }
	inline int32_t get_totPooledSequences_19() const { return ___totPooledSequences_19; }
	inline int32_t* get_address_of_totPooledSequences_19() { return &___totPooledSequences_19; }
	inline void set_totPooledSequences_19(int32_t value)
	{
		___totPooledSequences_19 = value;
	}

	inline static int32_t get_offset_of_totTweeners_20() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totTweeners_20)); }
	inline int32_t get_totTweeners_20() const { return ___totTweeners_20; }
	inline int32_t* get_address_of_totTweeners_20() { return &___totTweeners_20; }
	inline void set_totTweeners_20(int32_t value)
	{
		___totTweeners_20 = value;
	}

	inline static int32_t get_offset_of_totSequences_21() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___totSequences_21)); }
	inline int32_t get_totSequences_21() const { return ___totSequences_21; }
	inline int32_t* get_address_of_totSequences_21() { return &___totSequences_21; }
	inline void set_totSequences_21(int32_t value)
	{
		___totSequences_21 = value;
	}

	inline static int32_t get_offset_of_isUpdateLoop_22() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ___isUpdateLoop_22)); }
	inline bool get_isUpdateLoop_22() const { return ___isUpdateLoop_22; }
	inline bool* get_address_of_isUpdateLoop_22() { return &___isUpdateLoop_22; }
	inline void set_isUpdateLoop_22(bool value)
	{
		___isUpdateLoop_22 = value;
	}

	inline static int32_t get_offset_of__activeTweens_23() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____activeTweens_23)); }
	inline TweenU5BU5D_t1320466404* get__activeTweens_23() const { return ____activeTweens_23; }
	inline TweenU5BU5D_t1320466404** get_address_of__activeTweens_23() { return &____activeTweens_23; }
	inline void set__activeTweens_23(TweenU5BU5D_t1320466404* value)
	{
		____activeTweens_23 = value;
		Il2CppCodeGenWriteBarrier((&____activeTweens_23), value);
	}

	inline static int32_t get_offset_of__pooledTweeners_24() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____pooledTweeners_24)); }
	inline TweenU5BU5D_t1320466404* get__pooledTweeners_24() const { return ____pooledTweeners_24; }
	inline TweenU5BU5D_t1320466404** get_address_of__pooledTweeners_24() { return &____pooledTweeners_24; }
	inline void set__pooledTweeners_24(TweenU5BU5D_t1320466404* value)
	{
		____pooledTweeners_24 = value;
		Il2CppCodeGenWriteBarrier((&____pooledTweeners_24), value);
	}

	inline static int32_t get_offset_of__PooledSequences_25() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____PooledSequences_25)); }
	inline Stack_1_t3186308008 * get__PooledSequences_25() const { return ____PooledSequences_25; }
	inline Stack_1_t3186308008 ** get_address_of__PooledSequences_25() { return &____PooledSequences_25; }
	inline void set__PooledSequences_25(Stack_1_t3186308008 * value)
	{
		____PooledSequences_25 = value;
		Il2CppCodeGenWriteBarrier((&____PooledSequences_25), value);
	}

	inline static int32_t get_offset_of__KillList_26() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____KillList_26)); }
	inline List_1_t3814993295 * get__KillList_26() const { return ____KillList_26; }
	inline List_1_t3814993295 ** get_address_of__KillList_26() { return &____KillList_26; }
	inline void set__KillList_26(List_1_t3814993295 * value)
	{
		____KillList_26 = value;
		Il2CppCodeGenWriteBarrier((&____KillList_26), value);
	}

	inline static int32_t get_offset_of__maxActiveLookupId_27() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____maxActiveLookupId_27)); }
	inline int32_t get__maxActiveLookupId_27() const { return ____maxActiveLookupId_27; }
	inline int32_t* get_address_of__maxActiveLookupId_27() { return &____maxActiveLookupId_27; }
	inline void set__maxActiveLookupId_27(int32_t value)
	{
		____maxActiveLookupId_27 = value;
	}

	inline static int32_t get_offset_of__requiresActiveReorganization_28() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____requiresActiveReorganization_28)); }
	inline bool get__requiresActiveReorganization_28() const { return ____requiresActiveReorganization_28; }
	inline bool* get_address_of__requiresActiveReorganization_28() { return &____requiresActiveReorganization_28; }
	inline void set__requiresActiveReorganization_28(bool value)
	{
		____requiresActiveReorganization_28 = value;
	}

	inline static int32_t get_offset_of__reorganizeFromId_29() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____reorganizeFromId_29)); }
	inline int32_t get__reorganizeFromId_29() const { return ____reorganizeFromId_29; }
	inline int32_t* get_address_of__reorganizeFromId_29() { return &____reorganizeFromId_29; }
	inline void set__reorganizeFromId_29(int32_t value)
	{
		____reorganizeFromId_29 = value;
	}

	inline static int32_t get_offset_of__minPooledTweenerId_30() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____minPooledTweenerId_30)); }
	inline int32_t get__minPooledTweenerId_30() const { return ____minPooledTweenerId_30; }
	inline int32_t* get_address_of__minPooledTweenerId_30() { return &____minPooledTweenerId_30; }
	inline void set__minPooledTweenerId_30(int32_t value)
	{
		____minPooledTweenerId_30 = value;
	}

	inline static int32_t get_offset_of__maxPooledTweenerId_31() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____maxPooledTweenerId_31)); }
	inline int32_t get__maxPooledTweenerId_31() const { return ____maxPooledTweenerId_31; }
	inline int32_t* get_address_of__maxPooledTweenerId_31() { return &____maxPooledTweenerId_31; }
	inline void set__maxPooledTweenerId_31(int32_t value)
	{
		____maxPooledTweenerId_31 = value;
	}

	inline static int32_t get_offset_of__despawnAllCalledFromUpdateLoopCallback_32() { return static_cast<int32_t>(offsetof(TweenManager_t374091826_StaticFields, ____despawnAllCalledFromUpdateLoopCallback_32)); }
	inline bool get__despawnAllCalledFromUpdateLoopCallback_32() const { return ____despawnAllCalledFromUpdateLoopCallback_32; }
	inline bool* get_address_of__despawnAllCalledFromUpdateLoopCallback_32() { return &____despawnAllCalledFromUpdateLoopCallback_32; }
	inline void set__despawnAllCalledFromUpdateLoopCallback_32(bool value)
	{
		____despawnAllCalledFromUpdateLoopCallback_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENMANAGER_T374091826_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef U3CWAITFORKILLU3ED__16_T383710286_H
#define U3CWAITFORKILLU3ED__16_T383710286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16
struct  U3CWaitForKillU3Ed__16_t383710286  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_t383710286, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_t383710286, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForKillU3Ed__16_t383710286, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORKILLU3ED__16_T383710286_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef SPECIALPLUGINSUTILS_T1116177272_H
#define SPECIALPLUGINSUTILS_T1116177272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.SpecialPluginsUtils
struct  SpecialPluginsUtils_t1116177272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALPLUGINSUTILS_T1116177272_H
#ifndef PLUGINSMANAGER_T1753812699_H
#define PLUGINSMANAGER_T1753812699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PluginsManager
struct  PluginsManager_t1753812699  : public RuntimeObject
{
public:

public:
};

struct PluginsManager_t1753812699_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_floatPlugin
	RuntimeObject* ____floatPlugin_0;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_doublePlugin
	RuntimeObject* ____doublePlugin_1;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_intPlugin
	RuntimeObject* ____intPlugin_2;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_uintPlugin
	RuntimeObject* ____uintPlugin_3;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_longPlugin
	RuntimeObject* ____longPlugin_4;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_ulongPlugin
	RuntimeObject* ____ulongPlugin_5;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector2Plugin
	RuntimeObject* ____vector2Plugin_6;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3Plugin
	RuntimeObject* ____vector3Plugin_7;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector4Plugin
	RuntimeObject* ____vector4Plugin_8;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_quaternionPlugin
	RuntimeObject* ____quaternionPlugin_9;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_colorPlugin
	RuntimeObject* ____colorPlugin_10;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectPlugin
	RuntimeObject* ____rectPlugin_11;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectOffsetPlugin
	RuntimeObject* ____rectOffsetPlugin_12;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_stringPlugin
	RuntimeObject* ____stringPlugin_13;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3ArrayPlugin
	RuntimeObject* ____vector3ArrayPlugin_14;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_color2Plugin
	RuntimeObject* ____color2Plugin_15;
	// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin> DG.Tweening.Plugins.Core.PluginsManager::_customPlugins
	Dictionary_2_t109512516 * ____customPlugins_17;

public:
	inline static int32_t get_offset_of__floatPlugin_0() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____floatPlugin_0)); }
	inline RuntimeObject* get__floatPlugin_0() const { return ____floatPlugin_0; }
	inline RuntimeObject** get_address_of__floatPlugin_0() { return &____floatPlugin_0; }
	inline void set__floatPlugin_0(RuntimeObject* value)
	{
		____floatPlugin_0 = value;
		Il2CppCodeGenWriteBarrier((&____floatPlugin_0), value);
	}

	inline static int32_t get_offset_of__doublePlugin_1() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____doublePlugin_1)); }
	inline RuntimeObject* get__doublePlugin_1() const { return ____doublePlugin_1; }
	inline RuntimeObject** get_address_of__doublePlugin_1() { return &____doublePlugin_1; }
	inline void set__doublePlugin_1(RuntimeObject* value)
	{
		____doublePlugin_1 = value;
		Il2CppCodeGenWriteBarrier((&____doublePlugin_1), value);
	}

	inline static int32_t get_offset_of__intPlugin_2() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____intPlugin_2)); }
	inline RuntimeObject* get__intPlugin_2() const { return ____intPlugin_2; }
	inline RuntimeObject** get_address_of__intPlugin_2() { return &____intPlugin_2; }
	inline void set__intPlugin_2(RuntimeObject* value)
	{
		____intPlugin_2 = value;
		Il2CppCodeGenWriteBarrier((&____intPlugin_2), value);
	}

	inline static int32_t get_offset_of__uintPlugin_3() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____uintPlugin_3)); }
	inline RuntimeObject* get__uintPlugin_3() const { return ____uintPlugin_3; }
	inline RuntimeObject** get_address_of__uintPlugin_3() { return &____uintPlugin_3; }
	inline void set__uintPlugin_3(RuntimeObject* value)
	{
		____uintPlugin_3 = value;
		Il2CppCodeGenWriteBarrier((&____uintPlugin_3), value);
	}

	inline static int32_t get_offset_of__longPlugin_4() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____longPlugin_4)); }
	inline RuntimeObject* get__longPlugin_4() const { return ____longPlugin_4; }
	inline RuntimeObject** get_address_of__longPlugin_4() { return &____longPlugin_4; }
	inline void set__longPlugin_4(RuntimeObject* value)
	{
		____longPlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____longPlugin_4), value);
	}

	inline static int32_t get_offset_of__ulongPlugin_5() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____ulongPlugin_5)); }
	inline RuntimeObject* get__ulongPlugin_5() const { return ____ulongPlugin_5; }
	inline RuntimeObject** get_address_of__ulongPlugin_5() { return &____ulongPlugin_5; }
	inline void set__ulongPlugin_5(RuntimeObject* value)
	{
		____ulongPlugin_5 = value;
		Il2CppCodeGenWriteBarrier((&____ulongPlugin_5), value);
	}

	inline static int32_t get_offset_of__vector2Plugin_6() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector2Plugin_6)); }
	inline RuntimeObject* get__vector2Plugin_6() const { return ____vector2Plugin_6; }
	inline RuntimeObject** get_address_of__vector2Plugin_6() { return &____vector2Plugin_6; }
	inline void set__vector2Plugin_6(RuntimeObject* value)
	{
		____vector2Plugin_6 = value;
		Il2CppCodeGenWriteBarrier((&____vector2Plugin_6), value);
	}

	inline static int32_t get_offset_of__vector3Plugin_7() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector3Plugin_7)); }
	inline RuntimeObject* get__vector3Plugin_7() const { return ____vector3Plugin_7; }
	inline RuntimeObject** get_address_of__vector3Plugin_7() { return &____vector3Plugin_7; }
	inline void set__vector3Plugin_7(RuntimeObject* value)
	{
		____vector3Plugin_7 = value;
		Il2CppCodeGenWriteBarrier((&____vector3Plugin_7), value);
	}

	inline static int32_t get_offset_of__vector4Plugin_8() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector4Plugin_8)); }
	inline RuntimeObject* get__vector4Plugin_8() const { return ____vector4Plugin_8; }
	inline RuntimeObject** get_address_of__vector4Plugin_8() { return &____vector4Plugin_8; }
	inline void set__vector4Plugin_8(RuntimeObject* value)
	{
		____vector4Plugin_8 = value;
		Il2CppCodeGenWriteBarrier((&____vector4Plugin_8), value);
	}

	inline static int32_t get_offset_of__quaternionPlugin_9() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____quaternionPlugin_9)); }
	inline RuntimeObject* get__quaternionPlugin_9() const { return ____quaternionPlugin_9; }
	inline RuntimeObject** get_address_of__quaternionPlugin_9() { return &____quaternionPlugin_9; }
	inline void set__quaternionPlugin_9(RuntimeObject* value)
	{
		____quaternionPlugin_9 = value;
		Il2CppCodeGenWriteBarrier((&____quaternionPlugin_9), value);
	}

	inline static int32_t get_offset_of__colorPlugin_10() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____colorPlugin_10)); }
	inline RuntimeObject* get__colorPlugin_10() const { return ____colorPlugin_10; }
	inline RuntimeObject** get_address_of__colorPlugin_10() { return &____colorPlugin_10; }
	inline void set__colorPlugin_10(RuntimeObject* value)
	{
		____colorPlugin_10 = value;
		Il2CppCodeGenWriteBarrier((&____colorPlugin_10), value);
	}

	inline static int32_t get_offset_of__rectPlugin_11() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____rectPlugin_11)); }
	inline RuntimeObject* get__rectPlugin_11() const { return ____rectPlugin_11; }
	inline RuntimeObject** get_address_of__rectPlugin_11() { return &____rectPlugin_11; }
	inline void set__rectPlugin_11(RuntimeObject* value)
	{
		____rectPlugin_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectPlugin_11), value);
	}

	inline static int32_t get_offset_of__rectOffsetPlugin_12() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____rectOffsetPlugin_12)); }
	inline RuntimeObject* get__rectOffsetPlugin_12() const { return ____rectOffsetPlugin_12; }
	inline RuntimeObject** get_address_of__rectOffsetPlugin_12() { return &____rectOffsetPlugin_12; }
	inline void set__rectOffsetPlugin_12(RuntimeObject* value)
	{
		____rectOffsetPlugin_12 = value;
		Il2CppCodeGenWriteBarrier((&____rectOffsetPlugin_12), value);
	}

	inline static int32_t get_offset_of__stringPlugin_13() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____stringPlugin_13)); }
	inline RuntimeObject* get__stringPlugin_13() const { return ____stringPlugin_13; }
	inline RuntimeObject** get_address_of__stringPlugin_13() { return &____stringPlugin_13; }
	inline void set__stringPlugin_13(RuntimeObject* value)
	{
		____stringPlugin_13 = value;
		Il2CppCodeGenWriteBarrier((&____stringPlugin_13), value);
	}

	inline static int32_t get_offset_of__vector3ArrayPlugin_14() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____vector3ArrayPlugin_14)); }
	inline RuntimeObject* get__vector3ArrayPlugin_14() const { return ____vector3ArrayPlugin_14; }
	inline RuntimeObject** get_address_of__vector3ArrayPlugin_14() { return &____vector3ArrayPlugin_14; }
	inline void set__vector3ArrayPlugin_14(RuntimeObject* value)
	{
		____vector3ArrayPlugin_14 = value;
		Il2CppCodeGenWriteBarrier((&____vector3ArrayPlugin_14), value);
	}

	inline static int32_t get_offset_of__color2Plugin_15() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____color2Plugin_15)); }
	inline RuntimeObject* get__color2Plugin_15() const { return ____color2Plugin_15; }
	inline RuntimeObject** get_address_of__color2Plugin_15() { return &____color2Plugin_15; }
	inline void set__color2Plugin_15(RuntimeObject* value)
	{
		____color2Plugin_15 = value;
		Il2CppCodeGenWriteBarrier((&____color2Plugin_15), value);
	}

	inline static int32_t get_offset_of__customPlugins_17() { return static_cast<int32_t>(offsetof(PluginsManager_t1753812699_StaticFields, ____customPlugins_17)); }
	inline Dictionary_2_t109512516 * get__customPlugins_17() const { return ____customPlugins_17; }
	inline Dictionary_2_t109512516 ** get_address_of__customPlugins_17() { return &____customPlugins_17; }
	inline void set__customPlugins_17(Dictionary_2_t109512516 * value)
	{
		____customPlugins_17 = value;
		Il2CppCodeGenWriteBarrier((&____customPlugins_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINSMANAGER_T1753812699_H
#ifndef ABSPATHDECODER_T2613982196_H
#define ABSPATHDECODER_T2613982196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct  ABSPathDecoder_t2613982196  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSPATHDECODER_T2613982196_H
#ifndef FLASH_T1955515410_H
#define FLASH_T1955515410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.Flash
struct  Flash_t1955515410  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLASH_T1955515410_H
#ifndef EASECURVE_T2771593608_H
#define EASECURVE_T2771593608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.EaseCurve
struct  EaseCurve_t2771593608  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve DG.Tweening.Core.Easing.EaseCurve::_animCurve
	AnimationCurve_t3046754366 * ____animCurve_0;

public:
	inline static int32_t get_offset_of__animCurve_0() { return static_cast<int32_t>(offsetof(EaseCurve_t2771593608, ____animCurve_0)); }
	inline AnimationCurve_t3046754366 * get__animCurve_0() const { return ____animCurve_0; }
	inline AnimationCurve_t3046754366 ** get_address_of__animCurve_0() { return &____animCurve_0; }
	inline void set__animCurve_0(AnimationCurve_t3046754366 * value)
	{
		____animCurve_0 = value;
		Il2CppCodeGenWriteBarrier((&____animCurve_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASECURVE_T2771593608_H
#ifndef U3CWAITFORREWINDU3ED__15_T2339862385_H
#define U3CWAITFORREWINDU3ED__15_T2339862385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15
struct  U3CWaitForRewindU3Ed__15_t2339862385  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_t2339862385, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_t2339862385, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForRewindU3Ed__15_t2339862385, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORREWINDU3ED__15_T2339862385_H
#ifndef U3CWAITFORCOMPLETIONU3ED__14_T1419758899_H
#define U3CWAITFORCOMPLETIONU3ED__14_T1419758899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14
struct  U3CWaitForCompletionU3Ed__14_t1419758899  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::t
	Tween_t2342918553 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_t1419758899, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_t1419758899, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__14_t1419758899, ___t_2)); }
	inline Tween_t2342918553 * get_t_2() const { return ___t_2; }
	inline Tween_t2342918553 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t2342918553 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORCOMPLETIONU3ED__14_T1419758899_H
#ifndef EASEMANAGER_T1997294889_H
#define EASEMANAGER_T1997294889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.EaseManager
struct  EaseManager_t1997294889  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEMANAGER_T1997294889_H
#ifndef ABSTWEENPLUGIN_3_T3321825548_H
#define ABSTWEENPLUGIN_3_T3321825548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t3321825548  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3321825548_H
#ifndef DEBUGGER_T1756157868_H
#define DEBUGGER_T1756157868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Debugger
struct  Debugger_t1756157868  : public RuntimeObject
{
public:

public:
};

struct Debugger_t1756157868_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_t1756157868_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_T1756157868_H
#ifndef U3CU3EC_T1320040651_H
#define U3CU3EC_T1320040651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Easing.EaseManager/<>c
struct  U3CU3Ec_t1320040651  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1320040651_StaticFields
{
public:
	// DG.Tweening.Core.Easing.EaseManager/<>c DG.Tweening.Core.Easing.EaseManager/<>c::<>9
	U3CU3Ec_t1320040651 * ___U3CU3E9_0;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_0
	EaseFunction_t3531141372 * ___U3CU3E9__4_0_1;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_1
	EaseFunction_t3531141372 * ___U3CU3E9__4_1_2;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_2
	EaseFunction_t3531141372 * ___U3CU3E9__4_2_3;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_3
	EaseFunction_t3531141372 * ___U3CU3E9__4_3_4;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_4
	EaseFunction_t3531141372 * ___U3CU3E9__4_4_5;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_5
	EaseFunction_t3531141372 * ___U3CU3E9__4_5_6;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_6
	EaseFunction_t3531141372 * ___U3CU3E9__4_6_7;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_7
	EaseFunction_t3531141372 * ___U3CU3E9__4_7_8;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_8
	EaseFunction_t3531141372 * ___U3CU3E9__4_8_9;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_9
	EaseFunction_t3531141372 * ___U3CU3E9__4_9_10;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_10
	EaseFunction_t3531141372 * ___U3CU3E9__4_10_11;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_11
	EaseFunction_t3531141372 * ___U3CU3E9__4_11_12;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_12
	EaseFunction_t3531141372 * ___U3CU3E9__4_12_13;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_13
	EaseFunction_t3531141372 * ___U3CU3E9__4_13_14;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_14
	EaseFunction_t3531141372 * ___U3CU3E9__4_14_15;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_15
	EaseFunction_t3531141372 * ___U3CU3E9__4_15_16;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_16
	EaseFunction_t3531141372 * ___U3CU3E9__4_16_17;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_17
	EaseFunction_t3531141372 * ___U3CU3E9__4_17_18;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_18
	EaseFunction_t3531141372 * ___U3CU3E9__4_18_19;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_19
	EaseFunction_t3531141372 * ___U3CU3E9__4_19_20;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_20
	EaseFunction_t3531141372 * ___U3CU3E9__4_20_21;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_21
	EaseFunction_t3531141372 * ___U3CU3E9__4_21_22;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_22
	EaseFunction_t3531141372 * ___U3CU3E9__4_22_23;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_23
	EaseFunction_t3531141372 * ___U3CU3E9__4_23_24;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_24
	EaseFunction_t3531141372 * ___U3CU3E9__4_24_25;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_25
	EaseFunction_t3531141372 * ___U3CU3E9__4_25_26;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_26
	EaseFunction_t3531141372 * ___U3CU3E9__4_26_27;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_27
	EaseFunction_t3531141372 * ___U3CU3E9__4_27_28;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_28
	EaseFunction_t3531141372 * ___U3CU3E9__4_28_29;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_29
	EaseFunction_t3531141372 * ___U3CU3E9__4_29_30;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_30
	EaseFunction_t3531141372 * ___U3CU3E9__4_30_31;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_31
	EaseFunction_t3531141372 * ___U3CU3E9__4_31_32;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_32
	EaseFunction_t3531141372 * ___U3CU3E9__4_32_33;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_33
	EaseFunction_t3531141372 * ___U3CU3E9__4_33_34;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_34
	EaseFunction_t3531141372 * ___U3CU3E9__4_34_35;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_35
	EaseFunction_t3531141372 * ___U3CU3E9__4_35_36;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1320040651 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1320040651 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1320040651 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_0_1)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_1_2)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_1_2() const { return ___U3CU3E9__4_1_2; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_1_2() { return &___U3CU3E9__4_1_2; }
	inline void set_U3CU3E9__4_1_2(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_2_3)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_2_3() const { return ___U3CU3E9__4_2_3; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_2_3() { return &___U3CU3E9__4_2_3; }
	inline void set_U3CU3E9__4_2_3(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_3_4)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_3_4() const { return ___U3CU3E9__4_3_4; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_3_4() { return &___U3CU3E9__4_3_4; }
	inline void set_U3CU3E9__4_3_4(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_4_5)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_4_5() const { return ___U3CU3E9__4_4_5; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_4_5() { return &___U3CU3E9__4_4_5; }
	inline void set_U3CU3E9__4_4_5(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_5_6)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_5_6() const { return ___U3CU3E9__4_5_6; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_5_6() { return &___U3CU3E9__4_5_6; }
	inline void set_U3CU3E9__4_5_6(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_5_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_6_7)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_6_7() const { return ___U3CU3E9__4_6_7; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_6_7() { return &___U3CU3E9__4_6_7; }
	inline void set_U3CU3E9__4_6_7(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_6_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_7_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_7_8)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_7_8() const { return ___U3CU3E9__4_7_8; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_7_8() { return &___U3CU3E9__4_7_8; }
	inline void set_U3CU3E9__4_7_8(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_7_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_7_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_8_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_8_9)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_8_9() const { return ___U3CU3E9__4_8_9; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_8_9() { return &___U3CU3E9__4_8_9; }
	inline void set_U3CU3E9__4_8_9(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_8_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_8_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_9_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_9_10)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_9_10() const { return ___U3CU3E9__4_9_10; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_9_10() { return &___U3CU3E9__4_9_10; }
	inline void set_U3CU3E9__4_9_10(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_9_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_9_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_10_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_10_11)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_10_11() const { return ___U3CU3E9__4_10_11; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_10_11() { return &___U3CU3E9__4_10_11; }
	inline void set_U3CU3E9__4_10_11(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_10_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_10_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_11_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_11_12)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_11_12() const { return ___U3CU3E9__4_11_12; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_11_12() { return &___U3CU3E9__4_11_12; }
	inline void set_U3CU3E9__4_11_12(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_11_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_11_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_12_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_12_13)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_12_13() const { return ___U3CU3E9__4_12_13; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_12_13() { return &___U3CU3E9__4_12_13; }
	inline void set_U3CU3E9__4_12_13(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_12_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_12_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_13_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_13_14)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_13_14() const { return ___U3CU3E9__4_13_14; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_13_14() { return &___U3CU3E9__4_13_14; }
	inline void set_U3CU3E9__4_13_14(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_13_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_13_14), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_14_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_14_15)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_14_15() const { return ___U3CU3E9__4_14_15; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_14_15() { return &___U3CU3E9__4_14_15; }
	inline void set_U3CU3E9__4_14_15(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_14_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_14_15), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_15_16() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_15_16)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_15_16() const { return ___U3CU3E9__4_15_16; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_15_16() { return &___U3CU3E9__4_15_16; }
	inline void set_U3CU3E9__4_15_16(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_15_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_15_16), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_16_17() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_16_17)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_16_17() const { return ___U3CU3E9__4_16_17; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_16_17() { return &___U3CU3E9__4_16_17; }
	inline void set_U3CU3E9__4_16_17(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_16_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_16_17), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_17_18() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_17_18)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_17_18() const { return ___U3CU3E9__4_17_18; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_17_18() { return &___U3CU3E9__4_17_18; }
	inline void set_U3CU3E9__4_17_18(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_17_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_17_18), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_18_19() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_18_19)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_18_19() const { return ___U3CU3E9__4_18_19; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_18_19() { return &___U3CU3E9__4_18_19; }
	inline void set_U3CU3E9__4_18_19(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_18_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_18_19), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_19_20() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_19_20)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_19_20() const { return ___U3CU3E9__4_19_20; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_19_20() { return &___U3CU3E9__4_19_20; }
	inline void set_U3CU3E9__4_19_20(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_19_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_19_20), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_20_21() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_20_21)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_20_21() const { return ___U3CU3E9__4_20_21; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_20_21() { return &___U3CU3E9__4_20_21; }
	inline void set_U3CU3E9__4_20_21(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_20_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_20_21), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_21_22() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_21_22)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_21_22() const { return ___U3CU3E9__4_21_22; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_21_22() { return &___U3CU3E9__4_21_22; }
	inline void set_U3CU3E9__4_21_22(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_21_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_21_22), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_22_23() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_22_23)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_22_23() const { return ___U3CU3E9__4_22_23; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_22_23() { return &___U3CU3E9__4_22_23; }
	inline void set_U3CU3E9__4_22_23(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_22_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_22_23), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_23_24() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_23_24)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_23_24() const { return ___U3CU3E9__4_23_24; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_23_24() { return &___U3CU3E9__4_23_24; }
	inline void set_U3CU3E9__4_23_24(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_23_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_23_24), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_24_25() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_24_25)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_24_25() const { return ___U3CU3E9__4_24_25; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_24_25() { return &___U3CU3E9__4_24_25; }
	inline void set_U3CU3E9__4_24_25(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_24_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_24_25), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_25_26() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_25_26)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_25_26() const { return ___U3CU3E9__4_25_26; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_25_26() { return &___U3CU3E9__4_25_26; }
	inline void set_U3CU3E9__4_25_26(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_25_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_25_26), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_26_27() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_26_27)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_26_27() const { return ___U3CU3E9__4_26_27; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_26_27() { return &___U3CU3E9__4_26_27; }
	inline void set_U3CU3E9__4_26_27(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_26_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_26_27), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_27_28() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_27_28)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_27_28() const { return ___U3CU3E9__4_27_28; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_27_28() { return &___U3CU3E9__4_27_28; }
	inline void set_U3CU3E9__4_27_28(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_27_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_27_28), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_28_29() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_28_29)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_28_29() const { return ___U3CU3E9__4_28_29; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_28_29() { return &___U3CU3E9__4_28_29; }
	inline void set_U3CU3E9__4_28_29(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_28_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_28_29), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_29_30() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_29_30)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_29_30() const { return ___U3CU3E9__4_29_30; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_29_30() { return &___U3CU3E9__4_29_30; }
	inline void set_U3CU3E9__4_29_30(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_29_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_29_30), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_30_31() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_30_31)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_30_31() const { return ___U3CU3E9__4_30_31; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_30_31() { return &___U3CU3E9__4_30_31; }
	inline void set_U3CU3E9__4_30_31(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_30_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_30_31), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_31_32() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_31_32)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_31_32() const { return ___U3CU3E9__4_31_32; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_31_32() { return &___U3CU3E9__4_31_32; }
	inline void set_U3CU3E9__4_31_32(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_31_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_31_32), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_32_33() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_32_33)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_32_33() const { return ___U3CU3E9__4_32_33; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_32_33() { return &___U3CU3E9__4_32_33; }
	inline void set_U3CU3E9__4_32_33(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_32_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_32_33), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_33_34() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_33_34)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_33_34() const { return ___U3CU3E9__4_33_34; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_33_34() { return &___U3CU3E9__4_33_34; }
	inline void set_U3CU3E9__4_33_34(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_33_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_33_34), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_34_35() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_34_35)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_34_35() const { return ___U3CU3E9__4_34_35; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_34_35() { return &___U3CU3E9__4_34_35; }
	inline void set_U3CU3E9__4_34_35(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_34_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_34_35), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_35_36() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1320040651_StaticFields, ___U3CU3E9__4_35_36)); }
	inline EaseFunction_t3531141372 * get_U3CU3E9__4_35_36() const { return ___U3CU3E9__4_35_36; }
	inline EaseFunction_t3531141372 ** get_address_of_U3CU3E9__4_35_36() { return &___U3CU3E9__4_35_36; }
	inline void set_U3CU3E9__4_35_36(EaseFunction_t3531141372 * value)
	{
		___U3CU3E9__4_35_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_35_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1320040651_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T1548391511_H
#define __STATICARRAYINITTYPESIZEU3D20_T1548391511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_t1548391511 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t1548391511__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T1548391511_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef __STATICARRAYINITTYPESIZEU3D50_T1547932759_H
#define __STATICARRAYINITTYPESIZEU3D50_T1547932759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50
struct  __StaticArrayInitTypeSizeU3D50_t1547932759 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D50_t1547932759__padding[50];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D50_T1547932759_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef CATMULLROMDECODER_T2053048079_H
#define CATMULLROMDECODER_T2053048079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct  CatmullRomDecoder_t2053048079  : public ABSPathDecoder_t2613982196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATMULLROMDECODER_T2053048079_H
#ifndef LINEARDECODER_T2708327777_H
#define LINEARDECODER_T2708327777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct  LinearDecoder_t2708327777  : public ABSPathDecoder_t2613982196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARDECODER_T2708327777_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef PUREQUATERNIONPLUGIN_T587122414_H
#define PUREQUATERNIONPLUGIN_T587122414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct  PureQuaternionPlugin_t587122414  : public ABSTweenPlugin_3_t3321825548
{
public:

public:
};

struct PureQuaternionPlugin_t587122414_StaticFields
{
public:
	// DG.Tweening.CustomPlugins.PureQuaternionPlugin DG.Tweening.CustomPlugins.PureQuaternionPlugin::_plug
	PureQuaternionPlugin_t587122414 * ____plug_0;

public:
	inline static int32_t get_offset_of__plug_0() { return static_cast<int32_t>(offsetof(PureQuaternionPlugin_t587122414_StaticFields, ____plug_0)); }
	inline PureQuaternionPlugin_t587122414 * get__plug_0() const { return ____plug_0; }
	inline PureQuaternionPlugin_t587122414 ** get_address_of__plug_0() { return &____plug_0; }
	inline void set__plug_0(PureQuaternionPlugin_t587122414 * value)
	{
		____plug_0 = value;
		Il2CppCodeGenWriteBarrier((&____plug_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUREQUATERNIONPLUGIN_T587122414_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef UNITYEVENT_1_T489719741_H
#define UNITYEVENT_1_T489719741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t489719741  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t489719741, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T489719741_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#define __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_t3297148301 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t3297148301__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#ifndef COLORTWEENMODE_T1000778859_H
#define COLORTWEENMODE_T1000778859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
struct  ColorTweenMode_t1000778859 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorTweenMode_t1000778859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_T1000778859_H
#ifndef INPUTMODE_T3382566315_H
#define INPUTMODE_T3382566315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.StandaloneInputModule/InputMode
struct  InputMode_t3382566315 
{
public:
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule/InputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputMode_t3382566315, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMODE_T3382566315_H
#ifndef FRAMEPRESSSTATE_T3039385657_H
#define FRAMEPRESSSTATE_T3039385657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/FramePressState
struct  FramePressState_t3039385657 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/FramePressState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FramePressState_t3039385657, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEPRESSSTATE_T3039385657_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef AUTOPLAY_T1346164433_H
#define AUTOPLAY_T1346164433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t1346164433 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoPlay_t1346164433, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T1346164433_H
#ifndef LOGBEHAVIOUR_T1548882435_H
#define LOGBEHAVIOUR_T1548882435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_t1548882435 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogBehaviour_t1548882435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_T1548882435_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef NULLABLE_1_T1149908250_H
#define NULLABLE_1_T1149908250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t1149908250 
{
public:
	// T System.Nullable`1::value
	Vector3_t3722313464  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1149908250, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1149908250, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1149908250_H
#ifndef PATHTYPE_T3777299409_H
#define PATHTYPE_T3777299409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t3777299409 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t3777299409, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T3777299409_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef EVENTHANDLE_T600343995_H
#define EVENTHANDLE_T600343995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t600343995 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventHandle_t600343995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_T600343995_H
#ifndef OPERATIONTYPE_T2152057980_H
#define OPERATIONTYPE_T2152057980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.OperationType
struct  OperationType_t2152057980 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.OperationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperationType_t2152057980, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONTYPE_T2152057980_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef UPDATENOTICE_T1001625488_H
#define UPDATENOTICE_T1001625488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.UpdateNotice
struct  UpdateNotice_t1001625488 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.UpdateNotice::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateNotice_t1001625488, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATENOTICE_T1001625488_H
#ifndef UPDATEMODE_T1164472539_H
#define UPDATEMODE_T1164472539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.UpdateMode
struct  UpdateMode_t1164472539 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.UpdateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateMode_t1164472539, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEMODE_T1164472539_H
#ifndef EVENTTRIGGERTYPE_T55832929_H
#define EVENTTRIGGERTYPE_T55832929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_t55832929 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventTriggerType_t55832929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGERTYPE_T55832929_H
#ifndef REWINDCALLBACKMODE_T4293633524_H
#define REWINDCALLBACKMODE_T4293633524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.RewindCallbackMode
struct  RewindCallbackMode_t4293633524 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.RewindCallbackMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RewindCallbackMode_t4293633524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWINDCALLBACKMODE_T4293633524_H
#ifndef TRIGGEREVENT_T3867320123_H
#define TRIGGEREVENT_T3867320123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct  TriggerEvent_t3867320123  : public UnityEvent_1_t489719741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T3867320123_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50 <PrivateImplementationDetails>::6F98278EFCD257898AD01BE39D1D0AEFB78FC551
	__StaticArrayInitTypeSizeU3D50_t1547932759  ___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8
	__StaticArrayInitTypeSizeU3D20_t1548391511  ___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::9DC5F4D0A1418B4EC71B22D21E93D134922FA735
	__StaticArrayInitTypeSizeU3D120_t3297148301  ___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50 <PrivateImplementationDetails>::FD0BD55CDDDFD0B323012A45F83437763AF58952
	__StaticArrayInitTypeSizeU3D50_t1547932759  ___FD0BD55CDDDFD0B323012A45F83437763AF58952_3;

public:
	inline static int32_t get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0)); }
	inline __StaticArrayInitTypeSizeU3D50_t1547932759  get_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0() const { return ___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0; }
	inline __StaticArrayInitTypeSizeU3D50_t1547932759 * get_address_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0() { return &___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0; }
	inline void set_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(__StaticArrayInitTypeSizeU3D50_t1547932759  value)
	{
		___6F98278EFCD257898AD01BE39D1D0AEFB78FC551_0 = value;
	}

	inline static int32_t get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1)); }
	inline __StaticArrayInitTypeSizeU3D20_t1548391511  get_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1() const { return ___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1; }
	inline __StaticArrayInitTypeSizeU3D20_t1548391511 * get_address_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1() { return &___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1; }
	inline void set_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(__StaticArrayInitTypeSizeU3D20_t1548391511  value)
	{
		___8C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1 = value;
	}

	inline static int32_t get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2)); }
	inline __StaticArrayInitTypeSizeU3D120_t3297148301  get_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2() const { return ___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2; }
	inline __StaticArrayInitTypeSizeU3D120_t3297148301 * get_address_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2() { return &___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2; }
	inline void set_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(__StaticArrayInitTypeSizeU3D120_t3297148301  value)
	{
		___9DC5F4D0A1418B4EC71B22D21E93D134922FA735_2 = value;
	}

	inline static int32_t get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___FD0BD55CDDDFD0B323012A45F83437763AF58952_3)); }
	inline __StaticArrayInitTypeSizeU3D50_t1547932759  get_FD0BD55CDDDFD0B323012A45F83437763AF58952_3() const { return ___FD0BD55CDDDFD0B323012A45F83437763AF58952_3; }
	inline __StaticArrayInitTypeSizeU3D50_t1547932759 * get_address_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3() { return &___FD0BD55CDDDFD0B323012A45F83437763AF58952_3; }
	inline void set_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(__StaticArrayInitTypeSizeU3D50_t1547932759  value)
	{
		___FD0BD55CDDDFD0B323012A45F83437763AF58952_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef MOVEDIRECTION_T1216237838_H
#define MOVEDIRECTION_T1216237838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t1216237838 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1216237838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1216237838_H
#ifndef CONTROLPOINT_T3892672090_H
#define CONTROLPOINT_T3892672090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ControlPoint
struct  ControlPoint_t3892672090 
{
public:
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::a
	Vector3_t3722313464  ___a_0;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::b
	Vector3_t3722313464  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ControlPoint_t3892672090, ___a_0)); }
	inline Vector3_t3722313464  get_a_0() const { return ___a_0; }
	inline Vector3_t3722313464 * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector3_t3722313464  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ControlPoint_t3892672090, ___b_1)); }
	inline Vector3_t3722313464  get_b_1() const { return ___b_1; }
	inline Vector3_t3722313464 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_t3722313464  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPOINT_T3892672090_H
#ifndef SETTINGSLOCATION_T2227561762_H
#define SETTINGSLOCATION_T2227561762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenSettings/SettingsLocation
struct  SettingsLocation_t2227561762 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenSettings/SettingsLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SettingsLocation_t2227561762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSLOCATION_T2227561762_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef CAPACITYINCREASEMODE_T828064684_H
#define CAPACITYINCREASEMODE_T828064684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
struct  CapacityIncreaseMode_t828064684 
{
public:
	// System.Int32 DG.Tweening.Core.TweenManager/CapacityIncreaseMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CapacityIncreaseMode_t828064684, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPACITYINCREASEMODE_T828064684_H
#ifndef FILTERTYPE_T3228157936_H
#define FILTERTYPE_T3228157936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.FilterType
struct  FilterType_t3228157936 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.FilterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterType_t3228157936, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERTYPE_T3228157936_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef VECTOROPTIONS_T1354903650_H
#define VECTOROPTIONS_T1354903650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t1354903650 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
#endif // VECTOROPTIONS_T1354903650_H
#ifndef PATH_T3614338981_H
#define PATH_T3614338981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.Path
struct  Path_t3614338981  : public RuntimeObject
{
public:
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t1444911251* ___wpLengths_2;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_3;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_4;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_5;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_t1718750761* ___wps_6;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t1567961855* ___controlPoints_7;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_8;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_9;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t1444911251* ___timesTable_10;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t1444911251* ___lengthsTable_11;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_12;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t3614338981 * ____incrementalClone_13;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_14;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_t2613982196 * ____decoder_15;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_16;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_t1718750761* ___nonLinearDrawWps_17;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t3722313464  ___targetPosition_18;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t1149908250  ___lookAtPosition_19;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_t2555686324  ___gizmoColor_20;

public:
	inline static int32_t get_offset_of_wpLengths_2() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___wpLengths_2)); }
	inline SingleU5BU5D_t1444911251* get_wpLengths_2() const { return ___wpLengths_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_wpLengths_2() { return &___wpLengths_2; }
	inline void set_wpLengths_2(SingleU5BU5D_t1444911251* value)
	{
		___wpLengths_2 = value;
		Il2CppCodeGenWriteBarrier((&___wpLengths_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_subdivisionsXSegment_4() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___subdivisionsXSegment_4)); }
	inline int32_t get_subdivisionsXSegment_4() const { return ___subdivisionsXSegment_4; }
	inline int32_t* get_address_of_subdivisionsXSegment_4() { return &___subdivisionsXSegment_4; }
	inline void set_subdivisionsXSegment_4(int32_t value)
	{
		___subdivisionsXSegment_4 = value;
	}

	inline static int32_t get_offset_of_subdivisions_5() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___subdivisions_5)); }
	inline int32_t get_subdivisions_5() const { return ___subdivisions_5; }
	inline int32_t* get_address_of_subdivisions_5() { return &___subdivisions_5; }
	inline void set_subdivisions_5(int32_t value)
	{
		___subdivisions_5 = value;
	}

	inline static int32_t get_offset_of_wps_6() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___wps_6)); }
	inline Vector3U5BU5D_t1718750761* get_wps_6() const { return ___wps_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_wps_6() { return &___wps_6; }
	inline void set_wps_6(Vector3U5BU5D_t1718750761* value)
	{
		___wps_6 = value;
		Il2CppCodeGenWriteBarrier((&___wps_6), value);
	}

	inline static int32_t get_offset_of_controlPoints_7() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___controlPoints_7)); }
	inline ControlPointU5BU5D_t1567961855* get_controlPoints_7() const { return ___controlPoints_7; }
	inline ControlPointU5BU5D_t1567961855** get_address_of_controlPoints_7() { return &___controlPoints_7; }
	inline void set_controlPoints_7(ControlPointU5BU5D_t1567961855* value)
	{
		___controlPoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_7), value);
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_isFinalized_9() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___isFinalized_9)); }
	inline bool get_isFinalized_9() const { return ___isFinalized_9; }
	inline bool* get_address_of_isFinalized_9() { return &___isFinalized_9; }
	inline void set_isFinalized_9(bool value)
	{
		___isFinalized_9 = value;
	}

	inline static int32_t get_offset_of_timesTable_10() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___timesTable_10)); }
	inline SingleU5BU5D_t1444911251* get_timesTable_10() const { return ___timesTable_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_timesTable_10() { return &___timesTable_10; }
	inline void set_timesTable_10(SingleU5BU5D_t1444911251* value)
	{
		___timesTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___timesTable_10), value);
	}

	inline static int32_t get_offset_of_lengthsTable_11() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___lengthsTable_11)); }
	inline SingleU5BU5D_t1444911251* get_lengthsTable_11() const { return ___lengthsTable_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_lengthsTable_11() { return &___lengthsTable_11; }
	inline void set_lengthsTable_11(SingleU5BU5D_t1444911251* value)
	{
		___lengthsTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___lengthsTable_11), value);
	}

	inline static int32_t get_offset_of_linearWPIndex_12() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___linearWPIndex_12)); }
	inline int32_t get_linearWPIndex_12() const { return ___linearWPIndex_12; }
	inline int32_t* get_address_of_linearWPIndex_12() { return &___linearWPIndex_12; }
	inline void set_linearWPIndex_12(int32_t value)
	{
		___linearWPIndex_12 = value;
	}

	inline static int32_t get_offset_of__incrementalClone_13() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____incrementalClone_13)); }
	inline Path_t3614338981 * get__incrementalClone_13() const { return ____incrementalClone_13; }
	inline Path_t3614338981 ** get_address_of__incrementalClone_13() { return &____incrementalClone_13; }
	inline void set__incrementalClone_13(Path_t3614338981 * value)
	{
		____incrementalClone_13 = value;
		Il2CppCodeGenWriteBarrier((&____incrementalClone_13), value);
	}

	inline static int32_t get_offset_of__incrementalIndex_14() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____incrementalIndex_14)); }
	inline int32_t get__incrementalIndex_14() const { return ____incrementalIndex_14; }
	inline int32_t* get_address_of__incrementalIndex_14() { return &____incrementalIndex_14; }
	inline void set__incrementalIndex_14(int32_t value)
	{
		____incrementalIndex_14 = value;
	}

	inline static int32_t get_offset_of__decoder_15() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____decoder_15)); }
	inline ABSPathDecoder_t2613982196 * get__decoder_15() const { return ____decoder_15; }
	inline ABSPathDecoder_t2613982196 ** get_address_of__decoder_15() { return &____decoder_15; }
	inline void set__decoder_15(ABSPathDecoder_t2613982196 * value)
	{
		____decoder_15 = value;
		Il2CppCodeGenWriteBarrier((&____decoder_15), value);
	}

	inline static int32_t get_offset_of__changed_16() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____changed_16)); }
	inline bool get__changed_16() const { return ____changed_16; }
	inline bool* get_address_of__changed_16() { return &____changed_16; }
	inline void set__changed_16(bool value)
	{
		____changed_16 = value;
	}

	inline static int32_t get_offset_of_nonLinearDrawWps_17() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___nonLinearDrawWps_17)); }
	inline Vector3U5BU5D_t1718750761* get_nonLinearDrawWps_17() const { return ___nonLinearDrawWps_17; }
	inline Vector3U5BU5D_t1718750761** get_address_of_nonLinearDrawWps_17() { return &___nonLinearDrawWps_17; }
	inline void set_nonLinearDrawWps_17(Vector3U5BU5D_t1718750761* value)
	{
		___nonLinearDrawWps_17 = value;
		Il2CppCodeGenWriteBarrier((&___nonLinearDrawWps_17), value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___targetPosition_18)); }
	inline Vector3_t3722313464  get_targetPosition_18() const { return ___targetPosition_18; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(Vector3_t3722313464  value)
	{
		___targetPosition_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_19() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___lookAtPosition_19)); }
	inline Nullable_1_t1149908250  get_lookAtPosition_19() const { return ___lookAtPosition_19; }
	inline Nullable_1_t1149908250 * get_address_of_lookAtPosition_19() { return &___lookAtPosition_19; }
	inline void set_lookAtPosition_19(Nullable_1_t1149908250  value)
	{
		___lookAtPosition_19 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_20() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___gizmoColor_20)); }
	inline Color_t2555686324  get_gizmoColor_20() const { return ___gizmoColor_20; }
	inline Color_t2555686324 * get_address_of_gizmoColor_20() { return &___gizmoColor_20; }
	inline void set_gizmoColor_20(Color_t2555686324  value)
	{
		___gizmoColor_20 = value;
	}
};

struct Path_t3614338981_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder DG.Tweening.Plugins.Core.PathCore.Path::_catmullRomDecoder
	CatmullRomDecoder_t2053048079 * ____catmullRomDecoder_0;
	// DG.Tweening.Plugins.Core.PathCore.LinearDecoder DG.Tweening.Plugins.Core.PathCore.Path::_linearDecoder
	LinearDecoder_t2708327777 * ____linearDecoder_1;

public:
	inline static int32_t get_offset_of__catmullRomDecoder_0() { return static_cast<int32_t>(offsetof(Path_t3614338981_StaticFields, ____catmullRomDecoder_0)); }
	inline CatmullRomDecoder_t2053048079 * get__catmullRomDecoder_0() const { return ____catmullRomDecoder_0; }
	inline CatmullRomDecoder_t2053048079 ** get_address_of__catmullRomDecoder_0() { return &____catmullRomDecoder_0; }
	inline void set__catmullRomDecoder_0(CatmullRomDecoder_t2053048079 * value)
	{
		____catmullRomDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&____catmullRomDecoder_0), value);
	}

	inline static int32_t get_offset_of__linearDecoder_1() { return static_cast<int32_t>(offsetof(Path_t3614338981_StaticFields, ____linearDecoder_1)); }
	inline LinearDecoder_t2708327777 * get__linearDecoder_1() const { return ____linearDecoder_1; }
	inline LinearDecoder_t2708327777 ** get_address_of__linearDecoder_1() { return &____linearDecoder_1; }
	inline void set__linearDecoder_1(LinearDecoder_t2708327777 * value)
	{
		____linearDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((&____linearDecoder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T3614338981_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef AXISEVENTDATA_T2331243652_H
#define AXISEVENTDATA_T2331243652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t2331243652  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_t2156229523  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t2331243652, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_t2156229523  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_t2156229523 * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_t2156229523  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t2331243652, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENTDATA_T2331243652_H
#ifndef BUTTONSTATE_T857139936_H
#define BUTTONSTATE_T857139936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct  ButtonState_t857139936  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerInputModule/ButtonState::m_Button
	int32_t ___m_Button_0;
	// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData UnityEngine.EventSystems.PointerInputModule/ButtonState::m_EventData
	MouseButtonEventData_t3190347560 * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_t857139936, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_t857139936, ___m_EventData_1)); }
	inline MouseButtonEventData_t3190347560 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_t3190347560 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_t3190347560 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATE_T857139936_H
#ifndef MOUSEBUTTONEVENTDATA_T3190347560_H
#define MOUSEBUTTONEVENTDATA_T3190347560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct  MouseButtonEventData_t3190347560  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonData
	PointerEventData_t3807901092 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3190347560, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3190347560, ___buttonData_1)); }
	inline PointerEventData_t3807901092 * get_buttonData_1() const { return ___buttonData_1; }
	inline PointerEventData_t3807901092 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PointerEventData_t3807901092 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttonData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONEVENTDATA_T3190347560_H
#ifndef COLORTWEEN_T809614380_H
#define COLORTWEEN_T809614380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t809614380 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_t1121741130 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t2555686324  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t2555686324  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_Target_0)); }
	inline ColorTweenCallback_t1121741130 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_t1121741130 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_t1121741130 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_StartColor_1)); }
	inline Color_t2555686324  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t2555686324 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t2555686324  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_TargetColor_2)); }
	inline Color_t2555686324  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t2555686324 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t2555686324  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t809614380_marshaled_pinvoke
{
	ColorTweenCallback_t1121741130 * ___m_Target_0;
	Color_t2555686324  ___m_StartColor_1;
	Color_t2555686324  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t809614380_marshaled_com
{
	ColorTweenCallback_t1121741130 * ___m_Target_0;
	Color_t2555686324  ___m_StartColor_1;
	Color_t2555686324  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T809614380_H
#ifndef STRINGOPTIONS_T3992490940_H
#define STRINGOPTIONS_T3992490940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.StringOptions
struct  StringOptions_t3992490940 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t3528271667* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;

public:
	inline static int32_t get_offset_of_richTextEnabled_0() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___richTextEnabled_0)); }
	inline bool get_richTextEnabled_0() const { return ___richTextEnabled_0; }
	inline bool* get_address_of_richTextEnabled_0() { return &___richTextEnabled_0; }
	inline void set_richTextEnabled_0(bool value)
	{
		___richTextEnabled_0 = value;
	}

	inline static int32_t get_offset_of_scrambleMode_1() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___scrambleMode_1)); }
	inline int32_t get_scrambleMode_1() const { return ___scrambleMode_1; }
	inline int32_t* get_address_of_scrambleMode_1() { return &___scrambleMode_1; }
	inline void set_scrambleMode_1(int32_t value)
	{
		___scrambleMode_1 = value;
	}

	inline static int32_t get_offset_of_scrambledChars_2() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___scrambledChars_2)); }
	inline CharU5BU5D_t3528271667* get_scrambledChars_2() const { return ___scrambledChars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_scrambledChars_2() { return &___scrambledChars_2; }
	inline void set_scrambledChars_2(CharU5BU5D_t3528271667* value)
	{
		___scrambledChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrambledChars_2), value);
	}

	inline static int32_t get_offset_of_startValueStrippedLength_3() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___startValueStrippedLength_3)); }
	inline int32_t get_startValueStrippedLength_3() const { return ___startValueStrippedLength_3; }
	inline int32_t* get_address_of_startValueStrippedLength_3() { return &___startValueStrippedLength_3; }
	inline void set_startValueStrippedLength_3(int32_t value)
	{
		___startValueStrippedLength_3 = value;
	}

	inline static int32_t get_offset_of_changeValueStrippedLength_4() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___changeValueStrippedLength_4)); }
	inline int32_t get_changeValueStrippedLength_4() const { return ___changeValueStrippedLength_4; }
	inline int32_t* get_address_of_changeValueStrippedLength_4() { return &___changeValueStrippedLength_4; }
	inline void set_changeValueStrippedLength_4(int32_t value)
	{
		___changeValueStrippedLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t3992490940_marshaled_pinvoke
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t3992490940_marshaled_com
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
#endif // STRINGOPTIONS_T3992490940_H
#ifndef ENTRY_T3344766165_H
#define ENTRY_T3344766165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger/Entry
struct  Entry_t3344766165  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger/Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger/TriggerEvent UnityEngine.EventSystems.EventTrigger/Entry::callback
	TriggerEvent_t3867320123 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t3344766165, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t3344766165, ___callback_1)); }
	inline TriggerEvent_t3867320123 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_t3867320123 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_t3867320123 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3344766165_H
#ifndef DOTWEENSETTINGS_T4078947542_H
#define DOTWEENSETTINGS_T4078947542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenSettings
struct  DOTweenSettings_t4078947542  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSafeMode
	bool ___useSafeMode_3;
	// System.Single DG.Tweening.Core.DOTweenSettings::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_5;
	// System.Single DG.Tweening.Core.DOTweenSettings::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_6;
	// DG.Tweening.Core.Enums.RewindCallbackMode DG.Tweening.Core.DOTweenSettings::rewindCallbackMode
	int32_t ___rewindCallbackMode_7;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showUnityEditorReport
	bool ___showUnityEditorReport_8;
	// DG.Tweening.LogBehaviour DG.Tweening.Core.DOTweenSettings::logBehaviour
	int32_t ___logBehaviour_9;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::drawGizmos
	bool ___drawGizmos_10;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultRecyclable
	bool ___defaultRecyclable_11;
	// DG.Tweening.AutoPlay DG.Tweening.Core.DOTweenSettings::defaultAutoPlay
	int32_t ___defaultAutoPlay_12;
	// DG.Tweening.UpdateType DG.Tweening.Core.DOTweenSettings::defaultUpdateType
	int32_t ___defaultUpdateType_13;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_14;
	// DG.Tweening.Ease DG.Tweening.Core.DOTweenSettings::defaultEaseType
	int32_t ___defaultEaseType_15;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_16;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEasePeriod
	float ___defaultEasePeriod_17;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultAutoKill
	bool ___defaultAutoKill_18;
	// DG.Tweening.LoopType DG.Tweening.Core.DOTweenSettings::defaultLoopType
	int32_t ___defaultLoopType_19;
	// DG.Tweening.Core.DOTweenSettings/SettingsLocation DG.Tweening.Core.DOTweenSettings::storeSettingsLocation
	int32_t ___storeSettingsLocation_20;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showPlayingTweens
	bool ___showPlayingTweens_21;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showPausedTweens
	bool ___showPausedTweens_22;

public:
	inline static int32_t get_offset_of_useSafeMode_3() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___useSafeMode_3)); }
	inline bool get_useSafeMode_3() const { return ___useSafeMode_3; }
	inline bool* get_address_of_useSafeMode_3() { return &___useSafeMode_3; }
	inline void set_useSafeMode_3(bool value)
	{
		___useSafeMode_3 = value;
	}

	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_5() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___useSmoothDeltaTime_5)); }
	inline bool get_useSmoothDeltaTime_5() const { return ___useSmoothDeltaTime_5; }
	inline bool* get_address_of_useSmoothDeltaTime_5() { return &___useSmoothDeltaTime_5; }
	inline void set_useSmoothDeltaTime_5(bool value)
	{
		___useSmoothDeltaTime_5 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_6() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___maxSmoothUnscaledTime_6)); }
	inline float get_maxSmoothUnscaledTime_6() const { return ___maxSmoothUnscaledTime_6; }
	inline float* get_address_of_maxSmoothUnscaledTime_6() { return &___maxSmoothUnscaledTime_6; }
	inline void set_maxSmoothUnscaledTime_6(float value)
	{
		___maxSmoothUnscaledTime_6 = value;
	}

	inline static int32_t get_offset_of_rewindCallbackMode_7() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___rewindCallbackMode_7)); }
	inline int32_t get_rewindCallbackMode_7() const { return ___rewindCallbackMode_7; }
	inline int32_t* get_address_of_rewindCallbackMode_7() { return &___rewindCallbackMode_7; }
	inline void set_rewindCallbackMode_7(int32_t value)
	{
		___rewindCallbackMode_7 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_8() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___showUnityEditorReport_8)); }
	inline bool get_showUnityEditorReport_8() const { return ___showUnityEditorReport_8; }
	inline bool* get_address_of_showUnityEditorReport_8() { return &___showUnityEditorReport_8; }
	inline void set_showUnityEditorReport_8(bool value)
	{
		___showUnityEditorReport_8 = value;
	}

	inline static int32_t get_offset_of_logBehaviour_9() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___logBehaviour_9)); }
	inline int32_t get_logBehaviour_9() const { return ___logBehaviour_9; }
	inline int32_t* get_address_of_logBehaviour_9() { return &___logBehaviour_9; }
	inline void set_logBehaviour_9(int32_t value)
	{
		___logBehaviour_9 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_10() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___drawGizmos_10)); }
	inline bool get_drawGizmos_10() const { return ___drawGizmos_10; }
	inline bool* get_address_of_drawGizmos_10() { return &___drawGizmos_10; }
	inline void set_drawGizmos_10(bool value)
	{
		___drawGizmos_10 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_11() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultRecyclable_11)); }
	inline bool get_defaultRecyclable_11() const { return ___defaultRecyclable_11; }
	inline bool* get_address_of_defaultRecyclable_11() { return &___defaultRecyclable_11; }
	inline void set_defaultRecyclable_11(bool value)
	{
		___defaultRecyclable_11 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_12() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultAutoPlay_12)); }
	inline int32_t get_defaultAutoPlay_12() const { return ___defaultAutoPlay_12; }
	inline int32_t* get_address_of_defaultAutoPlay_12() { return &___defaultAutoPlay_12; }
	inline void set_defaultAutoPlay_12(int32_t value)
	{
		___defaultAutoPlay_12 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_13() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultUpdateType_13)); }
	inline int32_t get_defaultUpdateType_13() const { return ___defaultUpdateType_13; }
	inline int32_t* get_address_of_defaultUpdateType_13() { return &___defaultUpdateType_13; }
	inline void set_defaultUpdateType_13(int32_t value)
	{
		___defaultUpdateType_13 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_14() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultTimeScaleIndependent_14)); }
	inline bool get_defaultTimeScaleIndependent_14() const { return ___defaultTimeScaleIndependent_14; }
	inline bool* get_address_of_defaultTimeScaleIndependent_14() { return &___defaultTimeScaleIndependent_14; }
	inline void set_defaultTimeScaleIndependent_14(bool value)
	{
		___defaultTimeScaleIndependent_14 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_15() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultEaseType_15)); }
	inline int32_t get_defaultEaseType_15() const { return ___defaultEaseType_15; }
	inline int32_t* get_address_of_defaultEaseType_15() { return &___defaultEaseType_15; }
	inline void set_defaultEaseType_15(int32_t value)
	{
		___defaultEaseType_15 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_16() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultEaseOvershootOrAmplitude_16)); }
	inline float get_defaultEaseOvershootOrAmplitude_16() const { return ___defaultEaseOvershootOrAmplitude_16; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_16() { return &___defaultEaseOvershootOrAmplitude_16; }
	inline void set_defaultEaseOvershootOrAmplitude_16(float value)
	{
		___defaultEaseOvershootOrAmplitude_16 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_17() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultEasePeriod_17)); }
	inline float get_defaultEasePeriod_17() const { return ___defaultEasePeriod_17; }
	inline float* get_address_of_defaultEasePeriod_17() { return &___defaultEasePeriod_17; }
	inline void set_defaultEasePeriod_17(float value)
	{
		___defaultEasePeriod_17 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_18() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultAutoKill_18)); }
	inline bool get_defaultAutoKill_18() const { return ___defaultAutoKill_18; }
	inline bool* get_address_of_defaultAutoKill_18() { return &___defaultAutoKill_18; }
	inline void set_defaultAutoKill_18(bool value)
	{
		___defaultAutoKill_18 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_19() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___defaultLoopType_19)); }
	inline int32_t get_defaultLoopType_19() const { return ___defaultLoopType_19; }
	inline int32_t* get_address_of_defaultLoopType_19() { return &___defaultLoopType_19; }
	inline void set_defaultLoopType_19(int32_t value)
	{
		___defaultLoopType_19 = value;
	}

	inline static int32_t get_offset_of_storeSettingsLocation_20() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___storeSettingsLocation_20)); }
	inline int32_t get_storeSettingsLocation_20() const { return ___storeSettingsLocation_20; }
	inline int32_t* get_address_of_storeSettingsLocation_20() { return &___storeSettingsLocation_20; }
	inline void set_storeSettingsLocation_20(int32_t value)
	{
		___storeSettingsLocation_20 = value;
	}

	inline static int32_t get_offset_of_showPlayingTweens_21() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___showPlayingTweens_21)); }
	inline bool get_showPlayingTweens_21() const { return ___showPlayingTweens_21; }
	inline bool* get_address_of_showPlayingTweens_21() { return &___showPlayingTweens_21; }
	inline void set_showPlayingTweens_21(bool value)
	{
		___showPlayingTweens_21 = value;
	}

	inline static int32_t get_offset_of_showPausedTweens_22() { return static_cast<int32_t>(offsetof(DOTweenSettings_t4078947542, ___showPausedTweens_22)); }
	inline bool get_showPausedTweens_22() const { return ___showPausedTweens_22; }
	inline bool* get_address_of_showPausedTweens_22() { return &___showPausedTweens_22; }
	inline void set_showPausedTweens_22(bool value)
	{
		___showPausedTweens_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENSETTINGS_T4078947542_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef SEQUENCECALLBACK_T1025787276_H
#define SEQUENCECALLBACK_T1025787276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.SequenceCallback
struct  SequenceCallback_t1025787276  : public ABSSequentiable_t3376041011
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCECALLBACK_T1025787276_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef EVENTTRIGGER_T1076084509_H
#define EVENTTRIGGER_T1076084509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_t1076084509  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t521873611 * ___m_Delegates_2;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::delegates
	List_1_t521873611 * ___delegates_3;

public:
	inline static int32_t get_offset_of_m_Delegates_2() { return static_cast<int32_t>(offsetof(EventTrigger_t1076084509, ___m_Delegates_2)); }
	inline List_1_t521873611 * get_m_Delegates_2() const { return ___m_Delegates_2; }
	inline List_1_t521873611 ** get_address_of_m_Delegates_2() { return &___m_Delegates_2; }
	inline void set_m_Delegates_2(List_1_t521873611 * value)
	{
		___m_Delegates_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegates_2), value);
	}

	inline static int32_t get_offset_of_delegates_3() { return static_cast<int32_t>(offsetof(EventTrigger_t1076084509, ___delegates_3)); }
	inline List_1_t521873611 * get_delegates_3() const { return ___delegates_3; }
	inline List_1_t521873611 ** get_address_of_delegates_3() { return &___delegates_3; }
	inline void set_delegates_3(List_1_t521873611 * value)
	{
		___delegates_3 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T1076084509_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef DOTWEENCOMPONENT_T828035757_H
#define DOTWEENCOMPONENT_T828035757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent
struct  DOTweenComponent_t828035757  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent::inspectorUpdater
	int32_t ___inspectorUpdater_2;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledTime
	float ____unscaledTime_3;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledDeltaTime
	float ____unscaledDeltaTime_4;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_duplicateToDestroy
	bool ____duplicateToDestroy_5;

public:
	inline static int32_t get_offset_of_inspectorUpdater_2() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ___inspectorUpdater_2)); }
	inline int32_t get_inspectorUpdater_2() const { return ___inspectorUpdater_2; }
	inline int32_t* get_address_of_inspectorUpdater_2() { return &___inspectorUpdater_2; }
	inline void set_inspectorUpdater_2(int32_t value)
	{
		___inspectorUpdater_2 = value;
	}

	inline static int32_t get_offset_of__unscaledTime_3() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ____unscaledTime_3)); }
	inline float get__unscaledTime_3() const { return ____unscaledTime_3; }
	inline float* get_address_of__unscaledTime_3() { return &____unscaledTime_3; }
	inline void set__unscaledTime_3(float value)
	{
		____unscaledTime_3 = value;
	}

	inline static int32_t get_offset_of__unscaledDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ____unscaledDeltaTime_4)); }
	inline float get__unscaledDeltaTime_4() const { return ____unscaledDeltaTime_4; }
	inline float* get_address_of__unscaledDeltaTime_4() { return &____unscaledDeltaTime_4; }
	inline void set__unscaledDeltaTime_4(float value)
	{
		____unscaledDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of__duplicateToDestroy_5() { return static_cast<int32_t>(offsetof(DOTweenComponent_t828035757, ____duplicateToDestroy_5)); }
	inline bool get__duplicateToDestroy_5() const { return ____duplicateToDestroy_5; }
	inline bool* get_address_of__duplicateToDestroy_5() { return &____duplicateToDestroy_5; }
	inline void set__duplicateToDestroy_5(bool value)
	{
		____duplicateToDestroy_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENCOMPONENT_T828035757_H
#ifndef EVENTSYSTEM_T1003666588_H
#define EVENTSYSTEM_T1003666588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t1003666588  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t3491343620 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t2019268878 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1113636619 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1113636619 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t3903027533 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SystemInputModules_2)); }
	inline List_1_t3491343620 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t3491343620 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t3491343620 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t2019268878 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t2019268878 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t2019268878 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_FirstSelected_5)); }
	inline GameObject_t1113636619 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1113636619 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1113636619 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentSelected_8)); }
	inline GameObject_t1113636619 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1113636619 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DummyData_11)); }
	inline BaseEventData_t3903027533 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t3903027533 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t3903027533 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t1003666588_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2475741330 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t3135238028 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2475741330 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2475741330 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2475741330 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t3135238028 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t3135238028 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t3135238028 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T1003666588_H
#ifndef BASEINPUT_T3630163547_H
#define BASEINPUT_T3630163547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInput
struct  BaseInput_t3630163547  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_T3630163547_H
#ifndef BASERAYCASTER_T4150874583_H
#define BASERAYCASTER_T4150874583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t4150874583  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T4150874583_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef POINTERINPUTMODULE_T3453173740_H
#define POINTERINPUTMODULE_T3453173740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t3453173740  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t2696614423 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_PointerData_12)); }
	inline Dictionary_2_t2696614423 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t2696614423 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t2696614423 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_MouseState_13)); }
	inline MouseState_t384203932 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t384203932 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T3453173740_H
#ifndef PHYSICSRAYCASTER_T437419520_H
#define PHYSICSRAYCASTER_T437419520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PhysicsRaycaster
struct  PhysicsRaycaster_t437419520  : public BaseRaycaster_t4150874583
{
public:
	// UnityEngine.Camera UnityEngine.EventSystems.PhysicsRaycaster::m_EventCamera
	Camera_t4157153871 * ___m_EventCamera_3;
	// UnityEngine.LayerMask UnityEngine.EventSystems.PhysicsRaycaster::m_EventMask
	LayerMask_t3493934918  ___m_EventMask_4;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_MaxRayIntersections
	int32_t ___m_MaxRayIntersections_5;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_LastMaxRayIntersections
	int32_t ___m_LastMaxRayIntersections_6;
	// UnityEngine.RaycastHit[] UnityEngine.EventSystems.PhysicsRaycaster::m_Hits
	RaycastHitU5BU5D_t1690781147* ___m_Hits_7;

public:
	inline static int32_t get_offset_of_m_EventCamera_3() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_EventCamera_3)); }
	inline Camera_t4157153871 * get_m_EventCamera_3() const { return ___m_EventCamera_3; }
	inline Camera_t4157153871 ** get_address_of_m_EventCamera_3() { return &___m_EventCamera_3; }
	inline void set_m_EventCamera_3(Camera_t4157153871 * value)
	{
		___m_EventCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventCamera_3), value);
	}

	inline static int32_t get_offset_of_m_EventMask_4() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_EventMask_4)); }
	inline LayerMask_t3493934918  get_m_EventMask_4() const { return ___m_EventMask_4; }
	inline LayerMask_t3493934918 * get_address_of_m_EventMask_4() { return &___m_EventMask_4; }
	inline void set_m_EventMask_4(LayerMask_t3493934918  value)
	{
		___m_EventMask_4 = value;
	}

	inline static int32_t get_offset_of_m_MaxRayIntersections_5() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_MaxRayIntersections_5)); }
	inline int32_t get_m_MaxRayIntersections_5() const { return ___m_MaxRayIntersections_5; }
	inline int32_t* get_address_of_m_MaxRayIntersections_5() { return &___m_MaxRayIntersections_5; }
	inline void set_m_MaxRayIntersections_5(int32_t value)
	{
		___m_MaxRayIntersections_5 = value;
	}

	inline static int32_t get_offset_of_m_LastMaxRayIntersections_6() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_LastMaxRayIntersections_6)); }
	inline int32_t get_m_LastMaxRayIntersections_6() const { return ___m_LastMaxRayIntersections_6; }
	inline int32_t* get_address_of_m_LastMaxRayIntersections_6() { return &___m_LastMaxRayIntersections_6; }
	inline void set_m_LastMaxRayIntersections_6(int32_t value)
	{
		___m_LastMaxRayIntersections_6 = value;
	}

	inline static int32_t get_offset_of_m_Hits_7() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_Hits_7)); }
	inline RaycastHitU5BU5D_t1690781147* get_m_Hits_7() const { return ___m_Hits_7; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_m_Hits_7() { return &___m_Hits_7; }
	inline void set_m_Hits_7(RaycastHitU5BU5D_t1690781147* value)
	{
		___m_Hits_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hits_7), value);
	}
};

struct PhysicsRaycaster_t437419520_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.RaycastHit> UnityEngine.EventSystems.PhysicsRaycaster::<>f__am$cache0
	Comparison_1_t830933145 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Comparison_1_t830933145 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Comparison_1_t830933145 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Comparison_1_t830933145 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSRAYCASTER_T437419520_H
#ifndef TOUCHINPUTMODULE_T4248229598_H
#define TOUCHINPUTMODULE_T4248229598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.TouchInputModule
struct  TouchInputModule_t4248229598  : public PointerInputModule_t3453173740
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_15;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.TouchInputModule::m_InputPointerEvent
	PointerEventData_t3807901092 * ___m_InputPointerEvent_16;
	// System.Boolean UnityEngine.EventSystems.TouchInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_17;

public:
	inline static int32_t get_offset_of_m_LastMousePosition_14() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_LastMousePosition_14)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_14() const { return ___m_LastMousePosition_14; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_14() { return &___m_LastMousePosition_14; }
	inline void set_m_LastMousePosition_14(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_14 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_15() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_MousePosition_15)); }
	inline Vector2_t2156229523  get_m_MousePosition_15() const { return ___m_MousePosition_15; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_15() { return &___m_MousePosition_15; }
	inline void set_m_MousePosition_15(Vector2_t2156229523  value)
	{
		___m_MousePosition_15 = value;
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_16() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_InputPointerEvent_16)); }
	inline PointerEventData_t3807901092 * get_m_InputPointerEvent_16() const { return ___m_InputPointerEvent_16; }
	inline PointerEventData_t3807901092 ** get_address_of_m_InputPointerEvent_16() { return &___m_InputPointerEvent_16; }
	inline void set_m_InputPointerEvent_16(PointerEventData_t3807901092 * value)
	{
		___m_InputPointerEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputPointerEvent_16), value);
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_17() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_ForceModuleActive_17)); }
	inline bool get_m_ForceModuleActive_17() const { return ___m_ForceModuleActive_17; }
	inline bool* get_address_of_m_ForceModuleActive_17() { return &___m_ForceModuleActive_17; }
	inline void set_m_ForceModuleActive_17(bool value)
	{
		___m_ForceModuleActive_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINPUTMODULE_T4248229598_H
#ifndef STANDALONEINPUTMODULE_T2760469101_H
#define STANDALONEINPUTMODULE_T2760469101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.StandaloneInputModule
struct  StandaloneInputModule_t2760469101  : public PointerInputModule_t3453173740
{
public:
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMoveVector
	Vector2_t2156229523  ___m_LastMoveVector_15;
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_17;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_18;
	// UnityEngine.GameObject UnityEngine.EventSystems.StandaloneInputModule::m_CurrentFocusedGameObject
	GameObject_t1113636619 * ___m_CurrentFocusedGameObject_19;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.StandaloneInputModule::m_InputPointerEvent
	PointerEventData_t3807901092 * ___m_InputPointerEvent_20;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_21;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_22;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_23;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_24;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_25;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_26;
	// System.Boolean UnityEngine.EventSystems.StandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_27;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_14() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_PrevActionTime_14)); }
	inline float get_m_PrevActionTime_14() const { return ___m_PrevActionTime_14; }
	inline float* get_address_of_m_PrevActionTime_14() { return &___m_PrevActionTime_14; }
	inline void set_m_PrevActionTime_14(float value)
	{
		___m_PrevActionTime_14 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_15() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMoveVector_15)); }
	inline Vector2_t2156229523  get_m_LastMoveVector_15() const { return ___m_LastMoveVector_15; }
	inline Vector2_t2156229523 * get_address_of_m_LastMoveVector_15() { return &___m_LastMoveVector_15; }
	inline void set_m_LastMoveVector_15(Vector2_t2156229523  value)
	{
		___m_LastMoveVector_15 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_16() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ConsecutiveMoveCount_16)); }
	inline int32_t get_m_ConsecutiveMoveCount_16() const { return ___m_ConsecutiveMoveCount_16; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_16() { return &___m_ConsecutiveMoveCount_16; }
	inline void set_m_ConsecutiveMoveCount_16(int32_t value)
	{
		___m_ConsecutiveMoveCount_16 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_17() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMousePosition_17)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_17() const { return ___m_LastMousePosition_17; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_17() { return &___m_LastMousePosition_17; }
	inline void set_m_LastMousePosition_17(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_17 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_18() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_MousePosition_18)); }
	inline Vector2_t2156229523  get_m_MousePosition_18() const { return ___m_MousePosition_18; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_18() { return &___m_MousePosition_18; }
	inline void set_m_MousePosition_18(Vector2_t2156229523  value)
	{
		___m_MousePosition_18 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFocusedGameObject_19() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CurrentFocusedGameObject_19)); }
	inline GameObject_t1113636619 * get_m_CurrentFocusedGameObject_19() const { return ___m_CurrentFocusedGameObject_19; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentFocusedGameObject_19() { return &___m_CurrentFocusedGameObject_19; }
	inline void set_m_CurrentFocusedGameObject_19(GameObject_t1113636619 * value)
	{
		___m_CurrentFocusedGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentFocusedGameObject_19), value);
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_20() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_InputPointerEvent_20)); }
	inline PointerEventData_t3807901092 * get_m_InputPointerEvent_20() const { return ___m_InputPointerEvent_20; }
	inline PointerEventData_t3807901092 ** get_address_of_m_InputPointerEvent_20() { return &___m_InputPointerEvent_20; }
	inline void set_m_InputPointerEvent_20(PointerEventData_t3807901092 * value)
	{
		___m_InputPointerEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputPointerEvent_20), value);
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_21() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_HorizontalAxis_21)); }
	inline String_t* get_m_HorizontalAxis_21() const { return ___m_HorizontalAxis_21; }
	inline String_t** get_address_of_m_HorizontalAxis_21() { return &___m_HorizontalAxis_21; }
	inline void set_m_HorizontalAxis_21(String_t* value)
	{
		___m_HorizontalAxis_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_21), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_22() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_VerticalAxis_22)); }
	inline String_t* get_m_VerticalAxis_22() const { return ___m_VerticalAxis_22; }
	inline String_t** get_address_of_m_VerticalAxis_22() { return &___m_VerticalAxis_22; }
	inline void set_m_VerticalAxis_22(String_t* value)
	{
		___m_VerticalAxis_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_22), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_23() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_SubmitButton_23)); }
	inline String_t* get_m_SubmitButton_23() const { return ___m_SubmitButton_23; }
	inline String_t** get_address_of_m_SubmitButton_23() { return &___m_SubmitButton_23; }
	inline void set_m_SubmitButton_23(String_t* value)
	{
		___m_SubmitButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_23), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_24() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CancelButton_24)); }
	inline String_t* get_m_CancelButton_24() const { return ___m_CancelButton_24; }
	inline String_t** get_address_of_m_CancelButton_24() { return &___m_CancelButton_24; }
	inline void set_m_CancelButton_24(String_t* value)
	{
		___m_CancelButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_24), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_25() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_InputActionsPerSecond_25)); }
	inline float get_m_InputActionsPerSecond_25() const { return ___m_InputActionsPerSecond_25; }
	inline float* get_address_of_m_InputActionsPerSecond_25() { return &___m_InputActionsPerSecond_25; }
	inline void set_m_InputActionsPerSecond_25(float value)
	{
		___m_InputActionsPerSecond_25 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_26() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_RepeatDelay_26)); }
	inline float get_m_RepeatDelay_26() const { return ___m_RepeatDelay_26; }
	inline float* get_address_of_m_RepeatDelay_26() { return &___m_RepeatDelay_26; }
	inline void set_m_RepeatDelay_26(float value)
	{
		___m_RepeatDelay_26 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_27() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ForceModuleActive_27)); }
	inline bool get_m_ForceModuleActive_27() const { return ___m_ForceModuleActive_27; }
	inline bool* get_address_of_m_ForceModuleActive_27() { return &___m_ForceModuleActive_27; }
	inline void set_m_ForceModuleActive_27(bool value)
	{
		___m_ForceModuleActive_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUTMODULE_T2760469101_H
#ifndef PHYSICS2DRAYCASTER_T3382992964_H
#define PHYSICS2DRAYCASTER_T3382992964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.Physics2DRaycaster
struct  Physics2DRaycaster_t3382992964  : public PhysicsRaycaster_t437419520
{
public:
	// UnityEngine.RaycastHit2D[] UnityEngine.EventSystems.Physics2DRaycaster::m_Hits
	RaycastHit2DU5BU5D_t4286651560* ___m_Hits_9;

public:
	inline static int32_t get_offset_of_m_Hits_9() { return static_cast<int32_t>(offsetof(Physics2DRaycaster_t3382992964, ___m_Hits_9)); }
	inline RaycastHit2DU5BU5D_t4286651560* get_m_Hits_9() const { return ___m_Hits_9; }
	inline RaycastHit2DU5BU5D_t4286651560** get_address_of_m_Hits_9() { return &___m_Hits_9; }
	inline void set_m_Hits_9(RaycastHit2DU5BU5D_t4286651560* value)
	{
		___m_Hits_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hits_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2DRAYCASTER_T3382992964_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (StringOptions_t3992490940)+ sizeof (RuntimeObject), sizeof(StringOptions_t3992490940_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2000[5] = 
{
	StringOptions_t3992490940::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t3992490940::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (VectorOptions_t1354903650)+ sizeof (RuntimeObject), sizeof(VectorOptions_t1354903650_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2001[2] = 
{
	VectorOptions_t1354903650::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VectorOptions_t1354903650::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (SpecialPluginsUtils_t1116177272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (PluginsManager_t1753812699), -1, sizeof(PluginsManager_t1753812699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2007[18] = 
{
	PluginsManager_t1753812699_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t1753812699_StaticFields::get_offset_of__color2Plugin_15(),
	0,
	PluginsManager_t1753812699_StaticFields::get_offset_of__customPlugins_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (ControlPoint_t3892672090)+ sizeof (RuntimeObject), sizeof(ControlPoint_t3892672090 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	ControlPoint_t3892672090::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControlPoint_t3892672090::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (ABSPathDecoder_t2613982196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (CatmullRomDecoder_t2053048079), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (LinearDecoder_t2708327777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (Path_t3614338981), -1, sizeof(Path_t3614338981_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2012[21] = 
{
	Path_t3614338981_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t3614338981_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t3614338981::get_offset_of_wpLengths_2(),
	Path_t3614338981::get_offset_of_type_3(),
	Path_t3614338981::get_offset_of_subdivisionsXSegment_4(),
	Path_t3614338981::get_offset_of_subdivisions_5(),
	Path_t3614338981::get_offset_of_wps_6(),
	Path_t3614338981::get_offset_of_controlPoints_7(),
	Path_t3614338981::get_offset_of_length_8(),
	Path_t3614338981::get_offset_of_isFinalized_9(),
	Path_t3614338981::get_offset_of_timesTable_10(),
	Path_t3614338981::get_offset_of_lengthsTable_11(),
	Path_t3614338981::get_offset_of_linearWPIndex_12(),
	Path_t3614338981::get_offset_of__incrementalClone_13(),
	Path_t3614338981::get_offset_of__incrementalIndex_14(),
	Path_t3614338981::get_offset_of__decoder_15(),
	Path_t3614338981::get_offset_of__changed_16(),
	Path_t3614338981::get_offset_of_nonLinearDrawWps_17(),
	Path_t3614338981::get_offset_of_targetPosition_18(),
	Path_t3614338981::get_offset_of_lookAtPosition_19(),
	Path_t3614338981::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (PureQuaternionPlugin_t587122414), -1, sizeof(PureQuaternionPlugin_t587122414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2013[1] = 
{
	PureQuaternionPlugin_t587122414_StaticFields::get_offset_of__plug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (ABSSequentiable_t3376041011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[4] = 
{
	ABSSequentiable_t3376041011::get_offset_of_tweenType_0(),
	ABSSequentiable_t3376041011::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t3376041011::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t3376041011::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (Debugger_t1756157868), -1, sizeof(Debugger_t1756157868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[1] = 
{
	Debugger_t1756157868_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (DOTweenComponent_t828035757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[4] = 
{
	DOTweenComponent_t828035757::get_offset_of_inspectorUpdater_2(),
	DOTweenComponent_t828035757::get_offset_of__unscaledTime_3(),
	DOTweenComponent_t828035757::get_offset_of__unscaledDeltaTime_4(),
	DOTweenComponent_t828035757::get_offset_of__duplicateToDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CWaitForCompletionU3Ed__14_t1419758899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[3] = 
{
	U3CWaitForCompletionU3Ed__14_t1419758899::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__14_t1419758899::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__14_t1419758899::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U3CWaitForRewindU3Ed__15_t2339862385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[3] = 
{
	U3CWaitForRewindU3Ed__15_t2339862385::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__15_t2339862385::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__15_t2339862385::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U3CWaitForKillU3Ed__16_t383710286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[3] = 
{
	U3CWaitForKillU3Ed__16_t383710286::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__16_t383710286::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__16_t383710286::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (U3CWaitForElapsedLoopsU3Ed__17_t1514468280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__17_t1514468280::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U3CWaitForPositionU3Ed__18_t1811625055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[4] = 
{
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__18_t1811625055::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U3CWaitForStartU3Ed__19_t3428776308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[3] = 
{
	U3CWaitForStartU3Ed__19_t3428776308::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__19_t3428776308::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__19_t3428776308::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (DOTweenSettings_t4078947542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[21] = 
{
	0,
	DOTweenSettings_t4078947542::get_offset_of_useSafeMode_3(),
	DOTweenSettings_t4078947542::get_offset_of_timeScale_4(),
	DOTweenSettings_t4078947542::get_offset_of_useSmoothDeltaTime_5(),
	DOTweenSettings_t4078947542::get_offset_of_maxSmoothUnscaledTime_6(),
	DOTweenSettings_t4078947542::get_offset_of_rewindCallbackMode_7(),
	DOTweenSettings_t4078947542::get_offset_of_showUnityEditorReport_8(),
	DOTweenSettings_t4078947542::get_offset_of_logBehaviour_9(),
	DOTweenSettings_t4078947542::get_offset_of_drawGizmos_10(),
	DOTweenSettings_t4078947542::get_offset_of_defaultRecyclable_11(),
	DOTweenSettings_t4078947542::get_offset_of_defaultAutoPlay_12(),
	DOTweenSettings_t4078947542::get_offset_of_defaultUpdateType_13(),
	DOTweenSettings_t4078947542::get_offset_of_defaultTimeScaleIndependent_14(),
	DOTweenSettings_t4078947542::get_offset_of_defaultEaseType_15(),
	DOTweenSettings_t4078947542::get_offset_of_defaultEaseOvershootOrAmplitude_16(),
	DOTweenSettings_t4078947542::get_offset_of_defaultEasePeriod_17(),
	DOTweenSettings_t4078947542::get_offset_of_defaultAutoKill_18(),
	DOTweenSettings_t4078947542::get_offset_of_defaultLoopType_19(),
	DOTweenSettings_t4078947542::get_offset_of_storeSettingsLocation_20(),
	DOTweenSettings_t4078947542::get_offset_of_showPlayingTweens_21(),
	DOTweenSettings_t4078947542::get_offset_of_showPausedTweens_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (SettingsLocation_t2227561762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[4] = 
{
	SettingsLocation_t2227561762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (Extensions_t3835977652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (SequenceCallback_t1025787276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (TweenManager_t374091826), -1, sizeof(TweenManager_t374091826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2029[33] = 
{
	0,
	0,
	0,
	TweenManager_t374091826_StaticFields::get_offset_of_maxActive_3(),
	TweenManager_t374091826_StaticFields::get_offset_of_maxTweeners_4(),
	TweenManager_t374091826_StaticFields::get_offset_of_maxSequences_5(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveTweens_6(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveDefaultTweens_7(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveLateTweens_8(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveFixedTweens_9(),
	TweenManager_t374091826_StaticFields::get_offset_of_hasActiveManualTweens_10(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveTweens_11(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveDefaultTweens_12(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveLateTweens_13(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveFixedTweens_14(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveManualTweens_15(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveTweeners_16(),
	TweenManager_t374091826_StaticFields::get_offset_of_totActiveSequences_17(),
	TweenManager_t374091826_StaticFields::get_offset_of_totPooledTweeners_18(),
	TweenManager_t374091826_StaticFields::get_offset_of_totPooledSequences_19(),
	TweenManager_t374091826_StaticFields::get_offset_of_totTweeners_20(),
	TweenManager_t374091826_StaticFields::get_offset_of_totSequences_21(),
	TweenManager_t374091826_StaticFields::get_offset_of_isUpdateLoop_22(),
	TweenManager_t374091826_StaticFields::get_offset_of__activeTweens_23(),
	TweenManager_t374091826_StaticFields::get_offset_of__pooledTweeners_24(),
	TweenManager_t374091826_StaticFields::get_offset_of__PooledSequences_25(),
	TweenManager_t374091826_StaticFields::get_offset_of__KillList_26(),
	TweenManager_t374091826_StaticFields::get_offset_of__maxActiveLookupId_27(),
	TweenManager_t374091826_StaticFields::get_offset_of__requiresActiveReorganization_28(),
	TweenManager_t374091826_StaticFields::get_offset_of__reorganizeFromId_29(),
	TweenManager_t374091826_StaticFields::get_offset_of__minPooledTweenerId_30(),
	TweenManager_t374091826_StaticFields::get_offset_of__maxPooledTweenerId_31(),
	TweenManager_t374091826_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (CapacityIncreaseMode_t828064684)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2030[4] = 
{
	CapacityIncreaseMode_t828064684::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (Utils_t849021263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (FilterType_t3228157936)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[6] = 
{
	FilterType_t3228157936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (OperationType_t2152057980)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[14] = 
{
	OperationType_t2152057980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (SpecialStartupMode_t1644068939)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2035[6] = 
{
	SpecialStartupMode_t1644068939::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (UpdateNotice_t1001625488)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2036[3] = 
{
	UpdateNotice_t1001625488::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (UpdateMode_t1164472539)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2037[5] = 
{
	UpdateMode_t1164472539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (RewindCallbackMode_t4293633524)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2038[4] = 
{
	RewindCallbackMode_t4293633524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (Bounce_t852251727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (EaseManager_t1997294889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (U3CU3Ec_t1320040651), -1, sizeof(U3CU3Ec_t1320040651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2041[37] = 
{
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_1_2(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_2_3(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_3_4(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_4_5(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_5_6(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_6_7(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_7_8(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_8_9(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_9_10(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_10_11(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_11_12(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_12_13(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_13_14(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_14_15(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_15_16(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_16_17(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_17_18(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_18_19(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_19_20(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_20_21(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_21_22(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_22_23(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_23_24(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_24_25(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_25_26(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_26_27(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_27_28(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_28_29(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_29_30(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_30_31(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_31_32(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_32_33(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_33_34(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_34_35(),
	U3CU3Ec_t1320040651_StaticFields::get_offset_of_U3CU3E9__4_35_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (EaseCurve_t2771593608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	EaseCurve_t2771593608::get_offset_of__animCurve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (Flash_t1955515410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2044[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(),
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (__StaticArrayInitTypeSizeU3D20_t1548391511)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t1548391511 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (__StaticArrayInitTypeSizeU3D50_t1547932759)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D50_t1547932759 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (__StaticArrayInitTypeSizeU3D120_t3297148301)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t3297148301 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (EventHandle_t600343995)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2049[3] = 
{
	EventHandle_t600343995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (EventSystem_t1003666588), -1, sizeof(EventSystem_t1003666588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2068[12] = 
{
	EventSystem_t1003666588::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t1003666588::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t1003666588_StaticFields::get_offset_of_m_EventSystems_4(),
	EventSystem_t1003666588::get_offset_of_m_FirstSelected_5(),
	EventSystem_t1003666588::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t1003666588::get_offset_of_m_DragThreshold_7(),
	EventSystem_t1003666588::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t1003666588::get_offset_of_m_HasFocus_9(),
	EventSystem_t1003666588::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t1003666588::get_offset_of_m_DummyData_11(),
	EventSystem_t1003666588_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (EventTrigger_t1076084509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[2] = 
{
	EventTrigger_t1076084509::get_offset_of_m_Delegates_2(),
	EventTrigger_t1076084509::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (TriggerEvent_t3867320123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Entry_t3344766165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[2] = 
{
	Entry_t3344766165::get_offset_of_eventID_0(),
	Entry_t3344766165::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (EventTriggerType_t55832929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2072[18] = 
{
	EventTriggerType_t55832929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (ExecuteEvents_t3484638744), -1, sizeof(ExecuteEvents_t3484638744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2073[36] = 
{
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (MoveDirection_t1216237838)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2075[6] = 
{
	MoveDirection_t1216237838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (RaycasterManager_t2536340562), -1, sizeof(RaycasterManager_t2536340562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2076[1] = 
{
	RaycasterManager_t2536340562_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (RaycastResult_t3360306849)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[10] = 
{
	RaycastResult_t3360306849::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (UIBehaviour_t3495933518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (AxisEventData_t2331243652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[2] = 
{
	AxisEventData_t2331243652::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t2331243652::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (AbstractEventData_t4171500731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[1] = 
{
	AbstractEventData_t4171500731::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (BaseEventData_t3903027533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[1] = 
{
	BaseEventData_t3903027533::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (PointerEventData_t3807901092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[21] = 
{
	PointerEventData_t3807901092::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t3807901092::get_offset_of_m_PointerPress_3(),
	PointerEventData_t3807901092::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t3807901092::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t3807901092::get_offset_of_hovered_9(),
	PointerEventData_t3807901092::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t3807901092::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t3807901092::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t3807901092::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t3807901092::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t3807901092::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t3807901092::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t3807901092::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t3807901092::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t3807901092::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t3807901092::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t3807901092::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (InputButton_t3704011348)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[4] = 
{
	InputButton_t3704011348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (FramePressState_t3039385657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2084[5] = 
{
	FramePressState_t3039385657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (BaseInput_t3630163547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (BaseInputModule_t2019268878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[6] = 
{
	BaseInputModule_t2019268878::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t2019268878::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t2019268878::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t2019268878::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t2019268878::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t2019268878::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (PointerInputModule_t3453173740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3453173740::get_offset_of_m_PointerData_12(),
	PointerInputModule_t3453173740::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (ButtonState_t857139936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[2] = 
{
	ButtonState_t857139936::get_offset_of_m_Button_0(),
	ButtonState_t857139936::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (MouseState_t384203932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[1] = 
{
	MouseState_t384203932::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (MouseButtonEventData_t3190347560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[2] = 
{
	MouseButtonEventData_t3190347560::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3190347560::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (StandaloneInputModule_t2760469101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[14] = 
{
	StandaloneInputModule_t2760469101::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t2760469101::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputPointerEvent_20(),
	StandaloneInputModule_t2760469101::get_offset_of_m_HorizontalAxis_21(),
	StandaloneInputModule_t2760469101::get_offset_of_m_VerticalAxis_22(),
	StandaloneInputModule_t2760469101::get_offset_of_m_SubmitButton_23(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CancelButton_24(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputActionsPerSecond_25(),
	StandaloneInputModule_t2760469101::get_offset_of_m_RepeatDelay_26(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ForceModuleActive_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (InputMode_t3382566315)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[3] = 
{
	InputMode_t3382566315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (TouchInputModule_t4248229598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[4] = 
{
	TouchInputModule_t4248229598::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t4248229598::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t4248229598::get_offset_of_m_InputPointerEvent_16(),
	TouchInputModule_t4248229598::get_offset_of_m_ForceModuleActive_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (BaseRaycaster_t4150874583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (Physics2DRaycaster_t3382992964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[1] = 
{
	Physics2DRaycaster_t3382992964::get_offset_of_m_Hits_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (PhysicsRaycaster_t437419520), -1, sizeof(PhysicsRaycaster_t437419520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2096[7] = 
{
	0,
	PhysicsRaycaster_t437419520::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t437419520::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t437419520::get_offset_of_m_MaxRayIntersections_5(),
	PhysicsRaycaster_t437419520::get_offset_of_m_LastMaxRayIntersections_6(),
	PhysicsRaycaster_t437419520::get_offset_of_m_Hits_7(),
	PhysicsRaycaster_t437419520_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (ColorTween_t809614380)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[6] = 
{
	ColorTween_t809614380::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (ColorTweenMode_t1000778859)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[4] = 
{
	ColorTweenMode_t1000778859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
