﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t5769829;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t3801793838;
// SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t3084478566;
// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t1269713426;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct IDictionary_2_t147948184;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>
struct IDictionary_2_t1890075675;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.Camera
struct Camera_t4157153871;
// DG.Tweening.TweenCallback`1<System.Single>
struct TweenCallback_1_t1456286679;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t2447375106;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t3163629820;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;
// UnityEngine.Analytics.UnityAnalyticsHandler
struct UnityAnalyticsHandler_t3011359618;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t828035757;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t904863771;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3009965658;
// System.Type
struct Type_t;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t3814993295;
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t553148457;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;

struct ContactPoint_t3758755253 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef U3CMODULEU3E_T692745542_H
#define U3CMODULEU3E_T692745542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745542 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745542_H
#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T264918772_H
#define U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T264918772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
struct  U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t264918772  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t264918772, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T264918772_H
#ifndef U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T264984308_H
#define U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T264984308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey1
struct  U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t264984308  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey1::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t264984308, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T264984308_H
#ifndef U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_T962176190_H
#define U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_T962176190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey3
struct  U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t962176190  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey3::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t962176190, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_T962176190_H
#ifndef U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T1111257005_H
#define U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T1111257005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey0
struct  U3CGetConstructorByReflectionU3Ec__AnonStorey0_t1111257005  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey0::constructorInfo
	ConstructorInfo_t5769829 * ___constructorInfo_0;

public:
	inline static int32_t get_offset_of_constructorInfo_0() { return static_cast<int32_t>(offsetof(U3CGetConstructorByReflectionU3Ec__AnonStorey0_t1111257005, ___constructorInfo_0)); }
	inline ConstructorInfo_t5769829 * get_constructorInfo_0() const { return ___constructorInfo_0; }
	inline ConstructorInfo_t5769829 ** get_address_of_constructorInfo_0() { return &___constructorInfo_0; }
	inline void set_constructorInfo_0(ConstructorInfo_t5769829 * value)
	{
		___constructorInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___constructorInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T1111257005_H
#ifndef JSONOBJECT_T3371841791_H
#define JSONOBJECT_T3371841791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.JsonObject
struct  JsonObject_t3371841791  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> SimpleJson.JsonObject::_members
	Dictionary_2_t2865362463 * ____members_0;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(JsonObject_t3371841791, ____members_0)); }
	inline Dictionary_2_t2865362463 * get__members_0() const { return ____members_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(Dictionary_2_t2865362463 * value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T3371841791_H
#ifndef SIMPLEJSON_T666527214_H
#define SIMPLEJSON_T666527214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.SimpleJson
struct  SimpleJson_t666527214  : public RuntimeObject
{
public:

public:
};

struct SimpleJson_t666527214_StaticFields
{
public:
	// SimpleJson.IJsonSerializerStrategy SimpleJson.SimpleJson::_currentJsonSerializerStrategy
	RuntimeObject* ____currentJsonSerializerStrategy_0;
	// SimpleJson.PocoJsonSerializerStrategy SimpleJson.SimpleJson::_pocoJsonSerializerStrategy
	PocoJsonSerializerStrategy_t3084478566 * ____pocoJsonSerializerStrategy_1;

public:
	inline static int32_t get_offset_of__currentJsonSerializerStrategy_0() { return static_cast<int32_t>(offsetof(SimpleJson_t666527214_StaticFields, ____currentJsonSerializerStrategy_0)); }
	inline RuntimeObject* get__currentJsonSerializerStrategy_0() const { return ____currentJsonSerializerStrategy_0; }
	inline RuntimeObject** get_address_of__currentJsonSerializerStrategy_0() { return &____currentJsonSerializerStrategy_0; }
	inline void set__currentJsonSerializerStrategy_0(RuntimeObject* value)
	{
		____currentJsonSerializerStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentJsonSerializerStrategy_0), value);
	}

	inline static int32_t get_offset_of__pocoJsonSerializerStrategy_1() { return static_cast<int32_t>(offsetof(SimpleJson_t666527214_StaticFields, ____pocoJsonSerializerStrategy_1)); }
	inline PocoJsonSerializerStrategy_t3084478566 * get__pocoJsonSerializerStrategy_1() const { return ____pocoJsonSerializerStrategy_1; }
	inline PocoJsonSerializerStrategy_t3084478566 ** get_address_of__pocoJsonSerializerStrategy_1() { return &____pocoJsonSerializerStrategy_1; }
	inline void set__pocoJsonSerializerStrategy_1(PocoJsonSerializerStrategy_t3084478566 * value)
	{
		____pocoJsonSerializerStrategy_1 = value;
		Il2CppCodeGenWriteBarrier((&____pocoJsonSerializerStrategy_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEJSON_T666527214_H
#ifndef POCOJSONSERIALIZERSTRATEGY_T3084478566_H
#define POCOJSONSERIALIZERSTRATEGY_T3084478566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.PocoJsonSerializerStrategy
struct  PocoJsonSerializerStrategy_t3084478566  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate> SimpleJson.PocoJsonSerializerStrategy::ConstructorCache
	RuntimeObject* ___ConstructorCache_0;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>> SimpleJson.PocoJsonSerializerStrategy::GetCache
	RuntimeObject* ___GetCache_1;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>> SimpleJson.PocoJsonSerializerStrategy::SetCache
	RuntimeObject* ___SetCache_2;

public:
	inline static int32_t get_offset_of_ConstructorCache_0() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3084478566, ___ConstructorCache_0)); }
	inline RuntimeObject* get_ConstructorCache_0() const { return ___ConstructorCache_0; }
	inline RuntimeObject** get_address_of_ConstructorCache_0() { return &___ConstructorCache_0; }
	inline void set_ConstructorCache_0(RuntimeObject* value)
	{
		___ConstructorCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorCache_0), value);
	}

	inline static int32_t get_offset_of_GetCache_1() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3084478566, ___GetCache_1)); }
	inline RuntimeObject* get_GetCache_1() const { return ___GetCache_1; }
	inline RuntimeObject** get_address_of_GetCache_1() { return &___GetCache_1; }
	inline void set_GetCache_1(RuntimeObject* value)
	{
		___GetCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___GetCache_1), value);
	}

	inline static int32_t get_offset_of_SetCache_2() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3084478566, ___SetCache_2)); }
	inline RuntimeObject* get_SetCache_2() const { return ___SetCache_2; }
	inline RuntimeObject** get_address_of_SetCache_2() { return &___SetCache_2; }
	inline void set_SetCache_2(RuntimeObject* value)
	{
		___SetCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___SetCache_2), value);
	}
};

struct PocoJsonSerializerStrategy_t3084478566_StaticFields
{
public:
	// System.Type[] SimpleJson.PocoJsonSerializerStrategy::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Type[] SimpleJson.PocoJsonSerializerStrategy::ArrayConstructorParameterTypes
	TypeU5BU5D_t3940880105* ___ArrayConstructorParameterTypes_4;
	// System.String[] SimpleJson.PocoJsonSerializerStrategy::Iso8601Format
	StringU5BU5D_t1281789340* ___Iso8601Format_5;

public:
	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3084478566_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_ArrayConstructorParameterTypes_4() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3084478566_StaticFields, ___ArrayConstructorParameterTypes_4)); }
	inline TypeU5BU5D_t3940880105* get_ArrayConstructorParameterTypes_4() const { return ___ArrayConstructorParameterTypes_4; }
	inline TypeU5BU5D_t3940880105** get_address_of_ArrayConstructorParameterTypes_4() { return &___ArrayConstructorParameterTypes_4; }
	inline void set_ArrayConstructorParameterTypes_4(TypeU5BU5D_t3940880105* value)
	{
		___ArrayConstructorParameterTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayConstructorParameterTypes_4), value);
	}

	inline static int32_t get_offset_of_Iso8601Format_5() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3084478566_StaticFields, ___Iso8601Format_5)); }
	inline StringU5BU5D_t1281789340* get_Iso8601Format_5() const { return ___Iso8601Format_5; }
	inline StringU5BU5D_t1281789340** get_address_of_Iso8601Format_5() { return &___Iso8601Format_5; }
	inline void set_Iso8601Format_5(StringU5BU5D_t1281789340* value)
	{
		___Iso8601Format_5 = value;
		Il2CppCodeGenWriteBarrier((&___Iso8601Format_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POCOJSONSERIALIZERSTRATEGY_T3084478566_H
#ifndef REFLECTIONUTILS_T3359723746_H
#define REFLECTIONUTILS_T3359723746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t3359723746  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t3359723746_StaticFields
{
public:
	// System.Object[] SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t2843939325* ___EmptyObjects_0;

public:
	inline static int32_t get_offset_of_EmptyObjects_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3359723746_StaticFields, ___EmptyObjects_0)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyObjects_0() const { return ___EmptyObjects_0; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyObjects_0() { return &___EmptyObjects_0; }
	inline void set_EmptyObjects_0(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T3359723746_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T2368731853_H
#define U3CU3EC__DISPLAYCLASS10_0_T2368731853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t2368731853  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t2368731853, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T2368731853_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T481505380_H
#define U3CU3EC__DISPLAYCLASS9_0_T481505380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t481505380  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t481505380, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T481505380_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T481505379_H
#define U3CU3EC__DISPLAYCLASS8_0_T481505379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t481505379  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t481505379, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T481505379_H
#ifndef U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T962110654_H
#define U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T962110654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
struct  U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t962110654  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t962110654, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T962110654_H
#ifndef DOVIRTUAL_T3355183605_H
#define DOVIRTUAL_T3355183605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOVirtual
struct  DOVirtual_t3355183605  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOVIRTUAL_T3355183605_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T3914388664_H
#define U3CU3EC__DISPLAYCLASS0_0_T3914388664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOVirtual/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t3914388664  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.DOVirtual/<>c__DisplayClass0_0::val
	float ___val_0;
	// DG.Tweening.TweenCallback`1<System.Single> DG.Tweening.DOVirtual/<>c__DisplayClass0_0::onVirtualUpdate
	TweenCallback_1_t1456286679 * ___onVirtualUpdate_1;

public:
	inline static int32_t get_offset_of_val_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3914388664, ___val_0)); }
	inline float get_val_0() const { return ___val_0; }
	inline float* get_address_of_val_0() { return &___val_0; }
	inline void set_val_0(float value)
	{
		___val_0 = value;
	}

	inline static int32_t get_offset_of_onVirtualUpdate_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3914388664, ___onVirtualUpdate_1)); }
	inline TweenCallback_1_t1456286679 * get_onVirtualUpdate_1() const { return ___onVirtualUpdate_1; }
	inline TweenCallback_1_t1456286679 ** get_address_of_onVirtualUpdate_1() { return &___onVirtualUpdate_1; }
	inline void set_onVirtualUpdate_1(TweenCallback_1_t1456286679 * value)
	{
		___onVirtualUpdate_1 = value;
		Il2CppCodeGenWriteBarrier((&___onVirtualUpdate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T3914388664_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T481505372_H
#define U3CU3EC__DISPLAYCLASS1_0_T481505372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t481505372  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0::target
	AudioSource_t3935305588 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t481505372, ___target_0)); }
	inline AudioSource_t3935305588 * get_target_0() const { return ___target_0; }
	inline AudioSource_t3935305588 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t3935305588 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T481505372_H
#ifndef EASEFACTORY_T2344806846_H
#define EASEFACTORY_T2344806846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFactory
struct  EaseFactory_t2344806846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFACTORY_T2344806846_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T753111370_H
#define U3CU3EC__DISPLAYCLASS2_0_T753111370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFactory/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t753111370  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.EaseFactory/<>c__DisplayClass2_0::motionDelay
	float ___motionDelay_0;
	// DG.Tweening.EaseFunction DG.Tweening.EaseFactory/<>c__DisplayClass2_0::customEase
	EaseFunction_t3531141372 * ___customEase_1;

public:
	inline static int32_t get_offset_of_motionDelay_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t753111370, ___motionDelay_0)); }
	inline float get_motionDelay_0() const { return ___motionDelay_0; }
	inline float* get_address_of_motionDelay_0() { return &___motionDelay_0; }
	inline void set_motionDelay_0(float value)
	{
		___motionDelay_0 = value;
	}

	inline static int32_t get_offset_of_customEase_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t753111370, ___customEase_1)); }
	inline EaseFunction_t3531141372 * get_customEase_1() const { return ___customEase_1; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_1() { return &___customEase_1; }
	inline void set_customEase_1(EaseFunction_t3531141372 * value)
	{
		___customEase_1 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T753111370_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T481505371_H
#define U3CU3EC__DISPLAYCLASS0_0_T481505371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t481505371  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0::target
	AudioSource_t3935305588 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t481505371, ___target_0)); }
	inline AudioSource_t3935305588 * get_target_0() const { return ___target_0; }
	inline AudioSource_t3935305588 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t3935305588 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T481505371_H
#ifndef SHORTCUTEXTENSIONS_T1665800578_H
#define SHORTCUTEXTENSIONS_T1665800578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions
struct  ShortcutExtensions_t1665800578  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS_T1665800578_H
#ifndef U3CU3EC__DISPLAYCLASS55_0_T2778626132_H
#define U3CU3EC__DISPLAYCLASS55_0_T2778626132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween/<>c__DisplayClass55_0
struct  U3CU3Ec__DisplayClass55_0_t2778626132  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.DOTween/<>c__DisplayClass55_0::v
	float ___v_0;
	// DG.Tweening.Core.DOSetter`1<System.Single> DG.Tweening.DOTween/<>c__DisplayClass55_0::setter
	DOSetter_1_t2447375106 * ___setter_1;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass55_0_t2778626132, ___v_0)); }
	inline float get_v_0() const { return ___v_0; }
	inline float* get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(float value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_setter_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass55_0_t2778626132, ___setter_1)); }
	inline DOSetter_1_t2447375106 * get_setter_1() const { return ___setter_1; }
	inline DOSetter_1_t2447375106 ** get_address_of_setter_1() { return &___setter_1; }
	inline void set_setter_1(DOSetter_1_t2447375106 * value)
	{
		___setter_1 = value;
		Il2CppCodeGenWriteBarrier((&___setter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS55_0_T2778626132_H
#ifndef ANALYTICSSESSIONINFO_T2322308579_H
#define ANALYTICSSESSIONINFO_T2322308579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_t2322308579  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_t2322308579_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t3163629820 * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_t2322308579_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t3163629820 * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t3163629820 ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t3163629820 * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_T2322308579_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T481505370_H
#define U3CU3EC__DISPLAYCLASS7_0_T481505370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t481505370  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t481505370, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T481505370_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T481505369_H
#define U3CU3EC__DISPLAYCLASS6_0_T481505369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t481505369  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t481505369, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T481505369_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T481505368_H
#define U3CU3EC__DISPLAYCLASS5_0_T481505368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t481505368  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t481505368, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T481505368_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T481505367_H
#define U3CU3EC__DISPLAYCLASS4_0_T481505367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t481505367  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t481505367, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T481505367_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T481505374_H
#define U3CU3EC__DISPLAYCLASS3_0_T481505374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t481505374  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t481505374, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T481505374_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T481505373_H
#define U3CU3EC__DISPLAYCLASS2_0_T481505373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t481505373  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t481505373, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T481505373_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T2368797389_H
#define U3CU3EC__DISPLAYCLASS11_0_T2368797389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t2368797389  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2368797389, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T2368797389_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T2369059533_H
#define U3CU3EC__DISPLAYCLASS15_0_T2369059533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t2369059533  : public RuntimeObject
{
public:
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::target
	Light_t3756812086 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t2369059533, ___target_0)); }
	inline Light_t3756812086 * get_target_0() const { return ___target_0; }
	inline Light_t3756812086 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Light_t3756812086 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T2369059533_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T2368862925_H
#define U3CU3EC__DISPLAYCLASS16_0_T2368862925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t2368862925  : public RuntimeObject
{
public:
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0::target
	Light_t3756812086 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t2368862925, ___target_0)); }
	inline Light_t3756812086 * get_target_0() const { return ___target_0; }
	inline Light_t3756812086 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Light_t3756812086 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T2368862925_H
#ifndef RECTTRANSFORMUTILITY_T1743242446_H
#define RECTTRANSFORMUTILITY_T1743242446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t1743242446  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t1743242446_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t1718750761* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t1743242446_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_t1718750761* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_t1718750761* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T1743242446_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T2368207565_H
#define U3CU3EC__DISPLAYCLASS18_0_T2368207565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t2368207565  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::target
	Material_t340375123 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t2368207565, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T2368207565_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef UISYSTEMPROFILERAPI_T2230074258_H
#define UISYSTEMPROFILERAPI_T2230074258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_t2230074258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_T2230074258_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T2368993997_H
#define U3CU3EC__DISPLAYCLASS14_0_T2368993997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t2368993997  : public RuntimeObject
{
public:
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0::target
	Light_t3756812086 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t2368993997, ___target_0)); }
	inline Light_t3756812086 * get_target_0() const { return ___target_0; }
	inline Light_t3756812086 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Light_t3756812086 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T2368993997_H
#ifndef TWEENEXTENSIONS_T3641337881_H
#define TWEENEXTENSIONS_T3641337881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenExtensions
struct  TweenExtensions_t3641337881  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENEXTENSIONS_T3641337881_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T2368600781_H
#define U3CU3EC__DISPLAYCLASS12_0_T2368600781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t2368600781  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t2368600781, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T2368600781_H
#ifndef UNITYADSSETTINGS_T2370518495_H
#define UNITYADSSETTINGS_T2370518495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnityAdsSettings
struct  UnityAdsSettings_t2370518495  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSSETTINGS_T2370518495_H
#ifndef REMOTESETTINGS_T1718627291_H
#define REMOTESETTINGS_T1718627291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t1718627291  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t1718627291_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t1027848393 * ___Updated_0;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t1027848393 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t1027848393 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t1027848393 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T1718627291_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T2368666317_H
#define U3CU3EC__DISPLAYCLASS13_0_T2368666317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t2368666317  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2368666317, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T2368666317_H
#ifndef ANALYTICS_T661012366_H
#define ANALYTICS_T661012366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.Analytics
struct  Analytics_t661012366  : public RuntimeObject
{
public:

public:
};

struct Analytics_t661012366_StaticFields
{
public:
	// UnityEngine.Analytics.UnityAnalyticsHandler UnityEngine.Analytics.Analytics::s_UnityAnalyticsHandler
	UnityAnalyticsHandler_t3011359618 * ___s_UnityAnalyticsHandler_0;

public:
	inline static int32_t get_offset_of_s_UnityAnalyticsHandler_0() { return static_cast<int32_t>(offsetof(Analytics_t661012366_StaticFields, ___s_UnityAnalyticsHandler_0)); }
	inline UnityAnalyticsHandler_t3011359618 * get_s_UnityAnalyticsHandler_0() const { return ___s_UnityAnalyticsHandler_0; }
	inline UnityAnalyticsHandler_t3011359618 ** get_address_of_s_UnityAnalyticsHandler_0() { return &___s_UnityAnalyticsHandler_0; }
	inline void set_s_UnityAnalyticsHandler_0(UnityAnalyticsHandler_t3011359618 * value)
	{
		___s_UnityAnalyticsHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityAnalyticsHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICS_T661012366_H
#ifndef JOINTLIMITS_T2681268900_H
#define JOINTLIMITS_T2681268900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointLimits
struct  JointLimits_t2681268900 
{
public:
	// System.Single UnityEngine.JointLimits::m_Min
	float ___m_Min_0;
	// System.Single UnityEngine.JointLimits::m_Max
	float ___m_Max_1;
	// System.Single UnityEngine.JointLimits::m_Bounciness
	float ___m_Bounciness_2;
	// System.Single UnityEngine.JointLimits::m_BounceMinVelocity
	float ___m_BounceMinVelocity_3;
	// System.Single UnityEngine.JointLimits::m_ContactDistance
	float ___m_ContactDistance_4;
	// System.Single UnityEngine.JointLimits::minBounce
	float ___minBounce_5;
	// System.Single UnityEngine.JointLimits::maxBounce
	float ___maxBounce_6;

public:
	inline static int32_t get_offset_of_m_Min_0() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_Min_0)); }
	inline float get_m_Min_0() const { return ___m_Min_0; }
	inline float* get_address_of_m_Min_0() { return &___m_Min_0; }
	inline void set_m_Min_0(float value)
	{
		___m_Min_0 = value;
	}

	inline static int32_t get_offset_of_m_Max_1() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_Max_1)); }
	inline float get_m_Max_1() const { return ___m_Max_1; }
	inline float* get_address_of_m_Max_1() { return &___m_Max_1; }
	inline void set_m_Max_1(float value)
	{
		___m_Max_1 = value;
	}

	inline static int32_t get_offset_of_m_Bounciness_2() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_Bounciness_2)); }
	inline float get_m_Bounciness_2() const { return ___m_Bounciness_2; }
	inline float* get_address_of_m_Bounciness_2() { return &___m_Bounciness_2; }
	inline void set_m_Bounciness_2(float value)
	{
		___m_Bounciness_2 = value;
	}

	inline static int32_t get_offset_of_m_BounceMinVelocity_3() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_BounceMinVelocity_3)); }
	inline float get_m_BounceMinVelocity_3() const { return ___m_BounceMinVelocity_3; }
	inline float* get_address_of_m_BounceMinVelocity_3() { return &___m_BounceMinVelocity_3; }
	inline void set_m_BounceMinVelocity_3(float value)
	{
		___m_BounceMinVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_ContactDistance_4() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___m_ContactDistance_4)); }
	inline float get_m_ContactDistance_4() const { return ___m_ContactDistance_4; }
	inline float* get_address_of_m_ContactDistance_4() { return &___m_ContactDistance_4; }
	inline void set_m_ContactDistance_4(float value)
	{
		___m_ContactDistance_4 = value;
	}

	inline static int32_t get_offset_of_minBounce_5() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___minBounce_5)); }
	inline float get_minBounce_5() const { return ___minBounce_5; }
	inline float* get_address_of_minBounce_5() { return &___minBounce_5; }
	inline void set_minBounce_5(float value)
	{
		___minBounce_5 = value;
	}

	inline static int32_t get_offset_of_maxBounce_6() { return static_cast<int32_t>(offsetof(JointLimits_t2681268900, ___maxBounce_6)); }
	inline float get_maxBounce_6() const { return ___maxBounce_6; }
	inline float* get_address_of_maxBounce_6() { return &___maxBounce_6; }
	inline void set_maxBounce_6(float value)
	{
		___maxBounce_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTLIMITS_T2681268900_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef JSONARRAY_T3708654325_H
#define JSONARRAY_T3708654325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.JsonArray
struct  JsonArray_t3708654325  : public List_1_t257213610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T3708654325_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef LOGBEHAVIOUR_T1548882435_H
#define LOGBEHAVIOUR_T1548882435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_t1548882435 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogBehaviour_t1548882435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_T1548882435_H
#ifndef REWINDCALLBACKMODE_T4293633524_H
#define REWINDCALLBACKMODE_T4293633524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.RewindCallbackMode
struct  RewindCallbackMode_t4293633524 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.RewindCallbackMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RewindCallbackMode_t4293633524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWINDCALLBACKMODE_T4293633524_H
#ifndef CONTACTPOINT_T3758755253_H
#define CONTACTPOINT_T3758755253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContactPoint
struct  ContactPoint_t3758755253 
{
public:
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisColliderInstanceID_2() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_ThisColliderInstanceID_2)); }
	inline int32_t get_m_ThisColliderInstanceID_2() const { return ___m_ThisColliderInstanceID_2; }
	inline int32_t* get_address_of_m_ThisColliderInstanceID_2() { return &___m_ThisColliderInstanceID_2; }
	inline void set_m_ThisColliderInstanceID_2(int32_t value)
	{
		___m_ThisColliderInstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_OtherColliderInstanceID_3)); }
	inline int32_t get_m_OtherColliderInstanceID_3() const { return ___m_OtherColliderInstanceID_3; }
	inline int32_t* get_address_of_m_OtherColliderInstanceID_3() { return &___m_OtherColliderInstanceID_3; }
	inline void set_m_OtherColliderInstanceID_3(int32_t value)
	{
		___m_OtherColliderInstanceID_3 = value;
	}

	inline static int32_t get_offset_of_m_Separation_4() { return static_cast<int32_t>(offsetof(ContactPoint_t3758755253, ___m_Separation_4)); }
	inline float get_m_Separation_4() const { return ___m_Separation_4; }
	inline float* get_address_of_m_Separation_4() { return &___m_Separation_4; }
	inline void set_m_Separation_4(float value)
	{
		___m_Separation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTPOINT_T3758755253_H
#ifndef AUTOPLAY_T1346164433_H
#define AUTOPLAY_T1346164433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t1346164433 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoPlay_t1346164433, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T1346164433_H
#ifndef QUERYTRIGGERINTERACTION_T962663221_H
#define QUERYTRIGGERINTERACTION_T962663221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.QueryTriggerInteraction
struct  QueryTriggerInteraction_t962663221 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t962663221, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYTRIGGERINTERACTION_T962663221_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef RIGIDBODYCONSTRAINTS_T3348042902_H
#define RIGIDBODYCONSTRAINTS_T3348042902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_t3348042902 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_t3348042902, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS_T3348042902_H
#ifndef ANALYTICSRESULT_T2273004240_H
#define ANALYTICSRESULT_T2273004240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsResult
struct  AnalyticsResult_t2273004240 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsResult_t2273004240, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSRESULT_T2273004240_H
#ifndef UNITYANALYTICSHANDLER_T3011359618_H
#define UNITYANALYTICSHANDLER_T3011359618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.UnityAnalyticsHandler
struct  UnityAnalyticsHandler_t3011359618  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Analytics.UnityAnalyticsHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityAnalyticsHandler_t3011359618, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.UnityAnalyticsHandler
struct UnityAnalyticsHandler_t3011359618_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Analytics.UnityAnalyticsHandler
struct UnityAnalyticsHandler_t3011359618_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UNITYANALYTICSHANDLER_T3011359618_H
#ifndef CUSTOMEVENTDATA_T317522481_H
#define CUSTOMEVENTDATA_T317522481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.CustomEventData
struct  CustomEventData_t317522481  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Analytics.CustomEventData::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CustomEventData_t317522481, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Analytics.CustomEventData
struct CustomEventData_t317522481_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Analytics.CustomEventData
struct CustomEventData_t317522481_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // CUSTOMEVENTDATA_T317522481_H
#ifndef CONTROLLERCOLLIDERHIT_T240592346_H
#define CONTROLLERCOLLIDERHIT_T240592346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ControllerColliderHit
struct  ControllerColliderHit_t240592346  : public RuntimeObject
{
public:
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t1138636865 * ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1773347010 * ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t3722313464  ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t3722313464  ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t3722313464  ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;

public:
	inline static int32_t get_offset_of_m_Controller_0() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Controller_0)); }
	inline CharacterController_t1138636865 * get_m_Controller_0() const { return ___m_Controller_0; }
	inline CharacterController_t1138636865 ** get_address_of_m_Controller_0() { return &___m_Controller_0; }
	inline void set_m_Controller_0(CharacterController_t1138636865 * value)
	{
		___m_Controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_0), value);
	}

	inline static int32_t get_offset_of_m_Collider_1() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Collider_1)); }
	inline Collider_t1773347010 * get_m_Collider_1() const { return ___m_Collider_1; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_1() { return &___m_Collider_1; }
	inline void set_m_Collider_1(Collider_t1773347010 * value)
	{
		___m_Collider_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_1), value);
	}

	inline static int32_t get_offset_of_m_Point_2() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Point_2)); }
	inline Vector3_t3722313464  get_m_Point_2() const { return ___m_Point_2; }
	inline Vector3_t3722313464 * get_address_of_m_Point_2() { return &___m_Point_2; }
	inline void set_m_Point_2(Vector3_t3722313464  value)
	{
		___m_Point_2 = value;
	}

	inline static int32_t get_offset_of_m_Normal_3() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Normal_3)); }
	inline Vector3_t3722313464  get_m_Normal_3() const { return ___m_Normal_3; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_3() { return &___m_Normal_3; }
	inline void set_m_Normal_3(Vector3_t3722313464  value)
	{
		___m_Normal_3 = value;
	}

	inline static int32_t get_offset_of_m_MoveDirection_4() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveDirection_4)); }
	inline Vector3_t3722313464  get_m_MoveDirection_4() const { return ___m_MoveDirection_4; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDirection_4() { return &___m_MoveDirection_4; }
	inline void set_m_MoveDirection_4(Vector3_t3722313464  value)
	{
		___m_MoveDirection_4 = value;
	}

	inline static int32_t get_offset_of_m_MoveLength_5() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveLength_5)); }
	inline float get_m_MoveLength_5() const { return ___m_MoveLength_5; }
	inline float* get_address_of_m_MoveLength_5() { return &___m_MoveLength_5; }
	inline void set_m_MoveLength_5(float value)
	{
		___m_MoveLength_5 = value;
	}

	inline static int32_t get_offset_of_m_Push_6() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Push_6)); }
	inline int32_t get_m_Push_6() const { return ___m_Push_6; }
	inline int32_t* get_address_of_m_Push_6() { return &___m_Push_6; }
	inline void set_m_Push_6(int32_t value)
	{
		___m_Push_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_pinvoke
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_com
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
#endif // CONTROLLERCOLLIDERHIT_T240592346_H
#ifndef COLLISION_T4262080450_H
#define COLLISION_T4262080450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision
struct  Collision_t4262080450  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t3722313464  ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	// UnityEngine.Rigidbody UnityEngine.Collision::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1773347010 * ___m_Collider_3;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_Contacts
	ContactPointU5BU5D_t872956888* ___m_Contacts_4;

public:
	inline static int32_t get_offset_of_m_Impulse_0() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Impulse_0)); }
	inline Vector3_t3722313464  get_m_Impulse_0() const { return ___m_Impulse_0; }
	inline Vector3_t3722313464 * get_address_of_m_Impulse_0() { return &___m_Impulse_0; }
	inline void set_m_Impulse_0(Vector3_t3722313464  value)
	{
		___m_Impulse_0 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_1() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_RelativeVelocity_1)); }
	inline Vector3_t3722313464  get_m_RelativeVelocity_1() const { return ___m_RelativeVelocity_1; }
	inline Vector3_t3722313464 * get_address_of_m_RelativeVelocity_1() { return &___m_RelativeVelocity_1; }
	inline void set_m_RelativeVelocity_1(Vector3_t3722313464  value)
	{
		___m_RelativeVelocity_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Rigidbody_2)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_2), value);
	}

	inline static int32_t get_offset_of_m_Collider_3() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Collider_3)); }
	inline Collider_t1773347010 * get_m_Collider_3() const { return ___m_Collider_3; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_3() { return &___m_Collider_3; }
	inline void set_m_Collider_3(Collider_t1773347010 * value)
	{
		___m_Collider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_3), value);
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Contacts_4)); }
	inline ContactPointU5BU5D_t872956888* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPointU5BU5D_t872956888** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPointU5BU5D_t872956888* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_com
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
#endif // COLLISION_T4262080450_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef ROTATEMODE_T2548570174_H
#define ROTATEMODE_T2548570174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t2548570174 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2548570174, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2548570174_H
#ifndef RENDERMODE_T4077056833_H
#define RENDERMODE_T4077056833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4077056833 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4077056833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4077056833_H
#ifndef PATHTYPE_T3777299409_H
#define PATHTYPE_T3777299409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t3777299409 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t3777299409, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T3777299409_H
#ifndef PATHMODE_T2165603100_H
#define PATHMODE_T2165603100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t2165603100 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathMode_t2165603100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T2165603100_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef COLOR2_T3097643075_H
#define COLOR2_T3097643075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Color2
struct  Color2_t3097643075 
{
public:
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_t2555686324  ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_t2555686324  ___cb_1;

public:
	inline static int32_t get_offset_of_ca_0() { return static_cast<int32_t>(offsetof(Color2_t3097643075, ___ca_0)); }
	inline Color_t2555686324  get_ca_0() const { return ___ca_0; }
	inline Color_t2555686324 * get_address_of_ca_0() { return &___ca_0; }
	inline void set_ca_0(Color_t2555686324  value)
	{
		___ca_0 = value;
	}

	inline static int32_t get_offset_of_cb_1() { return static_cast<int32_t>(offsetof(Color2_t3097643075, ___cb_1)); }
	inline Color_t2555686324  get_cb_1() const { return ___cb_1; }
	inline Color_t2555686324 * get_address_of_cb_1() { return &___cb_1; }
	inline void set_cb_1(Color_t2555686324  value)
	{
		___cb_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2_T3097643075_H
#ifndef SAMPLETYPE_T1208595618_H
#define SAMPLETYPE_T1208595618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi/SampleType
struct  SampleType_t1208595618 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi/SampleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleType_t1208595618, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T1208595618_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T2368928461_H
#define U3CU3EC__DISPLAYCLASS17_0_T2368928461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t2368928461  : public RuntimeObject
{
public:
	// DG.Tweening.Color2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::startValue
	Color2_t3097643075  ___startValue_0;
	// UnityEngine.LineRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::target
	LineRenderer_t3154350270 * ___target_1;

public:
	inline static int32_t get_offset_of_startValue_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t2368928461, ___startValue_0)); }
	inline Color2_t3097643075  get_startValue_0() const { return ___startValue_0; }
	inline Color2_t3097643075 * get_address_of_startValue_0() { return &___startValue_0; }
	inline void set_startValue_0(Color2_t3097643075  value)
	{
		___startValue_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t2368928461, ___target_1)); }
	inline LineRenderer_t3154350270 * get_target_1() const { return ___target_1; }
	inline LineRenderer_t3154350270 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(LineRenderer_t3154350270 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T2368928461_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef DOTWEEN_T2744875806_H
#define DOTWEEN_T2744875806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween
struct  DOTween_t2744875806  : public RuntimeObject
{
public:

public:
};

struct DOTween_t2744875806_StaticFields
{
public:
	// System.String DG.Tweening.DOTween::Version
	String_t* ___Version_0;
	// System.Boolean DG.Tweening.DOTween::useSafeMode
	bool ___useSafeMode_1;
	// System.Boolean DG.Tweening.DOTween::showUnityEditorReport
	bool ___showUnityEditorReport_2;
	// System.Single DG.Tweening.DOTween::timeScale
	float ___timeScale_3;
	// System.Boolean DG.Tweening.DOTween::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_4;
	// System.Single DG.Tweening.DOTween::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_5;
	// DG.Tweening.Core.Enums.RewindCallbackMode DG.Tweening.DOTween::rewindCallbackMode
	int32_t ___rewindCallbackMode_6;
	// DG.Tweening.LogBehaviour DG.Tweening.DOTween::_logBehaviour
	int32_t ____logBehaviour_7;
	// System.Boolean DG.Tweening.DOTween::drawGizmos
	bool ___drawGizmos_8;
	// DG.Tweening.UpdateType DG.Tweening.DOTween::defaultUpdateType
	int32_t ___defaultUpdateType_9;
	// System.Boolean DG.Tweening.DOTween::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_10;
	// DG.Tweening.AutoPlay DG.Tweening.DOTween::defaultAutoPlay
	int32_t ___defaultAutoPlay_11;
	// System.Boolean DG.Tweening.DOTween::defaultAutoKill
	bool ___defaultAutoKill_12;
	// DG.Tweening.LoopType DG.Tweening.DOTween::defaultLoopType
	int32_t ___defaultLoopType_13;
	// System.Boolean DG.Tweening.DOTween::defaultRecyclable
	bool ___defaultRecyclable_14;
	// DG.Tweening.Ease DG.Tweening.DOTween::defaultEaseType
	int32_t ___defaultEaseType_15;
	// System.Single DG.Tweening.DOTween::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_16;
	// System.Single DG.Tweening.DOTween::defaultEasePeriod
	float ___defaultEasePeriod_17;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.DOTween::instance
	DOTweenComponent_t828035757 * ___instance_18;
	// System.Boolean DG.Tweening.DOTween::isUnityEditor
	bool ___isUnityEditor_19;
	// System.Boolean DG.Tweening.DOTween::isDebugBuild
	bool ___isDebugBuild_20;
	// System.Int32 DG.Tweening.DOTween::maxActiveTweenersReached
	int32_t ___maxActiveTweenersReached_21;
	// System.Int32 DG.Tweening.DOTween::maxActiveSequencesReached
	int32_t ___maxActiveSequencesReached_22;
	// System.Collections.Generic.List`1<DG.Tweening.TweenCallback> DG.Tweening.DOTween::GizmosDelegates
	List_1_t904863771 * ___GizmosDelegates_23;
	// System.Boolean DG.Tweening.DOTween::initialized
	bool ___initialized_24;
	// System.Boolean DG.Tweening.DOTween::isQuitting
	bool ___isQuitting_25;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_useSafeMode_1() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___useSafeMode_1)); }
	inline bool get_useSafeMode_1() const { return ___useSafeMode_1; }
	inline bool* get_address_of_useSafeMode_1() { return &___useSafeMode_1; }
	inline void set_useSafeMode_1(bool value)
	{
		___useSafeMode_1 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_2() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___showUnityEditorReport_2)); }
	inline bool get_showUnityEditorReport_2() const { return ___showUnityEditorReport_2; }
	inline bool* get_address_of_showUnityEditorReport_2() { return &___showUnityEditorReport_2; }
	inline void set_showUnityEditorReport_2(bool value)
	{
		___showUnityEditorReport_2 = value;
	}

	inline static int32_t get_offset_of_timeScale_3() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___timeScale_3)); }
	inline float get_timeScale_3() const { return ___timeScale_3; }
	inline float* get_address_of_timeScale_3() { return &___timeScale_3; }
	inline void set_timeScale_3(float value)
	{
		___timeScale_3 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___useSmoothDeltaTime_4)); }
	inline bool get_useSmoothDeltaTime_4() const { return ___useSmoothDeltaTime_4; }
	inline bool* get_address_of_useSmoothDeltaTime_4() { return &___useSmoothDeltaTime_4; }
	inline void set_useSmoothDeltaTime_4(bool value)
	{
		___useSmoothDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_5() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxSmoothUnscaledTime_5)); }
	inline float get_maxSmoothUnscaledTime_5() const { return ___maxSmoothUnscaledTime_5; }
	inline float* get_address_of_maxSmoothUnscaledTime_5() { return &___maxSmoothUnscaledTime_5; }
	inline void set_maxSmoothUnscaledTime_5(float value)
	{
		___maxSmoothUnscaledTime_5 = value;
	}

	inline static int32_t get_offset_of_rewindCallbackMode_6() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___rewindCallbackMode_6)); }
	inline int32_t get_rewindCallbackMode_6() const { return ___rewindCallbackMode_6; }
	inline int32_t* get_address_of_rewindCallbackMode_6() { return &___rewindCallbackMode_6; }
	inline void set_rewindCallbackMode_6(int32_t value)
	{
		___rewindCallbackMode_6 = value;
	}

	inline static int32_t get_offset_of__logBehaviour_7() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ____logBehaviour_7)); }
	inline int32_t get__logBehaviour_7() const { return ____logBehaviour_7; }
	inline int32_t* get_address_of__logBehaviour_7() { return &____logBehaviour_7; }
	inline void set__logBehaviour_7(int32_t value)
	{
		____logBehaviour_7 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_8() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___drawGizmos_8)); }
	inline bool get_drawGizmos_8() const { return ___drawGizmos_8; }
	inline bool* get_address_of_drawGizmos_8() { return &___drawGizmos_8; }
	inline void set_drawGizmos_8(bool value)
	{
		___drawGizmos_8 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_9() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultUpdateType_9)); }
	inline int32_t get_defaultUpdateType_9() const { return ___defaultUpdateType_9; }
	inline int32_t* get_address_of_defaultUpdateType_9() { return &___defaultUpdateType_9; }
	inline void set_defaultUpdateType_9(int32_t value)
	{
		___defaultUpdateType_9 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_10() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultTimeScaleIndependent_10)); }
	inline bool get_defaultTimeScaleIndependent_10() const { return ___defaultTimeScaleIndependent_10; }
	inline bool* get_address_of_defaultTimeScaleIndependent_10() { return &___defaultTimeScaleIndependent_10; }
	inline void set_defaultTimeScaleIndependent_10(bool value)
	{
		___defaultTimeScaleIndependent_10 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_11() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultAutoPlay_11)); }
	inline int32_t get_defaultAutoPlay_11() const { return ___defaultAutoPlay_11; }
	inline int32_t* get_address_of_defaultAutoPlay_11() { return &___defaultAutoPlay_11; }
	inline void set_defaultAutoPlay_11(int32_t value)
	{
		___defaultAutoPlay_11 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_12() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultAutoKill_12)); }
	inline bool get_defaultAutoKill_12() const { return ___defaultAutoKill_12; }
	inline bool* get_address_of_defaultAutoKill_12() { return &___defaultAutoKill_12; }
	inline void set_defaultAutoKill_12(bool value)
	{
		___defaultAutoKill_12 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_13() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultLoopType_13)); }
	inline int32_t get_defaultLoopType_13() const { return ___defaultLoopType_13; }
	inline int32_t* get_address_of_defaultLoopType_13() { return &___defaultLoopType_13; }
	inline void set_defaultLoopType_13(int32_t value)
	{
		___defaultLoopType_13 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_14() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultRecyclable_14)); }
	inline bool get_defaultRecyclable_14() const { return ___defaultRecyclable_14; }
	inline bool* get_address_of_defaultRecyclable_14() { return &___defaultRecyclable_14; }
	inline void set_defaultRecyclable_14(bool value)
	{
		___defaultRecyclable_14 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_15() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEaseType_15)); }
	inline int32_t get_defaultEaseType_15() const { return ___defaultEaseType_15; }
	inline int32_t* get_address_of_defaultEaseType_15() { return &___defaultEaseType_15; }
	inline void set_defaultEaseType_15(int32_t value)
	{
		___defaultEaseType_15 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_16() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEaseOvershootOrAmplitude_16)); }
	inline float get_defaultEaseOvershootOrAmplitude_16() const { return ___defaultEaseOvershootOrAmplitude_16; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_16() { return &___defaultEaseOvershootOrAmplitude_16; }
	inline void set_defaultEaseOvershootOrAmplitude_16(float value)
	{
		___defaultEaseOvershootOrAmplitude_16 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_17() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEasePeriod_17)); }
	inline float get_defaultEasePeriod_17() const { return ___defaultEasePeriod_17; }
	inline float* get_address_of_defaultEasePeriod_17() { return &___defaultEasePeriod_17; }
	inline void set_defaultEasePeriod_17(float value)
	{
		___defaultEasePeriod_17 = value;
	}

	inline static int32_t get_offset_of_instance_18() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___instance_18)); }
	inline DOTweenComponent_t828035757 * get_instance_18() const { return ___instance_18; }
	inline DOTweenComponent_t828035757 ** get_address_of_instance_18() { return &___instance_18; }
	inline void set_instance_18(DOTweenComponent_t828035757 * value)
	{
		___instance_18 = value;
		Il2CppCodeGenWriteBarrier((&___instance_18), value);
	}

	inline static int32_t get_offset_of_isUnityEditor_19() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isUnityEditor_19)); }
	inline bool get_isUnityEditor_19() const { return ___isUnityEditor_19; }
	inline bool* get_address_of_isUnityEditor_19() { return &___isUnityEditor_19; }
	inline void set_isUnityEditor_19(bool value)
	{
		___isUnityEditor_19 = value;
	}

	inline static int32_t get_offset_of_isDebugBuild_20() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isDebugBuild_20)); }
	inline bool get_isDebugBuild_20() const { return ___isDebugBuild_20; }
	inline bool* get_address_of_isDebugBuild_20() { return &___isDebugBuild_20; }
	inline void set_isDebugBuild_20(bool value)
	{
		___isDebugBuild_20 = value;
	}

	inline static int32_t get_offset_of_maxActiveTweenersReached_21() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxActiveTweenersReached_21)); }
	inline int32_t get_maxActiveTweenersReached_21() const { return ___maxActiveTweenersReached_21; }
	inline int32_t* get_address_of_maxActiveTweenersReached_21() { return &___maxActiveTweenersReached_21; }
	inline void set_maxActiveTweenersReached_21(int32_t value)
	{
		___maxActiveTweenersReached_21 = value;
	}

	inline static int32_t get_offset_of_maxActiveSequencesReached_22() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxActiveSequencesReached_22)); }
	inline int32_t get_maxActiveSequencesReached_22() const { return ___maxActiveSequencesReached_22; }
	inline int32_t* get_address_of_maxActiveSequencesReached_22() { return &___maxActiveSequencesReached_22; }
	inline void set_maxActiveSequencesReached_22(int32_t value)
	{
		___maxActiveSequencesReached_22 = value;
	}

	inline static int32_t get_offset_of_GizmosDelegates_23() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___GizmosDelegates_23)); }
	inline List_1_t904863771 * get_GizmosDelegates_23() const { return ___GizmosDelegates_23; }
	inline List_1_t904863771 ** get_address_of_GizmosDelegates_23() { return &___GizmosDelegates_23; }
	inline void set_GizmosDelegates_23(List_1_t904863771 * value)
	{
		___GizmosDelegates_23 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosDelegates_23), value);
	}

	inline static int32_t get_offset_of_initialized_24() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___initialized_24)); }
	inline bool get_initialized_24() const { return ___initialized_24; }
	inline bool* get_address_of_initialized_24() { return &___initialized_24; }
	inline void set_initialized_24(bool value)
	{
		___initialized_24 = value;
	}

	inline static int32_t get_offset_of_isQuitting_25() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isQuitting_25)); }
	inline bool get_isQuitting_25() const { return ___isQuitting_25; }
	inline bool* get_address_of_isQuitting_25() { return &___isQuitting_25; }
	inline void set_isQuitting_25(bool value)
	{
		___isQuitting_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEEN_T2744875806_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef TWEEN_T2342918553_H
#define TWEEN_T2342918553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t2342918553  : public ABSSequentiable_t3376041011
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.String DG.Tweening.Tween::stringId
	String_t* ___stringId_7;
	// System.Int32 DG.Tweening.Tween::intId
	int32_t ___intId_8;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_9;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_10;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3727756325 * ___onPlay_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3727756325 * ___onPause_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3727756325 * ___onRewind_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_16;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3727756325 * ___onComplete_17;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3727756325 * ___onKill_18;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_19;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_20;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_21;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_22;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_23;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_24;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_25;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_26;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_27;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_28;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_29;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_30;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3531141372 * ___customEase_31;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_32;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_33;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_34;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_35;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_36;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_37;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_38;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t2050373119 * ___sequenceParent_39;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_40;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_41;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_42;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_43;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_44;
	// System.Single DG.Tweening.Tween::position
	float ___position_45;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_46;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_47;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_48;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_49;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_50;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_51;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_52;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_stringId_7() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___stringId_7)); }
	inline String_t* get_stringId_7() const { return ___stringId_7; }
	inline String_t** get_address_of_stringId_7() { return &___stringId_7; }
	inline void set_stringId_7(String_t* value)
	{
		___stringId_7 = value;
		Il2CppCodeGenWriteBarrier((&___stringId_7), value);
	}

	inline static int32_t get_offset_of_intId_8() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___intId_8)); }
	inline int32_t get_intId_8() const { return ___intId_8; }
	inline int32_t* get_address_of_intId_8() { return &___intId_8; }
	inline void set_intId_8(int32_t value)
	{
		___intId_8 = value;
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___target_9)); }
	inline RuntimeObject * get_target_9() const { return ___target_9; }
	inline RuntimeObject ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(RuntimeObject * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((&___target_9), value);
	}

	inline static int32_t get_offset_of_updateType_10() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___updateType_10)); }
	inline int32_t get_updateType_10() const { return ___updateType_10; }
	inline int32_t* get_address_of_updateType_10() { return &___updateType_10; }
	inline void set_updateType_10(int32_t value)
	{
		___updateType_10 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_11() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isIndependentUpdate_11)); }
	inline bool get_isIndependentUpdate_11() const { return ___isIndependentUpdate_11; }
	inline bool* get_address_of_isIndependentUpdate_11() { return &___isIndependentUpdate_11; }
	inline void set_isIndependentUpdate_11(bool value)
	{
		___isIndependentUpdate_11 = value;
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPlay_12)); }
	inline TweenCallback_t3727756325 * get_onPlay_12() const { return ___onPlay_12; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(TweenCallback_t3727756325 * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_12), value);
	}

	inline static int32_t get_offset_of_onPause_13() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPause_13)); }
	inline TweenCallback_t3727756325 * get_onPause_13() const { return ___onPause_13; }
	inline TweenCallback_t3727756325 ** get_address_of_onPause_13() { return &___onPause_13; }
	inline void set_onPause_13(TweenCallback_t3727756325 * value)
	{
		___onPause_13 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_13), value);
	}

	inline static int32_t get_offset_of_onRewind_14() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onRewind_14)); }
	inline TweenCallback_t3727756325 * get_onRewind_14() const { return ___onRewind_14; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_14() { return &___onRewind_14; }
	inline void set_onRewind_14(TweenCallback_t3727756325 * value)
	{
		___onRewind_14 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_14), value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onUpdate_15)); }
	inline TweenCallback_t3727756325 * get_onUpdate_15() const { return ___onUpdate_15; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(TweenCallback_t3727756325 * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_15), value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onStepComplete_16)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_16), value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onComplete_17)); }
	inline TweenCallback_t3727756325 * get_onComplete_17() const { return ___onComplete_17; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(TweenCallback_t3727756325 * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_17), value);
	}

	inline static int32_t get_offset_of_onKill_18() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onKill_18)); }
	inline TweenCallback_t3727756325 * get_onKill_18() const { return ___onKill_18; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_18() { return &___onKill_18; }
	inline void set_onKill_18(TweenCallback_t3727756325 * value)
	{
		___onKill_18 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_18), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_19() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onWaypointChange_19)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_19() const { return ___onWaypointChange_19; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_19() { return &___onWaypointChange_19; }
	inline void set_onWaypointChange_19(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_19 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_19), value);
	}

	inline static int32_t get_offset_of_isFrom_20() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isFrom_20)); }
	inline bool get_isFrom_20() const { return ___isFrom_20; }
	inline bool* get_address_of_isFrom_20() { return &___isFrom_20; }
	inline void set_isFrom_20(bool value)
	{
		___isFrom_20 = value;
	}

	inline static int32_t get_offset_of_isBlendable_21() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBlendable_21)); }
	inline bool get_isBlendable_21() const { return ___isBlendable_21; }
	inline bool* get_address_of_isBlendable_21() { return &___isBlendable_21; }
	inline void set_isBlendable_21(bool value)
	{
		___isBlendable_21 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_22() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRecyclable_22)); }
	inline bool get_isRecyclable_22() const { return ___isRecyclable_22; }
	inline bool* get_address_of_isRecyclable_22() { return &___isRecyclable_22; }
	inline void set_isRecyclable_22(bool value)
	{
		___isRecyclable_22 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_23() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSpeedBased_23)); }
	inline bool get_isSpeedBased_23() const { return ___isSpeedBased_23; }
	inline bool* get_address_of_isSpeedBased_23() { return &___isSpeedBased_23; }
	inline void set_isSpeedBased_23(bool value)
	{
		___isSpeedBased_23 = value;
	}

	inline static int32_t get_offset_of_autoKill_24() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___autoKill_24)); }
	inline bool get_autoKill_24() const { return ___autoKill_24; }
	inline bool* get_address_of_autoKill_24() { return &___autoKill_24; }
	inline void set_autoKill_24(bool value)
	{
		___autoKill_24 = value;
	}

	inline static int32_t get_offset_of_duration_25() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___duration_25)); }
	inline float get_duration_25() const { return ___duration_25; }
	inline float* get_address_of_duration_25() { return &___duration_25; }
	inline void set_duration_25(float value)
	{
		___duration_25 = value;
	}

	inline static int32_t get_offset_of_loops_26() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loops_26)); }
	inline int32_t get_loops_26() const { return ___loops_26; }
	inline int32_t* get_address_of_loops_26() { return &___loops_26; }
	inline void set_loops_26(int32_t value)
	{
		___loops_26 = value;
	}

	inline static int32_t get_offset_of_loopType_27() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loopType_27)); }
	inline int32_t get_loopType_27() const { return ___loopType_27; }
	inline int32_t* get_address_of_loopType_27() { return &___loopType_27; }
	inline void set_loopType_27(int32_t value)
	{
		___loopType_27 = value;
	}

	inline static int32_t get_offset_of_delay_28() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delay_28)); }
	inline float get_delay_28() const { return ___delay_28; }
	inline float* get_address_of_delay_28() { return &___delay_28; }
	inline void set_delay_28(float value)
	{
		___delay_28 = value;
	}

	inline static int32_t get_offset_of_isRelative_29() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRelative_29)); }
	inline bool get_isRelative_29() const { return ___isRelative_29; }
	inline bool* get_address_of_isRelative_29() { return &___isRelative_29; }
	inline void set_isRelative_29(bool value)
	{
		___isRelative_29 = value;
	}

	inline static int32_t get_offset_of_easeType_30() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeType_30)); }
	inline int32_t get_easeType_30() const { return ___easeType_30; }
	inline int32_t* get_address_of_easeType_30() { return &___easeType_30; }
	inline void set_easeType_30(int32_t value)
	{
		___easeType_30 = value;
	}

	inline static int32_t get_offset_of_customEase_31() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___customEase_31)); }
	inline EaseFunction_t3531141372 * get_customEase_31() const { return ___customEase_31; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_31() { return &___customEase_31; }
	inline void set_customEase_31(EaseFunction_t3531141372 * value)
	{
		___customEase_31 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_31), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_32() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeOvershootOrAmplitude_32)); }
	inline float get_easeOvershootOrAmplitude_32() const { return ___easeOvershootOrAmplitude_32; }
	inline float* get_address_of_easeOvershootOrAmplitude_32() { return &___easeOvershootOrAmplitude_32; }
	inline void set_easeOvershootOrAmplitude_32(float value)
	{
		___easeOvershootOrAmplitude_32 = value;
	}

	inline static int32_t get_offset_of_easePeriod_33() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easePeriod_33)); }
	inline float get_easePeriod_33() const { return ___easePeriod_33; }
	inline float* get_address_of_easePeriod_33() { return &___easePeriod_33; }
	inline void set_easePeriod_33(float value)
	{
		___easePeriod_33 = value;
	}

	inline static int32_t get_offset_of_typeofT1_34() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT1_34)); }
	inline Type_t * get_typeofT1_34() const { return ___typeofT1_34; }
	inline Type_t ** get_address_of_typeofT1_34() { return &___typeofT1_34; }
	inline void set_typeofT1_34(Type_t * value)
	{
		___typeofT1_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_34), value);
	}

	inline static int32_t get_offset_of_typeofT2_35() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT2_35)); }
	inline Type_t * get_typeofT2_35() const { return ___typeofT2_35; }
	inline Type_t ** get_address_of_typeofT2_35() { return &___typeofT2_35; }
	inline void set_typeofT2_35(Type_t * value)
	{
		___typeofT2_35 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_35), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_36() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofTPlugOptions_36)); }
	inline Type_t * get_typeofTPlugOptions_36() const { return ___typeofTPlugOptions_36; }
	inline Type_t ** get_address_of_typeofTPlugOptions_36() { return &___typeofTPlugOptions_36; }
	inline void set_typeofTPlugOptions_36(Type_t * value)
	{
		___typeofTPlugOptions_36 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_36), value);
	}

	inline static int32_t get_offset_of_active_37() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___active_37)); }
	inline bool get_active_37() const { return ___active_37; }
	inline bool* get_address_of_active_37() { return &___active_37; }
	inline void set_active_37(bool value)
	{
		___active_37 = value;
	}

	inline static int32_t get_offset_of_isSequenced_38() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSequenced_38)); }
	inline bool get_isSequenced_38() const { return ___isSequenced_38; }
	inline bool* get_address_of_isSequenced_38() { return &___isSequenced_38; }
	inline void set_isSequenced_38(bool value)
	{
		___isSequenced_38 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_39() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___sequenceParent_39)); }
	inline Sequence_t2050373119 * get_sequenceParent_39() const { return ___sequenceParent_39; }
	inline Sequence_t2050373119 ** get_address_of_sequenceParent_39() { return &___sequenceParent_39; }
	inline void set_sequenceParent_39(Sequence_t2050373119 * value)
	{
		___sequenceParent_39 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_39), value);
	}

	inline static int32_t get_offset_of_activeId_40() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___activeId_40)); }
	inline int32_t get_activeId_40() const { return ___activeId_40; }
	inline int32_t* get_address_of_activeId_40() { return &___activeId_40; }
	inline void set_activeId_40(int32_t value)
	{
		___activeId_40 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_41() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___specialStartupMode_41)); }
	inline int32_t get_specialStartupMode_41() const { return ___specialStartupMode_41; }
	inline int32_t* get_address_of_specialStartupMode_41() { return &___specialStartupMode_41; }
	inline void set_specialStartupMode_41(int32_t value)
	{
		___specialStartupMode_41 = value;
	}

	inline static int32_t get_offset_of_creationLocked_42() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___creationLocked_42)); }
	inline bool get_creationLocked_42() const { return ___creationLocked_42; }
	inline bool* get_address_of_creationLocked_42() { return &___creationLocked_42; }
	inline void set_creationLocked_42(bool value)
	{
		___creationLocked_42 = value;
	}

	inline static int32_t get_offset_of_startupDone_43() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___startupDone_43)); }
	inline bool get_startupDone_43() const { return ___startupDone_43; }
	inline bool* get_address_of_startupDone_43() { return &___startupDone_43; }
	inline void set_startupDone_43(bool value)
	{
		___startupDone_43 = value;
	}

	inline static int32_t get_offset_of_playedOnce_44() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___playedOnce_44)); }
	inline bool get_playedOnce_44() const { return ___playedOnce_44; }
	inline bool* get_address_of_playedOnce_44() { return &___playedOnce_44; }
	inline void set_playedOnce_44(bool value)
	{
		___playedOnce_44 = value;
	}

	inline static int32_t get_offset_of_position_45() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___position_45)); }
	inline float get_position_45() const { return ___position_45; }
	inline float* get_address_of_position_45() { return &___position_45; }
	inline void set_position_45(float value)
	{
		___position_45 = value;
	}

	inline static int32_t get_offset_of_fullDuration_46() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___fullDuration_46)); }
	inline float get_fullDuration_46() const { return ___fullDuration_46; }
	inline float* get_address_of_fullDuration_46() { return &___fullDuration_46; }
	inline void set_fullDuration_46(float value)
	{
		___fullDuration_46 = value;
	}

	inline static int32_t get_offset_of_completedLoops_47() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___completedLoops_47)); }
	inline int32_t get_completedLoops_47() const { return ___completedLoops_47; }
	inline int32_t* get_address_of_completedLoops_47() { return &___completedLoops_47; }
	inline void set_completedLoops_47(int32_t value)
	{
		___completedLoops_47 = value;
	}

	inline static int32_t get_offset_of_isPlaying_48() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isPlaying_48)); }
	inline bool get_isPlaying_48() const { return ___isPlaying_48; }
	inline bool* get_address_of_isPlaying_48() { return &___isPlaying_48; }
	inline void set_isPlaying_48(bool value)
	{
		___isPlaying_48 = value;
	}

	inline static int32_t get_offset_of_isComplete_49() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isComplete_49)); }
	inline bool get_isComplete_49() const { return ___isComplete_49; }
	inline bool* get_address_of_isComplete_49() { return &___isComplete_49; }
	inline void set_isComplete_49(bool value)
	{
		___isComplete_49 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_50() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___elapsedDelay_50)); }
	inline float get_elapsedDelay_50() const { return ___elapsedDelay_50; }
	inline float* get_address_of_elapsedDelay_50() { return &___elapsedDelay_50; }
	inline void set_elapsedDelay_50(float value)
	{
		___elapsedDelay_50 = value;
	}

	inline static int32_t get_offset_of_delayComplete_51() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delayComplete_51)); }
	inline bool get_delayComplete_51() const { return ___delayComplete_51; }
	inline bool* get_address_of_delayComplete_51() { return &___delayComplete_51; }
	inline void set_delayComplete_51(bool value)
	{
		___delayComplete_51 = value;
	}

	inline static int32_t get_offset_of_miscInt_52() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___miscInt_52)); }
	inline int32_t get_miscInt_52() const { return ___miscInt_52; }
	inline int32_t* get_address_of_miscInt_52() { return &___miscInt_52; }
	inline void set_miscInt_52(int32_t value)
	{
		___miscInt_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T2342918553_H
#ifndef JOINT_T1358886274_H
#define JOINT_T1358886274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Joint
struct  Joint_t1358886274  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINT_T1358886274_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TWEENCALLBACK_T3727756325_H
#define TWEENCALLBACK_T3727756325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenCallback
struct  TweenCallback_t3727756325  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENCALLBACK_T3727756325_H
#ifndef CONSTRUCTORDELEGATE_T361514971_H
#define CONSTRUCTORDELEGATE_T361514971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t361514971  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORDELEGATE_T361514971_H
#ifndef SETDELEGATE_T2185717595_H
#define SETDELEGATE_T2185717595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct  SetDelegate_t2185717595  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETDELEGATE_T2185717595_H
#ifndef GETDELEGATE_T990642039_H
#define GETDELEGATE_T990642039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct  GetDelegate_t990642039  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDELEGATE_T990642039_H
#ifndef EASEFUNCTION_T3531141372_H
#define EASEFUNCTION_T3531141372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFunction
struct  EaseFunction_t3531141372  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFUNCTION_T3531141372_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef WILLRENDERCANVASES_T3309123499_H
#define WILLRENDERCANVASES_T3309123499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas/WillRenderCanvases
struct  WillRenderCanvases_t3309123499  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_T3309123499_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef CANVASRENDERER_T2598313366_H
#define CANVASRENDERER_T2598313366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_t2598313366  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_T2598313366_H
#ifndef UPDATEDEVENTHANDLER_T1027848393_H
#define UPDATEDEVENTHANDLER_T1027848393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t1027848393  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T1027848393_H
#ifndef SPHERECOLLIDER_T2077223608_H
#define SPHERECOLLIDER_T2077223608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SphereCollider
struct  SphereCollider_t2077223608  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERECOLLIDER_T2077223608_H
#ifndef SEQUENCE_T2050373119_H
#define SEQUENCE_T2050373119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Sequence
struct  Sequence_t2050373119  : public Tween_t2342918553
{
public:
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Sequence::sequencedTweens
	List_1_t3814993295 * ___sequencedTweens_53;
	// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable> DG.Tweening.Sequence::_sequencedObjs
	List_1_t553148457 * ____sequencedObjs_54;
	// System.Single DG.Tweening.Sequence::lastTweenInsertTime
	float ___lastTweenInsertTime_55;

public:
	inline static int32_t get_offset_of_sequencedTweens_53() { return static_cast<int32_t>(offsetof(Sequence_t2050373119, ___sequencedTweens_53)); }
	inline List_1_t3814993295 * get_sequencedTweens_53() const { return ___sequencedTweens_53; }
	inline List_1_t3814993295 ** get_address_of_sequencedTweens_53() { return &___sequencedTweens_53; }
	inline void set_sequencedTweens_53(List_1_t3814993295 * value)
	{
		___sequencedTweens_53 = value;
		Il2CppCodeGenWriteBarrier((&___sequencedTweens_53), value);
	}

	inline static int32_t get_offset_of__sequencedObjs_54() { return static_cast<int32_t>(offsetof(Sequence_t2050373119, ____sequencedObjs_54)); }
	inline List_1_t553148457 * get__sequencedObjs_54() const { return ____sequencedObjs_54; }
	inline List_1_t553148457 ** get_address_of__sequencedObjs_54() { return &____sequencedObjs_54; }
	inline void set__sequencedObjs_54(List_1_t553148457 * value)
	{
		____sequencedObjs_54 = value;
		Il2CppCodeGenWriteBarrier((&____sequencedObjs_54), value);
	}

	inline static int32_t get_offset_of_lastTweenInsertTime_55() { return static_cast<int32_t>(offsetof(Sequence_t2050373119, ___lastTweenInsertTime_55)); }
	inline float get_lastTweenInsertTime_55() const { return ___lastTweenInsertTime_55; }
	inline float* get_address_of_lastTweenInsertTime_55() { return &___lastTweenInsertTime_55; }
	inline void set_lastTweenInsertTime_55(float value)
	{
		___lastTweenInsertTime_55 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCE_T2050373119_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef CHARACTERCONTROLLER_T1138636865_H
#define CHARACTERCONTROLLER_T1138636865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterController
struct  CharacterController_t1138636865  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLER_T1138636865_H
#ifndef HINGEJOINT_T2516258575_H
#define HINGEJOINT_T2516258575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HingeJoint
struct  HingeJoint_t2516258575  : public Joint_t1358886274
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HINGEJOINT_T2516258575_H
#ifndef CAPSULECOLLIDER_T197597763_H
#define CAPSULECOLLIDER_T197597763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CapsuleCollider
struct  CapsuleCollider_t197597763  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPSULECOLLIDER_T197597763_H
#ifndef BOXCOLLIDER_T1640800422_H
#define BOXCOLLIDER_T1640800422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider
struct  BoxCollider_t1640800422  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER_T1640800422_H
#ifndef MESHCOLLIDER_T903564387_H
#define MESHCOLLIDER_T903564387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshCollider
struct  MeshCollider_t903564387  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCOLLIDER_T903564387_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (ContactPoint_t3758755253)+ sizeof (RuntimeObject), sizeof(ContactPoint_t3758755253 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	ContactPoint_t3758755253::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ContactPoint_t3758755253::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ContactPoint_t3758755253::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ContactPoint_t3758755253::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ContactPoint_t3758755253::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (Rigidbody_t3916780224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (Joint_t1358886274), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (HingeJoint_t2516258575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (Collider_t1773347010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (BoxCollider_t1640800422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (SphereCollider_t2077223608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (MeshCollider_t903564387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (CapsuleCollider_t197597763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (RaycastHit_t1056001966)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[6] = 
{
	RaycastHit_t1056001966::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit_t1056001966::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (CharacterController_t1138636865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (RigidbodyConstraints_t3348042902)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1811[11] = 
{
	RigidbodyConstraints_t3348042902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (JointLimits_t2681268900)+ sizeof (RuntimeObject), sizeof(JointLimits_t2681268900 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[7] = 
{
	JointLimits_t2681268900::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_Bounciness_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_BounceMinVelocity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_m_ContactDistance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_minBounce_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JointLimits_t2681268900::get_offset_of_maxBounce_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (ControllerColliderHit_t240592346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[7] = 
{
	ControllerColliderHit_t240592346::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t240592346::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t240592346::get_offset_of_m_Point_2(),
	ControllerColliderHit_t240592346::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t240592346::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (Collision_t4262080450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[5] = 
{
	Collision_t4262080450::get_offset_of_m_Impulse_0(),
	Collision_t4262080450::get_offset_of_m_RelativeVelocity_1(),
	Collision_t4262080450::get_offset_of_m_Rigidbody_2(),
	Collision_t4262080450::get_offset_of_m_Collider_3(),
	Collision_t4262080450::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (QueryTriggerInteraction_t962663221)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	QueryTriggerInteraction_t962663221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (U3CModuleU3E_t692745542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (RenderMode_t4077056833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[4] = 
{
	RenderMode_t4077056833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (Canvas_t3310196443), -1, sizeof(Canvas_t3310196443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1818[1] = 
{
	Canvas_t3310196443_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (WillRenderCanvases_t3309123499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (UISystemProfilerApi_t2230074258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (SampleType_t1208595618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1821[3] = 
{
	SampleType_t1208595618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (CanvasGroup_t4083511760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (CanvasRenderer_t2598313366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (RectTransformUtility_t1743242446), -1, sizeof(RectTransformUtility_t1743242446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1825[1] = 
{
	RectTransformUtility_t1743242446_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (U3CModuleU3E_t692745543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (CustomEventData_t317522481), sizeof(CustomEventData_t317522481_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[1] = 
{
	CustomEventData_t317522481::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (UnityAnalyticsHandler_t3011359618), sizeof(UnityAnalyticsHandler_t3011359618_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1828[1] = 
{
	UnityAnalyticsHandler_t3011359618::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (AnalyticsResult_t2273004240)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[9] = 
{
	AnalyticsResult_t2273004240::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (Analytics_t661012366), -1, sizeof(Analytics_t661012366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	Analytics_t661012366_StaticFields::get_offset_of_s_UnityAnalyticsHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (UnityAdsSettings_t2370518495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (AnalyticsSessionState_t681173134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1835[5] = 
{
	AnalyticsSessionState_t681173134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (AnalyticsSessionInfo_t2322308579), -1, sizeof(AnalyticsSessionInfo_t2322308579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[1] = 
{
	AnalyticsSessionInfo_t2322308579_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (WWW_t3688466362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[1] = 
{
	WWW_t3688466362::get_offset_of__uwr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (JsonArray_t3708654325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (JsonObject_t3371841791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[1] = 
{
	JsonObject_t3371841791::get_offset_of__members_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (SimpleJson_t666527214), -1, sizeof(SimpleJson_t666527214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1843[2] = 
{
	SimpleJson_t666527214_StaticFields::get_offset_of__currentJsonSerializerStrategy_0(),
	SimpleJson_t666527214_StaticFields::get_offset_of__pocoJsonSerializerStrategy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (PocoJsonSerializerStrategy_t3084478566), -1, sizeof(PocoJsonSerializerStrategy_t3084478566_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1845[6] = 
{
	PocoJsonSerializerStrategy_t3084478566::get_offset_of_ConstructorCache_0(),
	PocoJsonSerializerStrategy_t3084478566::get_offset_of_GetCache_1(),
	PocoJsonSerializerStrategy_t3084478566::get_offset_of_SetCache_2(),
	PocoJsonSerializerStrategy_t3084478566_StaticFields::get_offset_of_EmptyTypes_3(),
	PocoJsonSerializerStrategy_t3084478566_StaticFields::get_offset_of_ArrayConstructorParameterTypes_4(),
	PocoJsonSerializerStrategy_t3084478566_StaticFields::get_offset_of_Iso8601Format_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (ReflectionUtils_t3359723746), -1, sizeof(ReflectionUtils_t3359723746_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1846[1] = 
{
	ReflectionUtils_t3359723746_StaticFields::get_offset_of_EmptyObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (GetDelegate_t990642039), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (SetDelegate_t2185717595), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ConstructorDelegate_t361514971), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey0_t1111257005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	U3CGetConstructorByReflectionU3Ec__AnonStorey0_t1111257005::get_offset_of_constructorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t264984308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t264984308::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t264918772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t264918772::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t962176190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey3_t962176190::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t962110654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t962110654::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (AutoPlay_t1346164433)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[5] = 
{
	AutoPlay_t1346164433::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (AxisConstraint_t2771958344)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1860[6] = 
{
	AxisConstraint_t2771958344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Color2_t3097643075)+ sizeof (RuntimeObject), sizeof(Color2_t3097643075 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[2] = 
{
	Color2_t3097643075::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Color2_t3097643075::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (TweenCallback_t3727756325), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (EaseFunction_t3531141372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (DOTween_t2744875806), -1, sizeof(DOTween_t2744875806_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1865[26] = 
{
	DOTween_t2744875806_StaticFields::get_offset_of_Version_0(),
	DOTween_t2744875806_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t2744875806_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t2744875806_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t2744875806_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t2744875806_StaticFields::get_offset_of_maxSmoothUnscaledTime_5(),
	DOTween_t2744875806_StaticFields::get_offset_of_rewindCallbackMode_6(),
	DOTween_t2744875806_StaticFields::get_offset_of__logBehaviour_7(),
	DOTween_t2744875806_StaticFields::get_offset_of_drawGizmos_8(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultUpdateType_9(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultTimeScaleIndependent_10(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultAutoPlay_11(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultAutoKill_12(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultLoopType_13(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultRecyclable_14(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultEaseType_15(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_16(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultEasePeriod_17(),
	DOTween_t2744875806_StaticFields::get_offset_of_instance_18(),
	DOTween_t2744875806_StaticFields::get_offset_of_isUnityEditor_19(),
	DOTween_t2744875806_StaticFields::get_offset_of_isDebugBuild_20(),
	DOTween_t2744875806_StaticFields::get_offset_of_maxActiveTweenersReached_21(),
	DOTween_t2744875806_StaticFields::get_offset_of_maxActiveSequencesReached_22(),
	DOTween_t2744875806_StaticFields::get_offset_of_GizmosDelegates_23(),
	DOTween_t2744875806_StaticFields::get_offset_of_initialized_24(),
	DOTween_t2744875806_StaticFields::get_offset_of_isQuitting_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (U3CU3Ec__DisplayClass55_0_t2778626132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[2] = 
{
	U3CU3Ec__DisplayClass55_0_t2778626132::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass55_0_t2778626132::get_offset_of_setter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (DOVirtual_t3355183605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (U3CU3Ec__DisplayClass0_0_t3914388664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3914388664::get_offset_of_val_0(),
	U3CU3Ec__DisplayClass0_0_t3914388664::get_offset_of_onVirtualUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (Ease_t4010715394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[39] = 
{
	Ease_t4010715394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (EaseFactory_t2344806846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (U3CU3Ec__DisplayClass2_0_t753111370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[2] = 
{
	U3CU3Ec__DisplayClass2_0_t753111370::get_offset_of_motionDelay_0(),
	U3CU3Ec__DisplayClass2_0_t753111370::get_offset_of_customEase_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (PathMode_t2165603100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1873[5] = 
{
	PathMode_t2165603100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (PathType_t3777299409)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1874[3] = 
{
	PathType_t3777299409::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (RotateMode_t2548570174)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1875[5] = 
{
	RotateMode_t2548570174::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (ScrambleMode_t1285273342)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[7] = 
{
	ScrambleMode_t1285273342::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (TweenExtensions_t3641337881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (LoopType_t3049802818)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1878[4] = 
{
	LoopType_t3049802818::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (Sequence_t2050373119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[3] = 
{
	Sequence_t2050373119::get_offset_of_sequencedTweens_53(),
	Sequence_t2050373119::get_offset_of__sequencedObjs_54(),
	Sequence_t2050373119::get_offset_of_lastTweenInsertTime_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (ShortcutExtensions_t1665800578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (U3CU3Ec__DisplayClass0_0_t481505371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[1] = 
{
	U3CU3Ec__DisplayClass0_0_t481505371::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (U3CU3Ec__DisplayClass1_0_t481505372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	U3CU3Ec__DisplayClass1_0_t481505372::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (U3CU3Ec__DisplayClass2_0_t481505373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[1] = 
{
	U3CU3Ec__DisplayClass2_0_t481505373::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (U3CU3Ec__DisplayClass3_0_t481505374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[1] = 
{
	U3CU3Ec__DisplayClass3_0_t481505374::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (U3CU3Ec__DisplayClass4_0_t481505367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[1] = 
{
	U3CU3Ec__DisplayClass4_0_t481505367::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (U3CU3Ec__DisplayClass5_0_t481505368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[1] = 
{
	U3CU3Ec__DisplayClass5_0_t481505368::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (U3CU3Ec__DisplayClass6_0_t481505369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[1] = 
{
	U3CU3Ec__DisplayClass6_0_t481505369::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (U3CU3Ec__DisplayClass7_0_t481505370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[1] = 
{
	U3CU3Ec__DisplayClass7_0_t481505370::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (U3CU3Ec__DisplayClass8_0_t481505379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[1] = 
{
	U3CU3Ec__DisplayClass8_0_t481505379::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (U3CU3Ec__DisplayClass9_0_t481505380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[1] = 
{
	U3CU3Ec__DisplayClass9_0_t481505380::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (U3CU3Ec__DisplayClass10_0_t2368731853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[1] = 
{
	U3CU3Ec__DisplayClass10_0_t2368731853::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (U3CU3Ec__DisplayClass11_0_t2368797389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2368797389::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (U3CU3Ec__DisplayClass12_0_t2368600781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2368600781::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (U3CU3Ec__DisplayClass13_0_t2368666317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	U3CU3Ec__DisplayClass13_0_t2368666317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (U3CU3Ec__DisplayClass14_0_t2368993997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[1] = 
{
	U3CU3Ec__DisplayClass14_0_t2368993997::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (U3CU3Ec__DisplayClass15_0_t2369059533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[1] = 
{
	U3CU3Ec__DisplayClass15_0_t2369059533::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (U3CU3Ec__DisplayClass16_0_t2368862925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[1] = 
{
	U3CU3Ec__DisplayClass16_0_t2368862925::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (U3CU3Ec__DisplayClass17_0_t2368928461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[2] = 
{
	U3CU3Ec__DisplayClass17_0_t2368928461::get_offset_of_startValue_0(),
	U3CU3Ec__DisplayClass17_0_t2368928461::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (U3CU3Ec__DisplayClass18_0_t2368207565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[1] = 
{
	U3CU3Ec__DisplayClass18_0_t2368207565::get_offset_of_target_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
