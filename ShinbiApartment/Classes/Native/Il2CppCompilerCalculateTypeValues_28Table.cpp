﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip>
struct IEqualityComparer_1_t130870709;
// Spine.EventData
struct EventData_t724759987;
// System.String
struct String_t;
// Spine.AnimationState
struct AnimationState_t3637309382;
// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction>
struct ExposedList_1_t2759233236;
// Spine.ExposedList`1<Spine.Attachment>
struct ExposedList_1_t1455901122;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t651787775;
// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh>
struct DoubleBuffered_1_t2489386064;
// Spine.ExposedList`1<UnityEngine.Material>
struct ExposedList_1_t3047486989;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[]
struct MixModeU5BU5D_t2594259929;
// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation>
struct Dictionary_2_t3799463910;
// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32>
struct Dictionary_2_t3091039290;
// System.Collections.Generic.List`1<Spine.Animation>
struct List_1_t2087858025;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t333824601;
// UnityEngine.Animator
struct Animator_t434523843;
// Spine.Unity.AtlasAsset
struct AtlasAsset_t1167231206;
// Spine.Unity.Modules.SkeletonRagdoll2D
struct SkeletonRagdoll2D_t2972821364;
// Spine.AtlasRegion
struct AtlasRegion_t13903284;
// Spine.TransformConstraintData
struct TransformConstraintData_t529073346;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_t3793468194;
// Spine.Bone
struct Bone_t1086356328;
// Spine.ExposedList`1<Spine.BoneData>
struct ExposedList_1_t1542319060;
// Spine.BoneData
struct BoneData_t3130174490;
// Spine.Unity.Modules.SkeletonRagdoll
struct SkeletonRagdoll_t480923817;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>>
struct ExposedList_1_t2516523210;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>>
struct ExposedList_1_t4070202189;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1363090323;
// Spine.ExposedList`1<System.Boolean>
struct ExposedList_1_t2804399831;
// Spine.Pool`1<Spine.ExposedList`1<System.Single>>
struct Pool_1_t3075278099;
// Spine.Pool`1<Spine.ExposedList`1<System.Int32>>
struct Pool_1_t333989782;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Void
struct Void_t1185182177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t1354683548;
// Spine.Skeleton
struct Skeleton_t3686076450;
// Spine.ExposedList`1<UnityEngine.Vector3>
struct ExposedList_1_t2134458034;
// Spine.ExposedList`1<UnityEngine.Vector2>
struct ExposedList_1_t568374093;
// Spine.ExposedList`1<UnityEngine.Color32>
struct ExposedList_1_t1012645862;
// Spine.SkeletonClipping
struct SkeletonClipping_t1669006083;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// Spine.Unity.Modules.SkeletonGhostRenderer
struct SkeletonGhostRenderer_t2445315009;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D>
struct Dictionary_2_t47542197;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t2098681813;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t3748144825;
// Spine.Animation
struct Animation_t615783283;
// UnityEngine.TextAsset
struct TextAsset_t3022178571;
// Spine.Atlas
struct Atlas_t4040192941;
// Spine.Unity.AtlasAsset[]
struct AtlasAssetU5BU5D_t395880643;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t2933699135;
// Spine.SkeletonData
struct SkeletonData_t2032710716;
// Spine.AnimationStateData
struct AnimationStateData_t3010651567;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_t3931555305;
// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct SkeletonRendererDelegate_t3507789975;
// Spine.Unity.MeshGeneratorDelegate
struct MeshGeneratorDelegate_t1654156803;
// System.Collections.Generic.List`1<Spine.Slot>
struct List_1_t692048146;
// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct InstructionDelegate_t2225421195;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t3700682020;
// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material>
struct Dictionary_2_t3424054551;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// Spine.Unity.MeshRendererBuffers
struct MeshRendererBuffers_t756429994;
// Spine.PointAttachment
struct PointAttachment_t2275020146;
// Spine.Slot
struct Slot_t3514940700;
// Spine.BoundingBoxAttachment
struct BoundingBoxAttachment_t2797506510;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t57175488;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D>
struct Dictionary_2_t3714483914;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String>
struct Dictionary_2_t1209791819;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair>
struct List_1_t972525202;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer>
struct List_1_t2139201959;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material>
struct Dictionary_2_t556337403;
// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct SkeletonUtilityDelegate_t487527757;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone>
struct List_1_t3697594876;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint>
struct List_1_t2486457248;
// Spine.Unity.SkeletonUtility
struct SkeletonUtility_t2980767925;
// Spine.Unity.SkeletonUtilityBone
struct SkeletonUtilityBone_t2225520134;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t1744877482;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform>
struct Dictionary_2_t2478265129;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Shader
struct Shader_t4151988712;
// Spine.Unity.Modules.SkeletonGhostRenderer[]
struct SkeletonGhostRendererU5BU5D_t513912860;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride>
struct List_1_t2474053923;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride>
struct List_1_t3907116131;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct MecanimTranslator_t2363469064;
// Spine.Unity.UpdateBonesDelegate
struct UpdateBonesDelegate_t735903178;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Texture
struct Texture_t3661962703;

struct Vector3_t3722313464 ;
struct Vector2_t2156229523 ;
struct Color32_t2600501292 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef INTEQUALITYCOMPARER_T2049031273_H
#define INTEQUALITYCOMPARER_T2049031273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer
struct  IntEqualityComparer_t2049031273  : public RuntimeObject
{
public:

public:
};

struct IntEqualityComparer_t2049031273_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(IntEqualityComparer_t2049031273_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEQUALITYCOMPARER_T2049031273_H
#ifndef WAITFORSPINETRACKENTRYEND_T2617682820_H
#define WAITFORSPINETRACKENTRYEND_T2617682820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineTrackEntryEnd
struct  WaitForSpineTrackEntryEnd_t2617682820  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineTrackEntryEnd::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineTrackEntryEnd_t2617682820, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINETRACKENTRYEND_T2617682820_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SKELETONEXTENSIONS_T1502749191_H
#define SKELETONEXTENSIONS_T1502749191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonExtensions
struct  SkeletonExtensions_t1502749191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T1502749191_H
#ifndef ATTACHMENTCLONEEXTENSIONS_T460514876_H
#define ATTACHMENTCLONEEXTENSIONS_T460514876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentCloneExtensions
struct  AttachmentCloneExtensions_t460514876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTCLONEEXTENSIONS_T460514876_H
#ifndef ANIMATIONCLIPEQUALITYCOMPARER_T3042476764_H
#define ANIMATIONCLIPEQUALITYCOMPARER_T3042476764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer
struct  AnimationClipEqualityComparer_t3042476764  : public RuntimeObject
{
public:

public:
};

struct AnimationClipEqualityComparer_t3042476764_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip> Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationClipEqualityComparer_t3042476764_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPEQUALITYCOMPARER_T3042476764_H
#ifndef WAITFORSPINEEVENT_T3999047839_H
#define WAITFORSPINEEVENT_T3999047839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineEvent
struct  WaitForSpineEvent_t3999047839  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Unity.WaitForSpineEvent::m_TargetEvent
	EventData_t724759987 * ___m_TargetEvent_0;
	// System.String Spine.Unity.WaitForSpineEvent::m_EventName
	String_t* ___m_EventName_1;
	// Spine.AnimationState Spine.Unity.WaitForSpineEvent::m_AnimationState
	AnimationState_t3637309382 * ___m_AnimationState_2;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_WasFired
	bool ___m_WasFired_3;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_unsubscribeAfterFiring
	bool ___m_unsubscribeAfterFiring_4;

public:
	inline static int32_t get_offset_of_m_TargetEvent_0() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_TargetEvent_0)); }
	inline EventData_t724759987 * get_m_TargetEvent_0() const { return ___m_TargetEvent_0; }
	inline EventData_t724759987 ** get_address_of_m_TargetEvent_0() { return &___m_TargetEvent_0; }
	inline void set_m_TargetEvent_0(EventData_t724759987 * value)
	{
		___m_TargetEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetEvent_0), value);
	}

	inline static int32_t get_offset_of_m_EventName_1() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_EventName_1)); }
	inline String_t* get_m_EventName_1() const { return ___m_EventName_1; }
	inline String_t** get_address_of_m_EventName_1() { return &___m_EventName_1; }
	inline void set_m_EventName_1(String_t* value)
	{
		___m_EventName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_1), value);
	}

	inline static int32_t get_offset_of_m_AnimationState_2() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_AnimationState_2)); }
	inline AnimationState_t3637309382 * get_m_AnimationState_2() const { return ___m_AnimationState_2; }
	inline AnimationState_t3637309382 ** get_address_of_m_AnimationState_2() { return &___m_AnimationState_2; }
	inline void set_m_AnimationState_2(AnimationState_t3637309382 * value)
	{
		___m_AnimationState_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationState_2), value);
	}

	inline static int32_t get_offset_of_m_WasFired_3() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_WasFired_3)); }
	inline bool get_m_WasFired_3() const { return ___m_WasFired_3; }
	inline bool* get_address_of_m_WasFired_3() { return &___m_WasFired_3; }
	inline void set_m_WasFired_3(bool value)
	{
		___m_WasFired_3 = value;
	}

	inline static int32_t get_offset_of_m_unsubscribeAfterFiring_4() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3999047839, ___m_unsubscribeAfterFiring_4)); }
	inline bool get_m_unsubscribeAfterFiring_4() const { return ___m_unsubscribeAfterFiring_4; }
	inline bool* get_address_of_m_unsubscribeAfterFiring_4() { return &___m_unsubscribeAfterFiring_4; }
	inline void set_m_unsubscribeAfterFiring_4(bool value)
	{
		___m_unsubscribeAfterFiring_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEEVENT_T3999047839_H
#ifndef SKINUTILITIES_T498157063_H
#define SKINUTILITIES_T498157063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.SkinUtilities
struct  SkinUtilities_t498157063  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINUTILITIES_T498157063_H
#ifndef WAITFORSPINEANIMATIONCOMPLETE_T3713264939_H
#define WAITFORSPINEANIMATIONCOMPLETE_T3713264939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineAnimationComplete
struct  WaitForSpineAnimationComplete_t3713264939  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineAnimationComplete::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineAnimationComplete_t3713264939, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEANIMATIONCOMPLETE_T3713264939_H
#ifndef ATTACHMENTREGIONEXTENSIONS_T1591891965_H
#define ATTACHMENTREGIONEXTENSIONS_T1591891965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentRegionExtensions
struct  AttachmentRegionExtensions_t1591891965  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTREGIONEXTENSIONS_T1591891965_H
#ifndef SKELETONRENDERERINSTRUCTION_T651787775_H
#define SKELETONRENDERERINSTRUCTION_T651787775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRendererInstruction
struct  SkeletonRendererInstruction_t651787775  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::immutableTriangles
	bool ___immutableTriangles_0;
	// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction> Spine.Unity.SkeletonRendererInstruction::submeshInstructions
	ExposedList_1_t2759233236 * ___submeshInstructions_1;
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::hasActiveClipping
	bool ___hasActiveClipping_2;
	// System.Int32 Spine.Unity.SkeletonRendererInstruction::rawVertexCount
	int32_t ___rawVertexCount_3;
	// Spine.ExposedList`1<Spine.Attachment> Spine.Unity.SkeletonRendererInstruction::attachments
	ExposedList_1_t1455901122 * ___attachments_4;

public:
	inline static int32_t get_offset_of_immutableTriangles_0() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___immutableTriangles_0)); }
	inline bool get_immutableTriangles_0() const { return ___immutableTriangles_0; }
	inline bool* get_address_of_immutableTriangles_0() { return &___immutableTriangles_0; }
	inline void set_immutableTriangles_0(bool value)
	{
		___immutableTriangles_0 = value;
	}

	inline static int32_t get_offset_of_submeshInstructions_1() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___submeshInstructions_1)); }
	inline ExposedList_1_t2759233236 * get_submeshInstructions_1() const { return ___submeshInstructions_1; }
	inline ExposedList_1_t2759233236 ** get_address_of_submeshInstructions_1() { return &___submeshInstructions_1; }
	inline void set_submeshInstructions_1(ExposedList_1_t2759233236 * value)
	{
		___submeshInstructions_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshInstructions_1), value);
	}

	inline static int32_t get_offset_of_hasActiveClipping_2() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___hasActiveClipping_2)); }
	inline bool get_hasActiveClipping_2() const { return ___hasActiveClipping_2; }
	inline bool* get_address_of_hasActiveClipping_2() { return &___hasActiveClipping_2; }
	inline void set_hasActiveClipping_2(bool value)
	{
		___hasActiveClipping_2 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_3() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___rawVertexCount_3)); }
	inline int32_t get_rawVertexCount_3() const { return ___rawVertexCount_3; }
	inline int32_t* get_address_of_rawVertexCount_3() { return &___rawVertexCount_3; }
	inline void set_rawVertexCount_3(int32_t value)
	{
		___rawVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_attachments_4() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t651787775, ___attachments_4)); }
	inline ExposedList_1_t1455901122 * get_attachments_4() const { return ___attachments_4; }
	inline ExposedList_1_t1455901122 ** get_address_of_attachments_4() { return &___attachments_4; }
	inline void set_attachments_4(ExposedList_1_t1455901122 * value)
	{
		___attachments_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERINSTRUCTION_T651787775_H
#ifndef SMARTMESH_T524811292_H
#define SMARTMESH_T524811292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers/SmartMesh
struct  SmartMesh_t524811292  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Spine.Unity.MeshRendererBuffers/SmartMesh::mesh
	Mesh_t3648964284 * ___mesh_0;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.MeshRendererBuffers/SmartMesh::instructionUsed
	SkeletonRendererInstruction_t651787775 * ___instructionUsed_1;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(SmartMesh_t524811292, ___mesh_0)); }
	inline Mesh_t3648964284 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t3648964284 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t3648964284 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_instructionUsed_1() { return static_cast<int32_t>(offsetof(SmartMesh_t524811292, ___instructionUsed_1)); }
	inline SkeletonRendererInstruction_t651787775 * get_instructionUsed_1() const { return ___instructionUsed_1; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_instructionUsed_1() { return &___instructionUsed_1; }
	inline void set_instructionUsed_1(SkeletonRendererInstruction_t651787775 * value)
	{
		___instructionUsed_1 = value;
		Il2CppCodeGenWriteBarrier((&___instructionUsed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTMESH_T524811292_H
#ifndef MESHRENDERERBUFFERS_T756429994_H
#define MESHRENDERERBUFFERS_T756429994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers
struct  MeshRendererBuffers_t756429994  : public RuntimeObject
{
public:
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.MeshRendererBuffers::doubleBufferedMesh
	DoubleBuffered_1_t2489386064 * ___doubleBufferedMesh_0;
	// Spine.ExposedList`1<UnityEngine.Material> Spine.Unity.MeshRendererBuffers::submeshMaterials
	ExposedList_1_t3047486989 * ___submeshMaterials_1;
	// UnityEngine.Material[] Spine.Unity.MeshRendererBuffers::sharedMaterials
	MaterialU5BU5D_t561872642* ___sharedMaterials_2;

public:
	inline static int32_t get_offset_of_doubleBufferedMesh_0() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t756429994, ___doubleBufferedMesh_0)); }
	inline DoubleBuffered_1_t2489386064 * get_doubleBufferedMesh_0() const { return ___doubleBufferedMesh_0; }
	inline DoubleBuffered_1_t2489386064 ** get_address_of_doubleBufferedMesh_0() { return &___doubleBufferedMesh_0; }
	inline void set_doubleBufferedMesh_0(DoubleBuffered_1_t2489386064 * value)
	{
		___doubleBufferedMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___doubleBufferedMesh_0), value);
	}

	inline static int32_t get_offset_of_submeshMaterials_1() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t756429994, ___submeshMaterials_1)); }
	inline ExposedList_1_t3047486989 * get_submeshMaterials_1() const { return ___submeshMaterials_1; }
	inline ExposedList_1_t3047486989 ** get_address_of_submeshMaterials_1() { return &___submeshMaterials_1; }
	inline void set_submeshMaterials_1(ExposedList_1_t3047486989 * value)
	{
		___submeshMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshMaterials_1), value);
	}

	inline static int32_t get_offset_of_sharedMaterials_2() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t756429994, ___sharedMaterials_2)); }
	inline MaterialU5BU5D_t561872642* get_sharedMaterials_2() const { return ___sharedMaterials_2; }
	inline MaterialU5BU5D_t561872642** get_address_of_sharedMaterials_2() { return &___sharedMaterials_2; }
	inline void set_sharedMaterials_2(MaterialU5BU5D_t561872642* value)
	{
		___sharedMaterials_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterials_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERERBUFFERS_T756429994_H
#ifndef MECANIMTRANSLATOR_T2363469064_H
#define MECANIMTRANSLATOR_T2363469064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct  MecanimTranslator_t2363469064  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonAnimator/MecanimTranslator::autoReset
	bool ___autoReset_0;
	// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[] Spine.Unity.SkeletonAnimator/MecanimTranslator::layerMixModes
	MixModeU5BU5D_t2594259929* ___layerMixModes_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::animationTable
	Dictionary_2_t3799463910 * ___animationTable_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipNameHashCodeTable
	Dictionary_2_t3091039290 * ___clipNameHashCodeTable_3;
	// System.Collections.Generic.List`1<Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::previousAnimations
	List_1_t2087858025 * ___previousAnimations_4;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipInfoCache
	List_1_t333824601 * ___clipInfoCache_5;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator::nextClipInfoCache
	List_1_t333824601 * ___nextClipInfoCache_6;
	// UnityEngine.Animator Spine.Unity.SkeletonAnimator/MecanimTranslator::animator
	Animator_t434523843 * ___animator_7;

public:
	inline static int32_t get_offset_of_autoReset_0() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___autoReset_0)); }
	inline bool get_autoReset_0() const { return ___autoReset_0; }
	inline bool* get_address_of_autoReset_0() { return &___autoReset_0; }
	inline void set_autoReset_0(bool value)
	{
		___autoReset_0 = value;
	}

	inline static int32_t get_offset_of_layerMixModes_1() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___layerMixModes_1)); }
	inline MixModeU5BU5D_t2594259929* get_layerMixModes_1() const { return ___layerMixModes_1; }
	inline MixModeU5BU5D_t2594259929** get_address_of_layerMixModes_1() { return &___layerMixModes_1; }
	inline void set_layerMixModes_1(MixModeU5BU5D_t2594259929* value)
	{
		___layerMixModes_1 = value;
		Il2CppCodeGenWriteBarrier((&___layerMixModes_1), value);
	}

	inline static int32_t get_offset_of_animationTable_2() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___animationTable_2)); }
	inline Dictionary_2_t3799463910 * get_animationTable_2() const { return ___animationTable_2; }
	inline Dictionary_2_t3799463910 ** get_address_of_animationTable_2() { return &___animationTable_2; }
	inline void set_animationTable_2(Dictionary_2_t3799463910 * value)
	{
		___animationTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___animationTable_2), value);
	}

	inline static int32_t get_offset_of_clipNameHashCodeTable_3() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___clipNameHashCodeTable_3)); }
	inline Dictionary_2_t3091039290 * get_clipNameHashCodeTable_3() const { return ___clipNameHashCodeTable_3; }
	inline Dictionary_2_t3091039290 ** get_address_of_clipNameHashCodeTable_3() { return &___clipNameHashCodeTable_3; }
	inline void set_clipNameHashCodeTable_3(Dictionary_2_t3091039290 * value)
	{
		___clipNameHashCodeTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___clipNameHashCodeTable_3), value);
	}

	inline static int32_t get_offset_of_previousAnimations_4() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___previousAnimations_4)); }
	inline List_1_t2087858025 * get_previousAnimations_4() const { return ___previousAnimations_4; }
	inline List_1_t2087858025 ** get_address_of_previousAnimations_4() { return &___previousAnimations_4; }
	inline void set_previousAnimations_4(List_1_t2087858025 * value)
	{
		___previousAnimations_4 = value;
		Il2CppCodeGenWriteBarrier((&___previousAnimations_4), value);
	}

	inline static int32_t get_offset_of_clipInfoCache_5() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___clipInfoCache_5)); }
	inline List_1_t333824601 * get_clipInfoCache_5() const { return ___clipInfoCache_5; }
	inline List_1_t333824601 ** get_address_of_clipInfoCache_5() { return &___clipInfoCache_5; }
	inline void set_clipInfoCache_5(List_1_t333824601 * value)
	{
		___clipInfoCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___clipInfoCache_5), value);
	}

	inline static int32_t get_offset_of_nextClipInfoCache_6() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___nextClipInfoCache_6)); }
	inline List_1_t333824601 * get_nextClipInfoCache_6() const { return ___nextClipInfoCache_6; }
	inline List_1_t333824601 ** get_address_of_nextClipInfoCache_6() { return &___nextClipInfoCache_6; }
	inline void set_nextClipInfoCache_6(List_1_t333824601 * value)
	{
		___nextClipInfoCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextClipInfoCache_6), value);
	}

	inline static int32_t get_offset_of_animator_7() { return static_cast<int32_t>(offsetof(MecanimTranslator_t2363469064, ___animator_7)); }
	inline Animator_t434523843 * get_animator_7() const { return ___animator_7; }
	inline Animator_t434523843 ** get_address_of_animator_7() { return &___animator_7; }
	inline void set_animator_7(Animator_t434523843 * value)
	{
		___animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___animator_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MECANIMTRANSLATOR_T2363469064_H
#ifndef MATERIALSTEXTURELOADER_T1402074808_H
#define MATERIALSTEXTURELOADER_T1402074808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MaterialsTextureLoader
struct  MaterialsTextureLoader_t1402074808  : public RuntimeObject
{
public:
	// Spine.Unity.AtlasAsset Spine.Unity.MaterialsTextureLoader::atlasAsset
	AtlasAsset_t1167231206 * ___atlasAsset_0;

public:
	inline static int32_t get_offset_of_atlasAsset_0() { return static_cast<int32_t>(offsetof(MaterialsTextureLoader_t1402074808, ___atlasAsset_0)); }
	inline AtlasAsset_t1167231206 * get_atlasAsset_0() const { return ___atlasAsset_0; }
	inline AtlasAsset_t1167231206 ** get_address_of_atlasAsset_0() { return &___atlasAsset_0; }
	inline void set_atlasAsset_0(AtlasAsset_t1167231206 * value)
	{
		___atlasAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAsset_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSTEXTURELOADER_T1402074808_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3329131839_H
#define U3CSTARTU3EC__ITERATOR0_T3329131839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3329131839  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$this
	SkeletonRagdoll2D_t2972821364 * ___U24this_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24this_0)); }
	inline SkeletonRagdoll2D_t2972821364 * get_U24this_0() const { return ___U24this_0; }
	inline SkeletonRagdoll2D_t2972821364 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkeletonRagdoll2D_t2972821364 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3329131839, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3329131839_H
#ifndef REGIONLESSATTACHMENTLOADER_T2643180673_H
#define REGIONLESSATTACHMENTLOADER_T2643180673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.RegionlessAttachmentLoader
struct  RegionlessAttachmentLoader_t2643180673  : public RuntimeObject
{
public:

public:
};

struct RegionlessAttachmentLoader_t2643180673_StaticFields
{
public:
	// Spine.AtlasRegion Spine.Unity.RegionlessAttachmentLoader::emptyRegion
	AtlasRegion_t13903284 * ___emptyRegion_0;

public:
	inline static int32_t get_offset_of_emptyRegion_0() { return static_cast<int32_t>(offsetof(RegionlessAttachmentLoader_t2643180673_StaticFields, ___emptyRegion_0)); }
	inline AtlasRegion_t13903284 * get_emptyRegion_0() const { return ___emptyRegion_0; }
	inline AtlasRegion_t13903284 ** get_address_of_emptyRegion_0() { return &___emptyRegion_0; }
	inline void set_emptyRegion_0(AtlasRegion_t13903284 * value)
	{
		___emptyRegion_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyRegion_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONLESSATTACHMENTLOADER_T2643180673_H
#ifndef U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T3141170647_H
#define U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T3141170647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1
struct  U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::<startMix>__0
	float ___U3CstartMixU3E__0_1;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::target
	float ___target_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::duration
	float ___duration_3;
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$this
	SkeletonRagdoll2D_t2972821364 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U3CstartMixU3E__0_1)); }
	inline float get_U3CstartMixU3E__0_1() const { return ___U3CstartMixU3E__0_1; }
	inline float* get_address_of_U3CstartMixU3E__0_1() { return &___U3CstartMixU3E__0_1; }
	inline void set_U3CstartMixU3E__0_1(float value)
	{
		___U3CstartMixU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24this_4)); }
	inline SkeletonRagdoll2D_t2972821364 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonRagdoll2D_t2972821364 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonRagdoll2D_t2972821364 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T3141170647_H
#ifndef SKELETONEXTENSIONS_T671502775_H
#define SKELETONEXTENSIONS_T671502775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonExtensions
struct  SkeletonExtensions_t671502775  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T671502775_H
#ifndef TRANSFORMCONSTRAINT_T454030229_H
#define TRANSFORMCONSTRAINT_T454030229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraint
struct  TransformConstraint_t454030229  : public RuntimeObject
{
public:
	// Spine.TransformConstraintData Spine.TransformConstraint::data
	TransformConstraintData_t529073346 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.TransformConstraint::bones
	ExposedList_1_t3793468194 * ___bones_1;
	// Spine.Bone Spine.TransformConstraint::target
	Bone_t1086356328 * ___target_2;
	// System.Single Spine.TransformConstraint::rotateMix
	float ___rotateMix_3;
	// System.Single Spine.TransformConstraint::translateMix
	float ___translateMix_4;
	// System.Single Spine.TransformConstraint::scaleMix
	float ___scaleMix_5;
	// System.Single Spine.TransformConstraint::shearMix
	float ___shearMix_6;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___data_0)); }
	inline TransformConstraintData_t529073346 * get_data_0() const { return ___data_0; }
	inline TransformConstraintData_t529073346 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(TransformConstraintData_t529073346 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___bones_1)); }
	inline ExposedList_1_t3793468194 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3793468194 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3793468194 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___target_2)); }
	inline Bone_t1086356328 * get_target_2() const { return ___target_2; }
	inline Bone_t1086356328 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t1086356328 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_rotateMix_3() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___rotateMix_3)); }
	inline float get_rotateMix_3() const { return ___rotateMix_3; }
	inline float* get_address_of_rotateMix_3() { return &___rotateMix_3; }
	inline void set_rotateMix_3(float value)
	{
		___rotateMix_3 = value;
	}

	inline static int32_t get_offset_of_translateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___translateMix_4)); }
	inline float get_translateMix_4() const { return ___translateMix_4; }
	inline float* get_address_of_translateMix_4() { return &___translateMix_4; }
	inline void set_translateMix_4(float value)
	{
		___translateMix_4 = value;
	}

	inline static int32_t get_offset_of_scaleMix_5() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___scaleMix_5)); }
	inline float get_scaleMix_5() const { return ___scaleMix_5; }
	inline float* get_address_of_scaleMix_5() { return &___scaleMix_5; }
	inline void set_scaleMix_5(float value)
	{
		___scaleMix_5 = value;
	}

	inline static int32_t get_offset_of_shearMix_6() { return static_cast<int32_t>(offsetof(TransformConstraint_t454030229, ___shearMix_6)); }
	inline float get_shearMix_6() const { return ___shearMix_6; }
	inline float* get_address_of_shearMix_6() { return &___shearMix_6; }
	inline void set_shearMix_6(float value)
	{
		___shearMix_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINT_T454030229_H
#ifndef TRANSFORMCONSTRAINTDATA_T529073346_H
#define TRANSFORMCONSTRAINTDATA_T529073346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintData
struct  TransformConstraintData_t529073346  : public RuntimeObject
{
public:
	// System.String Spine.TransformConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.TransformConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.TransformConstraintData::bones
	ExposedList_1_t1542319060 * ___bones_2;
	// Spine.BoneData Spine.TransformConstraintData::target
	BoneData_t3130174490 * ___target_3;
	// System.Single Spine.TransformConstraintData::rotateMix
	float ___rotateMix_4;
	// System.Single Spine.TransformConstraintData::translateMix
	float ___translateMix_5;
	// System.Single Spine.TransformConstraintData::scaleMix
	float ___scaleMix_6;
	// System.Single Spine.TransformConstraintData::shearMix
	float ___shearMix_7;
	// System.Single Spine.TransformConstraintData::offsetRotation
	float ___offsetRotation_8;
	// System.Single Spine.TransformConstraintData::offsetX
	float ___offsetX_9;
	// System.Single Spine.TransformConstraintData::offsetY
	float ___offsetY_10;
	// System.Single Spine.TransformConstraintData::offsetScaleX
	float ___offsetScaleX_11;
	// System.Single Spine.TransformConstraintData::offsetScaleY
	float ___offsetScaleY_12;
	// System.Single Spine.TransformConstraintData::offsetShearY
	float ___offsetShearY_13;
	// System.Boolean Spine.TransformConstraintData::relative
	bool ___relative_14;
	// System.Boolean Spine.TransformConstraintData::local
	bool ___local_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___bones_2)); }
	inline ExposedList_1_t1542319060 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t1542319060 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t1542319060 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___target_3)); }
	inline BoneData_t3130174490 * get_target_3() const { return ___target_3; }
	inline BoneData_t3130174490 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t3130174490 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_rotateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___rotateMix_4)); }
	inline float get_rotateMix_4() const { return ___rotateMix_4; }
	inline float* get_address_of_rotateMix_4() { return &___rotateMix_4; }
	inline void set_rotateMix_4(float value)
	{
		___rotateMix_4 = value;
	}

	inline static int32_t get_offset_of_translateMix_5() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___translateMix_5)); }
	inline float get_translateMix_5() const { return ___translateMix_5; }
	inline float* get_address_of_translateMix_5() { return &___translateMix_5; }
	inline void set_translateMix_5(float value)
	{
		___translateMix_5 = value;
	}

	inline static int32_t get_offset_of_scaleMix_6() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___scaleMix_6)); }
	inline float get_scaleMix_6() const { return ___scaleMix_6; }
	inline float* get_address_of_scaleMix_6() { return &___scaleMix_6; }
	inline void set_scaleMix_6(float value)
	{
		___scaleMix_6 = value;
	}

	inline static int32_t get_offset_of_shearMix_7() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___shearMix_7)); }
	inline float get_shearMix_7() const { return ___shearMix_7; }
	inline float* get_address_of_shearMix_7() { return &___shearMix_7; }
	inline void set_shearMix_7(float value)
	{
		___shearMix_7 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_8() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetRotation_8)); }
	inline float get_offsetRotation_8() const { return ___offsetRotation_8; }
	inline float* get_address_of_offsetRotation_8() { return &___offsetRotation_8; }
	inline void set_offsetRotation_8(float value)
	{
		___offsetRotation_8 = value;
	}

	inline static int32_t get_offset_of_offsetX_9() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetX_9)); }
	inline float get_offsetX_9() const { return ___offsetX_9; }
	inline float* get_address_of_offsetX_9() { return &___offsetX_9; }
	inline void set_offsetX_9(float value)
	{
		___offsetX_9 = value;
	}

	inline static int32_t get_offset_of_offsetY_10() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetY_10)); }
	inline float get_offsetY_10() const { return ___offsetY_10; }
	inline float* get_address_of_offsetY_10() { return &___offsetY_10; }
	inline void set_offsetY_10(float value)
	{
		___offsetY_10 = value;
	}

	inline static int32_t get_offset_of_offsetScaleX_11() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetScaleX_11)); }
	inline float get_offsetScaleX_11() const { return ___offsetScaleX_11; }
	inline float* get_address_of_offsetScaleX_11() { return &___offsetScaleX_11; }
	inline void set_offsetScaleX_11(float value)
	{
		___offsetScaleX_11 = value;
	}

	inline static int32_t get_offset_of_offsetScaleY_12() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetScaleY_12)); }
	inline float get_offsetScaleY_12() const { return ___offsetScaleY_12; }
	inline float* get_address_of_offsetScaleY_12() { return &___offsetScaleY_12; }
	inline void set_offsetScaleY_12(float value)
	{
		___offsetScaleY_12 = value;
	}

	inline static int32_t get_offset_of_offsetShearY_13() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___offsetShearY_13)); }
	inline float get_offsetShearY_13() const { return ___offsetShearY_13; }
	inline float* get_address_of_offsetShearY_13() { return &___offsetShearY_13; }
	inline void set_offsetShearY_13(float value)
	{
		___offsetShearY_13 = value;
	}

	inline static int32_t get_offset_of_relative_14() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___relative_14)); }
	inline bool get_relative_14() const { return ___relative_14; }
	inline bool* get_address_of_relative_14() { return &___relative_14; }
	inline void set_relative_14(bool value)
	{
		___relative_14 = value;
	}

	inline static int32_t get_offset_of_local_15() { return static_cast<int32_t>(offsetof(TransformConstraintData_t529073346, ___local_15)); }
	inline bool get_local_15() const { return ___local_15; }
	inline bool* get_address_of_local_15() { return &___local_15; }
	inline void set_local_15(bool value)
	{
		___local_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTDATA_T529073346_H
#ifndef U3CSTARTU3EC__ITERATOR0_T294813647_H
#define U3CSTARTU3EC__ITERATOR0_T294813647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t294813647  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$this
	SkeletonRagdoll_t480923817 * ___U24this_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24this_0)); }
	inline SkeletonRagdoll_t480923817 * get_U24this_0() const { return ___U24this_0; }
	inline SkeletonRagdoll_t480923817 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkeletonRagdoll_t480923817 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t294813647, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T294813647_H
#ifndef TRIANGULATOR_T2502879214_H
#define TRIANGULATOR_T2502879214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Triangulator
struct  Triangulator_t2502879214  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::convexPolygons
	ExposedList_1_t2516523210 * ___convexPolygons_0;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::convexPolygonsIndices
	ExposedList_1_t4070202189 * ___convexPolygonsIndices_1;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::indicesArray
	ExposedList_1_t1363090323 * ___indicesArray_2;
	// Spine.ExposedList`1<System.Boolean> Spine.Triangulator::isConcaveArray
	ExposedList_1_t2804399831 * ___isConcaveArray_3;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::triangles
	ExposedList_1_t1363090323 * ___triangles_4;
	// Spine.Pool`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::polygonPool
	Pool_1_t3075278099 * ___polygonPool_5;
	// Spine.Pool`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::polygonIndicesPool
	Pool_1_t333989782 * ___polygonIndicesPool_6;

public:
	inline static int32_t get_offset_of_convexPolygons_0() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___convexPolygons_0)); }
	inline ExposedList_1_t2516523210 * get_convexPolygons_0() const { return ___convexPolygons_0; }
	inline ExposedList_1_t2516523210 ** get_address_of_convexPolygons_0() { return &___convexPolygons_0; }
	inline void set_convexPolygons_0(ExposedList_1_t2516523210 * value)
	{
		___convexPolygons_0 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygons_0), value);
	}

	inline static int32_t get_offset_of_convexPolygonsIndices_1() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___convexPolygonsIndices_1)); }
	inline ExposedList_1_t4070202189 * get_convexPolygonsIndices_1() const { return ___convexPolygonsIndices_1; }
	inline ExposedList_1_t4070202189 ** get_address_of_convexPolygonsIndices_1() { return &___convexPolygonsIndices_1; }
	inline void set_convexPolygonsIndices_1(ExposedList_1_t4070202189 * value)
	{
		___convexPolygonsIndices_1 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygonsIndices_1), value);
	}

	inline static int32_t get_offset_of_indicesArray_2() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___indicesArray_2)); }
	inline ExposedList_1_t1363090323 * get_indicesArray_2() const { return ___indicesArray_2; }
	inline ExposedList_1_t1363090323 ** get_address_of_indicesArray_2() { return &___indicesArray_2; }
	inline void set_indicesArray_2(ExposedList_1_t1363090323 * value)
	{
		___indicesArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___indicesArray_2), value);
	}

	inline static int32_t get_offset_of_isConcaveArray_3() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___isConcaveArray_3)); }
	inline ExposedList_1_t2804399831 * get_isConcaveArray_3() const { return ___isConcaveArray_3; }
	inline ExposedList_1_t2804399831 ** get_address_of_isConcaveArray_3() { return &___isConcaveArray_3; }
	inline void set_isConcaveArray_3(ExposedList_1_t2804399831 * value)
	{
		___isConcaveArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___isConcaveArray_3), value);
	}

	inline static int32_t get_offset_of_triangles_4() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___triangles_4)); }
	inline ExposedList_1_t1363090323 * get_triangles_4() const { return ___triangles_4; }
	inline ExposedList_1_t1363090323 ** get_address_of_triangles_4() { return &___triangles_4; }
	inline void set_triangles_4(ExposedList_1_t1363090323 * value)
	{
		___triangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_4), value);
	}

	inline static int32_t get_offset_of_polygonPool_5() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___polygonPool_5)); }
	inline Pool_1_t3075278099 * get_polygonPool_5() const { return ___polygonPool_5; }
	inline Pool_1_t3075278099 ** get_address_of_polygonPool_5() { return &___polygonPool_5; }
	inline void set_polygonPool_5(Pool_1_t3075278099 * value)
	{
		___polygonPool_5 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_5), value);
	}

	inline static int32_t get_offset_of_polygonIndicesPool_6() { return static_cast<int32_t>(offsetof(Triangulator_t2502879214, ___polygonIndicesPool_6)); }
	inline Pool_1_t333989782 * get_polygonIndicesPool_6() const { return ___polygonIndicesPool_6; }
	inline Pool_1_t333989782 ** get_address_of_polygonIndicesPool_6() { return &___polygonIndicesPool_6; }
	inline void set_polygonIndicesPool_6(Pool_1_t333989782 * value)
	{
		___polygonIndicesPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___polygonIndicesPool_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATOR_T2502879214_H
#ifndef U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T2718417871_H
#define U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T2718417871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1
struct  U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::<startMix>__0
	float ___U3CstartMixU3E__0_1;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::target
	float ___target_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::duration
	float ___duration_3;
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$this
	SkeletonRagdoll_t480923817 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U3CstartMixU3E__0_1)); }
	inline float get_U3CstartMixU3E__0_1() const { return ___U3CstartMixU3E__0_1; }
	inline float* get_address_of_U3CstartMixU3E__0_1() { return &___U3CstartMixU3E__0_1; }
	inline void set_U3CstartMixU3E__0_1(float value)
	{
		___U3CstartMixU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24this_4)); }
	inline SkeletonRagdoll_t480923817 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonRagdoll_t480923817 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonRagdoll_t480923817 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T2718417871_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef HIERARCHY_T2161623951_H
#define HIERARCHY_T2161623951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment/Hierarchy
struct  Hierarchy_t2161623951 
{
public:
	// System.String Spine.Unity.SpineAttachment/Hierarchy::skin
	String_t* ___skin_0;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::slot
	String_t* ___slot_1;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_skin_0() { return static_cast<int32_t>(offsetof(Hierarchy_t2161623951, ___skin_0)); }
	inline String_t* get_skin_0() const { return ___skin_0; }
	inline String_t** get_address_of_skin_0() { return &___skin_0; }
	inline void set_skin_0(String_t* value)
	{
		___skin_0 = value;
		Il2CppCodeGenWriteBarrier((&___skin_0), value);
	}

	inline static int32_t get_offset_of_slot_1() { return static_cast<int32_t>(offsetof(Hierarchy_t2161623951, ___slot_1)); }
	inline String_t* get_slot_1() const { return ___slot_1; }
	inline String_t** get_address_of_slot_1() { return &___slot_1; }
	inline void set_slot_1(String_t* value)
	{
		___slot_1 = value;
		Il2CppCodeGenWriteBarrier((&___slot_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Hierarchy_t2161623951, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t2161623951_marshaled_pinvoke
{
	char* ___skin_0;
	char* ___slot_1;
	char* ___name_2;
};
// Native definition for COM marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t2161623951_marshaled_com
{
	Il2CppChar* ___skin_0;
	Il2CppChar* ___slot_1;
	Il2CppChar* ___name_2;
};
#endif // HIERARCHY_T2161623951_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef TRANSFORMPAIR_T3795417756_H
#define TRANSFORMPAIR_T3795417756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct  TransformPair_t3795417756 
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::dest
	Transform_t3600365921 * ___dest_0;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::src
	Transform_t3600365921 * ___src_1;

public:
	inline static int32_t get_offset_of_dest_0() { return static_cast<int32_t>(offsetof(TransformPair_t3795417756, ___dest_0)); }
	inline Transform_t3600365921 * get_dest_0() const { return ___dest_0; }
	inline Transform_t3600365921 ** get_address_of_dest_0() { return &___dest_0; }
	inline void set_dest_0(Transform_t3600365921 * value)
	{
		___dest_0 = value;
		Il2CppCodeGenWriteBarrier((&___dest_0), value);
	}

	inline static int32_t get_offset_of_src_1() { return static_cast<int32_t>(offsetof(TransformPair_t3795417756, ___src_1)); }
	inline Transform_t3600365921 * get_src_1() const { return ___src_1; }
	inline Transform_t3600365921 ** get_address_of_src_1() { return &___src_1; }
	inline void set_src_1(Transform_t3600365921 * value)
	{
		___src_1 = value;
		Il2CppCodeGenWriteBarrier((&___src_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t3795417756_marshaled_pinvoke
{
	Transform_t3600365921 * ___dest_0;
	Transform_t3600365921 * ___src_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t3795417756_marshaled_com
{
	Transform_t3600365921 * ___dest_0;
	Transform_t3600365921 * ___src_1;
};
#endif // TRANSFORMPAIR_T3795417756_H
#ifndef U24ARRAYTYPEU3D20_T1702832645_H
#define U24ARRAYTYPEU3D20_T1702832645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832645 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832645__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832645_H
#ifndef ATLASMATERIALOVERRIDE_T2435041389_H
#define ATLASMATERIALOVERRIDE_T2435041389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct  AtlasMaterialOverride_t2435041389 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::originalMaterial
	Material_t340375123 * ___originalMaterial_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::replacementMaterial
	Material_t340375123 * ___replacementMaterial_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t2435041389, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_originalMaterial_1() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t2435041389, ___originalMaterial_1)); }
	inline Material_t340375123 * get_originalMaterial_1() const { return ___originalMaterial_1; }
	inline Material_t340375123 ** get_address_of_originalMaterial_1() { return &___originalMaterial_1; }
	inline void set_originalMaterial_1(Material_t340375123 * value)
	{
		___originalMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___originalMaterial_1), value);
	}

	inline static int32_t get_offset_of_replacementMaterial_2() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t2435041389, ___replacementMaterial_2)); }
	inline Material_t340375123 * get_replacementMaterial_2() const { return ___replacementMaterial_2; }
	inline Material_t340375123 ** get_address_of_replacementMaterial_2() { return &___replacementMaterial_2; }
	inline void set_replacementMaterial_2(Material_t340375123 * value)
	{
		___replacementMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___replacementMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_t2435041389_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	Material_t340375123 * ___originalMaterial_1;
	Material_t340375123 * ___replacementMaterial_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_t2435041389_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Material_t340375123 * ___originalMaterial_1;
	Material_t340375123 * ___replacementMaterial_2;
};
#endif // ATLASMATERIALOVERRIDE_T2435041389_H
#ifndef SLOTMATERIALOVERRIDE_T1001979181_H
#define SLOTMATERIALOVERRIDE_T1001979181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct  SlotMaterialOverride_t1001979181 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// System.String Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::slotName
	String_t* ___slotName_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::material
	Material_t340375123 * ___material_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t1001979181, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_slotName_1() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t1001979181, ___slotName_1)); }
	inline String_t* get_slotName_1() const { return ___slotName_1; }
	inline String_t** get_address_of_slotName_1() { return &___slotName_1; }
	inline void set_slotName_1(String_t* value)
	{
		___slotName_1 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_1), value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t1001979181, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_t1001979181_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	char* ___slotName_1;
	Material_t340375123 * ___material_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_t1001979181_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Il2CppChar* ___slotName_1;
	Material_t340375123 * ___material_2;
};
#endif // SLOTMATERIALOVERRIDE_T1001979181_H
#ifndef MATERIALTEXTUREPAIR_T1637207048_H
#define MATERIALTEXTUREPAIR_T1637207048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct  MaterialTexturePair_t1637207048 
{
public:
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::texture2D
	Texture2D_t3840446185 * ___texture2D_0;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::material
	Material_t340375123 * ___material_1;

public:
	inline static int32_t get_offset_of_texture2D_0() { return static_cast<int32_t>(offsetof(MaterialTexturePair_t1637207048, ___texture2D_0)); }
	inline Texture2D_t3840446185 * get_texture2D_0() const { return ___texture2D_0; }
	inline Texture2D_t3840446185 ** get_address_of_texture2D_0() { return &___texture2D_0; }
	inline void set_texture2D_0(Texture2D_t3840446185 * value)
	{
		___texture2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture2D_0), value);
	}

	inline static int32_t get_offset_of_material_1() { return static_cast<int32_t>(offsetof(MaterialTexturePair_t1637207048, ___material_1)); }
	inline Material_t340375123 * get_material_1() const { return ___material_1; }
	inline Material_t340375123 ** get_address_of_material_1() { return &___material_1; }
	inline void set_material_1(Material_t340375123 * value)
	{
		___material_1 = value;
		Il2CppCodeGenWriteBarrier((&___material_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_t1637207048_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___texture2D_0;
	Material_t340375123 * ___material_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_t1637207048_marshaled_com
{
	Texture2D_t3840446185 * ___texture2D_0;
	Material_t340375123 * ___material_1;
};
#endif // MATERIALTEXTUREPAIR_T1637207048_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef BONEMATRIX_T2668835335_H
#define BONEMATRIX_T2668835335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoneMatrix
struct  BoneMatrix_t2668835335 
{
public:
	// System.Single Spine.BoneMatrix::a
	float ___a_0;
	// System.Single Spine.BoneMatrix::b
	float ___b_1;
	// System.Single Spine.BoneMatrix::c
	float ___c_2;
	// System.Single Spine.BoneMatrix::d
	float ___d_3;
	// System.Single Spine.BoneMatrix::x
	float ___x_4;
	// System.Single Spine.BoneMatrix::y
	float ___y_5;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(BoneMatrix_t2668835335, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(BoneMatrix_t2668835335, ___b_1)); }
	inline float get_b_1() const { return ___b_1; }
	inline float* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(float value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(BoneMatrix_t2668835335, ___c_2)); }
	inline float get_c_2() const { return ___c_2; }
	inline float* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(float value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(BoneMatrix_t2668835335, ___d_3)); }
	inline float get_d_3() const { return ___d_3; }
	inline float* get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(float value)
	{
		___d_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(BoneMatrix_t2668835335, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(BoneMatrix_t2668835335, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEMATRIX_T2668835335_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef SETTINGS_T612870457_H
#define SETTINGS_T612870457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator/Settings
struct  Settings_t612870457 
{
public:
	// System.Boolean Spine.Unity.MeshGenerator/Settings::useClipping
	bool ___useClipping_0;
	// System.Single Spine.Unity.MeshGenerator/Settings::zSpacing
	float ___zSpacing_1;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::pmaVertexColors
	bool ___pmaVertexColors_2;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::tintBlack
	bool ___tintBlack_3;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::calculateTangents
	bool ___calculateTangents_4;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::addNormals
	bool ___addNormals_5;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::immutableTriangles
	bool ___immutableTriangles_6;

public:
	inline static int32_t get_offset_of_useClipping_0() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___useClipping_0)); }
	inline bool get_useClipping_0() const { return ___useClipping_0; }
	inline bool* get_address_of_useClipping_0() { return &___useClipping_0; }
	inline void set_useClipping_0(bool value)
	{
		___useClipping_0 = value;
	}

	inline static int32_t get_offset_of_zSpacing_1() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___zSpacing_1)); }
	inline float get_zSpacing_1() const { return ___zSpacing_1; }
	inline float* get_address_of_zSpacing_1() { return &___zSpacing_1; }
	inline void set_zSpacing_1(float value)
	{
		___zSpacing_1 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_2() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___pmaVertexColors_2)); }
	inline bool get_pmaVertexColors_2() const { return ___pmaVertexColors_2; }
	inline bool* get_address_of_pmaVertexColors_2() { return &___pmaVertexColors_2; }
	inline void set_pmaVertexColors_2(bool value)
	{
		___pmaVertexColors_2 = value;
	}

	inline static int32_t get_offset_of_tintBlack_3() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___tintBlack_3)); }
	inline bool get_tintBlack_3() const { return ___tintBlack_3; }
	inline bool* get_address_of_tintBlack_3() { return &___tintBlack_3; }
	inline void set_tintBlack_3(bool value)
	{
		___tintBlack_3 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_4() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___calculateTangents_4)); }
	inline bool get_calculateTangents_4() const { return ___calculateTangents_4; }
	inline bool* get_address_of_calculateTangents_4() { return &___calculateTangents_4; }
	inline void set_calculateTangents_4(bool value)
	{
		___calculateTangents_4 = value;
	}

	inline static int32_t get_offset_of_addNormals_5() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___addNormals_5)); }
	inline bool get_addNormals_5() const { return ___addNormals_5; }
	inline bool* get_address_of_addNormals_5() { return &___addNormals_5; }
	inline void set_addNormals_5(bool value)
	{
		___addNormals_5 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_6() { return static_cast<int32_t>(offsetof(Settings_t612870457, ___immutableTriangles_6)); }
	inline bool get_immutableTriangles_6() const { return ___immutableTriangles_6; }
	inline bool* get_address_of_immutableTriangles_6() { return &___immutableTriangles_6; }
	inline void set_immutableTriangles_6(bool value)
	{
		___immutableTriangles_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t612870457_marshaled_pinvoke
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
// Native definition for COM marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t612870457_marshaled_com
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
#endif // SETTINGS_T612870457_H
#ifndef MESHGENERATORBUFFERS_T1424700926_H
#define MESHGENERATORBUFFERS_T1424700926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorBuffers
struct  MeshGeneratorBuffers_t1424700926 
{
public:
	// System.Int32 Spine.Unity.MeshGeneratorBuffers::vertexCount
	int32_t ___vertexCount_0;
	// UnityEngine.Vector3[] Spine.Unity.MeshGeneratorBuffers::vertexBuffer
	Vector3U5BU5D_t1718750761* ___vertexBuffer_1;
	// UnityEngine.Vector2[] Spine.Unity.MeshGeneratorBuffers::uvBuffer
	Vector2U5BU5D_t1457185986* ___uvBuffer_2;
	// UnityEngine.Color32[] Spine.Unity.MeshGeneratorBuffers::colorBuffer
	Color32U5BU5D_t3850468773* ___colorBuffer_3;
	// Spine.Unity.MeshGenerator Spine.Unity.MeshGeneratorBuffers::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_4;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_1() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___vertexBuffer_1)); }
	inline Vector3U5BU5D_t1718750761* get_vertexBuffer_1() const { return ___vertexBuffer_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertexBuffer_1() { return &___vertexBuffer_1; }
	inline void set_vertexBuffer_1(Vector3U5BU5D_t1718750761* value)
	{
		___vertexBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_1), value);
	}

	inline static int32_t get_offset_of_uvBuffer_2() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___uvBuffer_2)); }
	inline Vector2U5BU5D_t1457185986* get_uvBuffer_2() const { return ___uvBuffer_2; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvBuffer_2() { return &___uvBuffer_2; }
	inline void set_uvBuffer_2(Vector2U5BU5D_t1457185986* value)
	{
		___uvBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_2), value);
	}

	inline static int32_t get_offset_of_colorBuffer_3() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___colorBuffer_3)); }
	inline Color32U5BU5D_t3850468773* get_colorBuffer_3() const { return ___colorBuffer_3; }
	inline Color32U5BU5D_t3850468773** get_address_of_colorBuffer_3() { return &___colorBuffer_3; }
	inline void set_colorBuffer_3(Color32U5BU5D_t3850468773* value)
	{
		___colorBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_3), value);
	}

	inline static int32_t get_offset_of_meshGenerator_4() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1424700926, ___meshGenerator_4)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_4() const { return ___meshGenerator_4; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_4() { return &___meshGenerator_4; }
	inline void set_meshGenerator_4(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_t1424700926_marshaled_pinvoke
{
	int32_t ___vertexCount_0;
	Vector3_t3722313464 * ___vertexBuffer_1;
	Vector2_t2156229523 * ___uvBuffer_2;
	Color32_t2600501292 * ___colorBuffer_3;
	MeshGenerator_t1354683548 * ___meshGenerator_4;
};
// Native definition for COM marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_t1424700926_marshaled_com
{
	int32_t ___vertexCount_0;
	Vector3_t3722313464 * ___vertexBuffer_1;
	Vector2_t2156229523 * ___uvBuffer_2;
	Color32_t2600501292 * ___colorBuffer_3;
	MeshGenerator_t1354683548 * ___meshGenerator_4;
};
#endif // MESHGENERATORBUFFERS_T1424700926_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SUBMESHINSTRUCTION_T52121370_H
#define SUBMESHINSTRUCTION_T52121370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SubmeshInstruction
struct  SubmeshInstruction_t52121370 
{
public:
	// Spine.Skeleton Spine.Unity.SubmeshInstruction::skeleton
	Skeleton_t3686076450 * ___skeleton_0;
	// System.Int32 Spine.Unity.SubmeshInstruction::startSlot
	int32_t ___startSlot_1;
	// System.Int32 Spine.Unity.SubmeshInstruction::endSlot
	int32_t ___endSlot_2;
	// UnityEngine.Material Spine.Unity.SubmeshInstruction::material
	Material_t340375123 * ___material_3;
	// System.Boolean Spine.Unity.SubmeshInstruction::forceSeparate
	bool ___forceSeparate_4;
	// System.Int32 Spine.Unity.SubmeshInstruction::preActiveClippingSlotSource
	int32_t ___preActiveClippingSlotSource_5;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawTriangleCount
	int32_t ___rawTriangleCount_6;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawVertexCount
	int32_t ___rawVertexCount_7;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawFirstVertexIndex
	int32_t ___rawFirstVertexIndex_8;
	// System.Boolean Spine.Unity.SubmeshInstruction::hasClipping
	bool ___hasClipping_9;

public:
	inline static int32_t get_offset_of_skeleton_0() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___skeleton_0)); }
	inline Skeleton_t3686076450 * get_skeleton_0() const { return ___skeleton_0; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_0() { return &___skeleton_0; }
	inline void set_skeleton_0(Skeleton_t3686076450 * value)
	{
		___skeleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_0), value);
	}

	inline static int32_t get_offset_of_startSlot_1() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___startSlot_1)); }
	inline int32_t get_startSlot_1() const { return ___startSlot_1; }
	inline int32_t* get_address_of_startSlot_1() { return &___startSlot_1; }
	inline void set_startSlot_1(int32_t value)
	{
		___startSlot_1 = value;
	}

	inline static int32_t get_offset_of_endSlot_2() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___endSlot_2)); }
	inline int32_t get_endSlot_2() const { return ___endSlot_2; }
	inline int32_t* get_address_of_endSlot_2() { return &___endSlot_2; }
	inline void set_endSlot_2(int32_t value)
	{
		___endSlot_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_forceSeparate_4() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___forceSeparate_4)); }
	inline bool get_forceSeparate_4() const { return ___forceSeparate_4; }
	inline bool* get_address_of_forceSeparate_4() { return &___forceSeparate_4; }
	inline void set_forceSeparate_4(bool value)
	{
		___forceSeparate_4 = value;
	}

	inline static int32_t get_offset_of_preActiveClippingSlotSource_5() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___preActiveClippingSlotSource_5)); }
	inline int32_t get_preActiveClippingSlotSource_5() const { return ___preActiveClippingSlotSource_5; }
	inline int32_t* get_address_of_preActiveClippingSlotSource_5() { return &___preActiveClippingSlotSource_5; }
	inline void set_preActiveClippingSlotSource_5(int32_t value)
	{
		___preActiveClippingSlotSource_5 = value;
	}

	inline static int32_t get_offset_of_rawTriangleCount_6() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___rawTriangleCount_6)); }
	inline int32_t get_rawTriangleCount_6() const { return ___rawTriangleCount_6; }
	inline int32_t* get_address_of_rawTriangleCount_6() { return &___rawTriangleCount_6; }
	inline void set_rawTriangleCount_6(int32_t value)
	{
		___rawTriangleCount_6 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_7() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___rawVertexCount_7)); }
	inline int32_t get_rawVertexCount_7() const { return ___rawVertexCount_7; }
	inline int32_t* get_address_of_rawVertexCount_7() { return &___rawVertexCount_7; }
	inline void set_rawVertexCount_7(int32_t value)
	{
		___rawVertexCount_7 = value;
	}

	inline static int32_t get_offset_of_rawFirstVertexIndex_8() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___rawFirstVertexIndex_8)); }
	inline int32_t get_rawFirstVertexIndex_8() const { return ___rawFirstVertexIndex_8; }
	inline int32_t* get_address_of_rawFirstVertexIndex_8() { return &___rawFirstVertexIndex_8; }
	inline void set_rawFirstVertexIndex_8(int32_t value)
	{
		___rawFirstVertexIndex_8 = value;
	}

	inline static int32_t get_offset_of_hasClipping_9() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t52121370, ___hasClipping_9)); }
	inline bool get_hasClipping_9() const { return ___hasClipping_9; }
	inline bool* get_address_of_hasClipping_9() { return &___hasClipping_9; }
	inline void set_hasClipping_9(bool value)
	{
		___hasClipping_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t52121370_marshaled_pinvoke
{
	Skeleton_t3686076450 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t340375123 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
// Native definition for COM marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t52121370_marshaled_com
{
	Skeleton_t3686076450 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t340375123 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
#endif // SUBMESHINSTRUCTION_T52121370_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef SPINEATLASREGION_T3654419954_H
#define SPINEATLASREGION_T3654419954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAtlasRegion
struct  SpineAtlasRegion_t3654419954  : public PropertyAttribute_t3677895545
{
public:
	// System.String Spine.Unity.SpineAtlasRegion::atlasAssetField
	String_t* ___atlasAssetField_0;

public:
	inline static int32_t get_offset_of_atlasAssetField_0() { return static_cast<int32_t>(offsetof(SpineAtlasRegion_t3654419954, ___atlasAssetField_0)); }
	inline String_t* get_atlasAssetField_0() const { return ___atlasAssetField_0; }
	inline String_t** get_address_of_atlasAssetField_0() { return &___atlasAssetField_0; }
	inline void set_atlasAssetField_0(String_t* value)
	{
		___atlasAssetField_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssetField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATLASREGION_T3654419954_H
#ifndef PHYSICSSYSTEM_T1859354049_H
#define PHYSICSSYSTEM_T1859354049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/PhysicsSystem
struct  PhysicsSystem_t1859354049 
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonUtilityKinematicShadow/PhysicsSystem::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhysicsSystem_t1859354049, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSSYSTEM_T1859354049_H
#ifndef MODE_T3700196520_H
#define MODE_T3700196520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone/Mode
struct  Mode_t3700196520 
{
public:
	// System.Int32 Spine.Unity.SkeletonUtilityBone/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3700196520, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3700196520_H
#ifndef UPDATEPHASE_T1465898593_H
#define UPDATEPHASE_T1465898593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone/UpdatePhase
struct  UpdatePhase_t1465898593 
{
public:
	// System.Int32 Spine.Unity.SkeletonUtilityBone/UpdatePhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdatePhase_t1465898593, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEPHASE_T1465898593_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-B6E6EA57C32297E83203480EE50A22C3581AA09C
	U24ArrayTypeU3D20_t1702832645  ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-23447D2BC3FF6984AB09F575BC63CFE460337394
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2;

public:
	inline static int32_t get_offset_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0)); }
	inline U24ArrayTypeU3D20_t1702832645  get_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0() const { return ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0; }
	inline U24ArrayTypeU3D20_t1702832645 * get_address_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0() { return &___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0; }
	inline void set_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0(U24ArrayTypeU3D20_t1702832645  value)
	{
		___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() const { return ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return &___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline void set_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2() const { return ___U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2() { return &___U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2; }
	inline void set_U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef BLENDMODE_T358635744_H
#define BLENDMODE_T358635744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t358635744 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendMode_t358635744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T358635744_H
#ifndef SPINEATTRIBUTEBASE_T340149836_H
#define SPINEATTRIBUTEBASE_T340149836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttributeBase
struct  SpineAttributeBase_t340149836  : public PropertyAttribute_t3677895545
{
public:
	// System.String Spine.Unity.SpineAttributeBase::dataField
	String_t* ___dataField_0;
	// System.String Spine.Unity.SpineAttributeBase::startsWith
	String_t* ___startsWith_1;
	// System.Boolean Spine.Unity.SpineAttributeBase::includeNone
	bool ___includeNone_2;
	// System.Boolean Spine.Unity.SpineAttributeBase::fallbackToTextField
	bool ___fallbackToTextField_3;

public:
	inline static int32_t get_offset_of_dataField_0() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___dataField_0)); }
	inline String_t* get_dataField_0() const { return ___dataField_0; }
	inline String_t** get_address_of_dataField_0() { return &___dataField_0; }
	inline void set_dataField_0(String_t* value)
	{
		___dataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataField_0), value);
	}

	inline static int32_t get_offset_of_startsWith_1() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___startsWith_1)); }
	inline String_t* get_startsWith_1() const { return ___startsWith_1; }
	inline String_t** get_address_of_startsWith_1() { return &___startsWith_1; }
	inline void set_startsWith_1(String_t* value)
	{
		___startsWith_1 = value;
		Il2CppCodeGenWriteBarrier((&___startsWith_1), value);
	}

	inline static int32_t get_offset_of_includeNone_2() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___includeNone_2)); }
	inline bool get_includeNone_2() const { return ___includeNone_2; }
	inline bool* get_address_of_includeNone_2() { return &___includeNone_2; }
	inline void set_includeNone_2(bool value)
	{
		___includeNone_2 = value;
	}

	inline static int32_t get_offset_of_fallbackToTextField_3() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t340149836, ___fallbackToTextField_3)); }
	inline bool get_fallbackToTextField_3() const { return ___fallbackToTextField_3; }
	inline bool* get_address_of_fallbackToTextField_3() { return &___fallbackToTextField_3; }
	inline void set_fallbackToTextField_3(bool value)
	{
		___fallbackToTextField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTRIBUTEBASE_T340149836_H
#ifndef MESHGENERATOR_T1354683548_H
#define MESHGENERATOR_T1354683548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator
struct  MeshGenerator_t1354683548  : public RuntimeObject
{
public:
	// Spine.Unity.MeshGenerator/Settings Spine.Unity.MeshGenerator::settings
	Settings_t612870457  ___settings_0;
	// Spine.ExposedList`1<UnityEngine.Vector3> Spine.Unity.MeshGenerator::vertexBuffer
	ExposedList_1_t2134458034 * ___vertexBuffer_3;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uvBuffer
	ExposedList_1_t568374093 * ___uvBuffer_4;
	// Spine.ExposedList`1<UnityEngine.Color32> Spine.Unity.MeshGenerator::colorBuffer
	ExposedList_1_t1012645862 * ___colorBuffer_5;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Unity.MeshGenerator::submeshes
	ExposedList_1_t4070202189 * ___submeshes_6;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMin
	Vector2_t2156229523  ___meshBoundsMin_7;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMax
	Vector2_t2156229523  ___meshBoundsMax_8;
	// System.Single Spine.Unity.MeshGenerator::meshBoundsThickness
	float ___meshBoundsThickness_9;
	// System.Int32 Spine.Unity.MeshGenerator::submeshIndex
	int32_t ___submeshIndex_10;
	// Spine.SkeletonClipping Spine.Unity.MeshGenerator::clipper
	SkeletonClipping_t1669006083 * ___clipper_11;
	// System.Single[] Spine.Unity.MeshGenerator::tempVerts
	SingleU5BU5D_t1444911251* ___tempVerts_12;
	// System.Int32[] Spine.Unity.MeshGenerator::regionTriangles
	Int32U5BU5D_t385246372* ___regionTriangles_13;
	// UnityEngine.Vector3[] Spine.Unity.MeshGenerator::normals
	Vector3U5BU5D_t1718750761* ___normals_14;
	// UnityEngine.Vector4[] Spine.Unity.MeshGenerator::tangents
	Vector4U5BU5D_t934056436* ___tangents_15;
	// UnityEngine.Vector2[] Spine.Unity.MeshGenerator::tempTanBuffer
	Vector2U5BU5D_t1457185986* ___tempTanBuffer_16;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv2
	ExposedList_1_t568374093 * ___uv2_17;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv3
	ExposedList_1_t568374093 * ___uv3_18;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___settings_0)); }
	inline Settings_t612870457  get_settings_0() const { return ___settings_0; }
	inline Settings_t612870457 * get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(Settings_t612870457  value)
	{
		___settings_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_3() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___vertexBuffer_3)); }
	inline ExposedList_1_t2134458034 * get_vertexBuffer_3() const { return ___vertexBuffer_3; }
	inline ExposedList_1_t2134458034 ** get_address_of_vertexBuffer_3() { return &___vertexBuffer_3; }
	inline void set_vertexBuffer_3(ExposedList_1_t2134458034 * value)
	{
		___vertexBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_3), value);
	}

	inline static int32_t get_offset_of_uvBuffer_4() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___uvBuffer_4)); }
	inline ExposedList_1_t568374093 * get_uvBuffer_4() const { return ___uvBuffer_4; }
	inline ExposedList_1_t568374093 ** get_address_of_uvBuffer_4() { return &___uvBuffer_4; }
	inline void set_uvBuffer_4(ExposedList_1_t568374093 * value)
	{
		___uvBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_4), value);
	}

	inline static int32_t get_offset_of_colorBuffer_5() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___colorBuffer_5)); }
	inline ExposedList_1_t1012645862 * get_colorBuffer_5() const { return ___colorBuffer_5; }
	inline ExposedList_1_t1012645862 ** get_address_of_colorBuffer_5() { return &___colorBuffer_5; }
	inline void set_colorBuffer_5(ExposedList_1_t1012645862 * value)
	{
		___colorBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_5), value);
	}

	inline static int32_t get_offset_of_submeshes_6() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___submeshes_6)); }
	inline ExposedList_1_t4070202189 * get_submeshes_6() const { return ___submeshes_6; }
	inline ExposedList_1_t4070202189 ** get_address_of_submeshes_6() { return &___submeshes_6; }
	inline void set_submeshes_6(ExposedList_1_t4070202189 * value)
	{
		___submeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___submeshes_6), value);
	}

	inline static int32_t get_offset_of_meshBoundsMin_7() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___meshBoundsMin_7)); }
	inline Vector2_t2156229523  get_meshBoundsMin_7() const { return ___meshBoundsMin_7; }
	inline Vector2_t2156229523 * get_address_of_meshBoundsMin_7() { return &___meshBoundsMin_7; }
	inline void set_meshBoundsMin_7(Vector2_t2156229523  value)
	{
		___meshBoundsMin_7 = value;
	}

	inline static int32_t get_offset_of_meshBoundsMax_8() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___meshBoundsMax_8)); }
	inline Vector2_t2156229523  get_meshBoundsMax_8() const { return ___meshBoundsMax_8; }
	inline Vector2_t2156229523 * get_address_of_meshBoundsMax_8() { return &___meshBoundsMax_8; }
	inline void set_meshBoundsMax_8(Vector2_t2156229523  value)
	{
		___meshBoundsMax_8 = value;
	}

	inline static int32_t get_offset_of_meshBoundsThickness_9() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___meshBoundsThickness_9)); }
	inline float get_meshBoundsThickness_9() const { return ___meshBoundsThickness_9; }
	inline float* get_address_of_meshBoundsThickness_9() { return &___meshBoundsThickness_9; }
	inline void set_meshBoundsThickness_9(float value)
	{
		___meshBoundsThickness_9 = value;
	}

	inline static int32_t get_offset_of_submeshIndex_10() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___submeshIndex_10)); }
	inline int32_t get_submeshIndex_10() const { return ___submeshIndex_10; }
	inline int32_t* get_address_of_submeshIndex_10() { return &___submeshIndex_10; }
	inline void set_submeshIndex_10(int32_t value)
	{
		___submeshIndex_10 = value;
	}

	inline static int32_t get_offset_of_clipper_11() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___clipper_11)); }
	inline SkeletonClipping_t1669006083 * get_clipper_11() const { return ___clipper_11; }
	inline SkeletonClipping_t1669006083 ** get_address_of_clipper_11() { return &___clipper_11; }
	inline void set_clipper_11(SkeletonClipping_t1669006083 * value)
	{
		___clipper_11 = value;
		Il2CppCodeGenWriteBarrier((&___clipper_11), value);
	}

	inline static int32_t get_offset_of_tempVerts_12() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___tempVerts_12)); }
	inline SingleU5BU5D_t1444911251* get_tempVerts_12() const { return ___tempVerts_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_tempVerts_12() { return &___tempVerts_12; }
	inline void set_tempVerts_12(SingleU5BU5D_t1444911251* value)
	{
		___tempVerts_12 = value;
		Il2CppCodeGenWriteBarrier((&___tempVerts_12), value);
	}

	inline static int32_t get_offset_of_regionTriangles_13() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___regionTriangles_13)); }
	inline Int32U5BU5D_t385246372* get_regionTriangles_13() const { return ___regionTriangles_13; }
	inline Int32U5BU5D_t385246372** get_address_of_regionTriangles_13() { return &___regionTriangles_13; }
	inline void set_regionTriangles_13(Int32U5BU5D_t385246372* value)
	{
		___regionTriangles_13 = value;
		Il2CppCodeGenWriteBarrier((&___regionTriangles_13), value);
	}

	inline static int32_t get_offset_of_normals_14() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___normals_14)); }
	inline Vector3U5BU5D_t1718750761* get_normals_14() const { return ___normals_14; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_14() { return &___normals_14; }
	inline void set_normals_14(Vector3U5BU5D_t1718750761* value)
	{
		___normals_14 = value;
		Il2CppCodeGenWriteBarrier((&___normals_14), value);
	}

	inline static int32_t get_offset_of_tangents_15() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___tangents_15)); }
	inline Vector4U5BU5D_t934056436* get_tangents_15() const { return ___tangents_15; }
	inline Vector4U5BU5D_t934056436** get_address_of_tangents_15() { return &___tangents_15; }
	inline void set_tangents_15(Vector4U5BU5D_t934056436* value)
	{
		___tangents_15 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_15), value);
	}

	inline static int32_t get_offset_of_tempTanBuffer_16() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___tempTanBuffer_16)); }
	inline Vector2U5BU5D_t1457185986* get_tempTanBuffer_16() const { return ___tempTanBuffer_16; }
	inline Vector2U5BU5D_t1457185986** get_address_of_tempTanBuffer_16() { return &___tempTanBuffer_16; }
	inline void set_tempTanBuffer_16(Vector2U5BU5D_t1457185986* value)
	{
		___tempTanBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&___tempTanBuffer_16), value);
	}

	inline static int32_t get_offset_of_uv2_17() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___uv2_17)); }
	inline ExposedList_1_t568374093 * get_uv2_17() const { return ___uv2_17; }
	inline ExposedList_1_t568374093 ** get_address_of_uv2_17() { return &___uv2_17; }
	inline void set_uv2_17(ExposedList_1_t568374093 * value)
	{
		___uv2_17 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_17), value);
	}

	inline static int32_t get_offset_of_uv3_18() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548, ___uv3_18)); }
	inline ExposedList_1_t568374093 * get_uv3_18() const { return ___uv3_18; }
	inline ExposedList_1_t568374093 ** get_address_of_uv3_18() { return &___uv3_18; }
	inline void set_uv3_18(ExposedList_1_t568374093 * value)
	{
		___uv3_18 = value;
		Il2CppCodeGenWriteBarrier((&___uv3_18), value);
	}
};

struct MeshGenerator_t1354683548_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Spine.Unity.MeshGenerator::AttachmentVerts
	List_1_t899420910 * ___AttachmentVerts_19;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::AttachmentUVs
	List_1_t3628304265 * ___AttachmentUVs_20;
	// System.Collections.Generic.List`1<UnityEngine.Color32> Spine.Unity.MeshGenerator::AttachmentColors32
	List_1_t4072576034 * ___AttachmentColors32_21;
	// System.Collections.Generic.List`1<System.Int32> Spine.Unity.MeshGenerator::AttachmentIndices
	List_1_t128053199 * ___AttachmentIndices_22;

public:
	inline static int32_t get_offset_of_AttachmentVerts_19() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548_StaticFields, ___AttachmentVerts_19)); }
	inline List_1_t899420910 * get_AttachmentVerts_19() const { return ___AttachmentVerts_19; }
	inline List_1_t899420910 ** get_address_of_AttachmentVerts_19() { return &___AttachmentVerts_19; }
	inline void set_AttachmentVerts_19(List_1_t899420910 * value)
	{
		___AttachmentVerts_19 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentVerts_19), value);
	}

	inline static int32_t get_offset_of_AttachmentUVs_20() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548_StaticFields, ___AttachmentUVs_20)); }
	inline List_1_t3628304265 * get_AttachmentUVs_20() const { return ___AttachmentUVs_20; }
	inline List_1_t3628304265 ** get_address_of_AttachmentUVs_20() { return &___AttachmentUVs_20; }
	inline void set_AttachmentUVs_20(List_1_t3628304265 * value)
	{
		___AttachmentUVs_20 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentUVs_20), value);
	}

	inline static int32_t get_offset_of_AttachmentColors32_21() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548_StaticFields, ___AttachmentColors32_21)); }
	inline List_1_t4072576034 * get_AttachmentColors32_21() const { return ___AttachmentColors32_21; }
	inline List_1_t4072576034 ** get_address_of_AttachmentColors32_21() { return &___AttachmentColors32_21; }
	inline void set_AttachmentColors32_21(List_1_t4072576034 * value)
	{
		___AttachmentColors32_21 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentColors32_21), value);
	}

	inline static int32_t get_offset_of_AttachmentIndices_22() { return static_cast<int32_t>(offsetof(MeshGenerator_t1354683548_StaticFields, ___AttachmentIndices_22)); }
	inline List_1_t128053199 * get_AttachmentIndices_22() const { return ___AttachmentIndices_22; }
	inline List_1_t128053199 ** get_address_of_AttachmentIndices_22() { return &___AttachmentIndices_22; }
	inline void set_AttachmentIndices_22(List_1_t128053199 * value)
	{
		___AttachmentIndices_22 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentIndices_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATOR_T1354683548_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef MIXMODE_T3058145032_H
#define MIXMODE_T3058145032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode
struct  MixMode_t3058145032 
{
public:
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixMode_t3058145032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXMODE_T3058145032_H
#ifndef U3CFADEU3EC__ITERATOR0_T1290633589_H
#define U3CFADEU3EC__ITERATOR0_T1290633589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0
struct  U3CFadeU3Ec__Iterator0_t1290633589  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<t>__1
	int32_t ___U3CtU3E__1_0;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<breakout>__2
	bool ___U3CbreakoutU3E__2_1;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<c>__3
	Color32_t2600501292  ___U3CcU3E__3_2;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$this
	SkeletonGhostRenderer_t2445315009 * ___U24this_3;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U3CtU3E__1_0)); }
	inline int32_t get_U3CtU3E__1_0() const { return ___U3CtU3E__1_0; }
	inline int32_t* get_address_of_U3CtU3E__1_0() { return &___U3CtU3E__1_0; }
	inline void set_U3CtU3E__1_0(int32_t value)
	{
		___U3CtU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CbreakoutU3E__2_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U3CbreakoutU3E__2_1)); }
	inline bool get_U3CbreakoutU3E__2_1() const { return ___U3CbreakoutU3E__2_1; }
	inline bool* get_address_of_U3CbreakoutU3E__2_1() { return &___U3CbreakoutU3E__2_1; }
	inline void set_U3CbreakoutU3E__2_1(bool value)
	{
		___U3CbreakoutU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U3CcU3E__3_2)); }
	inline Color32_t2600501292  get_U3CcU3E__3_2() const { return ___U3CcU3E__3_2; }
	inline Color32_t2600501292 * get_address_of_U3CcU3E__3_2() { return &___U3CcU3E__3_2; }
	inline void set_U3CcU3E__3_2(Color32_t2600501292  value)
	{
		___U3CcU3E__3_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24this_3)); }
	inline SkeletonGhostRenderer_t2445315009 * get_U24this_3() const { return ___U24this_3; }
	inline SkeletonGhostRenderer_t2445315009 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SkeletonGhostRenderer_t2445315009 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1290633589, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3EC__ITERATOR0_T1290633589_H
#ifndef LAYERFIELDATTRIBUTE_T3408802261_H
#define LAYERFIELDATTRIBUTE_T3408802261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/LayerFieldAttribute
struct  LayerFieldAttribute_t3408802261  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERFIELDATTRIBUTE_T3408802261_H
#ifndef U3CFADEADDITIVEU3EC__ITERATOR1_T410398119_H
#define U3CFADEADDITIVEU3EC__ITERATOR1_T410398119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1
struct  U3CFadeAdditiveU3Ec__Iterator1_t410398119  : public RuntimeObject
{
public:
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<black>__0
	Color32_t2600501292  ___U3CblackU3E__0_0;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<t>__1
	int32_t ___U3CtU3E__1_1;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<breakout>__2
	bool ___U3CbreakoutU3E__2_2;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<c>__3
	Color32_t2600501292  ___U3CcU3E__3_3;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$this
	SkeletonGhostRenderer_t2445315009 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CblackU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CblackU3E__0_0)); }
	inline Color32_t2600501292  get_U3CblackU3E__0_0() const { return ___U3CblackU3E__0_0; }
	inline Color32_t2600501292 * get_address_of_U3CblackU3E__0_0() { return &___U3CblackU3E__0_0; }
	inline void set_U3CblackU3E__0_0(Color32_t2600501292  value)
	{
		___U3CblackU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_1() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CtU3E__1_1)); }
	inline int32_t get_U3CtU3E__1_1() const { return ___U3CtU3E__1_1; }
	inline int32_t* get_address_of_U3CtU3E__1_1() { return &___U3CtU3E__1_1; }
	inline void set_U3CtU3E__1_1(int32_t value)
	{
		___U3CtU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbreakoutU3E__2_2() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CbreakoutU3E__2_2)); }
	inline bool get_U3CbreakoutU3E__2_2() const { return ___U3CbreakoutU3E__2_2; }
	inline bool* get_address_of_U3CbreakoutU3E__2_2() { return &___U3CbreakoutU3E__2_2; }
	inline void set_U3CbreakoutU3E__2_2(bool value)
	{
		___U3CbreakoutU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_3() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U3CcU3E__3_3)); }
	inline Color32_t2600501292  get_U3CcU3E__3_3() const { return ___U3CcU3E__3_3; }
	inline Color32_t2600501292 * get_address_of_U3CcU3E__3_3() { return &___U3CcU3E__3_3; }
	inline void set_U3CcU3E__3_3(Color32_t2600501292  value)
	{
		___U3CcU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24this_4)); }
	inline SkeletonGhostRenderer_t2445315009 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonGhostRenderer_t2445315009 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonGhostRenderer_t2445315009 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t410398119, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEADDITIVEU3EC__ITERATOR1_T410398119_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SPINEIKCONSTRAINT_T1587650654_H
#define SPINEIKCONSTRAINT_T1587650654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineIkConstraint
struct  SpineIkConstraint_t1587650654  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEIKCONSTRAINT_T1587650654_H
#ifndef SPINESLOT_T2906857509_H
#define SPINESLOT_T2906857509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSlot
struct  SpineSlot_t2906857509  : public SpineAttributeBase_t340149836
{
public:
	// System.Boolean Spine.Unity.SpineSlot::containsBoundingBoxes
	bool ___containsBoundingBoxes_4;

public:
	inline static int32_t get_offset_of_containsBoundingBoxes_4() { return static_cast<int32_t>(offsetof(SpineSlot_t2906857509, ___containsBoundingBoxes_4)); }
	inline bool get_containsBoundingBoxes_4() const { return ___containsBoundingBoxes_4; }
	inline bool* get_address_of_containsBoundingBoxes_4() { return &___containsBoundingBoxes_4; }
	inline void set_containsBoundingBoxes_4(bool value)
	{
		___containsBoundingBoxes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESLOT_T2906857509_H
#ifndef SPINEEVENT_T2537401913_H
#define SPINEEVENT_T2537401913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineEvent
struct  SpineEvent_t2537401913  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENT_T2537401913_H
#ifndef SPINEPATHCONSTRAINT_T3765886426_H
#define SPINEPATHCONSTRAINT_T3765886426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpinePathConstraint
struct  SpinePathConstraint_t3765886426  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEPATHCONSTRAINT_T3765886426_H
#ifndef SPINEBONE_T3329529041_H
#define SPINEBONE_T3329529041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineBone
struct  SpineBone_t3329529041  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEBONE_T3329529041_H
#ifndef SPINEATTACHMENT_T743638603_H
#define SPINEATTACHMENT_T743638603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment
struct  SpineAttachment_t743638603  : public SpineAttributeBase_t340149836
{
public:
	// System.Boolean Spine.Unity.SpineAttachment::returnAttachmentPath
	bool ___returnAttachmentPath_4;
	// System.Boolean Spine.Unity.SpineAttachment::currentSkinOnly
	bool ___currentSkinOnly_5;
	// System.Boolean Spine.Unity.SpineAttachment::placeholdersOnly
	bool ___placeholdersOnly_6;
	// System.String Spine.Unity.SpineAttachment::skinField
	String_t* ___skinField_7;
	// System.String Spine.Unity.SpineAttachment::slotField
	String_t* ___slotField_8;

public:
	inline static int32_t get_offset_of_returnAttachmentPath_4() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___returnAttachmentPath_4)); }
	inline bool get_returnAttachmentPath_4() const { return ___returnAttachmentPath_4; }
	inline bool* get_address_of_returnAttachmentPath_4() { return &___returnAttachmentPath_4; }
	inline void set_returnAttachmentPath_4(bool value)
	{
		___returnAttachmentPath_4 = value;
	}

	inline static int32_t get_offset_of_currentSkinOnly_5() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___currentSkinOnly_5)); }
	inline bool get_currentSkinOnly_5() const { return ___currentSkinOnly_5; }
	inline bool* get_address_of_currentSkinOnly_5() { return &___currentSkinOnly_5; }
	inline void set_currentSkinOnly_5(bool value)
	{
		___currentSkinOnly_5 = value;
	}

	inline static int32_t get_offset_of_placeholdersOnly_6() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___placeholdersOnly_6)); }
	inline bool get_placeholdersOnly_6() const { return ___placeholdersOnly_6; }
	inline bool* get_address_of_placeholdersOnly_6() { return &___placeholdersOnly_6; }
	inline void set_placeholdersOnly_6(bool value)
	{
		___placeholdersOnly_6 = value;
	}

	inline static int32_t get_offset_of_skinField_7() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___skinField_7)); }
	inline String_t* get_skinField_7() const { return ___skinField_7; }
	inline String_t** get_address_of_skinField_7() { return &___skinField_7; }
	inline void set_skinField_7(String_t* value)
	{
		___skinField_7 = value;
		Il2CppCodeGenWriteBarrier((&___skinField_7), value);
	}

	inline static int32_t get_offset_of_slotField_8() { return static_cast<int32_t>(offsetof(SpineAttachment_t743638603, ___slotField_8)); }
	inline String_t* get_slotField_8() const { return ___slotField_8; }
	inline String_t** get_address_of_slotField_8() { return &___slotField_8; }
	inline void set_slotField_8(String_t* value)
	{
		___slotField_8 = value;
		Il2CppCodeGenWriteBarrier((&___slotField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTACHMENT_T743638603_H
#ifndef SPINEANIMATION_T42895223_H
#define SPINEANIMATION_T42895223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAnimation
struct  SpineAnimation_t42895223  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATION_T42895223_H
#ifndef SPINESKIN_T4048709426_H
#define SPINESKIN_T4048709426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSkin
struct  SpineSkin_t4048709426  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKIN_T4048709426_H
#ifndef SPINETRANSFORMCONSTRAINT_T1324873370_H
#define SPINETRANSFORMCONSTRAINT_T1324873370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineTransformConstraint
struct  SpineTransformConstraint_t1324873370  : public SpineAttributeBase_t340149836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINETRANSFORMCONSTRAINT_T1324873370_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef SLOTDATA_T154801902_H
#define SLOTDATA_T154801902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SlotData
struct  SlotData_t154801902  : public RuntimeObject
{
public:
	// System.Int32 Spine.SlotData::index
	int32_t ___index_0;
	// System.String Spine.SlotData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.SlotData::boneData
	BoneData_t3130174490 * ___boneData_2;
	// System.Single Spine.SlotData::r
	float ___r_3;
	// System.Single Spine.SlotData::g
	float ___g_4;
	// System.Single Spine.SlotData::b
	float ___b_5;
	// System.Single Spine.SlotData::a
	float ___a_6;
	// System.Single Spine.SlotData::r2
	float ___r2_7;
	// System.Single Spine.SlotData::g2
	float ___g2_8;
	// System.Single Spine.SlotData::b2
	float ___b2_9;
	// System.Boolean Spine.SlotData::hasSecondColor
	bool ___hasSecondColor_10;
	// System.String Spine.SlotData::attachmentName
	String_t* ___attachmentName_11;
	// Spine.BlendMode Spine.SlotData::blendMode
	int32_t ___blendMode_12;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_boneData_2() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___boneData_2)); }
	inline BoneData_t3130174490 * get_boneData_2() const { return ___boneData_2; }
	inline BoneData_t3130174490 ** get_address_of_boneData_2() { return &___boneData_2; }
	inline void set_boneData_2(BoneData_t3130174490 * value)
	{
		___boneData_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneData_2), value);
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___r_3)); }
	inline float get_r_3() const { return ___r_3; }
	inline float* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(float value)
	{
		___r_3 = value;
	}

	inline static int32_t get_offset_of_g_4() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___g_4)); }
	inline float get_g_4() const { return ___g_4; }
	inline float* get_address_of_g_4() { return &___g_4; }
	inline void set_g_4(float value)
	{
		___g_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___b_5)); }
	inline float get_b_5() const { return ___b_5; }
	inline float* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(float value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___a_6)); }
	inline float get_a_6() const { return ___a_6; }
	inline float* get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(float value)
	{
		___a_6 = value;
	}

	inline static int32_t get_offset_of_r2_7() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___r2_7)); }
	inline float get_r2_7() const { return ___r2_7; }
	inline float* get_address_of_r2_7() { return &___r2_7; }
	inline void set_r2_7(float value)
	{
		___r2_7 = value;
	}

	inline static int32_t get_offset_of_g2_8() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___g2_8)); }
	inline float get_g2_8() const { return ___g2_8; }
	inline float* get_address_of_g2_8() { return &___g2_8; }
	inline void set_g2_8(float value)
	{
		___g2_8 = value;
	}

	inline static int32_t get_offset_of_b2_9() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___b2_9)); }
	inline float get_b2_9() const { return ___b2_9; }
	inline float* get_address_of_b2_9() { return &___b2_9; }
	inline void set_b2_9(float value)
	{
		___b2_9 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_10() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___hasSecondColor_10)); }
	inline bool get_hasSecondColor_10() const { return ___hasSecondColor_10; }
	inline bool* get_address_of_hasSecondColor_10() { return &___hasSecondColor_10; }
	inline void set_hasSecondColor_10(bool value)
	{
		___hasSecondColor_10 = value;
	}

	inline static int32_t get_offset_of_attachmentName_11() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___attachmentName_11)); }
	inline String_t* get_attachmentName_11() const { return ___attachmentName_11; }
	inline String_t** get_address_of_attachmentName_11() { return &___attachmentName_11; }
	inline void set_attachmentName_11(String_t* value)
	{
		___attachmentName_11 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentName_11), value);
	}

	inline static int32_t get_offset_of_blendMode_12() { return static_cast<int32_t>(offsetof(SlotData_t154801902, ___blendMode_12)); }
	inline int32_t get_blendMode_12() const { return ___blendMode_12; }
	inline int32_t* get_address_of_blendMode_12() { return &___blendMode_12; }
	inline void set_blendMode_12(int32_t value)
	{
		___blendMode_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTDATA_T154801902_H
#ifndef ATLASUTILITIES_T1517471311_H
#define ATLASUTILITIES_T1517471311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AtlasUtilities
struct  AtlasUtilities_t1517471311  : public RuntimeObject
{
public:

public:
};

struct AtlasUtilities_t1517471311_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTextures
	Dictionary_2_t47542197 * ___CachedRegionTextures_4;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTexturesList
	List_1_t1017553631 * ___CachedRegionTexturesList_5;

public:
	inline static int32_t get_offset_of_CachedRegionTextures_4() { return static_cast<int32_t>(offsetof(AtlasUtilities_t1517471311_StaticFields, ___CachedRegionTextures_4)); }
	inline Dictionary_2_t47542197 * get_CachedRegionTextures_4() const { return ___CachedRegionTextures_4; }
	inline Dictionary_2_t47542197 ** get_address_of_CachedRegionTextures_4() { return &___CachedRegionTextures_4; }
	inline void set_CachedRegionTextures_4(Dictionary_2_t47542197 * value)
	{
		___CachedRegionTextures_4 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTextures_4), value);
	}

	inline static int32_t get_offset_of_CachedRegionTexturesList_5() { return static_cast<int32_t>(offsetof(AtlasUtilities_t1517471311_StaticFields, ___CachedRegionTexturesList_5)); }
	inline List_1_t1017553631 * get_CachedRegionTexturesList_5() const { return ___CachedRegionTexturesList_5; }
	inline List_1_t1017553631 ** get_address_of_CachedRegionTexturesList_5() { return &___CachedRegionTexturesList_5; }
	inline void set_CachedRegionTexturesList_5(List_1_t1017553631 * value)
	{
		___CachedRegionTexturesList_5 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTexturesList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASUTILITIES_T1517471311_H
#ifndef SPINEMESH_T2128742078_H
#define SPINEMESH_T2128742078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineMesh
struct  SpineMesh_t2128742078  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEMESH_T2128742078_H
#ifndef MESHGENERATORDELEGATE_T1654156803_H
#define MESHGENERATORDELEGATE_T1654156803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorDelegate
struct  MeshGeneratorDelegate_t1654156803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATORDELEGATE_T1654156803_H
#ifndef SKELETONRENDERERDELEGATE_T3507789975_H
#define SKELETONRENDERERDELEGATE_T3507789975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct  SkeletonRendererDelegate_t3507789975  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERDELEGATE_T3507789975_H
#ifndef INSTRUCTIONDELEGATE_T2225421195_H
#define INSTRUCTIONDELEGATE_T2225421195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct  InstructionDelegate_t2225421195  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTIONDELEGATE_T2225421195_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ANIMATIONREFERENCEASSET_T3989077076_H
#define ANIMATIONREFERENCEASSET_T3989077076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AnimationReferenceAsset
struct  AnimationReferenceAsset_t3989077076  : public ScriptableObject_t2528358522
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.AnimationReferenceAsset::skeletonDataAsset
	SkeletonDataAsset_t3748144825 * ___skeletonDataAsset_3;
	// System.String Spine.Unity.AnimationReferenceAsset::animationName
	String_t* ___animationName_4;
	// Spine.Animation Spine.Unity.AnimationReferenceAsset::animation
	Animation_t615783283 * ___animation_5;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_3() { return static_cast<int32_t>(offsetof(AnimationReferenceAsset_t3989077076, ___skeletonDataAsset_3)); }
	inline SkeletonDataAsset_t3748144825 * get_skeletonDataAsset_3() const { return ___skeletonDataAsset_3; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_skeletonDataAsset_3() { return &___skeletonDataAsset_3; }
	inline void set_skeletonDataAsset_3(SkeletonDataAsset_t3748144825 * value)
	{
		___skeletonDataAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_3), value);
	}

	inline static int32_t get_offset_of_animationName_4() { return static_cast<int32_t>(offsetof(AnimationReferenceAsset_t3989077076, ___animationName_4)); }
	inline String_t* get_animationName_4() const { return ___animationName_4; }
	inline String_t** get_address_of_animationName_4() { return &___animationName_4; }
	inline void set_animationName_4(String_t* value)
	{
		___animationName_4 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_4), value);
	}

	inline static int32_t get_offset_of_animation_5() { return static_cast<int32_t>(offsetof(AnimationReferenceAsset_t3989077076, ___animation_5)); }
	inline Animation_t615783283 * get_animation_5() const { return ___animation_5; }
	inline Animation_t615783283 ** get_address_of_animation_5() { return &___animation_5; }
	inline void set_animation_5(Animation_t615783283 * value)
	{
		___animation_5 = value;
		Il2CppCodeGenWriteBarrier((&___animation_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONREFERENCEASSET_T3989077076_H
#ifndef SKELETONUTILITYDELEGATE_T487527757_H
#define SKELETONUTILITYDELEGATE_T487527757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct  SkeletonUtilityDelegate_t487527757  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYDELEGATE_T487527757_H
#ifndef ATLASASSET_T1167231206_H
#define ATLASASSET_T1167231206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AtlasAsset
struct  AtlasAsset_t1167231206  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.TextAsset Spine.Unity.AtlasAsset::atlasFile
	TextAsset_t3022178571 * ___atlasFile_2;
	// UnityEngine.Material[] Spine.Unity.AtlasAsset::materials
	MaterialU5BU5D_t561872642* ___materials_3;
	// Spine.Atlas Spine.Unity.AtlasAsset::atlas
	Atlas_t4040192941 * ___atlas_4;

public:
	inline static int32_t get_offset_of_atlasFile_2() { return static_cast<int32_t>(offsetof(AtlasAsset_t1167231206, ___atlasFile_2)); }
	inline TextAsset_t3022178571 * get_atlasFile_2() const { return ___atlasFile_2; }
	inline TextAsset_t3022178571 ** get_address_of_atlasFile_2() { return &___atlasFile_2; }
	inline void set_atlasFile_2(TextAsset_t3022178571 * value)
	{
		___atlasFile_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasFile_2), value);
	}

	inline static int32_t get_offset_of_materials_3() { return static_cast<int32_t>(offsetof(AtlasAsset_t1167231206, ___materials_3)); }
	inline MaterialU5BU5D_t561872642* get_materials_3() const { return ___materials_3; }
	inline MaterialU5BU5D_t561872642** get_address_of_materials_3() { return &___materials_3; }
	inline void set_materials_3(MaterialU5BU5D_t561872642* value)
	{
		___materials_3 = value;
		Il2CppCodeGenWriteBarrier((&___materials_3), value);
	}

	inline static int32_t get_offset_of_atlas_4() { return static_cast<int32_t>(offsetof(AtlasAsset_t1167231206, ___atlas_4)); }
	inline Atlas_t4040192941 * get_atlas_4() const { return ___atlas_4; }
	inline Atlas_t4040192941 ** get_address_of_atlas_4() { return &___atlas_4; }
	inline void set_atlas_4(Atlas_t4040192941 * value)
	{
		___atlas_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASASSET_T1167231206_H
#ifndef EVENTDATAREFERENCEASSET_T4034732881_H
#define EVENTDATAREFERENCEASSET_T4034732881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.EventDataReferenceAsset
struct  EventDataReferenceAsset_t4034732881  : public ScriptableObject_t2528358522
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.EventDataReferenceAsset::skeletonDataAsset
	SkeletonDataAsset_t3748144825 * ___skeletonDataAsset_3;
	// System.String Spine.Unity.EventDataReferenceAsset::eventName
	String_t* ___eventName_4;
	// Spine.EventData Spine.Unity.EventDataReferenceAsset::eventData
	EventData_t724759987 * ___eventData_5;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_3() { return static_cast<int32_t>(offsetof(EventDataReferenceAsset_t4034732881, ___skeletonDataAsset_3)); }
	inline SkeletonDataAsset_t3748144825 * get_skeletonDataAsset_3() const { return ___skeletonDataAsset_3; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_skeletonDataAsset_3() { return &___skeletonDataAsset_3; }
	inline void set_skeletonDataAsset_3(SkeletonDataAsset_t3748144825 * value)
	{
		___skeletonDataAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_3), value);
	}

	inline static int32_t get_offset_of_eventName_4() { return static_cast<int32_t>(offsetof(EventDataReferenceAsset_t4034732881, ___eventName_4)); }
	inline String_t* get_eventName_4() const { return ___eventName_4; }
	inline String_t** get_address_of_eventName_4() { return &___eventName_4; }
	inline void set_eventName_4(String_t* value)
	{
		___eventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventName_4), value);
	}

	inline static int32_t get_offset_of_eventData_5() { return static_cast<int32_t>(offsetof(EventDataReferenceAsset_t4034732881, ___eventData_5)); }
	inline EventData_t724759987 * get_eventData_5() const { return ___eventData_5; }
	inline EventData_t724759987 ** get_address_of_eventData_5() { return &___eventData_5; }
	inline void set_eventData_5(EventData_t724759987 * value)
	{
		___eventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATAREFERENCEASSET_T4034732881_H
#ifndef SKELETONDATAASSET_T3748144825_H
#define SKELETONDATAASSET_T3748144825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonDataAsset
struct  SkeletonDataAsset_t3748144825  : public ScriptableObject_t2528358522
{
public:
	// Spine.Unity.AtlasAsset[] Spine.Unity.SkeletonDataAsset::atlasAssets
	AtlasAssetU5BU5D_t395880643* ___atlasAssets_2;
	// System.Single Spine.Unity.SkeletonDataAsset::scale
	float ___scale_3;
	// UnityEngine.TextAsset Spine.Unity.SkeletonDataAsset::skeletonJSON
	TextAsset_t3022178571 * ___skeletonJSON_4;
	// System.String[] Spine.Unity.SkeletonDataAsset::fromAnimation
	StringU5BU5D_t1281789340* ___fromAnimation_5;
	// System.String[] Spine.Unity.SkeletonDataAsset::toAnimation
	StringU5BU5D_t1281789340* ___toAnimation_6;
	// System.Single[] Spine.Unity.SkeletonDataAsset::duration
	SingleU5BU5D_t1444911251* ___duration_7;
	// System.Single Spine.Unity.SkeletonDataAsset::defaultMix
	float ___defaultMix_8;
	// UnityEngine.RuntimeAnimatorController Spine.Unity.SkeletonDataAsset::controller
	RuntimeAnimatorController_t2933699135 * ___controller_9;
	// Spine.SkeletonData Spine.Unity.SkeletonDataAsset::skeletonData
	SkeletonData_t2032710716 * ___skeletonData_10;
	// Spine.AnimationStateData Spine.Unity.SkeletonDataAsset::stateData
	AnimationStateData_t3010651567 * ___stateData_11;

public:
	inline static int32_t get_offset_of_atlasAssets_2() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___atlasAssets_2)); }
	inline AtlasAssetU5BU5D_t395880643* get_atlasAssets_2() const { return ___atlasAssets_2; }
	inline AtlasAssetU5BU5D_t395880643** get_address_of_atlasAssets_2() { return &___atlasAssets_2; }
	inline void set_atlasAssets_2(AtlasAssetU5BU5D_t395880643* value)
	{
		___atlasAssets_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssets_2), value);
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___scale_3)); }
	inline float get_scale_3() const { return ___scale_3; }
	inline float* get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(float value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_skeletonJSON_4() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___skeletonJSON_4)); }
	inline TextAsset_t3022178571 * get_skeletonJSON_4() const { return ___skeletonJSON_4; }
	inline TextAsset_t3022178571 ** get_address_of_skeletonJSON_4() { return &___skeletonJSON_4; }
	inline void set_skeletonJSON_4(TextAsset_t3022178571 * value)
	{
		___skeletonJSON_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonJSON_4), value);
	}

	inline static int32_t get_offset_of_fromAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___fromAnimation_5)); }
	inline StringU5BU5D_t1281789340* get_fromAnimation_5() const { return ___fromAnimation_5; }
	inline StringU5BU5D_t1281789340** get_address_of_fromAnimation_5() { return &___fromAnimation_5; }
	inline void set_fromAnimation_5(StringU5BU5D_t1281789340* value)
	{
		___fromAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___fromAnimation_5), value);
	}

	inline static int32_t get_offset_of_toAnimation_6() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___toAnimation_6)); }
	inline StringU5BU5D_t1281789340* get_toAnimation_6() const { return ___toAnimation_6; }
	inline StringU5BU5D_t1281789340** get_address_of_toAnimation_6() { return &___toAnimation_6; }
	inline void set_toAnimation_6(StringU5BU5D_t1281789340* value)
	{
		___toAnimation_6 = value;
		Il2CppCodeGenWriteBarrier((&___toAnimation_6), value);
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___duration_7)); }
	inline SingleU5BU5D_t1444911251* get_duration_7() const { return ___duration_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(SingleU5BU5D_t1444911251* value)
	{
		___duration_7 = value;
		Il2CppCodeGenWriteBarrier((&___duration_7), value);
	}

	inline static int32_t get_offset_of_defaultMix_8() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___defaultMix_8)); }
	inline float get_defaultMix_8() const { return ___defaultMix_8; }
	inline float* get_address_of_defaultMix_8() { return &___defaultMix_8; }
	inline void set_defaultMix_8(float value)
	{
		___defaultMix_8 = value;
	}

	inline static int32_t get_offset_of_controller_9() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___controller_9)); }
	inline RuntimeAnimatorController_t2933699135 * get_controller_9() const { return ___controller_9; }
	inline RuntimeAnimatorController_t2933699135 ** get_address_of_controller_9() { return &___controller_9; }
	inline void set_controller_9(RuntimeAnimatorController_t2933699135 * value)
	{
		___controller_9 = value;
		Il2CppCodeGenWriteBarrier((&___controller_9), value);
	}

	inline static int32_t get_offset_of_skeletonData_10() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___skeletonData_10)); }
	inline SkeletonData_t2032710716 * get_skeletonData_10() const { return ___skeletonData_10; }
	inline SkeletonData_t2032710716 ** get_address_of_skeletonData_10() { return &___skeletonData_10; }
	inline void set_skeletonData_10(SkeletonData_t2032710716 * value)
	{
		___skeletonData_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_10), value);
	}

	inline static int32_t get_offset_of_stateData_11() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t3748144825, ___stateData_11)); }
	inline AnimationStateData_t3010651567 * get_stateData_11() const { return ___stateData_11; }
	inline AnimationStateData_t3010651567 ** get_address_of_stateData_11() { return &___stateData_11; }
	inline void set_stateData_11(AnimationStateData_t3010651567 * value)
	{
		___stateData_11 = value;
		Il2CppCodeGenWriteBarrier((&___stateData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATAASSET_T3748144825_H
#ifndef UPDATEBONESDELEGATE_T735903178_H
#define UPDATEBONESDELEGATE_T735903178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.UpdateBonesDelegate
struct  UpdateBonesDelegate_t735903178  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEBONESDELEGATE_T735903178_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SKELETONRENDERER_T2098681813_H
#define SKELETONRENDERER_T2098681813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer
struct  SkeletonRenderer_t2098681813  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate Spine.Unity.SkeletonRenderer::OnRebuild
	SkeletonRendererDelegate_t3507789975 * ___OnRebuild_2;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonRenderer::OnPostProcessVertices
	MeshGeneratorDelegate_t1654156803 * ___OnPostProcessVertices_3;
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonRenderer::skeletonDataAsset
	SkeletonDataAsset_t3748144825 * ___skeletonDataAsset_4;
	// System.String Spine.Unity.SkeletonRenderer::initialSkinName
	String_t* ___initialSkinName_5;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipX
	bool ___initialFlipX_6;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipY
	bool ___initialFlipY_7;
	// System.String[] Spine.Unity.SkeletonRenderer::separatorSlotNames
	StringU5BU5D_t1281789340* ___separatorSlotNames_8;
	// System.Collections.Generic.List`1<Spine.Slot> Spine.Unity.SkeletonRenderer::separatorSlots
	List_1_t692048146 * ___separatorSlots_9;
	// System.Single Spine.Unity.SkeletonRenderer::zSpacing
	float ___zSpacing_10;
	// System.Boolean Spine.Unity.SkeletonRenderer::useClipping
	bool ___useClipping_11;
	// System.Boolean Spine.Unity.SkeletonRenderer::immutableTriangles
	bool ___immutableTriangles_12;
	// System.Boolean Spine.Unity.SkeletonRenderer::pmaVertexColors
	bool ___pmaVertexColors_13;
	// System.Boolean Spine.Unity.SkeletonRenderer::clearStateOnDisable
	bool ___clearStateOnDisable_14;
	// System.Boolean Spine.Unity.SkeletonRenderer::tintBlack
	bool ___tintBlack_15;
	// System.Boolean Spine.Unity.SkeletonRenderer::singleSubmesh
	bool ___singleSubmesh_16;
	// System.Boolean Spine.Unity.SkeletonRenderer::addNormals
	bool ___addNormals_17;
	// System.Boolean Spine.Unity.SkeletonRenderer::calculateTangents
	bool ___calculateTangents_18;
	// System.Boolean Spine.Unity.SkeletonRenderer::logErrors
	bool ___logErrors_19;
	// System.Boolean Spine.Unity.SkeletonRenderer::disableRenderingOnOverride
	bool ___disableRenderingOnOverride_20;
	// Spine.Unity.SkeletonRenderer/InstructionDelegate Spine.Unity.SkeletonRenderer::generateMeshOverride
	InstructionDelegate_t2225421195 * ___generateMeshOverride_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customMaterialOverride
	Dictionary_2_t3700682020 * ___customMaterialOverride_22;
	// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customSlotMaterials
	Dictionary_2_t3424054551 * ___customSlotMaterials_23;
	// UnityEngine.MeshRenderer Spine.Unity.SkeletonRenderer::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_24;
	// UnityEngine.MeshFilter Spine.Unity.SkeletonRenderer::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_25;
	// System.Boolean Spine.Unity.SkeletonRenderer::valid
	bool ___valid_26;
	// Spine.Skeleton Spine.Unity.SkeletonRenderer::skeleton
	Skeleton_t3686076450 * ___skeleton_27;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonRenderer::currentInstructions
	SkeletonRendererInstruction_t651787775 * ___currentInstructions_28;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonRenderer::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_29;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.SkeletonRenderer::rendererBuffers
	MeshRendererBuffers_t756429994 * ___rendererBuffers_30;

public:
	inline static int32_t get_offset_of_OnRebuild_2() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___OnRebuild_2)); }
	inline SkeletonRendererDelegate_t3507789975 * get_OnRebuild_2() const { return ___OnRebuild_2; }
	inline SkeletonRendererDelegate_t3507789975 ** get_address_of_OnRebuild_2() { return &___OnRebuild_2; }
	inline void set_OnRebuild_2(SkeletonRendererDelegate_t3507789975 * value)
	{
		___OnRebuild_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRebuild_2), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_3() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___OnPostProcessVertices_3)); }
	inline MeshGeneratorDelegate_t1654156803 * get_OnPostProcessVertices_3() const { return ___OnPostProcessVertices_3; }
	inline MeshGeneratorDelegate_t1654156803 ** get_address_of_OnPostProcessVertices_3() { return &___OnPostProcessVertices_3; }
	inline void set_OnPostProcessVertices_3(MeshGeneratorDelegate_t1654156803 * value)
	{
		___OnPostProcessVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_3), value);
	}

	inline static int32_t get_offset_of_skeletonDataAsset_4() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___skeletonDataAsset_4)); }
	inline SkeletonDataAsset_t3748144825 * get_skeletonDataAsset_4() const { return ___skeletonDataAsset_4; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_skeletonDataAsset_4() { return &___skeletonDataAsset_4; }
	inline void set_skeletonDataAsset_4(SkeletonDataAsset_t3748144825 * value)
	{
		___skeletonDataAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_4), value);
	}

	inline static int32_t get_offset_of_initialSkinName_5() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___initialSkinName_5)); }
	inline String_t* get_initialSkinName_5() const { return ___initialSkinName_5; }
	inline String_t** get_address_of_initialSkinName_5() { return &___initialSkinName_5; }
	inline void set_initialSkinName_5(String_t* value)
	{
		___initialSkinName_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_5), value);
	}

	inline static int32_t get_offset_of_initialFlipX_6() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___initialFlipX_6)); }
	inline bool get_initialFlipX_6() const { return ___initialFlipX_6; }
	inline bool* get_address_of_initialFlipX_6() { return &___initialFlipX_6; }
	inline void set_initialFlipX_6(bool value)
	{
		___initialFlipX_6 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_7() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___initialFlipY_7)); }
	inline bool get_initialFlipY_7() const { return ___initialFlipY_7; }
	inline bool* get_address_of_initialFlipY_7() { return &___initialFlipY_7; }
	inline void set_initialFlipY_7(bool value)
	{
		___initialFlipY_7 = value;
	}

	inline static int32_t get_offset_of_separatorSlotNames_8() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___separatorSlotNames_8)); }
	inline StringU5BU5D_t1281789340* get_separatorSlotNames_8() const { return ___separatorSlotNames_8; }
	inline StringU5BU5D_t1281789340** get_address_of_separatorSlotNames_8() { return &___separatorSlotNames_8; }
	inline void set_separatorSlotNames_8(StringU5BU5D_t1281789340* value)
	{
		___separatorSlotNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlotNames_8), value);
	}

	inline static int32_t get_offset_of_separatorSlots_9() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___separatorSlots_9)); }
	inline List_1_t692048146 * get_separatorSlots_9() const { return ___separatorSlots_9; }
	inline List_1_t692048146 ** get_address_of_separatorSlots_9() { return &___separatorSlots_9; }
	inline void set_separatorSlots_9(List_1_t692048146 * value)
	{
		___separatorSlots_9 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlots_9), value);
	}

	inline static int32_t get_offset_of_zSpacing_10() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___zSpacing_10)); }
	inline float get_zSpacing_10() const { return ___zSpacing_10; }
	inline float* get_address_of_zSpacing_10() { return &___zSpacing_10; }
	inline void set_zSpacing_10(float value)
	{
		___zSpacing_10 = value;
	}

	inline static int32_t get_offset_of_useClipping_11() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___useClipping_11)); }
	inline bool get_useClipping_11() const { return ___useClipping_11; }
	inline bool* get_address_of_useClipping_11() { return &___useClipping_11; }
	inline void set_useClipping_11(bool value)
	{
		___useClipping_11 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_12() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___immutableTriangles_12)); }
	inline bool get_immutableTriangles_12() const { return ___immutableTriangles_12; }
	inline bool* get_address_of_immutableTriangles_12() { return &___immutableTriangles_12; }
	inline void set_immutableTriangles_12(bool value)
	{
		___immutableTriangles_12 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_13() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___pmaVertexColors_13)); }
	inline bool get_pmaVertexColors_13() const { return ___pmaVertexColors_13; }
	inline bool* get_address_of_pmaVertexColors_13() { return &___pmaVertexColors_13; }
	inline void set_pmaVertexColors_13(bool value)
	{
		___pmaVertexColors_13 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_14() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___clearStateOnDisable_14)); }
	inline bool get_clearStateOnDisable_14() const { return ___clearStateOnDisable_14; }
	inline bool* get_address_of_clearStateOnDisable_14() { return &___clearStateOnDisable_14; }
	inline void set_clearStateOnDisable_14(bool value)
	{
		___clearStateOnDisable_14 = value;
	}

	inline static int32_t get_offset_of_tintBlack_15() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___tintBlack_15)); }
	inline bool get_tintBlack_15() const { return ___tintBlack_15; }
	inline bool* get_address_of_tintBlack_15() { return &___tintBlack_15; }
	inline void set_tintBlack_15(bool value)
	{
		___tintBlack_15 = value;
	}

	inline static int32_t get_offset_of_singleSubmesh_16() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___singleSubmesh_16)); }
	inline bool get_singleSubmesh_16() const { return ___singleSubmesh_16; }
	inline bool* get_address_of_singleSubmesh_16() { return &___singleSubmesh_16; }
	inline void set_singleSubmesh_16(bool value)
	{
		___singleSubmesh_16 = value;
	}

	inline static int32_t get_offset_of_addNormals_17() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___addNormals_17)); }
	inline bool get_addNormals_17() const { return ___addNormals_17; }
	inline bool* get_address_of_addNormals_17() { return &___addNormals_17; }
	inline void set_addNormals_17(bool value)
	{
		___addNormals_17 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_18() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___calculateTangents_18)); }
	inline bool get_calculateTangents_18() const { return ___calculateTangents_18; }
	inline bool* get_address_of_calculateTangents_18() { return &___calculateTangents_18; }
	inline void set_calculateTangents_18(bool value)
	{
		___calculateTangents_18 = value;
	}

	inline static int32_t get_offset_of_logErrors_19() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___logErrors_19)); }
	inline bool get_logErrors_19() const { return ___logErrors_19; }
	inline bool* get_address_of_logErrors_19() { return &___logErrors_19; }
	inline void set_logErrors_19(bool value)
	{
		___logErrors_19 = value;
	}

	inline static int32_t get_offset_of_disableRenderingOnOverride_20() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___disableRenderingOnOverride_20)); }
	inline bool get_disableRenderingOnOverride_20() const { return ___disableRenderingOnOverride_20; }
	inline bool* get_address_of_disableRenderingOnOverride_20() { return &___disableRenderingOnOverride_20; }
	inline void set_disableRenderingOnOverride_20(bool value)
	{
		___disableRenderingOnOverride_20 = value;
	}

	inline static int32_t get_offset_of_generateMeshOverride_21() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___generateMeshOverride_21)); }
	inline InstructionDelegate_t2225421195 * get_generateMeshOverride_21() const { return ___generateMeshOverride_21; }
	inline InstructionDelegate_t2225421195 ** get_address_of_generateMeshOverride_21() { return &___generateMeshOverride_21; }
	inline void set_generateMeshOverride_21(InstructionDelegate_t2225421195 * value)
	{
		___generateMeshOverride_21 = value;
		Il2CppCodeGenWriteBarrier((&___generateMeshOverride_21), value);
	}

	inline static int32_t get_offset_of_customMaterialOverride_22() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___customMaterialOverride_22)); }
	inline Dictionary_2_t3700682020 * get_customMaterialOverride_22() const { return ___customMaterialOverride_22; }
	inline Dictionary_2_t3700682020 ** get_address_of_customMaterialOverride_22() { return &___customMaterialOverride_22; }
	inline void set_customMaterialOverride_22(Dictionary_2_t3700682020 * value)
	{
		___customMaterialOverride_22 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverride_22), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_23() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___customSlotMaterials_23)); }
	inline Dictionary_2_t3424054551 * get_customSlotMaterials_23() const { return ___customSlotMaterials_23; }
	inline Dictionary_2_t3424054551 ** get_address_of_customSlotMaterials_23() { return &___customSlotMaterials_23; }
	inline void set_customSlotMaterials_23(Dictionary_2_t3424054551 * value)
	{
		___customSlotMaterials_23 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_23), value);
	}

	inline static int32_t get_offset_of_meshRenderer_24() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___meshRenderer_24)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_24() const { return ___meshRenderer_24; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_24() { return &___meshRenderer_24; }
	inline void set_meshRenderer_24(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_24 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_24), value);
	}

	inline static int32_t get_offset_of_meshFilter_25() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___meshFilter_25)); }
	inline MeshFilter_t3523625662 * get_meshFilter_25() const { return ___meshFilter_25; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_25() { return &___meshFilter_25; }
	inline void set_meshFilter_25(MeshFilter_t3523625662 * value)
	{
		___meshFilter_25 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_25), value);
	}

	inline static int32_t get_offset_of_valid_26() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___valid_26)); }
	inline bool get_valid_26() const { return ___valid_26; }
	inline bool* get_address_of_valid_26() { return &___valid_26; }
	inline void set_valid_26(bool value)
	{
		___valid_26 = value;
	}

	inline static int32_t get_offset_of_skeleton_27() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___skeleton_27)); }
	inline Skeleton_t3686076450 * get_skeleton_27() const { return ___skeleton_27; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_27() { return &___skeleton_27; }
	inline void set_skeleton_27(Skeleton_t3686076450 * value)
	{
		___skeleton_27 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_27), value);
	}

	inline static int32_t get_offset_of_currentInstructions_28() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___currentInstructions_28)); }
	inline SkeletonRendererInstruction_t651787775 * get_currentInstructions_28() const { return ___currentInstructions_28; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_currentInstructions_28() { return &___currentInstructions_28; }
	inline void set_currentInstructions_28(SkeletonRendererInstruction_t651787775 * value)
	{
		___currentInstructions_28 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_28), value);
	}

	inline static int32_t get_offset_of_meshGenerator_29() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___meshGenerator_29)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_29() const { return ___meshGenerator_29; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_29() { return &___meshGenerator_29; }
	inline void set_meshGenerator_29(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_29 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_29), value);
	}

	inline static int32_t get_offset_of_rendererBuffers_30() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2098681813, ___rendererBuffers_30)); }
	inline MeshRendererBuffers_t756429994 * get_rendererBuffers_30() const { return ___rendererBuffers_30; }
	inline MeshRendererBuffers_t756429994 ** get_address_of_rendererBuffers_30() { return &___rendererBuffers_30; }
	inline void set_rendererBuffers_30(MeshRendererBuffers_t756429994 * value)
	{
		___rendererBuffers_30 = value;
		Il2CppCodeGenWriteBarrier((&___rendererBuffers_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERER_T2098681813_H
#ifndef POINTFOLLOWER_T1569279465_H
#define POINTFOLLOWER_T1569279465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.PointFollower
struct  PointFollower_t1569279465  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.PointFollower::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_2;
	// System.String Spine.Unity.PointFollower::slotName
	String_t* ___slotName_3;
	// System.String Spine.Unity.PointFollower::pointAttachmentName
	String_t* ___pointAttachmentName_4;
	// System.Boolean Spine.Unity.PointFollower::followRotation
	bool ___followRotation_5;
	// System.Boolean Spine.Unity.PointFollower::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.PointFollower::followSkeletonZPosition
	bool ___followSkeletonZPosition_7;
	// UnityEngine.Transform Spine.Unity.PointFollower::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_8;
	// System.Boolean Spine.Unity.PointFollower::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_9;
	// Spine.PointAttachment Spine.Unity.PointFollower::point
	PointAttachment_t2275020146 * ___point_10;
	// Spine.Bone Spine.Unity.PointFollower::bone
	Bone_t1086356328 * ___bone_11;
	// System.Boolean Spine.Unity.PointFollower::valid
	bool ___valid_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_slotName_3() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___slotName_3)); }
	inline String_t* get_slotName_3() const { return ___slotName_3; }
	inline String_t** get_address_of_slotName_3() { return &___slotName_3; }
	inline void set_slotName_3(String_t* value)
	{
		___slotName_3 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_3), value);
	}

	inline static int32_t get_offset_of_pointAttachmentName_4() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___pointAttachmentName_4)); }
	inline String_t* get_pointAttachmentName_4() const { return ___pointAttachmentName_4; }
	inline String_t** get_address_of_pointAttachmentName_4() { return &___pointAttachmentName_4; }
	inline void set_pointAttachmentName_4(String_t* value)
	{
		___pointAttachmentName_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointAttachmentName_4), value);
	}

	inline static int32_t get_offset_of_followRotation_5() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___followRotation_5)); }
	inline bool get_followRotation_5() const { return ___followRotation_5; }
	inline bool* get_address_of_followRotation_5() { return &___followRotation_5; }
	inline void set_followRotation_5(bool value)
	{
		___followRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followSkeletonZPosition_7() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___followSkeletonZPosition_7)); }
	inline bool get_followSkeletonZPosition_7() const { return ___followSkeletonZPosition_7; }
	inline bool* get_address_of_followSkeletonZPosition_7() { return &___followSkeletonZPosition_7; }
	inline void set_followSkeletonZPosition_7(bool value)
	{
		___followSkeletonZPosition_7 = value;
	}

	inline static int32_t get_offset_of_skeletonTransform_8() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___skeletonTransform_8)); }
	inline Transform_t3600365921 * get_skeletonTransform_8() const { return ___skeletonTransform_8; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_8() { return &___skeletonTransform_8; }
	inline void set_skeletonTransform_8(Transform_t3600365921 * value)
	{
		___skeletonTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_8), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_9() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___skeletonTransformIsParent_9)); }
	inline bool get_skeletonTransformIsParent_9() const { return ___skeletonTransformIsParent_9; }
	inline bool* get_address_of_skeletonTransformIsParent_9() { return &___skeletonTransformIsParent_9; }
	inline void set_skeletonTransformIsParent_9(bool value)
	{
		___skeletonTransformIsParent_9 = value;
	}

	inline static int32_t get_offset_of_point_10() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___point_10)); }
	inline PointAttachment_t2275020146 * get_point_10() const { return ___point_10; }
	inline PointAttachment_t2275020146 ** get_address_of_point_10() { return &___point_10; }
	inline void set_point_10(PointAttachment_t2275020146 * value)
	{
		___point_10 = value;
		Il2CppCodeGenWriteBarrier((&___point_10), value);
	}

	inline static int32_t get_offset_of_bone_11() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___bone_11)); }
	inline Bone_t1086356328 * get_bone_11() const { return ___bone_11; }
	inline Bone_t1086356328 ** get_address_of_bone_11() { return &___bone_11; }
	inline void set_bone_11(Bone_t1086356328 * value)
	{
		___bone_11 = value;
		Il2CppCodeGenWriteBarrier((&___bone_11), value);
	}

	inline static int32_t get_offset_of_valid_12() { return static_cast<int32_t>(offsetof(PointFollower_t1569279465, ___valid_12)); }
	inline bool get_valid_12() const { return ___valid_12; }
	inline bool* get_address_of_valid_12() { return &___valid_12; }
	inline void set_valid_12(bool value)
	{
		___valid_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTFOLLOWER_T1569279465_H
#ifndef BONEFOLLOWER_T3069400636_H
#define BONEFOLLOWER_T3069400636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollower
struct  BoneFollower_t3069400636  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoneFollower::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_2;
	// System.String Spine.Unity.BoneFollower::boneName
	String_t* ___boneName_3;
	// System.Boolean Spine.Unity.BoneFollower::followZPosition
	bool ___followZPosition_4;
	// System.Boolean Spine.Unity.BoneFollower::followBoneRotation
	bool ___followBoneRotation_5;
	// System.Boolean Spine.Unity.BoneFollower::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.BoneFollower::followLocalScale
	bool ___followLocalScale_7;
	// System.Boolean Spine.Unity.BoneFollower::initializeOnAwake
	bool ___initializeOnAwake_8;
	// System.Boolean Spine.Unity.BoneFollower::valid
	bool ___valid_9;
	// Spine.Bone Spine.Unity.BoneFollower::bone
	Bone_t1086356328 * ___bone_10;
	// UnityEngine.Transform Spine.Unity.BoneFollower::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_11;
	// System.Boolean Spine.Unity.BoneFollower::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_boneName_3() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___boneName_3)); }
	inline String_t* get_boneName_3() const { return ___boneName_3; }
	inline String_t** get_address_of_boneName_3() { return &___boneName_3; }
	inline void set_boneName_3(String_t* value)
	{
		___boneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_3), value);
	}

	inline static int32_t get_offset_of_followZPosition_4() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followZPosition_4)); }
	inline bool get_followZPosition_4() const { return ___followZPosition_4; }
	inline bool* get_address_of_followZPosition_4() { return &___followZPosition_4; }
	inline void set_followZPosition_4(bool value)
	{
		___followZPosition_4 = value;
	}

	inline static int32_t get_offset_of_followBoneRotation_5() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followBoneRotation_5)); }
	inline bool get_followBoneRotation_5() const { return ___followBoneRotation_5; }
	inline bool* get_address_of_followBoneRotation_5() { return &___followBoneRotation_5; }
	inline void set_followBoneRotation_5(bool value)
	{
		___followBoneRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_7() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___followLocalScale_7)); }
	inline bool get_followLocalScale_7() const { return ___followLocalScale_7; }
	inline bool* get_address_of_followLocalScale_7() { return &___followLocalScale_7; }
	inline void set_followLocalScale_7(bool value)
	{
		___followLocalScale_7 = value;
	}

	inline static int32_t get_offset_of_initializeOnAwake_8() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___initializeOnAwake_8)); }
	inline bool get_initializeOnAwake_8() const { return ___initializeOnAwake_8; }
	inline bool* get_address_of_initializeOnAwake_8() { return &___initializeOnAwake_8; }
	inline void set_initializeOnAwake_8(bool value)
	{
		___initializeOnAwake_8 = value;
	}

	inline static int32_t get_offset_of_valid_9() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___valid_9)); }
	inline bool get_valid_9() const { return ___valid_9; }
	inline bool* get_address_of_valid_9() { return &___valid_9; }
	inline void set_valid_9(bool value)
	{
		___valid_9 = value;
	}

	inline static int32_t get_offset_of_bone_10() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___bone_10)); }
	inline Bone_t1086356328 * get_bone_10() const { return ___bone_10; }
	inline Bone_t1086356328 ** get_address_of_bone_10() { return &___bone_10; }
	inline void set_bone_10(Bone_t1086356328 * value)
	{
		___bone_10 = value;
		Il2CppCodeGenWriteBarrier((&___bone_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_11() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___skeletonTransform_11)); }
	inline Transform_t3600365921 * get_skeletonTransform_11() const { return ___skeletonTransform_11; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_11() { return &___skeletonTransform_11; }
	inline void set_skeletonTransform_11(Transform_t3600365921 * value)
	{
		___skeletonTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_11), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_12() { return static_cast<int32_t>(offsetof(BoneFollower_t3069400636, ___skeletonTransformIsParent_12)); }
	inline bool get_skeletonTransformIsParent_12() const { return ___skeletonTransformIsParent_12; }
	inline bool* get_address_of_skeletonTransformIsParent_12() { return &___skeletonTransformIsParent_12; }
	inline void set_skeletonTransformIsParent_12(bool value)
	{
		___skeletonTransformIsParent_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWER_T3069400636_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef BOUNDINGBOXFOLLOWER_T4182505745_H
#define BOUNDINGBOXFOLLOWER_T4182505745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoundingBoxFollower
struct  BoundingBoxFollower_t4182505745  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoundingBoxFollower::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_3;
	// System.String Spine.Unity.BoundingBoxFollower::slotName
	String_t* ___slotName_4;
	// System.Boolean Spine.Unity.BoundingBoxFollower::isTrigger
	bool ___isTrigger_5;
	// System.Boolean Spine.Unity.BoundingBoxFollower::clearStateOnDisable
	bool ___clearStateOnDisable_6;
	// Spine.Slot Spine.Unity.BoundingBoxFollower::slot
	Slot_t3514940700 * ___slot_7;
	// Spine.BoundingBoxAttachment Spine.Unity.BoundingBoxFollower::currentAttachment
	BoundingBoxAttachment_t2797506510 * ___currentAttachment_8;
	// System.String Spine.Unity.BoundingBoxFollower::currentAttachmentName
	String_t* ___currentAttachmentName_9;
	// UnityEngine.PolygonCollider2D Spine.Unity.BoundingBoxFollower::currentCollider
	PolygonCollider2D_t57175488 * ___currentCollider_10;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D> Spine.Unity.BoundingBoxFollower::colliderTable
	Dictionary_2_t3714483914 * ___colliderTable_11;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String> Spine.Unity.BoundingBoxFollower::nameTable
	Dictionary_2_t1209791819 * ___nameTable_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}

	inline static int32_t get_offset_of_slotName_4() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___slotName_4)); }
	inline String_t* get_slotName_4() const { return ___slotName_4; }
	inline String_t** get_address_of_slotName_4() { return &___slotName_4; }
	inline void set_slotName_4(String_t* value)
	{
		___slotName_4 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_4), value);
	}

	inline static int32_t get_offset_of_isTrigger_5() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___isTrigger_5)); }
	inline bool get_isTrigger_5() const { return ___isTrigger_5; }
	inline bool* get_address_of_isTrigger_5() { return &___isTrigger_5; }
	inline void set_isTrigger_5(bool value)
	{
		___isTrigger_5 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_6() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___clearStateOnDisable_6)); }
	inline bool get_clearStateOnDisable_6() const { return ___clearStateOnDisable_6; }
	inline bool* get_address_of_clearStateOnDisable_6() { return &___clearStateOnDisable_6; }
	inline void set_clearStateOnDisable_6(bool value)
	{
		___clearStateOnDisable_6 = value;
	}

	inline static int32_t get_offset_of_slot_7() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___slot_7)); }
	inline Slot_t3514940700 * get_slot_7() const { return ___slot_7; }
	inline Slot_t3514940700 ** get_address_of_slot_7() { return &___slot_7; }
	inline void set_slot_7(Slot_t3514940700 * value)
	{
		___slot_7 = value;
		Il2CppCodeGenWriteBarrier((&___slot_7), value);
	}

	inline static int32_t get_offset_of_currentAttachment_8() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___currentAttachment_8)); }
	inline BoundingBoxAttachment_t2797506510 * get_currentAttachment_8() const { return ___currentAttachment_8; }
	inline BoundingBoxAttachment_t2797506510 ** get_address_of_currentAttachment_8() { return &___currentAttachment_8; }
	inline void set_currentAttachment_8(BoundingBoxAttachment_t2797506510 * value)
	{
		___currentAttachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachment_8), value);
	}

	inline static int32_t get_offset_of_currentAttachmentName_9() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___currentAttachmentName_9)); }
	inline String_t* get_currentAttachmentName_9() const { return ___currentAttachmentName_9; }
	inline String_t** get_address_of_currentAttachmentName_9() { return &___currentAttachmentName_9; }
	inline void set_currentAttachmentName_9(String_t* value)
	{
		___currentAttachmentName_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachmentName_9), value);
	}

	inline static int32_t get_offset_of_currentCollider_10() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___currentCollider_10)); }
	inline PolygonCollider2D_t57175488 * get_currentCollider_10() const { return ___currentCollider_10; }
	inline PolygonCollider2D_t57175488 ** get_address_of_currentCollider_10() { return &___currentCollider_10; }
	inline void set_currentCollider_10(PolygonCollider2D_t57175488 * value)
	{
		___currentCollider_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentCollider_10), value);
	}

	inline static int32_t get_offset_of_colliderTable_11() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___colliderTable_11)); }
	inline Dictionary_2_t3714483914 * get_colliderTable_11() const { return ___colliderTable_11; }
	inline Dictionary_2_t3714483914 ** get_address_of_colliderTable_11() { return &___colliderTable_11; }
	inline void set_colliderTable_11(Dictionary_2_t3714483914 * value)
	{
		___colliderTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliderTable_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745, ___nameTable_12)); }
	inline Dictionary_2_t1209791819 * get_nameTable_12() const { return ___nameTable_12; }
	inline Dictionary_2_t1209791819 ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(Dictionary_2_t1209791819 * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}
};

struct BoundingBoxFollower_t4182505745_StaticFields
{
public:
	// System.Boolean Spine.Unity.BoundingBoxFollower::DebugMessages
	bool ___DebugMessages_2;

public:
	inline static int32_t get_offset_of_DebugMessages_2() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t4182505745_StaticFields, ___DebugMessages_2)); }
	inline bool get_DebugMessages_2() const { return ___DebugMessages_2; }
	inline bool* get_address_of_DebugMessages_2() { return &___DebugMessages_2; }
	inline void set_DebugMessages_2(bool value)
	{
		___DebugMessages_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXFOLLOWER_T4182505745_H
#ifndef SKELETONUTILITYKINEMATICSHADOW_T296913764_H
#define SKELETONUTILITYKINEMATICSHADOW_T296913764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow
struct  SkeletonUtilityKinematicShadow_t296913764  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::detachedShadow
	bool ___detachedShadow_2;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow::parent
	Transform_t3600365921 * ___parent_3;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::hideShadow
	bool ___hideShadow_4;
	// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/PhysicsSystem Spine.Unity.Modules.SkeletonUtilityKinematicShadow::physicsSystem
	int32_t ___physicsSystem_5;
	// UnityEngine.GameObject Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowRoot
	GameObject_t1113636619 * ___shadowRoot_6;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair> Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowTable
	List_1_t972525202 * ___shadowTable_7;

public:
	inline static int32_t get_offset_of_detachedShadow_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___detachedShadow_2)); }
	inline bool get_detachedShadow_2() const { return ___detachedShadow_2; }
	inline bool* get_address_of_detachedShadow_2() { return &___detachedShadow_2; }
	inline void set_detachedShadow_2(bool value)
	{
		___detachedShadow_2 = value;
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___parent_3)); }
	inline Transform_t3600365921 * get_parent_3() const { return ___parent_3; }
	inline Transform_t3600365921 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Transform_t3600365921 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_hideShadow_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___hideShadow_4)); }
	inline bool get_hideShadow_4() const { return ___hideShadow_4; }
	inline bool* get_address_of_hideShadow_4() { return &___hideShadow_4; }
	inline void set_hideShadow_4(bool value)
	{
		___hideShadow_4 = value;
	}

	inline static int32_t get_offset_of_physicsSystem_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___physicsSystem_5)); }
	inline int32_t get_physicsSystem_5() const { return ___physicsSystem_5; }
	inline int32_t* get_address_of_physicsSystem_5() { return &___physicsSystem_5; }
	inline void set_physicsSystem_5(int32_t value)
	{
		___physicsSystem_5 = value;
	}

	inline static int32_t get_offset_of_shadowRoot_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___shadowRoot_6)); }
	inline GameObject_t1113636619 * get_shadowRoot_6() const { return ___shadowRoot_6; }
	inline GameObject_t1113636619 ** get_address_of_shadowRoot_6() { return &___shadowRoot_6; }
	inline void set_shadowRoot_6(GameObject_t1113636619 * value)
	{
		___shadowRoot_6 = value;
		Il2CppCodeGenWriteBarrier((&___shadowRoot_6), value);
	}

	inline static int32_t get_offset_of_shadowTable_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t296913764, ___shadowTable_7)); }
	inline List_1_t972525202 * get_shadowTable_7() const { return ___shadowTable_7; }
	inline List_1_t972525202 ** get_address_of_shadowTable_7() { return &___shadowTable_7; }
	inline void set_shadowTable_7(List_1_t972525202 * value)
	{
		___shadowTable_7 = value;
		Il2CppCodeGenWriteBarrier((&___shadowTable_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYKINEMATICSHADOW_T296913764_H
#ifndef SKELETONPARTSRENDERER_T667127217_H
#define SKELETONPARTSRENDERER_T667127217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonPartsRenderer
struct  SkeletonPartsRenderer_t667127217  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.MeshGenerator Spine.Unity.Modules.SkeletonPartsRenderer::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_2;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonPartsRenderer::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_3;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonPartsRenderer::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_4;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.Modules.SkeletonPartsRenderer::buffers
	MeshRendererBuffers_t756429994 * ___buffers_5;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.Modules.SkeletonPartsRenderer::currentInstructions
	SkeletonRendererInstruction_t651787775 * ___currentInstructions_6;

public:
	inline static int32_t get_offset_of_meshGenerator_2() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___meshGenerator_2)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_2() const { return ___meshGenerator_2; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_2() { return &___meshGenerator_2; }
	inline void set_meshGenerator_2(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_2), value);
	}

	inline static int32_t get_offset_of_meshRenderer_3() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___meshRenderer_3)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_3() const { return ___meshRenderer_3; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_3() { return &___meshRenderer_3; }
	inline void set_meshRenderer_3(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_3), value);
	}

	inline static int32_t get_offset_of_meshFilter_4() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___meshFilter_4)); }
	inline MeshFilter_t3523625662 * get_meshFilter_4() const { return ___meshFilter_4; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_4() { return &___meshFilter_4; }
	inline void set_meshFilter_4(MeshFilter_t3523625662 * value)
	{
		___meshFilter_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_4), value);
	}

	inline static int32_t get_offset_of_buffers_5() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___buffers_5)); }
	inline MeshRendererBuffers_t756429994 * get_buffers_5() const { return ___buffers_5; }
	inline MeshRendererBuffers_t756429994 ** get_address_of_buffers_5() { return &___buffers_5; }
	inline void set_buffers_5(MeshRendererBuffers_t756429994 * value)
	{
		___buffers_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_5), value);
	}

	inline static int32_t get_offset_of_currentInstructions_6() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t667127217, ___currentInstructions_6)); }
	inline SkeletonRendererInstruction_t651787775 * get_currentInstructions_6() const { return ___currentInstructions_6; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_currentInstructions_6() { return &___currentInstructions_6; }
	inline void set_currentInstructions_6(SkeletonRendererInstruction_t651787775 * value)
	{
		___currentInstructions_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONPARTSRENDERER_T667127217_H
#ifndef SKELETONRENDERSEPARATOR_T2026602841_H
#define SKELETONRENDERSEPARATOR_T2026602841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRenderSeparator
struct  SkeletonRenderSeparator_t2026602841  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRenderSeparator::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_3;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonRenderSeparator::mainMeshRenderer
	MeshRenderer_t587009260 * ___mainMeshRenderer_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyPropertyBlock
	bool ___copyPropertyBlock_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyMeshRendererFlags
	bool ___copyMeshRendererFlags_6;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer> Spine.Unity.Modules.SkeletonRenderSeparator::partsRenderers
	List_1_t2139201959 * ___partsRenderers_7;
	// UnityEngine.MaterialPropertyBlock Spine.Unity.Modules.SkeletonRenderSeparator::copiedBlock
	MaterialPropertyBlock_t3213117958 * ___copiedBlock_8;

public:
	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}

	inline static int32_t get_offset_of_mainMeshRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___mainMeshRenderer_4)); }
	inline MeshRenderer_t587009260 * get_mainMeshRenderer_4() const { return ___mainMeshRenderer_4; }
	inline MeshRenderer_t587009260 ** get_address_of_mainMeshRenderer_4() { return &___mainMeshRenderer_4; }
	inline void set_mainMeshRenderer_4(MeshRenderer_t587009260 * value)
	{
		___mainMeshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainMeshRenderer_4), value);
	}

	inline static int32_t get_offset_of_copyPropertyBlock_5() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___copyPropertyBlock_5)); }
	inline bool get_copyPropertyBlock_5() const { return ___copyPropertyBlock_5; }
	inline bool* get_address_of_copyPropertyBlock_5() { return &___copyPropertyBlock_5; }
	inline void set_copyPropertyBlock_5(bool value)
	{
		___copyPropertyBlock_5 = value;
	}

	inline static int32_t get_offset_of_copyMeshRendererFlags_6() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___copyMeshRendererFlags_6)); }
	inline bool get_copyMeshRendererFlags_6() const { return ___copyMeshRendererFlags_6; }
	inline bool* get_address_of_copyMeshRendererFlags_6() { return &___copyMeshRendererFlags_6; }
	inline void set_copyMeshRendererFlags_6(bool value)
	{
		___copyMeshRendererFlags_6 = value;
	}

	inline static int32_t get_offset_of_partsRenderers_7() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___partsRenderers_7)); }
	inline List_1_t2139201959 * get_partsRenderers_7() const { return ___partsRenderers_7; }
	inline List_1_t2139201959 ** get_address_of_partsRenderers_7() { return &___partsRenderers_7; }
	inline void set_partsRenderers_7(List_1_t2139201959 * value)
	{
		___partsRenderers_7 = value;
		Il2CppCodeGenWriteBarrier((&___partsRenderers_7), value);
	}

	inline static int32_t get_offset_of_copiedBlock_8() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t2026602841, ___copiedBlock_8)); }
	inline MaterialPropertyBlock_t3213117958 * get_copiedBlock_8() const { return ___copiedBlock_8; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_copiedBlock_8() { return &___copiedBlock_8; }
	inline void set_copiedBlock_8(MaterialPropertyBlock_t3213117958 * value)
	{
		___copiedBlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___copiedBlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERSEPARATOR_T2026602841_H
#ifndef SLOTBLENDMODES_T1277474191_H
#define SLOTBLENDMODES_T1277474191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes
struct  SlotBlendModes_t1277474191  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::multiplyMaterialSource
	Material_t340375123 * ___multiplyMaterialSource_3;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::screenMaterialSource
	Material_t340375123 * ___screenMaterialSource_4;
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes::texture
	Texture2D_t3840446185 * ___texture_5;
	// System.Boolean Spine.Unity.Modules.SlotBlendModes::<Applied>k__BackingField
	bool ___U3CAppliedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_multiplyMaterialSource_3() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___multiplyMaterialSource_3)); }
	inline Material_t340375123 * get_multiplyMaterialSource_3() const { return ___multiplyMaterialSource_3; }
	inline Material_t340375123 ** get_address_of_multiplyMaterialSource_3() { return &___multiplyMaterialSource_3; }
	inline void set_multiplyMaterialSource_3(Material_t340375123 * value)
	{
		___multiplyMaterialSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyMaterialSource_3), value);
	}

	inline static int32_t get_offset_of_screenMaterialSource_4() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___screenMaterialSource_4)); }
	inline Material_t340375123 * get_screenMaterialSource_4() const { return ___screenMaterialSource_4; }
	inline Material_t340375123 ** get_address_of_screenMaterialSource_4() { return &___screenMaterialSource_4; }
	inline void set_screenMaterialSource_4(Material_t340375123 * value)
	{
		___screenMaterialSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenMaterialSource_4), value);
	}

	inline static int32_t get_offset_of_texture_5() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___texture_5)); }
	inline Texture2D_t3840446185 * get_texture_5() const { return ___texture_5; }
	inline Texture2D_t3840446185 ** get_address_of_texture_5() { return &___texture_5; }
	inline void set_texture_5(Texture2D_t3840446185 * value)
	{
		___texture_5 = value;
		Il2CppCodeGenWriteBarrier((&___texture_5), value);
	}

	inline static int32_t get_offset_of_U3CAppliedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191, ___U3CAppliedU3Ek__BackingField_6)); }
	inline bool get_U3CAppliedU3Ek__BackingField_6() const { return ___U3CAppliedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CAppliedU3Ek__BackingField_6() { return &___U3CAppliedU3Ek__BackingField_6; }
	inline void set_U3CAppliedU3Ek__BackingField_6(bool value)
	{
		___U3CAppliedU3Ek__BackingField_6 = value;
	}
};

struct SlotBlendModes_t1277474191_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material> Spine.Unity.Modules.SlotBlendModes::materialTable
	Dictionary_2_t556337403 * ___materialTable_2;

public:
	inline static int32_t get_offset_of_materialTable_2() { return static_cast<int32_t>(offsetof(SlotBlendModes_t1277474191_StaticFields, ___materialTable_2)); }
	inline Dictionary_2_t556337403 * get_materialTable_2() const { return ___materialTable_2; }
	inline Dictionary_2_t556337403 ** get_address_of_materialTable_2() { return &___materialTable_2; }
	inline void set_materialTable_2(Dictionary_2_t556337403 * value)
	{
		___materialTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTBLENDMODES_T1277474191_H
#ifndef SKELETONUTILITY_T2980767925_H
#define SKELETONUTILITY_T2980767925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility
struct  SkeletonUtility_t2980767925  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate Spine.Unity.SkeletonUtility::OnReset
	SkeletonUtilityDelegate_t487527757 * ___OnReset_2;
	// UnityEngine.Transform Spine.Unity.SkeletonUtility::boneRoot
	Transform_t3600365921 * ___boneRoot_3;
	// Spine.Unity.SkeletonRenderer Spine.Unity.SkeletonUtility::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_4;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.SkeletonUtility::skeletonAnimation
	RuntimeObject* ___skeletonAnimation_5;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone> Spine.Unity.SkeletonUtility::utilityBones
	List_1_t3697594876 * ___utilityBones_6;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint> Spine.Unity.SkeletonUtility::utilityConstraints
	List_1_t2486457248 * ___utilityConstraints_7;
	// System.Boolean Spine.Unity.SkeletonUtility::hasTransformBones
	bool ___hasTransformBones_8;
	// System.Boolean Spine.Unity.SkeletonUtility::hasUtilityConstraints
	bool ___hasUtilityConstraints_9;
	// System.Boolean Spine.Unity.SkeletonUtility::needToReprocessBones
	bool ___needToReprocessBones_10;

public:
	inline static int32_t get_offset_of_OnReset_2() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___OnReset_2)); }
	inline SkeletonUtilityDelegate_t487527757 * get_OnReset_2() const { return ___OnReset_2; }
	inline SkeletonUtilityDelegate_t487527757 ** get_address_of_OnReset_2() { return &___OnReset_2; }
	inline void set_OnReset_2(SkeletonUtilityDelegate_t487527757 * value)
	{
		___OnReset_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnReset_2), value);
	}

	inline static int32_t get_offset_of_boneRoot_3() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___boneRoot_3)); }
	inline Transform_t3600365921 * get_boneRoot_3() const { return ___boneRoot_3; }
	inline Transform_t3600365921 ** get_address_of_boneRoot_3() { return &___boneRoot_3; }
	inline void set_boneRoot_3(Transform_t3600365921 * value)
	{
		___boneRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___boneRoot_3), value);
	}

	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_skeletonAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___skeletonAnimation_5)); }
	inline RuntimeObject* get_skeletonAnimation_5() const { return ___skeletonAnimation_5; }
	inline RuntimeObject** get_address_of_skeletonAnimation_5() { return &___skeletonAnimation_5; }
	inline void set_skeletonAnimation_5(RuntimeObject* value)
	{
		___skeletonAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_5), value);
	}

	inline static int32_t get_offset_of_utilityBones_6() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___utilityBones_6)); }
	inline List_1_t3697594876 * get_utilityBones_6() const { return ___utilityBones_6; }
	inline List_1_t3697594876 ** get_address_of_utilityBones_6() { return &___utilityBones_6; }
	inline void set_utilityBones_6(List_1_t3697594876 * value)
	{
		___utilityBones_6 = value;
		Il2CppCodeGenWriteBarrier((&___utilityBones_6), value);
	}

	inline static int32_t get_offset_of_utilityConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___utilityConstraints_7)); }
	inline List_1_t2486457248 * get_utilityConstraints_7() const { return ___utilityConstraints_7; }
	inline List_1_t2486457248 ** get_address_of_utilityConstraints_7() { return &___utilityConstraints_7; }
	inline void set_utilityConstraints_7(List_1_t2486457248 * value)
	{
		___utilityConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___utilityConstraints_7), value);
	}

	inline static int32_t get_offset_of_hasTransformBones_8() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___hasTransformBones_8)); }
	inline bool get_hasTransformBones_8() const { return ___hasTransformBones_8; }
	inline bool* get_address_of_hasTransformBones_8() { return &___hasTransformBones_8; }
	inline void set_hasTransformBones_8(bool value)
	{
		___hasTransformBones_8 = value;
	}

	inline static int32_t get_offset_of_hasUtilityConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___hasUtilityConstraints_9)); }
	inline bool get_hasUtilityConstraints_9() const { return ___hasUtilityConstraints_9; }
	inline bool* get_address_of_hasUtilityConstraints_9() { return &___hasUtilityConstraints_9; }
	inline void set_hasUtilityConstraints_9(bool value)
	{
		___hasUtilityConstraints_9 = value;
	}

	inline static int32_t get_offset_of_needToReprocessBones_10() { return static_cast<int32_t>(offsetof(SkeletonUtility_t2980767925, ___needToReprocessBones_10)); }
	inline bool get_needToReprocessBones_10() const { return ___needToReprocessBones_10; }
	inline bool* get_address_of_needToReprocessBones_10() { return &___needToReprocessBones_10; }
	inline void set_needToReprocessBones_10(bool value)
	{
		___needToReprocessBones_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITY_T2980767925_H
#ifndef SKELETONUTILITYBONE_T2225520134_H
#define SKELETONUTILITYBONE_T2225520134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone
struct  SkeletonUtilityBone_t2225520134  : public MonoBehaviour_t3962482529
{
public:
	// System.String Spine.Unity.SkeletonUtilityBone::boneName
	String_t* ___boneName_2;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::parentReference
	Transform_t3600365921 * ___parentReference_3;
	// Spine.Unity.SkeletonUtilityBone/Mode Spine.Unity.SkeletonUtilityBone::mode
	int32_t ___mode_4;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::position
	bool ___position_5;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::rotation
	bool ___rotation_6;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::scale
	bool ___scale_7;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::zPosition
	bool ___zPosition_8;
	// System.Single Spine.Unity.SkeletonUtilityBone::overrideAlpha
	float ___overrideAlpha_9;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityBone::skeletonUtility
	SkeletonUtility_t2980767925 * ___skeletonUtility_10;
	// Spine.Bone Spine.Unity.SkeletonUtilityBone::bone
	Bone_t1086356328 * ___bone_11;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::transformLerpComplete
	bool ___transformLerpComplete_12;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::valid
	bool ___valid_13;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::cachedTransform
	Transform_t3600365921 * ___cachedTransform_14;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_15;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::incompatibleTransformMode
	bool ___incompatibleTransformMode_16;

public:
	inline static int32_t get_offset_of_boneName_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___boneName_2)); }
	inline String_t* get_boneName_2() const { return ___boneName_2; }
	inline String_t** get_address_of_boneName_2() { return &___boneName_2; }
	inline void set_boneName_2(String_t* value)
	{
		___boneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_2), value);
	}

	inline static int32_t get_offset_of_parentReference_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___parentReference_3)); }
	inline Transform_t3600365921 * get_parentReference_3() const { return ___parentReference_3; }
	inline Transform_t3600365921 ** get_address_of_parentReference_3() { return &___parentReference_3; }
	inline void set_parentReference_3(Transform_t3600365921 * value)
	{
		___parentReference_3 = value;
		Il2CppCodeGenWriteBarrier((&___parentReference_3), value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___position_5)); }
	inline bool get_position_5() const { return ___position_5; }
	inline bool* get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(bool value)
	{
		___position_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___rotation_6)); }
	inline bool get_rotation_6() const { return ___rotation_6; }
	inline bool* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(bool value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___scale_7)); }
	inline bool get_scale_7() const { return ___scale_7; }
	inline bool* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(bool value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_zPosition_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___zPosition_8)); }
	inline bool get_zPosition_8() const { return ___zPosition_8; }
	inline bool* get_address_of_zPosition_8() { return &___zPosition_8; }
	inline void set_zPosition_8(bool value)
	{
		___zPosition_8 = value;
	}

	inline static int32_t get_offset_of_overrideAlpha_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___overrideAlpha_9)); }
	inline float get_overrideAlpha_9() const { return ___overrideAlpha_9; }
	inline float* get_address_of_overrideAlpha_9() { return &___overrideAlpha_9; }
	inline void set_overrideAlpha_9(float value)
	{
		___overrideAlpha_9 = value;
	}

	inline static int32_t get_offset_of_skeletonUtility_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___skeletonUtility_10)); }
	inline SkeletonUtility_t2980767925 * get_skeletonUtility_10() const { return ___skeletonUtility_10; }
	inline SkeletonUtility_t2980767925 ** get_address_of_skeletonUtility_10() { return &___skeletonUtility_10; }
	inline void set_skeletonUtility_10(SkeletonUtility_t2980767925 * value)
	{
		___skeletonUtility_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_10), value);
	}

	inline static int32_t get_offset_of_bone_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___bone_11)); }
	inline Bone_t1086356328 * get_bone_11() const { return ___bone_11; }
	inline Bone_t1086356328 ** get_address_of_bone_11() { return &___bone_11; }
	inline void set_bone_11(Bone_t1086356328 * value)
	{
		___bone_11 = value;
		Il2CppCodeGenWriteBarrier((&___bone_11), value);
	}

	inline static int32_t get_offset_of_transformLerpComplete_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___transformLerpComplete_12)); }
	inline bool get_transformLerpComplete_12() const { return ___transformLerpComplete_12; }
	inline bool* get_address_of_transformLerpComplete_12() { return &___transformLerpComplete_12; }
	inline void set_transformLerpComplete_12(bool value)
	{
		___transformLerpComplete_12 = value;
	}

	inline static int32_t get_offset_of_valid_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___valid_13)); }
	inline bool get_valid_13() const { return ___valid_13; }
	inline bool* get_address_of_valid_13() { return &___valid_13; }
	inline void set_valid_13(bool value)
	{
		___valid_13 = value;
	}

	inline static int32_t get_offset_of_cachedTransform_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___cachedTransform_14)); }
	inline Transform_t3600365921 * get_cachedTransform_14() const { return ___cachedTransform_14; }
	inline Transform_t3600365921 ** get_address_of_cachedTransform_14() { return &___cachedTransform_14; }
	inline void set_cachedTransform_14(Transform_t3600365921 * value)
	{
		___cachedTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_14), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___skeletonTransform_15)); }
	inline Transform_t3600365921 * get_skeletonTransform_15() const { return ___skeletonTransform_15; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_15() { return &___skeletonTransform_15; }
	inline void set_skeletonTransform_15(Transform_t3600365921 * value)
	{
		___skeletonTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_15), value);
	}

	inline static int32_t get_offset_of_incompatibleTransformMode_16() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t2225520134, ___incompatibleTransformMode_16)); }
	inline bool get_incompatibleTransformMode_16() const { return ___incompatibleTransformMode_16; }
	inline bool* get_address_of_incompatibleTransformMode_16() { return &___incompatibleTransformMode_16; }
	inline void set_incompatibleTransformMode_16(bool value)
	{
		___incompatibleTransformMode_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYBONE_T2225520134_H
#ifndef SKELETONUTILITYCONSTRAINT_T1014382506_H
#define SKELETONUTILITYCONSTRAINT_T1014382506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityConstraint
struct  SkeletonUtilityConstraint_t1014382506  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonUtilityBone Spine.Unity.SkeletonUtilityConstraint::utilBone
	SkeletonUtilityBone_t2225520134 * ___utilBone_2;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityConstraint::skeletonUtility
	SkeletonUtility_t2980767925 * ___skeletonUtility_3;

public:
	inline static int32_t get_offset_of_utilBone_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_t1014382506, ___utilBone_2)); }
	inline SkeletonUtilityBone_t2225520134 * get_utilBone_2() const { return ___utilBone_2; }
	inline SkeletonUtilityBone_t2225520134 ** get_address_of_utilBone_2() { return &___utilBone_2; }
	inline void set_utilBone_2(SkeletonUtilityBone_t2225520134 * value)
	{
		___utilBone_2 = value;
		Il2CppCodeGenWriteBarrier((&___utilBone_2), value);
	}

	inline static int32_t get_offset_of_skeletonUtility_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_t1014382506, ___skeletonUtility_3)); }
	inline SkeletonUtility_t2980767925 * get_skeletonUtility_3() const { return ___skeletonUtility_3; }
	inline SkeletonUtility_t2980767925 ** get_address_of_skeletonUtility_3() { return &___skeletonUtility_3; }
	inline void set_skeletonUtility_3(SkeletonUtility_t2980767925 * value)
	{
		___skeletonUtility_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYCONSTRAINT_T1014382506_H
#ifndef BONEFOLLOWERGRAPHIC_T46362405_H
#define BONEFOLLOWERGRAPHIC_T46362405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollowerGraphic
struct  BoneFollowerGraphic_t46362405  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonGraphic Spine.Unity.BoneFollowerGraphic::skeletonGraphic
	SkeletonGraphic_t1744877482 * ___skeletonGraphic_2;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::initializeOnAwake
	bool ___initializeOnAwake_3;
	// System.String Spine.Unity.BoneFollowerGraphic::boneName
	String_t* ___boneName_4;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followBoneRotation
	bool ___followBoneRotation_5;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followLocalScale
	bool ___followLocalScale_7;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followZPosition
	bool ___followZPosition_8;
	// Spine.Bone Spine.Unity.BoneFollowerGraphic::bone
	Bone_t1086356328 * ___bone_9;
	// UnityEngine.Transform Spine.Unity.BoneFollowerGraphic::skeletonTransform
	Transform_t3600365921 * ___skeletonTransform_10;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_11;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::valid
	bool ___valid_12;

public:
	inline static int32_t get_offset_of_skeletonGraphic_2() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___skeletonGraphic_2)); }
	inline SkeletonGraphic_t1744877482 * get_skeletonGraphic_2() const { return ___skeletonGraphic_2; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_skeletonGraphic_2() { return &___skeletonGraphic_2; }
	inline void set_skeletonGraphic_2(SkeletonGraphic_t1744877482 * value)
	{
		___skeletonGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonGraphic_2), value);
	}

	inline static int32_t get_offset_of_initializeOnAwake_3() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___initializeOnAwake_3)); }
	inline bool get_initializeOnAwake_3() const { return ___initializeOnAwake_3; }
	inline bool* get_address_of_initializeOnAwake_3() { return &___initializeOnAwake_3; }
	inline void set_initializeOnAwake_3(bool value)
	{
		___initializeOnAwake_3 = value;
	}

	inline static int32_t get_offset_of_boneName_4() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___boneName_4)); }
	inline String_t* get_boneName_4() const { return ___boneName_4; }
	inline String_t** get_address_of_boneName_4() { return &___boneName_4; }
	inline void set_boneName_4(String_t* value)
	{
		___boneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_4), value);
	}

	inline static int32_t get_offset_of_followBoneRotation_5() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followBoneRotation_5)); }
	inline bool get_followBoneRotation_5() const { return ___followBoneRotation_5; }
	inline bool* get_address_of_followBoneRotation_5() { return &___followBoneRotation_5; }
	inline void set_followBoneRotation_5(bool value)
	{
		___followBoneRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_7() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followLocalScale_7)); }
	inline bool get_followLocalScale_7() const { return ___followLocalScale_7; }
	inline bool* get_address_of_followLocalScale_7() { return &___followLocalScale_7; }
	inline void set_followLocalScale_7(bool value)
	{
		___followLocalScale_7 = value;
	}

	inline static int32_t get_offset_of_followZPosition_8() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___followZPosition_8)); }
	inline bool get_followZPosition_8() const { return ___followZPosition_8; }
	inline bool* get_address_of_followZPosition_8() { return &___followZPosition_8; }
	inline void set_followZPosition_8(bool value)
	{
		___followZPosition_8 = value;
	}

	inline static int32_t get_offset_of_bone_9() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___bone_9)); }
	inline Bone_t1086356328 * get_bone_9() const { return ___bone_9; }
	inline Bone_t1086356328 ** get_address_of_bone_9() { return &___bone_9; }
	inline void set_bone_9(Bone_t1086356328 * value)
	{
		___bone_9 = value;
		Il2CppCodeGenWriteBarrier((&___bone_9), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_10() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___skeletonTransform_10)); }
	inline Transform_t3600365921 * get_skeletonTransform_10() const { return ___skeletonTransform_10; }
	inline Transform_t3600365921 ** get_address_of_skeletonTransform_10() { return &___skeletonTransform_10; }
	inline void set_skeletonTransform_10(Transform_t3600365921 * value)
	{
		___skeletonTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_11() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___skeletonTransformIsParent_11)); }
	inline bool get_skeletonTransformIsParent_11() const { return ___skeletonTransformIsParent_11; }
	inline bool* get_address_of_skeletonTransformIsParent_11() { return &___skeletonTransformIsParent_11; }
	inline void set_skeletonTransformIsParent_11(bool value)
	{
		___skeletonTransformIsParent_11 = value;
	}

	inline static int32_t get_offset_of_valid_12() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t46362405, ___valid_12)); }
	inline bool get_valid_12() const { return ___valid_12; }
	inline bool* get_address_of_valid_12() { return &___valid_12; }
	inline void set_valid_12(bool value)
	{
		___valid_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWERGRAPHIC_T46362405_H
#ifndef SKELETONGHOSTRENDERER_T2445315009_H
#define SKELETONGHOSTRENDERER_T2445315009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer
struct  SkeletonGhostRenderer_t2445315009  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Spine.Unity.Modules.SkeletonGhostRenderer::fadeSpeed
	float ___fadeSpeed_2;
	// UnityEngine.Color32[] Spine.Unity.Modules.SkeletonGhostRenderer::colors
	Color32U5BU5D_t3850468773* ___colors_3;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer::black
	Color32_t2600501292  ___black_4;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhostRenderer::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_5;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhostRenderer::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_6;

public:
	inline static int32_t get_offset_of_fadeSpeed_2() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___fadeSpeed_2)); }
	inline float get_fadeSpeed_2() const { return ___fadeSpeed_2; }
	inline float* get_address_of_fadeSpeed_2() { return &___fadeSpeed_2; }
	inline void set_fadeSpeed_2(float value)
	{
		___fadeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_colors_3() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___colors_3)); }
	inline Color32U5BU5D_t3850468773* get_colors_3() const { return ___colors_3; }
	inline Color32U5BU5D_t3850468773** get_address_of_colors_3() { return &___colors_3; }
	inline void set_colors_3(Color32U5BU5D_t3850468773* value)
	{
		___colors_3 = value;
		Il2CppCodeGenWriteBarrier((&___colors_3), value);
	}

	inline static int32_t get_offset_of_black_4() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___black_4)); }
	inline Color32_t2600501292  get_black_4() const { return ___black_4; }
	inline Color32_t2600501292 * get_address_of_black_4() { return &___black_4; }
	inline void set_black_4(Color32_t2600501292  value)
	{
		___black_4 = value;
	}

	inline static int32_t get_offset_of_meshFilter_5() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___meshFilter_5)); }
	inline MeshFilter_t3523625662 * get_meshFilter_5() const { return ___meshFilter_5; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_5() { return &___meshFilter_5; }
	inline void set_meshFilter_5(MeshFilter_t3523625662 * value)
	{
		___meshFilter_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_5), value);
	}

	inline static int32_t get_offset_of_meshRenderer_6() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t2445315009, ___meshRenderer_6)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_6() const { return ___meshRenderer_6; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_6() { return &___meshRenderer_6; }
	inline void set_meshRenderer_6(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOSTRENDERER_T2445315009_H
#ifndef SKELETONRAGDOLL2D_T2972821364_H
#define SKELETONRAGDOLL2D_T2972821364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D
struct  SkeletonRagdoll2D_t2972821364  : public MonoBehaviour_t3962482529
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll2D::startingBoneName
	String_t* ___startingBoneName_3;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll2D::stopBoneNames
	List_1_t3319525431 * ___stopBoneNames_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::applyOnStart
	bool ___applyOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableIK
	bool ___disableIK_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableOtherConstraints
	bool ___disableOtherConstraints_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::pinStartBone
	bool ___pinStartBone_8;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::gravityScale
	float ___gravityScale_9;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::thickness
	float ___thickness_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rotationLimit
	float ___rotationLimit_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rootMass
	float ___rootMass_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::massFalloffFactor
	float ___massFalloffFactor_13;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D::colliderLayer
	int32_t ___colliderLayer_14;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::mix
	float ___mix_15;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll2D::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_16;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll2D::skeleton
	Skeleton_t3686076450 * ___skeleton_17;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll2D::boneTable
	Dictionary_2_t2478265129 * ___boneTable_18;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::ragdollRoot
	Transform_t3600365921 * ___ragdollRoot_19;
	// UnityEngine.Rigidbody2D Spine.Unity.Modules.SkeletonRagdoll2D::<RootRigidbody>k__BackingField
	Rigidbody2D_t939494601 * ___U3CRootRigidbodyU3Ek__BackingField_20;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll2D::<StartingBone>k__BackingField
	Bone_t1086356328 * ___U3CStartingBoneU3Ek__BackingField_21;
	// UnityEngine.Vector2 Spine.Unity.Modules.SkeletonRagdoll2D::rootOffset
	Vector2_t2156229523  ___rootOffset_22;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::isActive
	bool ___isActive_23;

public:
	inline static int32_t get_offset_of_startingBoneName_3() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___startingBoneName_3)); }
	inline String_t* get_startingBoneName_3() const { return ___startingBoneName_3; }
	inline String_t** get_address_of_startingBoneName_3() { return &___startingBoneName_3; }
	inline void set_startingBoneName_3(String_t* value)
	{
		___startingBoneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_3), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___stopBoneNames_4)); }
	inline List_1_t3319525431 * get_stopBoneNames_4() const { return ___stopBoneNames_4; }
	inline List_1_t3319525431 ** get_address_of_stopBoneNames_4() { return &___stopBoneNames_4; }
	inline void set_stopBoneNames_4(List_1_t3319525431 * value)
	{
		___stopBoneNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_4), value);
	}

	inline static int32_t get_offset_of_applyOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___applyOnStart_5)); }
	inline bool get_applyOnStart_5() const { return ___applyOnStart_5; }
	inline bool* get_address_of_applyOnStart_5() { return &___applyOnStart_5; }
	inline void set_applyOnStart_5(bool value)
	{
		___applyOnStart_5 = value;
	}

	inline static int32_t get_offset_of_disableIK_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___disableIK_6)); }
	inline bool get_disableIK_6() const { return ___disableIK_6; }
	inline bool* get_address_of_disableIK_6() { return &___disableIK_6; }
	inline void set_disableIK_6(bool value)
	{
		___disableIK_6 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___disableOtherConstraints_7)); }
	inline bool get_disableOtherConstraints_7() const { return ___disableOtherConstraints_7; }
	inline bool* get_address_of_disableOtherConstraints_7() { return &___disableOtherConstraints_7; }
	inline void set_disableOtherConstraints_7(bool value)
	{
		___disableOtherConstraints_7 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___pinStartBone_8)); }
	inline bool get_pinStartBone_8() const { return ___pinStartBone_8; }
	inline bool* get_address_of_pinStartBone_8() { return &___pinStartBone_8; }
	inline void set_pinStartBone_8(bool value)
	{
		___pinStartBone_8 = value;
	}

	inline static int32_t get_offset_of_gravityScale_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___gravityScale_9)); }
	inline float get_gravityScale_9() const { return ___gravityScale_9; }
	inline float* get_address_of_gravityScale_9() { return &___gravityScale_9; }
	inline void set_gravityScale_9(float value)
	{
		___gravityScale_9 = value;
	}

	inline static int32_t get_offset_of_thickness_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___thickness_10)); }
	inline float get_thickness_10() const { return ___thickness_10; }
	inline float* get_address_of_thickness_10() { return &___thickness_10; }
	inline void set_thickness_10(float value)
	{
		___thickness_10 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___rotationLimit_11)); }
	inline float get_rotationLimit_11() const { return ___rotationLimit_11; }
	inline float* get_address_of_rotationLimit_11() { return &___rotationLimit_11; }
	inline void set_rotationLimit_11(float value)
	{
		___rotationLimit_11 = value;
	}

	inline static int32_t get_offset_of_rootMass_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___rootMass_12)); }
	inline float get_rootMass_12() const { return ___rootMass_12; }
	inline float* get_address_of_rootMass_12() { return &___rootMass_12; }
	inline void set_rootMass_12(float value)
	{
		___rootMass_12 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___massFalloffFactor_13)); }
	inline float get_massFalloffFactor_13() const { return ___massFalloffFactor_13; }
	inline float* get_address_of_massFalloffFactor_13() { return &___massFalloffFactor_13; }
	inline void set_massFalloffFactor_13(float value)
	{
		___massFalloffFactor_13 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___colliderLayer_14)); }
	inline int32_t get_colliderLayer_14() const { return ___colliderLayer_14; }
	inline int32_t* get_address_of_colliderLayer_14() { return &___colliderLayer_14; }
	inline void set_colliderLayer_14(int32_t value)
	{
		___colliderLayer_14 = value;
	}

	inline static int32_t get_offset_of_mix_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___mix_15)); }
	inline float get_mix_15() const { return ___mix_15; }
	inline float* get_address_of_mix_15() { return &___mix_15; }
	inline void set_mix_15(float value)
	{
		___mix_15 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___targetSkeletonComponent_16)); }
	inline RuntimeObject* get_targetSkeletonComponent_16() const { return ___targetSkeletonComponent_16; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_16() { return &___targetSkeletonComponent_16; }
	inline void set_targetSkeletonComponent_16(RuntimeObject* value)
	{
		___targetSkeletonComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_16), value);
	}

	inline static int32_t get_offset_of_skeleton_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___skeleton_17)); }
	inline Skeleton_t3686076450 * get_skeleton_17() const { return ___skeleton_17; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_17() { return &___skeleton_17; }
	inline void set_skeleton_17(Skeleton_t3686076450 * value)
	{
		___skeleton_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_17), value);
	}

	inline static int32_t get_offset_of_boneTable_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___boneTable_18)); }
	inline Dictionary_2_t2478265129 * get_boneTable_18() const { return ___boneTable_18; }
	inline Dictionary_2_t2478265129 ** get_address_of_boneTable_18() { return &___boneTable_18; }
	inline void set_boneTable_18(Dictionary_2_t2478265129 * value)
	{
		___boneTable_18 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_18), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___ragdollRoot_19)); }
	inline Transform_t3600365921 * get_ragdollRoot_19() const { return ___ragdollRoot_19; }
	inline Transform_t3600365921 ** get_address_of_ragdollRoot_19() { return &___ragdollRoot_19; }
	inline void set_ragdollRoot_19(Transform_t3600365921 * value)
	{
		___ragdollRoot_19 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_19), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___U3CRootRigidbodyU3Ek__BackingField_20)); }
	inline Rigidbody2D_t939494601 * get_U3CRootRigidbodyU3Ek__BackingField_20() const { return ___U3CRootRigidbodyU3Ek__BackingField_20; }
	inline Rigidbody2D_t939494601 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_20() { return &___U3CRootRigidbodyU3Ek__BackingField_20; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_20(Rigidbody2D_t939494601 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___U3CStartingBoneU3Ek__BackingField_21)); }
	inline Bone_t1086356328 * get_U3CStartingBoneU3Ek__BackingField_21() const { return ___U3CStartingBoneU3Ek__BackingField_21; }
	inline Bone_t1086356328 ** get_address_of_U3CStartingBoneU3Ek__BackingField_21() { return &___U3CStartingBoneU3Ek__BackingField_21; }
	inline void set_U3CStartingBoneU3Ek__BackingField_21(Bone_t1086356328 * value)
	{
		___U3CStartingBoneU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_rootOffset_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___rootOffset_22)); }
	inline Vector2_t2156229523  get_rootOffset_22() const { return ___rootOffset_22; }
	inline Vector2_t2156229523 * get_address_of_rootOffset_22() { return &___rootOffset_22; }
	inline void set_rootOffset_22(Vector2_t2156229523  value)
	{
		___rootOffset_22 = value;
	}

	inline static int32_t get_offset_of_isActive_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364, ___isActive_23)); }
	inline bool get_isActive_23() const { return ___isActive_23; }
	inline bool* get_address_of_isActive_23() { return &___isActive_23; }
	inline void set_isActive_23(bool value)
	{
		___isActive_23 = value;
	}
};

struct SkeletonRagdoll2D_t2972821364_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::parentSpaceHelper
	Transform_t3600365921 * ___parentSpaceHelper_2;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_2() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t2972821364_StaticFields, ___parentSpaceHelper_2)); }
	inline Transform_t3600365921 * get_parentSpaceHelper_2() const { return ___parentSpaceHelper_2; }
	inline Transform_t3600365921 ** get_address_of_parentSpaceHelper_2() { return &___parentSpaceHelper_2; }
	inline void set_parentSpaceHelper_2(Transform_t3600365921 * value)
	{
		___parentSpaceHelper_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL2D_T2972821364_H
#ifndef SKELETONRAGDOLL_T480923817_H
#define SKELETONRAGDOLL_T480923817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll
struct  SkeletonRagdoll_t480923817  : public MonoBehaviour_t3962482529
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll::startingBoneName
	String_t* ___startingBoneName_3;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll::stopBoneNames
	List_1_t3319525431 * ___stopBoneNames_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::applyOnStart
	bool ___applyOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableIK
	bool ___disableIK_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableOtherConstraints
	bool ___disableOtherConstraints_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::pinStartBone
	bool ___pinStartBone_8;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::enableJointCollision
	bool ___enableJointCollision_9;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::useGravity
	bool ___useGravity_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::thickness
	float ___thickness_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rotationLimit
	float ___rotationLimit_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rootMass
	float ___rootMass_13;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::massFalloffFactor
	float ___massFalloffFactor_14;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll::colliderLayer
	int32_t ___colliderLayer_15;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::mix
	float ___mix_16;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_17;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll::skeleton
	Skeleton_t3686076450 * ___skeleton_18;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll::boneTable
	Dictionary_2_t2478265129 * ___boneTable_19;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::ragdollRoot
	Transform_t3600365921 * ___ragdollRoot_20;
	// UnityEngine.Rigidbody Spine.Unity.Modules.SkeletonRagdoll::<RootRigidbody>k__BackingField
	Rigidbody_t3916780224 * ___U3CRootRigidbodyU3Ek__BackingField_21;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll::<StartingBone>k__BackingField
	Bone_t1086356328 * ___U3CStartingBoneU3Ek__BackingField_22;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonRagdoll::rootOffset
	Vector3_t3722313464  ___rootOffset_23;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::isActive
	bool ___isActive_24;

public:
	inline static int32_t get_offset_of_startingBoneName_3() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___startingBoneName_3)); }
	inline String_t* get_startingBoneName_3() const { return ___startingBoneName_3; }
	inline String_t** get_address_of_startingBoneName_3() { return &___startingBoneName_3; }
	inline void set_startingBoneName_3(String_t* value)
	{
		___startingBoneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_3), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___stopBoneNames_4)); }
	inline List_1_t3319525431 * get_stopBoneNames_4() const { return ___stopBoneNames_4; }
	inline List_1_t3319525431 ** get_address_of_stopBoneNames_4() { return &___stopBoneNames_4; }
	inline void set_stopBoneNames_4(List_1_t3319525431 * value)
	{
		___stopBoneNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_4), value);
	}

	inline static int32_t get_offset_of_applyOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___applyOnStart_5)); }
	inline bool get_applyOnStart_5() const { return ___applyOnStart_5; }
	inline bool* get_address_of_applyOnStart_5() { return &___applyOnStart_5; }
	inline void set_applyOnStart_5(bool value)
	{
		___applyOnStart_5 = value;
	}

	inline static int32_t get_offset_of_disableIK_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___disableIK_6)); }
	inline bool get_disableIK_6() const { return ___disableIK_6; }
	inline bool* get_address_of_disableIK_6() { return &___disableIK_6; }
	inline void set_disableIK_6(bool value)
	{
		___disableIK_6 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___disableOtherConstraints_7)); }
	inline bool get_disableOtherConstraints_7() const { return ___disableOtherConstraints_7; }
	inline bool* get_address_of_disableOtherConstraints_7() { return &___disableOtherConstraints_7; }
	inline void set_disableOtherConstraints_7(bool value)
	{
		___disableOtherConstraints_7 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___pinStartBone_8)); }
	inline bool get_pinStartBone_8() const { return ___pinStartBone_8; }
	inline bool* get_address_of_pinStartBone_8() { return &___pinStartBone_8; }
	inline void set_pinStartBone_8(bool value)
	{
		___pinStartBone_8 = value;
	}

	inline static int32_t get_offset_of_enableJointCollision_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___enableJointCollision_9)); }
	inline bool get_enableJointCollision_9() const { return ___enableJointCollision_9; }
	inline bool* get_address_of_enableJointCollision_9() { return &___enableJointCollision_9; }
	inline void set_enableJointCollision_9(bool value)
	{
		___enableJointCollision_9 = value;
	}

	inline static int32_t get_offset_of_useGravity_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___useGravity_10)); }
	inline bool get_useGravity_10() const { return ___useGravity_10; }
	inline bool* get_address_of_useGravity_10() { return &___useGravity_10; }
	inline void set_useGravity_10(bool value)
	{
		___useGravity_10 = value;
	}

	inline static int32_t get_offset_of_thickness_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___thickness_11)); }
	inline float get_thickness_11() const { return ___thickness_11; }
	inline float* get_address_of_thickness_11() { return &___thickness_11; }
	inline void set_thickness_11(float value)
	{
		___thickness_11 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___rotationLimit_12)); }
	inline float get_rotationLimit_12() const { return ___rotationLimit_12; }
	inline float* get_address_of_rotationLimit_12() { return &___rotationLimit_12; }
	inline void set_rotationLimit_12(float value)
	{
		___rotationLimit_12 = value;
	}

	inline static int32_t get_offset_of_rootMass_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___rootMass_13)); }
	inline float get_rootMass_13() const { return ___rootMass_13; }
	inline float* get_address_of_rootMass_13() { return &___rootMass_13; }
	inline void set_rootMass_13(float value)
	{
		___rootMass_13 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___massFalloffFactor_14)); }
	inline float get_massFalloffFactor_14() const { return ___massFalloffFactor_14; }
	inline float* get_address_of_massFalloffFactor_14() { return &___massFalloffFactor_14; }
	inline void set_massFalloffFactor_14(float value)
	{
		___massFalloffFactor_14 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___colliderLayer_15)); }
	inline int32_t get_colliderLayer_15() const { return ___colliderLayer_15; }
	inline int32_t* get_address_of_colliderLayer_15() { return &___colliderLayer_15; }
	inline void set_colliderLayer_15(int32_t value)
	{
		___colliderLayer_15 = value;
	}

	inline static int32_t get_offset_of_mix_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___mix_16)); }
	inline float get_mix_16() const { return ___mix_16; }
	inline float* get_address_of_mix_16() { return &___mix_16; }
	inline void set_mix_16(float value)
	{
		___mix_16 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___targetSkeletonComponent_17)); }
	inline RuntimeObject* get_targetSkeletonComponent_17() const { return ___targetSkeletonComponent_17; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_17() { return &___targetSkeletonComponent_17; }
	inline void set_targetSkeletonComponent_17(RuntimeObject* value)
	{
		___targetSkeletonComponent_17 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_17), value);
	}

	inline static int32_t get_offset_of_skeleton_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___skeleton_18)); }
	inline Skeleton_t3686076450 * get_skeleton_18() const { return ___skeleton_18; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_18() { return &___skeleton_18; }
	inline void set_skeleton_18(Skeleton_t3686076450 * value)
	{
		___skeleton_18 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_18), value);
	}

	inline static int32_t get_offset_of_boneTable_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___boneTable_19)); }
	inline Dictionary_2_t2478265129 * get_boneTable_19() const { return ___boneTable_19; }
	inline Dictionary_2_t2478265129 ** get_address_of_boneTable_19() { return &___boneTable_19; }
	inline void set_boneTable_19(Dictionary_2_t2478265129 * value)
	{
		___boneTable_19 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_19), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___ragdollRoot_20)); }
	inline Transform_t3600365921 * get_ragdollRoot_20() const { return ___ragdollRoot_20; }
	inline Transform_t3600365921 ** get_address_of_ragdollRoot_20() { return &___ragdollRoot_20; }
	inline void set_ragdollRoot_20(Transform_t3600365921 * value)
	{
		___ragdollRoot_20 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_20), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___U3CRootRigidbodyU3Ek__BackingField_21)); }
	inline Rigidbody_t3916780224 * get_U3CRootRigidbodyU3Ek__BackingField_21() const { return ___U3CRootRigidbodyU3Ek__BackingField_21; }
	inline Rigidbody_t3916780224 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_21() { return &___U3CRootRigidbodyU3Ek__BackingField_21; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_21(Rigidbody_t3916780224 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___U3CStartingBoneU3Ek__BackingField_22)); }
	inline Bone_t1086356328 * get_U3CStartingBoneU3Ek__BackingField_22() const { return ___U3CStartingBoneU3Ek__BackingField_22; }
	inline Bone_t1086356328 ** get_address_of_U3CStartingBoneU3Ek__BackingField_22() { return &___U3CStartingBoneU3Ek__BackingField_22; }
	inline void set_U3CStartingBoneU3Ek__BackingField_22(Bone_t1086356328 * value)
	{
		___U3CStartingBoneU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_rootOffset_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___rootOffset_23)); }
	inline Vector3_t3722313464  get_rootOffset_23() const { return ___rootOffset_23; }
	inline Vector3_t3722313464 * get_address_of_rootOffset_23() { return &___rootOffset_23; }
	inline void set_rootOffset_23(Vector3_t3722313464  value)
	{
		___rootOffset_23 = value;
	}

	inline static int32_t get_offset_of_isActive_24() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817, ___isActive_24)); }
	inline bool get_isActive_24() const { return ___isActive_24; }
	inline bool* get_address_of_isActive_24() { return &___isActive_24; }
	inline void set_isActive_24(bool value)
	{
		___isActive_24 = value;
	}
};

struct SkeletonRagdoll_t480923817_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::parentSpaceHelper
	Transform_t3600365921 * ___parentSpaceHelper_2;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_2() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t480923817_StaticFields, ___parentSpaceHelper_2)); }
	inline Transform_t3600365921 * get_parentSpaceHelper_2() const { return ___parentSpaceHelper_2; }
	inline Transform_t3600365921 ** get_address_of_parentSpaceHelper_2() { return &___parentSpaceHelper_2; }
	inline void set_parentSpaceHelper_2(Transform_t3600365921 * value)
	{
		___parentSpaceHelper_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL_T480923817_H
#ifndef SKELETONGHOST_T1898327037_H
#define SKELETONGHOST_T1898327037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhost
struct  SkeletonGhost_t1898327037  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::ghostingEnabled
	bool ___ghostingEnabled_4;
	// System.Single Spine.Unity.Modules.SkeletonGhost::spawnRate
	float ___spawnRate_5;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhost::color
	Color32_t2600501292  ___color_6;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::additive
	bool ___additive_7;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::maximumGhosts
	int32_t ___maximumGhosts_8;
	// System.Single Spine.Unity.Modules.SkeletonGhost::fadeSpeed
	float ___fadeSpeed_9;
	// UnityEngine.Shader Spine.Unity.Modules.SkeletonGhost::ghostShader
	Shader_t4151988712 * ___ghostShader_10;
	// System.Single Spine.Unity.Modules.SkeletonGhost::textureFade
	float ___textureFade_11;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::sortWithDistanceOnly
	bool ___sortWithDistanceOnly_12;
	// System.Single Spine.Unity.Modules.SkeletonGhost::zOffset
	float ___zOffset_13;
	// System.Single Spine.Unity.Modules.SkeletonGhost::nextSpawnTime
	float ___nextSpawnTime_14;
	// Spine.Unity.Modules.SkeletonGhostRenderer[] Spine.Unity.Modules.SkeletonGhost::pool
	SkeletonGhostRendererU5BU5D_t513912860* ___pool_15;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::poolIndex
	int32_t ___poolIndex_16;
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonGhost::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_17;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhost::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_18;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhost::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_19;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.Modules.SkeletonGhost::materialTable
	Dictionary_2_t3700682020 * ___materialTable_20;

public:
	inline static int32_t get_offset_of_ghostingEnabled_4() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___ghostingEnabled_4)); }
	inline bool get_ghostingEnabled_4() const { return ___ghostingEnabled_4; }
	inline bool* get_address_of_ghostingEnabled_4() { return &___ghostingEnabled_4; }
	inline void set_ghostingEnabled_4(bool value)
	{
		___ghostingEnabled_4 = value;
	}

	inline static int32_t get_offset_of_spawnRate_5() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___spawnRate_5)); }
	inline float get_spawnRate_5() const { return ___spawnRate_5; }
	inline float* get_address_of_spawnRate_5() { return &___spawnRate_5; }
	inline void set_spawnRate_5(float value)
	{
		___spawnRate_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___color_6)); }
	inline Color32_t2600501292  get_color_6() const { return ___color_6; }
	inline Color32_t2600501292 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Color32_t2600501292  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of_additive_7() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___additive_7)); }
	inline bool get_additive_7() const { return ___additive_7; }
	inline bool* get_address_of_additive_7() { return &___additive_7; }
	inline void set_additive_7(bool value)
	{
		___additive_7 = value;
	}

	inline static int32_t get_offset_of_maximumGhosts_8() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___maximumGhosts_8)); }
	inline int32_t get_maximumGhosts_8() const { return ___maximumGhosts_8; }
	inline int32_t* get_address_of_maximumGhosts_8() { return &___maximumGhosts_8; }
	inline void set_maximumGhosts_8(int32_t value)
	{
		___maximumGhosts_8 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_9() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___fadeSpeed_9)); }
	inline float get_fadeSpeed_9() const { return ___fadeSpeed_9; }
	inline float* get_address_of_fadeSpeed_9() { return &___fadeSpeed_9; }
	inline void set_fadeSpeed_9(float value)
	{
		___fadeSpeed_9 = value;
	}

	inline static int32_t get_offset_of_ghostShader_10() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___ghostShader_10)); }
	inline Shader_t4151988712 * get_ghostShader_10() const { return ___ghostShader_10; }
	inline Shader_t4151988712 ** get_address_of_ghostShader_10() { return &___ghostShader_10; }
	inline void set_ghostShader_10(Shader_t4151988712 * value)
	{
		___ghostShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___ghostShader_10), value);
	}

	inline static int32_t get_offset_of_textureFade_11() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___textureFade_11)); }
	inline float get_textureFade_11() const { return ___textureFade_11; }
	inline float* get_address_of_textureFade_11() { return &___textureFade_11; }
	inline void set_textureFade_11(float value)
	{
		___textureFade_11 = value;
	}

	inline static int32_t get_offset_of_sortWithDistanceOnly_12() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___sortWithDistanceOnly_12)); }
	inline bool get_sortWithDistanceOnly_12() const { return ___sortWithDistanceOnly_12; }
	inline bool* get_address_of_sortWithDistanceOnly_12() { return &___sortWithDistanceOnly_12; }
	inline void set_sortWithDistanceOnly_12(bool value)
	{
		___sortWithDistanceOnly_12 = value;
	}

	inline static int32_t get_offset_of_zOffset_13() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___zOffset_13)); }
	inline float get_zOffset_13() const { return ___zOffset_13; }
	inline float* get_address_of_zOffset_13() { return &___zOffset_13; }
	inline void set_zOffset_13(float value)
	{
		___zOffset_13 = value;
	}

	inline static int32_t get_offset_of_nextSpawnTime_14() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___nextSpawnTime_14)); }
	inline float get_nextSpawnTime_14() const { return ___nextSpawnTime_14; }
	inline float* get_address_of_nextSpawnTime_14() { return &___nextSpawnTime_14; }
	inline void set_nextSpawnTime_14(float value)
	{
		___nextSpawnTime_14 = value;
	}

	inline static int32_t get_offset_of_pool_15() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___pool_15)); }
	inline SkeletonGhostRendererU5BU5D_t513912860* get_pool_15() const { return ___pool_15; }
	inline SkeletonGhostRendererU5BU5D_t513912860** get_address_of_pool_15() { return &___pool_15; }
	inline void set_pool_15(SkeletonGhostRendererU5BU5D_t513912860* value)
	{
		___pool_15 = value;
		Il2CppCodeGenWriteBarrier((&___pool_15), value);
	}

	inline static int32_t get_offset_of_poolIndex_16() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___poolIndex_16)); }
	inline int32_t get_poolIndex_16() const { return ___poolIndex_16; }
	inline int32_t* get_address_of_poolIndex_16() { return &___poolIndex_16; }
	inline void set_poolIndex_16(int32_t value)
	{
		___poolIndex_16 = value;
	}

	inline static int32_t get_offset_of_skeletonRenderer_17() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___skeletonRenderer_17)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_17() const { return ___skeletonRenderer_17; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_17() { return &___skeletonRenderer_17; }
	inline void set_skeletonRenderer_17(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_17), value);
	}

	inline static int32_t get_offset_of_meshRenderer_18() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___meshRenderer_18)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_18() const { return ___meshRenderer_18; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_18() { return &___meshRenderer_18; }
	inline void set_meshRenderer_18(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_18), value);
	}

	inline static int32_t get_offset_of_meshFilter_19() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___meshFilter_19)); }
	inline MeshFilter_t3523625662 * get_meshFilter_19() const { return ___meshFilter_19; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_19() { return &___meshFilter_19; }
	inline void set_meshFilter_19(MeshFilter_t3523625662 * value)
	{
		___meshFilter_19 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_19), value);
	}

	inline static int32_t get_offset_of_materialTable_20() { return static_cast<int32_t>(offsetof(SkeletonGhost_t1898327037, ___materialTable_20)); }
	inline Dictionary_2_t3700682020 * get_materialTable_20() const { return ___materialTable_20; }
	inline Dictionary_2_t3700682020 ** get_address_of_materialTable_20() { return &___materialTable_20; }
	inline void set_materialTable_20(Dictionary_2_t3700682020 * value)
	{
		___materialTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOST_T1898327037_H
#ifndef SKELETONRENDERERCUSTOMMATERIALS_T1555313811_H
#define SKELETONRENDERERCUSTOMMATERIALS_T1555313811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials
struct  SkeletonRendererCustomMaterials_t1555313811  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRendererCustomMaterials::skeletonRenderer
	SkeletonRenderer_t2098681813 * ___skeletonRenderer_2;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customSlotMaterials
	List_1_t2474053923 * ___customSlotMaterials_3;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customMaterialOverrides
	List_1_t3907116131 * ___customMaterialOverrides_4;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t1555313811, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t2098681813 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t2098681813 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t2098681813 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_3() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t1555313811, ___customSlotMaterials_3)); }
	inline List_1_t2474053923 * get_customSlotMaterials_3() const { return ___customSlotMaterials_3; }
	inline List_1_t2474053923 ** get_address_of_customSlotMaterials_3() { return &___customSlotMaterials_3; }
	inline void set_customSlotMaterials_3(List_1_t2474053923 * value)
	{
		___customSlotMaterials_3 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_3), value);
	}

	inline static int32_t get_offset_of_customMaterialOverrides_4() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t1555313811, ___customMaterialOverrides_4)); }
	inline List_1_t3907116131 * get_customMaterialOverrides_4() const { return ___customMaterialOverrides_4; }
	inline List_1_t3907116131 ** get_address_of_customMaterialOverrides_4() { return &___customMaterialOverrides_4; }
	inline void set_customMaterialOverrides_4(List_1_t3907116131 * value)
	{
		___customMaterialOverrides_4 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverrides_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERCUSTOMMATERIALS_T1555313811_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SKELETONANIMATOR_T1073737811_H
#define SKELETONANIMATOR_T1073737811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator
struct  SkeletonAnimator_t1073737811  : public SkeletonRenderer_t2098681813
{
public:
	// Spine.Unity.SkeletonAnimator/MecanimTranslator Spine.Unity.SkeletonAnimator::translator
	MecanimTranslator_t2363469064 * ___translator_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateLocal
	UpdateBonesDelegate_t735903178 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateWorld
	UpdateBonesDelegate_t735903178 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateComplete
	UpdateBonesDelegate_t735903178 * ____UpdateComplete_34;

public:
	inline static int32_t get_offset_of_translator_31() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ___translator_31)); }
	inline MecanimTranslator_t2363469064 * get_translator_31() const { return ___translator_31; }
	inline MecanimTranslator_t2363469064 ** get_address_of_translator_31() { return &___translator_31; }
	inline void set_translator_31(MecanimTranslator_t2363469064 * value)
	{
		___translator_31 = value;
		Il2CppCodeGenWriteBarrier((&___translator_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t1073737811, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATOR_T1073737811_H
#ifndef SKELETONUTILITYGROUNDCONSTRAINT_T1570100391_H
#define SKELETONUTILITYGROUNDCONSTRAINT_T1570100391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityGroundConstraint
struct  SkeletonUtilityGroundConstraint_t1570100391  : public SkeletonUtilityConstraint_t1014382506
{
public:
	// UnityEngine.LayerMask Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundMask
	LayerMask_t3493934918  ___groundMask_4;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::use2D
	bool ___use2D_5;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::useRadius
	bool ___useRadius_6;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castRadius
	float ___castRadius_7;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castDistance
	float ___castDistance_8;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castOffset
	float ___castOffset_9;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundOffset
	float ___groundOffset_10;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::adjustSpeed
	float ___adjustSpeed_11;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayOrigin
	Vector3_t3722313464  ___rayOrigin_12;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayDir
	Vector3_t3722313464  ___rayDir_13;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::hitY
	float ___hitY_14;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::lastHitY
	float ___lastHitY_15;

public:
	inline static int32_t get_offset_of_groundMask_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___groundMask_4)); }
	inline LayerMask_t3493934918  get_groundMask_4() const { return ___groundMask_4; }
	inline LayerMask_t3493934918 * get_address_of_groundMask_4() { return &___groundMask_4; }
	inline void set_groundMask_4(LayerMask_t3493934918  value)
	{
		___groundMask_4 = value;
	}

	inline static int32_t get_offset_of_use2D_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___use2D_5)); }
	inline bool get_use2D_5() const { return ___use2D_5; }
	inline bool* get_address_of_use2D_5() { return &___use2D_5; }
	inline void set_use2D_5(bool value)
	{
		___use2D_5 = value;
	}

	inline static int32_t get_offset_of_useRadius_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___useRadius_6)); }
	inline bool get_useRadius_6() const { return ___useRadius_6; }
	inline bool* get_address_of_useRadius_6() { return &___useRadius_6; }
	inline void set_useRadius_6(bool value)
	{
		___useRadius_6 = value;
	}

	inline static int32_t get_offset_of_castRadius_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___castRadius_7)); }
	inline float get_castRadius_7() const { return ___castRadius_7; }
	inline float* get_address_of_castRadius_7() { return &___castRadius_7; }
	inline void set_castRadius_7(float value)
	{
		___castRadius_7 = value;
	}

	inline static int32_t get_offset_of_castDistance_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___castDistance_8)); }
	inline float get_castDistance_8() const { return ___castDistance_8; }
	inline float* get_address_of_castDistance_8() { return &___castDistance_8; }
	inline void set_castDistance_8(float value)
	{
		___castDistance_8 = value;
	}

	inline static int32_t get_offset_of_castOffset_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___castOffset_9)); }
	inline float get_castOffset_9() const { return ___castOffset_9; }
	inline float* get_address_of_castOffset_9() { return &___castOffset_9; }
	inline void set_castOffset_9(float value)
	{
		___castOffset_9 = value;
	}

	inline static int32_t get_offset_of_groundOffset_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___groundOffset_10)); }
	inline float get_groundOffset_10() const { return ___groundOffset_10; }
	inline float* get_address_of_groundOffset_10() { return &___groundOffset_10; }
	inline void set_groundOffset_10(float value)
	{
		___groundOffset_10 = value;
	}

	inline static int32_t get_offset_of_adjustSpeed_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___adjustSpeed_11)); }
	inline float get_adjustSpeed_11() const { return ___adjustSpeed_11; }
	inline float* get_address_of_adjustSpeed_11() { return &___adjustSpeed_11; }
	inline void set_adjustSpeed_11(float value)
	{
		___adjustSpeed_11 = value;
	}

	inline static int32_t get_offset_of_rayOrigin_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___rayOrigin_12)); }
	inline Vector3_t3722313464  get_rayOrigin_12() const { return ___rayOrigin_12; }
	inline Vector3_t3722313464 * get_address_of_rayOrigin_12() { return &___rayOrigin_12; }
	inline void set_rayOrigin_12(Vector3_t3722313464  value)
	{
		___rayOrigin_12 = value;
	}

	inline static int32_t get_offset_of_rayDir_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___rayDir_13)); }
	inline Vector3_t3722313464  get_rayDir_13() const { return ___rayDir_13; }
	inline Vector3_t3722313464 * get_address_of_rayDir_13() { return &___rayDir_13; }
	inline void set_rayDir_13(Vector3_t3722313464  value)
	{
		___rayDir_13 = value;
	}

	inline static int32_t get_offset_of_hitY_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___hitY_14)); }
	inline float get_hitY_14() const { return ___hitY_14; }
	inline float* get_address_of_hitY_14() { return &___hitY_14; }
	inline void set_hitY_14(float value)
	{
		___hitY_14 = value;
	}

	inline static int32_t get_offset_of_lastHitY_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t1570100391, ___lastHitY_15)); }
	inline float get_lastHitY_15() const { return ___lastHitY_15; }
	inline float* get_address_of_lastHitY_15() { return &___lastHitY_15; }
	inline void set_lastHitY_15(float value)
	{
		___lastHitY_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYGROUNDCONSTRAINT_T1570100391_H
#ifndef SKELETONUTILITYEYECONSTRAINT_T2225743321_H
#define SKELETONUTILITYEYECONSTRAINT_T2225743321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityEyeConstraint
struct  SkeletonUtilityEyeConstraint_t2225743321  : public SkeletonUtilityConstraint_t1014382506
{
public:
	// UnityEngine.Transform[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::eyes
	TransformU5BU5D_t807237628* ___eyes_4;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::radius
	float ___radius_5;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityEyeConstraint::target
	Transform_t3600365921 * ___target_6;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::targetPosition
	Vector3_t3722313464  ___targetPosition_7;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::speed
	float ___speed_8;
	// UnityEngine.Vector3[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::origins
	Vector3U5BU5D_t1718750761* ___origins_9;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::centerPoint
	Vector3_t3722313464  ___centerPoint_10;

public:
	inline static int32_t get_offset_of_eyes_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___eyes_4)); }
	inline TransformU5BU5D_t807237628* get_eyes_4() const { return ___eyes_4; }
	inline TransformU5BU5D_t807237628** get_address_of_eyes_4() { return &___eyes_4; }
	inline void set_eyes_4(TransformU5BU5D_t807237628* value)
	{
		___eyes_4 = value;
		Il2CppCodeGenWriteBarrier((&___eyes_4), value);
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_targetPosition_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___targetPosition_7)); }
	inline Vector3_t3722313464  get_targetPosition_7() const { return ___targetPosition_7; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_7() { return &___targetPosition_7; }
	inline void set_targetPosition_7(Vector3_t3722313464  value)
	{
		___targetPosition_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_origins_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___origins_9)); }
	inline Vector3U5BU5D_t1718750761* get_origins_9() const { return ___origins_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of_origins_9() { return &___origins_9; }
	inline void set_origins_9(Vector3U5BU5D_t1718750761* value)
	{
		___origins_9 = value;
		Il2CppCodeGenWriteBarrier((&___origins_9), value);
	}

	inline static int32_t get_offset_of_centerPoint_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t2225743321, ___centerPoint_10)); }
	inline Vector3_t3722313464  get_centerPoint_10() const { return ___centerPoint_10; }
	inline Vector3_t3722313464 * get_address_of_centerPoint_10() { return &___centerPoint_10; }
	inline void set_centerPoint_10(Vector3_t3722313464  value)
	{
		___centerPoint_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYEYECONSTRAINT_T2225743321_H
#ifndef SKELETONANIMATION_T3693186521_H
#define SKELETONANIMATION_T3693186521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimation
struct  SkeletonAnimation_t3693186521  : public SkeletonRenderer_t2098681813
{
public:
	// Spine.AnimationState Spine.Unity.SkeletonAnimation::state
	AnimationState_t3637309382 * ___state_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateLocal
	UpdateBonesDelegate_t735903178 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateWorld
	UpdateBonesDelegate_t735903178 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateComplete
	UpdateBonesDelegate_t735903178 * ____UpdateComplete_34;
	// System.String Spine.Unity.SkeletonAnimation::_animationName
	String_t* ____animationName_35;
	// System.Boolean Spine.Unity.SkeletonAnimation::loop
	bool ___loop_36;
	// System.Single Spine.Unity.SkeletonAnimation::timeScale
	float ___timeScale_37;

public:
	inline static int32_t get_offset_of_state_31() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ___state_31)); }
	inline AnimationState_t3637309382 * get_state_31() const { return ___state_31; }
	inline AnimationState_t3637309382 ** get_address_of_state_31() { return &___state_31; }
	inline void set_state_31(AnimationState_t3637309382 * value)
	{
		___state_31 = value;
		Il2CppCodeGenWriteBarrier((&___state_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t735903178 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t735903178 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}

	inline static int32_t get_offset_of__animationName_35() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ____animationName_35)); }
	inline String_t* get__animationName_35() const { return ____animationName_35; }
	inline String_t** get_address_of__animationName_35() { return &____animationName_35; }
	inline void set__animationName_35(String_t* value)
	{
		____animationName_35 = value;
		Il2CppCodeGenWriteBarrier((&____animationName_35), value);
	}

	inline static int32_t get_offset_of_loop_36() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ___loop_36)); }
	inline bool get_loop_36() const { return ___loop_36; }
	inline bool* get_address_of_loop_36() { return &___loop_36; }
	inline void set_loop_36(bool value)
	{
		___loop_36 = value;
	}

	inline static int32_t get_offset_of_timeScale_37() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t3693186521, ___timeScale_37)); }
	inline float get_timeScale_37() const { return ___timeScale_37; }
	inline float* get_address_of_timeScale_37() { return &___timeScale_37; }
	inline void set_timeScale_37(float value)
	{
		___timeScale_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATION_T3693186521_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef SKELETONGRAPHIC_T1744877482_H
#define SKELETONGRAPHIC_T1744877482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonGraphic
struct  SkeletonGraphic_t1744877482  : public MaskableGraphic_t3839221559
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonGraphic::skeletonDataAsset
	SkeletonDataAsset_t3748144825 * ___skeletonDataAsset_28;
	// System.String Spine.Unity.SkeletonGraphic::initialSkinName
	String_t* ___initialSkinName_29;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipX
	bool ___initialFlipX_30;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipY
	bool ___initialFlipY_31;
	// System.String Spine.Unity.SkeletonGraphic::startingAnimation
	String_t* ___startingAnimation_32;
	// System.Boolean Spine.Unity.SkeletonGraphic::startingLoop
	bool ___startingLoop_33;
	// System.Single Spine.Unity.SkeletonGraphic::timeScale
	float ___timeScale_34;
	// System.Boolean Spine.Unity.SkeletonGraphic::freeze
	bool ___freeze_35;
	// System.Boolean Spine.Unity.SkeletonGraphic::unscaledTime
	bool ___unscaledTime_36;
	// UnityEngine.Texture Spine.Unity.SkeletonGraphic::overrideTexture
	Texture_t3661962703 * ___overrideTexture_37;
	// Spine.Skeleton Spine.Unity.SkeletonGraphic::skeleton
	Skeleton_t3686076450 * ___skeleton_38;
	// Spine.AnimationState Spine.Unity.SkeletonGraphic::state
	AnimationState_t3637309382 * ___state_39;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonGraphic::meshGenerator
	MeshGenerator_t1354683548 * ___meshGenerator_40;
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.SkeletonGraphic::meshBuffers
	DoubleBuffered_1_t2489386064 * ___meshBuffers_41;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonGraphic::currentInstructions
	SkeletonRendererInstruction_t651787775 * ___currentInstructions_42;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateLocal
	UpdateBonesDelegate_t735903178 * ___UpdateLocal_43;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateWorld
	UpdateBonesDelegate_t735903178 * ___UpdateWorld_44;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateComplete
	UpdateBonesDelegate_t735903178 * ___UpdateComplete_45;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonGraphic::OnPostProcessVertices
	MeshGeneratorDelegate_t1654156803 * ___OnPostProcessVertices_46;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_28() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___skeletonDataAsset_28)); }
	inline SkeletonDataAsset_t3748144825 * get_skeletonDataAsset_28() const { return ___skeletonDataAsset_28; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_skeletonDataAsset_28() { return &___skeletonDataAsset_28; }
	inline void set_skeletonDataAsset_28(SkeletonDataAsset_t3748144825 * value)
	{
		___skeletonDataAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_28), value);
	}

	inline static int32_t get_offset_of_initialSkinName_29() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___initialSkinName_29)); }
	inline String_t* get_initialSkinName_29() const { return ___initialSkinName_29; }
	inline String_t** get_address_of_initialSkinName_29() { return &___initialSkinName_29; }
	inline void set_initialSkinName_29(String_t* value)
	{
		___initialSkinName_29 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_29), value);
	}

	inline static int32_t get_offset_of_initialFlipX_30() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___initialFlipX_30)); }
	inline bool get_initialFlipX_30() const { return ___initialFlipX_30; }
	inline bool* get_address_of_initialFlipX_30() { return &___initialFlipX_30; }
	inline void set_initialFlipX_30(bool value)
	{
		___initialFlipX_30 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_31() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___initialFlipY_31)); }
	inline bool get_initialFlipY_31() const { return ___initialFlipY_31; }
	inline bool* get_address_of_initialFlipY_31() { return &___initialFlipY_31; }
	inline void set_initialFlipY_31(bool value)
	{
		___initialFlipY_31 = value;
	}

	inline static int32_t get_offset_of_startingAnimation_32() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___startingAnimation_32)); }
	inline String_t* get_startingAnimation_32() const { return ___startingAnimation_32; }
	inline String_t** get_address_of_startingAnimation_32() { return &___startingAnimation_32; }
	inline void set_startingAnimation_32(String_t* value)
	{
		___startingAnimation_32 = value;
		Il2CppCodeGenWriteBarrier((&___startingAnimation_32), value);
	}

	inline static int32_t get_offset_of_startingLoop_33() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___startingLoop_33)); }
	inline bool get_startingLoop_33() const { return ___startingLoop_33; }
	inline bool* get_address_of_startingLoop_33() { return &___startingLoop_33; }
	inline void set_startingLoop_33(bool value)
	{
		___startingLoop_33 = value;
	}

	inline static int32_t get_offset_of_timeScale_34() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___timeScale_34)); }
	inline float get_timeScale_34() const { return ___timeScale_34; }
	inline float* get_address_of_timeScale_34() { return &___timeScale_34; }
	inline void set_timeScale_34(float value)
	{
		___timeScale_34 = value;
	}

	inline static int32_t get_offset_of_freeze_35() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___freeze_35)); }
	inline bool get_freeze_35() const { return ___freeze_35; }
	inline bool* get_address_of_freeze_35() { return &___freeze_35; }
	inline void set_freeze_35(bool value)
	{
		___freeze_35 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_36() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___unscaledTime_36)); }
	inline bool get_unscaledTime_36() const { return ___unscaledTime_36; }
	inline bool* get_address_of_unscaledTime_36() { return &___unscaledTime_36; }
	inline void set_unscaledTime_36(bool value)
	{
		___unscaledTime_36 = value;
	}

	inline static int32_t get_offset_of_overrideTexture_37() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___overrideTexture_37)); }
	inline Texture_t3661962703 * get_overrideTexture_37() const { return ___overrideTexture_37; }
	inline Texture_t3661962703 ** get_address_of_overrideTexture_37() { return &___overrideTexture_37; }
	inline void set_overrideTexture_37(Texture_t3661962703 * value)
	{
		___overrideTexture_37 = value;
		Il2CppCodeGenWriteBarrier((&___overrideTexture_37), value);
	}

	inline static int32_t get_offset_of_skeleton_38() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___skeleton_38)); }
	inline Skeleton_t3686076450 * get_skeleton_38() const { return ___skeleton_38; }
	inline Skeleton_t3686076450 ** get_address_of_skeleton_38() { return &___skeleton_38; }
	inline void set_skeleton_38(Skeleton_t3686076450 * value)
	{
		___skeleton_38 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_38), value);
	}

	inline static int32_t get_offset_of_state_39() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___state_39)); }
	inline AnimationState_t3637309382 * get_state_39() const { return ___state_39; }
	inline AnimationState_t3637309382 ** get_address_of_state_39() { return &___state_39; }
	inline void set_state_39(AnimationState_t3637309382 * value)
	{
		___state_39 = value;
		Il2CppCodeGenWriteBarrier((&___state_39), value);
	}

	inline static int32_t get_offset_of_meshGenerator_40() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___meshGenerator_40)); }
	inline MeshGenerator_t1354683548 * get_meshGenerator_40() const { return ___meshGenerator_40; }
	inline MeshGenerator_t1354683548 ** get_address_of_meshGenerator_40() { return &___meshGenerator_40; }
	inline void set_meshGenerator_40(MeshGenerator_t1354683548 * value)
	{
		___meshGenerator_40 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_40), value);
	}

	inline static int32_t get_offset_of_meshBuffers_41() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___meshBuffers_41)); }
	inline DoubleBuffered_1_t2489386064 * get_meshBuffers_41() const { return ___meshBuffers_41; }
	inline DoubleBuffered_1_t2489386064 ** get_address_of_meshBuffers_41() { return &___meshBuffers_41; }
	inline void set_meshBuffers_41(DoubleBuffered_1_t2489386064 * value)
	{
		___meshBuffers_41 = value;
		Il2CppCodeGenWriteBarrier((&___meshBuffers_41), value);
	}

	inline static int32_t get_offset_of_currentInstructions_42() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___currentInstructions_42)); }
	inline SkeletonRendererInstruction_t651787775 * get_currentInstructions_42() const { return ___currentInstructions_42; }
	inline SkeletonRendererInstruction_t651787775 ** get_address_of_currentInstructions_42() { return &___currentInstructions_42; }
	inline void set_currentInstructions_42(SkeletonRendererInstruction_t651787775 * value)
	{
		___currentInstructions_42 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_42), value);
	}

	inline static int32_t get_offset_of_UpdateLocal_43() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___UpdateLocal_43)); }
	inline UpdateBonesDelegate_t735903178 * get_UpdateLocal_43() const { return ___UpdateLocal_43; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of_UpdateLocal_43() { return &___UpdateLocal_43; }
	inline void set_UpdateLocal_43(UpdateBonesDelegate_t735903178 * value)
	{
		___UpdateLocal_43 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateLocal_43), value);
	}

	inline static int32_t get_offset_of_UpdateWorld_44() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___UpdateWorld_44)); }
	inline UpdateBonesDelegate_t735903178 * get_UpdateWorld_44() const { return ___UpdateWorld_44; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of_UpdateWorld_44() { return &___UpdateWorld_44; }
	inline void set_UpdateWorld_44(UpdateBonesDelegate_t735903178 * value)
	{
		___UpdateWorld_44 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateWorld_44), value);
	}

	inline static int32_t get_offset_of_UpdateComplete_45() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___UpdateComplete_45)); }
	inline UpdateBonesDelegate_t735903178 * get_UpdateComplete_45() const { return ___UpdateComplete_45; }
	inline UpdateBonesDelegate_t735903178 ** get_address_of_UpdateComplete_45() { return &___UpdateComplete_45; }
	inline void set_UpdateComplete_45(UpdateBonesDelegate_t735903178 * value)
	{
		___UpdateComplete_45 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateComplete_45), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_46() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1744877482, ___OnPostProcessVertices_46)); }
	inline MeshGeneratorDelegate_t1654156803 * get_OnPostProcessVertices_46() const { return ___OnPostProcessVertices_46; }
	inline MeshGeneratorDelegate_t1654156803 ** get_address_of_OnPostProcessVertices_46() { return &___OnPostProcessVertices_46; }
	inline void set_OnPostProcessVertices_46(MeshGeneratorDelegate_t1654156803 * value)
	{
		___OnPostProcessVertices_46 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGRAPHIC_T1744877482_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (SlotData_t154801902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[13] = 
{
	SlotData_t154801902::get_offset_of_index_0(),
	SlotData_t154801902::get_offset_of_name_1(),
	SlotData_t154801902::get_offset_of_boneData_2(),
	SlotData_t154801902::get_offset_of_r_3(),
	SlotData_t154801902::get_offset_of_g_4(),
	SlotData_t154801902::get_offset_of_b_5(),
	SlotData_t154801902::get_offset_of_a_6(),
	SlotData_t154801902::get_offset_of_r2_7(),
	SlotData_t154801902::get_offset_of_g2_8(),
	SlotData_t154801902::get_offset_of_b2_9(),
	SlotData_t154801902::get_offset_of_hasSecondColor_10(),
	SlotData_t154801902::get_offset_of_attachmentName_11(),
	SlotData_t154801902::get_offset_of_blendMode_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (TransformConstraint_t454030229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[7] = 
{
	TransformConstraint_t454030229::get_offset_of_data_0(),
	TransformConstraint_t454030229::get_offset_of_bones_1(),
	TransformConstraint_t454030229::get_offset_of_target_2(),
	TransformConstraint_t454030229::get_offset_of_rotateMix_3(),
	TransformConstraint_t454030229::get_offset_of_translateMix_4(),
	TransformConstraint_t454030229::get_offset_of_scaleMix_5(),
	TransformConstraint_t454030229::get_offset_of_shearMix_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (TransformConstraintData_t529073346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[16] = 
{
	TransformConstraintData_t529073346::get_offset_of_name_0(),
	TransformConstraintData_t529073346::get_offset_of_order_1(),
	TransformConstraintData_t529073346::get_offset_of_bones_2(),
	TransformConstraintData_t529073346::get_offset_of_target_3(),
	TransformConstraintData_t529073346::get_offset_of_rotateMix_4(),
	TransformConstraintData_t529073346::get_offset_of_translateMix_5(),
	TransformConstraintData_t529073346::get_offset_of_scaleMix_6(),
	TransformConstraintData_t529073346::get_offset_of_shearMix_7(),
	TransformConstraintData_t529073346::get_offset_of_offsetRotation_8(),
	TransformConstraintData_t529073346::get_offset_of_offsetX_9(),
	TransformConstraintData_t529073346::get_offset_of_offsetY_10(),
	TransformConstraintData_t529073346::get_offset_of_offsetScaleX_11(),
	TransformConstraintData_t529073346::get_offset_of_offsetScaleY_12(),
	TransformConstraintData_t529073346::get_offset_of_offsetShearY_13(),
	TransformConstraintData_t529073346::get_offset_of_relative_14(),
	TransformConstraintData_t529073346::get_offset_of_local_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (Triangulator_t2502879214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[7] = 
{
	Triangulator_t2502879214::get_offset_of_convexPolygons_0(),
	Triangulator_t2502879214::get_offset_of_convexPolygonsIndices_1(),
	Triangulator_t2502879214::get_offset_of_indicesArray_2(),
	Triangulator_t2502879214::get_offset_of_isConcaveArray_3(),
	Triangulator_t2502879214::get_offset_of_triangles_4(),
	Triangulator_t2502879214::get_offset_of_polygonPool_5(),
	Triangulator_t2502879214::get_offset_of_polygonIndicesPool_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (AnimationReferenceAsset_t3989077076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[4] = 
{
	0,
	AnimationReferenceAsset_t3989077076::get_offset_of_skeletonDataAsset_3(),
	AnimationReferenceAsset_t3989077076::get_offset_of_animationName_4(),
	AnimationReferenceAsset_t3989077076::get_offset_of_animation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (AtlasAsset_t1167231206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[3] = 
{
	AtlasAsset_t1167231206::get_offset_of_atlasFile_2(),
	AtlasAsset_t1167231206::get_offset_of_materials_3(),
	AtlasAsset_t1167231206::get_offset_of_atlas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (MaterialsTextureLoader_t1402074808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[1] = 
{
	MaterialsTextureLoader_t1402074808::get_offset_of_atlasAsset_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (EventDataReferenceAsset_t4034732881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[4] = 
{
	0,
	EventDataReferenceAsset_t4034732881::get_offset_of_skeletonDataAsset_3(),
	EventDataReferenceAsset_t4034732881::get_offset_of_eventName_4(),
	EventDataReferenceAsset_t4034732881::get_offset_of_eventData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (RegionlessAttachmentLoader_t2643180673), -1, sizeof(RegionlessAttachmentLoader_t2643180673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2808[1] = 
{
	RegionlessAttachmentLoader_t2643180673_StaticFields::get_offset_of_emptyRegion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (SkeletonDataAsset_t3748144825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[10] = 
{
	SkeletonDataAsset_t3748144825::get_offset_of_atlasAssets_2(),
	SkeletonDataAsset_t3748144825::get_offset_of_scale_3(),
	SkeletonDataAsset_t3748144825::get_offset_of_skeletonJSON_4(),
	SkeletonDataAsset_t3748144825::get_offset_of_fromAnimation_5(),
	SkeletonDataAsset_t3748144825::get_offset_of_toAnimation_6(),
	SkeletonDataAsset_t3748144825::get_offset_of_duration_7(),
	SkeletonDataAsset_t3748144825::get_offset_of_defaultMix_8(),
	SkeletonDataAsset_t3748144825::get_offset_of_controller_9(),
	SkeletonDataAsset_t3748144825::get_offset_of_skeletonData_10(),
	SkeletonDataAsset_t3748144825::get_offset_of_stateData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (BoneFollower_t3069400636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[11] = 
{
	BoneFollower_t3069400636::get_offset_of_skeletonRenderer_2(),
	BoneFollower_t3069400636::get_offset_of_boneName_3(),
	BoneFollower_t3069400636::get_offset_of_followZPosition_4(),
	BoneFollower_t3069400636::get_offset_of_followBoneRotation_5(),
	BoneFollower_t3069400636::get_offset_of_followSkeletonFlip_6(),
	BoneFollower_t3069400636::get_offset_of_followLocalScale_7(),
	BoneFollower_t3069400636::get_offset_of_initializeOnAwake_8(),
	BoneFollower_t3069400636::get_offset_of_valid_9(),
	BoneFollower_t3069400636::get_offset_of_bone_10(),
	BoneFollower_t3069400636::get_offset_of_skeletonTransform_11(),
	BoneFollower_t3069400636::get_offset_of_skeletonTransformIsParent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (PointFollower_t1569279465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[11] = 
{
	PointFollower_t1569279465::get_offset_of_skeletonRenderer_2(),
	PointFollower_t1569279465::get_offset_of_slotName_3(),
	PointFollower_t1569279465::get_offset_of_pointAttachmentName_4(),
	PointFollower_t1569279465::get_offset_of_followRotation_5(),
	PointFollower_t1569279465::get_offset_of_followSkeletonFlip_6(),
	PointFollower_t1569279465::get_offset_of_followSkeletonZPosition_7(),
	PointFollower_t1569279465::get_offset_of_skeletonTransform_8(),
	PointFollower_t1569279465::get_offset_of_skeletonTransformIsParent_9(),
	PointFollower_t1569279465::get_offset_of_point_10(),
	PointFollower_t1569279465::get_offset_of_bone_11(),
	PointFollower_t1569279465::get_offset_of_valid_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (SkeletonAnimation_t3693186521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[7] = 
{
	SkeletonAnimation_t3693186521::get_offset_of_state_31(),
	SkeletonAnimation_t3693186521::get_offset_of__UpdateLocal_32(),
	SkeletonAnimation_t3693186521::get_offset_of__UpdateWorld_33(),
	SkeletonAnimation_t3693186521::get_offset_of__UpdateComplete_34(),
	SkeletonAnimation_t3693186521::get_offset_of__animationName_35(),
	SkeletonAnimation_t3693186521::get_offset_of_loop_36(),
	SkeletonAnimation_t3693186521::get_offset_of_timeScale_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (SkeletonAnimator_t1073737811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[4] = 
{
	SkeletonAnimator_t1073737811::get_offset_of_translator_31(),
	SkeletonAnimator_t1073737811::get_offset_of__UpdateLocal_32(),
	SkeletonAnimator_t1073737811::get_offset_of__UpdateWorld_33(),
	SkeletonAnimator_t1073737811::get_offset_of__UpdateComplete_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (MecanimTranslator_t2363469064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[8] = 
{
	MecanimTranslator_t2363469064::get_offset_of_autoReset_0(),
	MecanimTranslator_t2363469064::get_offset_of_layerMixModes_1(),
	MecanimTranslator_t2363469064::get_offset_of_animationTable_2(),
	MecanimTranslator_t2363469064::get_offset_of_clipNameHashCodeTable_3(),
	MecanimTranslator_t2363469064::get_offset_of_previousAnimations_4(),
	MecanimTranslator_t2363469064::get_offset_of_clipInfoCache_5(),
	MecanimTranslator_t2363469064::get_offset_of_nextClipInfoCache_6(),
	MecanimTranslator_t2363469064::get_offset_of_animator_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (MixMode_t3058145032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2815[4] = 
{
	MixMode_t3058145032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (AnimationClipEqualityComparer_t3042476764), -1, sizeof(AnimationClipEqualityComparer_t3042476764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2816[1] = 
{
	AnimationClipEqualityComparer_t3042476764_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (IntEqualityComparer_t2049031273), -1, sizeof(IntEqualityComparer_t2049031273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2817[1] = 
{
	IntEqualityComparer_t2049031273_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (SkeletonRenderer_t2098681813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[29] = 
{
	SkeletonRenderer_t2098681813::get_offset_of_OnRebuild_2(),
	SkeletonRenderer_t2098681813::get_offset_of_OnPostProcessVertices_3(),
	SkeletonRenderer_t2098681813::get_offset_of_skeletonDataAsset_4(),
	SkeletonRenderer_t2098681813::get_offset_of_initialSkinName_5(),
	SkeletonRenderer_t2098681813::get_offset_of_initialFlipX_6(),
	SkeletonRenderer_t2098681813::get_offset_of_initialFlipY_7(),
	SkeletonRenderer_t2098681813::get_offset_of_separatorSlotNames_8(),
	SkeletonRenderer_t2098681813::get_offset_of_separatorSlots_9(),
	SkeletonRenderer_t2098681813::get_offset_of_zSpacing_10(),
	SkeletonRenderer_t2098681813::get_offset_of_useClipping_11(),
	SkeletonRenderer_t2098681813::get_offset_of_immutableTriangles_12(),
	SkeletonRenderer_t2098681813::get_offset_of_pmaVertexColors_13(),
	SkeletonRenderer_t2098681813::get_offset_of_clearStateOnDisable_14(),
	SkeletonRenderer_t2098681813::get_offset_of_tintBlack_15(),
	SkeletonRenderer_t2098681813::get_offset_of_singleSubmesh_16(),
	SkeletonRenderer_t2098681813::get_offset_of_addNormals_17(),
	SkeletonRenderer_t2098681813::get_offset_of_calculateTangents_18(),
	SkeletonRenderer_t2098681813::get_offset_of_logErrors_19(),
	SkeletonRenderer_t2098681813::get_offset_of_disableRenderingOnOverride_20(),
	SkeletonRenderer_t2098681813::get_offset_of_generateMeshOverride_21(),
	SkeletonRenderer_t2098681813::get_offset_of_customMaterialOverride_22(),
	SkeletonRenderer_t2098681813::get_offset_of_customSlotMaterials_23(),
	SkeletonRenderer_t2098681813::get_offset_of_meshRenderer_24(),
	SkeletonRenderer_t2098681813::get_offset_of_meshFilter_25(),
	SkeletonRenderer_t2098681813::get_offset_of_valid_26(),
	SkeletonRenderer_t2098681813::get_offset_of_skeleton_27(),
	SkeletonRenderer_t2098681813::get_offset_of_currentInstructions_28(),
	SkeletonRenderer_t2098681813::get_offset_of_meshGenerator_29(),
	SkeletonRenderer_t2098681813::get_offset_of_rendererBuffers_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (SkeletonRendererDelegate_t3507789975), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (InstructionDelegate_t2225421195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (UpdateBonesDelegate_t735903178), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (SpineMesh_t2128742078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (SubmeshInstruction_t52121370)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[10] = 
{
	SubmeshInstruction_t52121370::get_offset_of_skeleton_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_startSlot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_endSlot_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_forceSeparate_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_preActiveClippingSlotSource_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_rawTriangleCount_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_rawVertexCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_rawFirstVertexIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t52121370::get_offset_of_hasClipping_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (MeshGeneratorDelegate_t1654156803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (MeshGeneratorBuffers_t1424700926)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[5] = 
{
	MeshGeneratorBuffers_t1424700926::get_offset_of_vertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_vertexBuffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_uvBuffer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_colorBuffer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1424700926::get_offset_of_meshGenerator_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (MeshGenerator_t1354683548), -1, sizeof(MeshGenerator_t1354683548_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[23] = 
{
	MeshGenerator_t1354683548::get_offset_of_settings_0(),
	0,
	0,
	MeshGenerator_t1354683548::get_offset_of_vertexBuffer_3(),
	MeshGenerator_t1354683548::get_offset_of_uvBuffer_4(),
	MeshGenerator_t1354683548::get_offset_of_colorBuffer_5(),
	MeshGenerator_t1354683548::get_offset_of_submeshes_6(),
	MeshGenerator_t1354683548::get_offset_of_meshBoundsMin_7(),
	MeshGenerator_t1354683548::get_offset_of_meshBoundsMax_8(),
	MeshGenerator_t1354683548::get_offset_of_meshBoundsThickness_9(),
	MeshGenerator_t1354683548::get_offset_of_submeshIndex_10(),
	MeshGenerator_t1354683548::get_offset_of_clipper_11(),
	MeshGenerator_t1354683548::get_offset_of_tempVerts_12(),
	MeshGenerator_t1354683548::get_offset_of_regionTriangles_13(),
	MeshGenerator_t1354683548::get_offset_of_normals_14(),
	MeshGenerator_t1354683548::get_offset_of_tangents_15(),
	MeshGenerator_t1354683548::get_offset_of_tempTanBuffer_16(),
	MeshGenerator_t1354683548::get_offset_of_uv2_17(),
	MeshGenerator_t1354683548::get_offset_of_uv3_18(),
	MeshGenerator_t1354683548_StaticFields::get_offset_of_AttachmentVerts_19(),
	MeshGenerator_t1354683548_StaticFields::get_offset_of_AttachmentUVs_20(),
	MeshGenerator_t1354683548_StaticFields::get_offset_of_AttachmentColors32_21(),
	MeshGenerator_t1354683548_StaticFields::get_offset_of_AttachmentIndices_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (Settings_t612870457)+ sizeof (RuntimeObject), sizeof(Settings_t612870457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2834[7] = 
{
	Settings_t612870457::get_offset_of_useClipping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_zSpacing_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_pmaVertexColors_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_tintBlack_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_calculateTangents_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_addNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t612870457::get_offset_of_immutableTriangles_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (MeshRendererBuffers_t756429994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[3] = 
{
	MeshRendererBuffers_t756429994::get_offset_of_doubleBufferedMesh_0(),
	MeshRendererBuffers_t756429994::get_offset_of_submeshMaterials_1(),
	MeshRendererBuffers_t756429994::get_offset_of_sharedMaterials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (SmartMesh_t524811292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[2] = 
{
	SmartMesh_t524811292::get_offset_of_mesh_0(),
	SmartMesh_t524811292::get_offset_of_instructionUsed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (SkeletonRendererInstruction_t651787775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[5] = 
{
	SkeletonRendererInstruction_t651787775::get_offset_of_immutableTriangles_0(),
	SkeletonRendererInstruction_t651787775::get_offset_of_submeshInstructions_1(),
	SkeletonRendererInstruction_t651787775::get_offset_of_hasActiveClipping_2(),
	SkeletonRendererInstruction_t651787775::get_offset_of_rawVertexCount_3(),
	SkeletonRendererInstruction_t651787775::get_offset_of_attachments_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (AttachmentRegionExtensions_t1591891965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (AtlasUtilities_t1517471311), -1, sizeof(AtlasUtilities_t1517471311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2839[6] = 
{
	0,
	0,
	0,
	0,
	AtlasUtilities_t1517471311_StaticFields::get_offset_of_CachedRegionTextures_4(),
	AtlasUtilities_t1517471311_StaticFields::get_offset_of_CachedRegionTexturesList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (SkinUtilities_t498157063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (AttachmentCloneExtensions_t460514876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (BoundingBoxFollower_t4182505745), -1, sizeof(BoundingBoxFollower_t4182505745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2842[11] = 
{
	BoundingBoxFollower_t4182505745_StaticFields::get_offset_of_DebugMessages_2(),
	BoundingBoxFollower_t4182505745::get_offset_of_skeletonRenderer_3(),
	BoundingBoxFollower_t4182505745::get_offset_of_slotName_4(),
	BoundingBoxFollower_t4182505745::get_offset_of_isTrigger_5(),
	BoundingBoxFollower_t4182505745::get_offset_of_clearStateOnDisable_6(),
	BoundingBoxFollower_t4182505745::get_offset_of_slot_7(),
	BoundingBoxFollower_t4182505745::get_offset_of_currentAttachment_8(),
	BoundingBoxFollower_t4182505745::get_offset_of_currentAttachmentName_9(),
	BoundingBoxFollower_t4182505745::get_offset_of_currentCollider_10(),
	BoundingBoxFollower_t4182505745::get_offset_of_colliderTable_11(),
	BoundingBoxFollower_t4182505745::get_offset_of_nameTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (SkeletonRendererCustomMaterials_t1555313811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[3] = 
{
	SkeletonRendererCustomMaterials_t1555313811::get_offset_of_skeletonRenderer_2(),
	SkeletonRendererCustomMaterials_t1555313811::get_offset_of_customSlotMaterials_3(),
	SkeletonRendererCustomMaterials_t1555313811::get_offset_of_customMaterialOverrides_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (SlotMaterialOverride_t1001979181)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[3] = 
{
	SlotMaterialOverride_t1001979181::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_t1001979181::get_offset_of_slotName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_t1001979181::get_offset_of_material_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (AtlasMaterialOverride_t2435041389)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[3] = 
{
	AtlasMaterialOverride_t2435041389::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_t2435041389::get_offset_of_originalMaterial_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_t2435041389::get_offset_of_replacementMaterial_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (SkeletonGhost_t1898327037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[19] = 
{
	0,
	0,
	SkeletonGhost_t1898327037::get_offset_of_ghostingEnabled_4(),
	SkeletonGhost_t1898327037::get_offset_of_spawnRate_5(),
	SkeletonGhost_t1898327037::get_offset_of_color_6(),
	SkeletonGhost_t1898327037::get_offset_of_additive_7(),
	SkeletonGhost_t1898327037::get_offset_of_maximumGhosts_8(),
	SkeletonGhost_t1898327037::get_offset_of_fadeSpeed_9(),
	SkeletonGhost_t1898327037::get_offset_of_ghostShader_10(),
	SkeletonGhost_t1898327037::get_offset_of_textureFade_11(),
	SkeletonGhost_t1898327037::get_offset_of_sortWithDistanceOnly_12(),
	SkeletonGhost_t1898327037::get_offset_of_zOffset_13(),
	SkeletonGhost_t1898327037::get_offset_of_nextSpawnTime_14(),
	SkeletonGhost_t1898327037::get_offset_of_pool_15(),
	SkeletonGhost_t1898327037::get_offset_of_poolIndex_16(),
	SkeletonGhost_t1898327037::get_offset_of_skeletonRenderer_17(),
	SkeletonGhost_t1898327037::get_offset_of_meshRenderer_18(),
	SkeletonGhost_t1898327037::get_offset_of_meshFilter_19(),
	SkeletonGhost_t1898327037::get_offset_of_materialTable_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (SkeletonGhostRenderer_t2445315009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[5] = 
{
	SkeletonGhostRenderer_t2445315009::get_offset_of_fadeSpeed_2(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_colors_3(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_black_4(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_meshFilter_5(),
	SkeletonGhostRenderer_t2445315009::get_offset_of_meshRenderer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (U3CFadeU3Ec__Iterator0_t1290633589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[7] = 
{
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U3CtU3E__1_0(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U3CbreakoutU3E__2_1(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U3CcU3E__3_2(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24this_3(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24current_4(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24disposing_5(),
	U3CFadeU3Ec__Iterator0_t1290633589::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (U3CFadeAdditiveU3Ec__Iterator1_t410398119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[8] = 
{
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CblackU3E__0_0(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CtU3E__1_1(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CbreakoutU3E__2_2(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U3CcU3E__3_3(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24this_4(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24current_5(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24disposing_6(),
	U3CFadeAdditiveU3Ec__Iterator1_t410398119::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (SkeletonRagdoll_t480923817), -1, sizeof(SkeletonRagdoll_t480923817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2850[23] = 
{
	SkeletonRagdoll_t480923817_StaticFields::get_offset_of_parentSpaceHelper_2(),
	SkeletonRagdoll_t480923817::get_offset_of_startingBoneName_3(),
	SkeletonRagdoll_t480923817::get_offset_of_stopBoneNames_4(),
	SkeletonRagdoll_t480923817::get_offset_of_applyOnStart_5(),
	SkeletonRagdoll_t480923817::get_offset_of_disableIK_6(),
	SkeletonRagdoll_t480923817::get_offset_of_disableOtherConstraints_7(),
	SkeletonRagdoll_t480923817::get_offset_of_pinStartBone_8(),
	SkeletonRagdoll_t480923817::get_offset_of_enableJointCollision_9(),
	SkeletonRagdoll_t480923817::get_offset_of_useGravity_10(),
	SkeletonRagdoll_t480923817::get_offset_of_thickness_11(),
	SkeletonRagdoll_t480923817::get_offset_of_rotationLimit_12(),
	SkeletonRagdoll_t480923817::get_offset_of_rootMass_13(),
	SkeletonRagdoll_t480923817::get_offset_of_massFalloffFactor_14(),
	SkeletonRagdoll_t480923817::get_offset_of_colliderLayer_15(),
	SkeletonRagdoll_t480923817::get_offset_of_mix_16(),
	SkeletonRagdoll_t480923817::get_offset_of_targetSkeletonComponent_17(),
	SkeletonRagdoll_t480923817::get_offset_of_skeleton_18(),
	SkeletonRagdoll_t480923817::get_offset_of_boneTable_19(),
	SkeletonRagdoll_t480923817::get_offset_of_ragdollRoot_20(),
	SkeletonRagdoll_t480923817::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_21(),
	SkeletonRagdoll_t480923817::get_offset_of_U3CStartingBoneU3Ek__BackingField_22(),
	SkeletonRagdoll_t480923817::get_offset_of_rootOffset_23(),
	SkeletonRagdoll_t480923817::get_offset_of_isActive_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (LayerFieldAttribute_t3408802261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (U3CStartU3Ec__Iterator0_t294813647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[4] = 
{
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t294813647::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[8] = 
{
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U3CstartMixU3E__0_1(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_target_2(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_duration_3(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24this_4(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24current_5(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24disposing_6(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t2718417871::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (SkeletonRagdoll2D_t2972821364), -1, sizeof(SkeletonRagdoll2D_t2972821364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2854[22] = 
{
	SkeletonRagdoll2D_t2972821364_StaticFields::get_offset_of_parentSpaceHelper_2(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_startingBoneName_3(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_stopBoneNames_4(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_applyOnStart_5(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_disableIK_6(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_disableOtherConstraints_7(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_pinStartBone_8(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_gravityScale_9(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_thickness_10(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_rotationLimit_11(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_rootMass_12(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_massFalloffFactor_13(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_colliderLayer_14(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_mix_15(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_targetSkeletonComponent_16(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_skeleton_17(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_boneTable_18(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_ragdollRoot_19(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_20(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_U3CStartingBoneU3Ek__BackingField_21(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_rootOffset_22(),
	SkeletonRagdoll2D_t2972821364::get_offset_of_isActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (U3CStartU3Ec__Iterator0_t3329131839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[4] = 
{
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3329131839::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[8] = 
{
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U3CstartMixU3E__0_1(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_target_2(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_duration_3(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24this_4(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24current_5(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24disposing_6(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t3141170647::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (BoneFollowerGraphic_t46362405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[11] = 
{
	BoneFollowerGraphic_t46362405::get_offset_of_skeletonGraphic_2(),
	BoneFollowerGraphic_t46362405::get_offset_of_initializeOnAwake_3(),
	BoneFollowerGraphic_t46362405::get_offset_of_boneName_4(),
	BoneFollowerGraphic_t46362405::get_offset_of_followBoneRotation_5(),
	BoneFollowerGraphic_t46362405::get_offset_of_followSkeletonFlip_6(),
	BoneFollowerGraphic_t46362405::get_offset_of_followLocalScale_7(),
	BoneFollowerGraphic_t46362405::get_offset_of_followZPosition_8(),
	BoneFollowerGraphic_t46362405::get_offset_of_bone_9(),
	BoneFollowerGraphic_t46362405::get_offset_of_skeletonTransform_10(),
	BoneFollowerGraphic_t46362405::get_offset_of_skeletonTransformIsParent_11(),
	BoneFollowerGraphic_t46362405::get_offset_of_valid_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (SkeletonGraphic_t1744877482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[19] = 
{
	SkeletonGraphic_t1744877482::get_offset_of_skeletonDataAsset_28(),
	SkeletonGraphic_t1744877482::get_offset_of_initialSkinName_29(),
	SkeletonGraphic_t1744877482::get_offset_of_initialFlipX_30(),
	SkeletonGraphic_t1744877482::get_offset_of_initialFlipY_31(),
	SkeletonGraphic_t1744877482::get_offset_of_startingAnimation_32(),
	SkeletonGraphic_t1744877482::get_offset_of_startingLoop_33(),
	SkeletonGraphic_t1744877482::get_offset_of_timeScale_34(),
	SkeletonGraphic_t1744877482::get_offset_of_freeze_35(),
	SkeletonGraphic_t1744877482::get_offset_of_unscaledTime_36(),
	SkeletonGraphic_t1744877482::get_offset_of_overrideTexture_37(),
	SkeletonGraphic_t1744877482::get_offset_of_skeleton_38(),
	SkeletonGraphic_t1744877482::get_offset_of_state_39(),
	SkeletonGraphic_t1744877482::get_offset_of_meshGenerator_40(),
	SkeletonGraphic_t1744877482::get_offset_of_meshBuffers_41(),
	SkeletonGraphic_t1744877482::get_offset_of_currentInstructions_42(),
	SkeletonGraphic_t1744877482::get_offset_of_UpdateLocal_43(),
	SkeletonGraphic_t1744877482::get_offset_of_UpdateWorld_44(),
	SkeletonGraphic_t1744877482::get_offset_of_UpdateComplete_45(),
	SkeletonGraphic_t1744877482::get_offset_of_OnPostProcessVertices_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (SkeletonPartsRenderer_t667127217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[5] = 
{
	SkeletonPartsRenderer_t667127217::get_offset_of_meshGenerator_2(),
	SkeletonPartsRenderer_t667127217::get_offset_of_meshRenderer_3(),
	SkeletonPartsRenderer_t667127217::get_offset_of_meshFilter_4(),
	SkeletonPartsRenderer_t667127217::get_offset_of_buffers_5(),
	SkeletonPartsRenderer_t667127217::get_offset_of_currentInstructions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (SkeletonRenderSeparator_t2026602841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[7] = 
{
	0,
	SkeletonRenderSeparator_t2026602841::get_offset_of_skeletonRenderer_3(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_mainMeshRenderer_4(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_copyPropertyBlock_5(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_copyMeshRendererFlags_6(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_partsRenderers_7(),
	SkeletonRenderSeparator_t2026602841::get_offset_of_copiedBlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (SkeletonUtilityEyeConstraint_t2225743321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[7] = 
{
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_eyes_4(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_radius_5(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_target_6(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_targetPosition_7(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_speed_8(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_origins_9(),
	SkeletonUtilityEyeConstraint_t2225743321::get_offset_of_centerPoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (SkeletonUtilityGroundConstraint_t1570100391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[12] = 
{
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_groundMask_4(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_use2D_5(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_useRadius_6(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_castRadius_7(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_castDistance_8(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_castOffset_9(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_groundOffset_10(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_adjustSpeed_11(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_rayOrigin_12(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_rayDir_13(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_hitY_14(),
	SkeletonUtilityGroundConstraint_t1570100391::get_offset_of_lastHitY_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (SkeletonUtilityKinematicShadow_t296913764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[6] = 
{
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_detachedShadow_2(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_parent_3(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_hideShadow_4(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_physicsSystem_5(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_shadowRoot_6(),
	SkeletonUtilityKinematicShadow_t296913764::get_offset_of_shadowTable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (TransformPair_t3795417756)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[2] = 
{
	TransformPair_t3795417756::get_offset_of_dest_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPair_t3795417756::get_offset_of_src_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (PhysicsSystem_t1859354049)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2865[3] = 
{
	PhysicsSystem_t1859354049::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (SlotBlendModes_t1277474191), -1, sizeof(SlotBlendModes_t1277474191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2866[5] = 
{
	SlotBlendModes_t1277474191_StaticFields::get_offset_of_materialTable_2(),
	SlotBlendModes_t1277474191::get_offset_of_multiplyMaterialSource_3(),
	SlotBlendModes_t1277474191::get_offset_of_screenMaterialSource_4(),
	SlotBlendModes_t1277474191::get_offset_of_texture_5(),
	SlotBlendModes_t1277474191::get_offset_of_U3CAppliedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (MaterialTexturePair_t1637207048)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[2] = 
{
	MaterialTexturePair_t1637207048::get_offset_of_texture2D_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTexturePair_t1637207048::get_offset_of_material_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (WaitForSpineAnimationComplete_t3713264939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[1] = 
{
	WaitForSpineAnimationComplete_t3713264939::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (WaitForSpineEvent_t3999047839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[5] = 
{
	WaitForSpineEvent_t3999047839::get_offset_of_m_TargetEvent_0(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_EventName_1(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_AnimationState_2(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_WasFired_3(),
	WaitForSpineEvent_t3999047839::get_offset_of_m_unsubscribeAfterFiring_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (WaitForSpineTrackEntryEnd_t2617682820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[1] = 
{
	WaitForSpineTrackEntryEnd_t2617682820::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (SkeletonExtensions_t1502749191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (BoneMatrix_t2668835335)+ sizeof (RuntimeObject), sizeof(BoneMatrix_t2668835335 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2872[6] = 
{
	BoneMatrix_t2668835335::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_t2668835335::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_t2668835335::get_offset_of_c_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_t2668835335::get_offset_of_d_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_t2668835335::get_offset_of_x_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_t2668835335::get_offset_of_y_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (SkeletonExtensions_t671502775), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (SkeletonUtility_t2980767925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[9] = 
{
	SkeletonUtility_t2980767925::get_offset_of_OnReset_2(),
	SkeletonUtility_t2980767925::get_offset_of_boneRoot_3(),
	SkeletonUtility_t2980767925::get_offset_of_skeletonRenderer_4(),
	SkeletonUtility_t2980767925::get_offset_of_skeletonAnimation_5(),
	SkeletonUtility_t2980767925::get_offset_of_utilityBones_6(),
	SkeletonUtility_t2980767925::get_offset_of_utilityConstraints_7(),
	SkeletonUtility_t2980767925::get_offset_of_hasTransformBones_8(),
	SkeletonUtility_t2980767925::get_offset_of_hasUtilityConstraints_9(),
	SkeletonUtility_t2980767925::get_offset_of_needToReprocessBones_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (SkeletonUtilityDelegate_t487527757), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (SkeletonUtilityBone_t2225520134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[15] = 
{
	SkeletonUtilityBone_t2225520134::get_offset_of_boneName_2(),
	SkeletonUtilityBone_t2225520134::get_offset_of_parentReference_3(),
	SkeletonUtilityBone_t2225520134::get_offset_of_mode_4(),
	SkeletonUtilityBone_t2225520134::get_offset_of_position_5(),
	SkeletonUtilityBone_t2225520134::get_offset_of_rotation_6(),
	SkeletonUtilityBone_t2225520134::get_offset_of_scale_7(),
	SkeletonUtilityBone_t2225520134::get_offset_of_zPosition_8(),
	SkeletonUtilityBone_t2225520134::get_offset_of_overrideAlpha_9(),
	SkeletonUtilityBone_t2225520134::get_offset_of_skeletonUtility_10(),
	SkeletonUtilityBone_t2225520134::get_offset_of_bone_11(),
	SkeletonUtilityBone_t2225520134::get_offset_of_transformLerpComplete_12(),
	SkeletonUtilityBone_t2225520134::get_offset_of_valid_13(),
	SkeletonUtilityBone_t2225520134::get_offset_of_cachedTransform_14(),
	SkeletonUtilityBone_t2225520134::get_offset_of_skeletonTransform_15(),
	SkeletonUtilityBone_t2225520134::get_offset_of_incompatibleTransformMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (Mode_t3700196520)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2877[3] = 
{
	Mode_t3700196520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (UpdatePhase_t1465898593)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2878[4] = 
{
	UpdatePhase_t1465898593::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (SkeletonUtilityConstraint_t1014382506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[2] = 
{
	SkeletonUtilityConstraint_t1014382506::get_offset_of_utilBone_2(),
	SkeletonUtilityConstraint_t1014382506::get_offset_of_skeletonUtility_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (SpineAttributeBase_t340149836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[4] = 
{
	SpineAttributeBase_t340149836::get_offset_of_dataField_0(),
	SpineAttributeBase_t340149836::get_offset_of_startsWith_1(),
	SpineAttributeBase_t340149836::get_offset_of_includeNone_2(),
	SpineAttributeBase_t340149836::get_offset_of_fallbackToTextField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (SpineSlot_t2906857509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[1] = 
{
	SpineSlot_t2906857509::get_offset_of_containsBoundingBoxes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (SpineEvent_t2537401913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (SpineIkConstraint_t1587650654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (SpinePathConstraint_t3765886426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (SpineTransformConstraint_t1324873370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (SpineSkin_t4048709426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (SpineAnimation_t42895223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (SpineAttachment_t743638603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[5] = 
{
	SpineAttachment_t743638603::get_offset_of_returnAttachmentPath_4(),
	SpineAttachment_t743638603::get_offset_of_currentSkinOnly_5(),
	SpineAttachment_t743638603::get_offset_of_placeholdersOnly_6(),
	SpineAttachment_t743638603::get_offset_of_skinField_7(),
	SpineAttachment_t743638603::get_offset_of_slotField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (Hierarchy_t2161623951)+ sizeof (RuntimeObject), sizeof(Hierarchy_t2161623951_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2889[3] = 
{
	Hierarchy_t2161623951::get_offset_of_skin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t2161623951::get_offset_of_slot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t2161623951::get_offset_of_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (SpineBone_t3329529041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (SpineAtlasRegion_t3654419954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[1] = 
{
	SpineAtlasRegion_t3654419954::get_offset_of_atlasAssetField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2893[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D23447D2BC3FF6984AB09F575BC63CFE460337394_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (U24ArrayTypeU3D20_t1702832645)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t1702832645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
