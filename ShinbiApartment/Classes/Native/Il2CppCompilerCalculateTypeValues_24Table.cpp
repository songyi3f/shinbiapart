﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Info
struct Info_t1004903773;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Func`2<System.Boolean,System.String>
struct Func_2_t1267953766;
// System.Func`2<System.Enum,System.Guid>
struct Func_2_t313512738;
// System.String
struct String_t;
// GhostSelectPopup
struct GhostSelectPopup_t3778673497;
// GhostSelectListPopup
struct GhostSelectListPopup_t692877192;
// PuzzleItem
struct PuzzleItem_t2123471318;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// PuzzleLiveTimer
struct PuzzleLiveTimer_t464531872;
// CharmTimer
struct CharmTimer_t90502612;
// CandyTimer
struct CandyTimer_t2979372557;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0
struct U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618;
// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey2
struct U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378;
// System.Collections.Generic.List`1<PuzzleItem>
struct List_1_t3595546060;
// PuzzleManager
struct PuzzleManager_t474948643;
// System.Action`1<System.DateTime>
struct Action_1_t3910997380;
// UnityEngine.WWW
struct WWW_t3688466362;
// TimeManager/TimeInfo
struct TimeInfo_t2264797512;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// SceneLoader
struct SceneLoader_t4130533360;
// Charm
struct Charm_t3703444127;
// ChangeCharmPopup
struct ChangeCharmPopup_t1980703935;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// CharacterPopup
struct CharacterPopup_t2617494106;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// GhostSchoolPopup
struct GhostSchoolPopup_t1455275853;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t2609895709;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.Dictionary`2<CharmType,System.Int32>
struct Dictionary_2_t466317717;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t3064354032;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t1932989778;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<FullSerializer.fsAotConfiguration/Entry>
struct List_1_t431521422;
// System.Action`2<System.TimeSpan,System.Int32>
struct Action_2_t3788266618;
// System.Action
struct Action_t1264377477;
// GameVanilla.Core.BaseScene
struct BaseScene_t2918414559;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.Animator
struct Animator_t434523843;
// CharmGameManager
struct CharmGameManager_t204340045;
// ChangeCharmItem[]
struct ChangeCharmItemU5BU5D_t1024856007;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// DG.Tweening.Tween
struct Tween_t2342918553;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// System.Collections.Generic.Stack`1<UnityEngine.GameObject>
struct Stack_1_t1957026074;
// System.Collections.Generic.List`1<GhostSkillInfoItem>
struct List_1_t339037232;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t1744877482;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1514609158;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t3312407083;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t3748144825;
// Ghost
struct Ghost_t201992404;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// PuzzleItem[0...,0...]
struct PuzzleItemU5B0___U2C0___U5D_t216270356;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct List_1_t1741830302;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PLAYERDATA_T220878115_H
#define PLAYERDATA_T220878115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t220878115  : public RuntimeObject
{
public:
	// Info PlayerData::playerInfo
	Info_t1004903773 * ___playerInfo_0;

public:
	inline static int32_t get_offset_of_playerInfo_0() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___playerInfo_0)); }
	inline Info_t1004903773 * get_playerInfo_0() const { return ___playerInfo_0; }
	inline Info_t1004903773 ** get_address_of_playerInfo_0() { return &___playerInfo_0; }
	inline void set_playerInfo_0(Info_t1004903773 * value)
	{
		___playerInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___playerInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T220878115_H
#ifndef VIDEOCAPABILITIES_T1298735124_H
#define VIDEOCAPABILITIES_T1298735124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Video.VideoCapabilities
struct  VideoCapabilities_t1298735124  : public RuntimeObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsCameraSupported
	bool ___mIsCameraSupported_0;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsMicSupported
	bool ___mIsMicSupported_1;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsWriteStorageSupported
	bool ___mIsWriteStorageSupported_2;
	// System.Boolean[] GooglePlayGames.BasicApi.Video.VideoCapabilities::mCaptureModesSupported
	BooleanU5BU5D_t2897418192* ___mCaptureModesSupported_3;
	// System.Boolean[] GooglePlayGames.BasicApi.Video.VideoCapabilities::mQualityLevelsSupported
	BooleanU5BU5D_t2897418192* ___mQualityLevelsSupported_4;

public:
	inline static int32_t get_offset_of_mIsCameraSupported_0() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mIsCameraSupported_0)); }
	inline bool get_mIsCameraSupported_0() const { return ___mIsCameraSupported_0; }
	inline bool* get_address_of_mIsCameraSupported_0() { return &___mIsCameraSupported_0; }
	inline void set_mIsCameraSupported_0(bool value)
	{
		___mIsCameraSupported_0 = value;
	}

	inline static int32_t get_offset_of_mIsMicSupported_1() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mIsMicSupported_1)); }
	inline bool get_mIsMicSupported_1() const { return ___mIsMicSupported_1; }
	inline bool* get_address_of_mIsMicSupported_1() { return &___mIsMicSupported_1; }
	inline void set_mIsMicSupported_1(bool value)
	{
		___mIsMicSupported_1 = value;
	}

	inline static int32_t get_offset_of_mIsWriteStorageSupported_2() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mIsWriteStorageSupported_2)); }
	inline bool get_mIsWriteStorageSupported_2() const { return ___mIsWriteStorageSupported_2; }
	inline bool* get_address_of_mIsWriteStorageSupported_2() { return &___mIsWriteStorageSupported_2; }
	inline void set_mIsWriteStorageSupported_2(bool value)
	{
		___mIsWriteStorageSupported_2 = value;
	}

	inline static int32_t get_offset_of_mCaptureModesSupported_3() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mCaptureModesSupported_3)); }
	inline BooleanU5BU5D_t2897418192* get_mCaptureModesSupported_3() const { return ___mCaptureModesSupported_3; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mCaptureModesSupported_3() { return &___mCaptureModesSupported_3; }
	inline void set_mCaptureModesSupported_3(BooleanU5BU5D_t2897418192* value)
	{
		___mCaptureModesSupported_3 = value;
		Il2CppCodeGenWriteBarrier((&___mCaptureModesSupported_3), value);
	}

	inline static int32_t get_offset_of_mQualityLevelsSupported_4() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124, ___mQualityLevelsSupported_4)); }
	inline BooleanU5BU5D_t2897418192* get_mQualityLevelsSupported_4() const { return ___mQualityLevelsSupported_4; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mQualityLevelsSupported_4() { return &___mQualityLevelsSupported_4; }
	inline void set_mQualityLevelsSupported_4(BooleanU5BU5D_t2897418192* value)
	{
		___mQualityLevelsSupported_4 = value;
		Il2CppCodeGenWriteBarrier((&___mQualityLevelsSupported_4), value);
	}
};

struct VideoCapabilities_t1298735124_StaticFields
{
public:
	// System.Func`2<System.Boolean,System.String> GooglePlayGames.BasicApi.Video.VideoCapabilities::<>f__am$cache0
	Func_2_t1267953766 * ___U3CU3Ef__amU24cache0_5;
	// System.Func`2<System.Boolean,System.String> GooglePlayGames.BasicApi.Video.VideoCapabilities::<>f__am$cache1
	Func_2_t1267953766 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t1267953766 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t1267953766 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t1267953766 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(VideoCapabilities_t1298735124_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Func_2_t1267953766 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Func_2_t1267953766 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Func_2_t1267953766 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPABILITIES_T1298735124_H
#ifndef ENUMEXTENSIONS_T4248483755_H
#define ENUMEXTENSIONS_T4248483755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumExtensions
struct  EnumExtensions_t4248483755  : public RuntimeObject
{
public:

public:
};

struct EnumExtensions_t4248483755_StaticFields
{
public:
	// System.Func`2<System.Enum,System.Guid> EnumExtensions::<>f__am$cache0
	Func_2_t313512738 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(EnumExtensions_t4248483755_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t313512738 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t313512738 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t313512738 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMEXTENSIONS_T4248483755_H
#ifndef LOGGER_T3934082555_H
#define LOGGER_T3934082555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger
struct  Logger_t3934082555  : public RuntimeObject
{
public:

public:
};

struct Logger_t3934082555_StaticFields
{
public:
	// System.Boolean GooglePlayGames.OurUtils.Logger::debugLogEnabled
	bool ___debugLogEnabled_0;
	// System.Boolean GooglePlayGames.OurUtils.Logger::warningLogEnabled
	bool ___warningLogEnabled_1;

public:
	inline static int32_t get_offset_of_debugLogEnabled_0() { return static_cast<int32_t>(offsetof(Logger_t3934082555_StaticFields, ___debugLogEnabled_0)); }
	inline bool get_debugLogEnabled_0() const { return ___debugLogEnabled_0; }
	inline bool* get_address_of_debugLogEnabled_0() { return &___debugLogEnabled_0; }
	inline void set_debugLogEnabled_0(bool value)
	{
		___debugLogEnabled_0 = value;
	}

	inline static int32_t get_offset_of_warningLogEnabled_1() { return static_cast<int32_t>(offsetof(Logger_t3934082555_StaticFields, ___warningLogEnabled_1)); }
	inline bool get_warningLogEnabled_1() const { return ___warningLogEnabled_1; }
	inline bool* get_address_of_warningLogEnabled_1() { return &___warningLogEnabled_1; }
	inline void set_warningLogEnabled_1(bool value)
	{
		___warningLogEnabled_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T3934082555_H
#ifndef U3CDU3EC__ANONSTOREY0_T2350509859_H
#define U3CDU3EC__ANONSTOREY0_T2350509859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey0
struct  U3CdU3Ec__AnonStorey0_t2350509859  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey0::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CdU3Ec__AnonStorey0_t2350509859, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((&___msg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDU3EC__ANONSTOREY0_T2350509859_H
#ifndef U3CWU3EC__ANONSTOREY1_T2080961746_H
#define U3CWU3EC__ANONSTOREY1_T2080961746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey1
struct  U3CwU3Ec__AnonStorey1_t2080961746  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey1::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CwU3Ec__AnonStorey1_t2080961746, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((&___msg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWU3EC__ANONSTOREY1_T2080961746_H
#ifndef U3CAUTOKILLU3EC__ITERATOR0_T191949947_H
#define U3CAUTOKILLU3EC__ITERATOR0_T191949947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSelectPopup/<AutoKill>c__Iterator0
struct  U3CAutoKillU3Ec__Iterator0_t191949947  : public RuntimeObject
{
public:
	// GhostSelectPopup GhostSelectPopup/<AutoKill>c__Iterator0::$this
	GhostSelectPopup_t3778673497 * ___U24this_0;
	// System.Object GhostSelectPopup/<AutoKill>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GhostSelectPopup/<AutoKill>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GhostSelectPopup/<AutoKill>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t191949947, ___U24this_0)); }
	inline GhostSelectPopup_t3778673497 * get_U24this_0() const { return ___U24this_0; }
	inline GhostSelectPopup_t3778673497 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GhostSelectPopup_t3778673497 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t191949947, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t191949947, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t191949947, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOKILLU3EC__ITERATOR0_T191949947_H
#ifndef U3CEU3EC__ANONSTOREY2_T2346119983_H
#define U3CEU3EC__ANONSTOREY2_T2346119983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey2
struct  U3CeU3Ec__AnonStorey2_t2346119983  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey2::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CeU3Ec__AnonStorey2_t2346119983, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((&___msg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEU3EC__ANONSTOREY2_T2346119983_H
#ifndef U3CAUTOKILLU3EC__ITERATOR0_T1906957385_H
#define U3CAUTOKILLU3EC__ITERATOR0_T1906957385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSelectListPopup/<AutoKill>c__Iterator0
struct  U3CAutoKillU3Ec__Iterator0_t1906957385  : public RuntimeObject
{
public:
	// GhostSelectListPopup GhostSelectListPopup/<AutoKill>c__Iterator0::$this
	GhostSelectListPopup_t692877192 * ___U24this_0;
	// System.Object GhostSelectListPopup/<AutoKill>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GhostSelectListPopup/<AutoKill>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GhostSelectListPopup/<AutoKill>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t1906957385, ___U24this_0)); }
	inline GhostSelectListPopup_t692877192 * get_U24this_0() const { return ___U24this_0; }
	inline GhostSelectListPopup_t692877192 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GhostSelectListPopup_t692877192 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t1906957385, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t1906957385, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t1906957385, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOKILLU3EC__ITERATOR0_T1906957385_H
#ifndef U3CADDPUZZLEITEMU3EC__ANONSTOREY1_T2446037076_H
#define U3CADDPUZZLEITEMU3EC__ANONSTOREY1_T2446037076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleManager/<AddPuzzleItem>c__AnonStorey1
struct  U3CAddPuzzleItemU3Ec__AnonStorey1_t2446037076  : public RuntimeObject
{
public:
	// PuzzleItem PuzzleManager/<AddPuzzleItem>c__AnonStorey1::item
	PuzzleItem_t2123471318 * ___item_0;
	// UnityEngine.GameObject PuzzleManager/<AddPuzzleItem>c__AnonStorey1::itemPrefab
	GameObject_t1113636619 * ___itemPrefab_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CAddPuzzleItemU3Ec__AnonStorey1_t2446037076, ___item_0)); }
	inline PuzzleItem_t2123471318 * get_item_0() const { return ___item_0; }
	inline PuzzleItem_t2123471318 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(PuzzleItem_t2123471318 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_itemPrefab_1() { return static_cast<int32_t>(offsetof(U3CAddPuzzleItemU3Ec__AnonStorey1_t2446037076, ___itemPrefab_1)); }
	inline GameObject_t1113636619 * get_itemPrefab_1() const { return ___itemPrefab_1; }
	inline GameObject_t1113636619 ** get_address_of_itemPrefab_1() { return &___itemPrefab_1; }
	inline void set_itemPrefab_1(GameObject_t1113636619 * value)
	{
		___itemPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDPUZZLEITEMU3EC__ANONSTOREY1_T2446037076_H
#ifndef U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T3301235440_H
#define U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T3301235440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleLiveTimer/<RunningCountdown>c__Iterator0
struct  U3CRunningCountdownU3Ec__Iterator0_t3301235440  : public RuntimeObject
{
public:
	// PuzzleLiveTimer PuzzleLiveTimer/<RunningCountdown>c__Iterator0::$this
	PuzzleLiveTimer_t464531872 * ___U24this_0;
	// System.Object PuzzleLiveTimer/<RunningCountdown>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PuzzleLiveTimer/<RunningCountdown>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PuzzleLiveTimer/<RunningCountdown>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3301235440, ___U24this_0)); }
	inline PuzzleLiveTimer_t464531872 * get_U24this_0() const { return ___U24this_0; }
	inline PuzzleLiveTimer_t464531872 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PuzzleLiveTimer_t464531872 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3301235440, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3301235440, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3301235440, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T3301235440_H
#ifndef DOTWEENANIMATIONEXTENSIONS_T980608231_H
#define DOTWEENANIMATIONEXTENSIONS_T980608231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimationExtensions
struct  DOTweenAnimationExtensions_t980608231  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONEXTENSIONS_T980608231_H
#ifndef U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T3085404936_H
#define U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T3085404936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmTimer/<RunningCountdown>c__Iterator0
struct  U3CRunningCountdownU3Ec__Iterator0_t3085404936  : public RuntimeObject
{
public:
	// CharmTimer CharmTimer/<RunningCountdown>c__Iterator0::$this
	CharmTimer_t90502612 * ___U24this_0;
	// System.Object CharmTimer/<RunningCountdown>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CharmTimer/<RunningCountdown>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CharmTimer/<RunningCountdown>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3085404936, ___U24this_0)); }
	inline CharmTimer_t90502612 * get_U24this_0() const { return ___U24this_0; }
	inline CharmTimer_t90502612 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CharmTimer_t90502612 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3085404936, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3085404936, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t3085404936, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T3085404936_H
#ifndef U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T2994566354_H
#define U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T2994566354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CandyTimer/<RunningCountdown>c__Iterator0
struct  U3CRunningCountdownU3Ec__Iterator0_t2994566354  : public RuntimeObject
{
public:
	// CandyTimer CandyTimer/<RunningCountdown>c__Iterator0::$this
	CandyTimer_t2979372557 * ___U24this_0;
	// System.Object CandyTimer/<RunningCountdown>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CandyTimer/<RunningCountdown>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CandyTimer/<RunningCountdown>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t2994566354, ___U24this_0)); }
	inline CandyTimer_t2979372557 * get_U24this_0() const { return ___U24this_0; }
	inline CandyTimer_t2979372557 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CandyTimer_t2979372557 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t2994566354, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t2994566354, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunningCountdownU3Ec__Iterator0_t2994566354, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNNINGCOUNTDOWNU3EC__ITERATOR0_T2994566354_H
#ifndef COMMONTYPESUTIL_T3521372089_H
#define COMMONTYPESUTIL_T3521372089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.CommonTypesUtil
struct  CommonTypesUtil_t3521372089  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONTYPESUTIL_T3521372089_H
#ifndef SOUND_T3007421746_H
#define SOUND_T3007421746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sound
struct  Sound_t3007421746  : public RuntimeObject
{
public:
	// System.String Sound::name
	String_t* ___name_0;
	// UnityEngine.AudioClip Sound::clip
	AudioClip_t3680889665 * ___clip_1;
	// UnityEngine.AudioSource Sound::source
	AudioSource_t3935305588 * ___source_2;
	// System.Boolean Sound::loop
	bool ___loop_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_clip_1() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___clip_1)); }
	inline AudioClip_t3680889665 * get_clip_1() const { return ___clip_1; }
	inline AudioClip_t3680889665 ** get_address_of_clip_1() { return &___clip_1; }
	inline void set_clip_1(AudioClip_t3680889665 * value)
	{
		___clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___clip_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___source_2)); }
	inline AudioSource_t3935305588 * get_source_2() const { return ___source_2; }
	inline AudioSource_t3935305588 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(AudioSource_t3935305588 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUND_T3007421746_H
#ifndef U3CSPAWNNEWPUZZLEITEMSU3EC__ANONSTOREY3_T832581023_H
#define U3CSPAWNNEWPUZZLEITEMSU3EC__ANONSTOREY3_T832581023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey3
struct  U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023  : public RuntimeObject
{
public:
	// PuzzleItem PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey3::targetItem
	PuzzleItem_t2123471318 * ___targetItem_0;
	// UnityEngine.GameObject PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey3::itemPrefab
	GameObject_t1113636619 * ___itemPrefab_1;
	// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0 PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey3::<>f__ref$0
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618 * ___U3CU3Ef__refU240_2;
	// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey2 PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey3::<>f__ref$2
	U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378 * ___U3CU3Ef__refU242_3;

public:
	inline static int32_t get_offset_of_targetItem_0() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023, ___targetItem_0)); }
	inline PuzzleItem_t2123471318 * get_targetItem_0() const { return ___targetItem_0; }
	inline PuzzleItem_t2123471318 ** get_address_of_targetItem_0() { return &___targetItem_0; }
	inline void set_targetItem_0(PuzzleItem_t2123471318 * value)
	{
		___targetItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___targetItem_0), value);
	}

	inline static int32_t get_offset_of_itemPrefab_1() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023, ___itemPrefab_1)); }
	inline GameObject_t1113636619 * get_itemPrefab_1() const { return ___itemPrefab_1; }
	inline GameObject_t1113636619 ** get_address_of_itemPrefab_1() { return &___itemPrefab_1; }
	inline void set_itemPrefab_1(GameObject_t1113636619 * value)
	{
		___itemPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemPrefab_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023, ___U3CU3Ef__refU240_2)); }
	inline U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_3() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023, ___U3CU3Ef__refU242_3)); }
	inline U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378 * get_U3CU3Ef__refU242_3() const { return ___U3CU3Ef__refU242_3; }
	inline U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378 ** get_address_of_U3CU3Ef__refU242_3() { return &___U3CU3Ef__refU242_3; }
	inline void set_U3CU3Ef__refU242_3(U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378 * value)
	{
		___U3CU3Ef__refU242_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNNEWPUZZLEITEMSU3EC__ANONSTOREY3_T832581023_H
#ifndef U3CSPAWNNEWPUZZLEITEMSU3EC__ANONSTOREY2_T3561464378_H
#define U3CSPAWNNEWPUZZLEITEMSU3EC__ANONSTOREY2_T3561464378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey2
struct  U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378  : public RuntimeObject
{
public:
	// System.Int32 PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0/<SpawnNewPuzzleItems>c__AnonStorey2::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNNEWPUZZLEITEMSU3EC__ANONSTOREY2_T3561464378_H
#ifndef U3CSPAWNNEWPUZZLEITEMSU3EC__ITERATOR0_T862492618_H
#define U3CSPAWNNEWPUZZLEITEMSU3EC__ITERATOR0_T862492618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0
struct  U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618  : public RuntimeObject
{
public:
	// System.Int32 PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0::row
	int32_t ___row_0;
	// System.Collections.Generic.List`1<PuzzleItem> PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0::<emptyItems>__0
	List_1_t3595546060 * ___U3CemptyItemsU3E__0_1;
	// PuzzleManager PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0::$this
	PuzzleManager_t474948643 * ___U24this_2;
	// System.Object PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PuzzleManager/<SpawnNewPuzzleItems>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_row_0() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618, ___row_0)); }
	inline int32_t get_row_0() const { return ___row_0; }
	inline int32_t* get_address_of_row_0() { return &___row_0; }
	inline void set_row_0(int32_t value)
	{
		___row_0 = value;
	}

	inline static int32_t get_offset_of_U3CemptyItemsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618, ___U3CemptyItemsU3E__0_1)); }
	inline List_1_t3595546060 * get_U3CemptyItemsU3E__0_1() const { return ___U3CemptyItemsU3E__0_1; }
	inline List_1_t3595546060 ** get_address_of_U3CemptyItemsU3E__0_1() { return &___U3CemptyItemsU3E__0_1; }
	inline void set_U3CemptyItemsU3E__0_1(List_1_t3595546060 * value)
	{
		___U3CemptyItemsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CemptyItemsU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618, ___U24this_2)); }
	inline PuzzleManager_t474948643 * get_U24this_2() const { return ___U24this_2; }
	inline PuzzleManager_t474948643 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PuzzleManager_t474948643 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNNEWPUZZLEITEMSU3EC__ITERATOR0_T862492618_H
#ifndef MISC_T4208016214_H
#define MISC_T4208016214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Misc
struct  Misc_t4208016214  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISC_T4208016214_H
#ifndef U3CGETSERVERTIMEU3EC__ITERATOR0_T2759080239_H
#define U3CGETSERVERTIMEU3EC__ITERATOR0_T2759080239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeManager/<GetServerTime>c__Iterator0
struct  U3CGetServerTimeU3Ec__Iterator0_t2759080239  : public RuntimeObject
{
public:
	// System.Action`1<System.DateTime> TimeManager/<GetServerTime>c__Iterator0::action
	Action_1_t3910997380 * ___action_0;
	// UnityEngine.WWW TimeManager/<GetServerTime>c__Iterator0::<www>__1
	WWW_t3688466362 * ___U3CwwwU3E__1_1;
	// TimeManager/TimeInfo TimeManager/<GetServerTime>c__Iterator0::<data>__1
	TimeInfo_t2264797512 * ___U3CdataU3E__1_2;
	// System.Object TimeManager/<GetServerTime>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean TimeManager/<GetServerTime>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 TimeManager/<GetServerTime>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CGetServerTimeU3Ec__Iterator0_t2759080239, ___action_0)); }
	inline Action_1_t3910997380 * get_action_0() const { return ___action_0; }
	inline Action_1_t3910997380 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3910997380 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetServerTimeU3Ec__Iterator0_t2759080239, ___U3CwwwU3E__1_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__1_1() const { return ___U3CwwwU3E__1_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__1_1() { return &___U3CwwwU3E__1_1; }
	inline void set_U3CwwwU3E__1_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetServerTimeU3Ec__Iterator0_t2759080239, ___U3CdataU3E__1_2)); }
	inline TimeInfo_t2264797512 * get_U3CdataU3E__1_2() const { return ___U3CdataU3E__1_2; }
	inline TimeInfo_t2264797512 ** get_address_of_U3CdataU3E__1_2() { return &___U3CdataU3E__1_2; }
	inline void set_U3CdataU3E__1_2(TimeInfo_t2264797512 * value)
	{
		___U3CdataU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetServerTimeU3Ec__Iterator0_t2759080239, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetServerTimeU3Ec__Iterator0_t2759080239, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetServerTimeU3Ec__Iterator0_t2759080239, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSERVERTIMEU3EC__ITERATOR0_T2759080239_H
#ifndef U3CLOADASYNCHRONOUSLYU3EC__ITERATOR0_T4235513134_H
#define U3CLOADASYNCHRONOUSLYU3EC__ITERATOR0_T4235513134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLoader/<LoadAsynchronously>c__Iterator0
struct  U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134  : public RuntimeObject
{
public:
	// System.String SceneLoader/<LoadAsynchronously>c__Iterator0::sceneName
	String_t* ___sceneName_0;
	// UnityEngine.AsyncOperation SceneLoader/<LoadAsynchronously>c__Iterator0::<operation>__0
	AsyncOperation_t1445031843 * ___U3CoperationU3E__0_1;
	// UnityEngine.GameObject SceneLoader/<LoadAsynchronously>c__Iterator0::<Pop>__0
	GameObject_t1113636619 * ___U3CPopU3E__0_2;
	// System.Single SceneLoader/<LoadAsynchronously>c__Iterator0::<progress>__1
	float ___U3CprogressU3E__1_3;
	// SceneLoader SceneLoader/<LoadAsynchronously>c__Iterator0::$this
	SceneLoader_t4130533360 * ___U24this_4;
	// System.Object SceneLoader/<LoadAsynchronously>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean SceneLoader/<LoadAsynchronously>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 SceneLoader/<LoadAsynchronously>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_sceneName_0() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___sceneName_0)); }
	inline String_t* get_sceneName_0() const { return ___sceneName_0; }
	inline String_t** get_address_of_sceneName_0() { return &___sceneName_0; }
	inline void set_sceneName_0(String_t* value)
	{
		___sceneName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_0), value);
	}

	inline static int32_t get_offset_of_U3CoperationU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U3CoperationU3E__0_1)); }
	inline AsyncOperation_t1445031843 * get_U3CoperationU3E__0_1() const { return ___U3CoperationU3E__0_1; }
	inline AsyncOperation_t1445031843 ** get_address_of_U3CoperationU3E__0_1() { return &___U3CoperationU3E__0_1; }
	inline void set_U3CoperationU3E__0_1(AsyncOperation_t1445031843 * value)
	{
		___U3CoperationU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoperationU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CPopU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U3CPopU3E__0_2)); }
	inline GameObject_t1113636619 * get_U3CPopU3E__0_2() const { return ___U3CPopU3E__0_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CPopU3E__0_2() { return &___U3CPopU3E__0_2; }
	inline void set_U3CPopU3E__0_2(GameObject_t1113636619 * value)
	{
		___U3CPopU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPopU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CprogressU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U3CprogressU3E__1_3)); }
	inline float get_U3CprogressU3E__1_3() const { return ___U3CprogressU3E__1_3; }
	inline float* get_address_of_U3CprogressU3E__1_3() { return &___U3CprogressU3E__1_3; }
	inline void set_U3CprogressU3E__1_3(float value)
	{
		___U3CprogressU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U24this_4)); }
	inline SceneLoader_t4130533360 * get_U24this_4() const { return ___U24this_4; }
	inline SceneLoader_t4130533360 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SceneLoader_t4130533360 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCHRONOUSLYU3EC__ITERATOR0_T4235513134_H
#ifndef U3CSETCHARMITEMSU3EC__ANONSTOREY0_T1426642183_H
#define U3CSETCHARMITEMSU3EC__ANONSTOREY0_T1426642183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeCharmPopup/<SetCharmItems>c__AnonStorey0
struct  U3CSetCharmItemsU3Ec__AnonStorey0_t1426642183  : public RuntimeObject
{
public:
	// Charm ChangeCharmPopup/<SetCharmItems>c__AnonStorey0::charm
	Charm_t3703444127 * ___charm_0;
	// ChangeCharmPopup ChangeCharmPopup/<SetCharmItems>c__AnonStorey0::$this
	ChangeCharmPopup_t1980703935 * ___U24this_1;

public:
	inline static int32_t get_offset_of_charm_0() { return static_cast<int32_t>(offsetof(U3CSetCharmItemsU3Ec__AnonStorey0_t1426642183, ___charm_0)); }
	inline Charm_t3703444127 * get_charm_0() const { return ___charm_0; }
	inline Charm_t3703444127 ** get_address_of_charm_0() { return &___charm_0; }
	inline void set_charm_0(Charm_t3703444127 * value)
	{
		___charm_0 = value;
		Il2CppCodeGenWriteBarrier((&___charm_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSetCharmItemsU3Ec__AnonStorey0_t1426642183, ___U24this_1)); }
	inline ChangeCharmPopup_t1980703935 * get_U24this_1() const { return ___U24this_1; }
	inline ChangeCharmPopup_t1980703935 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChangeCharmPopup_t1980703935 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETCHARMITEMSU3EC__ANONSTOREY0_T1426642183_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FSAOTCOMPILATIONMANAGER_T2784389809_H
#define FSAOTCOMPILATIONMANAGER_T2784389809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotCompilationManager
struct  fsAotCompilationManager_t2784389809  : public RuntimeObject
{
public:

public:
};

struct fsAotCompilationManager_t2784389809_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Type> FullSerializer.fsAotCompilationManager::AotCandidateTypes
	HashSet_1_t1048894234 * ___AotCandidateTypes_0;

public:
	inline static int32_t get_offset_of_AotCandidateTypes_0() { return static_cast<int32_t>(offsetof(fsAotCompilationManager_t2784389809_StaticFields, ___AotCandidateTypes_0)); }
	inline HashSet_1_t1048894234 * get_AotCandidateTypes_0() const { return ___AotCandidateTypes_0; }
	inline HashSet_1_t1048894234 ** get_address_of_AotCandidateTypes_0() { return &___AotCandidateTypes_0; }
	inline void set_AotCandidateTypes_0(HashSet_1_t1048894234 * value)
	{
		___AotCandidateTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___AotCandidateTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSAOTCOMPILATIONMANAGER_T2784389809_H
#ifndef GPGSIDS_T1675576834_H
#define GPGSIDS_T1675576834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPGSIds
struct  GPGSIds_t1675576834  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPGSIDS_T1675576834_H
#ifndef U3CAUTOKILLU3EC__ITERATOR0_T3340172795_H
#define U3CAUTOKILLU3EC__ITERATOR0_T3340172795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterPopup/<AutoKill>c__Iterator0
struct  U3CAutoKillU3Ec__Iterator0_t3340172795  : public RuntimeObject
{
public:
	// CharacterPopup CharacterPopup/<AutoKill>c__Iterator0::$this
	CharacterPopup_t2617494106 * ___U24this_0;
	// System.Object CharacterPopup/<AutoKill>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CharacterPopup/<AutoKill>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CharacterPopup/<AutoKill>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t3340172795, ___U24this_0)); }
	inline CharacterPopup_t2617494106 * get_U24this_0() const { return ___U24this_0; }
	inline CharacterPopup_t2617494106 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CharacterPopup_t2617494106 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t3340172795, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t3340172795, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t3340172795, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOKILLU3EC__ITERATOR0_T3340172795_H
#ifndef PLUGINVERSION_T2872281160_H
#define PLUGINVERSION_T2872281160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.PluginVersion
struct  PluginVersion_t2872281160  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINVERSION_T2872281160_H
#ifndef U3CRUNCOROUTINEU3EC__ANONSTOREY0_T3592917427_H
#define U3CRUNCOROUTINEU3EC__ANONSTOREY0_T3592917427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey0
struct  U3CRunCoroutineU3Ec__AnonStorey0_t3592917427  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey0::action
	RuntimeObject* ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CRunCoroutineU3Ec__AnonStorey0_t3592917427, ___action_0)); }
	inline RuntimeObject* get_action_0() const { return ___action_0; }
	inline RuntimeObject** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(RuntimeObject* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNCOROUTINEU3EC__ANONSTOREY0_T3592917427_H
#ifndef U3CAUTOKILLU3EC__ITERATOR0_T2318114076_H
#define U3CAUTOKILLU3EC__ITERATOR0_T2318114076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSchoolPopup/<AutoKill>c__Iterator0
struct  U3CAutoKillU3Ec__Iterator0_t2318114076  : public RuntimeObject
{
public:
	// GhostSchoolPopup GhostSchoolPopup/<AutoKill>c__Iterator0::$this
	GhostSchoolPopup_t1455275853 * ___U24this_0;
	// System.Object GhostSchoolPopup/<AutoKill>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GhostSchoolPopup/<AutoKill>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GhostSchoolPopup/<AutoKill>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t2318114076, ___U24this_0)); }
	inline GhostSchoolPopup_t1455275853 * get_U24this_0() const { return ___U24this_0; }
	inline GhostSchoolPopup_t1455275853 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GhostSchoolPopup_t1455275853 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t2318114076, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t2318114076, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAutoKillU3Ec__Iterator0_t2318114076, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOKILLU3EC__ITERATOR0_T2318114076_H
#ifndef TIMEINFO_T2264797512_H
#define TIMEINFO_T2264797512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeManager/TimeInfo
struct  TimeInfo_t2264797512  : public RuntimeObject
{
public:
	// System.String TimeManager/TimeInfo::currentDateTime
	String_t* ___currentDateTime_0;

public:
	inline static int32_t get_offset_of_currentDateTime_0() { return static_cast<int32_t>(offsetof(TimeInfo_t2264797512, ___currentDateTime_0)); }
	inline String_t* get_currentDateTime_0() const { return ___currentDateTime_0; }
	inline String_t** get_address_of_currentDateTime_0() { return &___currentDateTime_0; }
	inline void set_currentDateTime_0(String_t* value)
	{
		___currentDateTime_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentDateTime_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEINFO_T2264797512_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENDPOINTDETAILS_T3891698496_H
#define ENDPOINTDETAILS_T3891698496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct  EndpointDetails_t3891698496 
{
public:
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mEndpointId
	String_t* ___mEndpointId_0;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mName
	String_t* ___mName_1;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mServiceId
	String_t* ___mServiceId_2;

public:
	inline static int32_t get_offset_of_mEndpointId_0() { return static_cast<int32_t>(offsetof(EndpointDetails_t3891698496, ___mEndpointId_0)); }
	inline String_t* get_mEndpointId_0() const { return ___mEndpointId_0; }
	inline String_t** get_address_of_mEndpointId_0() { return &___mEndpointId_0; }
	inline void set_mEndpointId_0(String_t* value)
	{
		___mEndpointId_0 = value;
		Il2CppCodeGenWriteBarrier((&___mEndpointId_0), value);
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(EndpointDetails_t3891698496, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}

	inline static int32_t get_offset_of_mServiceId_2() { return static_cast<int32_t>(offsetof(EndpointDetails_t3891698496, ___mServiceId_2)); }
	inline String_t* get_mServiceId_2() const { return ___mServiceId_2; }
	inline String_t** get_address_of_mServiceId_2() { return &___mServiceId_2; }
	inline void set_mServiceId_2(String_t* value)
	{
		___mServiceId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mServiceId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_t3891698496_marshaled_pinvoke
{
	char* ___mEndpointId_0;
	char* ___mName_1;
	char* ___mServiceId_2;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_t3891698496_marshaled_com
{
	Il2CppChar* ___mEndpointId_0;
	Il2CppChar* ___mName_1;
	Il2CppChar* ___mServiceId_2;
};
#endif // ENDPOINTDETAILS_T3891698496_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef NEARBYCONNECTIONCONFIGURATION_T2019425596_H
#define NEARBYCONNECTIONCONFIGURATION_T2019425596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct  NearbyConnectionConfiguration_t2019425596 
{
public:
	// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::mInitializationCallback
	Action_1_t2609895709 * ___mInitializationCallback_2;
	// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::mLocalClientId
	int64_t ___mLocalClientId_3;

public:
	inline static int32_t get_offset_of_mInitializationCallback_2() { return static_cast<int32_t>(offsetof(NearbyConnectionConfiguration_t2019425596, ___mInitializationCallback_2)); }
	inline Action_1_t2609895709 * get_mInitializationCallback_2() const { return ___mInitializationCallback_2; }
	inline Action_1_t2609895709 ** get_address_of_mInitializationCallback_2() { return &___mInitializationCallback_2; }
	inline void set_mInitializationCallback_2(Action_1_t2609895709 * value)
	{
		___mInitializationCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInitializationCallback_2), value);
	}

	inline static int32_t get_offset_of_mLocalClientId_3() { return static_cast<int32_t>(offsetof(NearbyConnectionConfiguration_t2019425596, ___mLocalClientId_3)); }
	inline int64_t get_mLocalClientId_3() const { return ___mLocalClientId_3; }
	inline int64_t* get_address_of_mLocalClientId_3() { return &___mLocalClientId_3; }
	inline void set_mLocalClientId_3(int64_t value)
	{
		___mLocalClientId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t2019425596_marshaled_pinvoke
{
	Il2CppMethodPointer ___mInitializationCallback_2;
	int64_t ___mLocalClientId_3;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t2019425596_marshaled_com
{
	Il2CppMethodPointer ___mInitializationCallback_2;
	int64_t ___mLocalClientId_3;
};
#endif // NEARBYCONNECTIONCONFIGURATION_T2019425596_H
#ifndef VIDEOQUALITYLEVEL_T4283418091_H
#define VIDEOQUALITYLEVEL_T4283418091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoQualityLevel
struct  VideoQualityLevel_t4283418091 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoQualityLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoQualityLevel_t4283418091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOQUALITYLEVEL_T4283418091_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef GRAVITY_T1500868723_H
#define GRAVITY_T1500868723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Gravity
struct  Gravity_t1500868723 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Gravity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Gravity_t1500868723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAVITY_T1500868723_H
#ifndef VIDEOCAPTUREOVERLAYSTATE_T4111180056_H
#define VIDEOCAPTUREOVERLAYSTATE_T4111180056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoCaptureOverlayState
struct  VideoCaptureOverlayState_t4111180056 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureOverlayState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoCaptureOverlayState_t4111180056, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTUREOVERLAYSTATE_T4111180056_H
#ifndef PUZZLECOLOR_T2810668265_H
#define PUZZLECOLOR_T2810668265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleColor
struct  PuzzleColor_t2810668265 
{
public:
	// System.Int32 PuzzleColor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PuzzleColor_t2810668265, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLECOLOR_T2810668265_H
#ifndef VIDEOCAPTUREMODE_T1984088482_H
#define VIDEOCAPTUREMODE_T1984088482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.VideoCaptureMode
struct  VideoCaptureMode_t1984088482 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoCaptureMode_t1984088482, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTUREMODE_T1984088482_H
#ifndef LEADERBOARDCOLLECTION_T3003544407_H
#define LEADERBOARDCOLLECTION_T3003544407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.LeaderboardCollection
struct  LeaderboardCollection_t3003544407 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardCollection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LeaderboardCollection_t3003544407, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDCOLLECTION_T3003544407_H
#ifndef EVENTVISIBILITY_T3702936362_H
#define EVENTVISIBILITY_T3702936362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Events.EventVisibility
struct  EventVisibility_t3702936362 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Events.EventVisibility::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventVisibility_t3702936362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTVISIBILITY_T3702936362_H
#ifndef SELECTUISTATUS_T1293076877_H
#define SELECTUISTATUS_T1293076877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
struct  SelectUIStatus_t1293076877 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.SelectUIStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectUIStatus_t1293076877, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTUISTATUS_T1293076877_H
#ifndef SAVEDGAMEREQUESTSTATUS_T3745141777_H
#define SAVEDGAMEREQUESTSTATUS_T3745141777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
struct  SavedGameRequestStatus_t3745141777 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SavedGameRequestStatus_t3745141777, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEDGAMEREQUESTSTATUS_T3745141777_H
#ifndef CONFLICTRESOLUTIONSTRATEGY_T4255039905_H
#define CONFLICTRESOLUTIONSTRATEGY_T4255039905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
struct  ConflictResolutionStrategy_t4255039905 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConflictResolutionStrategy_t4255039905, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTRESOLUTIONSTRATEGY_T4255039905_H
#ifndef INITIALIZATIONSTATUS_T2437428114_H
#define INITIALIZATIONSTATUS_T2437428114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.InitializationStatus
struct  InitializationStatus_t2437428114 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Nearby.InitializationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitializationStatus_t2437428114, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZATIONSTATUS_T2437428114_H
#ifndef ROTATEMODE_T2548570174_H
#define ROTATEMODE_T2548570174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t2548570174 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2548570174, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2548570174_H
#ifndef STATUS_T465938950_H
#define STATUS_T465938950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
struct  Status_t465938950 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t465938950, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T465938950_H
#ifndef CONNECTIONREQUEST_T684574500_H
#define CONNECTIONREQUEST_T684574500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct  ConnectionRequest_t684574500 
{
public:
	// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mRemoteEndpoint
	EndpointDetails_t3891698496  ___mRemoteEndpoint_0;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mPayload
	ByteU5BU5D_t4116647657* ___mPayload_1;

public:
	inline static int32_t get_offset_of_mRemoteEndpoint_0() { return static_cast<int32_t>(offsetof(ConnectionRequest_t684574500, ___mRemoteEndpoint_0)); }
	inline EndpointDetails_t3891698496  get_mRemoteEndpoint_0() const { return ___mRemoteEndpoint_0; }
	inline EndpointDetails_t3891698496 * get_address_of_mRemoteEndpoint_0() { return &___mRemoteEndpoint_0; }
	inline void set_mRemoteEndpoint_0(EndpointDetails_t3891698496  value)
	{
		___mRemoteEndpoint_0 = value;
	}

	inline static int32_t get_offset_of_mPayload_1() { return static_cast<int32_t>(offsetof(ConnectionRequest_t684574500, ___mPayload_1)); }
	inline ByteU5BU5D_t4116647657* get_mPayload_1() const { return ___mPayload_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_mPayload_1() { return &___mPayload_1; }
	inline void set_mPayload_1(ByteU5BU5D_t4116647657* value)
	{
		___mPayload_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPayload_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_t684574500_marshaled_pinvoke
{
	EndpointDetails_t3891698496_marshaled_pinvoke ___mRemoteEndpoint_0;
	uint8_t* ___mPayload_1;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_t684574500_marshaled_com
{
	EndpointDetails_t3891698496_marshaled_com ___mRemoteEndpoint_0;
	uint8_t* ___mPayload_1;
};
#endif // CONNECTIONREQUEST_T684574500_H
#ifndef LEADERBOARDTIMESPAN_T1503936786_H
#define LEADERBOARDTIMESPAN_T1503936786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.LeaderboardTimeSpan
struct  LeaderboardTimeSpan_t1503936786 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardTimeSpan::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LeaderboardTimeSpan_t1503936786, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDTIMESPAN_T1503936786_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef BLOCKTYPE_T4096884630_H
#define BLOCKTYPE_T4096884630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Game.Common.BlockType
struct  BlockType_t4096884630 
{
public:
	// System.Int32 GameVanilla.Game.Common.BlockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockType_t4096884630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKTYPE_T4096884630_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CHARMTYPE_T3822141316_H
#define CHARMTYPE_T3822141316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmType
struct  CharmType_t3822141316 
{
public:
	// System.Int32 CharmType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharmType_t3822141316, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMTYPE_T3822141316_H
#ifndef DATASOURCE_T833220627_H
#define DATASOURCE_T833220627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.DataSource
struct  DataSource_t833220627 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.DataSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataSource_t833220627, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASOURCE_T833220627_H
#ifndef NULLABLE_1_T2603721331_H
#define NULLABLE_1_T2603721331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t2603721331 
{
public:
	// T System.Nullable`1::value
	TimeSpan_t881159249  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2603721331, ___value_0)); }
	inline TimeSpan_t881159249  get_value_0() const { return ___value_0; }
	inline TimeSpan_t881159249 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_t881159249  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2603721331, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2603721331_H
#ifndef LEADERBOARDSTART_T3259090716_H
#define LEADERBOARDSTART_T3259090716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.LeaderboardStart
struct  LeaderboardStart_t3259090716 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardStart::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LeaderboardStart_t3259090716, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDSTART_T3259090716_H
#ifndef DOTWEENANIMATIONTYPE_T2748799855_H
#define DOTWEENANIMATIONTYPE_T2748799855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenAnimationType
struct  DOTweenAnimationType_t2748799855 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenAnimationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DOTweenAnimationType_t2748799855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONTYPE_T2748799855_H
#ifndef TARGETTYPE_T3479356996_H
#define TARGETTYPE_T3479356996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TargetType
struct  TargetType_t3479356996 
{
public:
	// System.Int32 DG.Tweening.Core.TargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetType_t3479356996, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T3479356996_H
#ifndef UISTATUS_T582920877_H
#define UISTATUS_T582920877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.UIStatus
struct  UIStatus_t582920877 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.UIStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UIStatus_t582920877, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTATUS_T582920877_H
#ifndef RESPONSESTATUS_T4073530167_H
#define RESPONSESTATUS_T4073530167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.ResponseStatus
struct  ResponseStatus_t4073530167 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.ResponseStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResponseStatus_t4073530167, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSESTATUS_T4073530167_H
#ifndef BUILDER_T140438593_H
#define BUILDER_T140438593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct  Builder_t140438593 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mDescriptionUpdated
	bool ___mDescriptionUpdated_0;
	// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mNewDescription
	String_t* ___mNewDescription_1;
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mCoverImageUpdated
	bool ___mCoverImageUpdated_2;
	// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mNewPngCoverImage
	ByteU5BU5D_t4116647657* ___mNewPngCoverImage_3;
	// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::mNewPlayedTime
	Nullable_1_t2603721331  ___mNewPlayedTime_4;

public:
	inline static int32_t get_offset_of_mDescriptionUpdated_0() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mDescriptionUpdated_0)); }
	inline bool get_mDescriptionUpdated_0() const { return ___mDescriptionUpdated_0; }
	inline bool* get_address_of_mDescriptionUpdated_0() { return &___mDescriptionUpdated_0; }
	inline void set_mDescriptionUpdated_0(bool value)
	{
		___mDescriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of_mNewDescription_1() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mNewDescription_1)); }
	inline String_t* get_mNewDescription_1() const { return ___mNewDescription_1; }
	inline String_t** get_address_of_mNewDescription_1() { return &___mNewDescription_1; }
	inline void set_mNewDescription_1(String_t* value)
	{
		___mNewDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNewDescription_1), value);
	}

	inline static int32_t get_offset_of_mCoverImageUpdated_2() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mCoverImageUpdated_2)); }
	inline bool get_mCoverImageUpdated_2() const { return ___mCoverImageUpdated_2; }
	inline bool* get_address_of_mCoverImageUpdated_2() { return &___mCoverImageUpdated_2; }
	inline void set_mCoverImageUpdated_2(bool value)
	{
		___mCoverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mNewPngCoverImage_3() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mNewPngCoverImage_3)); }
	inline ByteU5BU5D_t4116647657* get_mNewPngCoverImage_3() const { return ___mNewPngCoverImage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_mNewPngCoverImage_3() { return &___mNewPngCoverImage_3; }
	inline void set_mNewPngCoverImage_3(ByteU5BU5D_t4116647657* value)
	{
		___mNewPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNewPngCoverImage_3), value);
	}

	inline static int32_t get_offset_of_mNewPlayedTime_4() { return static_cast<int32_t>(offsetof(Builder_t140438593, ___mNewPlayedTime_4)); }
	inline Nullable_1_t2603721331  get_mNewPlayedTime_4() const { return ___mNewPlayedTime_4; }
	inline Nullable_1_t2603721331 * get_address_of_mNewPlayedTime_4() { return &___mNewPlayedTime_4; }
	inline void set_mNewPlayedTime_4(Nullable_1_t2603721331  value)
	{
		___mNewPlayedTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t140438593_marshaled_pinvoke
{
	int32_t ___mDescriptionUpdated_0;
	char* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t140438593_marshaled_com
{
	int32_t ___mDescriptionUpdated_0;
	Il2CppChar* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
#endif // BUILDER_T140438593_H
#ifndef VIDEOCAPTURESTATE_T2350658599_H
#define VIDEOCAPTURESTATE_T2350658599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Video.VideoCaptureState
struct  VideoCaptureState_t2350658599  : public RuntimeObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsCapturing
	bool ___mIsCapturing_0;
	// GooglePlayGames.BasicApi.VideoCaptureMode GooglePlayGames.BasicApi.Video.VideoCaptureState::mCaptureMode
	int32_t ___mCaptureMode_1;
	// GooglePlayGames.BasicApi.VideoQualityLevel GooglePlayGames.BasicApi.Video.VideoCaptureState::mQualityLevel
	int32_t ___mQualityLevel_2;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsOverlayVisible
	bool ___mIsOverlayVisible_3;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsPaused
	bool ___mIsPaused_4;

public:
	inline static int32_t get_offset_of_mIsCapturing_0() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mIsCapturing_0)); }
	inline bool get_mIsCapturing_0() const { return ___mIsCapturing_0; }
	inline bool* get_address_of_mIsCapturing_0() { return &___mIsCapturing_0; }
	inline void set_mIsCapturing_0(bool value)
	{
		___mIsCapturing_0 = value;
	}

	inline static int32_t get_offset_of_mCaptureMode_1() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mCaptureMode_1)); }
	inline int32_t get_mCaptureMode_1() const { return ___mCaptureMode_1; }
	inline int32_t* get_address_of_mCaptureMode_1() { return &___mCaptureMode_1; }
	inline void set_mCaptureMode_1(int32_t value)
	{
		___mCaptureMode_1 = value;
	}

	inline static int32_t get_offset_of_mQualityLevel_2() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mQualityLevel_2)); }
	inline int32_t get_mQualityLevel_2() const { return ___mQualityLevel_2; }
	inline int32_t* get_address_of_mQualityLevel_2() { return &___mQualityLevel_2; }
	inline void set_mQualityLevel_2(int32_t value)
	{
		___mQualityLevel_2 = value;
	}

	inline static int32_t get_offset_of_mIsOverlayVisible_3() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mIsOverlayVisible_3)); }
	inline bool get_mIsOverlayVisible_3() const { return ___mIsOverlayVisible_3; }
	inline bool* get_address_of_mIsOverlayVisible_3() { return &___mIsOverlayVisible_3; }
	inline void set_mIsOverlayVisible_3(bool value)
	{
		___mIsOverlayVisible_3 = value;
	}

	inline static int32_t get_offset_of_mIsPaused_4() { return static_cast<int32_t>(offsetof(VideoCaptureState_t2350658599, ___mIsPaused_4)); }
	inline bool get_mIsPaused_4() const { return ___mIsPaused_4; }
	inline bool* get_address_of_mIsPaused_4() { return &___mIsPaused_4; }
	inline void set_mIsPaused_4(bool value)
	{
		___mIsPaused_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCAPTURESTATE_T2350658599_H
#ifndef INFO_T1004903773_H
#define INFO_T1004903773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Info
struct  Info_t1004903773  : public RuntimeObject
{
public:
	// System.Int32 Info::candy
	int32_t ___candy_0;
	// System.Int32 Info::maxCandy
	int32_t ___maxCandy_1;
	// System.String Info::nextCandyTime
	String_t* ___nextCandyTime_2;
	// System.Int32 Info::timeToNextCandy
	int32_t ___timeToNextCandy_3;
	// System.Int32 Info::puzzleLive
	int32_t ___puzzleLive_4;
	// System.Int32 Info::maxPuzzleLive
	int32_t ___maxPuzzleLive_5;
	// System.String Info::nextPuzzleLiveTime
	String_t* ___nextPuzzleLiveTime_6;
	// System.Int32 Info::timeToNextPuzzleLive
	int32_t ___timeToNextPuzzleLive_7;
	// CharmType Info::currentCharm
	int32_t ___currentCharm_8;
	// System.Collections.Generic.Dictionary`2<CharmType,System.Int32> Info::CharmDic
	Dictionary_2_t466317717 * ___CharmDic_9;
	// System.Int32 Info::maxCharm
	int32_t ___maxCharm_10;
	// System.String Info::nextCharmTime
	String_t* ___nextCharmTime_11;
	// System.Int32 Info::timeToNextCharm
	int32_t ___timeToNextCharm_12;
	// System.Int32 Info::itemCharm
	int32_t ___itemCharm_13;

public:
	inline static int32_t get_offset_of_candy_0() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___candy_0)); }
	inline int32_t get_candy_0() const { return ___candy_0; }
	inline int32_t* get_address_of_candy_0() { return &___candy_0; }
	inline void set_candy_0(int32_t value)
	{
		___candy_0 = value;
	}

	inline static int32_t get_offset_of_maxCandy_1() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___maxCandy_1)); }
	inline int32_t get_maxCandy_1() const { return ___maxCandy_1; }
	inline int32_t* get_address_of_maxCandy_1() { return &___maxCandy_1; }
	inline void set_maxCandy_1(int32_t value)
	{
		___maxCandy_1 = value;
	}

	inline static int32_t get_offset_of_nextCandyTime_2() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___nextCandyTime_2)); }
	inline String_t* get_nextCandyTime_2() const { return ___nextCandyTime_2; }
	inline String_t** get_address_of_nextCandyTime_2() { return &___nextCandyTime_2; }
	inline void set_nextCandyTime_2(String_t* value)
	{
		___nextCandyTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextCandyTime_2), value);
	}

	inline static int32_t get_offset_of_timeToNextCandy_3() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___timeToNextCandy_3)); }
	inline int32_t get_timeToNextCandy_3() const { return ___timeToNextCandy_3; }
	inline int32_t* get_address_of_timeToNextCandy_3() { return &___timeToNextCandy_3; }
	inline void set_timeToNextCandy_3(int32_t value)
	{
		___timeToNextCandy_3 = value;
	}

	inline static int32_t get_offset_of_puzzleLive_4() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___puzzleLive_4)); }
	inline int32_t get_puzzleLive_4() const { return ___puzzleLive_4; }
	inline int32_t* get_address_of_puzzleLive_4() { return &___puzzleLive_4; }
	inline void set_puzzleLive_4(int32_t value)
	{
		___puzzleLive_4 = value;
	}

	inline static int32_t get_offset_of_maxPuzzleLive_5() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___maxPuzzleLive_5)); }
	inline int32_t get_maxPuzzleLive_5() const { return ___maxPuzzleLive_5; }
	inline int32_t* get_address_of_maxPuzzleLive_5() { return &___maxPuzzleLive_5; }
	inline void set_maxPuzzleLive_5(int32_t value)
	{
		___maxPuzzleLive_5 = value;
	}

	inline static int32_t get_offset_of_nextPuzzleLiveTime_6() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___nextPuzzleLiveTime_6)); }
	inline String_t* get_nextPuzzleLiveTime_6() const { return ___nextPuzzleLiveTime_6; }
	inline String_t** get_address_of_nextPuzzleLiveTime_6() { return &___nextPuzzleLiveTime_6; }
	inline void set_nextPuzzleLiveTime_6(String_t* value)
	{
		___nextPuzzleLiveTime_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextPuzzleLiveTime_6), value);
	}

	inline static int32_t get_offset_of_timeToNextPuzzleLive_7() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___timeToNextPuzzleLive_7)); }
	inline int32_t get_timeToNextPuzzleLive_7() const { return ___timeToNextPuzzleLive_7; }
	inline int32_t* get_address_of_timeToNextPuzzleLive_7() { return &___timeToNextPuzzleLive_7; }
	inline void set_timeToNextPuzzleLive_7(int32_t value)
	{
		___timeToNextPuzzleLive_7 = value;
	}

	inline static int32_t get_offset_of_currentCharm_8() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___currentCharm_8)); }
	inline int32_t get_currentCharm_8() const { return ___currentCharm_8; }
	inline int32_t* get_address_of_currentCharm_8() { return &___currentCharm_8; }
	inline void set_currentCharm_8(int32_t value)
	{
		___currentCharm_8 = value;
	}

	inline static int32_t get_offset_of_CharmDic_9() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___CharmDic_9)); }
	inline Dictionary_2_t466317717 * get_CharmDic_9() const { return ___CharmDic_9; }
	inline Dictionary_2_t466317717 ** get_address_of_CharmDic_9() { return &___CharmDic_9; }
	inline void set_CharmDic_9(Dictionary_2_t466317717 * value)
	{
		___CharmDic_9 = value;
		Il2CppCodeGenWriteBarrier((&___CharmDic_9), value);
	}

	inline static int32_t get_offset_of_maxCharm_10() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___maxCharm_10)); }
	inline int32_t get_maxCharm_10() const { return ___maxCharm_10; }
	inline int32_t* get_address_of_maxCharm_10() { return &___maxCharm_10; }
	inline void set_maxCharm_10(int32_t value)
	{
		___maxCharm_10 = value;
	}

	inline static int32_t get_offset_of_nextCharmTime_11() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___nextCharmTime_11)); }
	inline String_t* get_nextCharmTime_11() const { return ___nextCharmTime_11; }
	inline String_t** get_address_of_nextCharmTime_11() { return &___nextCharmTime_11; }
	inline void set_nextCharmTime_11(String_t* value)
	{
		___nextCharmTime_11 = value;
		Il2CppCodeGenWriteBarrier((&___nextCharmTime_11), value);
	}

	inline static int32_t get_offset_of_timeToNextCharm_12() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___timeToNextCharm_12)); }
	inline int32_t get_timeToNextCharm_12() const { return ___timeToNextCharm_12; }
	inline int32_t* get_address_of_timeToNextCharm_12() { return &___timeToNextCharm_12; }
	inline void set_timeToNextCharm_12(int32_t value)
	{
		___timeToNextCharm_12 = value;
	}

	inline static int32_t get_offset_of_itemCharm_13() { return static_cast<int32_t>(offsetof(Info_t1004903773, ___itemCharm_13)); }
	inline int32_t get_itemCharm_13() const { return ___itemCharm_13; }
	inline int32_t* get_address_of_itemCharm_13() { return &___itemCharm_13; }
	inline void set_itemCharm_13(int32_t value)
	{
		___itemCharm_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFO_T1004903773_H
#ifndef SAVEDGAMEMETADATAUPDATE_T1775293339_H
#define SAVEDGAMEMETADATAUPDATE_T1775293339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct  SavedGameMetadataUpdate_t1775293339 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mDescriptionUpdated
	bool ___mDescriptionUpdated_0;
	// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewDescription
	String_t* ___mNewDescription_1;
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mCoverImageUpdated
	bool ___mCoverImageUpdated_2;
	// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewPngCoverImage
	ByteU5BU5D_t4116647657* ___mNewPngCoverImage_3;
	// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewPlayedTime
	Nullable_1_t2603721331  ___mNewPlayedTime_4;

public:
	inline static int32_t get_offset_of_mDescriptionUpdated_0() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mDescriptionUpdated_0)); }
	inline bool get_mDescriptionUpdated_0() const { return ___mDescriptionUpdated_0; }
	inline bool* get_address_of_mDescriptionUpdated_0() { return &___mDescriptionUpdated_0; }
	inline void set_mDescriptionUpdated_0(bool value)
	{
		___mDescriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of_mNewDescription_1() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mNewDescription_1)); }
	inline String_t* get_mNewDescription_1() const { return ___mNewDescription_1; }
	inline String_t** get_address_of_mNewDescription_1() { return &___mNewDescription_1; }
	inline void set_mNewDescription_1(String_t* value)
	{
		___mNewDescription_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNewDescription_1), value);
	}

	inline static int32_t get_offset_of_mCoverImageUpdated_2() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mCoverImageUpdated_2)); }
	inline bool get_mCoverImageUpdated_2() const { return ___mCoverImageUpdated_2; }
	inline bool* get_address_of_mCoverImageUpdated_2() { return &___mCoverImageUpdated_2; }
	inline void set_mCoverImageUpdated_2(bool value)
	{
		___mCoverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mNewPngCoverImage_3() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mNewPngCoverImage_3)); }
	inline ByteU5BU5D_t4116647657* get_mNewPngCoverImage_3() const { return ___mNewPngCoverImage_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_mNewPngCoverImage_3() { return &___mNewPngCoverImage_3; }
	inline void set_mNewPngCoverImage_3(ByteU5BU5D_t4116647657* value)
	{
		___mNewPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNewPngCoverImage_3), value);
	}

	inline static int32_t get_offset_of_mNewPlayedTime_4() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_t1775293339, ___mNewPlayedTime_4)); }
	inline Nullable_1_t2603721331  get_mNewPlayedTime_4() const { return ___mNewPlayedTime_4; }
	inline Nullable_1_t2603721331 * get_address_of_mNewPlayedTime_4() { return &___mNewPlayedTime_4; }
	inline void set_mNewPlayedTime_4(Nullable_1_t2603721331  value)
	{
		___mNewPlayedTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_t1775293339_marshaled_pinvoke
{
	int32_t ___mDescriptionUpdated_0;
	char* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_t1775293339_marshaled_com
{
	int32_t ___mDescriptionUpdated_0;
	Il2CppChar* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	uint8_t* ___mNewPngCoverImage_3;
	Nullable_1_t2603721331  ___mNewPlayedTime_4;
};
#endif // SAVEDGAMEMETADATAUPDATE_T1775293339_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ADVERTISINGRESULT_T1229207569_H
#define ADVERTISINGRESULT_T1229207569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct  AdvertisingResult_t1229207569 
{
public:
	// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mStatus
	int32_t ___mStatus_0;
	// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mLocalEndpointName
	String_t* ___mLocalEndpointName_1;

public:
	inline static int32_t get_offset_of_mStatus_0() { return static_cast<int32_t>(offsetof(AdvertisingResult_t1229207569, ___mStatus_0)); }
	inline int32_t get_mStatus_0() const { return ___mStatus_0; }
	inline int32_t* get_address_of_mStatus_0() { return &___mStatus_0; }
	inline void set_mStatus_0(int32_t value)
	{
		___mStatus_0 = value;
	}

	inline static int32_t get_offset_of_mLocalEndpointName_1() { return static_cast<int32_t>(offsetof(AdvertisingResult_t1229207569, ___mLocalEndpointName_1)); }
	inline String_t* get_mLocalEndpointName_1() const { return ___mLocalEndpointName_1; }
	inline String_t** get_address_of_mLocalEndpointName_1() { return &___mLocalEndpointName_1; }
	inline void set_mLocalEndpointName_1(String_t* value)
	{
		___mLocalEndpointName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mLocalEndpointName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t1229207569_marshaled_pinvoke
{
	int32_t ___mStatus_0;
	char* ___mLocalEndpointName_1;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t1229207569_marshaled_com
{
	int32_t ___mStatus_0;
	Il2CppChar* ___mLocalEndpointName_1;
};
#endif // ADVERTISINGRESULT_T1229207569_H
#ifndef CONNECTIONRESPONSE_T735328601_H
#define CONNECTIONRESPONSE_T735328601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct  ConnectionResponse_t735328601 
{
public:
	// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mLocalClientId
	int64_t ___mLocalClientId_1;
	// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mRemoteEndpointId
	String_t* ___mRemoteEndpointId_2;
	// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mResponseStatus
	int32_t ___mResponseStatus_3;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mPayload
	ByteU5BU5D_t4116647657* ___mPayload_4;

public:
	inline static int32_t get_offset_of_mLocalClientId_1() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mLocalClientId_1)); }
	inline int64_t get_mLocalClientId_1() const { return ___mLocalClientId_1; }
	inline int64_t* get_address_of_mLocalClientId_1() { return &___mLocalClientId_1; }
	inline void set_mLocalClientId_1(int64_t value)
	{
		___mLocalClientId_1 = value;
	}

	inline static int32_t get_offset_of_mRemoteEndpointId_2() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mRemoteEndpointId_2)); }
	inline String_t* get_mRemoteEndpointId_2() const { return ___mRemoteEndpointId_2; }
	inline String_t** get_address_of_mRemoteEndpointId_2() { return &___mRemoteEndpointId_2; }
	inline void set_mRemoteEndpointId_2(String_t* value)
	{
		___mRemoteEndpointId_2 = value;
		Il2CppCodeGenWriteBarrier((&___mRemoteEndpointId_2), value);
	}

	inline static int32_t get_offset_of_mResponseStatus_3() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mResponseStatus_3)); }
	inline int32_t get_mResponseStatus_3() const { return ___mResponseStatus_3; }
	inline int32_t* get_address_of_mResponseStatus_3() { return &___mResponseStatus_3; }
	inline void set_mResponseStatus_3(int32_t value)
	{
		___mResponseStatus_3 = value;
	}

	inline static int32_t get_offset_of_mPayload_4() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601, ___mPayload_4)); }
	inline ByteU5BU5D_t4116647657* get_mPayload_4() const { return ___mPayload_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_mPayload_4() { return &___mPayload_4; }
	inline void set_mPayload_4(ByteU5BU5D_t4116647657* value)
	{
		___mPayload_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPayload_4), value);
	}
};

struct ConnectionResponse_t735328601_StaticFields
{
public:
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EmptyPayload
	ByteU5BU5D_t4116647657* ___EmptyPayload_0;

public:
	inline static int32_t get_offset_of_EmptyPayload_0() { return static_cast<int32_t>(offsetof(ConnectionResponse_t735328601_StaticFields, ___EmptyPayload_0)); }
	inline ByteU5BU5D_t4116647657* get_EmptyPayload_0() const { return ___EmptyPayload_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_EmptyPayload_0() { return &___EmptyPayload_0; }
	inline void set_EmptyPayload_0(ByteU5BU5D_t4116647657* value)
	{
		___EmptyPayload_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyPayload_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_t735328601_marshaled_pinvoke
{
	int64_t ___mLocalClientId_1;
	char* ___mRemoteEndpointId_2;
	int32_t ___mResponseStatus_3;
	uint8_t* ___mPayload_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_t735328601_marshaled_com
{
	int64_t ___mLocalClientId_1;
	Il2CppChar* ___mRemoteEndpointId_2;
	int32_t ___mResponseStatus_3;
	uint8_t* ___mPayload_4;
};
#endif // CONNECTIONRESPONSE_T735328601_H
#ifndef CONFLICTCALLBACK_T4045994657_H
#define CONFLICTCALLBACK_T4045994657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct  ConflictCallback_t4045994657  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFLICTCALLBACK_T4045994657_H
#ifndef FSAOTCONFIGURATION_T27208811_H
#define FSAOTCONFIGURATION_T27208811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotConfiguration
struct  fsAotConfiguration_t27208811  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<FullSerializer.fsAotConfiguration/Entry> FullSerializer.fsAotConfiguration::aotTypes
	List_1_t431521422 * ___aotTypes_2;
	// System.String FullSerializer.fsAotConfiguration::outputDirectory
	String_t* ___outputDirectory_3;

public:
	inline static int32_t get_offset_of_aotTypes_2() { return static_cast<int32_t>(offsetof(fsAotConfiguration_t27208811, ___aotTypes_2)); }
	inline List_1_t431521422 * get_aotTypes_2() const { return ___aotTypes_2; }
	inline List_1_t431521422 ** get_address_of_aotTypes_2() { return &___aotTypes_2; }
	inline void set_aotTypes_2(List_1_t431521422 * value)
	{
		___aotTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___aotTypes_2), value);
	}

	inline static int32_t get_offset_of_outputDirectory_3() { return static_cast<int32_t>(offsetof(fsAotConfiguration_t27208811, ___outputDirectory_3)); }
	inline String_t* get_outputDirectory_3() const { return ___outputDirectory_3; }
	inline String_t** get_address_of_outputDirectory_3() { return &___outputDirectory_3; }
	inline void set_outputDirectory_3(String_t* value)
	{
		___outputDirectory_3 = value;
		Il2CppCodeGenWriteBarrier((&___outputDirectory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSAOTCONFIGURATION_T27208811_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PLAYER_T3266647312_H
#define PLAYER_T3266647312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t3266647312  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Player_t3266647312_StaticFields
{
public:
	// Player Player::Instance
	Player_t3266647312 * ___Instance_2;
	// Info Player::_info
	Info_t1004903773 * ____info_3;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ___Instance_2)); }
	inline Player_t3266647312 * get_Instance_2() const { return ___Instance_2; }
	inline Player_t3266647312 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(Player_t3266647312 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}

	inline static int32_t get_offset_of__info_3() { return static_cast<int32_t>(offsetof(Player_t3266647312_StaticFields, ____info_3)); }
	inline Info_t1004903773 * get__info_3() const { return ____info_3; }
	inline Info_t1004903773 ** get_address_of__info_3() { return &____info_3; }
	inline void set__info_3(Info_t1004903773 * value)
	{
		____info_3 = value;
		Il2CppCodeGenWriteBarrier((&____info_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T3266647312_H
#ifndef TIMESYSTEM_T3122972587_H
#define TIMESYSTEM_T3122972587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeSystem
struct  TimeSystem_t3122972587  : public MonoBehaviour_t3962482529
{
public:
	// System.TimeSpan TimeSystem::timeSpan
	TimeSpan_t881159249  ___timeSpan_2;
	// System.Boolean TimeSystem::runningCountdown
	bool ___runningCountdown_3;
	// System.Action`2<System.TimeSpan,System.Int32> TimeSystem::onCountdownUpdated
	Action_2_t3788266618 * ___onCountdownUpdated_4;
	// System.Action TimeSystem::onCountdownFinished
	Action_t1264377477 * ___onCountdownFinished_5;

public:
	inline static int32_t get_offset_of_timeSpan_2() { return static_cast<int32_t>(offsetof(TimeSystem_t3122972587, ___timeSpan_2)); }
	inline TimeSpan_t881159249  get_timeSpan_2() const { return ___timeSpan_2; }
	inline TimeSpan_t881159249 * get_address_of_timeSpan_2() { return &___timeSpan_2; }
	inline void set_timeSpan_2(TimeSpan_t881159249  value)
	{
		___timeSpan_2 = value;
	}

	inline static int32_t get_offset_of_runningCountdown_3() { return static_cast<int32_t>(offsetof(TimeSystem_t3122972587, ___runningCountdown_3)); }
	inline bool get_runningCountdown_3() const { return ___runningCountdown_3; }
	inline bool* get_address_of_runningCountdown_3() { return &___runningCountdown_3; }
	inline void set_runningCountdown_3(bool value)
	{
		___runningCountdown_3 = value;
	}

	inline static int32_t get_offset_of_onCountdownUpdated_4() { return static_cast<int32_t>(offsetof(TimeSystem_t3122972587, ___onCountdownUpdated_4)); }
	inline Action_2_t3788266618 * get_onCountdownUpdated_4() const { return ___onCountdownUpdated_4; }
	inline Action_2_t3788266618 ** get_address_of_onCountdownUpdated_4() { return &___onCountdownUpdated_4; }
	inline void set_onCountdownUpdated_4(Action_2_t3788266618 * value)
	{
		___onCountdownUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCountdownUpdated_4), value);
	}

	inline static int32_t get_offset_of_onCountdownFinished_5() { return static_cast<int32_t>(offsetof(TimeSystem_t3122972587, ___onCountdownFinished_5)); }
	inline Action_t1264377477 * get_onCountdownFinished_5() const { return ___onCountdownFinished_5; }
	inline Action_t1264377477 ** get_address_of_onCountdownFinished_5() { return &___onCountdownFinished_5; }
	inline void set_onCountdownFinished_5(Action_t1264377477 * value)
	{
		___onCountdownFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCountdownFinished_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESYSTEM_T3122972587_H
#ifndef TIMEMANAGER_T1960693005_H
#define TIMEMANAGER_T1960693005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeManager
struct  TimeManager_t1960693005  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TimeManager::slowdownFactor
	float ___slowdownFactor_2;
	// System.Single TimeManager::slowdownLength
	float ___slowdownLength_3;
	// System.Boolean TimeManager::isSlowMotion
	bool ___isSlowMotion_4;
	// PuzzleLiveTimer TimeManager::liveTimer
	PuzzleLiveTimer_t464531872 * ___liveTimer_5;
	// CandyTimer TimeManager::candyTimer
	CandyTimer_t2979372557 * ___candyTimer_6;
	// CharmTimer TimeManager::charmTimer
	CharmTimer_t90502612 * ___charmTimer_7;

public:
	inline static int32_t get_offset_of_slowdownFactor_2() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ___slowdownFactor_2)); }
	inline float get_slowdownFactor_2() const { return ___slowdownFactor_2; }
	inline float* get_address_of_slowdownFactor_2() { return &___slowdownFactor_2; }
	inline void set_slowdownFactor_2(float value)
	{
		___slowdownFactor_2 = value;
	}

	inline static int32_t get_offset_of_slowdownLength_3() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ___slowdownLength_3)); }
	inline float get_slowdownLength_3() const { return ___slowdownLength_3; }
	inline float* get_address_of_slowdownLength_3() { return &___slowdownLength_3; }
	inline void set_slowdownLength_3(float value)
	{
		___slowdownLength_3 = value;
	}

	inline static int32_t get_offset_of_isSlowMotion_4() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ___isSlowMotion_4)); }
	inline bool get_isSlowMotion_4() const { return ___isSlowMotion_4; }
	inline bool* get_address_of_isSlowMotion_4() { return &___isSlowMotion_4; }
	inline void set_isSlowMotion_4(bool value)
	{
		___isSlowMotion_4 = value;
	}

	inline static int32_t get_offset_of_liveTimer_5() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ___liveTimer_5)); }
	inline PuzzleLiveTimer_t464531872 * get_liveTimer_5() const { return ___liveTimer_5; }
	inline PuzzleLiveTimer_t464531872 ** get_address_of_liveTimer_5() { return &___liveTimer_5; }
	inline void set_liveTimer_5(PuzzleLiveTimer_t464531872 * value)
	{
		___liveTimer_5 = value;
		Il2CppCodeGenWriteBarrier((&___liveTimer_5), value);
	}

	inline static int32_t get_offset_of_candyTimer_6() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ___candyTimer_6)); }
	inline CandyTimer_t2979372557 * get_candyTimer_6() const { return ___candyTimer_6; }
	inline CandyTimer_t2979372557 ** get_address_of_candyTimer_6() { return &___candyTimer_6; }
	inline void set_candyTimer_6(CandyTimer_t2979372557 * value)
	{
		___candyTimer_6 = value;
		Il2CppCodeGenWriteBarrier((&___candyTimer_6), value);
	}

	inline static int32_t get_offset_of_charmTimer_7() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ___charmTimer_7)); }
	inline CharmTimer_t90502612 * get_charmTimer_7() const { return ___charmTimer_7; }
	inline CharmTimer_t90502612 ** get_address_of_charmTimer_7() { return &___charmTimer_7; }
	inline void set_charmTimer_7(CharmTimer_t90502612 * value)
	{
		___charmTimer_7 = value;
		Il2CppCodeGenWriteBarrier((&___charmTimer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEMANAGER_T1960693005_H
#ifndef POPUP_T1063720813_H
#define POPUP_T1063720813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.Popup
struct  Popup_t1063720813  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Core.BaseScene GameVanilla.Core.Popup::parentScene
	BaseScene_t2918414559 * ___parentScene_2;
	// UnityEngine.Events.UnityEvent GameVanilla.Core.Popup::onOpen
	UnityEvent_t2581268647 * ___onOpen_3;
	// UnityEngine.Events.UnityEvent GameVanilla.Core.Popup::onClose
	UnityEvent_t2581268647 * ___onClose_4;
	// UnityEngine.Animator GameVanilla.Core.Popup::animator
	Animator_t434523843 * ___animator_5;

public:
	inline static int32_t get_offset_of_parentScene_2() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___parentScene_2)); }
	inline BaseScene_t2918414559 * get_parentScene_2() const { return ___parentScene_2; }
	inline BaseScene_t2918414559 ** get_address_of_parentScene_2() { return &___parentScene_2; }
	inline void set_parentScene_2(BaseScene_t2918414559 * value)
	{
		___parentScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentScene_2), value);
	}

	inline static int32_t get_offset_of_onOpen_3() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___onOpen_3)); }
	inline UnityEvent_t2581268647 * get_onOpen_3() const { return ___onOpen_3; }
	inline UnityEvent_t2581268647 ** get_address_of_onOpen_3() { return &___onOpen_3; }
	inline void set_onOpen_3(UnityEvent_t2581268647 * value)
	{
		___onOpen_3 = value;
		Il2CppCodeGenWriteBarrier((&___onOpen_3), value);
	}

	inline static int32_t get_offset_of_onClose_4() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___onClose_4)); }
	inline UnityEvent_t2581268647 * get_onClose_4() const { return ___onClose_4; }
	inline UnityEvent_t2581268647 ** get_address_of_onClose_4() { return &___onClose_4; }
	inline void set_onClose_4(UnityEvent_t2581268647 * value)
	{
		___onClose_4 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_4), value);
	}

	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Popup_t1063720813, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUP_T1063720813_H
#ifndef CHANGECHARMPOPUP_T1980703935_H
#define CHANGECHARMPOPUP_T1980703935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeCharmPopup
struct  ChangeCharmPopup_t1980703935  : public MonoBehaviour_t3962482529
{
public:
	// CharmGameManager ChangeCharmPopup::charmGameManager
	CharmGameManager_t204340045 * ___charmGameManager_2;
	// ChangeCharmItem[] ChangeCharmPopup::charmList
	ChangeCharmItemU5BU5D_t1024856007* ___charmList_3;
	// UnityEngine.GameObject ChangeCharmPopup::CharmShopPopup
	GameObject_t1113636619 * ___CharmShopPopup_4;
	// System.Boolean ChangeCharmPopup::isCharging
	bool ___isCharging_5;

public:
	inline static int32_t get_offset_of_charmGameManager_2() { return static_cast<int32_t>(offsetof(ChangeCharmPopup_t1980703935, ___charmGameManager_2)); }
	inline CharmGameManager_t204340045 * get_charmGameManager_2() const { return ___charmGameManager_2; }
	inline CharmGameManager_t204340045 ** get_address_of_charmGameManager_2() { return &___charmGameManager_2; }
	inline void set_charmGameManager_2(CharmGameManager_t204340045 * value)
	{
		___charmGameManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___charmGameManager_2), value);
	}

	inline static int32_t get_offset_of_charmList_3() { return static_cast<int32_t>(offsetof(ChangeCharmPopup_t1980703935, ___charmList_3)); }
	inline ChangeCharmItemU5BU5D_t1024856007* get_charmList_3() const { return ___charmList_3; }
	inline ChangeCharmItemU5BU5D_t1024856007** get_address_of_charmList_3() { return &___charmList_3; }
	inline void set_charmList_3(ChangeCharmItemU5BU5D_t1024856007* value)
	{
		___charmList_3 = value;
		Il2CppCodeGenWriteBarrier((&___charmList_3), value);
	}

	inline static int32_t get_offset_of_CharmShopPopup_4() { return static_cast<int32_t>(offsetof(ChangeCharmPopup_t1980703935, ___CharmShopPopup_4)); }
	inline GameObject_t1113636619 * get_CharmShopPopup_4() const { return ___CharmShopPopup_4; }
	inline GameObject_t1113636619 ** get_address_of_CharmShopPopup_4() { return &___CharmShopPopup_4; }
	inline void set_CharmShopPopup_4(GameObject_t1113636619 * value)
	{
		___CharmShopPopup_4 = value;
		Il2CppCodeGenWriteBarrier((&___CharmShopPopup_4), value);
	}

	inline static int32_t get_offset_of_isCharging_5() { return static_cast<int32_t>(offsetof(ChangeCharmPopup_t1980703935, ___isCharging_5)); }
	inline bool get_isCharging_5() const { return ___isCharging_5; }
	inline bool* get_address_of_isCharging_5() { return &___isCharging_5; }
	inline void set_isCharging_5(bool value)
	{
		___isCharging_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGECHARMPOPUP_T1980703935_H
#ifndef CHANGECHARMITEM_T2112099122_H
#define CHANGECHARMITEM_T2112099122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeCharmItem
struct  ChangeCharmItem_t2112099122  : public MonoBehaviour_t3962482529
{
public:
	// Charm ChangeCharmItem::charm
	Charm_t3703444127 * ___charm_2;
	// CharmType ChangeCharmItem::charmType
	int32_t ___charmType_3;
	// UnityEngine.UI.Text ChangeCharmItem::titleText
	Text_t1901882714 * ___titleText_4;
	// UnityEngine.UI.Image ChangeCharmItem::charmImage
	Image_t2670269651 * ___charmImage_5;
	// UnityEngine.UI.Text ChangeCharmItem::damageText
	Text_t1901882714 * ___damageText_6;
	// UnityEngine.UI.Text ChangeCharmItem::countText
	Text_t1901882714 * ___countText_7;
	// UnityEngine.UI.Button ChangeCharmItem::usingButton
	Button_t4055032469 * ___usingButton_8;
	// UnityEngine.UI.Button ChangeCharmItem::changeButton
	Button_t4055032469 * ___changeButton_9;
	// UnityEngine.UI.Button ChangeCharmItem::chargeButton
	Button_t4055032469 * ___chargeButton_10;
	// UnityEngine.GameObject ChangeCharmItem::chargeTimer
	GameObject_t1113636619 * ___chargeTimer_11;
	// UnityEngine.UI.Text ChangeCharmItem::chargeTimerText
	Text_t1901882714 * ___chargeTimerText_12;

public:
	inline static int32_t get_offset_of_charm_2() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___charm_2)); }
	inline Charm_t3703444127 * get_charm_2() const { return ___charm_2; }
	inline Charm_t3703444127 ** get_address_of_charm_2() { return &___charm_2; }
	inline void set_charm_2(Charm_t3703444127 * value)
	{
		___charm_2 = value;
		Il2CppCodeGenWriteBarrier((&___charm_2), value);
	}

	inline static int32_t get_offset_of_charmType_3() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___charmType_3)); }
	inline int32_t get_charmType_3() const { return ___charmType_3; }
	inline int32_t* get_address_of_charmType_3() { return &___charmType_3; }
	inline void set_charmType_3(int32_t value)
	{
		___charmType_3 = value;
	}

	inline static int32_t get_offset_of_titleText_4() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___titleText_4)); }
	inline Text_t1901882714 * get_titleText_4() const { return ___titleText_4; }
	inline Text_t1901882714 ** get_address_of_titleText_4() { return &___titleText_4; }
	inline void set_titleText_4(Text_t1901882714 * value)
	{
		___titleText_4 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_4), value);
	}

	inline static int32_t get_offset_of_charmImage_5() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___charmImage_5)); }
	inline Image_t2670269651 * get_charmImage_5() const { return ___charmImage_5; }
	inline Image_t2670269651 ** get_address_of_charmImage_5() { return &___charmImage_5; }
	inline void set_charmImage_5(Image_t2670269651 * value)
	{
		___charmImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___charmImage_5), value);
	}

	inline static int32_t get_offset_of_damageText_6() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___damageText_6)); }
	inline Text_t1901882714 * get_damageText_6() const { return ___damageText_6; }
	inline Text_t1901882714 ** get_address_of_damageText_6() { return &___damageText_6; }
	inline void set_damageText_6(Text_t1901882714 * value)
	{
		___damageText_6 = value;
		Il2CppCodeGenWriteBarrier((&___damageText_6), value);
	}

	inline static int32_t get_offset_of_countText_7() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___countText_7)); }
	inline Text_t1901882714 * get_countText_7() const { return ___countText_7; }
	inline Text_t1901882714 ** get_address_of_countText_7() { return &___countText_7; }
	inline void set_countText_7(Text_t1901882714 * value)
	{
		___countText_7 = value;
		Il2CppCodeGenWriteBarrier((&___countText_7), value);
	}

	inline static int32_t get_offset_of_usingButton_8() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___usingButton_8)); }
	inline Button_t4055032469 * get_usingButton_8() const { return ___usingButton_8; }
	inline Button_t4055032469 ** get_address_of_usingButton_8() { return &___usingButton_8; }
	inline void set_usingButton_8(Button_t4055032469 * value)
	{
		___usingButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___usingButton_8), value);
	}

	inline static int32_t get_offset_of_changeButton_9() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___changeButton_9)); }
	inline Button_t4055032469 * get_changeButton_9() const { return ___changeButton_9; }
	inline Button_t4055032469 ** get_address_of_changeButton_9() { return &___changeButton_9; }
	inline void set_changeButton_9(Button_t4055032469 * value)
	{
		___changeButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___changeButton_9), value);
	}

	inline static int32_t get_offset_of_chargeButton_10() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___chargeButton_10)); }
	inline Button_t4055032469 * get_chargeButton_10() const { return ___chargeButton_10; }
	inline Button_t4055032469 ** get_address_of_chargeButton_10() { return &___chargeButton_10; }
	inline void set_chargeButton_10(Button_t4055032469 * value)
	{
		___chargeButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___chargeButton_10), value);
	}

	inline static int32_t get_offset_of_chargeTimer_11() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___chargeTimer_11)); }
	inline GameObject_t1113636619 * get_chargeTimer_11() const { return ___chargeTimer_11; }
	inline GameObject_t1113636619 ** get_address_of_chargeTimer_11() { return &___chargeTimer_11; }
	inline void set_chargeTimer_11(GameObject_t1113636619 * value)
	{
		___chargeTimer_11 = value;
		Il2CppCodeGenWriteBarrier((&___chargeTimer_11), value);
	}

	inline static int32_t get_offset_of_chargeTimerText_12() { return static_cast<int32_t>(offsetof(ChangeCharmItem_t2112099122, ___chargeTimerText_12)); }
	inline Text_t1901882714 * get_chargeTimerText_12() const { return ___chargeTimerText_12; }
	inline Text_t1901882714 ** get_address_of_chargeTimerText_12() { return &___chargeTimerText_12; }
	inline void set_chargeTimerText_12(Text_t1901882714 * value)
	{
		___chargeTimerText_12 = value;
		Il2CppCodeGenWriteBarrier((&___chargeTimerText_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGECHARMITEM_T2112099122_H
#ifndef SCENELOADER_T4130533360_H
#define SCENELOADER_T4130533360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLoader
struct  SceneLoader_t4130533360  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SceneLoader::loadingScreen
	GameObject_t1113636619 * ___loadingScreen_3;
	// UnityEngine.UI.Slider SceneLoader::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.UI.Text SceneLoader::text
	Text_t1901882714 * ___text_5;

public:
	inline static int32_t get_offset_of_loadingScreen_3() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___loadingScreen_3)); }
	inline GameObject_t1113636619 * get_loadingScreen_3() const { return ___loadingScreen_3; }
	inline GameObject_t1113636619 ** get_address_of_loadingScreen_3() { return &___loadingScreen_3; }
	inline void set_loadingScreen_3(GameObject_t1113636619 * value)
	{
		___loadingScreen_3 = value;
		Il2CppCodeGenWriteBarrier((&___loadingScreen_3), value);
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___text_5)); }
	inline Text_t1901882714 * get_text_5() const { return ___text_5; }
	inline Text_t1901882714 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t1901882714 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}
};

struct SceneLoader_t4130533360_StaticFields
{
public:
	// SceneLoader SceneLoader::Instance
	SceneLoader_t4130533360 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___Instance_2)); }
	inline SceneLoader_t4130533360 * get_Instance_2() const { return ___Instance_2; }
	inline SceneLoader_t4130533360 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(SceneLoader_t4130533360 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELOADER_T4130533360_H
#ifndef ABSANIMATIONCOMPONENT_T262169234_H
#define ABSANIMATIONCOMPONENT_T262169234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_t262169234  : public MonoBehaviour_t3962482529
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_2;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_3;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_10;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_t2581268647 * ___onStart_11;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_t2581268647 * ___onPlay_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_t2581268647 * ___onUpdate_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_t2581268647 * ___onStepComplete_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_t2581268647 * ___onComplete_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_t2581268647 * ___onTweenCreated_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_t2581268647 * ___onRewind_17;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_t2342918553 * ___tween_18;

public:
	inline static int32_t get_offset_of_updateType_2() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___updateType_2)); }
	inline int32_t get_updateType_2() const { return ___updateType_2; }
	inline int32_t* get_address_of_updateType_2() { return &___updateType_2; }
	inline void set_updateType_2(int32_t value)
	{
		___updateType_2 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_3() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___isSpeedBased_3)); }
	inline bool get_isSpeedBased_3() const { return ___isSpeedBased_3; }
	inline bool* get_address_of_isSpeedBased_3() { return &___isSpeedBased_3; }
	inline void set_isSpeedBased_3(bool value)
	{
		___isSpeedBased_3 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStart_4)); }
	inline bool get_hasOnStart_4() const { return ___hasOnStart_4; }
	inline bool* get_address_of_hasOnStart_4() { return &___hasOnStart_4; }
	inline void set_hasOnStart_4(bool value)
	{
		___hasOnStart_4 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnPlay_5)); }
	inline bool get_hasOnPlay_5() const { return ___hasOnPlay_5; }
	inline bool* get_address_of_hasOnPlay_5() { return &___hasOnPlay_5; }
	inline void set_hasOnPlay_5(bool value)
	{
		___hasOnPlay_5 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnUpdate_6)); }
	inline bool get_hasOnUpdate_6() const { return ___hasOnUpdate_6; }
	inline bool* get_address_of_hasOnUpdate_6() { return &___hasOnUpdate_6; }
	inline void set_hasOnUpdate_6(bool value)
	{
		___hasOnUpdate_6 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStepComplete_7)); }
	inline bool get_hasOnStepComplete_7() const { return ___hasOnStepComplete_7; }
	inline bool* get_address_of_hasOnStepComplete_7() { return &___hasOnStepComplete_7; }
	inline void set_hasOnStepComplete_7(bool value)
	{
		___hasOnStepComplete_7 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnComplete_8)); }
	inline bool get_hasOnComplete_8() const { return ___hasOnComplete_8; }
	inline bool* get_address_of_hasOnComplete_8() { return &___hasOnComplete_8; }
	inline void set_hasOnComplete_8(bool value)
	{
		___hasOnComplete_8 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnTweenCreated_9)); }
	inline bool get_hasOnTweenCreated_9() const { return ___hasOnTweenCreated_9; }
	inline bool* get_address_of_hasOnTweenCreated_9() { return &___hasOnTweenCreated_9; }
	inline void set_hasOnTweenCreated_9(bool value)
	{
		___hasOnTweenCreated_9 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnRewind_10)); }
	inline bool get_hasOnRewind_10() const { return ___hasOnRewind_10; }
	inline bool* get_address_of_hasOnRewind_10() { return &___hasOnRewind_10; }
	inline void set_hasOnRewind_10(bool value)
	{
		___hasOnRewind_10 = value;
	}

	inline static int32_t get_offset_of_onStart_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStart_11)); }
	inline UnityEvent_t2581268647 * get_onStart_11() const { return ___onStart_11; }
	inline UnityEvent_t2581268647 ** get_address_of_onStart_11() { return &___onStart_11; }
	inline void set_onStart_11(UnityEvent_t2581268647 * value)
	{
		___onStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_11), value);
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onPlay_12)); }
	inline UnityEvent_t2581268647 * get_onPlay_12() const { return ___onPlay_12; }
	inline UnityEvent_t2581268647 ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(UnityEvent_t2581268647 * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onUpdate_13)); }
	inline UnityEvent_t2581268647 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline UnityEvent_t2581268647 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(UnityEvent_t2581268647 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStepComplete_14)); }
	inline UnityEvent_t2581268647 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline UnityEvent_t2581268647 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(UnityEvent_t2581268647 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onComplete_15)); }
	inline UnityEvent_t2581268647 * get_onComplete_15() const { return ___onComplete_15; }
	inline UnityEvent_t2581268647 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(UnityEvent_t2581268647 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onTweenCreated_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onTweenCreated_16)); }
	inline UnityEvent_t2581268647 * get_onTweenCreated_16() const { return ___onTweenCreated_16; }
	inline UnityEvent_t2581268647 ** get_address_of_onTweenCreated_16() { return &___onTweenCreated_16; }
	inline void set_onTweenCreated_16(UnityEvent_t2581268647 * value)
	{
		___onTweenCreated_16 = value;
		Il2CppCodeGenWriteBarrier((&___onTweenCreated_16), value);
	}

	inline static int32_t get_offset_of_onRewind_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onRewind_17)); }
	inline UnityEvent_t2581268647 * get_onRewind_17() const { return ___onRewind_17; }
	inline UnityEvent_t2581268647 ** get_address_of_onRewind_17() { return &___onRewind_17; }
	inline void set_onRewind_17(UnityEvent_t2581268647 * value)
	{
		___onRewind_17 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_17), value);
	}

	inline static int32_t get_offset_of_tween_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___tween_18)); }
	inline Tween_t2342918553 * get_tween_18() const { return ___tween_18; }
	inline Tween_t2342918553 ** get_address_of_tween_18() { return &___tween_18; }
	inline void set_tween_18(Tween_t2342918553 * value)
	{
		___tween_18 = value;
		Il2CppCodeGenWriteBarrier((&___tween_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSANIMATIONCOMPONENT_T262169234_H
#ifndef BASESCENE_T2918414559_H
#define BASESCENE_T2918414559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameVanilla.Core.BaseScene
struct  BaseScene_t2918414559  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas GameVanilla.Core.BaseScene::canvas
	Canvas_t3310196443 * ___canvas_2;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPopups
	Stack_1_t1957026074 * ___currentPopups_3;
	// System.Collections.Generic.Stack`1<UnityEngine.GameObject> GameVanilla.Core.BaseScene::currentPanels
	Stack_1_t1957026074 * ___currentPanels_4;

public:
	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___canvas_2)); }
	inline Canvas_t3310196443 * get_canvas_2() const { return ___canvas_2; }
	inline Canvas_t3310196443 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(Canvas_t3310196443 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}

	inline static int32_t get_offset_of_currentPopups_3() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPopups_3)); }
	inline Stack_1_t1957026074 * get_currentPopups_3() const { return ___currentPopups_3; }
	inline Stack_1_t1957026074 ** get_address_of_currentPopups_3() { return &___currentPopups_3; }
	inline void set_currentPopups_3(Stack_1_t1957026074 * value)
	{
		___currentPopups_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentPopups_3), value);
	}

	inline static int32_t get_offset_of_currentPanels_4() { return static_cast<int32_t>(offsetof(BaseScene_t2918414559, ___currentPanels_4)); }
	inline Stack_1_t1957026074 * get_currentPanels_4() const { return ___currentPanels_4; }
	inline Stack_1_t1957026074 ** get_address_of_currentPanels_4() { return &___currentPanels_4; }
	inline void set_currentPanels_4(Stack_1_t1957026074 * value)
	{
		___currentPanels_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentPanels_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCENE_T2918414559_H
#ifndef GHOSTSKILLINFO_T3192096645_H
#define GHOSTSKILLINFO_T3192096645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSkillInfo
struct  GhostSkillInfo_t3192096645  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<GhostSkillInfoItem> GhostSkillInfo::list
	List_1_t339037232 * ___list_2;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(GhostSkillInfo_t3192096645, ___list_2)); }
	inline List_1_t339037232 * get_list_2() const { return ___list_2; }
	inline List_1_t339037232 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(List_1_t339037232 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSKILLINFO_T3192096645_H
#ifndef PUZZLEITEM_T2123471318_H
#define PUZZLEITEM_T2123471318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleItem
struct  PuzzleItem_t2123471318  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PuzzleItem::Row
	int32_t ___Row_2;
	// System.Int32 PuzzleItem::Column
	int32_t ___Column_3;
	// UnityEngine.UI.Button PuzzleItem::button
	Button_t4055032469 * ___button_4;
	// UnityEngine.UI.Image PuzzleItem::image
	Image_t2670269651 * ___image_5;
	// PuzzleColor PuzzleItem::puzzleColor
	int32_t ___puzzleColor_6;

public:
	inline static int32_t get_offset_of_Row_2() { return static_cast<int32_t>(offsetof(PuzzleItem_t2123471318, ___Row_2)); }
	inline int32_t get_Row_2() const { return ___Row_2; }
	inline int32_t* get_address_of_Row_2() { return &___Row_2; }
	inline void set_Row_2(int32_t value)
	{
		___Row_2 = value;
	}

	inline static int32_t get_offset_of_Column_3() { return static_cast<int32_t>(offsetof(PuzzleItem_t2123471318, ___Column_3)); }
	inline int32_t get_Column_3() const { return ___Column_3; }
	inline int32_t* get_address_of_Column_3() { return &___Column_3; }
	inline void set_Column_3(int32_t value)
	{
		___Column_3 = value;
	}

	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(PuzzleItem_t2123471318, ___button_4)); }
	inline Button_t4055032469 * get_button_4() const { return ___button_4; }
	inline Button_t4055032469 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_t4055032469 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((&___button_4), value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(PuzzleItem_t2123471318, ___image_5)); }
	inline Image_t2670269651 * get_image_5() const { return ___image_5; }
	inline Image_t2670269651 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t2670269651 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}

	inline static int32_t get_offset_of_puzzleColor_6() { return static_cast<int32_t>(offsetof(PuzzleItem_t2123471318, ___puzzleColor_6)); }
	inline int32_t get_puzzleColor_6() const { return ___puzzleColor_6; }
	inline int32_t* get_address_of_puzzleColor_6() { return &___puzzleColor_6; }
	inline void set_puzzleColor_6(int32_t value)
	{
		___puzzleColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEITEM_T2123471318_H
#ifndef GHOSTSKILLINFOITEM_T3161929786_H
#define GHOSTSKILLINFOITEM_T3161929786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSkillInfoItem
struct  GhostSkillInfoItem_t3161929786  : public MonoBehaviour_t3962482529
{
public:
	// GameVanilla.Game.Common.BlockType GhostSkillInfoItem::blockType
	int32_t ___blockType_2;
	// UnityEngine.UI.Image GhostSkillInfoItem::maskBG
	Image_t2670269651 * ___maskBG_3;
	// Spine.Unity.SkeletonGraphic GhostSkillInfoItem::spineGraphic
	SkeletonGraphic_t1744877482 * ___spineGraphic_4;
	// UnityEngine.UI.Text GhostSkillInfoItem::levelText
	Text_t1901882714 * ___levelText_5;
	// UnityEngine.UI.Slider GhostSkillInfoItem::slider
	Slider_t3903728902 * ___slider_6;

public:
	inline static int32_t get_offset_of_blockType_2() { return static_cast<int32_t>(offsetof(GhostSkillInfoItem_t3161929786, ___blockType_2)); }
	inline int32_t get_blockType_2() const { return ___blockType_2; }
	inline int32_t* get_address_of_blockType_2() { return &___blockType_2; }
	inline void set_blockType_2(int32_t value)
	{
		___blockType_2 = value;
	}

	inline static int32_t get_offset_of_maskBG_3() { return static_cast<int32_t>(offsetof(GhostSkillInfoItem_t3161929786, ___maskBG_3)); }
	inline Image_t2670269651 * get_maskBG_3() const { return ___maskBG_3; }
	inline Image_t2670269651 ** get_address_of_maskBG_3() { return &___maskBG_3; }
	inline void set_maskBG_3(Image_t2670269651 * value)
	{
		___maskBG_3 = value;
		Il2CppCodeGenWriteBarrier((&___maskBG_3), value);
	}

	inline static int32_t get_offset_of_spineGraphic_4() { return static_cast<int32_t>(offsetof(GhostSkillInfoItem_t3161929786, ___spineGraphic_4)); }
	inline SkeletonGraphic_t1744877482 * get_spineGraphic_4() const { return ___spineGraphic_4; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_spineGraphic_4() { return &___spineGraphic_4; }
	inline void set_spineGraphic_4(SkeletonGraphic_t1744877482 * value)
	{
		___spineGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___spineGraphic_4), value);
	}

	inline static int32_t get_offset_of_levelText_5() { return static_cast<int32_t>(offsetof(GhostSkillInfoItem_t3161929786, ___levelText_5)); }
	inline Text_t1901882714 * get_levelText_5() const { return ___levelText_5; }
	inline Text_t1901882714 ** get_address_of_levelText_5() { return &___levelText_5; }
	inline void set_levelText_5(Text_t1901882714 * value)
	{
		___levelText_5 = value;
		Il2CppCodeGenWriteBarrier((&___levelText_5), value);
	}

	inline static int32_t get_offset_of_slider_6() { return static_cast<int32_t>(offsetof(GhostSkillInfoItem_t3161929786, ___slider_6)); }
	inline Slider_t3903728902 * get_slider_6() const { return ___slider_6; }
	inline Slider_t3903728902 ** get_address_of_slider_6() { return &___slider_6; }
	inline void set_slider_6(Slider_t3903728902 * value)
	{
		___slider_6 = value;
		Il2CppCodeGenWriteBarrier((&___slider_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSKILLINFOITEM_T3161929786_H
#ifndef WEBCAMERA_T13333461_H
#define WEBCAMERA_T13333461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamera
struct  WebCamera_t13333461  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean WebCamera::camAvailable
	bool ___camAvailable_2;
	// UnityEngine.WebCamTexture WebCamera::webCam
	WebCamTexture_t1514609158 * ___webCam_3;
	// UnityEngine.UI.RawImage WebCamera::background
	RawImage_t3182918964 * ___background_4;
	// UnityEngine.UI.AspectRatioFitter WebCamera::fitter
	AspectRatioFitter_t3312407083 * ___fitter_5;

public:
	inline static int32_t get_offset_of_camAvailable_2() { return static_cast<int32_t>(offsetof(WebCamera_t13333461, ___camAvailable_2)); }
	inline bool get_camAvailable_2() const { return ___camAvailable_2; }
	inline bool* get_address_of_camAvailable_2() { return &___camAvailable_2; }
	inline void set_camAvailable_2(bool value)
	{
		___camAvailable_2 = value;
	}

	inline static int32_t get_offset_of_webCam_3() { return static_cast<int32_t>(offsetof(WebCamera_t13333461, ___webCam_3)); }
	inline WebCamTexture_t1514609158 * get_webCam_3() const { return ___webCam_3; }
	inline WebCamTexture_t1514609158 ** get_address_of_webCam_3() { return &___webCam_3; }
	inline void set_webCam_3(WebCamTexture_t1514609158 * value)
	{
		___webCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___webCam_3), value);
	}

	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(WebCamera_t13333461, ___background_4)); }
	inline RawImage_t3182918964 * get_background_4() const { return ___background_4; }
	inline RawImage_t3182918964 ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(RawImage_t3182918964 * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier((&___background_4), value);
	}

	inline static int32_t get_offset_of_fitter_5() { return static_cast<int32_t>(offsetof(WebCamera_t13333461, ___fitter_5)); }
	inline AspectRatioFitter_t3312407083 * get_fitter_5() const { return ___fitter_5; }
	inline AspectRatioFitter_t3312407083 ** get_address_of_fitter_5() { return &___fitter_5; }
	inline void set_fitter_5(AspectRatioFitter_t3312407083 * value)
	{
		___fitter_5 = value;
		Il2CppCodeGenWriteBarrier((&___fitter_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMERA_T13333461_H
#ifndef RESULTUI_T719777132_H
#define RESULTUI_T719777132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultUI
struct  ResultUI_t719777132  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ResultUI::monsterNameText
	Text_t1901882714 * ___monsterNameText_2;
	// Spine.Unity.SkeletonGraphic ResultUI::SD_Graphic
	SkeletonGraphic_t1744877482 * ___SD_Graphic_3;
	// UnityEngine.GameObject ResultUI::candyRemainTime
	GameObject_t1113636619 * ___candyRemainTime_4;
	// UnityEngine.UI.Text ResultUI::candyRemainTimeText
	Text_t1901882714 * ___candyRemainTimeText_5;
	// UnityEngine.UI.Text ResultUI::candyCountText
	Text_t1901882714 * ___candyCountText_6;

public:
	inline static int32_t get_offset_of_monsterNameText_2() { return static_cast<int32_t>(offsetof(ResultUI_t719777132, ___monsterNameText_2)); }
	inline Text_t1901882714 * get_monsterNameText_2() const { return ___monsterNameText_2; }
	inline Text_t1901882714 ** get_address_of_monsterNameText_2() { return &___monsterNameText_2; }
	inline void set_monsterNameText_2(Text_t1901882714 * value)
	{
		___monsterNameText_2 = value;
		Il2CppCodeGenWriteBarrier((&___monsterNameText_2), value);
	}

	inline static int32_t get_offset_of_SD_Graphic_3() { return static_cast<int32_t>(offsetof(ResultUI_t719777132, ___SD_Graphic_3)); }
	inline SkeletonGraphic_t1744877482 * get_SD_Graphic_3() const { return ___SD_Graphic_3; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_SD_Graphic_3() { return &___SD_Graphic_3; }
	inline void set_SD_Graphic_3(SkeletonGraphic_t1744877482 * value)
	{
		___SD_Graphic_3 = value;
		Il2CppCodeGenWriteBarrier((&___SD_Graphic_3), value);
	}

	inline static int32_t get_offset_of_candyRemainTime_4() { return static_cast<int32_t>(offsetof(ResultUI_t719777132, ___candyRemainTime_4)); }
	inline GameObject_t1113636619 * get_candyRemainTime_4() const { return ___candyRemainTime_4; }
	inline GameObject_t1113636619 ** get_address_of_candyRemainTime_4() { return &___candyRemainTime_4; }
	inline void set_candyRemainTime_4(GameObject_t1113636619 * value)
	{
		___candyRemainTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___candyRemainTime_4), value);
	}

	inline static int32_t get_offset_of_candyRemainTimeText_5() { return static_cast<int32_t>(offsetof(ResultUI_t719777132, ___candyRemainTimeText_5)); }
	inline Text_t1901882714 * get_candyRemainTimeText_5() const { return ___candyRemainTimeText_5; }
	inline Text_t1901882714 ** get_address_of_candyRemainTimeText_5() { return &___candyRemainTimeText_5; }
	inline void set_candyRemainTimeText_5(Text_t1901882714 * value)
	{
		___candyRemainTimeText_5 = value;
		Il2CppCodeGenWriteBarrier((&___candyRemainTimeText_5), value);
	}

	inline static int32_t get_offset_of_candyCountText_6() { return static_cast<int32_t>(offsetof(ResultUI_t719777132, ___candyCountText_6)); }
	inline Text_t1901882714 * get_candyCountText_6() const { return ___candyCountText_6; }
	inline Text_t1901882714 ** get_address_of_candyCountText_6() { return &___candyCountText_6; }
	inline void set_candyCountText_6(Text_t1901882714 * value)
	{
		___candyCountText_6 = value;
		Il2CppCodeGenWriteBarrier((&___candyCountText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTUI_T719777132_H
#ifndef REMAINCHARMUI_T475548631_H
#define REMAINCHARMUI_T475548631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemainCharmUI
struct  RemainCharmUI_t475548631  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image RemainCharmUI::charmCountImg
	Image_t2670269651 * ___charmCountImg_2;
	// UnityEngine.UI.Text RemainCharmUI::charmCountText
	Text_t1901882714 * ___charmCountText_3;

public:
	inline static int32_t get_offset_of_charmCountImg_2() { return static_cast<int32_t>(offsetof(RemainCharmUI_t475548631, ___charmCountImg_2)); }
	inline Image_t2670269651 * get_charmCountImg_2() const { return ___charmCountImg_2; }
	inline Image_t2670269651 ** get_address_of_charmCountImg_2() { return &___charmCountImg_2; }
	inline void set_charmCountImg_2(Image_t2670269651 * value)
	{
		___charmCountImg_2 = value;
		Il2CppCodeGenWriteBarrier((&___charmCountImg_2), value);
	}

	inline static int32_t get_offset_of_charmCountText_3() { return static_cast<int32_t>(offsetof(RemainCharmUI_t475548631, ___charmCountText_3)); }
	inline Text_t1901882714 * get_charmCountText_3() const { return ___charmCountText_3; }
	inline Text_t1901882714 ** get_address_of_charmCountText_3() { return &___charmCountText_3; }
	inline void set_charmCountText_3(Text_t1901882714 * value)
	{
		___charmCountText_3 = value;
		Il2CppCodeGenWriteBarrier((&___charmCountText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMAINCHARMUI_T475548631_H
#ifndef MONSTERINFOUI_T3033209612_H
#define MONSTERINFOUI_T3033209612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonsterInfoUI
struct  MonsterInfoUI_t3033209612  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MonsterInfoUI::nameText
	Text_t1901882714 * ___nameText_2;
	// UnityEngine.UI.Text MonsterInfoUI::descriptionText
	Text_t1901882714 * ___descriptionText_3;
	// UnityEngine.UI.Slider MonsterInfoUI::monsterHPSlider
	Slider_t3903728902 * ___monsterHPSlider_4;

public:
	inline static int32_t get_offset_of_nameText_2() { return static_cast<int32_t>(offsetof(MonsterInfoUI_t3033209612, ___nameText_2)); }
	inline Text_t1901882714 * get_nameText_2() const { return ___nameText_2; }
	inline Text_t1901882714 ** get_address_of_nameText_2() { return &___nameText_2; }
	inline void set_nameText_2(Text_t1901882714 * value)
	{
		___nameText_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_2), value);
	}

	inline static int32_t get_offset_of_descriptionText_3() { return static_cast<int32_t>(offsetof(MonsterInfoUI_t3033209612, ___descriptionText_3)); }
	inline Text_t1901882714 * get_descriptionText_3() const { return ___descriptionText_3; }
	inline Text_t1901882714 ** get_address_of_descriptionText_3() { return &___descriptionText_3; }
	inline void set_descriptionText_3(Text_t1901882714 * value)
	{
		___descriptionText_3 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_3), value);
	}

	inline static int32_t get_offset_of_monsterHPSlider_4() { return static_cast<int32_t>(offsetof(MonsterInfoUI_t3033209612, ___monsterHPSlider_4)); }
	inline Slider_t3903728902 * get_monsterHPSlider_4() const { return ___monsterHPSlider_4; }
	inline Slider_t3903728902 ** get_address_of_monsterHPSlider_4() { return &___monsterHPSlider_4; }
	inline void set_monsterHPSlider_4(Slider_t3903728902 * value)
	{
		___monsterHPSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___monsterHPSlider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONSTERINFOUI_T3033209612_H
#ifndef ITEMCHARMUI_T437376365_H
#define ITEMCHARMUI_T437376365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemCharmUI
struct  ItemCharmUI_t437376365  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image ItemCharmUI::BG_image
	Image_t2670269651 * ___BG_image_2;
	// UnityEngine.UI.Image ItemCharmUI::image
	Image_t2670269651 * ___image_3;
	// UnityEngine.UI.Text ItemCharmUI::titleText
	Text_t1901882714 * ___titleText_4;
	// UnityEngine.UI.Text ItemCharmUI::countText
	Text_t1901882714 * ___countText_5;
	// UnityEngine.UI.Button ItemCharmUI::chargeItemCharmButton
	Button_t4055032469 * ___chargeItemCharmButton_6;
	// System.Boolean ItemCharmUI::isActive
	bool ___isActive_7;
	// System.Boolean ItemCharmUI::isTrigger
	bool ___isTrigger_8;

public:
	inline static int32_t get_offset_of_BG_image_2() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___BG_image_2)); }
	inline Image_t2670269651 * get_BG_image_2() const { return ___BG_image_2; }
	inline Image_t2670269651 ** get_address_of_BG_image_2() { return &___BG_image_2; }
	inline void set_BG_image_2(Image_t2670269651 * value)
	{
		___BG_image_2 = value;
		Il2CppCodeGenWriteBarrier((&___BG_image_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___image_3)); }
	inline Image_t2670269651 * get_image_3() const { return ___image_3; }
	inline Image_t2670269651 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t2670269651 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}

	inline static int32_t get_offset_of_titleText_4() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___titleText_4)); }
	inline Text_t1901882714 * get_titleText_4() const { return ___titleText_4; }
	inline Text_t1901882714 ** get_address_of_titleText_4() { return &___titleText_4; }
	inline void set_titleText_4(Text_t1901882714 * value)
	{
		___titleText_4 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_4), value);
	}

	inline static int32_t get_offset_of_countText_5() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___countText_5)); }
	inline Text_t1901882714 * get_countText_5() const { return ___countText_5; }
	inline Text_t1901882714 ** get_address_of_countText_5() { return &___countText_5; }
	inline void set_countText_5(Text_t1901882714 * value)
	{
		___countText_5 = value;
		Il2CppCodeGenWriteBarrier((&___countText_5), value);
	}

	inline static int32_t get_offset_of_chargeItemCharmButton_6() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___chargeItemCharmButton_6)); }
	inline Button_t4055032469 * get_chargeItemCharmButton_6() const { return ___chargeItemCharmButton_6; }
	inline Button_t4055032469 ** get_address_of_chargeItemCharmButton_6() { return &___chargeItemCharmButton_6; }
	inline void set_chargeItemCharmButton_6(Button_t4055032469 * value)
	{
		___chargeItemCharmButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___chargeItemCharmButton_6), value);
	}

	inline static int32_t get_offset_of_isActive_7() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___isActive_7)); }
	inline bool get_isActive_7() const { return ___isActive_7; }
	inline bool* get_address_of_isActive_7() { return &___isActive_7; }
	inline void set_isActive_7(bool value)
	{
		___isActive_7 = value;
	}

	inline static int32_t get_offset_of_isTrigger_8() { return static_cast<int32_t>(offsetof(ItemCharmUI_t437376365, ___isTrigger_8)); }
	inline bool get_isTrigger_8() const { return ___isTrigger_8; }
	inline bool* get_address_of_isTrigger_8() { return &___isTrigger_8; }
	inline void set_isTrigger_8(bool value)
	{
		___isTrigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMCHARMUI_T437376365_H
#ifndef GHOSTSCHOOLITEM_T364957914_H
#define GHOSTSCHOOLITEM_T364957914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSchoolItem
struct  GhostSchoolItem_t364957914  : public MonoBehaviour_t3962482529
{
public:
	// Spine.Unity.SkeletonDataAsset GhostSchoolItem::spineDataAsset
	SkeletonDataAsset_t3748144825 * ___spineDataAsset_2;
	// Ghost GhostSchoolItem::ghost
	Ghost_t201992404 * ___ghost_3;
	// UnityEngine.GameObject GhostSchoolItem::spineGraphicParent
	GameObject_t1113636619 * ___spineGraphicParent_4;
	// Spine.Unity.SkeletonGraphic GhostSchoolItem::spineGraphic
	SkeletonGraphic_t1744877482 * ___spineGraphic_5;
	// UnityEngine.UI.Text GhostSchoolItem::levelText
	Text_t1901882714 * ___levelText_6;
	// UnityEngine.UI.Text GhostSchoolItem::nameText
	Text_t1901882714 * ___nameText_7;

public:
	inline static int32_t get_offset_of_spineDataAsset_2() { return static_cast<int32_t>(offsetof(GhostSchoolItem_t364957914, ___spineDataAsset_2)); }
	inline SkeletonDataAsset_t3748144825 * get_spineDataAsset_2() const { return ___spineDataAsset_2; }
	inline SkeletonDataAsset_t3748144825 ** get_address_of_spineDataAsset_2() { return &___spineDataAsset_2; }
	inline void set_spineDataAsset_2(SkeletonDataAsset_t3748144825 * value)
	{
		___spineDataAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spineDataAsset_2), value);
	}

	inline static int32_t get_offset_of_ghost_3() { return static_cast<int32_t>(offsetof(GhostSchoolItem_t364957914, ___ghost_3)); }
	inline Ghost_t201992404 * get_ghost_3() const { return ___ghost_3; }
	inline Ghost_t201992404 ** get_address_of_ghost_3() { return &___ghost_3; }
	inline void set_ghost_3(Ghost_t201992404 * value)
	{
		___ghost_3 = value;
		Il2CppCodeGenWriteBarrier((&___ghost_3), value);
	}

	inline static int32_t get_offset_of_spineGraphicParent_4() { return static_cast<int32_t>(offsetof(GhostSchoolItem_t364957914, ___spineGraphicParent_4)); }
	inline GameObject_t1113636619 * get_spineGraphicParent_4() const { return ___spineGraphicParent_4; }
	inline GameObject_t1113636619 ** get_address_of_spineGraphicParent_4() { return &___spineGraphicParent_4; }
	inline void set_spineGraphicParent_4(GameObject_t1113636619 * value)
	{
		___spineGraphicParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___spineGraphicParent_4), value);
	}

	inline static int32_t get_offset_of_spineGraphic_5() { return static_cast<int32_t>(offsetof(GhostSchoolItem_t364957914, ___spineGraphic_5)); }
	inline SkeletonGraphic_t1744877482 * get_spineGraphic_5() const { return ___spineGraphic_5; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_spineGraphic_5() { return &___spineGraphic_5; }
	inline void set_spineGraphic_5(SkeletonGraphic_t1744877482 * value)
	{
		___spineGraphic_5 = value;
		Il2CppCodeGenWriteBarrier((&___spineGraphic_5), value);
	}

	inline static int32_t get_offset_of_levelText_6() { return static_cast<int32_t>(offsetof(GhostSchoolItem_t364957914, ___levelText_6)); }
	inline Text_t1901882714 * get_levelText_6() const { return ___levelText_6; }
	inline Text_t1901882714 ** get_address_of_levelText_6() { return &___levelText_6; }
	inline void set_levelText_6(Text_t1901882714 * value)
	{
		___levelText_6 = value;
		Il2CppCodeGenWriteBarrier((&___levelText_6), value);
	}

	inline static int32_t get_offset_of_nameText_7() { return static_cast<int32_t>(offsetof(GhostSchoolItem_t364957914, ___nameText_7)); }
	inline Text_t1901882714 * get_nameText_7() const { return ___nameText_7; }
	inline Text_t1901882714 ** get_address_of_nameText_7() { return &___nameText_7; }
	inline void set_nameText_7(Text_t1901882714 * value)
	{
		___nameText_7 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSCHOOLITEM_T364957914_H
#ifndef GHOSTINFOUI_T4215450973_H
#define GHOSTINFOUI_T4215450973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostInfoUI
struct  GhostInfoUI_t4215450973  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text GhostInfoUI::nameText
	Text_t1901882714 * ___nameText_2;
	// UnityEngine.UI.Text GhostInfoUI::descriptionText
	Text_t1901882714 * ___descriptionText_3;
	// UnityEngine.UI.Slider GhostInfoUI::ghostHPSlider
	Slider_t3903728902 * ___ghostHPSlider_4;

public:
	inline static int32_t get_offset_of_nameText_2() { return static_cast<int32_t>(offsetof(GhostInfoUI_t4215450973, ___nameText_2)); }
	inline Text_t1901882714 * get_nameText_2() const { return ___nameText_2; }
	inline Text_t1901882714 ** get_address_of_nameText_2() { return &___nameText_2; }
	inline void set_nameText_2(Text_t1901882714 * value)
	{
		___nameText_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_2), value);
	}

	inline static int32_t get_offset_of_descriptionText_3() { return static_cast<int32_t>(offsetof(GhostInfoUI_t4215450973, ___descriptionText_3)); }
	inline Text_t1901882714 * get_descriptionText_3() const { return ___descriptionText_3; }
	inline Text_t1901882714 ** get_address_of_descriptionText_3() { return &___descriptionText_3; }
	inline void set_descriptionText_3(Text_t1901882714 * value)
	{
		___descriptionText_3 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_3), value);
	}

	inline static int32_t get_offset_of_ghostHPSlider_4() { return static_cast<int32_t>(offsetof(GhostInfoUI_t4215450973, ___ghostHPSlider_4)); }
	inline Slider_t3903728902 * get_ghostHPSlider_4() const { return ___ghostHPSlider_4; }
	inline Slider_t3903728902 ** get_address_of_ghostHPSlider_4() { return &___ghostHPSlider_4; }
	inline void set_ghostHPSlider_4(Slider_t3903728902 * value)
	{
		___ghostHPSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___ghostHPSlider_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTINFOUI_T4215450973_H
#ifndef PUZZLEMANAGER_T474948643_H
#define PUZZLEMANAGER_T474948643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleManager
struct  PuzzleManager_t474948643  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<PuzzleItem> PuzzleManager::puzzleList
	List_1_t3595546060 * ___puzzleList_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> PuzzleManager::spawnPosList
	List_1_t777473367 * ___spawnPosList_4;
	// System.Collections.Generic.List`1<PuzzleItem> PuzzleManager::Complete_List
	List_1_t3595546060 * ___Complete_List_5;
	// System.Collections.Generic.List`1<PuzzleItem> PuzzleManager::Check_List
	List_1_t3595546060 * ___Check_List_6;
	// PuzzleItem[0...,0...] PuzzleManager::arrays
	PuzzleItemU5B0___U2C0___U5D_t216270356* ___arrays_7;
	// UnityEngine.GameObject PuzzleManager::puzzleItemPrefab
	GameObject_t1113636619 * ___puzzleItemPrefab_8;

public:
	inline static int32_t get_offset_of_puzzleList_3() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643, ___puzzleList_3)); }
	inline List_1_t3595546060 * get_puzzleList_3() const { return ___puzzleList_3; }
	inline List_1_t3595546060 ** get_address_of_puzzleList_3() { return &___puzzleList_3; }
	inline void set_puzzleList_3(List_1_t3595546060 * value)
	{
		___puzzleList_3 = value;
		Il2CppCodeGenWriteBarrier((&___puzzleList_3), value);
	}

	inline static int32_t get_offset_of_spawnPosList_4() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643, ___spawnPosList_4)); }
	inline List_1_t777473367 * get_spawnPosList_4() const { return ___spawnPosList_4; }
	inline List_1_t777473367 ** get_address_of_spawnPosList_4() { return &___spawnPosList_4; }
	inline void set_spawnPosList_4(List_1_t777473367 * value)
	{
		___spawnPosList_4 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPosList_4), value);
	}

	inline static int32_t get_offset_of_Complete_List_5() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643, ___Complete_List_5)); }
	inline List_1_t3595546060 * get_Complete_List_5() const { return ___Complete_List_5; }
	inline List_1_t3595546060 ** get_address_of_Complete_List_5() { return &___Complete_List_5; }
	inline void set_Complete_List_5(List_1_t3595546060 * value)
	{
		___Complete_List_5 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_List_5), value);
	}

	inline static int32_t get_offset_of_Check_List_6() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643, ___Check_List_6)); }
	inline List_1_t3595546060 * get_Check_List_6() const { return ___Check_List_6; }
	inline List_1_t3595546060 ** get_address_of_Check_List_6() { return &___Check_List_6; }
	inline void set_Check_List_6(List_1_t3595546060 * value)
	{
		___Check_List_6 = value;
		Il2CppCodeGenWriteBarrier((&___Check_List_6), value);
	}

	inline static int32_t get_offset_of_arrays_7() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643, ___arrays_7)); }
	inline PuzzleItemU5B0___U2C0___U5D_t216270356* get_arrays_7() const { return ___arrays_7; }
	inline PuzzleItemU5B0___U2C0___U5D_t216270356** get_address_of_arrays_7() { return &___arrays_7; }
	inline void set_arrays_7(PuzzleItemU5B0___U2C0___U5D_t216270356* value)
	{
		___arrays_7 = value;
		Il2CppCodeGenWriteBarrier((&___arrays_7), value);
	}

	inline static int32_t get_offset_of_puzzleItemPrefab_8() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643, ___puzzleItemPrefab_8)); }
	inline GameObject_t1113636619 * get_puzzleItemPrefab_8() const { return ___puzzleItemPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_puzzleItemPrefab_8() { return &___puzzleItemPrefab_8; }
	inline void set_puzzleItemPrefab_8(GameObject_t1113636619 * value)
	{
		___puzzleItemPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___puzzleItemPrefab_8), value);
	}
};

struct PuzzleManager_t474948643_StaticFields
{
public:
	// PuzzleManager PuzzleManager::Instance
	PuzzleManager_t474948643 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(PuzzleManager_t474948643_StaticFields, ___Instance_2)); }
	inline PuzzleManager_t474948643 * get_Instance_2() const { return ___Instance_2; }
	inline PuzzleManager_t474948643 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(PuzzleManager_t474948643 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLEMANAGER_T474948643_H
#ifndef PLAYGAMESHELPEROBJECT_T4023745847_H
#define PLAYGAMESHELPEROBJECT_T4023745847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct  PlayGamesHelperObject_t4023745847  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::localQueue
	List_1_t2736452219 * ___localQueue_5;

public:
	inline static int32_t get_offset_of_localQueue_5() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847, ___localQueue_5)); }
	inline List_1_t2736452219 * get_localQueue_5() const { return ___localQueue_5; }
	inline List_1_t2736452219 ** get_address_of_localQueue_5() { return &___localQueue_5; }
	inline void set_localQueue_5(List_1_t2736452219 * value)
	{
		___localQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___localQueue_5), value);
	}
};

struct PlayGamesHelperObject_t4023745847_StaticFields
{
public:
	// GooglePlayGames.OurUtils.PlayGamesHelperObject GooglePlayGames.OurUtils.PlayGamesHelperObject::instance
	PlayGamesHelperObject_t4023745847 * ___instance_2;
	// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::sIsDummy
	bool ___sIsDummy_3;
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueue
	List_1_t2736452219 * ___sQueue_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueueEmpty
	bool ___sQueueEmpty_6;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> GooglePlayGames.OurUtils.PlayGamesHelperObject::sPauseCallbackList
	List_1_t1741830302 * ___sPauseCallbackList_7;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> GooglePlayGames.OurUtils.PlayGamesHelperObject::sFocusCallbackList
	List_1_t1741830302 * ___sFocusCallbackList_8;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___instance_2)); }
	inline PlayGamesHelperObject_t4023745847 * get_instance_2() const { return ___instance_2; }
	inline PlayGamesHelperObject_t4023745847 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PlayGamesHelperObject_t4023745847 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_sIsDummy_3() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sIsDummy_3)); }
	inline bool get_sIsDummy_3() const { return ___sIsDummy_3; }
	inline bool* get_address_of_sIsDummy_3() { return &___sIsDummy_3; }
	inline void set_sIsDummy_3(bool value)
	{
		___sIsDummy_3 = value;
	}

	inline static int32_t get_offset_of_sQueue_4() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sQueue_4)); }
	inline List_1_t2736452219 * get_sQueue_4() const { return ___sQueue_4; }
	inline List_1_t2736452219 ** get_address_of_sQueue_4() { return &___sQueue_4; }
	inline void set_sQueue_4(List_1_t2736452219 * value)
	{
		___sQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___sQueue_4), value);
	}

	inline static int32_t get_offset_of_sQueueEmpty_6() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sQueueEmpty_6)); }
	inline bool get_sQueueEmpty_6() const { return ___sQueueEmpty_6; }
	inline bool* get_address_of_sQueueEmpty_6() { return &___sQueueEmpty_6; }
	inline void set_sQueueEmpty_6(bool value)
	{
		___sQueueEmpty_6 = value;
	}

	inline static int32_t get_offset_of_sPauseCallbackList_7() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sPauseCallbackList_7)); }
	inline List_1_t1741830302 * get_sPauseCallbackList_7() const { return ___sPauseCallbackList_7; }
	inline List_1_t1741830302 ** get_address_of_sPauseCallbackList_7() { return &___sPauseCallbackList_7; }
	inline void set_sPauseCallbackList_7(List_1_t1741830302 * value)
	{
		___sPauseCallbackList_7 = value;
		Il2CppCodeGenWriteBarrier((&___sPauseCallbackList_7), value);
	}

	inline static int32_t get_offset_of_sFocusCallbackList_8() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t4023745847_StaticFields, ___sFocusCallbackList_8)); }
	inline List_1_t1741830302 * get_sFocusCallbackList_8() const { return ___sFocusCallbackList_8; }
	inline List_1_t1741830302 ** get_address_of_sFocusCallbackList_8() { return &___sFocusCallbackList_8; }
	inline void set_sFocusCallbackList_8(List_1_t1741830302 * value)
	{
		___sFocusCallbackList_8 = value;
		Il2CppCodeGenWriteBarrier((&___sFocusCallbackList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYGAMESHELPEROBJECT_T4023745847_H
#ifndef LOADINGPOPUP_T3786168181_H
#define LOADINGPOPUP_T3786168181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingPopup
struct  LoadingPopup_t3786168181  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider LoadingPopup::slider
	Slider_t3903728902 * ___slider_2;
	// UnityEngine.UI.Text LoadingPopup::text
	Text_t1901882714 * ___text_3;

public:
	inline static int32_t get_offset_of_slider_2() { return static_cast<int32_t>(offsetof(LoadingPopup_t3786168181, ___slider_2)); }
	inline Slider_t3903728902 * get_slider_2() const { return ___slider_2; }
	inline Slider_t3903728902 ** get_address_of_slider_2() { return &___slider_2; }
	inline void set_slider_2(Slider_t3903728902 * value)
	{
		___slider_2 = value;
		Il2CppCodeGenWriteBarrier((&___slider_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(LoadingPopup_t3786168181, ___text_3)); }
	inline Text_t1901882714 * get_text_3() const { return ___text_3; }
	inline Text_t1901882714 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_t1901882714 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGPOPUP_T3786168181_H
#ifndef ITEMCHARMSHOPITEM_T2921973635_H
#define ITEMCHARMSHOPITEM_T2921973635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemCharmShopItem
struct  ItemCharmShopItem_t2921973635  : public MonoBehaviour_t3962482529
{
public:
	// Charm ItemCharmShopItem::charm
	Charm_t3703444127 * ___charm_2;
	// System.Int32 ItemCharmShopItem::count
	int32_t ___count_3;
	// UnityEngine.UI.Image ItemCharmShopItem::image
	Image_t2670269651 * ___image_4;
	// UnityEngine.UI.Text ItemCharmShopItem::titleText
	Text_t1901882714 * ___titleText_5;
	// UnityEngine.UI.Text ItemCharmShopItem::priceText
	Text_t1901882714 * ___priceText_6;
	// UnityEngine.UI.Button ItemCharmShopItem::GetButton
	Button_t4055032469 * ___GetButton_7;

public:
	inline static int32_t get_offset_of_charm_2() { return static_cast<int32_t>(offsetof(ItemCharmShopItem_t2921973635, ___charm_2)); }
	inline Charm_t3703444127 * get_charm_2() const { return ___charm_2; }
	inline Charm_t3703444127 ** get_address_of_charm_2() { return &___charm_2; }
	inline void set_charm_2(Charm_t3703444127 * value)
	{
		___charm_2 = value;
		Il2CppCodeGenWriteBarrier((&___charm_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(ItemCharmShopItem_t2921973635, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(ItemCharmShopItem_t2921973635, ___image_4)); }
	inline Image_t2670269651 * get_image_4() const { return ___image_4; }
	inline Image_t2670269651 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Image_t2670269651 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_titleText_5() { return static_cast<int32_t>(offsetof(ItemCharmShopItem_t2921973635, ___titleText_5)); }
	inline Text_t1901882714 * get_titleText_5() const { return ___titleText_5; }
	inline Text_t1901882714 ** get_address_of_titleText_5() { return &___titleText_5; }
	inline void set_titleText_5(Text_t1901882714 * value)
	{
		___titleText_5 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_5), value);
	}

	inline static int32_t get_offset_of_priceText_6() { return static_cast<int32_t>(offsetof(ItemCharmShopItem_t2921973635, ___priceText_6)); }
	inline Text_t1901882714 * get_priceText_6() const { return ___priceText_6; }
	inline Text_t1901882714 ** get_address_of_priceText_6() { return &___priceText_6; }
	inline void set_priceText_6(Text_t1901882714 * value)
	{
		___priceText_6 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_6), value);
	}

	inline static int32_t get_offset_of_GetButton_7() { return static_cast<int32_t>(offsetof(ItemCharmShopItem_t2921973635, ___GetButton_7)); }
	inline Button_t4055032469 * get_GetButton_7() const { return ___GetButton_7; }
	inline Button_t4055032469 ** get_address_of_GetButton_7() { return &___GetButton_7; }
	inline void set_GetButton_7(Button_t4055032469 * value)
	{
		___GetButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___GetButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMCHARMSHOPITEM_T2921973635_H
#ifndef CHARMSHOPITEM_T1928446277_H
#define CHARMSHOPITEM_T1928446277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmShopItem
struct  CharmShopItem_t1928446277  : public MonoBehaviour_t3962482529
{
public:
	// Charm CharmShopItem::charm
	Charm_t3703444127 * ___charm_2;
	// System.Int32 CharmShopItem::count
	int32_t ___count_3;
	// UnityEngine.UI.Image CharmShopItem::image
	Image_t2670269651 * ___image_4;
	// UnityEngine.UI.Text CharmShopItem::titleText
	Text_t1901882714 * ___titleText_5;
	// UnityEngine.UI.Text CharmShopItem::priceText
	Text_t1901882714 * ___priceText_6;
	// UnityEngine.UI.Button CharmShopItem::GetButton
	Button_t4055032469 * ___GetButton_7;

public:
	inline static int32_t get_offset_of_charm_2() { return static_cast<int32_t>(offsetof(CharmShopItem_t1928446277, ___charm_2)); }
	inline Charm_t3703444127 * get_charm_2() const { return ___charm_2; }
	inline Charm_t3703444127 ** get_address_of_charm_2() { return &___charm_2; }
	inline void set_charm_2(Charm_t3703444127 * value)
	{
		___charm_2 = value;
		Il2CppCodeGenWriteBarrier((&___charm_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(CharmShopItem_t1928446277, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(CharmShopItem_t1928446277, ___image_4)); }
	inline Image_t2670269651 * get_image_4() const { return ___image_4; }
	inline Image_t2670269651 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Image_t2670269651 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_titleText_5() { return static_cast<int32_t>(offsetof(CharmShopItem_t1928446277, ___titleText_5)); }
	inline Text_t1901882714 * get_titleText_5() const { return ___titleText_5; }
	inline Text_t1901882714 ** get_address_of_titleText_5() { return &___titleText_5; }
	inline void set_titleText_5(Text_t1901882714 * value)
	{
		___titleText_5 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_5), value);
	}

	inline static int32_t get_offset_of_priceText_6() { return static_cast<int32_t>(offsetof(CharmShopItem_t1928446277, ___priceText_6)); }
	inline Text_t1901882714 * get_priceText_6() const { return ___priceText_6; }
	inline Text_t1901882714 ** get_address_of_priceText_6() { return &___priceText_6; }
	inline void set_priceText_6(Text_t1901882714 * value)
	{
		___priceText_6 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_6), value);
	}

	inline static int32_t get_offset_of_GetButton_7() { return static_cast<int32_t>(offsetof(CharmShopItem_t1928446277, ___GetButton_7)); }
	inline Button_t4055032469 * get_GetButton_7() const { return ___GetButton_7; }
	inline Button_t4055032469 ** get_address_of_GetButton_7() { return &___GetButton_7; }
	inline void set_GetButton_7(Button_t4055032469 * value)
	{
		___GetButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___GetButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMSHOPITEM_T1928446277_H
#ifndef GHOSTEFFECT_T2138921326_H
#define GHOSTEFFECT_T2138921326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostEffect
struct  GhostEffect_t2138921326  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer GhostEffect::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_2;
	// UnityEngine.Color GhostEffect::color
	Color_t2555686324  ___color_3;
	// System.Single GhostEffect::timer
	float ___timer_4;
	// System.Single GhostEffect::alpha
	float ___alpha_5;
	// System.Boolean GhostEffect::isEnable
	bool ___isEnable_6;

public:
	inline static int32_t get_offset_of_spriteRenderer_2() { return static_cast<int32_t>(offsetof(GhostEffect_t2138921326, ___spriteRenderer_2)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_2() const { return ___spriteRenderer_2; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_2() { return &___spriteRenderer_2; }
	inline void set_spriteRenderer_2(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteRenderer_2), value);
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(GhostEffect_t2138921326, ___color_3)); }
	inline Color_t2555686324  get_color_3() const { return ___color_3; }
	inline Color_t2555686324 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color_t2555686324  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(GhostEffect_t2138921326, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}

	inline static int32_t get_offset_of_alpha_5() { return static_cast<int32_t>(offsetof(GhostEffect_t2138921326, ___alpha_5)); }
	inline float get_alpha_5() const { return ___alpha_5; }
	inline float* get_address_of_alpha_5() { return &___alpha_5; }
	inline void set_alpha_5(float value)
	{
		___alpha_5 = value;
	}

	inline static int32_t get_offset_of_isEnable_6() { return static_cast<int32_t>(offsetof(GhostEffect_t2138921326, ___isEnable_6)); }
	inline bool get_isEnable_6() const { return ___isEnable_6; }
	inline bool* get_address_of_isEnable_6() { return &___isEnable_6; }
	inline void set_isEnable_6(bool value)
	{
		___isEnable_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTEFFECT_T2138921326_H
#ifndef FREEITEMCHARMUI_T3416008829_H
#define FREEITEMCHARMUI_T3416008829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreeItemCharmUI
struct  FreeItemCharmUI_t3416008829  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean FreeItemCharmUI::isTrigger
	bool ___isTrigger_2;

public:
	inline static int32_t get_offset_of_isTrigger_2() { return static_cast<int32_t>(offsetof(FreeItemCharmUI_t3416008829, ___isTrigger_2)); }
	inline bool get_isTrigger_2() const { return ___isTrigger_2; }
	inline bool* get_address_of_isTrigger_2() { return &___isTrigger_2; }
	inline void set_isTrigger_2(bool value)
	{
		___isTrigger_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREEITEMCHARMUI_T3416008829_H
#ifndef GHOSTSCHOOLINFO_T3497322382_H
#define GHOSTSCHOOLINFO_T3497322382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSchoolInfo
struct  GhostSchoolInfo_t3497322382  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GhostSchoolInfo::nameLevelText
	Text_t1901882714 * ___nameLevelText_6;
	// UnityEngine.GameObject GhostSchoolInfo::ghostBG
	GameObject_t1113636619 * ___ghostBG_7;
	// UnityEngine.GameObject GhostSchoolInfo::spineGraphicGO
	GameObject_t1113636619 * ___spineGraphicGO_8;
	// Spine.Unity.SkeletonGraphic GhostSchoolInfo::spineGraphic
	SkeletonGraphic_t1744877482 * ___spineGraphic_9;

public:
	inline static int32_t get_offset_of_nameLevelText_6() { return static_cast<int32_t>(offsetof(GhostSchoolInfo_t3497322382, ___nameLevelText_6)); }
	inline Text_t1901882714 * get_nameLevelText_6() const { return ___nameLevelText_6; }
	inline Text_t1901882714 ** get_address_of_nameLevelText_6() { return &___nameLevelText_6; }
	inline void set_nameLevelText_6(Text_t1901882714 * value)
	{
		___nameLevelText_6 = value;
		Il2CppCodeGenWriteBarrier((&___nameLevelText_6), value);
	}

	inline static int32_t get_offset_of_ghostBG_7() { return static_cast<int32_t>(offsetof(GhostSchoolInfo_t3497322382, ___ghostBG_7)); }
	inline GameObject_t1113636619 * get_ghostBG_7() const { return ___ghostBG_7; }
	inline GameObject_t1113636619 ** get_address_of_ghostBG_7() { return &___ghostBG_7; }
	inline void set_ghostBG_7(GameObject_t1113636619 * value)
	{
		___ghostBG_7 = value;
		Il2CppCodeGenWriteBarrier((&___ghostBG_7), value);
	}

	inline static int32_t get_offset_of_spineGraphicGO_8() { return static_cast<int32_t>(offsetof(GhostSchoolInfo_t3497322382, ___spineGraphicGO_8)); }
	inline GameObject_t1113636619 * get_spineGraphicGO_8() const { return ___spineGraphicGO_8; }
	inline GameObject_t1113636619 ** get_address_of_spineGraphicGO_8() { return &___spineGraphicGO_8; }
	inline void set_spineGraphicGO_8(GameObject_t1113636619 * value)
	{
		___spineGraphicGO_8 = value;
		Il2CppCodeGenWriteBarrier((&___spineGraphicGO_8), value);
	}

	inline static int32_t get_offset_of_spineGraphic_9() { return static_cast<int32_t>(offsetof(GhostSchoolInfo_t3497322382, ___spineGraphic_9)); }
	inline SkeletonGraphic_t1744877482 * get_spineGraphic_9() const { return ___spineGraphic_9; }
	inline SkeletonGraphic_t1744877482 ** get_address_of_spineGraphic_9() { return &___spineGraphic_9; }
	inline void set_spineGraphic_9(SkeletonGraphic_t1744877482 * value)
	{
		___spineGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___spineGraphic_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSCHOOLINFO_T3497322382_H
#ifndef CHARACTERPOPUP_T2617494106_H
#define CHARACTERPOPUP_T2617494106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterPopup
struct  CharacterPopup_t2617494106  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERPOPUP_T2617494106_H
#ifndef GHOSTSCHOOLITEMINFOPOPUP_T232986025_H
#define GHOSTSCHOOLITEMINFOPOPUP_T232986025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSchoolItemInfoPopup
struct  GhostSchoolItemInfoPopup_t232986025  : public Popup_t1063720813
{
public:
	// UnityEngine.UI.Text GhostSchoolItemInfoPopup::levelText
	Text_t1901882714 * ___levelText_6;
	// UnityEngine.UI.Image GhostSchoolItemInfoPopup::itemImage
	Image_t2670269651 * ___itemImage_7;
	// UnityEngine.UI.Text GhostSchoolItemInfoPopup::nameText
	Text_t1901882714 * ___nameText_8;
	// UnityEngine.UI.Text GhostSchoolItemInfoPopup::effectText
	Text_t1901882714 * ___effectText_9;

public:
	inline static int32_t get_offset_of_levelText_6() { return static_cast<int32_t>(offsetof(GhostSchoolItemInfoPopup_t232986025, ___levelText_6)); }
	inline Text_t1901882714 * get_levelText_6() const { return ___levelText_6; }
	inline Text_t1901882714 ** get_address_of_levelText_6() { return &___levelText_6; }
	inline void set_levelText_6(Text_t1901882714 * value)
	{
		___levelText_6 = value;
		Il2CppCodeGenWriteBarrier((&___levelText_6), value);
	}

	inline static int32_t get_offset_of_itemImage_7() { return static_cast<int32_t>(offsetof(GhostSchoolItemInfoPopup_t232986025, ___itemImage_7)); }
	inline Image_t2670269651 * get_itemImage_7() const { return ___itemImage_7; }
	inline Image_t2670269651 ** get_address_of_itemImage_7() { return &___itemImage_7; }
	inline void set_itemImage_7(Image_t2670269651 * value)
	{
		___itemImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___itemImage_7), value);
	}

	inline static int32_t get_offset_of_nameText_8() { return static_cast<int32_t>(offsetof(GhostSchoolItemInfoPopup_t232986025, ___nameText_8)); }
	inline Text_t1901882714 * get_nameText_8() const { return ___nameText_8; }
	inline Text_t1901882714 ** get_address_of_nameText_8() { return &___nameText_8; }
	inline void set_nameText_8(Text_t1901882714 * value)
	{
		___nameText_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameText_8), value);
	}

	inline static int32_t get_offset_of_effectText_9() { return static_cast<int32_t>(offsetof(GhostSchoolItemInfoPopup_t232986025, ___effectText_9)); }
	inline Text_t1901882714 * get_effectText_9() const { return ___effectText_9; }
	inline Text_t1901882714 ** get_address_of_effectText_9() { return &___effectText_9; }
	inline void set_effectText_9(Text_t1901882714 * value)
	{
		___effectText_9 = value;
		Il2CppCodeGenWriteBarrier((&___effectText_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSCHOOLITEMINFOPOPUP_T232986025_H
#ifndef GHOSTSCHOOLPOPUP_T1455275853_H
#define GHOSTSCHOOLPOPUP_T1455275853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSchoolPopup
struct  GhostSchoolPopup_t1455275853  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSCHOOLPOPUP_T1455275853_H
#ifndef GHOSTSELECTPOPUP_T3778673497_H
#define GHOSTSELECTPOPUP_T3778673497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSelectPopup
struct  GhostSelectPopup_t3778673497  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSELECTPOPUP_T3778673497_H
#ifndef GHOSTSELECTLISTPOPUP_T692877192_H
#define GHOSTSELECTLISTPOPUP_T692877192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostSelectListPopup
struct  GhostSelectListPopup_t692877192  : public Popup_t1063720813
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTSELECTLISTPOPUP_T692877192_H
#ifndef CANDYTIMER_T2979372557_H
#define CANDYTIMER_T2979372557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CandyTimer
struct  CandyTimer_t2979372557  : public TimeSystem_t3122972587
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANDYTIMER_T2979372557_H
#ifndef CHARMTIMER_T90502612_H
#define CHARMTIMER_T90502612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharmTimer
struct  CharmTimer_t90502612  : public TimeSystem_t3122972587
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARMTIMER_T90502612_H
#ifndef PUZZLELIVETIMER_T464531872_H
#define PUZZLELIVETIMER_T464531872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleLiveTimer
struct  PuzzleLiveTimer_t464531872  : public TimeSystem_t3122972587
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLELIVETIMER_T464531872_H
#ifndef DOTWEENANIMATION_T3459117967_H
#define DOTWEENANIMATION_T3459117967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimation
struct  DOTweenAnimation_t3459117967  : public ABSAnimationComponent_t262169234
{
public:
	// System.Single DG.Tweening.DOTweenAnimation::delay
	float ___delay_19;
	// System.Single DG.Tweening.DOTweenAnimation::duration
	float ___duration_20;
	// DG.Tweening.Ease DG.Tweening.DOTweenAnimation::easeType
	int32_t ___easeType_21;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenAnimation::easeCurve
	AnimationCurve_t3046754366 * ___easeCurve_22;
	// DG.Tweening.LoopType DG.Tweening.DOTweenAnimation::loopType
	int32_t ___loopType_23;
	// System.Int32 DG.Tweening.DOTweenAnimation::loops
	int32_t ___loops_24;
	// System.String DG.Tweening.DOTweenAnimation::id
	String_t* ___id_25;
	// System.Boolean DG.Tweening.DOTweenAnimation::isRelative
	bool ___isRelative_26;
	// System.Boolean DG.Tweening.DOTweenAnimation::isFrom
	bool ___isFrom_27;
	// System.Boolean DG.Tweening.DOTweenAnimation::isIndependentUpdate
	bool ___isIndependentUpdate_28;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoKill
	bool ___autoKill_29;
	// System.Boolean DG.Tweening.DOTweenAnimation::isActive
	bool ___isActive_30;
	// System.Boolean DG.Tweening.DOTweenAnimation::isValid
	bool ___isValid_31;
	// UnityEngine.Component DG.Tweening.DOTweenAnimation::target
	Component_t1923634451 * ___target_32;
	// DG.Tweening.Core.DOTweenAnimationType DG.Tweening.DOTweenAnimation::animationType
	int32_t ___animationType_33;
	// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::targetType
	int32_t ___targetType_34;
	// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::forcedTargetType
	int32_t ___forcedTargetType_35;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoPlay
	bool ___autoPlay_36;
	// System.Boolean DG.Tweening.DOTweenAnimation::useTargetAsV3
	bool ___useTargetAsV3_37;
	// System.Single DG.Tweening.DOTweenAnimation::endValueFloat
	float ___endValueFloat_38;
	// UnityEngine.Vector3 DG.Tweening.DOTweenAnimation::endValueV3
	Vector3_t3722313464  ___endValueV3_39;
	// UnityEngine.Vector2 DG.Tweening.DOTweenAnimation::endValueV2
	Vector2_t2156229523  ___endValueV2_40;
	// UnityEngine.Color DG.Tweening.DOTweenAnimation::endValueColor
	Color_t2555686324  ___endValueColor_41;
	// System.String DG.Tweening.DOTweenAnimation::endValueString
	String_t* ___endValueString_42;
	// UnityEngine.Rect DG.Tweening.DOTweenAnimation::endValueRect
	Rect_t2360479859  ___endValueRect_43;
	// UnityEngine.Transform DG.Tweening.DOTweenAnimation::endValueTransform
	Transform_t3600365921 * ___endValueTransform_44;
	// System.Boolean DG.Tweening.DOTweenAnimation::optionalBool0
	bool ___optionalBool0_45;
	// System.Single DG.Tweening.DOTweenAnimation::optionalFloat0
	float ___optionalFloat0_46;
	// System.Int32 DG.Tweening.DOTweenAnimation::optionalInt0
	int32_t ___optionalInt0_47;
	// DG.Tweening.RotateMode DG.Tweening.DOTweenAnimation::optionalRotationMode
	int32_t ___optionalRotationMode_48;
	// DG.Tweening.ScrambleMode DG.Tweening.DOTweenAnimation::optionalScrambleMode
	int32_t ___optionalScrambleMode_49;
	// System.String DG.Tweening.DOTweenAnimation::optionalString
	String_t* ___optionalString_50;
	// System.Boolean DG.Tweening.DOTweenAnimation::_tweenCreated
	bool ____tweenCreated_51;
	// System.Int32 DG.Tweening.DOTweenAnimation::_playCount
	int32_t ____playCount_52;

public:
	inline static int32_t get_offset_of_delay_19() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___delay_19)); }
	inline float get_delay_19() const { return ___delay_19; }
	inline float* get_address_of_delay_19() { return &___delay_19; }
	inline void set_delay_19(float value)
	{
		___delay_19 = value;
	}

	inline static int32_t get_offset_of_duration_20() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___duration_20)); }
	inline float get_duration_20() const { return ___duration_20; }
	inline float* get_address_of_duration_20() { return &___duration_20; }
	inline void set_duration_20(float value)
	{
		___duration_20 = value;
	}

	inline static int32_t get_offset_of_easeType_21() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___easeType_21)); }
	inline int32_t get_easeType_21() const { return ___easeType_21; }
	inline int32_t* get_address_of_easeType_21() { return &___easeType_21; }
	inline void set_easeType_21(int32_t value)
	{
		___easeType_21 = value;
	}

	inline static int32_t get_offset_of_easeCurve_22() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___easeCurve_22)); }
	inline AnimationCurve_t3046754366 * get_easeCurve_22() const { return ___easeCurve_22; }
	inline AnimationCurve_t3046754366 ** get_address_of_easeCurve_22() { return &___easeCurve_22; }
	inline void set_easeCurve_22(AnimationCurve_t3046754366 * value)
	{
		___easeCurve_22 = value;
		Il2CppCodeGenWriteBarrier((&___easeCurve_22), value);
	}

	inline static int32_t get_offset_of_loopType_23() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___loopType_23)); }
	inline int32_t get_loopType_23() const { return ___loopType_23; }
	inline int32_t* get_address_of_loopType_23() { return &___loopType_23; }
	inline void set_loopType_23(int32_t value)
	{
		___loopType_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_id_25() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___id_25)); }
	inline String_t* get_id_25() const { return ___id_25; }
	inline String_t** get_address_of_id_25() { return &___id_25; }
	inline void set_id_25(String_t* value)
	{
		___id_25 = value;
		Il2CppCodeGenWriteBarrier((&___id_25), value);
	}

	inline static int32_t get_offset_of_isRelative_26() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isRelative_26)); }
	inline bool get_isRelative_26() const { return ___isRelative_26; }
	inline bool* get_address_of_isRelative_26() { return &___isRelative_26; }
	inline void set_isRelative_26(bool value)
	{
		___isRelative_26 = value;
	}

	inline static int32_t get_offset_of_isFrom_27() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isFrom_27)); }
	inline bool get_isFrom_27() const { return ___isFrom_27; }
	inline bool* get_address_of_isFrom_27() { return &___isFrom_27; }
	inline void set_isFrom_27(bool value)
	{
		___isFrom_27 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_28() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isIndependentUpdate_28)); }
	inline bool get_isIndependentUpdate_28() const { return ___isIndependentUpdate_28; }
	inline bool* get_address_of_isIndependentUpdate_28() { return &___isIndependentUpdate_28; }
	inline void set_isIndependentUpdate_28(bool value)
	{
		___isIndependentUpdate_28 = value;
	}

	inline static int32_t get_offset_of_autoKill_29() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___autoKill_29)); }
	inline bool get_autoKill_29() const { return ___autoKill_29; }
	inline bool* get_address_of_autoKill_29() { return &___autoKill_29; }
	inline void set_autoKill_29(bool value)
	{
		___autoKill_29 = value;
	}

	inline static int32_t get_offset_of_isActive_30() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isActive_30)); }
	inline bool get_isActive_30() const { return ___isActive_30; }
	inline bool* get_address_of_isActive_30() { return &___isActive_30; }
	inline void set_isActive_30(bool value)
	{
		___isActive_30 = value;
	}

	inline static int32_t get_offset_of_isValid_31() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isValid_31)); }
	inline bool get_isValid_31() const { return ___isValid_31; }
	inline bool* get_address_of_isValid_31() { return &___isValid_31; }
	inline void set_isValid_31(bool value)
	{
		___isValid_31 = value;
	}

	inline static int32_t get_offset_of_target_32() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___target_32)); }
	inline Component_t1923634451 * get_target_32() const { return ___target_32; }
	inline Component_t1923634451 ** get_address_of_target_32() { return &___target_32; }
	inline void set_target_32(Component_t1923634451 * value)
	{
		___target_32 = value;
		Il2CppCodeGenWriteBarrier((&___target_32), value);
	}

	inline static int32_t get_offset_of_animationType_33() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___animationType_33)); }
	inline int32_t get_animationType_33() const { return ___animationType_33; }
	inline int32_t* get_address_of_animationType_33() { return &___animationType_33; }
	inline void set_animationType_33(int32_t value)
	{
		___animationType_33 = value;
	}

	inline static int32_t get_offset_of_targetType_34() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___targetType_34)); }
	inline int32_t get_targetType_34() const { return ___targetType_34; }
	inline int32_t* get_address_of_targetType_34() { return &___targetType_34; }
	inline void set_targetType_34(int32_t value)
	{
		___targetType_34 = value;
	}

	inline static int32_t get_offset_of_forcedTargetType_35() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___forcedTargetType_35)); }
	inline int32_t get_forcedTargetType_35() const { return ___forcedTargetType_35; }
	inline int32_t* get_address_of_forcedTargetType_35() { return &___forcedTargetType_35; }
	inline void set_forcedTargetType_35(int32_t value)
	{
		___forcedTargetType_35 = value;
	}

	inline static int32_t get_offset_of_autoPlay_36() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___autoPlay_36)); }
	inline bool get_autoPlay_36() const { return ___autoPlay_36; }
	inline bool* get_address_of_autoPlay_36() { return &___autoPlay_36; }
	inline void set_autoPlay_36(bool value)
	{
		___autoPlay_36 = value;
	}

	inline static int32_t get_offset_of_useTargetAsV3_37() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___useTargetAsV3_37)); }
	inline bool get_useTargetAsV3_37() const { return ___useTargetAsV3_37; }
	inline bool* get_address_of_useTargetAsV3_37() { return &___useTargetAsV3_37; }
	inline void set_useTargetAsV3_37(bool value)
	{
		___useTargetAsV3_37 = value;
	}

	inline static int32_t get_offset_of_endValueFloat_38() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueFloat_38)); }
	inline float get_endValueFloat_38() const { return ___endValueFloat_38; }
	inline float* get_address_of_endValueFloat_38() { return &___endValueFloat_38; }
	inline void set_endValueFloat_38(float value)
	{
		___endValueFloat_38 = value;
	}

	inline static int32_t get_offset_of_endValueV3_39() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueV3_39)); }
	inline Vector3_t3722313464  get_endValueV3_39() const { return ___endValueV3_39; }
	inline Vector3_t3722313464 * get_address_of_endValueV3_39() { return &___endValueV3_39; }
	inline void set_endValueV3_39(Vector3_t3722313464  value)
	{
		___endValueV3_39 = value;
	}

	inline static int32_t get_offset_of_endValueV2_40() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueV2_40)); }
	inline Vector2_t2156229523  get_endValueV2_40() const { return ___endValueV2_40; }
	inline Vector2_t2156229523 * get_address_of_endValueV2_40() { return &___endValueV2_40; }
	inline void set_endValueV2_40(Vector2_t2156229523  value)
	{
		___endValueV2_40 = value;
	}

	inline static int32_t get_offset_of_endValueColor_41() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueColor_41)); }
	inline Color_t2555686324  get_endValueColor_41() const { return ___endValueColor_41; }
	inline Color_t2555686324 * get_address_of_endValueColor_41() { return &___endValueColor_41; }
	inline void set_endValueColor_41(Color_t2555686324  value)
	{
		___endValueColor_41 = value;
	}

	inline static int32_t get_offset_of_endValueString_42() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueString_42)); }
	inline String_t* get_endValueString_42() const { return ___endValueString_42; }
	inline String_t** get_address_of_endValueString_42() { return &___endValueString_42; }
	inline void set_endValueString_42(String_t* value)
	{
		___endValueString_42 = value;
		Il2CppCodeGenWriteBarrier((&___endValueString_42), value);
	}

	inline static int32_t get_offset_of_endValueRect_43() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueRect_43)); }
	inline Rect_t2360479859  get_endValueRect_43() const { return ___endValueRect_43; }
	inline Rect_t2360479859 * get_address_of_endValueRect_43() { return &___endValueRect_43; }
	inline void set_endValueRect_43(Rect_t2360479859  value)
	{
		___endValueRect_43 = value;
	}

	inline static int32_t get_offset_of_endValueTransform_44() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueTransform_44)); }
	inline Transform_t3600365921 * get_endValueTransform_44() const { return ___endValueTransform_44; }
	inline Transform_t3600365921 ** get_address_of_endValueTransform_44() { return &___endValueTransform_44; }
	inline void set_endValueTransform_44(Transform_t3600365921 * value)
	{
		___endValueTransform_44 = value;
		Il2CppCodeGenWriteBarrier((&___endValueTransform_44), value);
	}

	inline static int32_t get_offset_of_optionalBool0_45() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalBool0_45)); }
	inline bool get_optionalBool0_45() const { return ___optionalBool0_45; }
	inline bool* get_address_of_optionalBool0_45() { return &___optionalBool0_45; }
	inline void set_optionalBool0_45(bool value)
	{
		___optionalBool0_45 = value;
	}

	inline static int32_t get_offset_of_optionalFloat0_46() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalFloat0_46)); }
	inline float get_optionalFloat0_46() const { return ___optionalFloat0_46; }
	inline float* get_address_of_optionalFloat0_46() { return &___optionalFloat0_46; }
	inline void set_optionalFloat0_46(float value)
	{
		___optionalFloat0_46 = value;
	}

	inline static int32_t get_offset_of_optionalInt0_47() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalInt0_47)); }
	inline int32_t get_optionalInt0_47() const { return ___optionalInt0_47; }
	inline int32_t* get_address_of_optionalInt0_47() { return &___optionalInt0_47; }
	inline void set_optionalInt0_47(int32_t value)
	{
		___optionalInt0_47 = value;
	}

	inline static int32_t get_offset_of_optionalRotationMode_48() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalRotationMode_48)); }
	inline int32_t get_optionalRotationMode_48() const { return ___optionalRotationMode_48; }
	inline int32_t* get_address_of_optionalRotationMode_48() { return &___optionalRotationMode_48; }
	inline void set_optionalRotationMode_48(int32_t value)
	{
		___optionalRotationMode_48 = value;
	}

	inline static int32_t get_offset_of_optionalScrambleMode_49() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalScrambleMode_49)); }
	inline int32_t get_optionalScrambleMode_49() const { return ___optionalScrambleMode_49; }
	inline int32_t* get_address_of_optionalScrambleMode_49() { return &___optionalScrambleMode_49; }
	inline void set_optionalScrambleMode_49(int32_t value)
	{
		___optionalScrambleMode_49 = value;
	}

	inline static int32_t get_offset_of_optionalString_50() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalString_50)); }
	inline String_t* get_optionalString_50() const { return ___optionalString_50; }
	inline String_t** get_address_of_optionalString_50() { return &___optionalString_50; }
	inline void set_optionalString_50(String_t* value)
	{
		___optionalString_50 = value;
		Il2CppCodeGenWriteBarrier((&___optionalString_50), value);
	}

	inline static int32_t get_offset_of__tweenCreated_51() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ____tweenCreated_51)); }
	inline bool get__tweenCreated_51() const { return ____tweenCreated_51; }
	inline bool* get_address_of__tweenCreated_51() { return &____tweenCreated_51; }
	inline void set__tweenCreated_51(bool value)
	{
		____tweenCreated_51 = value;
	}

	inline static int32_t get_offset_of__playCount_52() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ____playCount_52)); }
	inline int32_t get__playCount_52() const { return ____playCount_52; }
	inline int32_t* get_address_of__playCount_52() { return &____playCount_52; }
	inline void set__playCount_52(int32_t value)
	{
		____playCount_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATION_T3459117967_H
#ifndef TITLEMANAGER_T232472850_H
#define TITLEMANAGER_T232472850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleManager
struct  TitleManager_t232472850  : public BaseScene_t2918414559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEMANAGER_T232472850_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (PlayerData_t220878115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[1] = 
{
	PlayerData_t220878115::get_offset_of_playerInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (SceneLoader_t4130533360), -1, sizeof(SceneLoader_t4130533360_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2401[4] = 
{
	SceneLoader_t4130533360_StaticFields::get_offset_of_Instance_2(),
	SceneLoader_t4130533360::get_offset_of_loadingScreen_3(),
	SceneLoader_t4130533360::get_offset_of_slider_4(),
	SceneLoader_t4130533360::get_offset_of_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[8] = 
{
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_sceneName_0(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U3CoperationU3E__0_1(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U3CPopU3E__0_2(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U3CprogressU3E__1_3(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U24this_4(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U24current_5(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U24disposing_6(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t4235513134::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (TimeManager_t1960693005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[6] = 
{
	TimeManager_t1960693005::get_offset_of_slowdownFactor_2(),
	TimeManager_t1960693005::get_offset_of_slowdownLength_3(),
	TimeManager_t1960693005::get_offset_of_isSlowMotion_4(),
	TimeManager_t1960693005::get_offset_of_liveTimer_5(),
	TimeManager_t1960693005::get_offset_of_candyTimer_6(),
	TimeManager_t1960693005::get_offset_of_charmTimer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (TimeInfo_t2264797512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[1] = 
{
	TimeInfo_t2264797512::get_offset_of_currentDateTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (U3CGetServerTimeU3Ec__Iterator0_t2759080239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[6] = 
{
	U3CGetServerTimeU3Ec__Iterator0_t2759080239::get_offset_of_action_0(),
	U3CGetServerTimeU3Ec__Iterator0_t2759080239::get_offset_of_U3CwwwU3E__1_1(),
	U3CGetServerTimeU3Ec__Iterator0_t2759080239::get_offset_of_U3CdataU3E__1_2(),
	U3CGetServerTimeU3Ec__Iterator0_t2759080239::get_offset_of_U24current_3(),
	U3CGetServerTimeU3Ec__Iterator0_t2759080239::get_offset_of_U24disposing_4(),
	U3CGetServerTimeU3Ec__Iterator0_t2759080239::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (TimeSystem_t3122972587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[4] = 
{
	TimeSystem_t3122972587::get_offset_of_timeSpan_2(),
	TimeSystem_t3122972587::get_offset_of_runningCountdown_3(),
	TimeSystem_t3122972587::get_offset_of_onCountdownUpdated_4(),
	TimeSystem_t3122972587::get_offset_of_onCountdownFinished_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (TitleManager_t232472850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (Player_t3266647312), -1, sizeof(Player_t3266647312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2408[2] = 
{
	Player_t3266647312_StaticFields::get_offset_of_Instance_2(),
	Player_t3266647312_StaticFields::get_offset_of__info_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (Info_t1004903773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[14] = 
{
	Info_t1004903773::get_offset_of_candy_0(),
	Info_t1004903773::get_offset_of_maxCandy_1(),
	Info_t1004903773::get_offset_of_nextCandyTime_2(),
	Info_t1004903773::get_offset_of_timeToNextCandy_3(),
	Info_t1004903773::get_offset_of_puzzleLive_4(),
	Info_t1004903773::get_offset_of_maxPuzzleLive_5(),
	Info_t1004903773::get_offset_of_nextPuzzleLiveTime_6(),
	Info_t1004903773::get_offset_of_timeToNextPuzzleLive_7(),
	Info_t1004903773::get_offset_of_currentCharm_8(),
	Info_t1004903773::get_offset_of_CharmDic_9(),
	Info_t1004903773::get_offset_of_maxCharm_10(),
	Info_t1004903773::get_offset_of_nextCharmTime_11(),
	Info_t1004903773::get_offset_of_timeToNextCharm_12(),
	Info_t1004903773::get_offset_of_itemCharm_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (ChangeCharmItem_t2112099122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[11] = 
{
	ChangeCharmItem_t2112099122::get_offset_of_charm_2(),
	ChangeCharmItem_t2112099122::get_offset_of_charmType_3(),
	ChangeCharmItem_t2112099122::get_offset_of_titleText_4(),
	ChangeCharmItem_t2112099122::get_offset_of_charmImage_5(),
	ChangeCharmItem_t2112099122::get_offset_of_damageText_6(),
	ChangeCharmItem_t2112099122::get_offset_of_countText_7(),
	ChangeCharmItem_t2112099122::get_offset_of_usingButton_8(),
	ChangeCharmItem_t2112099122::get_offset_of_changeButton_9(),
	ChangeCharmItem_t2112099122::get_offset_of_chargeButton_10(),
	ChangeCharmItem_t2112099122::get_offset_of_chargeTimer_11(),
	ChangeCharmItem_t2112099122::get_offset_of_chargeTimerText_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (ChangeCharmPopup_t1980703935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[4] = 
{
	ChangeCharmPopup_t1980703935::get_offset_of_charmGameManager_2(),
	ChangeCharmPopup_t1980703935::get_offset_of_charmList_3(),
	ChangeCharmPopup_t1980703935::get_offset_of_CharmShopPopup_4(),
	ChangeCharmPopup_t1980703935::get_offset_of_isCharging_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (U3CSetCharmItemsU3Ec__AnonStorey0_t1426642183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[2] = 
{
	U3CSetCharmItemsU3Ec__AnonStorey0_t1426642183::get_offset_of_charm_0(),
	U3CSetCharmItemsU3Ec__AnonStorey0_t1426642183::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (CharmShopItem_t1928446277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[6] = 
{
	CharmShopItem_t1928446277::get_offset_of_charm_2(),
	CharmShopItem_t1928446277::get_offset_of_count_3(),
	CharmShopItem_t1928446277::get_offset_of_image_4(),
	CharmShopItem_t1928446277::get_offset_of_titleText_5(),
	CharmShopItem_t1928446277::get_offset_of_priceText_6(),
	CharmShopItem_t1928446277::get_offset_of_GetButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (ItemCharmShopItem_t2921973635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[6] = 
{
	ItemCharmShopItem_t2921973635::get_offset_of_charm_2(),
	ItemCharmShopItem_t2921973635::get_offset_of_count_3(),
	ItemCharmShopItem_t2921973635::get_offset_of_image_4(),
	ItemCharmShopItem_t2921973635::get_offset_of_titleText_5(),
	ItemCharmShopItem_t2921973635::get_offset_of_priceText_6(),
	ItemCharmShopItem_t2921973635::get_offset_of_GetButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (LoadingPopup_t3786168181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[2] = 
{
	LoadingPopup_t3786168181::get_offset_of_slider_2(),
	LoadingPopup_t3786168181::get_offset_of_text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (CharacterPopup_t2617494106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (U3CAutoKillU3Ec__Iterator0_t3340172795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[4] = 
{
	U3CAutoKillU3Ec__Iterator0_t3340172795::get_offset_of_U24this_0(),
	U3CAutoKillU3Ec__Iterator0_t3340172795::get_offset_of_U24current_1(),
	U3CAutoKillU3Ec__Iterator0_t3340172795::get_offset_of_U24disposing_2(),
	U3CAutoKillU3Ec__Iterator0_t3340172795::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (GhostSchoolItemInfoPopup_t232986025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[4] = 
{
	GhostSchoolItemInfoPopup_t232986025::get_offset_of_levelText_6(),
	GhostSchoolItemInfoPopup_t232986025::get_offset_of_itemImage_7(),
	GhostSchoolItemInfoPopup_t232986025::get_offset_of_nameText_8(),
	GhostSchoolItemInfoPopup_t232986025::get_offset_of_effectText_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (GhostSchoolPopup_t1455275853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (U3CAutoKillU3Ec__Iterator0_t2318114076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[4] = 
{
	U3CAutoKillU3Ec__Iterator0_t2318114076::get_offset_of_U24this_0(),
	U3CAutoKillU3Ec__Iterator0_t2318114076::get_offset_of_U24current_1(),
	U3CAutoKillU3Ec__Iterator0_t2318114076::get_offset_of_U24disposing_2(),
	U3CAutoKillU3Ec__Iterator0_t2318114076::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (GhostSelectListPopup_t692877192), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U3CAutoKillU3Ec__Iterator0_t1906957385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[4] = 
{
	U3CAutoKillU3Ec__Iterator0_t1906957385::get_offset_of_U24this_0(),
	U3CAutoKillU3Ec__Iterator0_t1906957385::get_offset_of_U24current_1(),
	U3CAutoKillU3Ec__Iterator0_t1906957385::get_offset_of_U24disposing_2(),
	U3CAutoKillU3Ec__Iterator0_t1906957385::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (GhostSelectPopup_t3778673497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (U3CAutoKillU3Ec__Iterator0_t191949947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[4] = 
{
	U3CAutoKillU3Ec__Iterator0_t191949947::get_offset_of_U24this_0(),
	U3CAutoKillU3Ec__Iterator0_t191949947::get_offset_of_U24current_1(),
	U3CAutoKillU3Ec__Iterator0_t191949947::get_offset_of_U24disposing_2(),
	U3CAutoKillU3Ec__Iterator0_t191949947::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (GhostEffect_t2138921326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[5] = 
{
	GhostEffect_t2138921326::get_offset_of_spriteRenderer_2(),
	GhostEffect_t2138921326::get_offset_of_color_3(),
	GhostEffect_t2138921326::get_offset_of_timer_4(),
	GhostEffect_t2138921326::get_offset_of_alpha_5(),
	GhostEffect_t2138921326::get_offset_of_isEnable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (PuzzleItem_t2123471318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[5] = 
{
	PuzzleItem_t2123471318::get_offset_of_Row_2(),
	PuzzleItem_t2123471318::get_offset_of_Column_3(),
	PuzzleItem_t2123471318::get_offset_of_button_4(),
	PuzzleItem_t2123471318::get_offset_of_image_5(),
	PuzzleItem_t2123471318::get_offset_of_puzzleColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (PuzzleColor_t2810668265)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2427[7] = 
{
	PuzzleColor_t2810668265::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (EnumExtensions_t4248483755), -1, sizeof(EnumExtensions_t4248483755_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2428[1] = 
{
	EnumExtensions_t4248483755_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (PuzzleManager_t474948643), -1, sizeof(PuzzleManager_t474948643_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[7] = 
{
	PuzzleManager_t474948643_StaticFields::get_offset_of_Instance_2(),
	PuzzleManager_t474948643::get_offset_of_puzzleList_3(),
	PuzzleManager_t474948643::get_offset_of_spawnPosList_4(),
	PuzzleManager_t474948643::get_offset_of_Complete_List_5(),
	PuzzleManager_t474948643::get_offset_of_Check_List_6(),
	PuzzleManager_t474948643::get_offset_of_arrays_7(),
	PuzzleManager_t474948643::get_offset_of_puzzleItemPrefab_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (U3CAddPuzzleItemU3Ec__AnonStorey1_t2446037076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[2] = 
{
	U3CAddPuzzleItemU3Ec__AnonStorey1_t2446037076::get_offset_of_item_0(),
	U3CAddPuzzleItemU3Ec__AnonStorey1_t2446037076::get_offset_of_itemPrefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[6] = 
{
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618::get_offset_of_row_0(),
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618::get_offset_of_U3CemptyItemsU3E__0_1(),
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618::get_offset_of_U24this_2(),
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618::get_offset_of_U24current_3(),
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618::get_offset_of_U24disposing_4(),
	U3CSpawnNewPuzzleItemsU3Ec__Iterator0_t862492618::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[1] = 
{
	U3CSpawnNewPuzzleItemsU3Ec__AnonStorey2_t3561464378::get_offset_of_i_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[4] = 
{
	U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023::get_offset_of_targetItem_0(),
	U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023::get_offset_of_itemPrefab_1(),
	U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023::get_offset_of_U3CU3Ef__refU240_2(),
	U3CSpawnNewPuzzleItemsU3Ec__AnonStorey3_t832581023::get_offset_of_U3CU3Ef__refU242_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (GhostSkillInfo_t3192096645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[1] = 
{
	GhostSkillInfo_t3192096645::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (GhostSkillInfoItem_t3161929786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[5] = 
{
	GhostSkillInfoItem_t3161929786::get_offset_of_blockType_2(),
	GhostSkillInfoItem_t3161929786::get_offset_of_maskBG_3(),
	GhostSkillInfoItem_t3161929786::get_offset_of_spineGraphic_4(),
	GhostSkillInfoItem_t3161929786::get_offset_of_levelText_5(),
	GhostSkillInfoItem_t3161929786::get_offset_of_slider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (Sound_t3007421746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[4] = 
{
	Sound_t3007421746::get_offset_of_name_0(),
	Sound_t3007421746::get_offset_of_clip_1(),
	Sound_t3007421746::get_offset_of_source_2(),
	Sound_t3007421746::get_offset_of_loop_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (CandyTimer_t2979372557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (U3CRunningCountdownU3Ec__Iterator0_t2994566354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[4] = 
{
	U3CRunningCountdownU3Ec__Iterator0_t2994566354::get_offset_of_U24this_0(),
	U3CRunningCountdownU3Ec__Iterator0_t2994566354::get_offset_of_U24current_1(),
	U3CRunningCountdownU3Ec__Iterator0_t2994566354::get_offset_of_U24disposing_2(),
	U3CRunningCountdownU3Ec__Iterator0_t2994566354::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (CharmTimer_t90502612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (U3CRunningCountdownU3Ec__Iterator0_t3085404936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	U3CRunningCountdownU3Ec__Iterator0_t3085404936::get_offset_of_U24this_0(),
	U3CRunningCountdownU3Ec__Iterator0_t3085404936::get_offset_of_U24current_1(),
	U3CRunningCountdownU3Ec__Iterator0_t3085404936::get_offset_of_U24disposing_2(),
	U3CRunningCountdownU3Ec__Iterator0_t3085404936::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (PuzzleLiveTimer_t464531872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (U3CRunningCountdownU3Ec__Iterator0_t3301235440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[4] = 
{
	U3CRunningCountdownU3Ec__Iterator0_t3301235440::get_offset_of_U24this_0(),
	U3CRunningCountdownU3Ec__Iterator0_t3301235440::get_offset_of_U24current_1(),
	U3CRunningCountdownU3Ec__Iterator0_t3301235440::get_offset_of_U24disposing_2(),
	U3CRunningCountdownU3Ec__Iterator0_t3301235440::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (FreeItemCharmUI_t3416008829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[1] = 
{
	FreeItemCharmUI_t3416008829::get_offset_of_isTrigger_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (GhostInfoUI_t4215450973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[3] = 
{
	GhostInfoUI_t4215450973::get_offset_of_nameText_2(),
	GhostInfoUI_t4215450973::get_offset_of_descriptionText_3(),
	GhostInfoUI_t4215450973::get_offset_of_ghostHPSlider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (GhostSchoolInfo_t3497322382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[4] = 
{
	GhostSchoolInfo_t3497322382::get_offset_of_nameLevelText_6(),
	GhostSchoolInfo_t3497322382::get_offset_of_ghostBG_7(),
	GhostSchoolInfo_t3497322382::get_offset_of_spineGraphicGO_8(),
	GhostSchoolInfo_t3497322382::get_offset_of_spineGraphic_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (GhostSchoolItem_t364957914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[6] = 
{
	GhostSchoolItem_t364957914::get_offset_of_spineDataAsset_2(),
	GhostSchoolItem_t364957914::get_offset_of_ghost_3(),
	GhostSchoolItem_t364957914::get_offset_of_spineGraphicParent_4(),
	GhostSchoolItem_t364957914::get_offset_of_spineGraphic_5(),
	GhostSchoolItem_t364957914::get_offset_of_levelText_6(),
	GhostSchoolItem_t364957914::get_offset_of_nameText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (ItemCharmUI_t437376365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[7] = 
{
	ItemCharmUI_t437376365::get_offset_of_BG_image_2(),
	ItemCharmUI_t437376365::get_offset_of_image_3(),
	ItemCharmUI_t437376365::get_offset_of_titleText_4(),
	ItemCharmUI_t437376365::get_offset_of_countText_5(),
	ItemCharmUI_t437376365::get_offset_of_chargeItemCharmButton_6(),
	ItemCharmUI_t437376365::get_offset_of_isActive_7(),
	ItemCharmUI_t437376365::get_offset_of_isTrigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (MonsterInfoUI_t3033209612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[3] = 
{
	MonsterInfoUI_t3033209612::get_offset_of_nameText_2(),
	MonsterInfoUI_t3033209612::get_offset_of_descriptionText_3(),
	MonsterInfoUI_t3033209612::get_offset_of_monsterHPSlider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (RemainCharmUI_t475548631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	RemainCharmUI_t475548631::get_offset_of_charmCountImg_2(),
	RemainCharmUI_t475548631::get_offset_of_charmCountText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (ResultUI_t719777132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[5] = 
{
	ResultUI_t719777132::get_offset_of_monsterNameText_2(),
	ResultUI_t719777132::get_offset_of_SD_Graphic_3(),
	ResultUI_t719777132::get_offset_of_candyRemainTime_4(),
	ResultUI_t719777132::get_offset_of_candyRemainTimeText_5(),
	ResultUI_t719777132::get_offset_of_candyCountText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (WebCamera_t13333461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[4] = 
{
	WebCamera_t13333461::get_offset_of_camAvailable_2(),
	WebCamera_t13333461::get_offset_of_webCam_3(),
	WebCamera_t13333461::get_offset_of_background_4(),
	WebCamera_t13333461::get_offset_of_fitter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (DOTweenAnimation_t3459117967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[34] = 
{
	DOTweenAnimation_t3459117967::get_offset_of_delay_19(),
	DOTweenAnimation_t3459117967::get_offset_of_duration_20(),
	DOTweenAnimation_t3459117967::get_offset_of_easeType_21(),
	DOTweenAnimation_t3459117967::get_offset_of_easeCurve_22(),
	DOTweenAnimation_t3459117967::get_offset_of_loopType_23(),
	DOTweenAnimation_t3459117967::get_offset_of_loops_24(),
	DOTweenAnimation_t3459117967::get_offset_of_id_25(),
	DOTweenAnimation_t3459117967::get_offset_of_isRelative_26(),
	DOTweenAnimation_t3459117967::get_offset_of_isFrom_27(),
	DOTweenAnimation_t3459117967::get_offset_of_isIndependentUpdate_28(),
	DOTweenAnimation_t3459117967::get_offset_of_autoKill_29(),
	DOTweenAnimation_t3459117967::get_offset_of_isActive_30(),
	DOTweenAnimation_t3459117967::get_offset_of_isValid_31(),
	DOTweenAnimation_t3459117967::get_offset_of_target_32(),
	DOTweenAnimation_t3459117967::get_offset_of_animationType_33(),
	DOTweenAnimation_t3459117967::get_offset_of_targetType_34(),
	DOTweenAnimation_t3459117967::get_offset_of_forcedTargetType_35(),
	DOTweenAnimation_t3459117967::get_offset_of_autoPlay_36(),
	DOTweenAnimation_t3459117967::get_offset_of_useTargetAsV3_37(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueFloat_38(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueV3_39(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueV2_40(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueColor_41(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueString_42(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueRect_43(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueTransform_44(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalBool0_45(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalFloat0_46(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalInt0_47(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalRotationMode_48(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalScrambleMode_49(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalString_50(),
	DOTweenAnimation_t3459117967::get_offset_of__tweenCreated_51(),
	DOTweenAnimation_t3459117967::get_offset_of__playCount_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (DOTweenAnimationExtensions_t980608231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (DataSource_t833220627)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2454[3] = 
{
	DataSource_t833220627::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (ResponseStatus_t4073530167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2455[8] = 
{
	ResponseStatus_t4073530167::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (UIStatus_t582920877)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2456[9] = 
{
	UIStatus_t582920877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (LeaderboardStart_t3259090716)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2457[3] = 
{
	LeaderboardStart_t3259090716::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (LeaderboardTimeSpan_t1503936786)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2458[4] = 
{
	LeaderboardTimeSpan_t1503936786::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (LeaderboardCollection_t3003544407)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2459[3] = 
{
	LeaderboardCollection_t3003544407::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (VideoCaptureMode_t1984088482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2460[4] = 
{
	VideoCaptureMode_t1984088482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (VideoQualityLevel_t4283418091)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2461[6] = 
{
	VideoQualityLevel_t4283418091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (VideoCaptureOverlayState_t4111180056)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2462[6] = 
{
	VideoCaptureOverlayState_t4111180056::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (Gravity_t1500868723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2463[6] = 
{
	Gravity_t1500868723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (CommonTypesUtil_t3521372089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (EventVisibility_t3702936362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2465[3] = 
{
	EventVisibility_t3702936362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (AdvertisingResult_t1229207569)+ sizeof (RuntimeObject), sizeof(AdvertisingResult_t1229207569_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2467[2] = 
{
	AdvertisingResult_t1229207569::get_offset_of_mStatus_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AdvertisingResult_t1229207569::get_offset_of_mLocalEndpointName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (ConnectionRequest_t684574500)+ sizeof (RuntimeObject), sizeof(ConnectionRequest_t684574500_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2468[2] = 
{
	ConnectionRequest_t684574500::get_offset_of_mRemoteEndpoint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionRequest_t684574500::get_offset_of_mPayload_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (ConnectionResponse_t735328601)+ sizeof (RuntimeObject), sizeof(ConnectionResponse_t735328601_marshaled_pinvoke), sizeof(ConnectionResponse_t735328601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2469[5] = 
{
	ConnectionResponse_t735328601_StaticFields::get_offset_of_EmptyPayload_0(),
	ConnectionResponse_t735328601::get_offset_of_mLocalClientId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionResponse_t735328601::get_offset_of_mRemoteEndpointId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionResponse_t735328601::get_offset_of_mResponseStatus_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionResponse_t735328601::get_offset_of_mPayload_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (Status_t465938950)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2470[7] = 
{
	Status_t465938950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (EndpointDetails_t3891698496)+ sizeof (RuntimeObject), sizeof(EndpointDetails_t3891698496_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[3] = 
{
	EndpointDetails_t3891698496::get_offset_of_mEndpointId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EndpointDetails_t3891698496::get_offset_of_mName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EndpointDetails_t3891698496::get_offset_of_mServiceId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (InitializationStatus_t2437428114)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2474[4] = 
{
	InitializationStatus_t2437428114::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (NearbyConnectionConfiguration_t2019425596)+ sizeof (RuntimeObject), sizeof(NearbyConnectionConfiguration_t2019425596_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2475[4] = 
{
	0,
	0,
	NearbyConnectionConfiguration_t2019425596::get_offset_of_mInitializationCallback_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NearbyConnectionConfiguration_t2019425596::get_offset_of_mLocalClientId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (ConflictResolutionStrategy_t4255039905)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2476[7] = 
{
	ConflictResolutionStrategy_t4255039905::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (SavedGameRequestStatus_t3745141777)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2477[6] = 
{
	SavedGameRequestStatus_t3745141777::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (SelectUIStatus_t1293076877)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2478[7] = 
{
	SelectUIStatus_t1293076877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (ConflictCallback_t4045994657), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (SavedGameMetadataUpdate_t1775293339)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[5] = 
{
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SavedGameMetadataUpdate_t1775293339::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (Builder_t140438593)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[5] = 
{
	Builder_t140438593::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Builder_t140438593::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (VideoCapabilities_t1298735124), -1, sizeof(VideoCapabilities_t1298735124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2487[7] = 
{
	VideoCapabilities_t1298735124::get_offset_of_mIsCameraSupported_0(),
	VideoCapabilities_t1298735124::get_offset_of_mIsMicSupported_1(),
	VideoCapabilities_t1298735124::get_offset_of_mIsWriteStorageSupported_2(),
	VideoCapabilities_t1298735124::get_offset_of_mCaptureModesSupported_3(),
	VideoCapabilities_t1298735124::get_offset_of_mQualityLevelsSupported_4(),
	VideoCapabilities_t1298735124_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	VideoCapabilities_t1298735124_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (VideoCaptureState_t2350658599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[5] = 
{
	VideoCaptureState_t2350658599::get_offset_of_mIsCapturing_0(),
	VideoCaptureState_t2350658599::get_offset_of_mCaptureMode_1(),
	VideoCaptureState_t2350658599::get_offset_of_mQualityLevel_2(),
	VideoCaptureState_t2350658599::get_offset_of_mIsOverlayVisible_3(),
	VideoCaptureState_t2350658599::get_offset_of_mIsPaused_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (Logger_t3934082555), -1, sizeof(Logger_t3934082555_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2489[2] = 
{
	Logger_t3934082555_StaticFields::get_offset_of_debugLogEnabled_0(),
	Logger_t3934082555_StaticFields::get_offset_of_warningLogEnabled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (U3CdU3Ec__AnonStorey0_t2350509859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[1] = 
{
	U3CdU3Ec__AnonStorey0_t2350509859::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (U3CwU3Ec__AnonStorey1_t2080961746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[1] = 
{
	U3CwU3Ec__AnonStorey1_t2080961746::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (U3CeU3Ec__AnonStorey2_t2346119983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[1] = 
{
	U3CeU3Ec__AnonStorey2_t2346119983::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (Misc_t4208016214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (PlayGamesHelperObject_t4023745847), -1, sizeof(PlayGamesHelperObject_t4023745847_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2494[7] = 
{
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_instance_2(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sIsDummy_3(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sQueue_4(),
	PlayGamesHelperObject_t4023745847::get_offset_of_localQueue_5(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sQueueEmpty_6(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sPauseCallbackList_7(),
	PlayGamesHelperObject_t4023745847_StaticFields::get_offset_of_sFocusCallbackList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (U3CRunCoroutineU3Ec__AnonStorey0_t3592917427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[1] = 
{
	U3CRunCoroutineU3Ec__AnonStorey0_t3592917427::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (PluginVersion_t2872281160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (GPGSIds_t1675576834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (fsAotCompilationManager_t2784389809), -1, sizeof(fsAotCompilationManager_t2784389809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2498[1] = 
{
	fsAotCompilationManager_t2784389809_StaticFields::get_offset_of_AotCandidateTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (fsAotConfiguration_t27208811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[2] = 
{
	fsAotConfiguration_t27208811::get_offset_of_aotTypes_2(),
	fsAotConfiguration_t27208811::get_offset_of_outputDirectory_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
