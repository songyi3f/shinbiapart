﻿using UnityEngine;
using System.Collections;


public struct IntRange {
	public int min, max;
	
	public static IntRange Make(int min, int max) {
		return new IntRange { min=min, max=max };
	}

	public bool Contains(int v) {
		return v >= min && v <= max;
	}

	public int DistanceTo(int v) {
		if (v < min) {
			return min - v;
		} else if (v > max) {
			return v - max;
		} else {
			return 0;
		}
	}

	public int RandomPick() {
		int n = max - min + 1;
		if (n > 1) {
			return Random.Range(min, max+1);
		} else {
			return min;
		}
	}
	
	public int RandomPick(System.Func<int,int,int> randomRangeFunc) {
		int n = max - min + 1;
		if (n > 1) {
			return randomRangeFunc(min, max+1);
		} else {
			return min;
		}
	}

	public override string ToString() {
		return string.Format ("[{0}..{1}]", min, max);
	}
}


public struct FloatRange {
	public float min, max;
	
	public static FloatRange Make(float min, float max) {
		return new FloatRange { min=min, max=max };
	}

	public override string ToString () {
		return string.Format("[{0}, {1}]", min, max);
	}
}
