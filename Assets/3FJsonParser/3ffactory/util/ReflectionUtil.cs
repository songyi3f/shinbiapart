﻿using UnityEngine;
using System.Collections.Generic;

public static class ReflectionUtil {

	public static T GetFieldValue<T>(object obj, string name) where T : class {
		var field = obj.GetType ().GetField (name);
		if (field != null &&
			(field.FieldType == typeof(T) || field.FieldType.IsSubclassOf (typeof(T)))) {
			return (T)field.GetValue (obj);
		} else {
			return null;
		}
	}

	public static T GetFieldValue<T>(object obj, string name, T defVal) where T : struct {
		var field = obj.GetType ().GetField (name);
		if (field != null &&
			(field.FieldType == typeof(T) || field.FieldType.IsSubclassOf (typeof(T)))) {
			return (T)field.GetValue (obj);
		} else {
			return defVal;
		}
	}

	public static bool SetFieldValue<T>(object obj, string name, T v) {
		var field = obj.GetType ().GetField (name);
		if (field != null && field.FieldType == typeof(T)) {
			field.SetValue(obj, v);
			return true;
		} else {
			return false;
		}
	}

}
