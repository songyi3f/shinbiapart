﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using SimpleJSON;


namespace TF.JsonIO {
	
	public class JsonWriter : IEnumFieldsWithJson
    {
		static JsonWriter instance = new JsonWriter ();
		
		public void OnField(JSONNode root, object obj, FieldProp f, BindingFlags bindingAttr)
        {
			var valObj = f.GetValue (obj);
			
			if (string.IsNullOrEmpty (f.attr.Name))
            {
				ToJson(root, valObj, bindingAttr);
			}
            else
            {
                if (valObj == null)
                {
                    if (Util.IsGenericList(f.FieldType)){
                        Set(root, f.attr.Name,new JSONArray());
                    }
                    else
                    {
                        Set(root, f.attr.Name, new JSONClass());
                    }
                }
                else
                {
                    Set(root, f.attr.Name, ToJson(null, valObj, bindingAttr));
                }
			}
		}

		public static string WriteToString(object obj, BindingFlags bindingAttr)
        {
			var root = ToJson (null, obj, bindingAttr);
			var txt = root.ToString ();
			return txt;
		}

		public static void WriteToFile(string localPath, object obj, BindingFlags bindingAttr)
        {
			var root = ToJson (null, obj, bindingAttr);
			var txt = root.ToString ();
			Util.WriteTextFile (localPath, txt);
		}

		public static string ToJsonString(object obj, BindingFlags bindingAttr)
        {
			var root = ToJson (null, obj, bindingAttr);
			return root.ToString ();
		}

		public static JSONNode ToJson(JSONNode root, object obj, BindingFlags bindingAttr)
        {
			if (obj == null) {
				return new JSONClass();
			}
			
			var type = obj.GetType ();
			
			if (type.IsEnum) {
				return ToJsonEnum (type, obj);
			}
            else if (type == typeof(bool) || type == typeof(int) || type == typeof(float) || 
                     type == typeof(double) || type == typeof(string) || type == typeof(long)) {
				return new JSONData (obj.ToString ());
			}
            else if (type == typeof(System.DateTime))
            {
				var dt = (System.DateTime)obj;
				return dt.ToUniversalTime().ToString("O", System.Globalization.CultureInfo.InvariantCulture);
			}
            else if (type == typeof(Int2))
            {
				var v = (Int2)obj;
				return new JSONData (string.Format ("{0},{1}", v.x, v.y));
			}
            else if (type == typeof(IntRange))
            {
				var v = (IntRange)obj;
				return new JSONData (string.Format ("{0}~{1}", v.min, v.max));
			}
            else if (type == typeof(FloatRange))
            {
				var v = (FloatRange)obj;
				return new JSONData (string.Format ("{0}~{1}", v.min, v.max));
			}
            else if (type.IsArray && type.GetElementType () == typeof(byte))
            {
				var v = (byte[])obj;
				return new JSONData (System.Convert.ToBase64String (v));
			}
            else if (Util.IsGenericList(type))
            {
				//var argType = type.GetGenericArguments()[0];
				var list = (IList)obj;
				root = new JSONArray();
				for(int i = 0 ; i < list.Count ; ++i) {
					root.Add(ToJson(null, list[i], bindingAttr));
				}
				return root;
			}
            else if (Util.IsGenericListDicKeyValue(type))
            {
                var dic = (IDictionary)obj;
                root = new JSONClass();

                foreach (var entry in dic.Keys){
                    root.Add(ToJson(null, entry, bindingAttr), ToJson(null, dic[entry], bindingAttr));
                }
                return root;
            }
            else {
				
				if (root == null) {
					root = new JSONClass ();
				}
				
				Util.EnumFields (root, obj, instance, bindingAttr);
				return root;
			}
		}
		
		static void Set(JSONNode root, string name, JSONNode v)
        {
			var i = name.IndexOf ('/');
			if (i > 0) {
				var n1 = name.Substring(0, i);
				var n2 = name.Substring(i+1);
				var subNode = root[n1];
				if (subNode == null) {
					subNode = new JSONClass();
					root[n1] = subNode;
				}
				
				Set (subNode, n2, v);
			} else {
				root[name] = v;
			}
		}
		
		static JSONData ToJsonEnum(System.Type type, object obj)
        {
			var s = "";
			
			var memberInfo = type.GetMember (obj.ToString ());
			var attrs = memberInfo[0].GetCustomAttributes(typeof(DataNameAttribute), false);
			if (attrs == null || attrs.Length == 0) {
				s = obj.ToString ();
			} else {
				s = (attrs [0] as DataNameAttribute).Name;
			}
			
			return new JSONData(s);
		}
	}
}


