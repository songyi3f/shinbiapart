﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using SimpleJSON;

namespace TF.JsonIO
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class DataSerializableAttribute : Attribute
    {
		public DataSerializableAttribute()
        {

		}
	}

	[AttributeUsage(AttributeTargets.All, AllowMultiple=true)]
	public class DataNameAttribute : Attribute
    {
		public string Name { get; set; }
		public int Group { get; set; }
		public string Title { get; set; }

		private string titlePrefix = "";
		public string TitlePrefix
        {
			get { return titlePrefix; }
			set {
				titlePrefix = value;
				Title = titlePrefix + Name;
			}
		}

		public DataNameAttribute(string name) {
			this.Name = name;
			this.Title = name;
			this.Group = 0;
		}
	}

}
