﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using SimpleJSON;


namespace TF.JsonIO {

	public struct FieldProp {
		public FieldInfo field;
		public PropertyInfo prop;
		public DataNameAttribute attr;

		public static FieldProp Create(FieldInfo field, DataNameAttribute attr) {
			return new FieldProp { field = field, attr = attr };
		}

		public static FieldProp Create(PropertyInfo prop, DataNameAttribute attr) {
			return new FieldProp { prop = prop, attr = attr };
		}

		public Type FieldType {
			get {
				if (field != null) {
					return field.FieldType;
				} else if (prop != null) {
					return prop.PropertyType;
				} else {
					return null;
				}
			}
		}

		public object GetValue(object obj) {
			if (field != null) {
				return field.GetValue (obj);
			} else if (prop != null) {
				return prop.GetValue (obj, null);
			} else {
				return null;
			}
		}

		public void SetValue(object obj, object v) {
			if (field != null) {
				field.SetValue(obj, v);
			} else if (prop != null) {
				prop.SetValue(obj, v, null);
			}
		}
	}


	public interface IEnumFieldsWithJson {
		void OnField (JSONNode root, object obj, FieldProp field, BindingFlags bindingAttr);
	}


	public class Util {
		
		public static string AppDataFilePath(string localPath) {
			return System.IO.Path.Combine (Application.persistentDataPath, localPath + ".appdata");
		}
		
		public static string ReadTextFile(string localPath) {
			var filePath = AppDataFilePath(localPath);
			if (!System.IO.File.Exists (filePath)) {
				return "";
			}
			
			var txt = System.IO.File.ReadAllText (filePath);
			return txt;
		}
		
		public static void WriteTextFile(string localPath, string txt) {
			var filePath = AppDataFilePath(localPath);
			System.IO.File.WriteAllText (filePath, txt);
		}
		
		public static void EnumFields(JSONNode root, object obj, IEnumFieldsWithJson cb, BindingFlags bindingAttr)
        {
			var type = obj.GetType ();
            foreach (var field in type.GetFields (bindingAttr))
            {
				var attrs = field.GetCustomAttributes(typeof(DataNameAttribute), false);
				if (attrs.Length > 0) {
					var a = attrs[0] as DataNameAttribute;
					if (a != null) {
						cb.OnField(root, obj, FieldProp.Create(field, a), bindingAttr);
					}
				}
			}
			
			foreach(var prop in type.GetProperties ())
            {
				var attrs = prop.GetCustomAttributes(typeof(DataNameAttribute), false);

				if (attrs.Length > 0) {
					var a = attrs[0] as DataNameAttribute;
					if (a != null) {
						cb.OnField(root, obj, FieldProp.Create(prop, a), bindingAttr);
					}
				}
			}
		}
		
		public static void EnumFields(Type type, System.Action<FieldProp> cb, BindingFlags bindingAttr)
        {
			foreach(var field in type.GetFields ())
            {
				var attrs = field.GetCustomAttributes(typeof(DataNameAttribute), false);
				if (attrs.Length > 0) {
					var a = attrs[0] as DataNameAttribute;
					if (a != null) {
						cb(FieldProp.Create(field, a));
					}
				}
			}
			
			foreach(var prop in type.GetProperties ())
            {
				var attrs = prop.GetCustomAttributes(typeof(DataNameAttribute), false);
				if (attrs.Length > 0) {
					var a = attrs[0] as DataNameAttribute;
					if (a != null) {
						cb(FieldProp.Create(prop, a));
					}
				}
			}
		}


		public static bool IsDataSerializable(Type type) {
			return type.GetCustomAttributes (typeof(DataSerializableAttribute), false).Length > 0;
		}
		
		public static bool IsGenericList(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition () == typeof(List<>);
		}

        public static bool IsGenericListDicKeyValue(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>);
        }

        public static bool IsNullable(Type type) {
			return type.IsGenericType && type.GetGenericTypeDefinition () == typeof(Nullable<>);
		}
	}
	
}

