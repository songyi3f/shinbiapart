﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using SimpleJSON;


namespace TF.JsonIO {

	public class JsonReader : IEnumFieldsWithJson {
		static JsonReader instance = new JsonReader ();
		
		public void OnField(JSONNode root, object obj, FieldProp f, BindingFlags bindingAttrBba)
        {
			var fieldjson = root;

			if (!string.IsNullOrEmpty(f.attr.Name)){
				fieldjson = Get(root, f.attr.Name);
			}
			
			object valueObj = LoadObject(fieldjson, null, f.FieldType, bindingAttrBba);
			
			f.SetValue (obj, valueObj);
		}

		public static T ReadFromString<T>(string txt, BindingFlags bindingAttr) where T : class, new()
        {
			if (string.IsNullOrEmpty (txt))
				return null;

			var json = JSON.Parse(txt);
			return (T)LoadObject(json, null, typeof(T), bindingAttr);
		}
        public static object ReadFromString(string txt, object obj, Type type, BindingFlags bindingAttr)
        {
            if (string.IsNullOrEmpty(txt))
                return null;

            var json = JSON.Parse(txt);
            return LoadObject(json, obj, type, bindingAttr);
        }
        public static T ReadFromString<T>(string txt,Type type, BindingFlags bindingAttr)
        {
            if (string.IsNullOrEmpty(txt))
                return default(T);

            var json = JSON.Parse(txt);
            return (T)LoadObject(json, null, type, bindingAttr);
        }

        public static T ReadFromFile<T>(string localPath, BindingFlags bindingAttr) where T : class, new()
        {
			var txt = Util.ReadTextFile(localPath);
			if (string.IsNullOrEmpty (txt))
				return null;
			
			var json = JSON.Parse(txt);
			return (T)LoadObject(json, null, typeof(T), bindingAttr);
		}
		
		public static object LoadObject(JSONNode root, object obj, Type type, BindingFlags bindingAttr) {
			if (type.IsEnum) {
				obj = ToEnum (type, root);
			}
            else if (type == typeof(bool))
            {
				obj = ToBool (root);
			}
            else if (type == typeof(int))
            {
				obj = ToInt (root);
			}
            else if (type == typeof(float))
            {
				obj = ToFloat (root);
			}
            else if (type == typeof(double))
            {
				obj = ToDouble (root);
			}
            else if (type == typeof(long))
            {
                obj = ToLong(root);
            }
            else if (type == typeof(Int2)) {
				obj = ToInt2 (root);
			}
            else if (type == typeof(IntRange)) {
				obj = ToIntRange (root);
			}
            else if (type == typeof(FloatRange)) {
				obj = ToFloatRange (root);
			}
            else if (type == typeof(string))
            {
                if ((string.IsNullOrEmpty(root.Value) || root.Value == "null"))
                {
                    obj = null;
                }
                else
                {
                    obj = root.Value;
                }
			}
            else if (type == typeof(System.DateTime)) {
				obj = ToDateTime(root);
			}
            else if (Util.IsNullable(type))
            {
				if ((string.IsNullOrEmpty(root.Value) || root.Value == "null") && root.Count == 0)
                {
					obj = null;
				}
                else
                {
					var argType = type.GetGenericArguments()[0];
					var arg = LoadObject(root, null, argType, bindingAttr);
					obj = System.Activator.CreateInstance(type, arg);
				}
			}
            else if (type.IsArray && type.GetElementType() == typeof(byte))
            {
				if (string.IsNullOrEmpty(root.Value)) {
					obj = null;
				}
                else {
					obj = System.Convert.FromBase64String(root.Value);
				}
			}
            else if (Util.IsGenericList(type))
            {
				if (obj == null) {
					obj = System.Activator.CreateInstance(type);
				}
				
				var argType = type.GetGenericArguments()[0];
				var list = (IList)obj;
				list.Clear();
				
				for(int i = 0 ; i < root.Count ; ++i) {
					list.Add(LoadObject(root[i], null, argType, bindingAttr));
				}
            }
            else if (Util.IsGenericListDicKeyValue(type))
            {
                if (obj == null){
                    obj = System.Activator.CreateInstance(type);
                }

                var argKey   = type.GetGenericArguments()[0];
                var argValue = type.GetGenericArguments()[1];
                var dic = (IDictionary)obj;

                dic.Clear();

                for (int i = 0; i < root.Count; ++i)
                {
                    var key   = LoadObject(root.GetKey(i), null, argKey, bindingAttr);
                    var value = LoadObject(root[i], null, argValue, bindingAttr);
                    dic.Add(key, value);
                }
            }
            else
            {
				if (Util.IsDataSerializable(type))
                {
					if (obj == null) {
						obj = System.Activator.CreateInstance(type);
					}
					
					Util.EnumFields(root, obj, instance, bindingAttr);
				}
                else {
					throw new System.ArgumentException (
						string.Format("JsonReader.LoadObject - not serializable (t={0}, g={1})",
							type, type.IsGenericType),
						type.ToString());
				}
			}
			
			return obj;
		}
		
		static JSONNode Get(JSONNode root, string name) {
			var i = name.IndexOf ('/');
			if (i > 0) {
				var n1 = name.Substring(0, i);
				var n2 = name.Substring(i+1);
				return Get (root[n1], n2);
			} else {
				return root[name];
			}
		}
		
		static float ToFloat(JSONNode root) {
			var s = root.Value.TrimEnd ('%').Replace(",", "");
			if (string.IsNullOrEmpty (s))
				return 0;
			
			float v;
			if (float.TryParse (s, out v)) {
				return v;
			} else {
				throw new System.ArgumentException("ToFloat", root.Value);
			}
		}

        static long ToLong(JSONNode root)
        {
            var s = root.Value.TrimEnd('%').Replace(",", "");
            if (string.IsNullOrEmpty(s))
                return 0;

            long v;
            if (long.TryParse(s, out v))
            {
                return v;
            }
            else
            {
                throw new System.ArgumentException("ToLong", root.Value);
            }
        }
		static double ToDouble(JSONNode root) {
			var s = root.Value.TrimEnd ('%').Replace(",", "");
			if (string.IsNullOrEmpty (s))
				return 0;

			double v;
			if (double.TryParse (s, out v)) {
				return v;
			} else {
				throw new System.ArgumentException("ToDouble", root.Value);
			}
		}

		static bool ToBool(JSONNode root) {
			var s = root.Value;
			if (string.IsNullOrEmpty (s))
				return false;

			return s.ToLower () == "true";
		}

		static int ToInt(JSONNode root) {
			var s = root.Value.TrimEnd ('%').Replace(",", "");
			if (string.IsNullOrEmpty (s))
				return 0;
			
			int v;
			if (int.TryParse (s, out v)) {
				return v;
			}
            else {
				throw new System.ArgumentException("ToInt", root.Value);
			}
		}
		
		static Int2 ToInt2(JSONNode root) {
			var s = root.Value;
			if (string.IsNullOrEmpty (s)) {
				return Int2.zero;
			}

			var i = s.IndexOf (',');
			if (i > 0){
				return new Int2(int.Parse(s.Substring(0, i)), int.Parse(s.Substring(i+1)));
			}
            else {
				throw new System.ArgumentException("ToInt2", s);
			}
		}
		
		static IntRange ToIntRange(JSONNode root) {
			var s = root.Value;
			var i = s.IndexOf ('~');
			if (i > 0) {
				return IntRange.Make(int.Parse(s.Substring(0, i)), int.Parse(s.Substring(i+1)));
			} else {
				throw new System.ArgumentException("ToIntRange", s);
			}
		}
		
		static FloatRange ToFloatRange(JSONNode root) {
			return FloatRange.Make (ToFloat(root[0]), ToFloat(root[1]));
		}
		
		static float PercentToFloat(JSONNode root) {
			var f = ToFloat(root) / 100.0f;
			
			if (f < 0 || f > 1) {
				throw new System.ArgumentException ("PercentToFloat", root.Value);
			}
			
			return f;
		}
		
		static object ToEnum(System.Type type, string s)
        {
			foreach (var v in System.Enum.GetValues(type))
            {
				var f = type.GetField(System.Enum.GetName(type, v));
				var attrs = f.GetCustomAttributes(typeof(DataNameAttribute), false);

				foreach(DataNameAttribute a in attrs)
                {
					if (string.Equals(a.Name, s, System.StringComparison.OrdinalIgnoreCase)) {
						return System.Enum.ToObject(type, v);
					}
				}
			}
			
			Debug.LogWarning(string.Format("ToEnum: {0}.Parse('{1}')", type.Name, s));

			if (string.IsNullOrEmpty (s))
				return null;

			return System.Enum.Parse (type, s);
		}
		
		static System.DateTime ToDateTime(JSONNode root) {
			if (string.IsNullOrEmpty (root.Value)) {
				return new System.DateTime();
			}
			
			return System.DateTime.Parse (root.Value);
		}
	}
}


