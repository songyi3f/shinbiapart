﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TF.JsonIO;
using UnityEngine;
using UnityEngine.UI;

//!<==========================================================================

public enum ParserEnumType
{
    [DataName("None")] None,
    [DataName("EnumDevelopment")] Development,
    [DataName("EnumProduction")]  Production,
}

//!<==========================================================================

[DataSerializable]
public class ParserSample
{
    [DataName("IntValue")]
    public int IntValue;

    [DataName("IntValue2")]
    public int IntValue2 { get; protected set; }

    [DataName("String")]
    public string StringValue;

    [DataName("Float")]
    public float FloatValue;
}

//!<==========================================================================

[DataSerializable]
public class ParserSampleChild : ParserSample
{
    [DataName("IntValue2")]
    public List<ParserEnumType> ParserEnumTypes;

    [DataName("IntRange")]
    public IntRange IntRangeValue;
}

//!<==========================================================================

[DataSerializable]
public class SampleConfigs
{
    [DataName("Version")]
    public int Version { get; protected set; }
}

[DataSerializable]
public class BlockGoldScoreConfigs
{
	[DataName("Index")]
	public int Index;
	[DataName("GetGold")]
	public int GetGold;
	[DataName("GetScore")]
	public int GetScore;
}

[DataSerializable]
public class BlockConfigs
{
	[DataName("ScorePerBlock")]
	public int ScorePerBlock { get; protected set; }
	[DataName("GoldPerCoinBlock")]
	public int GoldPerCoinBlock { get; protected set; }
	[DataName("SkillEnergyPerBlock")]
	public int SkillEnergyPerBlock { get; protected set; }
	[DataName("SkillUseEnergy")]
	public int SkillUseEnergy { get; protected set; }
}

//!<==========================================================================

[DataSerializable]
public class SampleJsonConfigs
{
	[DataName("BlockGoldScoreConfigs")]
	public 	BlockGoldScoreConfigs blockGoldScoreConfigs;
	[DataName("BlockConfigs")]
	public BlockConfigs blockConfigs;

//    [DataName("Config")]
//    public SampleConfigs Config { get; protected set; }
//	[DataName("Config1")]
//    protected ParserSample      Sample1;
//	[DataName("Config2")]
//    protected ParserSampleChild Sample2;

}

//!<==========================================================================

public class SampleJsonConfigLoader
{
    public readonly string FilePath = "Data/GameConfig";

    public void StartLoading(SampleJsonConfigs configs)
    {
        var txt = Resources.Load<TextAsset>(FilePath);
        if (txt != null){
			Debug.Log ("txt is Not Null");
            this.LoadFromString(txt.text, configs);
        }
		else
			Debug.Log ("txt is Null");
    }

    public void StartLoadingFromWWW(MonoBehaviour mb,SampleJsonConfigs configs)
    {
        mb.StartCoroutine(this.DownloadConfig(configs));
    }

    public void WriteFile(string filePath, string data)
    {
        using (FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
        {
            using (var writer = new StreamWriter(fileStream)){
                writer.Write(data);
                writer.Flush();
            }
        }
    }

    private IEnumerator DownloadConfig(SampleJsonConfigs configs)
    {
		var url = "http://nexnwebapp.appspot.com/json?key=songyi3f%40gmail.com%2F1owKJYtlucvoldrs8Qjct4EIOjevjm1ulZEt3zywikJQ&pass=2679518dee4f31cd";
        var www = new WWW(url);

        yield return www;
        var txt = www.text;
        try
        {
            if (LoadFromString(txt,configs))
            {
                var fn = "ConfigFile";
                this.WriteFile(fn, txt);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
        }
    }
    bool LoadFromString(string txt, SampleJsonConfigs configs)
    {
        if (string.IsNullOrEmpty(txt)){
            return false;
        }

        SimpleJSON.JSONNode json = null;

        json = SimpleJSON.JSON.Parse(txt);

        if (json == null){
            return false;
        }

        try
        {
            configs = JsonReader.LoadObject(json,configs, typeof(SampleJsonConfigs),
                System.Reflection.BindingFlags.Instance | 
                System.Reflection.BindingFlags.Public | 
                System.Reflection.BindingFlags.NonPublic) as SampleJsonConfigs;
			Debug.Log ("configs try");
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
            return false;
        }

        return true;
    }
}

//!<==========================================================================

public class SampleLogic : MonoBehaviour
{
    //!<======================================================================

    [SerializeField] protected Button FileLoadBtn;
    [SerializeField] protected Button WWWLoadBtn;
	[SerializeField] protected Button ShowBtn;
    //!<======================================================================

    protected SampleJsonConfigs Configs;

    //!<======================================================================

    private void Awake()
    {
        this.FileLoadBtn.onClick.AddListener(() => 
        {
            this.Configs = new SampleJsonConfigs();
            new SampleJsonConfigLoader().StartLoading(this.Configs);
        });

        this.WWWLoadBtn.onClick.AddListener(() =>
        {
            this.Configs = new SampleJsonConfigs();
            new SampleJsonConfigLoader().StartLoadingFromWWW(this, this.Configs);
        });

		this.ShowBtn.onClick.AddListener (() => 
		{
				Debug.Log("GoldPerCoinBlock : " + this.Configs.blockConfigs.GoldPerCoinBlock);
				Debug.Log("ScorePerBlock : " + this.Configs.blockConfigs.ScorePerBlock);
				Debug.Log("SkillEnergyPerBlock : " + this.Configs.blockConfigs.SkillEnergyPerBlock);
				Debug.Log("SkillUseEnergy : " + this.Configs.blockConfigs.SkillUseEnergy);
		});
    }

}
