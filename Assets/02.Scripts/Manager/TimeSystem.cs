﻿using UnityEngine;
using System.Collections;
using System;

public class TimeSystem : MonoBehaviour {

	#region Variables
    // 충전 시스템
	protected TimeSpan timeSpan;
	protected bool runningCountdown;

    public Action<TimeSpan, int> onCountdownUpdated;
    public Action onCountdownFinished;   
    #endregion

}
