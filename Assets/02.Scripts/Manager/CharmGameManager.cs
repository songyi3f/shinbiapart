﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using UnityEngine.UI;
using Spine.Unity;

public enum CharmGameState { Idle, Play, Stop } // 게임 상태
public enum MoveDirection { Right, Left }  // 포인터 방향

public class CharmGameManager : MonoBehaviour
{
	#region Variables   
	public CharmGameState gameState;

	// 부적 리스트
	public CharmTrigger[] CharmList;
	private int targetCharmIdx; // 목표 부적 인덱스 
	private int newCharmIndex = int.MinValue;
	public GameObject charmPrefab;
	public int enabledCharmCount;

	// 남은 부적 표시 UI
	public RemainCharmUI remainCharm_UI;

	// 포인터   
	public Transform pointer;
	private MoveDirection moveDirection;

	// 터치 영역
	public GameObject touchArea;

	// 귀신 정보
	private GhostManager ghostManager;   
	public GhostInfoUI ghostInfoUI;

	// UI 오브젝트
	public GameObject GameMode_UI;
	public LastAttackManager LastAttack_UI;
	public ResultUI Result_UI;

	// UI 팝업
	public GameObject GameOverPopup;
	public GameObject ConfirmOutPopup;
	public GameObject RefillPopup;
	public ChangeCharmPopup ChangeCharmPopup;

	// 데미지 팝업 효과
	public GameObject damagePopText;

	// 게임 시간 데이터
	public Text timeText;
	public bool stopTime = false;

	#endregion

	private void OnEnable()
	{
		gameState = CharmGameState.Idle;
		moveDirection = MoveDirection.Right;

		// 가장 왼쪽(첫번째) 부적에서 시작 
		targetCharmIdx = 0;
		// 몬스터 HP & 정보 초기화
		ghostInfoUI.SetMonsterInfo(GameManager.instance.ghostManager.ghost);
		// UI 활성
		GameMode_UI.SetActive(true);
		pointer.gameObject.SetActive(false);
		LastAttack_UI.gameObject.SetActive(false);
		Result_UI.gameObject.SetActive(false);
		GameOverPopup.SetActive(false);
		ConfirmOutPopup.SetActive(false);
		RefillPopup.SetActive(false);
		ChangeCharmPopup.gameObject.SetActive(false);
      
		// 부적 활성       
		StartCoroutine("ShowAllCharm");
	}

	private void Start()
	{
		MasterAgent.Instance.timeManager.charmTimer.Subscribe(OnCandyCountdownUpdated, OnCandyCountdownFinished);
		ghostManager = GameManager.instance.ghostManager;
	}

	private void OnDisable()
	{
		pointer.DOKill();
		pointer.localPosition = new Vector3(-600f, -630f, pointer.localPosition.z);
		StopCoroutine("ShowAllCharm");
		touchArea.SetActive(false);

		MasterAgent.Instance.timeManager.charmTimer.Unsubscribe(OnCandyCountdownUpdated, OnCandyCountdownFinished);
	}

	void OnCandyCountdownUpdated(System.TimeSpan timeSpan, int charm)
    {
		ChangeCharmPopup.OnCharmUpdated(timeSpan, charm);
    }

    void OnCandyCountdownFinished()
    {
		ChangeCharmPopup.OnCharmFinished();
    }
       
	/// <summary>
	/// 게임 시간 측정
	/// </summary>
	IEnumerator GameTime()
	{
		int time = 60;
		stopTime = false;
		timeText.text = time.ToString();

		while (time > 0)
		{
			yield return new WaitUntil(() => !stopTime);
			timeText.text = time.ToString();
			yield return new WaitForSeconds(1f);
			time -= 1;
		}

		timeText.text = "0"; // 게임오버

		if (gameState == CharmGameState.Play)
		{
			gameState = CharmGameState.Idle;
			GameOverPopup.SetActive(true);
			timeText.text = "";
			// 게임 중일 때 귀신 도망감
			//monster.transform.DOScale(0f, 0.3f)
			//	   .OnComplete(() => GameManager.instance.StartCoroutine(GameManager.instance.StartVRMode(2f)));
		}
	}

	/// <summary>
	/// 게임 타임 활성 & 비활성    
	/// </summary>
	public void StopTime(bool isActive)
	{
		stopTime = isActive;
	}

	/// <summary>
	/// 부적 교체 버튼 이벤트
	/// </summary>
	public void ChangeCharms(Charm charm)
	{
		int enableCount = 0;
		foreach (CharmTrigger charmTrigger in CharmList)
		{
			if (charmTrigger.isEnable)
				enableCount++;

			charmTrigger.GetComponent<Image>().sprite = charm.image;
		}

		Player.Instance.Info.currentCharm = charm.charmType;

		int remainCharmCount = enableCount - Player.Instance.GetCharmCount(charm.charmType);
		if (remainCharmCount > 0)
		{
			// 깔린 부적보다 교체할 부적 개수가 적을 때 : 교체할 부적 개수만큼만 교체 후 나머지 부적 비활성시킴
			for (int i = 0; i < remainCharmCount; i++)
			{
				CharmTrigger charmItem = CharmList.First((x) => x.isEnable == true);
				charmItem.isEnable = false;
			}
		}

		// 오른쪽 밑 부적UI 갱신
		remainCharm_UI.SetRemainCharmInfo(charm.image, Player.Instance.GetCurrentCharmCount() - enableCount);
	}

	/// <summary>
	/// 처음 모든 부적 깔릴 때 애니메이션
	/// </summary>
	IEnumerator ShowAllCharm()
	{
		if (Player.Instance.GetCurrentCharmCount() <= 0)
		{
			// 1. 부적이 없을 때 : 부적 교체 팝업창 띄우기
			remainCharm_UI.SetRemainCharmInfo(Player.Instance.GetCharmSprite(), 0);
			stopTime = true;
			ChangeCharmPopup.gameObject.SetActive(true);
			yield return new WaitUntil(() => Player.Instance.GetCurrentCharmCount() > 0 && stopTime == false);
			ChangeCharmPopup.gameObject.SetActive(false);
		}

		for (int i = 0; i < CharmList.Length; i++)
			CharmList[i].isEnable = false;

		List<int> list = new List<int>(); // 리스트 생성 : 0~6까지 인덱스 들어있음
		for (int i = 0; i <= 6; i++)
			list.Add(i);
      
		int remainCharmCount = Player.Instance.GetCurrentCharmCount() - CharmList.Length;
		if (remainCharmCount < 0)
		{
			// 2. 부적이 7개 미만일 때 : 있는 개수 만큼만 생성됨         
			for (int i = 0; i < Mathf.Abs(remainCharmCount); i++)
			{
				int idx = Random.Range(0, list.Count);
				list.RemoveAt(idx);
			}
		}

		enabledCharmCount = 0;
		foreach (int idx in list)
		{
			CharmList[idx].isEnable = true;
			enabledCharmCount++;
		}

		// 오른쪽 밑 부적UI 갱신   
		remainCharm_UI.SetRemainCharmInfo(Player.Instance.GetCharmSprite(), remainCharmCount);
      
		// 부적 생성 애니메이션
		for (int i = 0; i < CharmList.Length; i++)
		{
			CharmList[i].GetComponent<Image>().sprite = Player.Instance.GetCharmSprite();
			CharmList[i].transform.localPosition = new Vector3(CharmList[i].transform.localPosition.x, -600f, 0f);
		}

		for (int i = 0; i < CharmList.Length; i++)
		{
			if (CharmList[i].isEnable)
				CharmList[i].transform.DOLocalMoveY(0f, 0.07f);
			yield return new WaitForSeconds(0.05f);
		}

		if (!pointer.gameObject.activeInHierarchy)
		{
			pointer.gameObject.SetActive(true);
			pointer.GetComponent<Animator>().speed = 5f;
			pointer.GetComponent<Animator>().SetTrigger("Play");
		}

		yield return new WaitForSeconds(1f);

		// 게임 첫 시작 일 때만
		if (gameState == CharmGameState.Idle)
		{
			gameState = CharmGameState.Play;
			MovePointer();
			touchArea.SetActive(true);
			StartCoroutine("GameTime"); // 게임 시간
		}
		else if (gameState == CharmGameState.Play)
		{
			// 게임중 부적교체 팝업 떳을 때 포인터가 멈추는 경우가 있어 재시작 시켜줌
			if (!DOTween.IsTweening(pointer))
			{
				Debug.Log("포인터 멈춰있을 때 재시작");
				pointer.DOKill();
				MovePointer();
			}
		}
	}

	/// <summary>
	/// 포인터 멈췄을 때 이벤트
	/// </summary>
	public void PausePointer()
	{
		if (gameState != CharmGameState.Play)
			return;

		CharmTrigger selectedCharm = CharmList.FirstOrDefault((x) => x.isSelected && x.isEnable);

		if (selectedCharm == null)
		{
			// 부적 맞추지 못했을 때 -> 몬스터 공격 애니메이션
			ghostManager.Animation(AnimationName.attack, false);
			return;
		}
      
		selectedCharm.isEnable = false;

		// 부적 사용
		Player.Instance.RefreshCharm(-1);
		enabledCharmCount--;

		// 포인터 애니메이션
		pointer.GetComponent<Animator>().speed = 5f;
		pointer.GetComponent<Animator>().SetTrigger("Play");

		// 부적 날아가는 애니메이션 -> 날아갈 부적은 새로 생성해준다
		GameObject flyCharm = Instantiate(charmPrefab, selectedCharm.transform.parent);
		flyCharm.GetComponentInChildren<SpriteRenderer>().sprite = selectedCharm.GetComponent<Image>().sprite;
		flyCharm.transform.localScale = Vector3.one * 11.5f;
		flyCharm.transform.localPosition = selectedCharm.transform.localPosition;
		flyCharm.transform.localRotation = selectedCharm.transform.localRotation;

		Sequence sequence = DOTween.Sequence();
		sequence.Append(flyCharm.transform.DOLookAt(ghostManager.ghostInfo.targetPoint.position, 0.05f));
		sequence.PrependInterval(0.05f);
		flyCharm.transform.SetParent(ghostManager.ghostInfo.targetPoint);
		sequence.Append(flyCharm.transform.DOLocalMove(new Vector3(Random.Range(-0.8f, 0.8f), Random.Range(-0.8f, 0.8f), 0f), 0.1f)
						.OnStart(() => CheckCharmZeroCount()) // 부적이 하나도 없는 경우 체크 -> 부적 새로 생성
						.OnComplete(() => StartCoroutine(DamageMonster(Player.Instance.GetCharmDamage())))); // HP 애니메이션, 몬스터 데미지
		sequence.Append(flyCharm.transform.DOLocalRotate(new Vector3(180f, -90f, -90f), 0.1f))
				.OnComplete(() => StartCoroutine(BurnAnimation(flyCharm)));


	}

	/// <summary>
	/// 부적 타는 효과 애니메이션
	/// </summary>
	IEnumerator BurnAnimation(GameObject flyCharm)
	{
		flyCharm.GetComponent<Animator>().speed = 2f;
		flyCharm.GetComponent<Animator>().SetTrigger("Burning");
		yield return new WaitForSeconds(0.8f);
		Destroy(flyCharm);
	}

	/// <summary>
	/// 부적 개수 체크 : 0개일 때 새로 생성시켜 줌 + 생성 애니메이션
	/// </summary>
	void CheckCharmZeroCount()
	{
		int charmCount = CharmList.Where((x) => x.isEnable).Count();
		if (charmCount <= 0)
		{
			Debug.Log("부적이 하나도 없다 -> 생성");
			StartCoroutine("ShowAllCharm");
		}
	}

	/// <summary>
	/// 몬스터 데미지 : HP 소모, 데미지 모션 
	/// </summary>
	IEnumerator DamageMonster(int damageValue)
	{
		// 변경 될 HP = 현재 HP - 데미지 HP 
		float hp = ghostInfoUI.ghostHPSlider.value - damageValue;

		if (hp <= 0)
			gameState = CharmGameState.Stop;

		// HP 애니메이션
		ghostInfoUI.ghostHPSlider.DOValue(hp, 0.3f);
		// 몬스터 데미지 애니메이션
		ghostManager.Animation(AnimationName.damage);
		// 데미지 텍스트 애니메이션
		// 데미지 팝업 텍스트 애니메이션
		//      GameObject popText = Instantiate(damagePopText, transform);
		//      popText.transform.localPosition = new Vector3(300f, 750f, 0f);
		//popText.GetComponentInChildren<Text>().text = damageValue.ToString();

		yield return new WaitForSeconds(0.3f);

		// 몬스터 HP = 0 : 게임 끝 
		if (ghostInfoUI.ghostHPSlider.value <= 0)
		{         
			// 타임스케일 느리게 효과 추가
			MasterAgent.Instance.timeManager.DoSlowMotion();
			ghostManager.Animation(AnimationName.death, true); // 몬스터 죽는 모션  
			ghostManager.transform.DOLocalMoveY(-35f, 1f);
			GameMode_UI.SetActive(false);

			yield return new WaitForSeconds(2f);
			LastAttack_UI.gameObject.SetActive(true);
		}
	}

	/// <summary>
	/// 포인터 움직임
	/// </summary>
	void MovePointer()
	{
		if (gameState != CharmGameState.Play)
			return;

		newCharmIndex = GetAbleIndexList();

		if(newCharmIndex == int.MinValue)
		{
			Debug.Log("newCharmIndex : " + newCharmIndex + " 이라서 멈춤");
			return;
		}

		// 이동거리 X 스피드(부적 하나 이동시 스피드) -> 일정한 속도로 움직이기 위해서
		float moveSpeed = Mathf.Abs(targetCharmIdx - newCharmIndex) * 0.4f;

		targetCharmIdx = newCharmIndex;

		pointer.DOLocalMoveX(CharmList[targetCharmIdx].xPos, moveSpeed)
			   .SetEase(Ease.Linear)
			   .OnComplete(() => MovePointer());

		// 목표 부적으로 이동
		newCharmIndex = int.MinValue; // 초기화      
	}

	/// <summary>
	/// 포인터 이동 목표 부적 설정
	/// </summary>
	private int GetAbleIndexList()
	{
		List<int> ableIdx = new List<int>();

		ableIdx = GetCharmIndex();

		if (ableIdx == null) // 가려는 방향에 부적이 없다 -> 방향 바꿔 다시 진행
		{
			ableIdx = GetCharmIndex();
		}

		if (ableIdx == null) // 방향을 바꿔도 부적이 없다 (=부적이 하나도 없다) -> 새 부적들 생성
		{
			CheckCharmZeroCount();
			ableIdx = GetCharmIndex();
		}

		if(ableIdx == null)
		{
			return int.MinValue;
		}
      
		if (ableIdx.Count > 1)
			ableIdx.Remove(targetCharmIdx); // 현재 위치는 제외시켜줘야 한다

		moveDirection = (moveDirection == MoveDirection.Left) ? MoveDirection.Right : MoveDirection.Left;
		int random = ableIdx[Random.Range(0, ableIdx.Count)];

		return random;
	}

	/// <summary>
	/// 부적 활성 여부와 이동 방향을 고려하여 목표 가능한 부적인덱스 배열을 리턴해준다
	/// </summary>
	private List<int> GetCharmIndex()
	{
		List<int> ableIdx = new List<int>();
		bool isEnableCharm = false;
		bool hasEnable = false;

		if (moveDirection == MoveDirection.Left)
		{
			for (int i = targetCharmIdx; i >= 0; i--)
			{
				if (CharmList[i].isEnable)
					hasEnable = true;

				if (!isEnableCharm && CharmList[i].isEnable)
				{
					isEnableCharm = true;
				}

				if (isEnableCharm) // 활성인 부적의 왼쪽 부적은 모두 포인터가 지나갈 수 있음
					ableIdx.Add(i);
			}
		}
		else if (moveDirection == MoveDirection.Right)
		{
			for (int i = targetCharmIdx; i < CharmList.Length; i++)
			{
				if (CharmList[i].isEnable)
					hasEnable = true;

				if (!isEnableCharm && CharmList[i].isEnable)
				{
					isEnableCharm = true;
				}

				if (isEnableCharm) // 활성인 부적의 왼쪽 부적은 모두 포인터가 지나갈 수 있음 // 6은 반드시 포함 (마지막 부적)
					ableIdx.Add(i);
			}
		}

		// 가려는 방향에 부적이 하나도 활성되어 있지 않을 때 -> 갈 필요가 없다 -> 방향 바꾸기
		if (!hasEnable)
		{
			moveDirection = (moveDirection == MoveDirection.Left) ? MoveDirection.Right : MoveDirection.Left;
			return null;
		}

		return ableIdx;
	}

	/// <summary>
	/// 마지막 공격이 끝나고 결과 띄워주기
	/// </summary>
	public void ShowResult()
	{
		if(Result_UI.SD_Graphic != null)
		{
			Destroy(Result_UI.SD_Graphic.gameObject);
			Result_UI.SD_Graphic = null;
		}
        
		GameObject graphicObject = new GameObject();
		graphicObject.transform.SetParent(Result_UI.transform);
		graphicObject.transform.localPosition = new Vector3(0f, 660f, 0f);
		graphicObject.transform.localScale = Vector3.one * 0.5f;
		graphicObject.transform.localRotation = Quaternion.identity;
		// SkeletonGraphic 재활용 했을 때 이미지 깨짐 문제 해결을 위해 새로 만드는 방법으로 수정
		Result_UI.SD_Graphic = graphicObject.AddComponent<SkeletonGraphic>();
      
		ghostManager.gameObject.SetActive(false);
		Result_UI.monsterNameText.text = GameManager.instance.ghostManager.ghost.name;

		Result_UI.SD_Graphic.skeletonDataAsset = GameManager.instance.ghostManager.ghost.SD_DataAsset;
		Result_UI.SD_Graphic.startingAnimation = "idle";
		Result_UI.SD_Graphic.startingLoop = true;
		Result_UI.gameObject.SetActive(true);
	}

}
