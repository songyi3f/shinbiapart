﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Assertions;

/// <summary>
/// 시간 관련 매니저 : 슬로우모션, 서버에서 현재시간 받아오기, 충전 시스템
/// </summary>
public class TimeManager : MonoBehaviour {

	#region Variables
    // 슬로우 모션
	public float slowdownFactor = 0.008f;
	public float slowdownLength = 2f;
	bool isSlowMotion = false;   
    [Serializable]
    public class TimeInfo { public string currentDateTime; }

	// 퍼즐 하트 충전 타이머
	[HideInInspector] public PuzzleLiveTimer liveTimer = new PuzzleLiveTimer();
	// 도깨비 엿 충전 타이머
	[HideInInspector] public CandyTimer candyTimer = new CandyTimer();
	// 기본 부적 충전 타이머
	[HideInInspector] public CharmTimer charmTimer = new CharmTimer();
	#endregion

	private void Start()
	{
		liveTimer = MasterAgent.Instance.GetComponent<PuzzleLiveTimer>();
		candyTimer = MasterAgent.Instance.GetComponent<CandyTimer>();
		charmTimer = MasterAgent.Instance.GetComponent<CharmTimer>();

		Assert.IsNotNull(candyTimer);
		Assert.IsNotNull(charmTimer);
	}

	private void Update()
    {      
        if (isSlowMotion)
        {
            Time.timeScale += (1f / slowdownLength) * Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);

            if (Time.timeScale >= 1f)
                isSlowMotion = false;
        }      
    }

    /// <summary>
    /// 도깨비 엿 & 기본 부적 충전 타이머 스타트
    /// </summary>
    public void StartTimer()
	{
		candyTimer.StartTimer();
        charmTimer.StartTimer();
		liveTimer.StartTimer();
	}

    /// <summary>
    /// 도깨비 엿 & 기본 부적 충전 타이머 스탑
    /// </summary>
    public void StopTimer()
	{
		candyTimer.StopCountdown();
		charmTimer.StopCountdown();
		liveTimer.StopCountdown();
	}
      
    /// <summary>
    /// 슬로우모션 실행
    /// </summary>
	public void DoSlowMotion()
	{
		Time.timeScale = slowdownFactor;
		Time.fixedDeltaTime = Time.timeScale * 0.02f;
		isSlowMotion = true;
	}

    /// <summary>
	/// 통신 연결 여부에 따라 서버 시간 가져오기
    /// </summary>
	public IEnumerator GetServerTime(Action<DateTime> action)
    {
		if(Application.internetReachability == NetworkReachability.NotReachable)
		{
			Debug.Log("통신 연결 안될 때 : DateTime.now 사용");
			// 통신 연결 안될 때 : DateTime.now 사용
			action(DateTime.Now);
			yield break;
		}
		else
		{
			Debug.Log("통신 연결이 됐을 때 : 서버 시간 사용");
			// 통신 연결이 됐을 때 : 서버 시간 사용
			WWW www = new WWW("http://worldclockapi.com/api/json/utc/now");
            yield return www;

			var data = JsonUtility.FromJson<TimeManager.TimeInfo>(www.text);         
			action(DateTime.Parse(data.currentDateTime));
		}
    }


}
