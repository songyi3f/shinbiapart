﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoadManager : MonoBehaviour{

	public static SaveLoadManager Instance = null;

	private void Awake()
	{
		if(Instance == null)
		{
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
		else if(Instance != this)
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		LoadData();
		StartCoroutine(Loop());
		// 타임 매니저 초기화 시켜줌 -> 데이터 불러온 후 순서 때문에
		MasterAgent.Instance.timeManager.StartTimer();
	}

    /// <summary>
    /// 5초마다 데이터 저장
    /// </summary>
	IEnumerator Loop()
	{
		while(true)
		{
			yield return new WaitForSeconds(5f);
			Debug.Log("SAVE");
			Save();
		}
	}

    /// <summary>
    /// 데이터 저장 전 바인딩
    /// </summary>
    public void Save()
	{
		PlayerData data = new PlayerData();
		data.playerInfo = new Info();
		data.playerInfo = Player.Instance.Info;

		SaveData(data);
	}

    /// <summary>
    /// 데이터 저장
    /// </summary>
	void SaveData(PlayerData data)
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream stream = new FileStream(Application.persistentDataPath + "/shinbiApartmentSave21.sav", FileMode.Create);
      
		binaryFormatter.Serialize(stream, data);
		stream.Close();
	} 

    /// <summary>
    /// 데이터 불러오기
    /// </summary>
	void LoadData()
	{
		if(File.Exists(Application.persistentDataPath + "/shinbiApartmentSave21.sav"))
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			FileStream stream = new FileStream(Application.persistentDataPath + "/shinbiApartmentSave21.sav", FileMode.Open);

			PlayerData data = binaryFormatter.Deserialize(stream) as PlayerData;

			stream.Close();
			SetData(data);
			Debug.Log("데이터불러옴");
		}
		else
		{
			Debug.Log("데이터 없어서 못불러 옴");
		}
           
	}
   
    /// <summary>
    /// 불러온 데이터 세팅
    /// </summary>
    void SetData(PlayerData data)
	{
		Player.Instance.Info = data.playerInfo;
	}
}

[Serializable]
class PlayerData
{   
	public Info playerInfo;   
}