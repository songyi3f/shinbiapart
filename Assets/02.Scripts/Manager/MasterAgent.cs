﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TF.JsonIO;

public class MasterAgent : MonoBehaviour
{

	#region Variables
	public static MasterAgent Instance = null;

	// Data Loader
	public JsonConfigLoader jsonConfigLoader;

	public TimeManager timeManager;
	#endregion

	private void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);

		jsonConfigLoader = new JsonConfigLoader ();
		timeManager = GetComponent<TimeManager>();
		CheckConfig ();
		//jsonConfigLoader.StartLoadingFromWWW(this, this.configs);
	}
    
	public GameManager gameManager
	{
		get
		{
			if (SceneManager.GetActiveScene().name == "Game" && Camera.main != null && Camera.main.GetComponent<GameManager>() != null)
            {
                return Camera.main.GetComponent<GameManager>();
            }
			else
			{
				return null;
			}
		}
	}


	private void CheckConfig()
	{
		jsonConfigLoader.StartLoading ();
		Debug.Log ("Count : " + jsonConfigLoader.configs.lionInfos.Count);
		Debug.Log ("Count : " + jsonConfigLoader.configs.lionItems.Count);
		Debug.Log ("Count : " + jsonConfigLoader.configs.lion_Active_Skills.Count);
		Debug.Log ("Count : " + jsonConfigLoader.configs.lion_Passive_Skills.Count);

	}
}
