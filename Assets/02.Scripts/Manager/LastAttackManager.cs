﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LastAttackManager : MonoBehaviour {

	#region Variables
	public CharmGameManager charmGameManager;
	public CharmGameState gameState = CharmGameState.Idle;
	public GameObject lastAttack_UI;
   
    public Transform pointer;
	public Transform getItemArea;
	public Transform normalArea;
	public GameObject charmPrefab;

	// 아이템 부적선택 UI
	public ItemCharmUI itemCharm_UI;
	public FreeItemCharmUI freeCharm_UI;
    #endregion


    private void OnEnable()
    {
		// TODO : 우선 리필 버튼은 비활성함 : 아이템 획득 부적 여부에 따라 활성
		lastAttack_UI.SetActive(true);
        StartCoroutine("StartAnimation");

		itemCharm_UI.SetItemCharmUI();

    }

    private void OnDisable()
    {
        pointer.DOKill();
		gameState = CharmGameState.Idle;
    }

    /// <summary>
    /// 시작시 애니메이션 
    /// </summary>
    IEnumerator StartAnimation()
    {
        // 포인터는 왼쪽에서 시작해 오른쪽으로 이동
        pointer.localPosition = new Vector3(-600f, pointer.localPosition.y, 0f);
        yield return new WaitForSeconds(0.5f);

        // 포인터 제자리 도는 모션
        pointer.gameObject.SetActive(true);
        pointer.GetComponent<Animator>().speed = 5f;
        pointer.GetComponent<Animator>().SetTrigger("Play");
        yield return new WaitForSeconds(1f);

        // 포인터 이동 시작
		pointer.DOLocalMoveX(600f, 2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
		gameState = CharmGameState.Play;
    }

    /// <summary>
    /// 포인터 멈췄을 때 이벤트
    /// </summary>
    public void PausePointer()
    {      
		if (gameState != CharmGameState.Play)
            return;

		float posX = pointer.localPosition.x;

		if(posX < 0)
		{
			if (itemCharm_UI.isActive && Player.Instance.Info.itemCharm > 0) 
			{
				Player.Instance.Info.itemCharm -= 1;
			}
			else
			{
				return;
			}
		}
         
		gameState = CharmGameState.Stop;
		lastAttack_UI.SetActive(false);

		// TODO : 트리거 체크 -> 부적 종류에 따라 날아가는 부적도 다름

		GhostManager ghostManager = GameManager.instance.ghostManager;

		// 부적 날아가는 애니메이션 -> 날아갈 부적은 새로 생성해준다
		GameObject flyCharm = Instantiate(charmPrefab, transform);
		flyCharm.transform.localPosition = new Vector3(0f, -1800f, 0f);
		flyCharm.transform.localScale = Vector3.one * 30f;
		flyCharm.transform.localRotation = Quaternion.Euler(-90f, 90f, -90f);
      
		Sequence sequence = DOTween.Sequence();
		sequence.Append(flyCharm.transform.DOLocalMoveY(-872f, 0.5f)); // 화면 밑에서 큰 부적이 올라옴 
		sequence.PrependInterval(0.5f);
		sequence.Append(flyCharm.transform.DOLookAt(ghostManager.ghostInfo.charmTransform.position, 0.1f));
		sequence.Join(flyCharm.transform.DOMove(ghostManager.ghostInfo.charmTransform.position, 0.2f)).OnComplete(()=> 
		{
			ghostManager.ghostInfo.charmTransform.gameObject.SetActive(true);
			ghostManager.ghostInfo.charmTransform.GetComponent<Animator>().SetTrigger("Burning");
			Destroy(flyCharm);
			StartCoroutine(ShowResult());
		});
    }   

    /// <summary>
    /// 2초 후 결과 띄워주기
    /// </summary>
	IEnumerator ShowResult()
	{
		yield return new WaitForSeconds(2f);
		charmGameManager.ShowResult();
	}
}
