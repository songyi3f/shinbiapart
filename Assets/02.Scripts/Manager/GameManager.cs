﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using System;
using GameVanilla.Core;

public class GameManager : BaseScene
{   
	#region Variables
	public static GameManager instance = null;
	public enum GameModeState { AR_MODE, VR_MODE }
	public GameModeState gameState = GameModeState.AR_MODE;

	public GyroManager gyroManager;
	public CharmGameManager charmGM;

	// 귀신 오브젝트
	public GhostManager ghostManager;

	// UI 조준 & 방향 표시
	public GameObject TargetTriangle;
	public SpriteRenderer AimSpriteRenderer;
	private bool isShowTargetTriangle;
	private bool isShot;

	// UI 화면 효과 
	public GameObject ShotEffect;
	public GameObject DamageEffect;
	public GameObject FadeEffect;

	// UI 오브젝트
	public GameObject AR_Canvas;
	public Transform VR_Area;
	public GameObject AR_UI;
	public GameObject VR_UI;
	public GameObject takePhotoUI;
	public GameObject takePhotoButton;
	public GameObject candyTutorialButton;
   
	// 부적 스크립터블 오브젝트
	public Charm[] charmList;

	// 카메라 뒤쪽 귀신 포지션
	public Transform ghostSpawn;

   
	#endregion

	private void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
	}

	private void Start()
	{
		gyroManager = GetComponent<GyroManager>();    
		gyroManager.isEnable = true; 

		gameState = GameModeState.AR_MODE;
		AudioManager.instance.Stop("BGM");
		AimSpriteRenderer.color = Color.black;
		AimSpriteRenderer.gameObject.SetActive(false);
		AR_Canvas.SetActive(true);
		VR_Area.gameObject.SetActive(false);
		AR_UI.SetActive(true);
		VR_UI.SetActive(false);
		ShotEffect.SetActive(false);
		DamageEffect.SetActive(false);
		FadeEffect.SetActive(false);
		TargetTriangle.SetActive(false);
		takePhotoUI.SetActive(false);
		takePhotoButton.SetActive(true);
		isShot = false;
		isShowTargetTriangle = false;
		candyTutorialButton.SetActive(true);
		ghostManager.gameObject.SetActive(false);

		UseCandy();
	}


	private void Update()
	{
		if (isShot)
			return;

		if (!isShowTargetTriangle)
			return;

		Vector3 viewPortPoint = Camera.main.WorldToViewportPoint(ghostManager.transform.position);

		// 스크린 좌표계 (0,0) ~ (1,1) 안에 오브젝트가 있는지 검사 : 몬스터가 화면에서 보이면 위치표시 꺼줌  
		if ((-0.1f <= viewPortPoint.x && viewPortPoint.x <= 1.1f) &&
			(-0.1f <= viewPortPoint.y && viewPortPoint.y <= 1.1f) &&
			(0 <= viewPortPoint.z))
		{
			TargetTriangle.SetActive(false);

			Debug.DrawRay(TargetTriangle.transform.position, TargetTriangle.transform.forward * 1000, Color.blue);
			RaycastHit hit;
			if (Physics.Raycast(TargetTriangle.transform.position, TargetTriangle.transform.forward, out hit))
			{
				if (hit.collider.CompareTag("Monster"))
				{
					ghostManager.StopMonsterMove(); // 몬스터 움직임 멈춤
					//monster.transform.SetParent(Camera.main.transform);
					AimSpriteRenderer.color = Color.white;

					//if(gameState == GameState.AR_MODE)
					//takePhotoUI.SetActive(true);
					//else 
					if (gameState == GameModeState.VR_MODE)
					{
						ghostManager.transform.SetParent(Camera.main.transform);
						StartCoroutine("StartCharmGame");
					}               
				}
			}
		}
		else
		{
			//if(takePhotoUI.activeInHierarchy)
			//    takePhotoUI.SetActive(false);

			AimSpriteRenderer.color = Color.black;

			Vector3 aimVewPortPoint = Camera.main.WorldToViewportPoint(TargetTriangle.transform.position);
			Vector2 direction = viewPortPoint - aimVewPortPoint;
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			Quaternion rotaion = Quaternion.AngleAxis(angle, Vector3.forward);
			TargetTriangle.transform.localRotation = Quaternion.Slerp(TargetTriangle.transform.localRotation, rotaion, 20f * Time.deltaTime);
		}
	}

    /// <summary>
    /// 카메라 뒤쪽에 귀신이 생성되도록 포지션 리턴
    /// </summary>
	public Vector3 SpawnPosition()
    {
		Vector3 center = ghostSpawn.localPosition;
		Vector3 size = ghostSpawn.localScale;
		Vector3 pos = center + new Vector3(UnityEngine.Random.Range(-size.x / 2, size.x / 2),
                                           UnityEngine.Random.Range(-size.y / 2, size.y / 2),
                                           UnityEngine.Random.Range(-size.z / 2, size.z / 2));

        return pos;
    }

	private bool isFirst = true;
    /// <summary>
	/// 앱을 홈키 등으로 비활성 시켰을 때 이벤트 함
	/// pause  (true : 홈키로 내렸을 때, false : 돌아왔을 때 , 첫 시작 시)
	/// </summary>
	private void OnApplicationPause(bool pause)
	{
		if (pause)
			return;

		if(isFirst)
		{
			// 처음 앱을 실행시키고 들어옴
			Debug.Log("==================처음 앱을 실행시키고 들어옴");
			isFirst = false;
		}
     
		// 도깨비 엿 충전 & 부적 충전 계산

		if(pause)
		{
			// 내렸을 때
			Debug.Log("===================== 홈키로 내렸을 때 ");
			MasterAgent.Instance.timeManager.StopTimer();
		}
		else
		{
			// 돌아왔을 때
			Debug.Log("===================== 홈키로 돌아왔을 때 ");
			MasterAgent.Instance.timeManager.StartTimer();
		}      
	}
   
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Monster"))
		{
			isShowTargetTriangle = true;
			TargetTriangle.SetActive(true);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("Monster"))
		{
			isShowTargetTriangle = false;
			TargetTriangle.SetActive(false);
		}
	}

	/// <summary>
	/// 게임 스타트 : 엿 소비
	/// </summary>
	public void UseCandy()
	{
		StartCoroutine(UseCandyCheck());
	}

	/// <summary>
	/// 게임 스타트 : 엿 소비
	/// </summary>
	IEnumerator UseCandyCheck()
	{
		yield return new WaitForSeconds(0.1f);
        StartCoroutine(StartGame());
	}
   
	/// <summary>
	/// 스타트 게임 : 몬스터 활성
	/// </summary>
	IEnumerator StartGame()
	{
		yield return new WaitForSeconds(3f);
		ghostManager.SetNewMonster();
		MoveMonster();
		ghostManager.transform.localScale = Vector3.one;      
		AimSpriteRenderer.gameObject.SetActive(true);      
	}

	/// <summary>
	/// 사진 찍기 카메라 버튼 이벤트
	/// </summary>
	public void ShotButtonEvent()
	{
		StartCoroutine(ShotButton());
	}

	/// <summary>
	/// 사진 찍기
	/// </summary>
	public IEnumerator ShotButton()
	{
		if (isShot)
			yield break;

		ShotEffect.SetActive(true);
		AudioManager.instance.Play("Shutter");
		takePhotoButton.SetActive(false);
		takePhotoUI.SetActive(false);
		AR_UI.SetActive(false);

		RaycastHit hit;
		if (Physics.Raycast(TargetTriangle.transform.position, TargetTriangle.transform.forward, out hit))
		{
			if (hit.collider.tag == "Monster")
			{
				ghostManager.transform.SetParent(Camera.main.transform);
				isShot = true;
				StartCoroutine("TurnOnVR_Area");
				yield break;
			}

		}
        
		ghostManager.StartMonsterMove();
		yield return new WaitForSeconds(1f);
		AimSpriteRenderer.color = Color.black;
		AR_UI.SetActive(true);
		yield return new WaitForSeconds(1f);
		ShotEffect.SetActive(false);
		takePhotoButton.SetActive(true);
	}

	/// <summary>
	/// 사진찍은 후 귀신 다가오는 애니메이션 & VR_Mode 시작
	/// </summary>
	IEnumerator TurnOnVR_Area()
	{
		// 찰칵 소리 이후 몬스터 데미지 애니메이션 재생 
		AudioManager.instance.Play("SV01_Damage");
		ghostManager.Animation(AnimationName.damage, false);
		yield return new WaitForSeconds(0.5f);

		// 자이로센서에 따라 몬스터 움직임 방지
		gyroManager.isEnable = false;

		// 귀신이 앞으로 온다
		GameObject newMonster = Instantiate(ghostManager.ghost.monsterPrefab, ghostManager.transform);            
		newMonster.transform.localScale = ghostManager.ghostGO.transform.localScale;
		newMonster.transform.localPosition = ghostManager.ghostGO.transform.localPosition;
		newMonster.transform.localRotation = ghostManager.ghostGO.transform.localRotation;
		newMonster.transform.SetParent(Camera.main.transform);
		ghostManager.gameObject.SetActive(false);
      
		newMonster.transform.DOMove(AR_UI.transform.position, 1f)
                  .SetEase(Ease.OutBack);

		newMonster.GetComponent<SkeletonAnimation>().AnimationName = AnimationName.attack.ToString();
		AudioManager.instance.Play("SV01_StartSpeech");
		yield return new WaitForSeconds(0.3f);

		DamageEffect.SetActive(true);

		yield return new WaitForSeconds(3f);
		ShotEffect.SetActive(false);
		DamageEffect.SetActive(false);
		FadeEffect.SetActive(true);

		Destroy(newMonster);

		gameState = GameModeState.VR_MODE;
		AimSpriteRenderer.color = Color.black;
		AR_Canvas.SetActive(false);
		VR_Area.gameObject.SetActive(true);
		AudioManager.instance.Play("BGM");
		gyroManager.isEnable = true; 
		yield return new WaitForSeconds(1f);

		StartCoroutine(StartVRMode());
	}

	/// <summary>
	/// VR모드 시작 : 귀신 세팅 
	/// </summary>
	public IEnumerator StartVRMode(float waitTime = 0)
	{
		MoveMonster();
		VR_UI.SetActive(false);
		AR_UI.SetActive(true);

		yield return new WaitForSeconds(waitTime);
		isShot = false;
		isShowTargetTriangle = true;
	}

	/// <summary>
	/// 부적 게임을 시작한다
	/// </summary>
	IEnumerator StartCharmGame()
	{
		ghostManager.transform.DOLocalMove(new Vector3(0f, ghostManager.ghost.VR_Mode_Pos.y, ghostManager.transform.localPosition.z), 0.3f);
		ghostManager.transform.DOScale(ghostManager.ghost.VR_Mode_Scale, 0.3f);

		yield return new WaitForSeconds(1f);

		//gyroManager.isEnable = false; 
		AR_UI.SetActive(false);
		VR_UI.SetActive(true);
		ghostManager.StartSound();
	}
    
	/// <summary>
	/// 잡혀있는 귀신을 놓아줌 : 부모자식 상태 해제
	/// </summary>
	void MoveMonster()
	{
		ghostManager.transform.SetParent(null);
		ghostManager.ghostInfo.charmTransform.gameObject.SetActive(false);
		ghostManager.gameObject.SetActive(true);
		ghostManager.StartMonsterMove();
		ghostManager.Animation(AnimationName.idle);
		ghostManager.StopSound();
	}

	/// <summary>
	/// AR 모드를 시작한다
	/// </summary>
	public void StartARMode()
	{
		Start();
	}
}
