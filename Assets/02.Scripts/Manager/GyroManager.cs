﻿using UnityEngine;

public class GyroManager : MonoBehaviour {

	#region Variables
	public bool isEnable = true;
    public bool gyroEnabled = false;
    private Gyroscope gyro;   
	private GameObject cameraContainer;
    private Quaternion rot;
 	#endregion

	void Start () 
	{
        // Make new GameObject and it has set parent of Main Camera.
        cameraContainer = new GameObject("@CameraContainer");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);

        gyroEnabled = EnableGyro();
	}

    private void FixedUpdate()
    {
		if (isEnable && gyroEnabled && gyro != null)
        {
            transform.localRotation = gyro.attitude * rot;
        }
    }

    /// <summary>
    /// Enables the gyro.
    /// </summary>
    bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            cameraContainer.transform.rotation = Quaternion.Euler(90f, 90f, 0f);
            rot = new Quaternion(0f, 0f, 1f, 0f);

            return true;
        }      
        return false;
    }

}
