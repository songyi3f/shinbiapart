﻿using System;
using GameVanilla.Core;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;

public class LobbyManager : BaseScene {

	#region Variables
    // 도깨비엿 충전 
	public UnityEngine.GameObject candyShopPopup;
	public UnityEngine.GameObject candyRemainTime;
	public Text candyRemainTimeText;
 	public Text candyCountText;

	// 퍼즐 하트 충전
	public UnityEngine.GameObject liveShopPopup;
	public UnityEngine.GameObject liveRemainTime;
	public Text liveRemainTimeText;
	public Text liveCountText;
	#endregion

	public void OnEnable()
	{
		MasterAgent.Instance.timeManager.liveTimer.Subscribe(OnLiveCountdownUpdated, OnLiveCountdownFinished);
		MasterAgent.Instance.timeManager.candyTimer.Subscribe(OnCandyCountdownUpdated, OnCandyCountdownFinished);
		candyCountText.text = "X " + Player.Instance.Info.candy.ToString();
	}

	private void OnDestroy()
    {
		MasterAgent.Instance.timeManager.liveTimer.Unsubscribe(OnLiveCountdownUpdated, OnLiveCountdownFinished);
        MasterAgent.Instance.timeManager.candyTimer.Unsubscribe(OnCandyCountdownUpdated, OnCandyCountdownFinished);
    }

    /// <summary>
    /// 도깨비엿 타이머 업데이트 이벤트
    /// </summary>
	void OnCandyCountdownUpdated(TimeSpan timeSpan, int candy)
    {
		candyRemainTime.SetActive(true);
		candyRemainTimeText.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
		candyCountText.text = "X " + candy.ToString();
    }

    /// <summary>
    /// 도깨비엿 타이머 종료 이벤트
    /// </summary>
    void OnCandyCountdownFinished()
    {
		candyRemainTime.SetActive(false);
		candyCountText.text = "X " + Player.Instance.Info.candy.ToString();
    }

    /// <summary>
    /// 퍼즐 하트 타이머 업데이트 이벤트
    /// </summary>
	void OnLiveCountdownUpdated(System.TimeSpan timeSpan, int live)
    {
		liveRemainTime.SetActive(true);
		liveRemainTimeText.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
		liveCountText.text = "X " + Player.Instance.Info.puzzleLive.ToString();
    }

    /// <summary>
    /// 퍼즐 하트 타이머 종료 이벤트
    /// </summary>
    void OnLiveCountdownFinished()
    {
		liveRemainTime.SetActive(false);
		liveCountText.text = "X " + Player.Instance.Info.puzzleLive.ToString();
    }

	public void Test()
	{
		OpenPopup<GhostSchoolPopup>("Popups/GhostSchoolPopup", popup =>
        {
			popup.gameObject.SetActive(true);
        });
	}

    /// <summary>
    /// 귀신 찾기 버튼 이벤트    
	/// </summary>
    public void GoAR_Mode()
	{  
		if(Player.Instance.Info.candy <= 0)
		{
			candyShopPopup.SetActive(true);
			return;
		}

		StartCoroutine(MoveARGameScene());
	}

	/// <summary>
    /// 귀신 찾기 씬 이동 코루틴   
    /// </summary>
	IEnumerator MoveARGameScene()
	{
		Player.Instance.RefreshCandy(-1);
        candyCountText.text = "X " + Player.Instance.Info.candy;

		yield return new UnityEngine.WaitForSeconds(0.5f);

		Transition.LoadLevel("Game", 1f, UnityEngine.Color.black);
	}

    /// <summary>
    /// 퍼즐 게임 버튼 이벤트
    /// </summary>
    public void GoPuzzle_Mode()
	{
		if(Player.Instance.Info.puzzleLive <= 0)
		{
			liveShopPopup.SetActive(true);
			return;
		}

		StartCoroutine(MovePuzzleGameScene());
	}

    /// <summary>
    /// 퍼즐 씬 이동 코루틴
    /// </summary>
    /// <returns>The puzzle game scene.</returns>
	IEnumerator MovePuzzleGameScene()
	{
		Player.Instance.RefreshPuzzleLive(-1);
		liveCountText.text = "X " + Player.Instance.Info.puzzleLive;
		yield return new UnityEngine.WaitForSeconds(0.5f);
		Transition.LoadLevel("GameScene", 1f, UnityEngine.Color.black);
	}

}
