﻿using UnityEngine;
using System;

public class AudioManager : MonoBehaviour {

	#region Variables
	public Sound[] sounds;
	public static AudioManager instance;
	#endregion

	void Awake () 
	{
		if (instance == null)
			instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);
        
		foreach(Sound sound in sounds)
		{
			sound.source = gameObject.AddComponent<AudioSource>();
			sound.source.clip = sound.clip;
			sound.source.loop = sound.loop;
		}
	}

    /// <summary>
    /// 사운드 재생
    /// </summary>
    public void Play (string name)
	{
		Sound sound = Array.Find(sounds, Sound => Sound.name == name);
		sound.source.Play();
	}

    /// <summary>
    /// 사운드 멈춤
    /// </summary>
    public void Stop(string name)
	{
		Sound sound = Array.Find(sounds, Sound => Sound.name == name);
		if (sound.source.isPlaying)
			sound.source.Stop();
	}

}
