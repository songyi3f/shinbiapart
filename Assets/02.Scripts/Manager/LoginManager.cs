﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;


public class LoginManager : MonoBehaviour
{

	public Text debugText;

	void Start()
	{
		
#if UNITY_ANDROID

		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                                                                              .EnableSavedGames()
                                                                              .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
      
#elif UNITY_IOS

        
#endif

	}

    public void SignIn()
	{
		Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                // 로그인 성공 처리
				debugText.text = "로그인 성공 : " + Social.localUser.id;            
            }
            else
            {
                // 로그인 실패 처리            
                debugText.text = "로그인 실패";
            }
        });
	}
   
}
