﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class SceneLoader : MonoBehaviour {

	#region Variables
	public static SceneLoader Instance = null;

	public GameObject loadingScreen;
	[HideInInspector] public Slider slider;
	[HideInInspector] public Text text;
	#endregion

	private void Awake()
    {
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void LoadScene(string sceneName)
	{
		StartCoroutine(LoadAsynchronously(sceneName));
	}

	IEnumerator LoadAsynchronously(string sceneName)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

		// 로딩 스크린 
		GameObject Pop = Instantiate(loadingScreen);
		slider = Pop.GetComponent<LoadingPopup>().slider;
		text = Pop.GetComponent<LoadingPopup>().text;

		while(!operation.isDone)
		{
			float progress = Mathf.Clamp01(operation.progress / .9f);
			slider.value = progress;
			text.text = progress * 100f + "%";
			Debug.Log(text.text);
			yield return null;
		}
	}
}
