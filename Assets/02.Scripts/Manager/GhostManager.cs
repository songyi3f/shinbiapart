﻿using UnityEngine;
using System.Collections;

public class GhostManager : MonoBehaviour {

	#region Variables
    // 귀신 스트립터블 오브젝트 리스트
    public Ghost[] GhostList;
    // 현재 귀신 스크립터블 오브젝트
    public Ghost ghost = null;
    public GameObject ghostGO = null;
    public GhostInfo ghostInfo = null;

    // Move & LookAt
    public bool isMove;
    public float spawnRange;
    public float spawnTime = 5f;
    #endregion

    void FixedUpdate()
    {
        if (Camera.main != null)
            transform.LookAt(Camera.main.transform); // 항상 귀신이 카메라를 향한다      
    }

    /// <summary>
    /// 새 귀신을 랜덤으로 세팅한다
    /// </summary>
    public void SetNewMonster()
    {
		if (ghostGO != null)
			Destroy(ghostGO);

		ghost = GhostList[Random.Range(0, GhostList.Length)];
		ghostGO = Instantiate(ghost.monsterPrefab, transform);
		ghostGO.transform.localRotation = new Quaternion(0f, 180f, 0f, 0f);
		ghostInfo = ghostGO.GetComponent<GhostInfo>();
    }

    /// <summary>
    /// 몬스터 이동 스타트
    /// </summary>
    public void StartMonsterMove()
    {
        isMove = true;
        CancelInvoke("MoveAnimation");
        InvokeRepeating("MoveAnimation", 0f, spawnTime);
    }

    /// <summary>
    /// 몬스터 이동 멈춤
    /// </summary>
    public void StopMonsterMove()
    {
        isMove = false;
        StopCoroutine("MovePos");
    }


    /// <summary>
    /// 랜덤 위치 뽑기 : 반지름 160f 반원 안에서 위치를 랜덤 뽑기   
    /// </summary>
    void MoveAnimation()
    {
        if (!isMove)
            return;

        while (true)
        {
            transform.SetParent(GameManager.instance.transform);
            transform.localPosition = GameManager.instance.SpawnPosition();
            transform.SetParent(null);
            break;
            //Vector3 pos = Random.insideUnitCircle * 160f;
            //Vector3 newPos = new Vector3(pos.x, Random.Range(0f, 70f), pos.y);

            //if (Vector3.Distance(pos, Vector3.zero) > 130f)
            //{
            //    transform.localPosition = newPos;
            //    break;
            //}
        }
    }

    /// <summary>
    /// 귀신 소리 스타트
    /// </summary>
    public void StartSound()
    {
        StartCoroutine("MonsterSound");
    }

    /// <summary>
    /// 귀신 소리 멈추기
    /// </summary>
    public void StopSound()
    {
        StopCoroutine("MonsterSound");
    }

    /// <summary>
    /// 귀신 사운드 반복 재생
    /// </summary>
    public IEnumerator MonsterSound()
    {
        while (true)
        {
            string clipName = "SV01_Speech_" + Random.Range(1, 5).ToString();
            AudioManager.instance.Play(clipName);
            yield return new WaitForSeconds(Random.Range(3f, 8f));
        }
    }

    /// <summary>
    /// 귀신 애니메이션 재생
    /// </summary>
    public void Animation(AnimationName name, bool isSound = true)
    {
        bool isLoop = false;
        switch (name)
        {
            case AnimationName.idle:
                isLoop = true;
                break;

            case AnimationName.attack:
                isLoop = false;
                break;

            case AnimationName.damage:
                isLoop = false;
                if (isSound)
                    AudioManager.instance.Play("SV01_Damage");
                break;

            case AnimationName.death:
                isLoop = false;
                if (isSound)
                    AudioManager.instance.Play("SV01_Dead");

                StopSound();
                break;
        }

		ghostInfo.spainAnimation.state.SetAnimation(0, name.ToString(), isLoop);

        if (!isLoop && name != AnimationName.death) // 한번만 실행하는 애니메이션은 재생 후 Idle로 돌아간다
			ghostInfo.spainAnimation.state.AddAnimation(0, AnimationName.idle.ToString(), true, 0f);
    }


}
