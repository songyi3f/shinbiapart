﻿using UnityEngine;
using UnityEngine.UI;

public class CandyShopItem : MonoBehaviour {

	#region Variables
	public int count;
	public Text titleText;
	public Text countText;
	public Text priceText;
	#endregion

	public void Start()
	{
		countText.text = "X" + count.ToString();
		titleText.text = "도깨비 엿 " + count.ToString() + "개";
	}

	public void GetButtonEvent()
	{
		Player.Instance.RefreshCandy(count);
		MasterAgent.Instance.timeManager.candyTimer.AddCandy();
	}
}
