﻿using UnityEngine;
using System;
using System.Collections;

public class PuzzleLiveTimer : TimeSystem {

    public void StartTimer()
	{
		StopCoroutine("RunningCountdown");
        StartCoroutine("RunningCountdown");

		var live = Player.Instance.Info.puzzleLive;
		var maxLive = Player.Instance.Info.maxPuzzleLive;
		var timeToNextLive = Player.Instance.Info.timeToNextPuzzleLive;

		if(live < maxLive && !string.IsNullOrEmpty(Player.Instance.Info.nextPuzzleLiveTime))
		{
			var temp = Convert.ToInt64(Player.Instance.Info.nextPuzzleLiveTime);
			var prevNextPuzzleLiveTime = DateTime.FromBinary(temp);
            
			TimeSpan remainingTime;
			var now = DateTime.Now;

			if(prevNextPuzzleLiveTime > now)
			{
				remainingTime = prevNextPuzzleLiveTime - now;

				if (live < maxLive)
					StartCountdown((int)remainingTime.TotalSeconds);
			}
            else
			{
				remainingTime = now - prevNextPuzzleLiveTime;
				var liveToGive = ((int)remainingTime.TotalSeconds / timeToNextLive) + 1;
				live = live + liveToGive;

				if (live > maxLive)
					live = maxLive;

				Player.Instance.Info.puzzleLive = live;

				if (live < maxLive)
					StartCountdown(timeToNextLive - ((int)remainingTime.TotalSeconds % timeToNextLive));
				else
					StopCountdown();
			}
		}
	}

	IEnumerator RunningCountdown()
	{
		while(true)
		{
			yield return new WaitUntil(() => runningCountdown);

			timeSpan = timeSpan.Subtract(TimeSpan.FromSeconds(1));
			SetTimeToNextLive((int)timeSpan.TotalSeconds);

			if (onCountdownUpdated != null)
				onCountdownUpdated(timeSpan, Player.Instance.Info.puzzleLive);

			if((int)timeSpan.TotalSeconds == 0)
			{
				StopCountdown();
				Player.Instance.RefreshPuzzleLive(1);
			}

			yield return new WaitForSeconds(1);
		}
	}

    public void AddLive()
	{
		var maxLive = Player.Instance.Info.maxPuzzleLive;
		var live = Player.Instance.Info.puzzleLive;

		if(live < maxLive)
		{
			if (!runningCountdown)
				StartCountdown(Player.Instance.Info.timeToNextPuzzleLive);
		}
		else
		{
			StopCountdown();
		}

	}

    public void RemoveLive()
	{
		var maxLive = Player.Instance.Info.maxPuzzleLive;
		var live = Player.Instance.Info.puzzleLive;

		if(!runningCountdown)
		{
			if (live < maxLive)
				StartCountdown(Player.Instance.Info.timeToNextPuzzleLive);
			else
				StopCountdown();
		}
	}

    public void StartCountdown(int timeToNextLive)
	{
		SetTimeToNextLive(timeToNextLive);
		timeSpan = TimeSpan.FromSeconds(timeToNextLive);
		runningCountdown = true;

		if (onCountdownUpdated == null)
			return;

		if (Player.Instance == null)
			Debug.Log("Player == null");
		
		onCountdownUpdated(timeSpan, Player.Instance.Info.puzzleLive);
	}

    public void StopCountdown()
	{
		runningCountdown = false;

		if (onCountdownFinished != null)
			onCountdownFinished();
	}

	public void Subscribe(Action<TimeSpan, int> updateCallback, Action finishCallback)
	{
		onCountdownUpdated += updateCallback;
		onCountdownFinished += finishCallback;

		var maxLive = Player.Instance.Info.maxPuzzleLive;
		var live = Player.Instance.Info.puzzleLive;

		if(live < maxLive)
		{
			if (onCountdownUpdated != null)
				onCountdownUpdated(timeSpan, live);
		}
		else
		{
			if (onCountdownFinished != null)
				onCountdownFinished();
		}
	}

	public void Unsubscribe(Action<TimeSpan, int> updateCallback, Action finishCallback)
	{
		if (onCountdownUpdated != null)
			onCountdownUpdated -= updateCallback;

		if (onCountdownFinished != null)
			onCountdownFinished -= finishCallback;
	}

    private void SetTimeToNextLive(int seconds)
	{
		var nextLiveTime = DateTime.Now.Add(TimeSpan.FromSeconds(seconds));
		Player.Instance.Info.nextPuzzleLiveTime = nextLiveTime.ToBinary().ToString();
	}
}
