﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class CharmTimer : TimeSystem {

    public void StartTimer()
	{
		StopCoroutine("RunningCountdown");
        StartCoroutine("RunningCountdown");

		var charm = Player.Instance.Info.CharmDic[CharmType.Red];
		var maxCharm = Player.Instance.Info.maxCharm;
		var timeToNextCharm = Player.Instance.Info.timeToNextCharm;

		Debug.Log("charm : " + charm);
		Debug.Log("maxCharm : " + maxCharm);
		Debug.Log("timeToNextCharm : " + timeToNextCharm);

		if(charm < maxCharm  && !string.IsNullOrEmpty(Player.Instance.Info.nextCharmTime))
		{
			var temp = Convert.ToInt64(Player.Instance.Info.nextCharmTime);
			var prevNextCharmTime = DateTime.FromBinary(temp);
			Debug.Log("prevNextCharmTime : " + prevNextCharmTime );


			TimeSpan remainingTime;
			var now = DateTime.Now;

			if(prevNextCharmTime > now)
			{
				remainingTime = prevNextCharmTime - now;

				if (charm < maxCharm)
					StartCountdown((int)remainingTime.TotalSeconds);
			}
			else
			{
				remainingTime = now - prevNextCharmTime;
				var charmToGive = ((int)remainingTime.TotalSeconds / timeToNextCharm) + 1;
				charm = charm + charmToGive;

				if (charm > maxCharm)
					charm = maxCharm;

				Player.Instance.Info.CharmDic[CharmType.Red] = charm;
                
				if (charm < maxCharm)
					StartCountdown(timeToNextCharm - ((int)remainingTime.TotalSeconds % timeToNextCharm));
				else
					StopCountdown();
			}
		}
	}

	IEnumerator RunningCountdown()
	{
		while(true)
		{
			yield return new WaitUntil(() => runningCountdown);

			timeSpan = timeSpan.Subtract(TimeSpan.FromSeconds(1));
			SetTimeToNextCharm((int)timeSpan.TotalSeconds);

			if (onCountdownUpdated != null)
				onCountdownUpdated(timeSpan, Player.Instance.Info.CharmDic[CharmType.Red]);

			if((int)timeSpan.TotalSeconds == 0)
			{
				StopCountdown();
				Player.Instance.RefreshCharm(CharmType.Red, 1);
			}

			yield return new WaitForSeconds(1);
		}
	}

    public void AddCharm()
	{
		var maxCharm = Player.Instance.Info.maxCharm;
		var charm = Player.Instance.Info.CharmDic[CharmType.Red];

		if(charm < maxCharm)
		{
			if (!runningCountdown)
				StartCountdown(Player.Instance.Info.timeToNextCharm);
		}
		else
		{
			StopCountdown();
		}
	}

    public void RemoveCharm()
	{
		var maxCharm = Player.Instance.Info.maxCharm;
		var charm = Player.Instance.Info.CharmDic[CharmType.Red];

		if(!runningCountdown)
		{
			if (charm < maxCharm)
				StartCountdown(Player.Instance.Info.timeToNextCharm);
			else
				StopCountdown();
		}
	}

    public void StartCountdown(int timeToNextCharm)
	{
		SetTimeToNextCharm(timeToNextCharm);
		timeSpan = TimeSpan.FromSeconds(timeToNextCharm);
		runningCountdown = true;

		if (onCountdownUpdated == null)
			return;

		onCountdownUpdated(timeSpan, Player.Instance.Info.CharmDic[CharmType.Red]);
	}

    public void StopCountdown()
	{
		runningCountdown = false;

		// 현재 부적 UI 갱신
		if (SceneManager.GetActiveScene().name == "Game" && Player.Instance.Info.currentCharm == CharmType.Red)
        {
			int remainCount = Player.Instance.GetCurrentCharmCount() - GameManager.instance.charmGM.enabledCharmCount;
			GameManager.instance.charmGM.remainCharm_UI.SetRemainCharmInfo(Player.Instance.GetCharmSprite(), remainCount);
        }
      
		if (onCountdownFinished != null)
			onCountdownFinished();
	}

	public void Subscribe(Action<TimeSpan, int> updateCallback, Action finishCallback)
	{
		onCountdownUpdated += updateCallback;
		onCountdownFinished += finishCallback;

		var maxCharm = Player.Instance.Info.maxCharm;
		var charm = Player.Instance.Info.CharmDic[CharmType.Red];

		if(charm < maxCharm)
		{
			if (onCountdownUpdated != null)
				onCountdownUpdated(timeSpan, charm);
		}
		else
		{
			if (onCountdownFinished != null)
				onCountdownFinished();
		}
	}

	public void Unsubscribe(Action<TimeSpan, int> updateCallback, Action finishCallback)
	{
		if (onCountdownUpdated != null)
			onCountdownUpdated -= updateCallback;

		if (onCountdownFinished != null)
			onCountdownFinished -= finishCallback;
	}

    private void SetTimeToNextCharm(int seconds)
	{
		var nextCharmTime = DateTime.Now.Add(TimeSpan.FromSeconds(seconds));
		Player.Instance.Info.nextCharmTime = nextCharmTime.ToBinary().ToString();
	}

}
