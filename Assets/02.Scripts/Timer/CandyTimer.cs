﻿using UnityEngine;
using System;
using System.Collections;

public class CandyTimer : TimeSystem {

    public void StartTimer()
	{      
		StopCoroutine("RunningCountdown");
		StartCoroutine("RunningCountdown");

		var candy = Player.Instance.Info.candy;
		var maxCandy = Player.Instance.Info.maxCandy;
		var timeToNextCandy = Player.Instance.Info.timeToNextCandy;

		if(candy < maxCandy && !string.IsNullOrEmpty(Player.Instance.Info.nextCandyTime))
		{
			var temp = Convert.ToInt64(Player.Instance.Info.nextCandyTime);
			var prevNextCandyTime = DateTime.FromBinary(temp);

			TimeSpan remainingTime;
			var now = DateTime.Now;

			if(prevNextCandyTime > now)
			{
				remainingTime = prevNextCandyTime - now;

				if (candy < maxCandy)
					StartCountdown((int)remainingTime.TotalSeconds);            
			}
			else
			{
				remainingTime = now - prevNextCandyTime;
				var candyToGive = ((int)remainingTime.TotalSeconds / timeToNextCandy) + 1;
				candy = candy + candyToGive;

				if (candy > maxCandy)
					candy = maxCandy;

				Player.Instance.Info.candy = candy;

				if (candy < maxCandy)
					StartCountdown(timeToNextCandy - ((int)remainingTime.TotalSeconds % timeToNextCandy));
				else
					StopCountdown();
			}
		}
	}


	IEnumerator RunningCountdown()
	{
		while(true)
		{
			yield return new WaitUntil(() => runningCountdown);

			timeSpan = timeSpan.Subtract(TimeSpan.FromSeconds(1));
            SetTimeToNextCandy((int)timeSpan.TotalSeconds);

            if (onCountdownUpdated != null)
                onCountdownUpdated(timeSpan, Player.Instance.Info.candy);

            if ((int)timeSpan.TotalSeconds == 0)
            {
                StopCountdown();
                Player.Instance.RefreshCandy(1);
            }

			yield return new WaitForSeconds(1);
		}
	}

    public void AddCandy()
	{
		var maxCandy = Player.Instance.Info.maxCandy;
        var candy = Player.Instance.Info.candy;
        if (candy < maxCandy)
        {
            if (!runningCountdown)
            {
                StartCountdown(Player.Instance.Info.timeToNextCandy);
            }
        }
        else
        {
            StopCountdown();
        }
	}

    public void RemoveCandy()
	{
		var maxCandy = Player.Instance.Info.maxCandy;
        var candy = Player.Instance.Info.candy;
            
        if(!runningCountdown)
        {
            if (candy < maxCandy)
                StartCountdown(Player.Instance.Info.timeToNextCandy);
            else
                StopCountdown();
        }      
	}

    public void RefillCandy()
	{
		Player.Instance.Info.candy = Player.Instance.Info.maxCandy;
        StopCountdown();
	}

    public void StartCountdown(int timeToNextCandy)
	{
		SetTimeToNextCandy(timeToNextCandy);
        timeSpan = TimeSpan.FromSeconds(timeToNextCandy);
        runningCountdown = true;

        if (onCountdownUpdated == null)
            return;

        onCountdownUpdated(timeSpan, Player.Instance.Info.candy);
	}

    public void StopCountdown()
	{
		runningCountdown = false;
        if (onCountdownFinished != null)
            onCountdownFinished();
	}

	public void Subscribe(Action<TimeSpan, int> updateCallback, Action finishCallback)
	{
		onCountdownUpdated += updateCallback;
        onCountdownFinished += finishCallback;

        var maxCandy = Player.Instance.Info.maxCandy;
        var candy = Player.Instance.Info.candy;

        if (candy < maxCandy)
        {
            if (onCountdownUpdated != null)
                onCountdownUpdated(timeSpan, candy);
        }
        else
        {
            if (onCountdownFinished != null)
                onCountdownFinished();
        }
	}

	public void Unsubscribe(Action<TimeSpan, int> updateCallback, Action finishCallback)
	{
		if (onCountdownUpdated != null)
            onCountdownUpdated -= updateCallback;

        if (onCountdownFinished != null)
            onCountdownFinished -= finishCallback;
	}

    private void SetTimeToNextCandy(int seconds)
	{
		var nextCandyTime = DateTime.Now.Add(TimeSpan.FromSeconds(seconds));
        Player.Instance.Info.nextCandyTime = nextCandyTime.ToBinary().ToString();
	}
 
}
