﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GhostEffect : MonoBehaviour {

    #region Variables
    private SpriteRenderer spriteRenderer;
    private Color color;
    private float timer = 0.3f;
    private float alpha = 0.4f;
    private bool isEnable = false;
	#endregion

    public void Init (GameObject target) 
	{
        transform.position = target.transform.position;
        transform.localScale = target.transform.localScale;

        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = target.GetComponent<SpriteRenderer>().sprite;
        color = new Color(45f / 255f, 6f / 255f, 75f / 255f);

        isEnable = true;
	}

	private void Update()
	{
        if (!isEnable)
            return;

        timer -= Time.deltaTime;
        alpha -= Time.deltaTime;

        color.a = alpha;
        spriteRenderer.color = color;

        if (timer <= 0)
            Destroy(gameObject);
	}

}
