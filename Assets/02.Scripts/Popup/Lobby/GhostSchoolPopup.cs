﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using GameVanilla.Game.Scenes;
using GameVanilla.Core;

public class GhostSchoolPopup : Popup {

	#region Variables
	public GhostSchoolItem[] Items;
	#endregion

	/// <summary>
    /// Unity's Awake method.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
    }

    /// <summary>
    /// Unity's Start method.
    /// </summary>
    protected override void Start()
    {
        base.Start();

		List<GhostInfoConfigs> list = MasterAgent.Instance.jsonConfigLoader.configs.ghostInfoConfigs;

		if (Items.Length < list.Count) {
			Debug.Log ("Items are not enough!");
			return;
		}

		for (int i = 0; i < list.Count; i++) 
		{
			Items [i].SetGhostSchoolItem (list [i]);
		}
    }

	/// <summary>
	/// This coroutine automatically closes the popup after its animation has finished.
	/// </summary>
	private IEnumerator AutoKill()
	{
		yield return new WaitForSeconds(2.4f);
		Close();
	}

}
