﻿using UnityEngine;
using UnityEngine.UI;
using GameVanilla.Core;

public class GhostSchoolItemInfoPopup : Popup 
{
	#region Variables
	public Text levelText;
	public Image itemImage;
	public Text nameText;
	public Text effectText;
	#endregion

	/// <summary>
    /// Unity's Awake method.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
    }

    /// <summary>
    /// Unity's Start method.
    /// </summary>
    protected override void Start()
    {
        base.Start();
    }
   
}
