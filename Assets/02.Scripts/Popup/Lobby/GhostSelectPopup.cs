﻿using UnityEngine;
using System.Collections;
using GameVanilla.Game.Scenes;
using GameVanilla.Core;

public class GhostSelectPopup : Popup {

	#region Variables

	#endregion

	/// <summary>
    /// Unity's Awake method.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
    }

    /// <summary>
    /// Unity's Start method.
    /// </summary>
    protected override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// This coroutine automatically closes the popup after its animation has finished.
    /// </summary>
    private IEnumerator AutoKill()
    {
        yield return new WaitForSeconds(2.4f);
        Close();
    }

}
