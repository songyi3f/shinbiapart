﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeCharmItem : MonoBehaviour {
    
	#region Variables
	public Charm charm;   
	public CharmType charmType;
	public Text titleText;
	public Image charmImage;
	public Text damageText;
	public Text countText;

    // 버튼
	public Button usingButton;
	public Button changeButton;
	public Button chargeButton;

	// 충전 타이머
	public GameObject chargeTimer;
	public Text chargeTimerText;

	#endregion
   
    public void SetItem()
	{
		usingButton.gameObject.SetActive(false);
        changeButton.gameObject.SetActive(false);
        chargeButton.gameObject.SetActive(false);
		chargeTimer.gameObject.SetActive(false);

		titleText.text = charm.name;
        charmImage.sprite = charm.image;
        damageText.text = charm.damage.ToString() + " 데미지";

		if(Player.Instance.GetCharmCount(charmType) > 0)
        {
			countText.text = "X " + Player.Instance.GetCharmCount(charmType).ToString();

			if (charmType == Player.Instance.Info.currentCharm)
                usingButton.gameObject.SetActive(true);  // 선택중인 부적 : 사용중 버튼
            else
                changeButton.gameObject.SetActive(true); // 교체 버튼
        }
        else
        {
            countText.text = "X 0";
            // 부적 개수가 0 이다
            chargeButton.gameObject.SetActive(true);
        }      
	}
   
}
