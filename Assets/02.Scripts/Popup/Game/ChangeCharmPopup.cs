﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeCharmPopup : MonoBehaviour
{   
	#region Variables
	public CharmGameManager charmGameManager;
	public ChangeCharmItem[] charmList;
	public GameObject CharmShopPopup;

	// 부적 충전   
	public bool isCharging = false;
	#endregion

	private void Start()
	{
		SetCharmItems();
	}
   
	public void OnCharmUpdated(System.TimeSpan timeSpan, int charm)
	{
		isCharging = true;
        charmList[0].chargeTimer.SetActive(true);
        charmList[0].chargeTimerText.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);  
	}

    public void OnCharmFinished()
	{
		Debug.Log("부적 시간 OFF");
        isCharging = false;
        charmList[0].chargeTimer.SetActive(false);
	}
   
    /// <summary>
    /// 부적 아이템 세팅
    /// </summary>
	public void SetCharmItems()
    {
        for (int i = 0; i < charmList.Length; i++)
		{
			// 기본 세팅
			charmList[i].SetItem();
			charmList[i].chargeTimer.SetActive(false);
            // 버튼 이벤트
			Charm charm = charmList[i].charm;
            // 교체 버튼 이벤트
            charmList[i].changeButton.onClick.RemoveAllListeners();
            charmList[i].changeButton.onClick.AddListener(() => ChangeButtonEvent(charm));
            // 충전 버튼 이벤트
            charmList[i].chargeButton.onClick.RemoveAllListeners();
            charmList[i].chargeButton.onClick.AddListener(() => ChargeButtonEvent(charm));
		}

		// 현재 부적 UI 갱신
        if (Player.Instance.Info.currentCharm == CharmType.Red)
        {
            int remainCount = Player.Instance.GetCurrentCharmCount() - charmGameManager.enabledCharmCount;
            charmGameManager.remainCharm_UI.SetRemainCharmInfo(Player.Instance.GetCharmSprite(), remainCount);
        }

    }
   
	/// <summary>
	/// 교체 버튼 이벤트
	/// </summary>
	void ChangeButtonEvent(Charm charm)
	{
		if(Player.Instance.GetCharmCount(charm.charmType) > 0)
		{
			// 부적 교체 : 개수 갱신 등
			charmGameManager.ChangeCharms(charm);
            // 부적 리스트 갱신
			SetCharmItems();         
		}
	}

    /// <summary>
    /// 충전 버튼 이벤트
    /// </summary>
	void ChargeButtonEvent(Charm charm)
	{
		if(Player.Instance.GetCharmCount(charm.charmType) <= 0)
		{
			CharmShopPopup.SetActive(true);
			gameObject.SetActive(false);
		}
	}
   
}
