﻿using UnityEngine;
using UnityEngine.UI;

public class ItemCharmShopItem : MonoBehaviour {

	#region Variables
	public Charm charm;
    public int count;
    public Image image;
    public Text titleText;
    public Text priceText;
    public Button GetButton;
	#endregion

	void Start () 
	{
		titleText.text = charm.name + " " + count.ToString() + "개";
	}

	/// <summary>
    /// 구입 버튼 이벤트
    /// </summary>
    public void GetButtonEvent()
    {
        Player.Instance.RefreshItemCharm(count);
    }

}
