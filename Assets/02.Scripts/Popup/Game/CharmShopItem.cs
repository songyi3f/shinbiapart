﻿using UnityEngine;
using UnityEngine.UI;

public class CharmShopItem : MonoBehaviour {

	#region Variables
	public Charm charm;
	public int count;
	public Image image;
	public Text titleText;
	public Text priceText;
	public Button GetButton;
	#endregion

	public void Start()
	{
		titleText.text = charm.name + " " + count.ToString() + "";
	}

	/// <summary>
	/// 구입 버튼 이벤트
	/// </summary>
	public void GetButtonEvent()
	{
		Player.Instance.RefreshCharm(charm.charmType, count);
		MasterAgent.Instance.timeManager.charmTimer.AddCharm();

		int charmCount = 0;
		foreach(CharmType type in Player.Instance.Info.CharmDic.Keys)
		{
			if (type == charm.charmType)
				continue;

			charmCount += Player.Instance.GetCharmCount(type);
		}
        
		if(charmCount == 0)
		{
			// 구입한 부적 외에 나머지 부적을 하나도 가지고 있지 않으면 : 구입한 부적을 선택 부적으로 지정
			Debug.Log("구입한 부적을 선택 부적으로 지정");
			Player.Instance.Info.currentCharm = charm.charmType;
		}
	}
}
