﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TF.JsonIO;
using UnityEngine;
using UnityEngine.UI;

public class Configs : MonoBehaviour {


}

/// <summary>
/// Total Json configs.
/// </summary>
[DataSerializable]
public class JsonConfigs
{
	[DataName("GhostInfoConfigs")]
	public List<GhostInfoConfigs> ghostInfoConfigs;
	[DataName("GhostAbilityS1Configs")]
	public List<GhostAbilityConfigs> ghostAbilityS1_Configs;
	[DataName("GhostAbilityS2Configs")]
	public List<GhostAbilityConfigs> ghostAbilityS2_Configs;

	// Ganglim Info
	[DataName("Ganglim")]
	public List<Ganglim> ganglimInfos;
	[DataName("GanglimItem")]
	public List<GanglimItem> ganglimItems;
	[DataName("GanglimSkill")]
	public List<GanglimSkill> ganglimSkill;
	[DataName("GanglimActiveSkill")]
	public List<GanglimSkillInfo> ganglim_Active_Skills;
	[DataName("GanglimPassiveSkill")]
	public List<GanglimSkillInfo> ganglim_Passive_Skills;

	// Lion Info
	[DataName("Lion")]
	public List<Lion> lionInfos;
	[DataName("LionItem")]
	public List<LionItem> lionItems;
	[DataName("LionSkill")]
	public List<LionSkill> LionSkill;
	[DataName("LionActiveSkill")]
	public List<LionSkillInfo> lion_Active_Skills;
	[DataName("LionPassiveSkill")]
	public List<LionSkillInfo> lion_Passive_Skills;

}

/// <summary>
/// Ghost Info
/// </summary>
[DataSerializable]
public class GhostInfoConfigs
{
	[DataName("ID")]
	public int ID;
	[DataName("Name")]
	public string Name;
	[DataName("Season")]
	public int Season;
	[DataName("Scene")]
	public string Scene;
	[DataName("Ability1")]
	public string Ability1;
	[DataName("Ability2")]
	public string Ability2;
	[DataName("SkillDesc")]
	public string SkillDesc;
	[DataName("ItemID1")]
	public int ItemID_1;
	[DataName("ItemID2")]
	public int ItemID_2;
	[DataName("ItemID3")]
	public int ItemID_3;
	[DataName("ItemID4")]
	public int ItemID_4;
}

/// <summary>
/// Ghost Ability Configs
/// </summary>
[DataSerializable]
public class GhostAbilityConfigs
{
	[DataName("ID")]
	public int ID;
	[DataName("Level")]
	public int Level;
	[DataName("NumOfLevelUp")]
	public int NumOfLevelUp;
	[DataName("SkillDamage")]
	public int SkillDamage;
	[DataName("ItemDamage1")]
	public int ItemDamage_1;
	[DataName("ItemDamage2")]
	public int ItemDamage_2;
	[DataName("ItemDamage3")]
	public int ItemDamage_3;
	[DataName("ItemDamage4")]
	public int ItemDamage_4;
}

/// <summary>
/// Ganglim Info
/// </summary>
[DataSerializable]
public class Ganglim
{
	[DataName("Level")]
	public int Level;
	[DataName("Bonus")]
	public int Bonus;
	[DataName("Damage")]
	public int Damage;
	[DataName("Cost")]
	public int Cost;
}

[DataSerializable]
public class GanglimItem
{
	[DataName("Level")]
	public int Level;
	[DataName("Damage")]
	public int Damage;
	[DataName("Cost")]
	public int Cost;
	[DataName("Name")]
	public string Name;
	[DataName("Desc")]
	public string Desc;
}

public enum SkillType
{
	[DataName("None")]  	None,
	[DataName("Active")] 	Active,
	[DataName("Passive")]  	Passive,
}

[DataSerializable]
public class GanglimSkill
{
	[DataName("Name")]
	public string Name;
	[DataName("SkillType")]
	public SkillType Type;
	[DataName("Desc")]
	public string Desc;
}

[DataSerializable]
public class GanglimSkillInfo
{
	[DataName("Level")]
	public int Level;
	[DataName("Stat")]
	public int Stat;
	[DataName("Cost")]
	public int Cost;
}


[DataSerializable]
public class Lion
{
	[DataName("Level")]
	public int Level;
	[DataName("Bonus")]
	public int Bonus;
	[DataName("Damage")]
	public int Damage;
	[DataName("Cost")]
	public int Cost;
}

[DataSerializable]
public class LionItem
{
	[DataName("Level")]
	public int Level;
	[DataName("Damage")]
	public int Damage;
	[DataName("Cost")]
	public int Cost;
	[DataName("Name")]
	public string Name;
	[DataName("Desc")]
	public string Desc;
}

[DataSerializable]
public class LionSkill
{
	[DataName("Name")]
	public string Name;
	[DataName("SkillType")]
	public SkillType Type;
	[DataName("Desc")]
	public string Desc;
}

[DataSerializable]
public class LionSkillInfo
{
	[DataName("Level")]
	public int Level;
	[DataName("Stat")]
	public int Stat;
	[DataName("Cost")]
	public int Cost;
}

