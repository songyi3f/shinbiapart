﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TF.JsonIO;
using UnityEngine;
using UnityEngine.UI;

public class JsonConfigLoader
{
	public readonly string FilePath = "data/gameconfigs";
	public JsonConfigs configs;

	public void StartLoading()
	{
		var txt = Resources.Load<TextAsset>(FilePath);
		if (txt != null){
			this.LoadFromString(txt.text);
		}
		else
			Debug.Log ("txt is Null");
	}

	public void StartLoadingFromWWW(MonoBehaviour mb)
	{
		mb.StartCoroutine(this.DownloadConfig());
	}

	public void WriteFile(string filePath, string data)
	{
		using (FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
		{
			using (var writer = new StreamWriter(fileStream)){
				writer.Write(data);
				writer.Flush();
			}
		}
	}

	private IEnumerator DownloadConfig()
	{
		var url = "http://nexnwebapp.appspot.com/json?key=songyi3f%40gmail.com%2F1owKJYtlucvoldrs8Qjct4EIOjevjm1ulZEt3zywikJQ&pass=2679518dee4f31cd";
		var www = new WWW(url);

		yield return www;
		var txt = www.text;
		try
		{
			if (LoadFromString(txt))
			{
				var fn = "gameconfigs";
				this.WriteFile(fn, txt);
			}
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.Message);
		}
	}

	bool LoadFromString(string txt)
	{
		if (string.IsNullOrEmpty(txt)){
			Debug.Log ("IsNullOrEmpty");
			return false;
		}

		SimpleJSON.JSONNode json = null;

		json = SimpleJSON.JSON.Parse(txt);

		if (json == null){
			Debug.Log ("Json Null");
			return false;
		}

		try
		{
			configs = JsonReader.LoadObject(json,configs, typeof(JsonConfigs),
				System.Reflection.BindingFlags.Instance | 
				System.Reflection.BindingFlags.Public | 
				System.Reflection.BindingFlags.NonPublic) as JsonConfigs;

			Debug.Log("Configs count: " + configs.ghostInfoConfigs.Count);
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.Message);
			return false;
		}
		Debug.Log ("True");
		return true;
	}
}
