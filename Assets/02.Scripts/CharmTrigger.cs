﻿using UnityEngine;
using UnityEngine.UI;

public class CharmTrigger : MonoBehaviour {

	#region Variables
    public bool isSelected; // 선택되었는지 여부 판단
    [HideInInspector] public float xPos; // 포인터가 이동할 기준 포지션
    private Image image;

    public bool isEnable // 이미지 활성 여부로 부적 활성 여부를 리턴 
    {
        get { return GetComponent<Image>().enabled; } 
        set
        {
            GetComponent<Image>().enabled = value;
            GetComponent<BoxCollider>().enabled = value;
            isSelected = false;
        }
    }
    #endregion

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    void Start()
    {
        isSelected = false;
        xPos = (int)transform.localPosition.x;
    }

    private void OnTriggerStay(Collider other)
    {
        if (string.Equals(other.name, "Pointer") && image.enabled)
            isSelected = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (string.Equals(other.name, "Pointer"))
            isSelected = false;
    }

}
