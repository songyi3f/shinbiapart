﻿using UnityEngine;

public enum CharmType
{
    Red, Yellow, Blue, Item
}

[CreateAssetMenu(fileName = "New Charm", menuName = "ScriptableObjects/Charm")]
public class Charm : ScriptableObject {

	#region Variables
	public CharmType charmType;
	public new string name;
	public Sprite image;
	public int damage;
	public int count;
	#endregion
}
