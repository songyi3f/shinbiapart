﻿using UnityEngine;

public class Hit : MonoBehaviour {

	#region Variables

	#endregion

	void Update () 
	{
        if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if(Input.touchCount >0 && Input.GetTouch(0).phase == TouchPhase.Began){
                CheckRaycast(Input.GetTouch(0).position);
            }

        }
        else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor){

            if (Input.GetMouseButton(0) && Input.GetMouseButtonDown(0))
            {
                CheckRaycast(Input.mousePosition);
            }
        }
	}

    /// <summary>
    /// Checks the raycast.
    /// </summary>
    /// <param name="inputVec">Input vec.</param>
    void CheckRaycast(Vector3 inputVec)
    {
        
        Vector3 mousePosFar = new Vector3(inputVec.x, inputVec.y, Camera.main.farClipPlane);
        Vector3 mousePosNear = new Vector3(inputVec.x, inputVec.y, Camera.main.nearClipPlane);
        Vector3 mousePosF = Camera.main.ScreenToWorldPoint(mousePosFar);
        Vector3 mousePosN = Camera.main.ScreenToWorldPoint(mousePosNear);

        RaycastHit hit;

        if (Physics.Raycast(mousePosN, mousePosF - mousePosN, out hit))
        {
            if (hit.collider.name == "Monster")
            {
                hit.collider.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
            }
        }
    }

}
