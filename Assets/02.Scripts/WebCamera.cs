﻿using UnityEngine;
using UnityEngine.UI;

public class WebCamera : MonoBehaviour {

    #region Variables
    private bool camAvailable;
    private WebCamTexture webCam;

    public RawImage background;
    public AspectRatioFitter fitter;
	#endregion

	void Start () 
	{
        WebCamDevice[] devices = WebCamTexture.devices;

        if(devices.Length == 0){
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++){
            if(!devices[i].isFrontFacing){
                webCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if(webCam == null){
            return;
        }

        webCam.Play();
        background.texture = webCam;

        camAvailable = true;
	}


	void Update()
	{
        if (!camAvailable)
            return;

        float ratio = (float)webCam.width / (float)webCam.height;
        fitter.aspectRatio = ratio;

        float scaleY = webCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        int orient = -webCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0f, 0f, orient);

	}

}
