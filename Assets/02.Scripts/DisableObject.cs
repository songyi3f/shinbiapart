﻿using UnityEngine;

public class DisableObject : MonoBehaviour {

	#region Variables
	public float disableTime;
	#endregion

	void OnEnable () 
	{
		CancelInvoke("OnDisable");
		Invoke("OnDisable", disableTime);
	}

	private void OnDisable()
	{
		gameObject.SetActive(false);
	}

}
