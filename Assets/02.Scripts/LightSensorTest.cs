﻿using UnityEngine;
using UnityEngine.UI;

public class LightSensorTest : MonoBehaviour {

	#region Variables
	LightSensorPluginScript lightSensor;
	public Text text;
   
	#endregion

	void Start () 
	{      
		lightSensor = GetComponent<LightSensorPluginScript>();
	}

	private void Update()
	{
		text.text = lightSensor.getLux().ToString();
	}

}
