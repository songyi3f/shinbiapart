﻿using UnityEngine;

public class CamGyro : MonoBehaviour {

    #region Variables
    GameObject camParent;
	#endregion

	void Start () 
	{
        camParent = new GameObject("CamParent");
        camParent.transform.position = this.transform.position;
        this.transform.parent = camParent.transform;
        Input.gyro.enabled = true;
	}

	private void Update()
	{
        camParent.transform.Rotate(0f, -Input.gyro.rotationRateUnbiased.y, 0f);
        this.transform.Rotate(-Input.gyro.rotationRateUnbiased.x, 0f, 0f);
	}

}
