﻿using UnityEngine;
using Spine.Unity;

public class GhostInfo : MonoBehaviour {

	#region Variables
    // 공격 위치
    public Transform targetPoint;
    // 귀신 머리 부적 위치
    public Transform charmTransform;
    // 귀신 스파인 애니메이션
    public SkeletonAnimation spainAnimation;
    #endregion

}
