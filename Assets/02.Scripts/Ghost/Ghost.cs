﻿using UnityEngine;
using Spine.Unity;

public enum AnimationName
{
    idle, attack, damage, death
}

public enum GhostGrade
{
    Normal, Advanced, Rare
}

[CreateAssetMenu(fileName = "New Ghost", menuName = "ScriptableObjects/Ghost")]
public class Ghost : ScriptableObject {

	#region Variables
    public GameObject monsterPrefab;
    public SkeletonDataAsset SD_DataAsset;

    // Info
    public new string name;
    public string description;
	public GhostGrade grade;
    public int hp;

    // Pos
    public Vector3 VR_Mode_Pos;
    public Vector3 VR_Mode_Scale;
    #endregion

}
