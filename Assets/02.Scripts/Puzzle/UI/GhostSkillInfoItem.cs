﻿using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using GameVanilla.Game.Common;
using DG.Tweening;

public class GhostSkillInfoItem : MonoBehaviour {

	public BlockType blockType;
	public Image maskBG;
	public SkeletonGraphic spineGraphic;
	public Text levelText;
	public Slider slider;

	private void Start()
	{
		slider.maxValue = 1000;
		slider.value = 0;
	}

    public void AddSkillEnergy(int value)
	{
		float hp = slider.value - value;

		slider.DOValue(hp, 0.3f).OnComplete(() => 
		{
			// 스킬 밸류 다 참 -> 스킬 사용 가능
            if (slider.value >= slider.maxValue)
            {
                slider.value = slider.maxValue;

                if (!DOTween.IsTweening(maskBG.transform))
                    maskBG.transform.DOScale(1.1f, 0.3f).SetLoops(-1, LoopType.Yoyo);
            }         
		});
	}

    /// <summary>
    /// 스킬 사용 버튼이벤트 
    /// </summary>
    public void OnClickSkillEvent()
	{
		slider.value = 0;
		maskBG.transform.DOKill();
		Debug.Log("스킬 사용!!!!!");
	}
}
