﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class PuzzleItem : MonoBehaviour {

    // [R][C]
	#region Variables
	public int Row;
    public int Column;


	public bool isEnable // 이미지 활성 여부로 부적 활성 여부를 리턴 
    {
        get { return GetComponent<Image>().enabled; }
        set { GetComponent<Image>().enabled = value; }
    }

	private Button button;
	public Image image;   
	public PuzzleColor puzzleColor;
	#endregion

	private void Start()
	{
		image = GetComponent<Image>();
		button = GetComponent<Button>();
        if(button == null)
			button = gameObject.AddComponent<Button>();
		button.transition = Selectable.Transition.None;

		SetPuzzleItem();
	}

	private void SetPuzzleItem()
	{
		isEnable = true;
		puzzleColor = (PuzzleColor)typeof(PuzzleColor).GetRandomEnumValue();

		switch(puzzleColor)
		{
			case PuzzleColor.Red: 
				image.color = Color.red; 
				break;
			case PuzzleColor.Yellow: 
				image.color = Color.yellow; 
				break;
			case PuzzleColor.Green: 
				image.color = Color.green; 
				break;
			case PuzzleColor.Blue: 
				image.color = Color.blue; 
				break;
			case PuzzleColor.White: 
				image.color = Color.white; 
				break;
			case PuzzleColor.Black: 
				image.color = Color.black; 
				break;
		}

		button.onClick.RemoveAllListeners();
		button.onClick.AddListener(() => PuzzleManager.Instance.ClickPuzzleItem(this));
    }
}
