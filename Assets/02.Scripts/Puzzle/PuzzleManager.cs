﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using DG.Tweening;


public enum PuzzleColor
{
	Red=1, Yellow=2, Green=3, Blue=4, White=5, Black=6
}

public static class EnumExtensions
{
    public static Enum GetRandomEnumValue(this Type t)
    {
        return Enum.GetValues(t)          // get values from Type provided
            .OfType<Enum>()               // casts to Enum
            .OrderBy(e => Guid.NewGuid()) // mess with order of results
            .FirstOrDefault();            // take first item in result
    }
}

public class PuzzleManager : MonoBehaviour {

	#region Variables
	public static PuzzleManager Instance = null;

	public List<PuzzleItem> puzzleList = new List<PuzzleItem>();
	public List<Transform> spawnPosList = new List<Transform>();

	private List<PuzzleItem> Complete_List = new List<PuzzleItem>();
	private List<PuzzleItem> Check_List = new List<PuzzleItem>();
	private PuzzleItem[,] arrays = new PuzzleItem[6, 6];


	// Prefab
	public GameObject puzzleItemPrefab; // 퍼즐 이동시 복사되어 이동만 할 프리팹
	#endregion
   
	private void Awake()
    {
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

	private void Start()
	{
		for (int i = 0; i < puzzleList.Count; i++)
		{
			arrays[puzzleList[i].Row, puzzleList[i].Column] = puzzleList[i];
		}
	}
    
    /// <summary>
    /// 퍼즐 클릭 이벤트
    /// </summary>
	public void ClickPuzzleItem(PuzzleItem item)
	{
		Complete_List.Clear();
		Check_List.Clear();
		Check_List.Add(item);

		while (Check_List.Count > 0)
		{      
			LoopCheckList(item);
		}

        // 한개짜리 블럭은 선택 할 수 없음
		if(Complete_List.Count <= 1)
		{
			Debug.Log("Complete_List.Count : " + Complete_List.Count + " 개라 리턴시킨다.");
			return;
		}

        // 색깔 같은 퍼즐 비활성 시켜줌
		for (int i = 0; i < Complete_List.Count; i++)
        {
			Complete_List[i].isEnable = false;
        }
        
		Invoke("AddPuzzleItem", 0.1f);
	}

	bool CheckPlayableItems()
	{
		List<PuzzleItem> list = new List<PuzzleItem>();

		foreach(PuzzleItem i in list)
		{
			List<PuzzleItem> childList = CheckPuzzleItem(i);

			foreach(PuzzleItem c in childList)
			{
				
			}
		}

		return false;
	}

    /// <summary>
	/// 선택 아이템의 상하좌우 같은 컬러 아이템이 중복되지 않게 최종리스트(Complete_List)에 저장
    /// </summary>
	void LoopCheckList(PuzzleItem item)
	{
		List<PuzzleItem> Temp_List = new List<PuzzleItem>();

		foreach (PuzzleItem i in Check_List)
        {
			List<PuzzleItem> childList = CheckPuzzleItem(i);

			foreach(PuzzleItem c in childList)
			{
				if (!Complete_List.Contains(c))
                    Temp_List.Add(c);
			}
                  
            Complete_List.Add(i);
        }

        Check_List.Clear();
        Check_List = Temp_List; // 다음 체크할 아이템 담아줌
	}

    /// <summary>
	/// 선택 아이템의 상하좌우를 체크해 같은 컬러의 아이템을 리스트에 담아 리턴
    /// </summary>
	List<PuzzleItem> CheckPuzzleItem(PuzzleItem item)
	{
		List<PuzzleItem> list = new List<PuzzleItem>();
		// 위 : Row -1      
        if(item.Row-1 >= 0)
        {
            PuzzleItem upItem = arrays[item.Row - 1, item.Column];
            if(upItem.puzzleColor == item.puzzleColor)
            {
                if(!list.Contains(upItem))
                    list.Add(upItem);
            }
        }

        // 아래 : Row +1
        if(item.Row+1 < 6)
        {
            PuzzleItem downItem = arrays[item.Row + 1, item.Column];
            if(downItem.puzzleColor == item.puzzleColor)
            {
                if (!list.Contains(downItem))
                    list.Add(downItem);
            }
        }

        // 좌 : Column -1
        if(item.Column-1 >= 0)
        {
            PuzzleItem leftItem = arrays[item.Row, item.Column - 1];
            if(leftItem.puzzleColor == item.puzzleColor)
            {
                if (!list.Contains(leftItem))
                    list.Add(leftItem);
            }
        }

        // 우 : Column +1
        if(item.Column+1 < 6)
        {
            PuzzleItem rightItem = arrays[item.Row, item.Column + 1];
            if(rightItem.puzzleColor == item.puzzleColor)
            {
                if (!list.Contains(rightItem))
                    list.Add(rightItem);
            }
        }      
		return list;
	}

    void AddPuzzleItem()
	{
		for (int i = 0; i < 6; i++)
		{    
			if (CheckRow(i) == null)
				continue;

			Debug.Log(" low : " + i);

			PuzzleItem emptyItem = CheckRow(i);
			List<PuzzleItem> move_List = new List<PuzzleItem>();

			for (int row = emptyItem.Row - 1; row >= 0; row--)
            {
				if (arrays[row, emptyItem.Column].isEnable)
					move_List.Add(arrays[row, emptyItem.Column]);
            }

			for(int row = emptyItem.Row - 1; row >= 0; row--)
			{
				arrays[row, emptyItem.Column].isEnable= false;
			}
		         
			Debug.Log("움직일 개수 : " + move_List.Count);
			for (int j = 0; j < move_List.Count; j++)
			{
				// 움직일 아이템 : move_List[j]
				// 기준이 되는 아이템 : emptyItem.Row-j
				PuzzleItem item = arrays[emptyItem.Row-j, emptyItem.Column];
				item.isEnable = false;

				//  빈 좌표 위에 활성 아이템 있음 : 이 아이템이 빈 좌표로 이동해야 함
                GameObject itemPrefab = Instantiate(puzzleItemPrefab, item.transform.parent);
                itemPrefab.transform.localPosition = item.transform.localPosition;
                itemPrefab.transform.localScale = item.transform.localScale;
				itemPrefab.GetComponent<Image>().color = move_List[j].image.color;
               
				// 데이터 변경            
				item.image.color = move_List[j].image.color;
				item.puzzleColor = move_List[j].puzzleColor;

                // 이동
                itemPrefab.transform
				          .DOLocalMoveY(item.transform.localPosition.y, 0.3f)
                          .SetEase(Ease.Linear)
                          .OnComplete(() =>
                          {
					          item.isEnable = true;
                              Destroy(itemPrefab);
                          });
            
			}

			//if(move_List.Count > 0)
			//{
			//	move_List[move_List.Count - 1].isEnable = false;  
			//	Debug.Log("움직일 것 중에 가장 마지막꺼 비활성");
			//}   

			// 0.3f초 후 새로 생성
			StartCoroutine(SpawnNewPuzzleItems(i));
		}

	}
    
	IEnumerator SpawnNewPuzzleItems(int row)
	{
		yield return new WaitForSeconds(0.3f);

		List<PuzzleItem> emptyItems = CheckEmptyPuzzleItem(row);
		Debug.Log(row.ToString() + " 열 : " + emptyItems.Count);

		for (int i = 0; i < emptyItems.Count; i++)
		{
			PuzzleItem targetItem = emptyItems[i];
			GameObject itemPrefab = Instantiate(puzzleItemPrefab, spawnPosList[row]);
            itemPrefab.transform.localPosition = new Vector3(0f, i * 233f, 0f);
			itemPrefab.transform.localScale = Vector3.one;
			PuzzleColor randomPC = (PuzzleColor)typeof(PuzzleColor).GetRandomEnumValue();
			itemPrefab.GetComponent<Image>().color = GetColor(randomPC);

			// 데이터 변경  
			targetItem.image.color = GetColor(randomPC);
			targetItem.puzzleColor = randomPC;

			itemPrefab.transform.SetParent(targetItem.transform.parent);

			itemPrefab.transform
			          .DOLocalMoveY(targetItem.transform.localPosition.y, 0.1f)
					  .SetEase(Ease.Linear)
			          .OnComplete(() => 
            			{
				            Debug.Log("i : " + i);
				            targetItem.isEnable = true;
				            Destroy(itemPrefab);
            			});
		}      
	}
   
	PuzzleItem CheckRow(int row)
	{      
		for (int i = 5; i >= 0; i--) //밑에서 부터 체크해서 위로~ 
		{
			if (!arrays[i, row].isEnable)
				return arrays[i, row];
		}
		return null;
	}

	List<PuzzleItem> CheckEmptyPuzzleItem(int row)
	{
		List<PuzzleItem> list = new List<PuzzleItem>();
		for (int i = 5; i >= 0; i--) //밑에서 부터 체크해서 위로~ 
        {
			if (!arrays[i, row].isEnable)
				list.Add(arrays[i, row]);
        }

		return list;
	}


	Color GetColor(PuzzleColor puzzleColor)
	{
		Color color = new Color();

		switch (puzzleColor)
        {
            case PuzzleColor.Red:
				color = Color.red;
                break;
            case PuzzleColor.Yellow:
                color = Color.yellow;
                break;
            case PuzzleColor.Green:
                color = Color.green;
                break;
            case PuzzleColor.Blue: 
                color = Color.blue; 
                break;
            case PuzzleColor.White: 
                color = Color.white; 
                break;
            case PuzzleColor.Black: 
                color = Color.black; 
                break;
        }

		return color;
	}
}
