﻿using UnityEngine;

public class FreeItemCharmUI : MonoBehaviour {

	#region Variables
    public bool isTrigger = false;
	#endregion

	private void OnTriggerStay(Collider other)
    {
		if(other.tag == "Pointer")
		{
			isTrigger = true;
		}
		else
		{
			isTrigger = false;
		}
		Debug.Log("freeItrmCharm other" + other.tag);
    }   

}
