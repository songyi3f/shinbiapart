﻿using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using System;

public class ResultUI : MonoBehaviour {

	#region Variables
	public Text monsterNameText;
	public SkeletonGraphic SD_Graphic;

	// 다시하기 버튼
	public GameObject candyRemainTime;
	public Text candyRemainTimeText;
	public Text candyCountText;
	#endregion

	public void Start()
	{
		MasterAgent.Instance.timeManager.candyTimer.Subscribe(OnCandyCountdownUpdated, OnCandyCountdownFinished);
	}

	private void OnDestroy()
    {
        MasterAgent.Instance.timeManager.candyTimer.Unsubscribe(OnCandyCountdownUpdated, OnCandyCountdownFinished);
    }

	/// <summary>
    /// 도깨비엿 타이머 업데이트 이벤트
    /// </summary>
    void OnCandyCountdownUpdated(TimeSpan timeSpan, int candy)
    {
        candyRemainTime.SetActive(true);
        candyRemainTimeText.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
        candyCountText.text = "X " + candy.ToString();
    }

    /// <summary>
    /// 도깨비엿 타이머 종료 이벤트
    /// </summary>
    void OnCandyCountdownFinished()
    {
        candyRemainTime.SetActive(false);
        candyCountText.text = "X " + Player.Instance.Info.candy.ToString();
    }

}
