﻿using UnityEngine;
using UnityEngine.UI;

public class RemainCharmUI : MonoBehaviour {

	#region Variables
	public Image charmCountImg;
    public Text charmCountText;
	#endregion

	/// <summary>
    /// 오른쪽 남은 부적 UI 갱신
    /// </summary>
    public void SetRemainCharmInfo(Sprite sprite, int count)
    {
        charmCountImg.sprite = sprite;
        charmCountText.text = (count > 0) ? "X " + count.ToString() : "X 0";
    }

}
