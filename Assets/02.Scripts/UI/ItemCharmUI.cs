﻿using UnityEngine;
using UnityEngine.UI; 

public class ItemCharmUI : MonoBehaviour {

	#region Variables
	public Image BG_image;
	public Image image;
	public Text titleText;
	public Text countText;

	public Button chargeItemCharmButton;
	public bool isActive = false;
	public bool isTrigger = false;
	#endregion

    public void SetItemCharmUI()
	{
		if (Player.Instance.Info.itemCharm > 0)
        {
			isActive = true;
            // 아이템 획득 부적 활성          
			countText.text = "X " + Player.Instance.Info.itemCharm.ToString();
			GetComponent<BoxCollider>().enabled = true;
			chargeItemCharmButton.gameObject.SetActive(false);
			BG_image.color = Color.red;
			titleText.color = Color.white;
			countText.color = Color.white;
			image.color = Color.white;
        }
        else
        {
			isActive = false;
			countText.text = "X 0";
			GetComponent<BoxCollider>().enabled = false;
			chargeItemCharmButton.gameObject.SetActive(true);
			BG_image.color = Color.gray;
			titleText.color = Color.gray;
            countText.color = Color.gray;
            image.color = Color.gray;
        }
	}

	private void OnTriggerStay(Collider other)
    {
		isTrigger = (other.tag == "Pointer" && isActive) ? true : false;
    }

}
