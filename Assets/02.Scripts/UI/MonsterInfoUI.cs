﻿using UnityEngine;
using UnityEngine.UI;

public class MonsterInfoUI : MonoBehaviour {

	#region Variables
	public Text nameText;
	public Text descriptionText;
	public Slider monsterHPSlider;
	#endregion

    public void SetMonsterInfo(Ghost ghost)
	{
		nameText.text = ghost.name;
		descriptionText.text = ghost.description;
		// 몬스터 HP 초기화
		monsterHPSlider.maxValue = ghost.hp;
		monsterHPSlider.value = ghost.hp;

		// TODO : 분등급에 따라서 색상 구
	}

}
