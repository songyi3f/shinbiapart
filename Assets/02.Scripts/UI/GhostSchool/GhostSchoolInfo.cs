﻿using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;
using GameVanilla.Core;

public class GhostSchoolInfo : Popup {

	#region Variables
	public Text nameLevelText;
	public GameObject ghostBG;
	public Text DescText;

	[HideInInspector] public GameObject spineGraphicGO;
	[HideInInspector] public SkeletonGraphic spineGraphic;
	#endregion

    /// <summary>
    /// 고스트 스쿨 세팅
    /// </summary>
	public void SetGhostSchoolInfo(GhostInfoConfigs item, SkeletonDataAsset SD_DataAsset)
	{ 
		if(spineGraphicGO != null)
			Destroy(spineGraphicGO);

		spineGraphicGO = new GameObject("Skeleton Graphic");
		spineGraphicGO.transform.SetParent(ghostBG.transform);
		spineGraphicGO.transform.localPosition = new Vector3(0f, -100f, 0f);
		spineGraphicGO.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
		spineGraphicGO.SetActive(false); // 껏다 켜줘야 스파인 그래픽 깨지지 않음... 

		spineGraphic = spineGraphicGO.AddComponent<SkeletonGraphic>();
		spineGraphic.skeletonDataAsset = SD_DataAsset;
		spineGraphic.startingAnimation = "idle";
		spineGraphic.startingLoop = true;
		spineGraphicGO.SetActive(true);

		// TODO : My Ghost Level
		nameLevelText.text = item.Name + " Lv.3"; 
		DescText.text = "등장 : " + item.Scene + "\n";
		DescText.text += "특징 : " + item.Ability1 + "\n          " + item.Ability2 +"\n";
		DescText.text += "스킬 : " + item.SkillDesc + "\n";

		gameObject.SetActive(true);
	}


}
