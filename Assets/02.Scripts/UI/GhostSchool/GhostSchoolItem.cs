﻿using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;
using GameVanilla.Core;

public class GhostSchoolItem : MonoBehaviour {

	#region Variables   
	public SkeletonDataAsset spineDataAsset;
	public Ghost ghost;

	[HideInInspector] public GameObject spineGraphicParent;
	[HideInInspector] public SkeletonGraphic spineGraphic;
	public Text levelText;
	public Text nameText;
	public GhostInfoConfigs info;

	private LobbyManager lobbyManager;
	#endregion

	public void Start()
	{
		lobbyManager = GameObject.Find ("@LobbyManager").GetComponent<LobbyManager>();
	}

	public void SetGhostSchoolItem(GhostInfoConfigs config)
	{
		this.info = config;
		// TODO  : if my Ghost  
		levelText.text = "Lv.1";
		nameText.text = info.Name;
      
		if (spineGraphicParent != null)
			Destroy(spineGraphicParent);

		spineGraphicParent = new GameObject("Skeleton Graphic");
		spineGraphicParent.transform.SetParent(transform);      
		spineGraphicParent.transform.localPosition = new Vector3(0f, 26f, 0f);
		spineGraphicParent.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
		spineGraphicParent.SetActive(false); // 껏다 켜줘야 스파인 그래픽 깨지지 않음...

		spineGraphic = spineGraphicParent.AddComponent<SkeletonGraphic>();      
		spineGraphic.skeletonDataAsset = ghost.SD_DataAsset;
		spineGraphic.startingAnimation = "idle";
		spineGraphic.startingLoop = true;
		spineGraphicParent.SetActive(true);
	}

	public void SelectEvent()
	{
		if (lobbyManager != null) 
		{
			lobbyManager.OpenPopup<GhostSchoolInfo>("Popups/GhostSchoolInfoPopup", popup =>
				{
					popup.SetGhostSchoolInfo(info, ghost.SD_DataAsset);
					popup.gameObject.SetActive(true);
				});
		}
	}

}
