﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{   
	#region Variables
	public static Player Instance = null;

    // 플레이어 데이터
	static private Info _info = new Info();   
	public Info Info
	{
		get { return _info; }
		set { _info = value; }
	}

	#endregion
    
	private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
   
    /// <summary>
    /// 퍼즐 하트 개수 갱신
    /// </summary>
    public void RefreshPuzzleLive(int value)
	{
		Info.puzzleLive += value;
		if (Info.puzzleLive < 0)
			Info.puzzleLive = 0;

		if (value > 0)
			MasterAgent.Instance.timeManager.liveTimer.AddLive();
		else
			MasterAgent.Instance.timeManager.liveTimer.RemoveLive();
	}

	/// <summary>
	/// 엿 개수 갱신
	/// </summary>
	public void RefreshCandy(int value)
	{
		Debug.Log("엿 개수 갱신 : " + value);
		Info.candy += value;
		if (Info.candy < 0)
			Info.candy = 0;
         
		if (value > 0)
			MasterAgent.Instance.timeManager.candyTimer.AddCandy();
        else
			MasterAgent.Instance.timeManager.candyTimer.RemoveCandy();
	}

    /// <summary>
    /// 부적 개수 갱신
    /// </summary>
	public void RefreshCharm(CharmType type, int count)
	{
		Info.CharmDic[type] += count;

		if (Info.CharmDic[type] < 0) // 0보다 작을 때는 0으로 고정
			Info.CharmDic[type] = 0;
      
		if (count > 0)
			MasterAgent.Instance.timeManager.charmTimer.AddCharm();
		else
			MasterAgent.Instance.timeManager.charmTimer.RemoveCharm();

		if(MasterAgent.Instance.gameManager != null)
		{
			MasterAgent.Instance.gameManager.charmGM.ChangeCharmPopup.SetCharmItems();         
		}
	}
 
    public void RefreshCharm(Charm charm, int count)
    {
		RefreshCharm(charm.charmType, count);
    }

	public void RefreshCharm(int count)
    {
		RefreshCharm(Info.currentCharm, count);
    }
   
    /// <summary>
    /// 부적 변경
    /// </summary>
	public void SetCurrentCharm(CharmType type)
	{
		Info.currentCharm = type;
	}

    /// <summary>
    /// 부적 카운트 리턴 
    /// </summary>
	public int GetCharmCount(CharmType type)
	{
		return Info.CharmDic[type];
	}

    /// <summary>
    /// 현재 선택된 부적 카운트 리턴
    /// </summary>
	public int GetCurrentCharmCount()
	{
		return Info.CharmDic[Info.currentCharm];
	}

    /// <summary>
	/// 부적 이미지(스프라이트) 리턴
    /// </summary>
	public Sprite GetCharmSprite()
	{
		if (MasterAgent.Instance.gameManager != null)
		{
			for (int i = 0; i < MasterAgent.Instance.gameManager.charmList.Length; i++)
			{
				if (MasterAgent.Instance.gameManager.charmList[i].charmType == Info.currentCharm)
					return MasterAgent.Instance.gameManager.charmList[i].image;
			}
		}
		return null;
	}

    /// <summary>
    /// 부적 데미지
    /// </summary>
    public int GetCharmDamage()
	{
		if (MasterAgent.Instance.gameManager != null)
		{
			return MasterAgent.Instance.gameManager.charmList.First((x) => x.charmType == Info.currentCharm).damage;
		}
		return int.MinValue;
	}

	/// <summary>
    /// 아이템 부적 개수 갱신
    /// </summary>
    public void RefreshItemCharm(int count)
    {
		Info.itemCharm += count;

		if (MasterAgent.Instance.gameManager != null)
        {
			// UI 갱신
			MasterAgent.Instance.gameManager.charmGM.LastAttack_UI.itemCharm_UI.SetItemCharmUI();      
        }      
    }
}

[Serializable]

public class Info
{
	//  ================================== 도깨비 엿 충전
	// 엿 보유 개수
	public int candy;
	// 최대 엿 보유 개수
	public int maxCandy;
	// 다음 충전시간
	public string nextCandyTime;
	// 도깨비엿 충전 시간 간격
	public int timeToNextCandy;

	//  ================================== 퍼즐 라이브 충전
    // 퍼즐 라이브
    public int puzzleLive;
    public int maxPuzzleLive;
    public string nextPuzzleLiveTime;
    public int timeToNextPuzzleLive;
   


	// 선택중인 부적
	public CharmType currentCharm;
    // 소유중인 부적 목록
	public Dictionary<CharmType, int> CharmDic;

	// ===================================== 기본 부적 충전
	// 기본 부적 최대 개수
	public int maxCharm;
	// 다음 충전 시간
	public string nextCharmTime;
	// 기본 부적 충전 시간 간격
	public int timeToNextCharm;


	// 아이템 부적
	public int itemCharm;
    
    public Info()
	{
		puzzleLive = 5;
		maxPuzzleLive = 5;
		nextPuzzleLiveTime = string.Empty;
		timeToNextPuzzleLive = 300;

		candy = 5;
		maxCandy = 5;
		nextCandyTime = string.Empty;
		timeToNextCandy = 300;

        // 부적
		currentCharm = CharmType.Red;
		CharmDic = new Dictionary<CharmType, int>();
		CharmDic.Add(CharmType.Red, 30); // 붉은 부적 30개는 기본제공
		CharmDic.Add(CharmType.Blue, 0);
		CharmDic.Add(CharmType.Yellow, 0);

		maxCharm = 30;
		nextCharmTime = string.Empty;
		timeToNextCharm = 30;

		itemCharm = 0;
	}
}
