﻿using UnityEngine;

[System.Serializable]
public class Sound {

	#region Variables
	public string name;
	public AudioClip clip;

	[HideInInspector]
	public AudioSource source;
	public bool loop;
	#endregion
   
}
