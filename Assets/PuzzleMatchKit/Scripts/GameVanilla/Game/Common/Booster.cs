﻿// Copyright (C) 2017 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

using System.Collections.Generic;

using UnityEngine;

using GameVanilla.Game.Scenes;

namespace GameVanilla.Game.Common
{
	/// <summary>
	/// The base class for boosters. Boosters are tile entities that provide a useful gameplay effect when used
	/// (usually exploding a specific set of blocks from the level).
	/// </summary>
	public class Booster : TileEntity
	{
		public BoosterType type;

		/// <summary>
		/// 상하좌우 주변 오브젝트를 모두 가져온다 
		/// </summary>
		/// <returns>The around object.</returns>
		/// <param name="scene">Scene.</param>
		/// <param name="idx">Index.</param>
		public virtual List<GameObject> GetAroundObject(GameScene scene, int idx)
		{
			var tiles = new List<GameObject>();
			var x = idx % scene.level.width;
			var y = idx / scene.level.width;
			AddTile(tiles, scene, x, y - 1);
			AddTile(tiles, scene, x - 1, y);
			AddTile(tiles, scene, x, y);
			AddTile(tiles, scene, x + 1, y);
			AddTile(tiles, scene, x, y + 1);

			return tiles;
		}

		public virtual List<GameObject> A_A_Match(GameScene scene, int idx)
		{
			var tiles = new List<GameObject>();
			var x = idx % scene.level.width;
			var y = idx / scene.level.width;

			for (var i = 0; i < scene.level.width; i++)
			{
				AddTile(tiles, scene, i, y);
			}

			for (var j = 0; j < scene.level.height; j++)
			{
				AddTile(tiles, scene, x, j);
			}

			return tiles;
		}


		public virtual List<GameObject> B_A_Match(GameScene scene, int idx, Bomb.Direction direction)
		{
			var tiles = new List<GameObject>();
			var x = idx % scene.level.width;
			var y = idx / scene.level.width;

			switch (direction)
			{
				case Bomb.Direction.Horizontal:
					{
						for (var i = 0; i < scene.level.width; i++)
						{
							AddTile(tiles, scene, i, y);
						}

						for (var i = 0; i < scene.level.width; i++)
						{
							AddTile(tiles, scene, i, y - 1);
						}
						for (var i = 0; i < scene.level.width; i++)
						{
							AddTile(tiles, scene, i, y + 1);
						}

						// hasMatchingCombo = true;
					}
					break;

				case Bomb.Direction.Vertical:
					{
						for (var j = 0; j < scene.level.height; j++)
						{
							AddTile(tiles, scene, x, j);
						}

						for (var j = 0; j < scene.level.height; j++)
						{
							AddTile(tiles, scene, x - 1, j);
						}
						for (var j = 0; j < scene.level.height; j++)
						{
							AddTile(tiles, scene, x + 1, j);
						}
						// hasMatchingCombo = true;
					}
					break;
			}

			return tiles;
		}

		public virtual List<GameObject> B_A_A_Match(GameScene scene, int idx)
		{
			var tiles = new List<GameObject>();
			var x = idx % scene.level.width;
			var y = idx / scene.level.width;

			for (var i = 0; i < scene.level.width; i++)
			{
				AddTile(tiles, scene, i, y);
			}
			for (var i = 0; i < scene.level.width; i++)
			{
				AddTile(tiles, scene, i, y - 1);
			}
			for (var i = 0; i < scene.level.width; i++)
			{
				AddTile(tiles, scene, i, y + 1);
			}

			for (var j = 0; j < scene.level.height; j++)
			{
				AddTile(tiles, scene, x, j);
			}
			for (var j = 0; j < scene.level.height; j++)
			{
				AddTile(tiles, scene, x - 1, j);
			}
			for (var j = 0; j < scene.level.height; j++)
			{
				AddTile(tiles, scene, x + 1, j);
			}

			return tiles;
		}

		public virtual List<GameObject> B_B_Match(GameScene scene, int idx)
		{
			var tiles = new List<GameObject>();
			var x = idx % scene.level.width;
			var y = idx / scene.level.width;

			AddTile(tiles, scene, x - 1, y - 1);
			AddTile(tiles, scene, x, y - 1);
			AddTile(tiles, scene, x + 1, y - 1);
			AddTile(tiles, scene, x - 1, y);
			AddTile(tiles, scene, x, y);
			AddTile(tiles, scene, x + 1, y);
			AddTile(tiles, scene, x - 1, y + 1);
			AddTile(tiles, scene, x, y + 1);
			AddTile(tiles, scene, x + 1, y + 1);

			AddTile(tiles, scene, x - 2, y - 2);
			AddTile(tiles, scene, x - 1, y - 2);
			AddTile(tiles, scene, x, y - 2);
			AddTile(tiles, scene, x + 1, y - 2);
			AddTile(tiles, scene, x + 2, y - 2);
			AddTile(tiles, scene, x - 2, y - 1);
			AddTile(tiles, scene, x + 2, y - 1);
			AddTile(tiles, scene, x - 2, y);
			AddTile(tiles, scene, x + 2, y);
			AddTile(tiles, scene, x - 2, y + 1);
			AddTile(tiles, scene, x + 2, y + 1);
			AddTile(tiles, scene, x - 2, y + 2);
			AddTile(tiles, scene, x - 1, y + 2);
			AddTile(tiles, scene, x, y + 2);
			AddTile(tiles, scene, x + 1, y + 2);
			AddTile(tiles, scene, x + 2, y + 2);

			return tiles;
		}
  
		public virtual List<GameObject> C_C_Match(GameScene scene, int idx)
		{
			var tiles = new List<GameObject>();
			var x = idx % scene.level.width;
			var y = idx / scene.level.width;

			for (var j = 0; j < scene.level.height; j++)
			{
				for (var i = 0; i < scene.level.width; i++)
				{
					var tileIndex = i + (j * scene.level.width);
					var tile = scene.tileEntities[tileIndex];
					if (tile != null)
					{
						var block = tile.GetComponent<Block>();
						if (block != null &&
							(block.type == BlockType.Block1 ||
							 block.type == BlockType.Block2 ||
							 block.type == BlockType.Block3 ||
							 block.type == BlockType.Block4 ||
							 block.type == BlockType.Block5 ||
							 block.type == BlockType.Block6))
						{
							AddTile(tiles, scene, i, j);
						}
					}
				}
			}
			AddTile(tiles, scene, x, y);

			var up = new TileDef(x, y - 1);
			var down = new TileDef(x, y + 1);
			var left = new TileDef(x - 1, y);
			var right = new TileDef(x + 1, y);

			AddTile(tiles, scene, x, y - 1);
			AddTile(tiles, scene, x, y + 1);
			AddTile(tiles, scene, x - 1, y);
			AddTile(tiles, scene, x + 1, y);

			return tiles;
		}


		/// <summary>
		/// Resolves the booster's effect at the specified tile index of the specified scene.
		/// </summary>
		/// <param name="scene">The scene in which to apply the booster.</param>
		/// <param name="idx">The tile index in which the booster is located.</param>
		/// <returns>The list containing the blocks destroyed by the booster.</returns>
		public virtual List<GameObject> Resolve(GameScene scene, int idx)
		{
			return new List<GameObject>();
		}

		/// <summary>
		/// Shows the visual effects when the booster effect is resolved.
		/// </summary>
		/// <param name="gamePools">The game pools containing the visual effects.</param>
		/// <param name="scene">The scene in which to apply the booster.</param>
		/// <param name="idx">The tile index in which the booster is located.</param>
		public virtual void ShowFx(GamePools gamePools, GameScene scene, int idx)
		{
		}

		/// <summary>
		/// Adds the tile located at the specified indexes to the specified list of tiles.
		/// </summary>
		/// <param name="tiles">The list of tiles.</param>
		/// <param name="scene">The game scene.</param>
		/// <param name="x">The x index of the tile to add.</param>
		/// <param name="y">The y index of the tile to add.</param>
		protected virtual void AddTile(List<GameObject> tiles, GameScene scene, int x, int y)
		{
			if (x < 0 || x >= scene.level.width ||
				y < 0 || y >= scene.level.height)
			{
				return;
			}

			var tileIndex = x + (y * scene.level.width);
			var tile = scene.tileEntities[tileIndex];
			if (tile != null)
			{
				var block = tile.GetComponent<Block>();
				if (block != null && (block.type == BlockType.Empty || block.type == BlockType.Collectable))
				{
					return;
				}
				if (tiles.Contains(tile))
				{
					return;
				}
				tiles.Add(tile);
			}
		}

		/// <summary>
		/// Returns true if the specified coordinates are valid within the specified level and false otherwise.
		/// </summary>
		/// <param name="level">The level.</param>
		/// <param name="x">The x coordinate to check.</param>
		/// <param name="y">The y coordinate to check.</param>
		/// <returns>True if the specified coordinates are valid within the specified level; false otherwise.</returns>
		protected bool IsValidTile(Level level, int x, int y)
		{
			return x >= 0 && x < level.width && y >= 0 && y < level.height;
		}
	}
}
