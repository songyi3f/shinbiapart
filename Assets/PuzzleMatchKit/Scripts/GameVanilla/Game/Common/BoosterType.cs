﻿// Copyright (C) 2017 gamevanilla. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

namespace GameVanilla.Game.Common
{
    /// <summary>
    /// The available booster types.
    /// </summary>
    public enum BoosterType
    {
        HorizontalBomb = 0,
        VerticalBomb = 1,
        Dynamite = 2,
        ColorBomb = 3
    }

	public enum MatchType
	{
		A_A_Match,
        B_A_H_Match,
        B_A_V_Match,
        B_A_A_Match,
        B_B_Match,
        C_A_Match,
		C_B_Match,
		C_C_Match,
        None
	}
}
